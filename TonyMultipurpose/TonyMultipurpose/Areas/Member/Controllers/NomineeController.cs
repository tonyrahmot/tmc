﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Member.BLL;
using TonyMultipurpose.Areas.Member.Models;

namespace TonyMultipurpose.Areas.Member.Controllers
{
    //ataur
    public class NomineeController : Controller
    {
        // GET: Member/Nominee
        public ActionResult Index()
        {
            NomineeDetail objNomineeDetail=new NomineeDetail();
            NomineeDetalBLL objNomineeDetalBll=new NomineeDetalBLL();
            List<NomineeDetail> nomineeInfo= objNomineeDetalBll.GetAllNomineeInfo();
            
            return View(nomineeInfo);
        }

        [HttpGet]
        public ActionResult Save()
        {
            MemberInfoBLL objMemberInfoBll=new MemberInfoBLL();
            var model = new NomineeDetail()
            {
                memberNames = objMemberInfoBll.GetAllMemberInfo()
            };
            return View(model);
        }

        //[HttpGet]
        //public JsonResult AutoCompleteConsignment(string term)
        //{
        //    TruckDetailBLL objTruckDetailBLL = new TruckDetailBLL();
        //    List<String> result = new List<String>();
        //    result = objTruckDetailBLL.GetAllTruckDetail().Where(x => x.ConsignmentNumber.ToString().StartsWith(term)).Select(y => y.ConsignmentNumber.ToString()).ToList();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult AutoCompleteAccountNumber(string term)
        {
            MemberInfoBLL objMemberInfoBll=new MemberInfoBLL();
            List<string> accountNumber=new List<string>();
            accountNumber =
                objMemberInfoBll.GetAllMemberInfo()
                    .Where(x => x.AccountNumber.ToString().StartsWith(term))
                    .Select(y => y.AccountNumber.ToString())
                    .ToList();
            return Json(accountNumber, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMemberNameByAccountNo(string accountNo)
        {
            MemberInfoBLL objMemberInfoBll = new MemberInfoBLL();
            
          var  memberName =
                objMemberInfoBll.GetAllMemberInfo()
                    .Where(x => x.AccountNumber == accountNo)
                    .FirstOrDefault();
            return Json(memberName, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(NomineeDetail objNomineeDetail)
        {

            return View();
        }
        public ActionResult Edit()
        {
            return View();
        }
        public ActionResult Delete()
        {
            return View();
        }
    }
}