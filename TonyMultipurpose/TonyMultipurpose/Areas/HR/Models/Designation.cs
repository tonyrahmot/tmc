﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class Designation
    {
        public int DesigId { get; set; }

        public string DesignationName { get; set; }

        public string DesignationCode { get; set; }

        public string FunctionalDesignation { get; set; }
    }
}