﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class NomineeDetail:CommonModel
    {
        [Key]
        public int NomineeId { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }



        [Display(Name = "Member Name")]
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public List<MemberInfo> memberNames { get; set; }


        [Required]
        [Display(Name = "Nominee Name")]
        public string NomineeName { get; set; }

        [Required]
        [Display(Name = "Father Name")]
        public string NomineeFatherName { get; set; }

        [Required]
        [Display(Name = "Mother Name")]
        public string NomineeMotherName { get; set; }


        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }


        [Display(Name = "Contact No.")]
        public string ContactNumber { get; set; }

        [Required]
        [Display(Name = "Adress")]
        public string Address { get; set; }


        [Display(Name = "NID")]
        public string NID { get; set; }

        [Required]
        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        public string DateOfBirth { get; set; }


        [Display(Name="Occupation")]
        public string Occupation { get; set; }


        [Display(Name = "Designation")]
        public string Designation { get; set; }


        [Display(Name = "Image")]
        public byte[] Image { get; set; }
    }
}