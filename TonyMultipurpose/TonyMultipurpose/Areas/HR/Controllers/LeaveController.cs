﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.HR.BLL;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Controllers
{
    public class LeaveController : Controller
    {
        #region Leave Entry
        LeaveInformationBLL objLeaveInformationBLL = new LeaveInformationBLL();
        CommonResult objCommonResult = new CommonResult();
        public ActionResult LeaveEntry()
        {
            //List<LeaveInformation> LeaveInformation = objLeaveInformationBLL.GetAllLeaveInfo();
            //return View(LeaveInformation);
            return View();
        }
        [HttpPost]
        public ActionResult SaveLeaveEntry(LeaveInformation objLeaveInformation)
        {
            objCommonResult = new CommonResult();
            objLeaveInformationBLL = new LeaveInformationBLL();
            objCommonResult = objLeaveInformationBLL.SaveLeaveEntry(objLeaveInformation);
            return new JsonResult { Data = new { objCommonResult.Status, objCommonResult.Message } };
        }
        [HttpPost]
        public ActionResult UpdateLeaveEntry(LeaveInformation objLeaveInformation)
        {
            objCommonResult = new CommonResult();
            objLeaveInformationBLL = new LeaveInformationBLL();
            objCommonResult = objLeaveInformationBLL.UpdateLeaveEntry(objLeaveInformation);
            return new JsonResult { Data = new { objCommonResult.Status, objCommonResult.Message } };
        }
        public ActionResult GetLeaveType()
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var LeaveType = objLeaveInformationBLL.GetLeaveType();
            return Json(LeaveType, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AutoCompleteEmployeeCode(string term)
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var result = objLeaveInformationBLL.GetEmployeeCodeForAutoComplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmployeeNameByEmpCode(string EmpCode)
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var result = objLeaveInformationBLL.GetEmployeeNameByEmpCode(EmpCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllLeaveInfo()
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            List<LeaveInformation> LeaveInformation = objLeaveInformationBLL.GetAllLeaveInfoList();
            return Json(new { data = LeaveInformation }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetHolidayInformation(string fromDate, string toDate)
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var HolidayInformation = objLeaveInformationBLL.GetHolidayInformation(fromDate, toDate);
            return Json(HolidayInformation, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LeaveApproval()
        {
            List<LeaveInformation> LeaveInformation = objLeaveInformationBLL.GetAllLeaveInfo();
            return View(LeaveInformation);
        }
        [HttpPost]

        public ActionResult LeaveApproval(FormCollection form)
        {
            var ch = form.GetValues("Id");
            objLeaveInformationBLL.LeaveApproval(ch);
            return RedirectToAction("LeaveApproval", "Leave");
        }
        [HttpGet]
        public ActionResult Detail(string EmpCode)
        {
            if (EmpCode == null) return null;
            var LeaveInformation = objLeaveInformationBLL.GetAllLeaveInfoDetailsByEmpCode(EmpCode);
            return View(LeaveInformation);
        }
        [HttpPost]
        public ActionResult Detail(LeaveInformation objLeaveInformation)
        {
            var LeaveInformation = objLeaveInformationBLL.ApprovedLeaveInfoDetailsByEmpCode(objLeaveInformation);
            return RedirectToAction("LeaveEntry", "Leave");
        }
        #endregion
    }
}