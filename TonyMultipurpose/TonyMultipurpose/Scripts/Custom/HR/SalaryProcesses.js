﻿
$(document).ready(function () {

    var oTable;
    EmployeeSalaryProcessTable();
    function EmployeeSalaryProcessTable() {
        oTable = $('#salaryProcessTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/GetAllSalaryProcessInfo",//'@Url.Action("GetAllSalaryProcessInfo", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                //{ "data": "EmployeeName" },
                { "data": "EmployeeName" },
                {
                    "data": "SalaryMonth",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "DlBookNumber" },
                { "data": "BasicSalary" },
                { "data": "DA" },
                { "data": "HRA" },
                { "data": "TeleBillAndConveyance" },
                { "data": "Gross" },
                { "data": "PF" },
                { "data": "EFund" },
                { "data": "TotalDeduction" },
                { "data": "NetSalary" }
            ],
            "order": [[0, "desc"]],
        });
        $('#salaryProcessTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }

        });
    }



    $('#btnclear').click(function () {
        window.location.reload();
    });


    //Save salary processes
    $('#btnProcess').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                ProcessDate: $('#ProcessDate').val()
            };

            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveSalaryProcesses",//'@Url.Action("SaveSalaryProcesses", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";

                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    function CheckValidation() {
        var isValid = true;
        if ($('#ProcessDate').val().trim() === '') {
            isValid = false;
            $('#ProcessDate').parent().prev().find('span').css('visibility', 'visible');
        } else {
            $('#ProcessDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;

    }

    //Process Date change activities
    //GetProcessDateWiseSalaryProcessInfo

    $("#ProcessDate").change(function () {
        //$('#EmployeeName').empty();
        var ProcessDate = $("#ProcessDate").val();
        var json = { ProcessDate: ProcessDate };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetProcessDateWiseSalaryProcessInfo",//'@Url.Action("GetProcessDateWiseSalaryProcessInfo", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                console.log(data);
                //EmployeeSalaryProcessTable(data);
                $('#salaryProcessTable').dataTable().fnDestroy();

                var dTable;
                SalaryProcessTable(data);
                function SalaryProcessTable() {
                    dTable = $('#salaryProcessTable').DataTable({
                        "columns": [
                            //{ "data": "EmployeeName" },
                            { "data": "EmployeeName" },
                            {
                                "data": "SalaryMonth",
                                "render": function (data) {
                                    var date = new Date(data);
                                    var month = date.getMonth() + 1;
                                    var mon = monthNumToName(month);
                                    return date.getDate() + "/" + mon + "/" + date.getFullYear();
                                }
                            },
                            { "data": "DlBookNumber" },
                            { "data": "BasicSalary" },
                            { "data": "DA" },
                            { "data": "HRA" },
                            { "data": "TeleBillAndConveyance" },
                            { "data": "Gross" },
                            { "data": "PF" },
                            { "data": "EFund" },
                            { "data": "TotalDeduction" },
                            { "data": "NetSalary" }
                        ],
                        //"order": [[0, "desc"]],
                    });

                }







            }
        });



    });




    //for date picker
    function FormateDate(jsonDate) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#ProcessDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });



    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

});





