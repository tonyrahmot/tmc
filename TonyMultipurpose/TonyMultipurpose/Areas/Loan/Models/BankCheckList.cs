﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class BankCheckList
    {
        [Key]
        public int BankCheckListId { get; set; }
        public string LoanId { get; set; }
        public string ChequeId { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public int BankId { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public HttpPostedFileBase Imagefile { get; set; }

    }
}