﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TonyMultipurpose.Enums;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsTextBox
    {
        public static MvcHtmlString BootstrapTextBoxFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          object htmlAttributes = null
          )
        {
            return BootstrapTextBoxFor(htmlHelper,
              expression, Html5InputTypes.Text, string.Empty, string.Empty, false, false,
              string.Empty, htmlAttributes);
        }
        public static MvcHtmlString BootstrapTextBoxFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          Html5InputTypes type,
          object htmlAttributes = null
          )
        {
            return BootstrapTextBoxFor(htmlHelper,
              expression, type, string.Empty, string.Empty, false, false,
              string.Empty, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextBoxFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          Html5InputTypes type,
          string cssClass,
          object htmlAttributes = null
          )
        {
            return BootstrapTextBoxFor(htmlHelper,
              expression, type, string.Empty, string.Empty, false, false,
              cssClass, htmlAttributes);
        }
        public static MvcHtmlString BootstrapTextBoxFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         Html5InputTypes type,
         string title,
         string placeholder,
         bool isRequired,
         bool isAutoFocus,
         object htmlAttributes = null
         )
        {
            return BootstrapTextBoxFor(htmlHelper,
              expression, type, title, placeholder, isRequired, isAutoFocus,
              string.Empty, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextBoxFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          Html5InputTypes type,
          string title,
          string placeholder,
          bool isRequired,
          bool isAutoFocus,
          string cssClass,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes)) { { "type", type.ToString().ToLower() } };


            // Add all other attributes below here
            if (!string.IsNullOrWhiteSpace(title))
            {
                rvd.Add("title", title);
            }
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                rvd.Add("placeholder", placeholder);
            }
            if (isRequired)
            {
                rvd.Add("required", "required");
            }
            if (isAutoFocus)
            {
                rvd.Add("autofocus", "autofocus");
            }
            if (string.IsNullOrWhiteSpace(cssClass))
            {
                cssClass = "form-control input-sm";
            }
            else
            {
                cssClass = "form-control input-sm " + cssClass;
            }
            rvd.Add("class", cssClass);

            return htmlHelper.TextBoxFor(expression, rvd);
        }
    }
}