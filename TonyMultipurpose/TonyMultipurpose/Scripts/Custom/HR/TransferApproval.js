﻿(function () {
    var oTable;
    $(document).ready(function () {
        transferRecordTable();
    });

    function transferRecordTable() {
        oTable = $('#TransferRecordTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/TransferUnApprovaList",//'@Url.Action("TransferUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpId" },
                { "data": "EmployeeName" },
                { "data": "FromBranch" },
                { "data": "ToBranch" },
                { "data": "Reason" },
                {
                    "data": "TransferDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                {
                    "data": "EmpId",
                    "render": function (data) {
                        return '<a class="btn btn-primary" href="' + window.applicationBaseUrl + 'HR/HR/TransferRecordApproval?id=' + data + '">Approve</a>';
                    }
                }
            ],
            "order": [[0, "desc"]]
        });
    }

    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }
})();