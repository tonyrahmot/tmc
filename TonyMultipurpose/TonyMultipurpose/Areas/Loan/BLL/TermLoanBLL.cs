﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Loan.BLL
{
    public class TermLoanBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        /// <summary>
        /// Install Information start
        /// </summary>
        /// <returns></returns>
        //public List<InstallmentInformation> GetInstallmentInformation()
        //{
        //    objDataAccess = DataAccess.NewDataAccess();
        //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
        //    DbDataReader objDbDataReader = null;
        //    List<InstallmentInformation> infoList = new List<InstallmentInformation>();
        //    InstallmentInformation objI;

        //    try
        //    {
        //        objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "uspGetTermLoanInstallmentInformation", CommandType.StoredProcedure);

        //        if (objDbDataReader.HasRows)
        //        {

        //            while (objDbDataReader.Read())
        //            {
        //                objI = new InstallmentInformation();
        //                objI.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
        //                objI.LoanId = objDbDataReader["LoanId"].ToString();
        //                objI.InstallmentDate = objDbDataReader["InstallmentDate"].ToString();
        //                objI.Particular = objDbDataReader["Particular"].ToString();
        //                objI.Outstanding = Convert.ToDecimal(objDbDataReader["Outstanding"].ToString());
        //                objI.InstallmentAmount = Convert.ToDecimal(objDbDataReader["InstallmentAmount"].ToString());
        //                objI.InterestAmount = Convert.ToDecimal(objDbDataReader["InterestAmount"].ToString());
        //                objI.PrincipleAmount = Convert.ToDecimal(objDbDataReader["PrincipleAmount"].ToString());

        //                infoList.Add(objI);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error : " + ex.Message);
        //    }
        //    finally
        //    {
        //        if (objDbDataReader != null)
        //        {
        //            objDbDataReader.Close();
        //        }
        //        objDataAccess.Dispose(objDbCommand);
        //    }
        //    return infoList;
        //}
        public bool SaveInstallmentInformation(InstallmentInformation objInfo)
        {
            bool isSucessful = false;
            int rowAffected = 0;
            objInfo.TransactionId = GenerateTransactionId();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanNo", objInfo.LoanNo);
            objDbCommand.AddInParameter("InstallmentDate", objInfo.InstallmentDate);
            objDbCommand.AddInParameter("InstallmentNo", objInfo.InstallmentNo);
            objDbCommand.AddInParameter("Particular", objInfo.Particular);
            objDbCommand.AddInParameter("InstallmentAmount", objInfo.InstallmentAmount);
            objDbCommand.AddInParameter("TotalAmount", objInfo.TotalAmount);
            objDbCommand.AddInParameter("BeginningBalance", objInfo.BeginningBalance);
            objDbCommand.AddInParameter("EndingBalance", objInfo.EndingBalance);
            objDbCommand.AddInParameter("Principle", objInfo.Principle);
            objDbCommand.AddInParameter("Interest", objInfo.Interest);
            objDbCommand.AddInParameter("CompanyId", objInfo.CompanyId);
            objDbCommand.AddInParameter("BranchId", objInfo.BranchId);
            objDbCommand.AddInParameter("CreatedBy", objInfo.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objInfo.CreatedDate);
            objDbCommand.AddInParameter("IsApproved", objInfo.IsApproved);
            objDbCommand.AddInParameter("IsRejected", objInfo.IsRejected);
            objDbCommand.AddInParameter("TransactionId", objInfo.TransactionId);
            //objDbCommand.AddInParameter("FineAmount", objInfo.FineAmount);
            objDbCommand.AddInParameter("IsChequeEntry", objInfo.IsChequeEntry);
            objDbCommand.AddInParameter("BankName", objInfo.BankName);
            objDbCommand.AddInParameter("BankAccountNo", objInfo.BankAccountNo);
            objDbCommand.AddInParameter("ChequeNo", objInfo.ChequeNo);
            objDbCommand.AddInParameter("IsEntry", objInfo.IsEntry);
            objDbCommand.AddInParameter("COAId", objInfo.COAId);
            objDbCommand.AddInParameter("FineInterest", objInfo.FineInterest);
            objDbCommand.AddInParameter("TotalFineInterest", objInfo.TotalFineInterest);
            objDbCommand.AddInParameter("UnpaidPrinciple", objInfo.UnpaidPrinciple);
            objDbCommand.AddInParameter("TotalUnpaidPrinciple", objInfo.TotalUnpaidPrinciple);
            objDbCommand.AddInParameter("ExtraFee", objInfo.ExtraFee);
            objDbCommand.AddInParameter("ExtraPayment", objInfo.ExtraPayment);
            objDbCommand.AddInParameter("VoucherNo", objInfo.VoucherNo);

            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveTermLoanInstallmentInformation]",
                   CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    isSucessful = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    isSucessful = false;
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return isSucessful;
        }

        public bool SaveTermLoanDuePayment(InstallmentInformation objInfo)
        {
            bool isSucessful = false;
            int rowAffected = 0;
            objInfo.TransactionId = GenerateTransactionId();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanNo", objInfo.LoanNo);
            objDbCommand.AddInParameter("TransactionDate", objInfo.InstallmentDate);
            objDbCommand.AddInParameter("Particular", objInfo.Particular);
            objDbCommand.AddInParameter("TotalAmount", objInfo.TotalAmount);
            objDbCommand.AddInParameter("CompanyId", objInfo.CompanyId);
            objDbCommand.AddInParameter("BranchId", objInfo.BranchId);
            objDbCommand.AddInParameter("CreatedBy", objInfo.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objInfo.CreatedDate);
            objDbCommand.AddInParameter("IsApproved", objInfo.IsApproved);
            objDbCommand.AddInParameter("IsRejected", objInfo.IsRejected);
            objDbCommand.AddInParameter("TransactionId", objInfo.TransactionId);
            objDbCommand.AddInParameter("IsChequeEntry", objInfo.IsChequeEntry);
            objDbCommand.AddInParameter("BankName", objInfo.BankName);
            objDbCommand.AddInParameter("BankAccountNo", objInfo.BankAccountNo);
            objDbCommand.AddInParameter("ChequeNo", objInfo.ChequeNo);
            objDbCommand.AddInParameter("COAId", objInfo.COAId);
            objDbCommand.AddInParameter("VoucherNo", objInfo.VoucherNo);

            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_SaveTermLoanDuePayment]",
                   CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    isSucessful = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    isSucessful = false;
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return isSucessful;
        }

        private string GenerateTransactionId()
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            //if (mbModel.AccountNumber != null)
            //{

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            //var prefix = string.Concat(year + days);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "Savings");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();
            //}
            return transactionId;
        }

        public List<string> GetAllTermLoanIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("LoanId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTermLoanIdForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanNo"] != null)
                        {
                            objList.Add(objDbDataReader["LoanNo"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objList;
        }

        public List<InstallmentInformation> GetTermLoanInfoByLoanId()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentInformation> infoList = new List<InstallmentInformation>();
            InstallmentInformation objI = new InstallmentInformation();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetTermLoanInfoByLoanId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new InstallmentInformation();

                        objI.Id = objDbDataReader["Id"].ToString() == "" ? 0 : (int)objDbDataReader["Id"];
                        objI.IsCleared = Convert.ToBoolean(objDbDataReader["IsCleared"]);
                        objI.LoanNo = Convert.ToString(objDbDataReader["LoanNo"]);
                        objI.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objI.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objI.FatherName = objDbDataReader["FatherName"].ToString();
                        objI.Particular = objDbDataReader["Particular"].ToString();
                        objI.InstallmentDate = objDbDataReader["InstallmentDate"].ToString();
                        objI.BeginningBalance = Convert.ToDecimal(objDbDataReader["BeginningBalance"].ToString());
                      //  objI.InstallmentAmount = Convert.ToDecimal(objDbDataReader["InstallmentAmount"].ToString());
                        objI.InstallmentAmount = objDbDataReader["InstallmentAmount"].ToString() == "" ? 0 : (decimal)objDbDataReader["InstallmentAmount"];
                        objI.PrincipleAmount = Convert.ToDecimal(objDbDataReader["LoanAmount"].ToString());
                        objI.ExtraFee = Convert.ToDecimal(objDbDataReader["ExtraFee"].ToString());
                        objI.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objI.EndingBalance = Convert.ToDecimal(objDbDataReader["EndingBalance"].ToString());
                        objI.Principle = Convert.ToDecimal(objDbDataReader["Principle"].ToString());
                        objI.Interest = Convert.ToDecimal(objDbDataReader["Interest"].ToString());
                        objI.FineInterest = Convert.ToDecimal(objDbDataReader["FineInterest"].ToString());
                        objI.TotalFineInterest = Convert.ToDecimal(objDbDataReader["TotalFineInterest"].ToString());
                        objI.UnpaidPrinciple = Convert.ToDecimal(objDbDataReader["UnpaidPrinciple"].ToString());
                        objI.TotalUnpaidPrinciple = Convert.ToDecimal(objDbDataReader["TotalUnpaidPrinciple"].ToString());
                      //  objI.NumberOfInstallment = Convert.ToByte(objDbDataReader["NumberOfInstallment"].ToString());
                        objI.NumberOfInstallment = Convert.ToByte(objDbDataReader["NumberOfInstallment"].ToString() == "" ? 0 : (byte)objDbDataReader["NumberOfInstallment"]);
                        objI.InstallmentTypeId = Convert.ToByte(objDbDataReader["InstallmentTypeId"].ToString() == "" ? 0 : (byte)objDbDataReader["InstallmentTypeId"]);
                        //objI.FineAmount = Convert.ToDecimal(objDbDataReader["FineAmount"].ToString());
                        //objI.IsPartial = Convert.ToBoolean(objDbDataReader["IsPartial"]);
                        //objI.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"]);
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objI.CustomerImage = imagePath;
                        }
                        else
                        {
                            objI.CustomerImage = "\\Not Found\\";
                        }
                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)

            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return infoList;
        }

        public InstallmentInformation GetInstallmentNoAndDateById(string LoanNo)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentInformation> infoList = new List<InstallmentInformation>();
            InstallmentInformation objI = new InstallmentInformation();
            try
            {
                objDbCommand.AddInParameter("LoanNo", LoanNo);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetInstallmentNoAndDateById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new InstallmentInformation();

                        //objI.Id = objDbDataReader["Id"].ToString() == "" ? 0 : (int)objDbDataReader["Id"];

                        objI.InstallmentNo = Convert.ToInt32(Convert.ToString(objDbDataReader["InstallmentNo"]));
                        objI.InstallmentDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(objDbDataReader["LoanInstallmentDate"]));

                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)

            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objI;
        }


        public List<InstallmentInformation> GetDueInstallment()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentInformation> infoList = new List<InstallmentInformation>();
            InstallmentInformation objI = new InstallmentInformation();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetDueInstallment]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new InstallmentInformation();
                        objI.Id = objDbDataReader["Id"].ToString() == "" ? 0 : (int)objDbDataReader["Id"];
                        objI.LoanId = objDbDataReader["LoanId"].ToString();
                        objI.DueInstallmentAmount = Convert.ToDecimal(objDbDataReader["DueInstallmentAmount"].ToString());

                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)

            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return infoList;
        }
        // start installinformatin approval part

        public List<InstallmentInformation> GetInstallmentInformation(InstallmentInformation objInstallmentInformation)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentInformation> objInstallmentInformationList = new List<InstallmentInformation>();
            InstallmentInformation objI;
            try
            {
                //objDbCommand.AddInParameter("BranchId", objInstallmentInformation.BranchId);
                objDbCommand.AddInParameter("IsApproved", objInstallmentInformation.IsApproved);
                objDbCommand.AddInParameter("CompanyId", objInstallmentInformation.CompanyId);
                objDbCommand.AddInParameter("IsRejected", objInstallmentInformation.IsRejected);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetInstallmentInformation]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new InstallmentInformation();
                        objI.Id = objDbDataReader["Id"].ToString() == "" ? 0 : (int)objDbDataReader["Id"];
                        objI.LoanNo = Convert.ToString(objDbDataReader["LoanNo"]);
                        objI.VoucherNo = Convert.ToString(objDbDataReader["VoucherNo"]);
                        objI.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objI.CustomerName = Convert.ToString(objDbDataReader["CustomerName"]);
                        objI.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objI.FatherName = objDbDataReader["FatherName"].ToString();
                        objI.Particular = objDbDataReader["Particular"].ToString();
                        objI.InstallmentDate = objDbDataReader["InstallmentDate"].ToString();                       
                        objI.InstallmentAmount = Convert.ToDecimal(objDbDataReader["InstallmentAmount"].ToString());
                        objI.InstallmentNo = Convert.ToByte(objDbDataReader["InstallmentNo"].ToString());
                        objI.PrincipleAmount = Convert.ToDecimal(objDbDataReader["LoanAmount"].ToString());
                        objI.ExtraFee = Convert.ToDecimal(objDbDataReader["ExtraFee"].ToString());
                        objI.BeginningBalance = Convert.ToDecimal(objDbDataReader["BeginningBalance"].ToString());
                        objI.EndingBalance = Convert.ToDecimal(objDbDataReader["EndingBalance"].ToString());
                        objI.TotalAmount = Convert.ToDecimal(objDbDataReader["TotalAmount"].ToString());
                        objI.Principle = Convert.ToDecimal(objDbDataReader["Principle"].ToString());
                        objI.Interest = Convert.ToDecimal(objDbDataReader["Interest"].ToString());
                        objI.UnpaidPrinciple = Convert.ToDecimal(objDbDataReader["UnpaidPrinciple"].ToString());
                        objI.TotalUnpaidPrinciple = Convert.ToDecimal(objDbDataReader["TotalUnpaidPrinciple"].ToString());
                        objI.FineInterest = Convert.ToDecimal(objDbDataReader["FineInterest"].ToString());
                        objI.TotalFineInterest = Convert.ToDecimal(objDbDataReader["TotalFineInterest"].ToString());
                        objI.ExtraPayment = Convert.ToDecimal(objDbDataReader["ExtraPayment"].ToString());

                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objI.CustomerImage = imagePath;
                        }
                        else
                        {
                            objI.CustomerImage = "\\Not Found\\";
                        }
                        objI.Username = (objDbDataReader["Username"].ToString());
                        objInstallmentInformationList.Add(objI);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objInstallmentInformationList;
        }

        public string ApproveInstallment(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                objDbCommand.AddInParameter("Id", id);

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].loan_ApproveInstallment", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";

        }

        //reject  transaction
        public string RejectInstallment(List<InstallmentInformation> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("Id", item.Id);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_RejectInstallment]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

    }
}