$(function () {
    document.getElementById('showMessage').innerHTML = '';
    $("#GResidentialCondition").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/Loan/GetResidentialConditionInfo",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#GResidentialCondition").append('<option value="0">--Select Type--</option>');
            $.each(data, function (key, value) {
                $("#GResidentialCondition").append('<option value=' + value.ResidentialConditionId + '>' + value.ResidentialConditionType + '</option>');
            });
        }
    });
})();

$(function () {

    $('#Guarantor_DateofBirth').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });
    alert(1);

    $('#UpdateGuarantor').hide();
    $('#deleteGuarantor').hide();
    // Load Function function
    var oGuarantorTable;
    LoadGuarantorTable();
    function LoadGuarantorTable() {
        oGuarantorTable = $('#GuarantorTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetAllGurantorInfo",
                "type": "get",
                "data": {
                    "LoanId": $('#Guarantor_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "GuarantorRoll" },
                { "data": "GuarantorName" },
                //{ "data": "NID" },
                //{ "data": "FatherName" },
                //{ "data": "MotherName" },
                //{ "data": "Occupation" },
                //{ "data": "Designation" },
                //{ "data": "InstituteName" },
                //{ "data": "InstituteAddress" },
                //{ "data": "CurrentAddress" },
                //{ "data": "PermanentAddress" },
                { "data": "ContactNo" },
            ],
            "order": [[0, "asc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#GuarantorTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oGuarantorTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataGuarantor(oGuarantorTable.row(this).data());
        });
    }
    function bindDataGuarantor(data) {
        $('#Guarantor_GuarantorRoll').attr('disabled', 'disabled');
        var num = Math.random();
        var imagePath = data.ImagePath + "?v=" + num;
        $('#img-upload').attr('src', imagePath);
        $('#hdfImagePath').val(data.OldImagePath);
        $('#hdfGuarantorId').val(data.GuarantorId);
        $('#Guarantor_GuarantorName').val(data.GuarantorName);
        $('#Guarantor_NID').val(data.NID);
        $('#Guarantor_FatherName').val(data.FatherName);
        $('#Guarantor_MotherName').val(data.MotherName);
        $('#Guarantor_Occupation').val(data.Occupation);
        $('#Guarantor_Designation').val(data.Designation);
        $('#Guarantor_InstituteName').val(data.InstituteName);
        $('#Guarantor_InstituteAddress').val(data.InstituteAddress);
        $('#Guarantor_CurrentAddress').val(data.CurrentAddress);
        $('#Guarantor_PermanentAddress').val(data.PermanentAddress);
        $('#Guarantor_ContactNo').val(data.ContactNo);
        $('#Guarantor_GuarantorRoll').val(data.GuarantorRoll);
        $('#Guarantor_SpouseName').val(data.SpouseName);
        $('#Guarantor_RelationWithApplicant').val(data.RelationWithApplicant);
        $('#Guarantor_DateofBirth').val(data.DateofBirth);
        $('#Guarantor_MonthlyIncome').val(data.MonthlyIncome);
        $('#Guarantor_BusinessAddress').val(data.BusinessAddress);
        $('#Guarantor_LoanStatusWithTmc').val(data.LoanStatusWithTmc);
        //$('#GResidentialCondition').val(data.ResidentialConditionId);
        if (data.ResidentialConditionId != null) {
            $('#GResidentialCondition').val(data.ResidentialConditionId);
        }
        $('#saveGuarantor').hide();
        $('#UpdateGuarantor').show();
        $('#deleteGuarantor').show();
    }
    // caler function
    $('#Guarantor_Clear').click(function () {
        GuarantorClearAll();
    });
    function GuarantorClearAll() {
        //$('#Guarantor_LoanId').val('');
        $('#img-upload').attr('src', '');
        $('#Guarantor_GuarantorName').val('');
        $('#Guarantor_NID').val('');
        $('#Guarantor_FatherName').val('');
        $('#Guarantor_MotherName').val('');
        $('#Guarantor_Occupation').val('');
        $('#Guarantor_Designation').val('');
        $('#Guarantor_InstituteName').val('');
        $('#Guarantor_InstituteAddress').val('');
        $('#Guarantor_CurrentAddress').val('');
        $('#Guarantor_PermanentAddress').val('');
        $('#Guarantor_ContactNo').val('');
        $('#Guarantor_GuarantorRoll').val('0');
        $('#Guarantor_SpouseName').val('');
        $('#Guarantor_RelationWithApplicant').val('');
        $('#Guarantor_DateofBirth').val('');
        $('#Guarantor_MonthlyIncome').val('');
        $('#Guarantor_BusinessAddress').val('');
        $('#Guarantor_LoanStatusWithTmc').val('');
        $('#GResidentialCondition').val(0);
        $('#saveGuarantor').show();
        $('#deleteGuarantor').hide();
        $('#UpdateGuarantor').hide();
        $('#Guarantor_GuarantorRoll').removeAttr('disabled', 'disabled');
        document.getElementById('showMessage').innerHTML = '';
    }
    function CheckGuarantorRoll() {
        var LoanId = $('#Guarantor_LoanId').val();
        var json = {
            LoanId: LoanId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetGuarantorRoll",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                console.log(data);
                var gRoll = $('#Guarantor_GuarantorRoll').val();
                if (gRoll == data[0] || gRoll == data[1] || gRoll == data[2]) {
                    alert('There is already a guarantor at ' + gRoll + ' position');
                    return;
                } else {
                    saveGuarantor();
                }
            }
        });
    }

    function saveGuarantor() {
        var isAllDataValid = CheckValidation();

        if (isAllDataValid) {
            var data = bindData();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/Guarantor",
                type: "POST",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oGuarantorTable.ajax.reload();
                        $('#saveGuarantor').show();
                        $('#deleteGuarantor').hide();
                        $('#UpdateGuarantor').hide();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    }

    function bindData() {
        var fd = new FormData();
        fd.append('LoanId', $('#Guarantor_LoanId').val());
        fd.append('GuarantorName', $('#Guarantor_GuarantorName').val());
        fd.append('NID', $('#Guarantor_NID').val());
        fd.append('FatherName', $('#Guarantor_FatherName').val());
        fd.append('MotherName', $('#Guarantor_MotherName').val());
        fd.append('Occupation', $('#Guarantor_Occupation').val());
        fd.append('Designation', $('#Guarantor_Designation').val());
        fd.append('InstituteName', $('#Guarantor_InstituteName').val());
        fd.append('InstituteAddress', $('#Guarantor_InstituteAddress').val());
        fd.append('CurrentAddress', $('#Guarantor_CurrentAddress').val());
        fd.append('PermanentAddress', $('#Guarantor_PermanentAddress').val());
        fd.append('ContactNo', $('#Guarantor_ContactNo').val());
        fd.append('GuarantorRoll', $('#Guarantor_GuarantorRoll').val());
        fd.append('SpouseName', $('#Guarantor_SpouseName').val());
        fd.append('RelationWithApplicant', $('#Guarantor_RelationWithApplicant').val());
        fd.append('DateofBirth', $('#Guarantor_DateofBirth').val());
        fd.append('MonthlyIncome', $('#Guarantor_MonthlyIncome').val());
        fd.append('BusinessAddress', $('#Guarantor_BusinessAddress').val());
        fd.append('LoanStatusWithTmc', $('#Guarantor_LoanStatusWithTmc').val());
        fd.append('ResidentialConditionId', $('#GResidentialCondition option:selected').val());
        fd.append('ImageFile', document.getElementById('ImageFile').files[0]);
        return fd;
    }

    // Save function
    $('#saveGuarantor').click(function () {

        if ($("#GuarantorTable > tbody > tr").length === 3) {
            alert('Maximun Three Guaranror can be added');
            return;
        }
        CheckGuarantorRoll();

    });
    $('#UpdateGuarantor').click(function () {

        var isAllDataValid = CheckValidation();

        if (isAllDataValid) {
            var data = updateBindData();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/UpdateGuarantor",
                type: "POST",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oGuarantorTable.ajax.reload();
                        GuarantorClearAll();
                        $('#saveGuarantor').show();
                        $('#Guarantor_Clear').show();
                        $('#deleteGuarantor').hide();
                        $('#UpdateGuarantor').hide();

                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });


    function updateBindData() {
        var fd = new FormData();
        var file = document.getElementById('ImageFile').files[0];
        if (file != null) {
            fd.append('ImageFile', document.getElementById('ImageFile').files[0]);
        } else {
            fd.append('ImagePath', $('#hdfImagePath').val());
        }
        fd.append('GuarantorId', $('#hdfGuarantorId').val());
        fd.append('LoanId', $('#Guarantor_LoanId').val());
        fd.append('GuarantorName', $('#Guarantor_GuarantorName').val());
        fd.append('NID', $('#Guarantor_NID').val());
        fd.append('FatherName', $('#Guarantor_FatherName').val());
        fd.append('MotherName', $('#Guarantor_MotherName').val());
        fd.append('Occupation', $('#Guarantor_Occupation').val());
        fd.append('Designation', $('#Guarantor_Designation').val());
        fd.append('InstituteName', $('#Guarantor_InstituteName').val());
        fd.append('InstituteAddress', $('#Guarantor_InstituteAddress').val());
        fd.append('CurrentAddress', $('#Guarantor_CurrentAddress').val());
        fd.append('PermanentAddress', $('#Guarantor_PermanentAddress').val());
        fd.append('ContactNo', $('#Guarantor_ContactNo').val());
        fd.append('GuarantorRoll', $('#Guarantor_GuarantorRoll').val());
        fd.append('SpouseName', $('#Guarantor_SpouseName').val());
        fd.append('RelationWithApplicant', $('#Guarantor_RelationWithApplicant').val());
        fd.append('DateofBirth', $('#Guarantor_DateofBirth').val());
        fd.append('MonthlyIncome', $('#Guarantor_MonthlyIncome').val());
        fd.append('BusinessAddress', $('#Guarantor_BusinessAddress').val());
        fd.append('LoanStatusWithTmc', $('#Guarantor_LoanStatusWithTmc').val());
        fd.append('ResidentialConditionId', $('#GResidentialCondition option:selected').val());

        return fd;
    }

    $('#deleteGuarantor').click(function () {
        if (confirm("Are you sure?")) {
            var data = {
                LoanId: $('#Guarantor_LoanId').val(),
                GuarantorId: $('#hdfGuarantorId').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/DeleteGuaranorInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oGuarantorTable.ajax.reload();
                        GuarantorClearAll();
                        $('#saveGuarantor').show();
                        $('#Guarantor_Clear').show();
                        $('#deleteGuarantor').hide();
                        $('#UpdateGuarantor').hide();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
        return false;
    });


    function CheckValidation() {
        var isValid = true;
        if ($('#Guarantor_GuarantorRoll').val().trim() === '0') {
            isValid = false;
            $('#Guarantor_GuarantorRoll').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Guarantor_GuarantorRoll').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#Guarantor_MonthlyIncome').val().trim() === '') {
            isValid = false;
            $('#Guarantor_MonthlyIncome').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Guarantor_MonthlyIncome').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#ImageFile").change(function () {
        readURL(this);
    });




});

function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}