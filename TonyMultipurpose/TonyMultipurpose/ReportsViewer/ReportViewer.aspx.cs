﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System;
using System.Data;
using System.IO;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.BLL;
using TonyMultipurpose.DAL;


namespace TonyMultipurpose.ReportsViewer
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        readonly ReportsBLL objReportsBLL = new ReportsBLL();
        private DataTable dtCommonDataTable;
        private string reportPath = string.Empty;
        string reportName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //            ShowCrystalReport();
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowCrystalReport();
            }

            if (Session["ReportDocument"] != null)
            {
                ReportDocument document = (ReportDocument)Session["ReportDocument"];
                crpViewer.ReportSource = document;
                crpViewer.DisplayToolbar = true;
                crpViewer.ToolPanelView = ToolPanelViewType.None;
                crpViewer.Zoom(125);
                crpViewer.ID = Session["ReportName"].ToString();
                crpViewer.RefreshReport();
            }

        }

        private void ShowCrystalReport()
        {
            var document = new ReportDocument();
            string strDate = string.Empty;
            string companyName = string.Empty;
            string strBranch = string.Empty;
            if (ReportObject.ReportName != null)
            {
                Session["ReportName"] = ReportObject.ReportName;

                switch (ReportObject.ReportName)
                {

                    #region TestReport
                    case "TestReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetTestReport();
                        //dtCommonDataTable.TableName = "TestReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/XMLFile.xml");
                        reportPath = "~/Rpt/testReport.rpt";
                        break;
                    #endregion

                    #region CustomerEntryReport
                    case "CustomerEntryReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.CustomerEntryReport();
                        //dtCommonDataTable.TableName = "CustomerEntryReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/CustomerEntryReportXMLFile.xml");
                        reportPath = "~/Rpt/CustomerEntryReport.rpt";
                        break;
                    #endregion

                    #region AccountNumberWiseReport
                    case "AccountNumberWiseReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.AccountNumberWiseReport();
                        //dtCommonDataTable.TableName = "AccountNumberWiseReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/AccountNumberWiseReport.xml");
                        strDate = "'From: " +
                                  Convert.ToDateTime(
                                          ReportObject.FromDate)
                                      .ToString("dd/MMM/yyyy")
                                  +
                                  " To: " +
                                  Convert.ToDateTime(
                                          ReportObject.ToDate)
                                      .ToString("dd/MMM/yyyy") + "'";
                        reportPath = "~/Rpt/AccountNumberWiseReport.rpt";
                        break;
                    #endregion

                    #region DepositStatement
                    case "DepositStatement":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDepositStatementReport();
                        BranchBLL objBranchBll = new BranchBLL();
                        //dtCommonDataTable.TableName = "DepositStatement";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/XMLFile.xml");
                        strBranch = "'Branch: " + (string.IsNullOrEmpty(ReportObject.BranchId)
                                        ? "All Branches"
                                        : objBranchBll.GetBranchName(int.Parse(ReportObject.BranchId))) + "'";
                        strDate = "'From: " +
                                  Convert.ToDateTime(
                                      ReportObject.FromDate)
                                      .ToString("dd-MMM-yyyy")
                                            +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        if (ReportObject.ReportFormat == "PDF")
                        {
                            reportPath = "~/RptPDF/DepositStatementReport.rpt";
                        }
                        else if (ReportObject.ReportFormat == "Excel")
                        {
                            reportPath = "~/Rpt/DepositStatementReport.rpt";
                        }
                        else if (ReportObject.ReportFormat == "CSV")
                        {
                            reportPath = "~/Rpt/DepositStatementReport.rpt";
                        }
                        break;
                    #endregion

                    #region CustomerwiseTransactionReport
                    case "CustomerwiseTransactionReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetCustomerwiseTransactionReport();
                        //dtCommonDataTable.TableName = "CustomerwiseTransactionReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/CustomerwiseTransactionReport.xml");
                        reportPath = "~/Rpt/CustomerwiseTransactionReport.rpt";
                        break;
                    #endregion

                    #region DateWiseAllTransactionReport
                    case "DateWiseAllTransactionReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDateWiseAllTransactionReport();
                        //dtCommonDataTable.TableName = "DateWiseAllTransactionReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DateWiseAllTransactionReport.xml");
                        strDate = "'From: " +
                                  Convert.ToDateTime(
                                      ReportObject.FromDate)
                                      .ToString("dd-MMM-yyyy")
                                            +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/DateWiseAllTransactionReport.rpt";
                        break;
                    #endregion

                    #region DateWiseWithdrawReport
                    case "DateWiseWithdrawReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDateWiseWithdrawReportReport();
                        //dtCommonDataTable.TableName = "DateWiseWithdrawReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DateWiseWithdrawReport.xml");
                        strDate = "'From: " +
                                  Convert.ToDateTime(
                                      ReportObject.FromDate)
                                      .ToString("dd-MMM-yyyy")
                                            +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/DateWiseWithdrawReport.rpt";
                        break;
                    #endregion

                    #region DateWiseDepositReport
                    case "DateWiseDepositReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDateWiseDepositReport();
                        //dtCommonDataTable.TableName = "DateWiseDepositReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DateWiseDepositReport.xml");
                        strDate = "'From: " +
                                  Convert.ToDateTime(
                                      ReportObject.FromDate)
                                      .ToString("dd-MMM-yyyy")
                                            +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/DateWiseDepositReport.rpt";
                        break;
                    #endregion

                    #region IrregularInstallmentReport
                    case "IrregularInstallmentReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetIrregularInstallmentReport();
                        //dtCommonDataTable.TableName = "IrregularInstallmentReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/IrregularInstallmentReport.xml");
                        reportPath = "~/Rpt/IrregularInstallmentReport.rpt";
                        break;
                    #endregion

                    #region NextMonthMaturedAccountReport
                    case "NextMonthMaturedAccountReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetNextMonthMaturedAccountReport();
                        //dtCommonDataTable.TableName = "NextMonthMaturedAccountReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/NextMonthMaturedAccountReport.xml");
                        reportPath = "~/Rpt/NextMonthMaturedAccountReport.rpt";
                        break;
                    #endregion

                    #region AccountOpeningReport
                    case "AccountOpeningReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetAccountOpeningReport();
                        //dtCommonDataTable.TableName = "AccountOpeningReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/AccountOpeningReport.xml");
                        reportPath = "~/Rpt/AccountOpeningReport.rpt";
                        break;
                    #endregion

                    #region AccountClosingReport
                    case "AccountClosingReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetAccountClosingReport();
                        //dtCommonDataTable.TableName = "AccountClosingReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/AccountClosingReport.xml");
                        reportPath = "~/Rpt/AccountClosingReport.rpt";
                        break;
                    #endregion

                    #region ReverseEntryReport
                    case "ReverseEntryReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetReverseEntryReport();
                        //dtCommonDataTable.TableName = "ReverseEntryReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ReverseEntryReport.xml");
                        strDate = "'From: " + Convert.ToDateTime(ReportObject.FromDate).ToString("dd-MMM-yyyy") + " To: " + Convert.ToDateTime(ReportObject.ToDate).ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/ReverseEntryReport.rpt";
                        break;
                    #endregion

                    #region LoanWiseInstallmentReport
                    case "LoanWiseInstallmentReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetLoanWiseInstallmentReport();
                        //dtCommonDataTable.TableName = "LoanWiseInstallmentReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/LoanWiseInstallmentReport.xml");
                        reportPath = "~/Rpt/LoanWiseInstallmentReport.rpt";
                        break;
                    #endregion

                    #region DateWisePaidInstallmentReport
                    case "DateWisePaidInstallmentReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDateWisePaidInstallmentReport();
                        //dtCommonDataTable.TableName = "DateWisePaidInstallmentReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DateWisePaidInstallmentReport.xml");
                        strDate = "";
                        if (!String.IsNullOrEmpty(ReportObject.FromDate))
                        {
                            strDate += "'From: " +
                                       Convert.ToDateTime(
                                               ReportObject.FromDate)
                                           .ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            strDate += "'From Beginning ";
                        }

                        if (!String.IsNullOrEmpty(ReportObject.ToDate))
                        {
                            strDate += "to: " +
                                       Convert.ToDateTime(
                                               ReportObject.ToDate)
                                           .ToString("dd-MM-yyyy") + "'";
                        }
                        else
                        {
                            strDate += "to: " +

                                               DateTime.Now
                                           .ToString("dd-MM-yyyy") + "'";
                        }

                        reportPath = "~/Rpt/DateWisePaidInstallmentReport.rpt";
                        break;
                    #endregion

                    #region DateWiseUnPaidInstallmentReport
                    case "DateWiseUnPaidInstallmentReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDateWiseUnPaidInstallmentReport();
                        //dtCommonDataTable.TableName = "DateWiseUnPaidInstallmentReportTable";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DateWiseUnPaidInstallmentReport.xml");
                        reportPath = "~/Rpt/DateWiseUnPaidInstallmentReport.rpt";
                        strDate = "";
                        if (!String.IsNullOrEmpty(ReportObject.FromDate))
                        {
                            strDate += "'From: " +
                                       Convert.ToDateTime(
                                               ReportObject.FromDate)
                                           .ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            strDate += "'From Beginning ";
                        }

                        if (!String.IsNullOrEmpty(ReportObject.ToDate))
                        {
                            strDate += "to: " +
                                       Convert.ToDateTime(
                                               ReportObject.ToDate)
                                           .ToString("dd-MM-yyyy") + "'";
                        }
                        else
                        {
                            strDate += "to: " +

                                       DateTime.Now
                                           .ToString("dd-MM-yyyy") + "'";
                        }
                        break;
                    #endregion

                    #region LoanAccountWiseInstallmentReport
                    case "LoanAccountWiseInstallmentReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.LoanAccountWiseInstallmentReport();
                        //dtCommonDataTable.TableName = "LoanAccountWiseInstallmentReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/LoanAccountWiseInstallmentReport.xml");
                        reportPath = "~/Rpt/LoanAccountWiseInstallmentReport.rpt";
                        break;
                    #endregion

                    #region ConLoanTransactionLoanIdWiseReport
                    case "ConLoanTransactionLoanIdWiseReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetConLoanTransactionLoanIdWiseReport();
                        //dtCommonDataTable.TableName = "ConLoanTransactionLoanIdWiseReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ConLoanTransactionLoanIdWiseReport.xml");
                        reportPath = "~/Rpt/ConLoanTransactionLoanIdWiseReport.rpt";
                        break;
                    #endregion

                    #region AccountPaymentVoucherReport
                    case "AccountPaymentVoucherReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetAccountPaymentVoucherReport();
                        //dtCommonDataTable.TableName = "AccountPaymentVoucher";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/AccountPaymentVoucher.xml");
                        reportPath = "~/Rpt/AccountPaymentVoucher.rpt";
                        break;
                    #endregion

                    #region AccountsRecievableVoucherReport
                    case "AccountsRecievableVoucherReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetAccountsRecievableReport();
                        //dtCommonDataTable.TableName = "AccountsRecievableVoucherReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/AccountsRecievableVoucherReport.xml");
                        reportPath = "~/Rpt/AccountsRecievableVoucherReport.rpt";
                        break;
                    #endregion

                    #region ContraEntryReport
                    case "ContraEntryReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetContraEntryReport();
                        //dtCommonDataTable.TableName = "ContraEntryReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ContraEntryReport.xml");
                        reportPath = "~/Rpt/ContraEntryReport.rpt";
                        break;
                    #endregion

                    #region JournalEntryReport
                    case "JournalEntryReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetJournalEntryReport();
                        //dtCommonDataTable.TableName = "JournalEntryReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/JournalEntryReport.xml");
                        reportPath = "~/Rpt/JournalEntryReport.rpt";
                        break;
                    #endregion

                    #region PeriodicDateWiseDetailsLedeger
                    case "PeriodicDateWiseDetailsLedeger":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetPeriodicLedgerReport();
                        //dtCommonDataTable.TableName = "PeriodicLedgerReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/PeriodicLedgerReport.xml");
                        strDate = "'From: " +
                                         Convert.ToDateTime(
                                             ReportObject.FromDate)
                                             .ToString("dd-MMM-yyyy") +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/PeriodicLedgerReport.rpt";
                        break;
                    #endregion

                    #region PeriodicDateWiseTrialBalance
                    case "PeriodicDateWiseTrialBalance":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetPeriodicTrialBalanceReport();
                        //dtCommonDataTable.TableName = "PeriodicTrialBalanceReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/PeriodicTrialBalanceReport.xml");
                        strDate = "'From: " +
                                         Convert.ToDateTime(
                                             ReportObject.FromDate)
                                             .ToString("dd-MMM-yyyy") +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/PeriodicTrialBalanceReport.rpt";
                        break;
                    #endregion

                    #region PeriodicTrialBalanceDetailsReport
                    case "PeriodicTrialBalanceDetailsReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetPeriodicTrialBalanceReport();
                        //dtCommonDataTable.TableName = "PeriodicTrialBalanceDetailsReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/PeriodicTrialBalanceDetailsReport.xml");
                        strDate = "'From: " +
                                         Convert.ToDateTime(
                                             ReportObject.FromDate)
                                             .ToString("dd-MMM-yyyy") +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/PeriodicTrialBalanceDetailsReport.rpt";
                        break;
                    #endregion

                    #region PeriodicDateWiseBalanceSheet
                    case "PeriodicDateWiseBalanceSheet":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetPeriodicDateWiseBalanceSheetReport();
                        //dtCommonDataTable.TableName = "PeriodicDateWiseBalanceSheet";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/PeriodicDateWiseBalanceSheetReport.xml");
                        strDate = "'From: " +
                                         Convert.ToDateTime(
                                             ReportObject.FromDate)
                                             .ToString("dd-MMM-yyyy") +
                                         " To: " +
                                         Convert.ToDateTime(
                                             ReportObject.ToDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/PeriodicBalanceSheetReport.rpt";
                        break;
                    #endregion

                    #region ExpenseStatementReport
                    case "ExpenseStatementReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetExpenseStatementReport();
                        //dtCommonDataTable.TableName = "ExpenseStatementReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ExpenseStatementReport.xml");
                        reportPath = "~/Rpt/ExpenseStatementReport.rpt";
                        break;
                    #endregion

                    #region IncomeStatementReport
                    case "IncomeStatementReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetIncomeStatementReport();
                        //dtCommonDataTable.TableName = "IncomeStatementReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/IncomeStatementReport.xml");
                        strDate = "'Date: " +
                                         Convert.ToDateTime(
                                             ReportObject.FromDate)
                                             .ToString("dd-MMM-yyyy") + "'";
                        reportPath = "~/Rpt/IncomeStatementReport.rpt";
                        break;
                    #endregion

                    #region ProfitAndLostStatementReport
                    case "ProfitAndLostStatementReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetProfitAndLostStatementReport();
                        //dtCommonDataTable.TableName = "ProfitAndLostStatementReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ProfitAndLostStatementReport.xml");
                        reportPath = "~/Rpt/ProfitAndLostStatement.rpt";
                        break;
                    #endregion

                    #region EmployeeAttendanceReport
                    case "EmployeeAttendanceReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetEmployeeAttendanceReport();
                        //dtCommonDataTable.TableName = "EmployeeAttendanceReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/EmployeeAttendanceReport.xml");
                        reportPath = "~/Rpt/EmployeeAttendanceReport.rpt";
                        break;
                    #endregion

                    #region LoanApplication
                    case "LoanApplication":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetLoanApplicationReport();
                        //dtCommonDataTable.TableName = "LoanApplicationReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/LoanApplicationReport.xml");
                        reportPath = "~/Rpt/LoanApplicationReport.rpt";
                        break;
                    #endregion

                    #region LoanApprovalReport
                    case "LoanApprovalReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetLoanApprovalReport(Server.MapPath("~/"));

                        //dtCommonDataTable.TableName = "LoanApprovalReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/LoanApprovalReport.xml");
                        reportPath = "~/Rpt/LoanApprovalReport.rpt";
                        break;
                    #endregion

                    #region SalaryProcessReport
                    case "SalaryProcessReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetSalaryProcessReport();
                        //dtCommonDataTable.TableName = "SalaryProcessReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/SalaryProcessReport.xml");
                        reportPath = "~/Rpt/SalaryProcessReport.rpt";
                        break;
                    #endregion

                    #region MonthlyPayslipReport
                    case "MonthlyPayslipReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetMonthlyPayslipReport();
                        //dtCommonDataTable.TableName = "MonthlyPayslipReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/MonthlyPayslipReport.xml");
                        reportPath = "~/Rpt/MonthlyPayslipReport.rpt";
                        break;
                    #endregion

                    #region DetailListReport
                    case "DetailListReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetDetailListReport();
                        //dtCommonDataTable.TableName = "DetailListReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/DetailListReport.xml");
                        if (ReportObject.ToDate != "")
                        {
                            strDate = "'Date: " + Convert.ToDateTime(ReportObject.ToDate).ToString("dd-MMM-yyyy") + "'";
                        }
                        else
                        {
                            strDate = "'Date: " + DateTime.Now.Date.ToString("dd-MMM-yyyy") + "'";
                        }

                        reportPath = "~/Rpt/DetailListReport.rpt";
                        break;
                    #endregion
                    #region ShareHoldingPosition
                    case "ShareHoldingPosition":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetShareHoldingPosition();
                        //dtCommonDataTable.TableName = "ShareHoldingPosition";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/ShareHoldingPosition.xml");
                        strDate = "";
                        if (!String.IsNullOrEmpty(ReportObject.FromDate))
                        {
                            strDate += "'From: " +
                                       Convert.ToDateTime(
                                               ReportObject.FromDate)
                                           .ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            strDate += "'From Beginning ";
                        }

                        if (!String.IsNullOrEmpty(ReportObject.ToDate))
                        {
                            strDate += "to: " +
                                       Convert.ToDateTime(
                                               ReportObject.ToDate)
                                           .ToString("dd-MM-yyyy") + "'";
                        }
                        else
                        {
                            strDate += "to: " +

                                       DateTime.Now
                                           .ToString("dd-MM-yyyy") + "'";
                        }
                        reportPath = "~/Rpt/ShareHoldingPosition.rpt";
                        break;
                    #endregion
                    #region UserListReport
                    case "UserListReport":
                        dtCommonDataTable = new DataTable();
                        dtCommonDataTable = objReportsBLL.GetUserListReport();
                        //dtCommonDataTable.TableName = "UserListReport";
                        //dtCommonDataTable.WriteXmlSchema(Server.MapPath(".") + "/UserListReport.xml");
                        reportPath = "~/Rpt/UserListReport.rpt";
                        break;
                    #endregion


                    default: break;
                }

                document.Load(Server.MapPath(reportPath));
                document.SetDataSource(dtCommonDataTable);
                //document.ExportToHttpResponse(ExportFormatType.Excel, Response, false, "EmployeeInformation");
                if (!string.IsNullOrEmpty(strDate))
                {
                    document.DataDefinition.FormulaFields["ReportDate"].Text = strDate;
                }
                if (!string.IsNullOrEmpty(strBranch))
                {
                    document.DataDefinition.FormulaFields["ReportBranch"].Text = strBranch;
                }
                document.DataDefinition.FormulaFields["CN"].Text = "'" + SessionUtility.TMSessionContainer.CompanyName + "'";

                Session["ReportDocument"] = document;


                //   document.SetParameterValue("ImageUrl", "F:/project.Net/TonyMultipurpose.root/TonyMultipurpose/TonyMultipurpose/LOGO/Logo.jpg");
                //document.SetParameterValue("@DateRange", "Date From " +
                //    Convert.ToDateTime(ReportObject.FromDate).ToString("dd/MM/yy") + " To " + Convert.ToDateTime(ReportObject.ToDate).ToString("dd/MM/yy"));
                crpViewer.ReportSource = document;
                crpViewer.DisplayToolbar = true;
                crpViewer.ToolPanelView = ToolPanelViewType.None;
                crpViewer.Zoom(125);
                crpViewer.ID = Session["ReportName"].ToString();
                //document.ExportToHttpResponse(ExportFormatType.Excel, Response, false, ReportObject.ReportName);

                if(ReportObject.ReportName== "LoanApplication"|| ReportObject.ReportName== "LoanApprovalReport")
                {
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();


                    ReportClass rptH = new ReportClass();
                    rptH.FileName = Server.MapPath(reportPath);
                    rptH.Load();
                    rptH.SetDataSource(dtCommonDataTable);
                    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //File(stream, "application/pdf");

                }
             
               else if (ReportObject.ReportFormat == "PDF")
                {
                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();


                    ReportClass rptH = new ReportClass();
                    rptH.FileName = Server.MapPath(reportPath);
                    rptH.Load();
                    rptH.SetDataSource(dtCommonDataTable);
                    Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                     //File(stream, "application/pdf");




                   // document.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, ReportObject.ReportName);
                    //document.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, ReportObject.ReportName);
                }
                else if (ReportObject.ReportFormat == "Excel")
                {
                    document.ExportToHttpResponse(ExportFormatType.Excel, Response, false, ReportObject.ReportName);
                }
                else if (ReportObject.ReportFormat == "CSV")
                {
                    document.ExportToHttpResponse(ExportFormatType.CharacterSeparatedValues, Response, false, ReportObject.ReportName);
                }
                crpViewer.RefreshReport();
            }
        }
    }
}