﻿//Account No autocomplete
$("#AccountNumber").autocomplete({
    source: function (request, response) {
        $.ajax({
            //url: "/FixedDeposit/MatureFD/AutoCompleteAccountNumber",
            url: window.applicationBaseUrl + "FixedDeposit/FDIAoumtWithdraw/AutoCompleteAccountNumber",//'@Url.Action("AutoCompleteAccountNumber", "FDIAoumtWithdraw", new {Area = "FixedDeposit"})',
            type: "GET",
            dataType: "json",
            data: { term: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return { label: item, value: item };
                }));
            }
        });
    },
    messages: {
        noResults: "",
        results: ""
    }
});

$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "FixedDeposit/FDIAoumtWithdraw/GetTransactionalCoaData",//'@Url.Action("GetTransactionalCoaData", "FDIAoumtWithdraw", new {Area = "FixedDeposit"})',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});


//load data
$("#AccountNumber").change(function () {
    var AccountNumber = $("#AccountNumber").val();
    var json = {
        AccountNumber: AccountNumber
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "FixedDeposit/FDIAoumtWithdraw/GetFDAccountInfoByAccountNoForInterestWithdraw",//'@Url.Action("GetFDAccountInfoByAccountNoForInterestWithdraw", "FDIAoumtWithdraw", new {Area = "FixedDeposit"})',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            $("#submit").removeAttr('disabled');
            var currentBalance = data.Balance;
            var capital = data.Capital;
            if (data.IsApproved == false) {
                $("#submit").attr('disabled', 'disabled');
                alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
            }
            if (data.IsActive == false) {
                $('#CustomerId').val('');
                $('#CustomerName').val('');
                $('#CustomerName').val('');
                $('#Capital').val('');
                $('#InterestRate').val('');
                $('#DurationofMonth').val('');
                $('#MatureDate').val('');
                $('#InterestAmount').val('');
                $("#submit").attr('disabled', 'disabled');
                alert('This Account was closed !!!');
            } else if (currentBalance > capital) {
                $("#submit").removeAttr('disabled');
                var accuredBalance = parseFloat(currentBalance - capital).toFixed(2);
                $("#submit").removeAttr('disabled');
                $('#CustomerId').val(data.CustomerId);
                $('#CustomerName').val(data.CustomerName);
                $('#CustomerName').val(data.CustomerName);
                $('#Capital').val(data.Capital);
                $('#InterestRate').val(data.InterestRate);
                $('#DurationofMonth').val(data.DurationofMonth);
                $('#MatureDate').val(FormateDate(data.MatureDate));
                $('#InterestAmount').val(accuredBalance);
            } else {
                $("#submit").attr('disabled', 'disabled');
                alert('No Interest Amount yet!!!');
            }

        }
    });
});


//--Interest Withdraw

$('#submit').click(function () {

    var isAllDataValid = true;

    if ($('#AccountNumber').val() == '') {
        $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
        isAllDataValid = false;

    } else {
        $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#InterestRate').val() == '') {
        isAllDataValid = false;
        $('#InterestRate').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#InterestRate').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#AccuredBalance').val() == '') {
        isAllDataValid = false;
        $('#AccuredBalance').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#AccuredBalance').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#InterestAmount').val() == '') {
        isAllDataValid = false;
        $('#InterestAmount').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#InterestAmount').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#MatureDate').val() == '') {
        isAllDataValid = false;
        $('#MatureDate').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#MatureDate').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#TransactionDate').val() == '') {
        isAllDataValid = false;
        $('#TransactionDate').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#TransactionDate').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#Capital').val() == '') {
        isAllDataValid = false;
        $('#Capital').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#Capital').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#CustomerName').val() == '') {
        isAllDataValid = false;
        $('#CustomerName').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#CustomerName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#CustomerId').val() == '') {
        isAllDataValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#DurationofMonth').val() == '') {
        isAllDataValid = false;
        $('#DurationofMonth').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#DurationofMonth').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#ChartOfAccount').val().trim() == 0) {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'visible');
        isAllDataValid = false;
    } else {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'hidden');
    }


    if (isAllDataValid) {

        var data = {
            AccountNumber: $('#AccountNumber').val(),
            CustomerName: $('#CustomerName').val(),
            CustomerId: $('#CustomerId').val(),
            InterestRate: $('#InterestRate').val(),
            DurationofMonth: $('#DurationofMonth').val(),
            //MatureAmount: $('#MatureAmount').val(),
            AccruedBalance: $('#InterestAmount').val(),
            MatureDate: $('#MatureDate').val(),
            COAId: $('#ChartOfAccount option:selected').val()
        }

        $.ajax({
            url: window.applicationBaseUrl + "FixedDeposit/FDIAoumtWithdraw/Save",//'@Url.Action("Save", "FDIAoumtWithdraw", new {Area = "FixedDeposit"})',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status === true) {
                    alert('Successfully done.');

                    $("#AccountNumber").val('');
                    $("#CustomerName").val('');
                    $("#CustomerId").val('');
                    $("#InterestRate").val('');
                    $('#DurationofMonth').val('');
                    $('#MatureDate').val('');
                    $('#InterestAmount').val('');
                    //$('#MatureAmount').val('');
                    $('#hdfTransactionDate').val('');
                    $('#ChartOfAccount').val('0');


                } else {
                    alert('Failed');
                }
                $('#submit').val('Save');
                window.location.reload();

            },
            error: function () {
                alert('Error. Please try again.');
            }
        });

    }


});
//convert datetime
function FormateDate(jsonDate) {
    var monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}
var months = [
    'January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September',
    'October', 'November', 'December'
];
function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}
function monthNameToNum(monthname) {
    var month = months.indexOf(monthname);
    return month ? month + 1 : 0;
}
function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}

