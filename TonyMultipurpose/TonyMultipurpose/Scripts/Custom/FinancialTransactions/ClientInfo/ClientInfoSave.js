﻿
(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/Save",//'@Url.Action("Save", "Loan", new {Area = "Loan"})',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "LoanTypeName" },
                { "data": "LoanDate" },
                { "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                { "data": "InterestRate" },
                { "data": "InterestAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" }
            ],
            "order": [[0, "desc"]],
        });
        $('#LoanTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#CustomerId').val(data.CustomerId);
        $("#CustomerId").change();
        $('#LoanType').val(data.LoanTypeId);
        $('#RefId').val(data.LoanId);
        $('#hdfLoanId').val(data.LoanId);
        $('#RefName').val(data.ReferenceName);
    }

    // chart of account load
    LoadCOA();

    function LoadCOA() {
        $("#COA_accountName").empty();
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "FinancialTransactions/ClientInfo/Get_COAaccountCodeInfo",//'@Url.Action("Get_COAaccountCodeInfo", "ClientInfo", new {Area = "FinancialTransactions"})',
            //contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json),
            success: function (data) {
                $("#COA_accountName").append('<option value="0">--Select Code--</option>');
                $.each(data, function (key, value) {
                    $("#COA_accountName").append('<option value=' + value.COAId + '>' + value.AccountName + '</option>');
                    //alert(value.AccountCode);
                });
            }
        });
    }

    function Bindata() {
        var data = {
            COAId: $('#COA_accountName option:selected').val(),
            ClientCode: $('#ClientCode').val(),
            ClientName: $('#ClientName').val(),
            Company: $('#Company').val(),
            Phone: $('#Phone').val()
        }
        return data;
    }

    $('#submit').click(function () {
        var isValid = true;
        if (isValid) {
            var data = Bindata();
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/ClientInfo/ClientInfoSave",//'@Url.Action("ClientInfoSave", "ClientInfo", new {Area = "FinancialTransactions"})',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.data.Status == true) {
                        //oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.data.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#clear').click(function () {
        $('#COA_accountName').val('');
        $('#ClientCode').val('');
        $('#ClientName').val('');
        $('#Company').val('');
        $('#Phone').val('');
    });

    //function CheckValidation() {
    //    var isValid = true;
    //    if ($('#LoanType').val().trim() === '0') {
    //        isValid = false;
    //        $('#LoanType').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#LoanType').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#RefId').val().trim() === '') {
    //        isValid = false;
    //        $('#RefId').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#CustomerId').val().trim() === '') {
    //        isValid = false;
    //        $('#CustomerId').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#CustomerId').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#AccountNumber').val() === '0') {
    //        isValid = false;
    //        $('#AccountNumber').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#AccountNumber').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#LoanDate').val().trim() === '') {
    //        isValid = false;
    //        $('#LoanDate').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#LoanDate').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#LoanAmount').val().trim() === '') {
    //        isValid = false;
    //        $('#LoanAmount').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#LoanAmount').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#InterestRate').val().trim() === '') {
    //        isValid = false;
    //        $('#InterestRate').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#InterestRate').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#InstallmentTypeId').val() === '0') {
    //        isValid = false;
    //        $('#InstallmentTypeId').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#InstallmentTypeId').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#NumberOfInstallment').val().trim() === '') {
    //        isValid = false;
    //        $('#NumberOfInstallment').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#NumberOfInstallment').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#ExtraFee').val().trim() === '') {
    //        isValid = false;
    //        $('#ExtraFee').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#ExtraFee').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#LoanExpiryDate').val().trim() === '') {
    //        isValid = false;
    //        $('#LoanExpiryDate').parent().prev().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#LoanExpiryDate').parent().prev().find('span').css('visibility', 'hidden');
    //    }
    //    return isValid;
    //}

    //json to date converter
    function FormateDate(jsonDate) {
        var monthNames = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    //date picker
    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#LoanDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });
    $('#LoanExpiryDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    //popup
    $('.popup').on('click', function (e) {
        e.preventDefault();
        var loanId = $('#hdfLoanId').val();
        if ($('#hdfLoanId').val() === 'NotASavedLoan') {
            return false;
        } else {
            var link = $(this).attr('href') + "?loanId=" + loanId;
            OpenPopup(link, $(this).data("title"));
        }

    });

    function OpenPopup(pageUrl, title) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $('#popupForm', $pageContent).removeData('validator');
            $('#popupForm', $pageContent).removeData('unobtrusiveValidation');
        });

        $dialog = $('<div class="popupWindow"  style="color:Black;"></div>')
            .html($pageContent)
            .dialog({
                draggable: true,
                autoOpen: false,
                resizable: true,
                model: true,
                title: title,
                height: 650,
                width: 800,
                close: function () {
                    $dialog.dialog('destroy').remove();
                }
            });
        $dialog.dialog('open');
    }

})();
