﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class FamilyDetail:CommonModel
    {
        [Key]
        public int FamilyId { get; set; }

        public int MemberId { get; set; }

        public string MemberName { get; set; }

        public string FamilyMemberName { get; set; }

        public string Age { get; set; }

        public string Relation { get; set; }

        public string EducationalQualification { get; set; }

        public string Occupation { get; set; }
    }
}