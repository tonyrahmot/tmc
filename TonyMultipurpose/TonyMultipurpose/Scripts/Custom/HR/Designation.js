﻿(function () {

    $('#submit1').hide();
    designationTable();
    $('#submit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                DesignationName: $('#DesignationName').val(),
                DesignationCode: $('#DesignationCode').val(),
                FunctionalDesignation: $('#FunctionalDesignation').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveDesignation",//'@Url.Action("SaveDesignation", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#DesignationName').val('');
                        $('#DesignationCode').val('');
                        $('#FunctionalDesignation').val('');
                        $('#hdfDesignationId').val(d.oCommonResult.DesigId);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#submit1').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                DesignationName: $('#DesignationName').val(),
                DesignationCode: $('#DesignationCode').val(),
                DesigId: $('#hdfDesignationId').val(),
                FunctionalDesignation: $('#FunctionalDesignation').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateDesignation",//'@Url.Action("UpdateDesignation", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#DesignationName').val('');
                        $('#DesignationCode').val('');
                        $('#FunctionalDesignation').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    //document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });




    var oTable;

    function designationTable() {
        oTable = $('#DesignationTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveDesignation",//'@Url.Action("SaveDesignation", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "DesignationName" },
                { "data": "DesignationCode" },
                { "data": "FunctionalDesignation" }
            ],
            "order": [[1, "asc"]],
        });
        $('#DesignationTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        $('#DesignationName').val(data.DesignationName);
        $('#DesignationCode').val(data.DesignationCode);
        $('#FunctionalDesignation').val(data.FunctionalDesignation);
        $('#hdfDesignationId').val(data.DesigId);
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    function CheckValidation() {
        var isValid = true;
        if ($('#DesignationCode').val().trim() === '') {
            isValid = false;
            $('#DesignationCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DesignationCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    //Duplicate Check Desination Name
    $("#DesignationName").keyup(function () {
        var designationName = $("#DesignationName").val();
        var status = $("#showMessage");
        var user = $.trim(designationName);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/HR/GetDesignationNameCheck", { designationName: designationName },//'@Url.Action("GetDesignationNameCheck", "HR", new {Area = "HR" })', { designationName: designationName },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + designationName + "</b>' is not exist !</font>");
                                $('#submit').attr('disabled', false);
                                $('#submit1').attr('disabled', false);

                            } else {
                                status.html("<font color=red>'<b>" + designationName + "</b>' is exist !</font>");
                                $('#submit').attr('disabled', true);
                                $('#submit1').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }

    });

    //Duplicate Check Designation Code
    $("#DesignationCode").keyup(function () {
        var designationCode = $("#DesignationCode").val();
        var status = $("#showMessage");
        var user = $.trim(designationCode);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/HR/GetDesignationCodeCheck", { designationCode: designationCode },//'@Url.Action("GetDesignationCodeCheck", "HR", new {Area = "HR" })', { designationCode: designationCode },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + designationCode + "</b>' is not exist !</font>");
                                $('#submit').attr('disabled', false);
                                $('#submit1').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + designationCode + "</b>' is exist !</font>");
                                $('#submit').attr('disabled', true);
                                $('#submit1').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }

    });

})();