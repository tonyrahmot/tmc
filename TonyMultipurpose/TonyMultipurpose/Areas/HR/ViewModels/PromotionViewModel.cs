﻿namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class PromotionViewModel
    {
        public string EmployeeName { get; set; }
        public string DesignationName { get; set; }

        public string FunctionalDesignation { get; set; }
    }
}