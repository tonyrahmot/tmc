﻿using System.Web.Mvc;

namespace TonyMultipurpose.Areas.FinancialTransactions
{
    public class FinancialTransactionsAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FinancialTransactions";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FinancialTransactions_default",
                "FinancialTransactions/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}