﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TonyMultipurpose.Areas.Setup.Models
{
    public class Branch
    {
        [Key]
        public int BranchId { get; set; }
        [Required]
        public int ComapnyId { get; set; }

        [Display(Name = "Company Name")]

        public string CompanyName { get; set; }

        [Display(Name = "Nick Name")]
        [Required]
        public string BranchName { get; set; }
        [Required]
        public int BranchCode { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Fax { get; set; }
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [Display(Name = "Branch Name")]
        [Required]
        public string FullName { get; set; }
        public IEnumerable<Company> CompanyInfo { get; set; }
    }
}