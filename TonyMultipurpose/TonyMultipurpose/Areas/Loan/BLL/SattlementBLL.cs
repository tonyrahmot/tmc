﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.BLL
{
    //--ata
    public class SattlementBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        #region old settlement methods
        public CommonResult SaveSettlementInfo(SettlementModel objSettlementModel)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("SettlementTypeId", objSettlementModel.SettlementTypeId);
            objDbCommand.AddInParameter("LoanNo", objSettlementModel.LoanNo);
            objDbCommand.AddInParameter("SettlementDate", objSettlementModel.SettlementDate);
            objDbCommand.AddInParameter("Reason", objSettlementModel.Reason);
            //objDbCommand.AddInParameter("BeginningBalance", objSettlementModel.BeginningBalance);
            //objDbCommand.AddInParameter("EndingBalance", objSettlementModel.EndingBalance);
            //objDbCommand.AddInParameter("Principle", objSettlementModel.Principle);
            //objDbCommand.AddInParameter("Interest", objSettlementModel.Interest);
            objDbCommand.AddInParameter("CurrentInterest", objSettlementModel.CurrentInterest);
            objDbCommand.AddInParameter("CurrentInstallmentAmount", objSettlementModel.CurrentInstallmentAmount);
            objDbCommand.AddInParameter("UpdatedInterest", objSettlementModel.UpdatedInterest);
            objDbCommand.AddInParameter("UpdatedInstallmentAmount", objSettlementModel.UpdatedInstallmentAmount);
            //objDbCommand.AddInParameter("InstallmentNo", objSettlementModel.InstallmentNo);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_SaveSettlementInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Save Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public SettlementModel GetSettlementInfoById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SettlementModel> objSettlementList = new List<SettlementModel>();
            SettlementModel settlementModel = new SettlementModel();
            try
            {
                objDbCommand.AddInParameter("SettlementId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetSettlementInfoById",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        settlementModel = new SettlementModel();
                        settlementModel.SettlementId = Convert.ToInt64(objDbDataReader["SettlementId"].ToString());
                        settlementModel.LoanId = objDbDataReader["LoanId"].ToString();
                        settlementModel.SettlementTypeId = Convert.ToInt16(objDbDataReader["SettlementTypeId"].ToString());
                        settlementModel.SettlementTypeName = objDbDataReader["SettlementTypeName"].ToString();
                        settlementModel.SettlementDate = string.Format("{0:dd/MMM/yyyy}",Convert.ToDateTime(objDbDataReader["SettlementDate"]));
                        var CurrentInterest = Convert.ToString(objDbDataReader["CurrentInterest"]);
                        settlementModel.CurrentInterest = CurrentInterest != "" ? Convert.ToDecimal(CurrentInterest) : 0;
                        var CurrentInstallmentAmount = Convert.ToString(objDbDataReader["CurrentInstallmentAmount"]);
                        settlementModel.CurrentInstallmentAmount = CurrentInstallmentAmount != "" ? Convert.ToDecimal(CurrentInstallmentAmount) : 0;
                        var UpdatedInterest = Convert.ToString(objDbDataReader["UpdatedInterest"]);
                        settlementModel.UpdatedInterest = CurrentInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInterest) : 0;
                        var UpdatedInstallmentAmount = Convert.ToString(objDbDataReader["UpdatedInstallmentAmount"]);
                        settlementModel.UpdatedInstallmentAmount = UpdatedInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInstallmentAmount) : 0;
                        settlementModel.Reason = objDbDataReader["Reason"].ToString();
                        objSettlementList.Add(settlementModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return settlementModel;
        }

        public CommonResult UpdateSettlementInfo(SettlementModel settlementModel)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("SettlementId", settlementModel.SettlementId);
            objDbCommand.AddInParameter("SettlementTypeId", settlementModel.SettlementTypeId);
            objDbCommand.AddInParameter("LoanNo", settlementModel.LoanNo);
            objDbCommand.AddInParameter("SettlementDate", settlementModel.SettlementDate);
            objDbCommand.AddInParameter("Reason", settlementModel.Reason);
            objDbCommand.AddInParameter("CurrentInterest", settlementModel.CurrentInterest);
            objDbCommand.AddInParameter("CurrentInstallmentAmount", settlementModel.CurrentInstallmentAmount);
            objDbCommand.AddInParameter("UpdatedInterest", settlementModel.UpdatedInterest);
            objDbCommand.AddInParameter("UpdatedInstallmentAmount", settlementModel.UpdatedInstallmentAmount);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_UpdateSettlementInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Update Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                   
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion old settlement methods






        #region settlement new methods
        public List<SettlementViewModel> GetAllSattlementInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SettlementViewModel> objSettlementList = new List<SettlementViewModel>();
            SettlementViewModel objSettlementModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetAllSattlementInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objSettlementModel = new SettlementViewModel();
                        objSettlementModel.SettlementId = Convert.ToInt64(objDbDataReader["SettlementId"].ToString());
                        objSettlementModel.LoanNo = objDbDataReader["LoanNo"].ToString();
                        objSettlementModel.SettlementTypeId =
                            Convert.ToInt16(objDbDataReader["SettlementTypeId"].ToString());
                        objSettlementModel.SettlementTypeName = objDbDataReader["SettlementTypeName"].ToString();
                        objSettlementModel.SettlementDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(objDbDataReader["SettlementDate"]));
                        var CurrentInterest = Convert.ToString(objDbDataReader["CurrentInterest"]);
                        objSettlementModel.CurrentInterest = CurrentInterest != "" ? Convert.ToDecimal(CurrentInterest) : 0;
                        var CurrentInstallmentAmount = Convert.ToString(objDbDataReader["CurrentInstallmentAmount"]);
                        objSettlementModel.CurrentInstallmentAmount = CurrentInstallmentAmount != "" ? Convert.ToDecimal(CurrentInstallmentAmount) : 0;
                        var UpdatedInterest = Convert.ToString(objDbDataReader["UpdatedInterest"]);
                        objSettlementModel.UpdatedInterest = CurrentInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInterest) : 0;
                        var UpdatedInstallmentAmount = Convert.ToString(objDbDataReader["UpdatedInstallmentAmount"]);
                        objSettlementModel.UpdatedInstallmentAmount = UpdatedInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInstallmentAmount) : 0;
                        objSettlementModel.Reason = objDbDataReader["Reason"].ToString();
                        objSettlementModel.CreatedName = objDbDataReader["CreatedName"].ToString();
                        objSettlementModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objSettlementList.Add(objSettlementModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objSettlementList;
        }

        public SettlementViewModel GetCustomerInfoByLoanId(string LoanNo)
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            SettlementViewModel objSettlementViewModel = new SettlementViewModel();
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("LoanNo", LoanNo);
              
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetCustomerInfoByLoanId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objSettlementViewModel = new SettlementViewModel();
                        objSettlementViewModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objSettlementViewModel.FatherName = objDbDataReader["FatherName"].ToString();
                        objSettlementViewModel.MotherName = objDbDataReader["MotherName"].ToString();
                        //objSettlementViewModel.Gender = objDbDataReader["Gender"].ToString();
                        objSettlementViewModel.Mobile = objDbDataReader["Mobile"].ToString();
                        objSettlementViewModel.LoanAmount = Convert.ToDecimal(objDbDataReader["LoanAmount"].ToString());
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objSettlementViewModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objSettlementViewModel.CustomerImage = "\\Not Found\\";
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objSettlementViewModel;
        }

        public List<SettlementModel> GetSettlementType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SettlementModel> objSettlmentTypeList = new List<SettlementModel>();
            SettlementModel objSettlementModel = new SettlementModel();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetSettlementType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objSettlementModel=new SettlementModel();
                        if (objDbDataReader["SettlementTypeId"] != null)
                        {
                            objSettlementModel.SettlementTypeId = Convert.ToInt16(objDbDataReader["SettlementTypeId"].ToString());
                            objSettlementModel.SettlementTypeName = objDbDataReader["SettlementTypeName"].ToString();
                            
                           objSettlmentTypeList.Add(objSettlementModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objSettlmentTypeList;
        }

        public List<string> AutoCompleteLoanId(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objLoanIdList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("LoanNo", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_AutoCompleteLoanId]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanNo"] != null)
                        {
                            objLoanIdList.Add(objDbDataReader["LoanNo"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanIdList;
        }
        public CommonResult DeleteSettlementInfo(string id)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("SettlementId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_DeleteSettlementInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Delete Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();

                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion 







        #region settlement approval methods
        public List<SettlementViewModel> GetUnApprovedSettlementList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SettlementViewModel> listOfSettlement = new List<SettlementViewModel>();
            SettlementViewModel objSettlementModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetUnApprovedSettlementList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objSettlementModel = new SettlementViewModel();
                        objSettlementModel.SettlementId = Convert.ToInt64(objDbDataReader["SettlementId"].ToString());
                        objSettlementModel.LoanNo = objDbDataReader["LoanNo"].ToString();
                        objSettlementModel.SettlementTypeId = Convert.ToInt16(objDbDataReader["SettlementTypeId"].ToString());
                        objSettlementModel.SettlementTypeName = objDbDataReader["SettlementTypeName"].ToString();
                        //objSettlementModel.SettlementDate =
                             //Convert.ToDateTime(objDbDataReader["SettlementDate"].ToString());
                        objSettlementModel.SettlementDate = String.Format("{0:dd-MMM-yyyy}",
                            objDbDataReader["SettlementDate"]);
                        var accType = Convert.ToInt16(objDbDataReader["AccountSetupId"]);
                        var CurrentInterest = Convert.ToString(objDbDataReader["CurrentInterest"]);
                        objSettlementModel.CurrentInterest = CurrentInterest != "" ? Convert.ToDecimal(CurrentInterest) : 0;
                        if (accType == 12)
                        {
                            objSettlementModel.CurrentInterest = objSettlementModel.CurrentInterest / 2;
                        }
                        var CurrentInstallmentAmount = Convert.ToString(objDbDataReader["CurrentInstallmentAmount"]);
                        objSettlementModel.CurrentInstallmentAmount = CurrentInstallmentAmount != "" ? Convert.ToDecimal(CurrentInstallmentAmount) : 0;
                        var UpdatedInterest = Convert.ToString(objDbDataReader["UpdatedInterest"]);
                        objSettlementModel.UpdatedInterest = CurrentInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInterest) : 0;
                        if (accType == 12)
                        {
                            objSettlementModel.UpdatedInterest = objSettlementModel.UpdatedInterest / 2;
                        }
                        var UpdatedInstallmentAmount = Convert.ToString(objDbDataReader["UpdatedInstallmentAmount"]);
                        objSettlementModel.UpdatedInstallmentAmount = UpdatedInstallmentAmount != "" ? Convert.ToDecimal(UpdatedInstallmentAmount) : 0;
                        objSettlementModel.Reason = objDbDataReader["Reason"].ToString();
                        objSettlementModel.CreatedName = objDbDataReader["CreatedName"].ToString();
                        objSettlementModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objSettlementModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objSettlementModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objSettlementModel.CustomerImage = "\\Not Found\\";
                        }
                        listOfSettlement.Add(objSettlementModel);

                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfSettlement;

        }

        public string ApproveSettlement(string[] ch)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("SettlementId", id);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_ApproveSettlement]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public string RejectSettlements(List<SettlementViewModel> settlements)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var value in settlements)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("SettlementId", value.SettlementId);
                objDbCommand.AddInParameter("RejectReason", value.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_RejectSettlements]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        #endregion

      
    }
}