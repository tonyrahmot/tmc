﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.Areas.Transaction.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Transaction.Controllers
{
    //--ataur
    //[AuthenticationFilter]
    public class SavingsApprovalController : Controller
    {
        // GET: Transaction/SavingsApproval
        TransactionBLL objTransactionBll=new TransactionBLL();
        public ActionResult Index( string ac)
        {
            TransactionModel objTransactionModel = new TransactionModel();
            objTransactionModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            objTransactionModel.TransactionType = ac;
            List<TransactionModel> listOfSavingsTransaction = objTransactionBll.GetSavingsTransactions(objTransactionModel);
            
            return View(listOfSavingsTransaction);
        }


        public ActionResult Detail(int Id)
        {
            TransactionModel objTransactionModel = new TransactionModel();
            var  savingsTransactionData = objTransactionBll.GetSavingsTransactionsById(Id);
            return View(savingsTransactionData);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
           var ch = form.GetValues("Id");
            if(ch != null)
            {
                objTransactionBll.ApproveSavingsTransaction(ch);
            }
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<TransactionModel> transactions = new List<TransactionModel>();
            TransactionModel objTransactionModel;
            var ids = form.GetValues("Id");
            if(ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objTransactionModel = new TransactionModel();
                    objTransactionModel.TransactionId = ids[i];
                    objTransactionModel.RejectReason = form.GetValues(objTransactionModel.TransactionId)[0];
                    transactions.Add(objTransactionModel);
                }
                objTransactionBll.RejectSavingsTransaction(transactions);
            }
            return RedirectToAction("Index");
        }
    }
}