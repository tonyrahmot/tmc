﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Share.ViewModels
{
    public class CustomerViewModel
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }
        public string Gender { get; set; }
        public string CustomerImage { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string OccupationAndPosition { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Home { get; set; }
        public string Mobile { get; set; }
        public decimal? ShareQty { get; set; }
        public int? BranchId { get; set; }
        public int? CompanyId { get; set; }
        public decimal? CurrentShareQty { get; set; }
    }
}