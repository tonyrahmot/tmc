﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;
using System.Web.Helpers;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class ExperienceController : Controller
    {
        // GET: Employee/Experience
        public ActionResult Index()
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
            ViewBag.employeeName = EmployeeInfo;
            return View();
        }

        public JsonResult GetAllEmployeeById(int employeeId)
        {
            try
            {
                ExperienceBLL objExperienceBLL = new ExperienceBLL();
                if (employeeId > 0)
                {
                    List<Experience> employee = objExperienceBLL.GetAllExperienceInfo(employeeId);
                    return Json(employee, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                    List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
                    return Json(EmployeeInfo, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public ActionResult Save()
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult Save(Experience objExperienceInfo)
        {

            //objExperienceInfo.CountryId = Convert.ToInt16(Request.Form["CountryId"]);

            if (ModelState.IsValid)
            {
                ExperienceBLL objExperienceBLL = new ExperienceBLL();

                objExperienceBLL.SaveExperienceInfo(objExperienceInfo);

            }

            return RedirectToAction("Index", "Experience");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ExperienceBLL objExperienceBLL = new ExperienceBLL();

            var experience = objExperienceBLL.GetAllExperienceInfo().Where(a => a.ExperienceId == id).FirstOrDefault();
            return View(experience);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Edit(Experience objExperienceInfo)
        {

            if (ModelState.IsValid)
            {
                ExperienceBLL objExperienceBLL = new ExperienceBLL();

                objExperienceBLL.UpdateExperienceInfo(objExperienceInfo);

            }
            return RedirectToAction("Index", "Experience");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ExperienceBLL objExperienceBLL = new ExperienceBLL();

            var experience = objExperienceBLL.GetAllExperienceInfo().Where(a => a.ExperienceId == id).FirstOrDefault();
            return View(experience);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        [ActionName("Delete")]
        public ActionResult DeleteExperienceInfo(int id)
        {

            if (ModelState.IsValid)
            {
                ExperienceBLL objExperienceBLL = new ExperienceBLL();
                objExperienceBLL.DeleteExperienceInfo(id);
            }
            return RedirectToAction("Index", "Experience");
        }
    }
}