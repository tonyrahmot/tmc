﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class ScanDocumentModelViewModel : CommonModel
    {
        public int Id { get; set; }
        public string LoanId { get; set; }

        [Display(Name = "Document Name")]
        public string DocumentName { get; set; }
        public string ImagePath { get; set; }

        public string OldImagePath { get; set; }
        public HttpPostedFileBase Imagefile { get; set; }
    }
}