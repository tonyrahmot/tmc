﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.BLL
{
    //--ataur
    public class RoleBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForRole(DbDataReader objDataReader, RoleInfo objRole)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "RoleId":
                        if (!Convert.IsDBNull(objDataReader["RoleId"]))
                        {
                            objRole.RoleId = Convert.ToByte(objDataReader["RoleId"]);
                        }
                        break;
                    case "RoleName":
                        if (!Convert.IsDBNull(objDataReader["RoleName"]))
                        {
                            objRole.RoleName = objDataReader["RoleName"].ToString();
                        }
                        break;

                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objRole.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;

                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objRole.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objRole.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objRole.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objRole.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objRole.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objRole.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objRole.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objRole.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objRole.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //get all role information for index
        public List<RoleInfo> GetAllRoleInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<RoleInfo> objRoleList = new List<RoleInfo>();

            RoleInfo objRole=new RoleInfo();

            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllRoleInformation]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objRole=new RoleInfo();
                        this.BuildModelForRole(objDbDataReader, objRole);
                        objRoleList.Add(objRole);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objRoleList;
        }


        //Save role information
        public string CreateRoleInfo(RoleInfo objRole)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("RoleName", objRole.RoleName);
            objDbCommand.AddInParameter("CompanyId", objRole.CompanyId);

            //objDbCommand.AddInParameter("IsActive", objUserInfo.IsActive);
            //objDbCommand.AddInParameter("AdminUserId", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateRoleInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //get roleinfo by id
        public RoleInfo GetRoleInfoById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<RoleInfo>objRoleList=new List<RoleInfo>();

            RoleInfo objRole=new RoleInfo();

            try
            {
                objDbCommand.AddInParameter("RoleId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetRoleInfoById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objRole=new RoleInfo();
                        this.BuildModelForRole(objDbDataReader, objRole);
                       objRoleList.Add(objRole);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objRole;
        }


        //update role info
        public string UpdateRoleInfo(RoleInfo objRole)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("RoleId", objRole.RoleId);
            objDbCommand.AddInParameter("RoleName", objRole.RoleName);
            objDbCommand.AddInParameter("CompanyId", objRole.CompanyId);
            objDbCommand.AddInParameter("IsActive", objRole.IsActive);

            //objDbCommand.AddInParameter("IsActive", objUserInfo.IsActive);
            //objDbCommand.AddInParameter("AdminUserId", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateRoleInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Update Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }


        //Delete role info (as IsDefault=false)
        public string DeleteRoleInfo(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("RoleId", id);
            
            //objDbCommand.AddInParameter("IsActive", objUserInfo.IsActive);
            //objDbCommand.AddInParameter("AdminUserId", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateRoleInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Update Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}