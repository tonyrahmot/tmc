﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class LookUpBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<COA> GetChartOfAccountHead()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> listOfCoaHead = new List<COA>();
            COA objCoa;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[GetChartOfAccountHeads]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCoa = new COA();
                        objCoa.COAId = Convert.ToInt32(objDbDataReader["COAId"]);
                        objCoa.AccountName = objDbDataReader["AccountName"].ToString();
                        listOfCoaHead.Add(objCoa);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfCoaHead;
        }

        public string SaveLookUpData(LookUpModel lookUpModel)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("AccountTypeId", lookUpModel.AccountTypeId);
            objDbCommand.AddInParameter("COAId", lookUpModel.COAId);

            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveLookUpData]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<LookUpModel> GetAllLookUpData()
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LookUpModel> listOfLookUps = new List<LookUpModel>();
            LookUpModel lookUpModel;
            try
            {

                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllLookUpData]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        lookUpModel = new LookUpModel();
                        lookUpModel.AccountType = objDbDataReader["AccountType"].ToString();
                        lookUpModel.AccountCode = objDbDataReader["AccountCode"].ToString();
                        listOfLookUps.Add(lookUpModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfLookUps;
        }

        public List<LookUpModel> GetAllAccountTypeLists()
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LookUpModel> listOfAccountTypes = new List<LookUpModel>();
            LookUpModel lookUpModel;
            try
            {

                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllAccountTypeLists]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        lookUpModel = new LookUpModel();
                        lookUpModel.AccountTypeId = Convert.ToInt32(objDbDataReader["AccountTypeId"]);
                        lookUpModel.AccountType = objDbDataReader["AccountType"].ToString();
                        listOfAccountTypes.Add(lookUpModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfAccountTypes;
        }
    }
}