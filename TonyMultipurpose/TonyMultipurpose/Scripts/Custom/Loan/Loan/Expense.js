  $('#UpdateMonthlyExpense').hide();
$('#deleteMonthlyExpense').hide();
var expenseTotal;
var y;
// Load Function function
var oMonthlyExpenseTable;
MonthlyExpenseTable();
function MonthlyExpenseTable() {
    oMonthlyExpenseTable = $('#MonthlyExpenseTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyExpenseInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyExpense_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "ExpenseType" },
            { "data": "Amount" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            expenseTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                 expenseTotal
            );
            var y = expenseTotal;
        }
    });
    $('#MonthlyExpenseTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            oMonthlyExpenseTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        bindDataMonthlyExpense(oMonthlyExpenseTable.row(this).data());
    });
}
function bindDataMonthlyExpense(data) {
    $('#hdfMonthlyExpenseId').val(data.MonthlyExpenseId);
    $('#MonthlyExpense_TypeId').val(data.ExpenseTypeId);
    $('#MonthlyExpense_Amount').val(data.Amount);
    $('#MonthlyExpense_LoanId').val(data.LoanId);
    $('#saveMonthlyExpense').hide();
    $('#UpdateMonthlyExpense').show();
    $('#deleteMonthlyExpense').show();
}
function MonthlyExpenseClearAll() {
    $('#MonthlyExpense_TypeId').val('');
    $('#MonthlyExpense_Amount').val('');
    $('#saveMonthlyExpense').show();
    $('#UpdateMonthlyExpense').hide();
    $('#deleteMonthlyExpense').hide();
}
// caler function
$('#MonthlyExpense_Clear').click(function () {
    MonthlyExpenseClearAll();
});
// data load function
function Binddata() {
    var LoanId = $('#MonthlyExpense_LoanId').val();
    var ExpenseTypeId = $('#MonthlyExpense_TypeId ').val();
    var Amount = $('#MonthlyExpense_Amount').val();
    var MonthlyExpenseId = $('#hdfMonthlyExpenseId').val();
    var data = {
        LoanId: $('#MonthlyExpense_LoanId').val(),
        ExpenseTypeId: $('#MonthlyExpense_TypeId option:selected').val(),
        Amount: $('#MonthlyExpense_Amount').val(),
        MonthlyExpenseId: $('#hdfMonthlyExpenseId').val()
    }
    return data;
}
//income type load function
ExpenseType();
function ExpenseType() {
    $("#MonthlyExpense_TypeId").empty();
    $.ajax({
        type: "GET",
        url: window.applicationBaseUrl + "Loan/Loan/GetExpenseType",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#MonthlyExpense_TypeId").append('<option value="0">--Select Expense Type--</option>');
            $.each(data, function (key, value) {
                $("#MonthlyExpense_TypeId").append('<option value=' + value.ExpenseTypeId + '>' + value.ExpenseType + '</option>');
            });
        }
    });
}
// Save function
$('#saveMonthlyExpense').click(function () {
    var isAllDataValid = true;
    if (isAllDataValid) {
        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/SaveExpense",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.oCommonResult.Status == true) {
                    oMonthlyExpenseTable.ajax.reload();
                    MonthlyExpenseClearAll();
                    $('#saveMonthlyExpense').show();
                    $('#UpdateMonthlyExpense').hide();
                    $('#deleteMonthlyExpense').hide();
                    document.getElementById('MonthlyExpense_showMessage').style.color = "green";
                }
                document.getElementById('MonthlyExpense_showMessage').innerHTML = d.oCommonResult.Message;
            },
            error: function () {
                document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Error. Please try again.';
            }
        });
    }
});
$('#UpdateMonthlyExpense').click(function () {

    var isAllDataValid = true;

    if (isAllDataValid) {

        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateMonthlyExpense",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    MonthlyExpenseClearAll();
                    oMonthlyExpenseTable.ajax.reload();
                    $('#saveMonthlyExpense').show();
                    $('#UpdateMonthlyExpense').hide();
                    $('#deleteMonthlyExpense').hide();
                }
            },
            error: function () {

            }
        });
    }
});
$('#deleteMonthlyExpense').click(function () {
    if (confirm("Are you sure?")) {
        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/DeleteMonthlyExpense",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    oMonthlyExpenseTable.ajax.reload();
                    MonthlyExpenseClearAll();
                    $('#saveMonthlyExpense').show();
                    $('#UpdateMonthlyExpense').hide();
                    $('#deleteMonthlyExpense').hide();
                }
            },
            error: function () {

            }
        });
    }
    return false;
});