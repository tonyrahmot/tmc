﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class EmployeeInfoViewBoth
    {
        public IEnumerable<EmployeeInformationViewModel> UnApprovedEmployeeInfo { get; set; }
        public IEnumerable<EmployeeInformationViewModel> ApprovedEmployeeInfo { get; set; }
    }
}