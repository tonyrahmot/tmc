﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
      [AuthenticationFilter]
    public class ContinuousLoanController : Controller
    {
        // GET: Loan/ContinuousLoan
        ContinuousLoanBLL objContinuousLoanBll = new ContinuousLoanBLL();
        CommonResult oCommonResult;

        public JsonResult AutoCompleteLoanId(string term)
        {
            var result = objContinuousLoanBll.GetContinuousLoanId(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetContinuousLoanInfo(string LoanId)
        {
            var result = objContinuousLoanBll.GetContinuousLoanInfo(LoanId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveContinuousLoanWithdraw(ContinuousLoan continuousLoan)
        {
            oCommonResult = new CommonResult();
            if (ModelState.IsValid)
            {
                oCommonResult = objContinuousLoanBll.SaveContinuousLoanWithdraw(continuousLoan);

            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        public ActionResult SaveContinuousLoanDeposit(ContinuousLoan continuousLoan)
        {
            oCommonResult = new CommonResult();
            if (ModelState.IsValid)
            {
                oCommonResult = objContinuousLoanBll.SaveContinuousLoanDeposit(continuousLoan);

            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        public ActionResult ContinuousLoanInfo(string id)
        {
            var model = objContinuousLoanBll.GetContinuousLoanInfo(id);
            return PartialView("_ContinuousLoanInfo", model);
        }

        #region Approval
        public ActionResult ContinuousLoanApprovalIndex()
        {
            List<ContinuousLoanViewModel> unApprovedContinuousLoanTransactions = objContinuousLoanBll.GetUnApprovedContinuousLoanTransactionList();
            return View(unApprovedContinuousLoanTransactions);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("TransactionId");
            if (ch != null)
            {
                objContinuousLoanBll.ApproveContinuousLoanTransactions(ch);
            }
            return RedirectToAction("ContinuousLoanApprovalIndex", "ContinuousLoan");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<ContinuousLoanViewModel> ContinuousLoanTransactions = new List<ContinuousLoanViewModel>();
            ContinuousLoanViewModel objContinuousLoanModel;
            var ids = form.GetValues("TransactionId");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objContinuousLoanModel = new ContinuousLoanViewModel();
                    objContinuousLoanModel.TransactionId = Convert.ToString(ids[i]);
                    objContinuousLoanModel.RejectReason = form.GetValues(objContinuousLoanModel.TransactionId.ToString())[0];
                    ContinuousLoanTransactions.Add(objContinuousLoanModel);
                }
                objContinuousLoanBll.RejectContinuousLoanTransactions(ContinuousLoanTransactions);
            }
            return RedirectToAction("ContinuousLoanApprovalIndex", "ContinuousLoan");
        }

        public ActionResult Detail(string id)
        {
            ContinuousLoanViewModel continuousLoanModel = objContinuousLoanBll.GetContinuousLoanTransactionDetail(id);
            return View(continuousLoanModel);

        }
        #endregion
    }
}