﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    public class EmployeeDetail : CommonModel
    {
        [Key]
        public short EmployeeId { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Employee Name is Required")]
        [Display(Name = "Employee Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Employee Name cannot be longer than 100 characters.")]
        public string EmployeeName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Father Name is Required")]
        [Display(Name = "Father Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Father Name cannot be longer than 100 characters.")]
        public string FatherName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Mother Name is Required")]
        [Display(Name = "Mother Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "MotherName cannot be longer than 100 characters.")]
        public string MotherName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Spouse Name is Required")]
        [Display(Name = "Spouse Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Spouse Name cannot be longer than 100 characters.")]
        public string SpouseName { get; set; }

        [Required(ErrorMessage = "Gender Required")]
        ////[Range(1,int.MaxValue,ErrorMessage = "select Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Marital Status is Required")]
        [Display(Name = "Marital Status")]
        public String MaritalStatus { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Phone is Required")]
        [Display(Name = "Phone")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Contact Number cannot be longer than 25 characters.")]
        public string Phone { get; set; }

        [Display(Name = "Email Address")]
        [StringLength(100, ErrorMessage = "Enter Valid Email Address !", MinimumLength = 3)]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", ErrorMessage = "Enter Valid Email Address !")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Nationality is Required")]
        public byte Nationality { get; set; }
        public string NationalityShow { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "NID No")]
        [Required(ErrorMessage = "NID No is Required")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "NID No cannot be longer than 25 characters.")]
        public string NationalId { get; set; }

        [Required(ErrorMessage = "Religion is Required")]
        public Religion Religion { get; set; }

        [Required(ErrorMessage = "Blood Group is Required")]
        [Display(Name = "Blood Group")]
        public byte BloodGroup { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Present Address is Required")]
        [Display(Name = "Present Address")]
        [StringLength(300, MinimumLength = 2, ErrorMessage = "Present Address cannot be longer than 300 characters.")]
        public string PresentAddress { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Permanent Address is Required")]
        [Display(Name = "Permanent Address")]
        [StringLength(300, MinimumLength = 2, ErrorMessage = "Permanent Address cannot be longer than 300 characters.")]
        public string PermanentAddress { get; set; }

        [Required]
        [Display(Name = "Joining Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime JoiningDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ending Date")]
        public DateTime EndingDate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Probation Period")]
        [StringLength(10, MinimumLength = 2, ErrorMessage = "Probation Period cannot be longer than 10 characters.")]
        public string ProbationPeriod { get; set; }

        [Display(Name = "Department")]
        public byte DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public List<Department> department { get; set; }

        [Display(Name = "Designation")]
        public byte DesignationId { get; set; }
        public string DesignationName { get; set; }
        public List<Designation> designation { get; set; }
    }

    //public enum Gender
    //{
    //    Male = 1,
    //    Female = 2,
    //    Others = 3
    //}
    //public enum MaritalStatus
    //{
    //    Married = 1,
    //    Unmarried = 2,
    //    Others = 3
    //}

    public enum Religion
    {
        Islam = 1,
        Hinduism = 2,
        Christian = 3,
        Buddhism = 4,
        Others = 5
    }
}