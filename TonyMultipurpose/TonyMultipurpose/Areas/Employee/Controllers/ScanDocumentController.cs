﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class ScanDocumentController : Controller
    {
        // GET: Employee/Education
        //public ActionResult Index()
        //{
        //    EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();
        //    List<EmployeeDetail> EmployeeInfo = objEmployeeDetailBLL.GetAllEmployee();
        //    ViewBag.employeename = EmployeeInfo;
        //    DocumentTypeBLL objDocumentTypeBLL = new DocumentTypeBLL();
        //    List<DocumentType> DocInfo = objDocumentTypeBLL.GetAllDocumentType();
        //    ViewBag.typeName = DocInfo;
        //    return View();
        //}

        //public JsonResult getProductCategories()
        //{

        //    EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
        //    List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
        //    return new JsonResult { Data = EmployeeInfo, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        [HttpGet]
        public ActionResult Save()
        {
            
            EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();
            List<EmployeeDetail> EmployeeInfo = objEmployeeDetailBLL.GetAllEmployee();
            ViewBag.employeename = EmployeeInfo;
            DocumentTypeBLL objDocumentTypeBLL = new DocumentTypeBLL();
            List<DocumentType> DocumentInfo = objDocumentTypeBLL.GetAllDocumentType();
            ViewBag.typeName = DocumentInfo;
            return View();
        }

      //  [HttpPost]

        //public async Task<ActionResult> Save(ScanDocument objScanDocument, HttpPostedFileBase File)
        //{
        //    bool status = true;

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
                    
        //            string filename = "";
        //            byte[] bytes;
        //            int BytestoRead;
        //            int numBytesRead;
        //            if (File != null)
        //            {
        //                filename = Path.GetFileName(File.FileName);
        //                bytes = new byte[File.ContentLength];
        //                BytestoRead = (int)File.ContentLength;
        //                numBytesRead = 0;
        //                while (BytestoRead > 0)
        //                {
        //                    int n = File.InputStream.Read(bytes, numBytesRead, BytestoRead);
        //                    if (n == 0) break;
        //                    numBytesRead += n;
        //                    BytestoRead -= n;
        //                }
        //                objScanDocument.Image = bytes;
        //                ScanDocumentBLL objobjScanDocumentBll = new ScanDocumentBLL();
        //                objobjScanDocumentBll.SaveScanDocument(objScanDocument);
        //            }
        //            else
        //            {
        //                status = false;
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            status = false;
        //        }
        //    }
        //    else
        //    {
        //        status = false;
        //    }
        //    if (status.Equals(true))
        //    {
        //        return RedirectToAction("Save", "ScanDocument");
        //    }
        //    else
        //    {
        //        return new JsonResult { Data = new { status = status } };
        //    }
        //}

        //public JsonResult GetAllEmployeeInfoById(int? employeeId)
        //{
        //    ScanDocumentBLL objScanDocumentBLL = new ScanDocumentBLL();
        //    List<ScanDocument> employeeList = objScanDocumentBLL.GetAllEmployeeInfoById(employeeId);
        //    //var employeeList = employee.Where(a => a.EmployeeId == employeeId);
        //    return Json(employeeList, JsonRequestBehavior.AllowGet);

        //}
    }
}