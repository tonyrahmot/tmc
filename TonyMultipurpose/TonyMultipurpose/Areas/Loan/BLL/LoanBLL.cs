﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.BLL
{
    public class LoanBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<LoanModel> GetAllLoanInfo(LoanModel objLoan)
        {
            DataTable objDataTable = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<LoanModel> objLoanList = new List<LoanModel>();

            try
            {
                objDbCommand.AddInParameter("IsApproved", objLoan.IsApproved);
                objDbCommand.AddInParameter("IsRejected", objLoan.IsRejected);
                //objDbCommand.AddInParameter("AccountSetupId", objLoan.AccountSetupId);
                objDataTable = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[loan_GetAllLoanInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objLoan = new LoanModel();
                        objLoan.LoanId = Convert.ToString(dr["LoanId"]);
                        objLoan.LoanNo = Convert.ToString(dr["LoanNo"]);

                        objLoan.AccountSetupId = Convert.ToInt32(Convert.ToString(dr["AccountSetupId"]));
                        objLoan.CustomerId = Convert.ToString(dr["CustomerId"]);
                        objLoan.AccountNumber = Convert.ToString(dr["AccountNumber"]);
                        if (string.IsNullOrEmpty(Convert.ToString(dr["LoanDate"])))
                        {
                            objLoan.LoanDate = null;
                        }
                        else
                        {
                            objLoan.LoanDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(dr["LoanDate"]));
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(dr["LoanExpiryDate"])))
                        {
                            objLoan.LoanExpiryDate = null;
                        }
                        else
                        {
                            objLoan.LoanExpiryDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(dr["LoanExpiryDate"]));
                        }
                        var LoanAmount = Convert.ToString(dr["LoanAmount"]);
                        objLoan.LoanAmount = LoanAmount != "" ? Convert.ToDecimal(LoanAmount) : 0;
                        var InterestRate = Convert.ToString(dr["InterestRate"]);
                        objLoan.InterestRate = InterestRate != "" ? Convert.ToDecimal(InterestRate) : 0;
                        var InterestRateToDisplay = Convert.ToString(dr["InterestRateToDisplay"]);
                        objLoan.InterestRateToDisplay = InterestRateToDisplay != "" ? Convert.ToDecimal(InterestRateToDisplay) : 0;
                        var InstallmentAmount = Convert.ToString(dr["InstallmentAmount"]);
                        objLoan.InstallmentAmount = InstallmentAmount != "" ? Convert.ToDecimal(InstallmentAmount) : 0;
                        var InterestAmount = Convert.ToString(dr["InterestAmount"]);
                        objLoan.InterestAmount = InterestAmount != "" ? Convert.ToDecimal(InterestAmount) : 0;
                        objLoan.InstallmentTypeId = Convert.ToByte(Convert.ToString(dr["InstallmentTypeId"]));
                        if (string.IsNullOrEmpty(Convert.ToString(dr["NumberOfInstallment"])))
                        {
                            objLoan.NumberOfInstallment = null;
                        }
                        else
                        {
                            objLoan.NumberOfInstallment = Convert.ToByte(Convert.ToString(dr["NumberOfInstallment"]));
                        }
                        var ExtraFee = Convert.ToString(dr["ExtraFee"]);
                        objLoan.ExtraFee = ExtraFee != "" ? Convert.ToDecimal(ExtraFee) : 0;
                        objLoan.TotalPurchaseAmount = Convert.ToString(dr["TotalPurchaseAmount"]);
                        //objLoan.TotalPurchaseAmount = totalPurchaseAmount != "" ? Convert.ToDecimal(totalPurchaseAmount) : 0;
                        objLoan.FullName= Convert.ToString(dr["FullName"]);

                        var totalAssetCost = Convert.ToString(dr["TotalAssetCost"]);
                        objLoan.TotalAssetCost = totalAssetCost != "" ? Convert.ToDecimal(totalAssetCost) : 0;
                        objLoan.PurposeOfLoan = Convert.ToString(dr["PurposeOfLoan"]);
                        objLoan.ReferenceName = Convert.ToString(dr["ReferenceName"]);
                        objLoan.DistributorName = Convert.ToString(dr["DistributorName"]);
                        objLoan.Message = Convert.ToString(dr["Message"]);
                        objLoan.Remarks = Convert.ToString(dr["Remarks"]);
                        objLoan.IsChecked = Convert.ToBoolean(Convert.ToString(dr["IsChecked"]));
                        objLoan.IsApproved = Convert.ToBoolean(Convert.ToString(dr["IsApproved"]));
                        objLoan.IsSubmittedToCheck = Convert.ToBoolean(Convert.ToString(dr["IsSubmittedToCheck"]));
                        objLoan.IsClosed = Convert.ToBoolean(Convert.ToString(dr["IsClosed"]));
                        objLoan.RejectReason = Convert.ToString(dr["RejectReason"]);
                        objLoan.CreatedBy = Convert.ToInt16(Convert.ToString(dr["CreatedBy"]));
                        objLoan.CreatedDate = Convert.ToString(dr["CreatedDate"]);
                        objLoan.CustomerName = Convert.ToString(dr["CustomerName"]);
                        objLoan.AccountTypeName = Convert.ToString(dr["AccountTypeName"]);
                        objLoan.InstallmentTypeName = Convert.ToString(dr["InstallmentTypeName"]);
                        objLoan.UserName = Convert.ToString(dr["UserName"]);
                        //objLoan.COAId =Convert.ToInt32(dr["coaID"].ToString());
                        //objLoan.AccountCode = dr["AccountCode"].ToString();
                        objLoan.IsDepositsFromSaving = Convert.ToBoolean(Convert.ToString(dr["IsDepositedFromSavings"]));
                        //var NetIncome = Convert.ToString(dr["NetIncum"]);
                        //objLoan.NetIncome = NetIncome != "" ? Convert.ToDecimal(NetIncome) : 0;
                        //var ExistingEmi = Convert.ToString(dr["ExistingEmi"]);
                        //objLoan.ExistingEmi = ExistingEmi != "" ? Convert.ToDecimal(ExistingEmi) : 0;
                        if (string.IsNullOrEmpty(Convert.ToString(dr["ResidentialConditionId"])))
                        {
                            objLoan.ResidentialConditionId = null;
                        }
                        else
                        {
                            objLoan.ResidentialConditionId = Convert.ToByte(Convert.ToString(dr["ResidentialConditionId"]));
                        }
                        objLoan.ResidentialConditionType = Convert.ToString(dr["ResidentialConditionType"]);
                        objLoanList.Add(objLoan);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanList;
        }

        public List<LoanModel> GetLoanInfoHistory()
        {
            DataTable objDataTable = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<LoanModel> objLoanList = new List<LoanModel>();

            try
            {
                objDataTable = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[loan_GetLoanInfoHistory]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        LoanModel objLoan = new LoanModel();
                        objLoan.LoanId = Convert.ToString(dr["LoanId"]);
                        objLoan.AccountSetupId = Convert.ToInt32(Convert.ToString(dr["AccountSetupId"]));
                        objLoan.AccountTypeName = Convert.ToString(dr["AccountTypeName"]);
                        var LoanAmount = Convert.ToString(dr["LoanAmount"]);
                        objLoan.LoanAmount = LoanAmount != "" ? Convert.ToDecimal(LoanAmount) : 0;
                        var InterestRate = Convert.ToString(dr["InterestRate"]);
                        objLoan.InterestRate = InterestRate != "" ? Convert.ToDecimal(InterestRate) : 0;
                        var InterestRateToDisplay = Convert.ToString(dr["InterestRateToDisplay"]);
                        objLoan.InterestRateToDisplay = InterestRateToDisplay != "" ? Convert.ToDecimal(InterestRateToDisplay) : 0;
                        objLoan.InstallmentTypeId = Convert.ToByte(Convert.ToString(dr["InstallmentTypeId"]));
                        objLoan.InstallmentTypeName = Convert.ToString(dr["InstallmentTypeName"]);

                        var monthlyInstallmentAmount = Convert.ToString(dr["MonthlyInstallmentAmount"]);
                        objLoan.MonthlyInstallmentAmount = monthlyInstallmentAmount != "" ? Convert.ToDecimal(monthlyInstallmentAmount) : 0;
                        var weeklyInstallmentAmount = Convert.ToString(dr["WeeklyInstallmentAmount"]);
                        objLoan.WeeklyInstallmentAmount = weeklyInstallmentAmount != "" ? Convert.ToDecimal(weeklyInstallmentAmount) : 0;

                        if (string.IsNullOrEmpty(Convert.ToString(dr["MonthlyNumberOfInstallment"])))
                        {
                            objLoan.MonthlyInstallmentNumber = null;
                        }
                        else
                        {
                            objLoan.MonthlyInstallmentNumber = Convert.ToByte(Convert.ToString(dr["MonthlyNumberOfInstallment"]));
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(dr["WeeklyNumberOfInstallment"])))
                        {
                            objLoan.WeeklyInstallmentNumber = null;
                        }
                        else
                        {
                            objLoan.WeeklyInstallmentNumber = Convert.ToByte(Convert.ToString(dr["WeeklyNumberOfInstallment"]));
                        }

                        var MonthlyDbr = Convert.ToString(dr["MonthlyDbr"]);
                        objLoan.MonthlyDbr = MonthlyDbr != "" ? Convert.ToDecimal(MonthlyDbr) : 0;

                        var WeeklyDbr = Convert.ToString(dr["WeeklyDbr"]);
                        objLoan.WeeklyDbr = WeeklyDbr != "" ? Convert.ToDecimal(WeeklyDbr) : 0;

                        var Equity = Convert.ToString(dr["Equity"]);
                        objLoan.Equity = Equity != "" ? Convert.ToDecimal(Equity) : 0;


                        objLoanList.Add(objLoan);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanList;
        }

        public List<LoanModel> GetAllLoanInfoForClosing(LoanModel objLoan)
        {
            DataTable objDataTable = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<LoanModel> objLoanList = new List<LoanModel>();

            try
            {
                objDbCommand.AddInParameter("IsApproved", objLoan.IsApproved);
                //objDbCommand.AddInParameter("IsRejected", objLoan.IsRejected);
                objDataTable = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[loan_GetAllLoanInfoForClosing]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objLoan = new LoanModel();
                        objLoan.LoanId = dr["LoanId"].ToString();
                        objLoan.LoanNo = dr["LoanNo"].ToString();
                        objLoan.AccountSetupId = Convert.ToInt32(dr["AccountSetupId"].ToString());
                        objLoan.CustomerId = dr["CustomerId"].ToString();
                        objLoan.AccountNumber = dr["AccountNumber"].ToString();
                        if (string.IsNullOrEmpty(Convert.ToString(dr["LoanDate"])))
                        {
                            objLoan.LoanDate = null;
                        }
                        else
                        {
                            objLoan.LoanDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(dr["LoanDate"]));
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(dr["LoanExpiryDate"])))
                        {
                            objLoan.LoanExpiryDate = null;
                        }
                        else
                        {
                            objLoan.LoanExpiryDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(dr["LoanExpiryDate"]));
                        }
                        var LoanAmount = Convert.ToString(dr["LoanAmount"]);
                        objLoan.LoanAmount = LoanAmount != "" ? Convert.ToDecimal(LoanAmount) : 0;
                        var InterestRate = Convert.ToString(dr["InterestRate"]);
                        objLoan.InterestRate = InterestRate != "" ? Convert.ToDecimal(InterestRate) : 0;
                        var InterestRateToDisplay = Convert.ToString(dr["InterestRateToDisplay"]);
                        objLoan.InterestRateToDisplay = InterestRateToDisplay != "" ? Convert.ToDecimal(InterestRateToDisplay) : 0;
                        var InstallmentAmount = Convert.ToString(dr["InstallmentAmount"]);
                        objLoan.InstallmentAmount = InstallmentAmount != "" ? Convert.ToDecimal(InstallmentAmount) : 0;
                        var InterestAmount = Convert.ToString(dr["InterestAmount"]);
                        objLoan.InterestAmount = InterestAmount != "" ? Convert.ToDecimal(InterestAmount) : 0;
                        objLoan.InstallmentTypeId = Convert.ToByte(Convert.ToString(dr["InstallmentTypeId"]));
                        if (string.IsNullOrEmpty(Convert.ToString(dr["NumberOfInstallment"])))
                        {
                            objLoan.NumberOfInstallment = null;
                        }
                        else
                        {
                            objLoan.NumberOfInstallment = Convert.ToByte(dr["NumberOfInstallment"].ToString());
                        }
                        var ExtraFee = Convert.ToString(dr["ExtraFee"]);
                        objLoan.ExtraFee = ExtraFee != "" ? Convert.ToDecimal(ExtraFee) : 0;
                        objLoan.TotalPurchaseAmount = Convert.ToString(dr["TotalPurchaseAmount"]);
                        //objLoan.TotalPurchaseAmount = totalPurchaseAmount != "" ? Convert.ToDecimal(totalPurchaseAmount) : 0;
                        var totalAssetCost = Convert.ToString(dr["TotalAssetCost"]);
                        objLoan.TotalAssetCost = totalAssetCost != "" ? Convert.ToDecimal(totalAssetCost) : 0;
                        objLoan.PurposeOfLoan = dr["PurposeOfLoan"].ToString();
                        objLoan.ReferenceName = dr["ReferenceName"].ToString();
                        objLoan.DistributorName = dr["DistributorName"].ToString();
                        objLoan.Message = dr["Message"].ToString();
                        objLoan.Remarks = dr["Remarks"].ToString();
                        objLoan.IsChecked = Convert.ToBoolean(dr["IsChecked"].ToString());

                        objLoan.RejectReason = dr["RejectReason"].ToString();
                        objLoan.CreatedBy = Convert.ToInt16(dr["CreatedBy"].ToString());
                        objLoan.CreatedDate = dr["CreatedDate"].ToString();
                        objLoan.CustomerName = dr["CustomerName"].ToString();
                        objLoan.AccountTypeName = dr["AccountTypeName"].ToString();
                        objLoan.InstallmentTypeName = dr["InstallmentTypeName"].ToString();
                        objLoan.UserName = dr["UserName"].ToString();
                        //objLoan.COAId =Convert.ToInt32(dr["coaID"].ToString());
                        //objLoan.AccountCode = dr["AccountCode"].ToString();
                        objLoan.IsDepositsFromSaving = Convert.ToBoolean(dr["IsDepositedFromSavings"].ToString());
                        //var NetIncome = Convert.ToString(dr["NetIncum"]);
                        //objLoan.NetIncome = NetIncome != "" ? Convert.ToDecimal(NetIncome) : 0;
                        //var ExistingEmi = Convert.ToString(dr["ExistingEmi"]);
                        //objLoan.ExistingEmi = ExistingEmi != "" ? Convert.ToDecimal(ExistingEmi) : 0;
                        if (string.IsNullOrEmpty(Convert.ToString(dr["ResidentialConditionId"])))
                        {
                            objLoan.ResidentialConditionId = null;
                        }
                        else
                        {
                            objLoan.ResidentialConditionId = Convert.ToByte(dr["ResidentialConditionId"].ToString());
                        }
                        objLoan.ResidentialConditionType = Convert.ToString(dr["ResidentialConditionType"]);
                        objLoanList.Add(objLoan);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanList;
        }

        public List<InstallmentTypeModel> GetInstallmentType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentTypeModel> installmentTypes = new List<InstallmentTypeModel>();
            InstallmentTypeModel objInstallmentType;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetInstallmentTypes]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objInstallmentType = new InstallmentTypeModel();
                        if (objDbDataReader["InstallmentTypeId"] != null)
                        {
                            objInstallmentType.InstallmentTypeId = Convert.ToByte(objDbDataReader["InstallmentTypeId"]);
                            objInstallmentType.InstallmentTypeName = Convert.ToString(objDbDataReader["InstallmentTypeName"]);
                            installmentTypes.Add(objInstallmentType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return installmentTypes;
        }

        public List<Account> GetSavingsAccounNumber(string customerId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Account> accounts = new List<Account>();
            Account objAccount;
            try
            {
                objDbCommand.AddInParameter("CustomerId", customerId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetSavingsAccounNumber]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccount = new Account();
                        if (objDbDataReader["Id"] != null)
                        {
                            objAccount.AccountNumber = Convert.ToString(objDbDataReader["Id"]);
                            objAccount.AccountName = Convert.ToString(objDbDataReader["Name"]);
                            accounts.Add(objAccount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return accounts;
        }

        public List<AccountTypeSetup> GetLoanTypeByAccountTitle()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetupList = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup = new AccountTypeSetup();
            try
            {
                objDbCommand.AddInParameter("AccountTitle", "Loan");
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountypeByTitle]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountTypeSetup = new AccountTypeSetup();
                        if (objDbDataReader["AccountSetupId"] != null)
                        {
                            objAccountTypeSetup.AccountSetupId = Convert.ToInt16(objDbDataReader["AccountSetupId"]);
                            objAccountTypeSetup.AccountTypeName = Convert.ToString(objDbDataReader["AccountTypeName"]);
                            objAccountTypeSetupList.Add(objAccountTypeSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountTypeSetupList;
        }

        public List<LoanTypeModel> GetLoanType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LoanTypeModel> objLoanTypes = new List<LoanTypeModel>();
            LoanTypeModel objLoanType = new LoanTypeModel();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetLoanType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLoanType = new LoanTypeModel();
                        if (objDbDataReader["LoanTypeId"] != null)
                        {
                            objLoanType.LoanTypeId = Convert.ToByte(objDbDataReader["LoanTypeId"]);
                            objLoanType.LoanTypeName = Convert.ToString(objDbDataReader["LoanTypeName"]);
                            objLoanTypes.Add(objLoanType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanTypes;
        }

        public List<LoanModel> GetResidentialConditionInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LoanModel> objLoanTypes = new List<LoanModel>();
            LoanModel objLoanType = new LoanModel();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetResidentialConditionInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLoanType = new LoanModel();
                        if (objDbDataReader["ResidentialConditionId"] != null)
                        {
                            objLoanType.ResidentialConditionId = Convert.ToByte(objDbDataReader["ResidentialConditionId"]);
                            objLoanType.ResidentialConditionType = Convert.ToString(objDbDataReader["ResidentialConditionType"]);
                            objLoanTypes.Add(objLoanType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanTypes;
        }

        #region LoanId creation
        //public string GetLoanId(string loanTypeId)
        //{
        //    var loanId = string.Empty;
        //    string data = "100";
        //    int affectedRows = DeleteExistingTempId(data);

        //    if (loanTypeId == "1")
        //    {
        //        loanId = "LD";
        //    }
        //    else
        //    {
        //        loanId = "SD";
        //    }

        //    string CurrentYear = (DateTime.Now.Year.ToString()).Substring(2, 2);
        //    string CurrentDayThisYear = DateTime.Now.DayOfYear.ToString();
        //    string existingCheck = string.Concat(loanId, CurrentYear, CurrentDayThisYear);
        //    string count = CheckExistingLoanId(existingCheck);
        //    if (!string.IsNullOrEmpty(count))
        //    {
        //        int Number = Convert.ToInt16(count.Substring(3, 1)) + 1;
        //        string lastNumber = string.Concat("000", Number);
        //        loanId = string.Concat(loanId, CurrentYear, CurrentDayThisYear, lastNumber);
        //    }
        //    else
        //    {
        //        string StartUniqueNumber = "0001".ToString();
        //        loanId = string.Concat(loanId, CurrentYear, CurrentDayThisYear, StartUniqueNumber);
        //    }
        //    affectedRows = SaveLoanId(loanId, data);
        //    return loanId;
        //}

        public string GetLoanId(string loanTypeId)
        {
            var loanId = string.Empty;
            int Id = 0;
            //string data = "100";
            string data = SessionUtility.TMSessionContainer.UserID.ToString();
            int affectedRows = DeleteExistingTempId(data);
            var year = (DateTime.Now.Year.ToString()).Substring(2, 2);
            var month = DateTime.Now.Month.ToString();
            month = month.PadLeft(2, '0');
            var day = DateTime.Now.Day.ToString();
            day = day.PadLeft(2, '0');
            var prefix = string.Concat(year, "-", month, "-", day, "-");

            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("FieldName", "LoanId");
            objDbCommand.AddInParameter("TableName", "Loan");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbCommand.AddInParameter("Condition", year);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV3]",
                        CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    loanId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    loanId += Convert.ToString(Id).PadLeft(6, '0');
                }
            }
            affectedRows = SaveLoanId(loanId, data);
            return loanId;
        }

        public string GetRefName(int loanTypeId)
        {

            string refname = "";
            if (loanTypeId == 1)
            {
                refname = "TMC / TL / ";
            }
            else if (loanTypeId == 12)
            {
                refname = "TMC / ET / ";
            }
            else if (loanTypeId == 3)
            {
                refname = "TMC / CL / ";
            }
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetRefName]",
                        CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    refname = refname + objDbDataReader["BranchName"].ToString();
                }
            }
            return refname;
        }

        public string GetLoanNo(string loanTypeId)
        {
            var loanNo = string.Empty;
            int Id = 0;
            string type;
            if (loanTypeId == "1" || loanTypeId == "12")
            {
                type = "LD";
            }
            else
            {
                type = "SD";
            }
            var year = (DateTime.Now.Year.ToString()).Substring(2, 2);

            var day = DateTime.Now.DayOfYear.ToString();
            day = day.PadLeft(3, '0');
            //var prefix = string.Concat(type, year, day);
            // created by Masum as New LD logic implemented
            var prefix = string.Concat(SessionUtility.TMSessionContainer.BranchCode, type, year, day);
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("FieldName", "LoanNo");
            objDbCommand.AddInParameter("TableName", "Loan");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbCommand.AddInParameter("Condition", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV3]",
                        CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    loanNo = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    loanNo += Convert.ToString(Id).PadLeft(5, '0');
                }
            }
            return loanNo;
        }

        private int DeleteExistingTempId(string data)
        {
            int rowsAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            try
            {
                objDbCommand.AddInParameter("data", data);
                rowsAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteExistingTempId]", CommandType.StoredProcedure);
                if (rowsAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return rowsAffected;
        }

        public string CheckExistingLoanId(string existingCheck)
        {
            string lastUniqueNumber = string.Empty;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            try
            {
                objDbCommand.AddInParameter("ExistingCheck", existingCheck);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_CheckExistingLoanId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        lastUniqueNumber = objDbDataReader["LastUniqueNumber"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return lastUniqueNumber;
        }
        public int SaveLoanId(string UserUniqId, string UserTempId)
        {
            int rowsAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            try
            {
                objDbCommand.AddInParameter("UserUniqId", UserUniqId);
                objDbCommand.AddInParameter("UserTempId", UserTempId);
                rowsAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveLoanId]", CommandType.StoredProcedure);
                if (rowsAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return rowsAffected;
        }
        #endregion

        #region Crud
        public CommonResult SaveLoanInfo(LoanModel objLoan)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", objLoan.LoanId);
            objDbCommand.AddInParameter("AccountSetupId", objLoan.AccountSetupId);
            objDbCommand.AddInParameter("CustomerId", objLoan.CustomerId);
            objDbCommand.AddInParameter("AccountNumber", objLoan.AccountNumber);
            objDbCommand.AddInParameter("LoanDate", objLoan.LoanDate);
            objDbCommand.AddInParameter("LoanExpiryDate", objLoan.LoanExpiryDate);
            objDbCommand.AddInParameter("LoanAmount", objLoan.LoanAmount);
            objDbCommand.AddInParameter("InterestRate", objLoan.InterestRate);
            objDbCommand.AddInParameter("InterestRateToDisplay", objLoan.InterestRateToDisplay);
            objDbCommand.AddInParameter("InterestAmount", objLoan.InterestAmount);
            objDbCommand.AddInParameter("InstallmentAmount", objLoan.InstallmentAmount);
            objDbCommand.AddInParameter("TotalInterest", objLoan.TotalInterest);
            objDbCommand.AddInParameter("InstallmentTypeId", objLoan.InstallmentTypeId);
            objDbCommand.AddInParameter("NumberOfInstallment", objLoan.NumberOfInstallment);
            objDbCommand.AddInParameter("ExtraFee", objLoan.ExtraFee);
            objDbCommand.AddInParameter("TotalPurchaseAmount", objLoan.TotalPurchaseAmount);
            objDbCommand.AddInParameter("TotalAssetCost", objLoan.TotalAssetCost);
            objDbCommand.AddInParameter("PurposeOfLoan", objLoan.PurposeOfLoan);
            objDbCommand.AddInParameter("ReferenceName", objLoan.ReferenceName);
            objDbCommand.AddInParameter("DistributorName", objLoan.DistributorName);
            objDbCommand.AddInParameter("Message", objLoan.Message);
            objDbCommand.AddInParameter("Remarks", objLoan.Remarks);
            objDbCommand.AddInParameter("IsEntry", objLoan.IsEntry);
            objDbCommand.AddInParameter("IsChecked", objLoan.IsChecked);
            objDbCommand.AddInParameter("IsApproved", objLoan.IsApproved);
            objDbCommand.AddInParameter("IsRejected", objLoan.IsRejected);
            objDbCommand.AddInParameter("IsClosed", objLoan.IsClosed);
            objDbCommand.AddInParameter("IsSubmittedToCheck", objLoan.IsSubmittedToCheck);
            objDbCommand.AddInParameter("CreatedBy", objLoan.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objLoan.CreatedDate);
            objDbCommand.AddInParameter("BranchId", objLoan.BranchId);
            objDbCommand.AddInParameter("CompanyId", objLoan.CompanyId);
            //objDbCommand.AddInParameter("COAId", objLoan.COAId);
            objDbCommand.AddInParameter("IsDepositsFromSaving", objLoan.IsDepositsFromSaving);
            objDbCommand.AddInParameter("ResidentialConditionId", objLoan.ResidentialConditionId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveLoanInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion

        #region Additional Loan
        public bool SaveAdditionalLoanInfo(string LoanId)
        {


            bool isSucessful = false;
            int rowAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", LoanId);
            objDbCommand.AddInParameter("CreatedBy", Convert.ToByte(SessionUtility.TMSessionContainer.UserID));
            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_AddAdditionalLoanInfo]",
                   CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    isSucessful = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    isSucessful = false;
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return isSucessful;
        }

        public bool DeductAdditonLoan(string LoanId)
        {


            bool isSucessful = false;
            int rowAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", LoanId);
            //objDbCommand.AddInParameter("CreatedBy", Convert.ToByte(SessionUtility.TMSessionContainer.UserID));
            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_DeductAdditonLoan]",
                   CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    isSucessful = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    isSucessful = false;
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return isSucessful;
        }

        public List<AdditionalLoanDetail> GetAdditionalLoanInfoById(string a)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AdditionalLoanDetail> infoList = new List<AdditionalLoanDetail>();
            AdditionalLoanDetail objI = new AdditionalLoanDetail();
            try
            {
                objDbCommand.AddInParameter("LoanId", a);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetAdditionalLoanInfoById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new AdditionalLoanDetail();
                        objI.AdditionalLoanDetailId = Convert.ToInt64(objDbDataReader["AdditionalLoanDetailId"].ToString());
                        objI.LoanId = objDbDataReader["LoanId"].ToString();
                        objI.InstituteName = objDbDataReader["InstituteName"].ToString();
                        objI.LoanAmount = Convert.ToDecimal(objDbDataReader["LoanAmount"].ToString());
                        objI.LoanType = objDbDataReader["LoanType"].ToString();
                        objI.AccountNo = objDbDataReader["AccountNo"].ToString();
                        objI.AccountName = objDbDataReader["AccountName"].ToString();
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["StartDate"])))
                        {
                            objI.StartDate = Convert.ToString(objDbDataReader["StartDate"].ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["ExpireDate"])))
                        {
                            objI.ExpireDate = Convert.ToString(objDbDataReader["ExpireDate"].ToString());
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["PresentOutAmount"])))
                        {
                            objI.PresentOutAmount = Convert.ToDecimal(objDbDataReader["PresentOutAmount"].ToString());
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["Installment"])))
                        {
                            objI.Installment = Convert.ToDecimal(objDbDataReader["Installment"].ToString());
                        }


                        objI.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"].ToString());
                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return infoList;
        }
        public List<AdditionalLoanDetail> GetAllAdditionalLoanInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AdditionalLoanDetail> infoList = new List<AdditionalLoanDetail>();
            AdditionalLoanDetail objI = new AdditionalLoanDetail();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetAllAdditionalLoanInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new AdditionalLoanDetail();
                        objI.LoanId = objDbDataReader["LoanId"].ToString();
                        objI.InstituteName = objDbDataReader["InstituteName"].ToString();
                        objI.LoanAmount = Convert.ToDecimal(objDbDataReader["LoanAmount"].ToString());
                        //objI.ExistingEmi = Convert.ToDecimal(objDbDataReader["ExistingEmi"].ToString());
                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return infoList;
        }
        public CommonResult UpdateAdditionalLoanInfo(List<AdditionalLoanDetail> objA)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            AdditionalLoanDetail _objALoanDetail = new AdditionalLoanDetail();
            _objALoanDetail.CreatedBy = Convert.ToByte(SessionUtility.TMSessionContainer.UserID);

            try
            {
                foreach (var item in objA)
                {
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("AdditionalLoanDetailId", item.AdditionalLoanDetailId);
                    objDbCommand.AddInParameter("InstituteName", item.InstituteName);
                    objDbCommand.AddInParameter("LoanAmount", item.LoanAmount);
                    objDbCommand.AddInParameter("Installment", item.Installment);
                    objDbCommand.AddInParameter("AccountName", item.AccountName);
                    objDbCommand.AddInParameter("AccountNo", item.AccountNo);
                    objDbCommand.AddInParameter("ExpireDate", item.ExpireDate);
                    objDbCommand.AddInParameter("StartDate", item.StartDate);
                    objDbCommand.AddInParameter("PresentOutAmount", item.PresentOutAmount);
                    objDbCommand.AddInParameter("LoanType", item.LoanType);
                    objDbCommand.AddInParameter("CreatedBy", _objALoanDetail.CreatedBy);

                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateAdditionalLoanInfo]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        oCommonResult.Status = true;
                        oCommonResult.Message = "Successful";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteAdditionalLoanInfo(AdditionalLoanDetail objA)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", objA.LoanId);
            objDbCommand.AddInParameter("AdditionalLoanDetailId", objA.AdditionalLoanDetailId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteAdditionalLoan]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion

        #region Gurantor
        //shahadat
        public CommonResult SaveGuarantor(Guarantor objGuarantor)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", objGuarantor.LoanId);
            objDbCommand.AddInParameter("GuarantorName", objGuarantor.GuarantorName);
            objDbCommand.AddInParameter("NID", objGuarantor.NID);
            objDbCommand.AddInParameter("FatherName", objGuarantor.FatherName);
            objDbCommand.AddInParameter("MotherName", objGuarantor.MotherName);
            objDbCommand.AddInParameter("Occupation", objGuarantor.Occupation);
            objDbCommand.AddInParameter("Designation", objGuarantor.Designation);
            objDbCommand.AddInParameter("InstituteName", objGuarantor.InstituteName);
            objDbCommand.AddInParameter("InstituteAddress", objGuarantor.InstituteAddress);
            objDbCommand.AddInParameter("CurrentAddress", objGuarantor.CurrentAddress);
            objDbCommand.AddInParameter("PermanentAddress", objGuarantor.PermanentAddress);
            objDbCommand.AddInParameter("ContactNo", objGuarantor.ContactNo);
            objDbCommand.AddInParameter("GuarantorImage", objGuarantor.ImagePath);
            objDbCommand.AddInParameter("GuarantorRoll", objGuarantor.GuarantorRoll);
            objDbCommand.AddInParameter("SpouseName", objGuarantor.SpouseName);
            objDbCommand.AddInParameter("RelationWithApplicant", objGuarantor.RelationWithApplicant);
            objDbCommand.AddInParameter("DateofBirth", objGuarantor.DateofBirth);
            objDbCommand.AddInParameter("MonthlyIncome", objGuarantor.MonthlyIncome);
            objDbCommand.AddInParameter("ResidentialConditionId", objGuarantor.ResidentialConditionId);
            objDbCommand.AddInParameter("BusinessAddress", objGuarantor.BusinessAddress);
            objDbCommand.AddInParameter("LoanStatusWithTmc", objGuarantor.LoanStatusWithTmc);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveGuarantor]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        //shahadat
        public List<Guarantor> GetAllGurantorInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Guarantor> objGurantorList = new List<Guarantor>();
            Guarantor objGurantor = new Guarantor();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllGurantorInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objGurantor = new Guarantor();
                        objGurantor.LoanId = Convert.ToString(objDbDataReader["LoanId"]);
                        objGurantor.GuarantorName = Convert.ToString(objDbDataReader["GuarantorName"]);
                        objGurantor.GuarantorId = Convert.ToInt32(objDbDataReader["GuarantorId"].ToString());
                        objGurantor.FatherName = Convert.ToString(objDbDataReader["FatherName"]);
                        objGurantor.MotherName = Convert.ToString(objDbDataReader["MotherName"]);
                        objGurantor.NID = Convert.ToString(objDbDataReader["NID"]);
                        objGurantor.Occupation = Convert.ToString(objDbDataReader["Occupation"]);
                        objGurantor.Designation = Convert.ToString(objDbDataReader["Designation"]);
                        objGurantor.CurrentAddress = Convert.ToString(objDbDataReader["CurrentAddress"]);
                        objGurantor.PermanentAddress = Convert.ToString(objDbDataReader["PermanentAddress"]);
                        objGurantor.InstituteName = Convert.ToString(objDbDataReader["InstituteName"]);
                        objGurantor.InstituteAddress = Convert.ToString(objDbDataReader["InstituteAddress"]);
                        objGurantor.ContactNo = Convert.ToString(objDbDataReader["ContactNo"]);

                        objGurantor.GuarantorRoll = Convert.ToString(objDbDataReader["GuarantorRoll"].ToString());
                        objGurantor.SpouseName = Convert.ToString(objDbDataReader["SpouseName"].ToString());
                        objGurantor.RelationWithApplicant = Convert.ToString(objDbDataReader["RelationWithApplicant"].ToString());
                        objGurantor.DateofBirth = Convert.ToString(objDbDataReader["DateofBirth"].ToString());
                        objGurantor.MonthlyIncome = Convert.ToDecimal(objDbDataReader["MonthlyIncome"].ToString());
                        //   objGurantor.ResidentialConditionId = Convert.ToByte(objDbDataReader["ResidentialConditionId"].ToString());
                        if (string.IsNullOrEmpty(Convert.ToString(objDbDataReader["ResidentialConditionId"])))
                        {
                            objGurantor.ResidentialConditionId = null;
                        }
                        else
                        {
                            objGurantor.ResidentialConditionId = Convert.ToByte(objDbDataReader["ResidentialConditionId"].ToString());
                        }
                        objGurantor.ResidentialConditionType = Convert.ToString(objDbDataReader["ResidentialConditionType"]);
                        objGurantor.BusinessAddress = Convert.ToString(objDbDataReader["BusinessAddress"]);
                        objGurantor.LoanStatusWithTmc = Convert.ToString(objDbDataReader["LoanStatusWithTmc"]);
                        objGurantor.OldImagePath = Convert.ToString(objDbDataReader["GuarantorImage"]);
                        if (!Convert.IsDBNull(objDbDataReader["GuarantorImage"]))
                        {
                            var imagePath = Convert.ToString(objDbDataReader["GuarantorImage"]);

                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objGurantor.ImagePath = imagePath;
                            }
                            else
                            {
                                objGurantor.ImagePath = imagePath;
                            }

                        }
                        else
                        {
                            objGurantor.ImagePath = "\\Not Found\\";
                        }
                        objGurantorList.Add(objGurantor);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objGurantorList;
        }
        //shahadat
        public CommonResult UpdateGuarantor(Guarantor objGuarantor)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("GuarantorId", objGuarantor.GuarantorId);
            objDbCommand.AddInParameter("LoanId", objGuarantor.LoanId);
            objDbCommand.AddInParameter("GuarantorName", objGuarantor.GuarantorName);
            objDbCommand.AddInParameter("NID", objGuarantor.NID);
            objDbCommand.AddInParameter("FatherName", objGuarantor.FatherName);
            objDbCommand.AddInParameter("MotherName", objGuarantor.MotherName);
            objDbCommand.AddInParameter("Occupation", objGuarantor.Occupation);
            objDbCommand.AddInParameter("Designation", objGuarantor.Designation);
            objDbCommand.AddInParameter("InstituteName", objGuarantor.InstituteName);
            objDbCommand.AddInParameter("InstituteAddress", objGuarantor.InstituteAddress);
            objDbCommand.AddInParameter("CurrentAddress", objGuarantor.CurrentAddress);
            objDbCommand.AddInParameter("PermanentAddress", objGuarantor.PermanentAddress);
            objDbCommand.AddInParameter("ContactNo", objGuarantor.ContactNo);
            objDbCommand.AddInParameter("GuarantorImage", objGuarantor.ImagePath);
            objDbCommand.AddInParameter("GuarantorRoll", objGuarantor.GuarantorRoll);
            objDbCommand.AddInParameter("SpouseName", objGuarantor.SpouseName);
            objDbCommand.AddInParameter("RelationWithApplicant", objGuarantor.RelationWithApplicant);
            objDbCommand.AddInParameter("DateofBirth", objGuarantor.DateofBirth);
            objDbCommand.AddInParameter("MonthlyIncome", objGuarantor.MonthlyIncome);
            objDbCommand.AddInParameter("ResidentialConditionId", objGuarantor.ResidentialConditionId);
            objDbCommand.AddInParameter("BusinessAddress", objGuarantor.BusinessAddress);
            objDbCommand.AddInParameter("LoanStatusWithTmc", objGuarantor.LoanStatusWithTmc);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateGuarantor]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteGuaranorInfo(Guarantor objGuarantor)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("GuarantorId", objGuarantor.GuarantorId);
            objDbCommand.AddInParameter("LoanId", objGuarantor.LoanId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteGuaranorInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successfully Deleted";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion

        #region MonthlyIncome
        public List<MonthlyIncome> GetIncomeType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            MonthlyIncome objMonthlyIncome = new MonthlyIncome();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetIncomeType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyIncome = new MonthlyIncome();
                        if (objDbDataReader["IncomeTypeId"] != null)
                        {
                            objMonthlyIncome.IncomeTypeId = Convert.ToByte(objDbDataReader["IncomeTypeId"]);
                            objMonthlyIncome.IncomeType = Convert.ToString(objDbDataReader["IncomeType"]);
                            objMonthlyIncomeList.Add(objMonthlyIncome);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objMonthlyIncomeList;
        }
        public List<MonthlyIncome> GetAllMonthlyIncomeInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            MonthlyIncome objMonthlyIncome = new MonthlyIncome();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllMonthlyIncomeInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyIncome = new MonthlyIncome();
                        objMonthlyIncome.MonthlyIncomeId = Convert.ToInt32(objDbDataReader["MonthlyIncomeId"].ToString());
                        objMonthlyIncome.LoanId = objDbDataReader["LoanId"].ToString();
                        objMonthlyIncome.IncomeType = objDbDataReader["IncomeType"].ToString();
                        //objMonthlyIncome.IncomeTypeId =Convert.ToByte( objDbDataReader["IncomeTypeId"].ToString());
                        objMonthlyIncome.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        objMonthlyIncome.SpouseAmount = Convert.ToDecimal(objDbDataReader["SpouseAmount"].ToString());
                        objMonthlyIncome.OtherIncomeAmount = Convert.ToDecimal(objDbDataReader["OtherIncomeAmount"].ToString());
                        objMonthlyIncome.SpouseIncomeType = objDbDataReader["SpouseIncomeType"].ToString();
                        objMonthlyIncome.ForeignRemitAmount = Convert.ToDecimal(objDbDataReader["ForeignRemitAmount"].ToString());
                        objMonthlyIncome.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"].ToString());
                        objMonthlyIncome.TotalIncome = Convert.ToDecimal(objDbDataReader["TotalIncome"].ToString());
                        objMonthlyIncomeList.Add(objMonthlyIncome);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objMonthlyIncomeList;
        }
        public MonthlyIncome GetaIncomeAmountByIncomeType(string incomeType, string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            MonthlyIncome objMonthlyIncome = new MonthlyIncome();
            try
            {
                objDbCommand.AddInParameter("incomeType", incomeType);
                objDbCommand.AddInParameter("loanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetaIncomeAmountByIncomeType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyIncome = new MonthlyIncome();
                        objMonthlyIncome.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        objMonthlyIncome.IncomeType = objDbDataReader["IncomeType"].ToString();
                        objMonthlyIncomeList.Add(objMonthlyIncome);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objMonthlyIncome;
        }
        public CommonResult SaveMonthlyIncome(MonthlyIncome monthlyIncome)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            //objDataAccess = DataAccess.NewDataAccess();
            //objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            //MonthlyIncome objMonthlyInc = new MonthlyIncome();
            //objMonthlyInc.Spouse = monthlyIncome.Spouse;
            //objMonthlyInc.Agriculture = monthlyIncome.Agriculture;
            //objMonthlyInc.AgricultureAmount = monthlyIncome.AgricultureAmount;
            //objMonthlyInc.SpouseAmount = monthlyIncome.SpouseAmount;
            //objMonthlyInc.OtherIncome = monthlyIncome.OtherIncome;
            //objMonthlyInc.ForeignRemittance = monthlyIncome.ForeignRemittance;
            //objMonthlyIncomeList.Add(objMonthlyInc);


            try
            {
                //    foreach (var item in objMonthlyIncomeList)
                //    {

                //        if (item.AgricultureAmount > 0)
                //        {
                //            objDataAccess = DataAccess.NewDataAccess();
                //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                //            objDbCommand.AddInParameter("LoanId", monthlyIncome.LoanId);
                //            objDbCommand.AddInParameter("Amount", item.AgricultureAmount);
                //            objDbCommand.AddInParameter("IncomeType", item.Agriculture);
                //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyIncome",
                //        CommandType.StoredProcedure);
                //            objDbCommand.Transaction.Commit();
                //            objDataAccess.Dispose(objDbCommand);

                //        }

                //        if (item.SpouseAmount > 0)
                //        {
                //            objDataAccess = DataAccess.NewDataAccess();
                //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                //            objDbCommand.AddInParameter("LoanId", monthlyIncome.LoanId);
                //            objDbCommand.AddInParameter("Amount", item.SpouseAmount);
                //            objDbCommand.AddInParameter("IncomeType", item.Spouse);
                //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyIncome",
                //        CommandType.StoredProcedure);
                //            objDbCommand.Transaction.Commit();
                //            objDataAccess.Dispose(objDbCommand);
                //        }

                //        if (item.OtherIncome > 0)
                //        {
                //            objDataAccess = DataAccess.NewDataAccess();
                //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                //            objDbCommand.AddInParameter("LoanId", monthlyIncome.LoanId);
                //            objDbCommand.AddInParameter("Amount", item.OtherIncome);
                //            objDbCommand.AddInParameter("IncomeType", "OtherIncome");
                //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyIncome",
                //        CommandType.StoredProcedure);
                //            objDbCommand.Transaction.Commit();
                //            objDataAccess.Dispose(objDbCommand);
                //        }

                //        if (item.ForeignRemittance > 0)
                //        {
                //            objDataAccess = DataAccess.NewDataAccess();
                //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                //            objDbCommand.AddInParameter("LoanId", monthlyIncome.LoanId);
                //            objDbCommand.AddInParameter("Amount", item.ForeignRemittance);
                //            objDbCommand.AddInParameter("IncomeType", "ForeignRemittance");
                //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyIncome",
                //        CommandType.StoredProcedure);
                //            objDbCommand.Transaction.Commit();
                //            objDataAccess.Dispose(objDbCommand);
                //        }

                //    }


                if (noOfAffacted > 0)
                {
                    //objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateMonthlyIncome(MonthlyIncome objMonthlyIncome)
        {

            objMonthlyIncome.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Amount", objMonthlyIncome.Amount);
            objDbCommand.AddInParameter("CreatedBy", objMonthlyIncome.CreatedBy);
            objDbCommand.AddInParameter("ForeignRemitAmount", objMonthlyIncome.ForeignRemitAmount);
            objDbCommand.AddInParameter("SpouseAmount", objMonthlyIncome.SpouseAmount);
            objDbCommand.AddInParameter("SpouseIncomeType", objMonthlyIncome.SpouseIncomeType);
            objDbCommand.AddInParameter("IncomeType", objMonthlyIncome.IncomeType);
            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            objDbCommand.AddInParameter("OtherIncomeAmount", objMonthlyIncome.OtherIncomeAmount);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyIncome]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;


            ///===========
            //var oCommonResult = new CommonResult();
            //oCommonResult.Status = false;
            //oCommonResult.Message = "Failed";
            //int noOfAffacted = 0;
            //objDataAccess = DataAccess.NewDataAccess();
            //objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            //MonthlyIncome objMonthlyInc = new MonthlyIncome();
            //objMonthlyInc.Spouse = objMonthlyIncome.Spouse;
            //objMonthlyInc.Agriculture = objMonthlyIncome.Agriculture;
            //objMonthlyInc.AgricultureAmount = objMonthlyIncome.AgricultureAmount;
            //objMonthlyInc.SpouseAmount = objMonthlyIncome.SpouseAmount;
            //objMonthlyInc.OtherIncome = objMonthlyIncome.OtherIncome;
            //objMonthlyInc.ForeignRemittance = objMonthlyIncome.ForeignRemittance;
            //objMonthlyIncomeList.Add(objMonthlyInc);
            //try
            //{
            //    foreach (var item in objMonthlyIncomeList)
            //    {

            //        if (item.AgricultureAmount > 0)
            //        {
            //            objDataAccess = DataAccess.NewDataAccess();
            //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            //            objDbCommand.AddInParameter("Amount", item.AgricultureAmount);
            //            objDbCommand.AddInParameter("IncomeType", item.Agriculture);
            //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyIncome]", CommandType.StoredProcedure);
            //            objDbCommand.Transaction.Commit();
            //            objDataAccess.Dispose(objDbCommand);

            //        }

            //        if (item.SpouseAmount > 0)
            //        {
            //            objDataAccess = DataAccess.NewDataAccess();
            //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            //            objDbCommand.AddInParameter("Amount", item.SpouseAmount);
            //            objDbCommand.AddInParameter("IncomeType", item.Spouse);
            //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyIncome]", CommandType.StoredProcedure);
            //            objDbCommand.Transaction.Commit();
            //            objDataAccess.Dispose(objDbCommand);
            //        }

            //        if (item.OtherIncome > 0)
            //        {
            //            objDataAccess = DataAccess.NewDataAccess();
            //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            //            objDbCommand.AddInParameter("Amount", item.OtherIncome);
            //            objDbCommand.AddInParameter("IncomeType", "OtherIncome");
            //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyIncome]", CommandType.StoredProcedure);
            //            objDbCommand.Transaction.Commit();
            //            objDataAccess.Dispose(objDbCommand);
            //        }

            //        if (item.ForeignRemittance > 0)
            //        {
            //            objDataAccess = DataAccess.NewDataAccess();
            //            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            //            objDbCommand.AddInParameter("Amount", item.ForeignRemittance);
            //            objDbCommand.AddInParameter("IncomeType", "ForeignRemittance");
            //            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyIncome]", CommandType.StoredProcedure);
            //            objDbCommand.Transaction.Commit();
            //            objDataAccess.Dispose(objDbCommand);
            //        }

            //    if (noOfAffacted > 0)
            //    {
            //        //objDbCommand.Transaction.Commit();
            //        oCommonResult.Status = true;
            //        oCommonResult.Message = "Successful";
            //    }
            //    else
            //    {
            //        objDbCommand.Transaction.Rollback();
            //    }
            //}
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Error : " + ex.Message);
            //}
            //finally
            //{
            //    objDataAccess.Dispose(objDbCommand);
            //}
            //return oCommonResult;
        }
        public CommonResult DeleteMonthlyIncome(MonthlyIncome objMonthlyIncome)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("MonthlyIncomeId", objMonthlyIncome.MonthlyIncomeId);
            objDbCommand.AddInParameter("LoanId", objMonthlyIncome.LoanId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteMonthlyIncome]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion

        #region
        public List<MonthlyExpense> GetExpenseType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyExpense> objMonthlyExpendList = new List<MonthlyExpense>();
            MonthlyExpense objMonthlyExpend = new MonthlyExpense();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetExpendType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyExpend = new MonthlyExpense();
                        if (objDbDataReader["ExpenseTypeId"] != null)
                        {
                            //objMonthlyExpend.ExpenseTypeId = Convert.ToByte(objDbDataReader["ExpenseTypeId"]);
                            //objMonthlyExpend.ExpenseType = Convert.ToString(objDbDataReader["ExpenseType"]);
                            objMonthlyExpendList.Add(objMonthlyExpend);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objMonthlyExpendList;
        }
        public List<MonthlyExpense> GetAllMonthlyExpenseInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyExpense> objMonthlyExpenseList = new List<MonthlyExpense>();
            MonthlyExpense objMonthlyExpense = new MonthlyExpense();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllMonthlyExpenseInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyExpense = new MonthlyExpense();
                        objMonthlyExpense.MonthlyExpenseId = Convert.ToInt32(objDbDataReader["MonthlyExpenseId"].ToString());
                        objMonthlyExpense.LoanId = objDbDataReader["LoanId"].ToString();
                        objMonthlyExpense.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"].ToString());
                        objMonthlyExpense.ChildrenEducation = Convert.ToDecimal(objDbDataReader["ChildrenEducation"].ToString());
                        objMonthlyExpense.FamilyExpense = Convert.ToDecimal(objDbDataReader["FamilyExpense"].ToString());
                        objMonthlyExpense.ExistingEMI = Convert.ToDecimal(objDbDataReader["ExistingEMI"].ToString());
                        objMonthlyExpense.OthersExpense = Convert.ToDecimal(objDbDataReader["OthersExpense"].ToString());
                        objMonthlyExpense.TotalExpense = Convert.ToDecimal(objDbDataReader["TotalExpense"].ToString());
                        objMonthlyExpenseList.Add(objMonthlyExpense);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objMonthlyExpenseList;
        }

        public MonthlyExpense GetaExpenseAmountByExpenseType(string expenseType, string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MonthlyExpense> objMonthlyExpenseList = new List<MonthlyExpense>();
            MonthlyExpense objMonthlyExpense = new MonthlyExpense();
            try
            {
                objDbCommand.AddInParameter("expenseType", expenseType);
                objDbCommand.AddInParameter("loanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetaExpenseAmountByExpenseType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMonthlyExpense = new MonthlyExpense();
                        //objMonthlyExpense.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        //objMonthlyExpense.LoanId = objDbDataReader["LoanId"].ToString();
                        objMonthlyExpenseList.Add(objMonthlyExpense);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objMonthlyExpense;
        }

        public CommonResult SaveExpense(MonthlyExpense objMonthlyExpense)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            List<MonthlyExpense> objMonthlyExpenseList = new List<MonthlyExpense>();
            MonthlyExpense objMonthlyExp = new MonthlyExpense();
            objMonthlyExp.FamilyExpense = objMonthlyExpense.FamilyExpense;
            objMonthlyExp.ChildrenEducation = objMonthlyExpense.ChildrenEducation;
            objMonthlyExp.ExistingEMI = objMonthlyExpense.ExistingEMI;
            objMonthlyExp.OthersExpense = objMonthlyExpense.OthersExpense;
            objMonthlyExpenseList.Add(objMonthlyExp);
            foreach (var item in objMonthlyExpenseList)
            {

                if (item.FamilyExpense > 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.FamilyExpense);
                    objDbCommand.AddInParameter("ExpenseType", "FamilyExpense");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);

                }

                if (item.ChildrenEducation > 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.ChildrenEducation);
                    objDbCommand.AddInParameter("ExpenseType", "ChildrenEducation");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

                if (item.ExistingEMI > 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.ExistingEMI);
                    objDbCommand.AddInParameter("ExpenseType", "ExistingEMI");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

                if (item.OthersExpense > 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.OthersExpense);
                    objDbCommand.AddInParameter("ExpenseType", "OthersExpense");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

            }


            try
            {
                if (noOfAffacted > 0)
                {
                    //objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }

            finally
            {
                //objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateExpense(MonthlyExpense objMonthlyExpense)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            List<MonthlyExpense> objMonthlyExpenseList = new List<MonthlyExpense>();
            MonthlyExpense objMonthlyExp = new MonthlyExpense();
            objMonthlyExp.FamilyExpense = objMonthlyExpense.FamilyExpense;
            objMonthlyExp.ChildrenEducation = objMonthlyExpense.ChildrenEducation;
            objMonthlyExp.ExistingEMI = objMonthlyExpense.ExistingEMI;
            objMonthlyExp.OthersExpense = objMonthlyExpense.OthersExpense;
            objMonthlyExpenseList.Add(objMonthlyExp);
            foreach (var item in objMonthlyExpenseList)
            {

                if (item.FamilyExpense >= 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.FamilyExpense);
                    objDbCommand.AddInParameter("IncomeType", "FamilyExpense");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_UpdateMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

                if (item.ChildrenEducation >= 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.ChildrenEducation);
                    objDbCommand.AddInParameter("IncomeType", "ChildrenEducation");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_UpdateMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

                if (item.ExistingEMI >= 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.ExistingEMI);
                    objDbCommand.AddInParameter("IncomeType", "ExistingEMI");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_UpdateMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }

                if (item.OthersExpense >= 0)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
                    objDbCommand.AddInParameter("Amount", item.OthersExpense);
                    objDbCommand.AddInParameter("IncomeType", "OthersExpense");
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_UpdateMonthlyExpense",
                        CommandType.StoredProcedure);
                    objDbCommand.Transaction.Commit();
                    objDataAccess.Dispose(objDbCommand);
                }
            }


            try
            {
                if (noOfAffacted > 0)
                {
                    //objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }

            finally
            {
                //objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateMonthlyExpense(MonthlyExpense objMonthlyExpense)
        {
            objMonthlyExpense.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //objDbCommand.AddInParameter("MonthlyExpenseId", objMonthlyExpense.MonthlyExpenseId);
            objDbCommand.AddInParameter("FamilyExpense", objMonthlyExpense.FamilyExpense);
            objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
            objDbCommand.AddInParameter("ExistingEMI", objMonthlyExpense.ExistingEMI);
            objDbCommand.AddInParameter("OthersExpense", objMonthlyExpense.OthersExpense);
            objDbCommand.AddInParameter("ChildrenEducation", objMonthlyExpense.ChildrenEducation);
            objDbCommand.AddInParameter("CreatedBy", objMonthlyExpense.CreatedBy);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateMonthlyExpense]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteMonthlyExpense(MonthlyExpense objMonthlyExpense)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("MonthlyExpenseId", objMonthlyExpense.MonthlyExpenseId);
            objDbCommand.AddInParameter("LoanId", objMonthlyExpense.LoanId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteMonthlyExpense]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion
        #region bank performance
        public List<BankPerformance> GetAllBankperformanceInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<BankPerformance> objList = new List<BankPerformance>();
            BankPerformance obj = new BankPerformance();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllBankperformanceInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        obj = new BankPerformance();
                        obj.BankperformanceId = Convert.ToInt64(objDbDataReader["BankperformanceId"].ToString());
                        obj.LoanId = objDbDataReader["LoanId"].ToString();
                        obj.NgoName = objDbDataReader["NgoName"].ToString();
                        obj.AccNo = objDbDataReader["AccNo"].ToString();
                        obj.DebitAmount = Convert.ToDecimal(objDbDataReader["DebitAmount"].ToString());
                        obj.CreditAmount = Convert.ToDecimal(objDbDataReader["CreditAmount"].ToString());
                        obj.FromDate = objDbDataReader["FromDate"].ToString();
                        obj.ToDate = objDbDataReader["ToDate"].ToString();
                        obj.Avg = Convert.ToDecimal(objDbDataReader["Avg"].ToString());
                        objList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objList;
        }

        public CommonResult AddbankPerformance(string LoanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", LoanId);
            objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_addBankperformance]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult Deductbankperformance(string LoanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", LoanId);
            //objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_Deductbankperformance]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult SaveBankperformance(BankPerformance obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            //  objDbCommand.AddInParameter("BankperformanceId", obj.BankperformanceId);
            objDbCommand.AddInParameter("NgoName", obj.NgoName);
            objDbCommand.AddInParameter("AccNo", obj.AccNo);
            objDbCommand.AddInParameter("DebitAmount", obj.DebitAmount);
            objDbCommand.AddInParameter("CreditAmount", obj.CreditAmount);
            objDbCommand.AddInParameter("Avg", obj.Avg);
            objDbCommand.AddInParameter("FromDate", obj.FromDate);
            objDbCommand.AddInParameter("ToDate", obj.ToDate);


            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveBankperformance",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateBankperformance(List<BankPerformance> objBP)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            BankPerformance _objBankPerformance = new BankPerformance();
            _objBankPerformance.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            try
            {
                foreach (var item in objBP)
                {
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("BankperformanceId", item.BankperformanceId);
                    objDbCommand.AddInParameter("NgoName", item.NgoName);
                    objDbCommand.AddInParameter("AccNo", item.AccNo);
                    objDbCommand.AddInParameter("DebitAmount", item.DebitAmount);
                    objDbCommand.AddInParameter("CreditAmount", item.CreditAmount);
                    objDbCommand.AddInParameter("Avg", item.Avg);
                    objDbCommand.AddInParameter("FromDate", item.FromDate);
                    objDbCommand.AddInParameter("ToDate", item.ToDate);
                    objDbCommand.AddInParameter("CreatedBy", _objBankPerformance.CreatedBy);

                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateBankperformance]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        oCommonResult.Status = true;
                        oCommonResult.Message = "Successful";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteBankperformance(BankPerformance obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("BankperformanceId", obj.BankperformanceId);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteBankperformance]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion
        #region
        public List<NotificationCheck> GetAllNotificationInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<NotificationCheck> objNotificationCheck = new List<NotificationCheck>();
            NotificationCheck obj = new NotificationCheck();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllNotificationInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        obj = new NotificationCheck();
                        obj.NotificationCheckId = Convert.ToInt32(objDbDataReader["NotificationCheckId"].ToString());
                        obj.LoanId = objDbDataReader["LoanId"].ToString();
                        obj.Notification = objDbDataReader["Notification"].ToString();
                        obj.Msg = objDbDataReader["Msg"].ToString();
                        obj.Status = Convert.ToBoolean(objDbDataReader["Status"].ToString());
                        objNotificationCheck.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objNotificationCheck;
        }
        public CommonResult SaveNotification(NotificationCheck obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            objDbCommand.AddInParameter("Notification", obj.Notification);
            objDbCommand.AddInParameter("Status", obj.Status);
            objDbCommand.AddInParameter("CreatedBy", obj.CreatedBy);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_SaveNotification",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateNotification(NotificationCheck obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("NotificationCheckId", obj.NotificationCheckId);
            objDbCommand.AddInParameter("Notification", obj.Notification);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            objDbCommand.AddInParameter("Status", obj.Status);
            objDbCommand.AddInParameter("CreatedBy", obj.CreatedBy);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateNotification]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteNotification(NotificationCheck obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("NotificationCheckId", obj.NotificationCheckId);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteNotification]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion
        #region
        public List<BankCheckList> GetBankName()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<BankCheckList> objBankCheckListList = new List<BankCheckList>();
            BankCheckList objBankCheckList = new BankCheckList();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetBankName]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objBankCheckList = new BankCheckList();
                        if (objDbDataReader["BankId"] != null)
                        {
                            objBankCheckList.BankId = Convert.ToByte(objDbDataReader["BankId"]);
                            objBankCheckList.BankName = Convert.ToString(objDbDataReader["BankName"]);
                            objBankCheckListList.Add(objBankCheckList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objBankCheckListList;
        }

        public string BankCheckListSave(BankCheckList objBankCheckList)
        {
            int noOfAffacted = 0;
            //   var customerId = GenerateCustomerId();
            string result = "failed";
            // Insert Customer

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", objBankCheckList.LoanId);
            objDbCommand.AddInParameter("BankId", objBankCheckList.BankId);
            objDbCommand.AddInParameter("AccountNumber", objBankCheckList.AccountNumber);
            objDbCommand.AddInParameter("ChequeId", objBankCheckList.ChequeId);
            objDbCommand.AddInParameter("Description", objBankCheckList.Description);
            objDbCommand.AddInParameter("Path", objBankCheckList.Path);

            try
            {

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_BankCheckListSave]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    //Save Bank Info  
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    result = "failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                // throw new Exception("Database Error Occured", ex);
                result = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return result;
        }
        public List<BankCheckList> GetAllBankCheckListInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<BankCheckList> objBankCheckList = new List<BankCheckList>();
            BankCheckList objBankCheck = new BankCheckList();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllBankCheckListInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objBankCheck = new BankCheckList();
                        objBankCheck.BankCheckListId = Convert.ToInt32(objDbDataReader["BankCheckListId"].ToString());
                        objBankCheck.LoanId = objDbDataReader["LoanId"].ToString();
                        objBankCheck.BankName = objDbDataReader["BankName"].ToString();
                        objBankCheck.BankId = Convert.ToInt32(objDbDataReader["BankId"].ToString());
                        objBankCheck.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                        objBankCheck.ChequeId = objDbDataReader["ChequeId"].ToString();
                        objBankCheck.Description = objDbDataReader["Description"].ToString();
                        var imagePath = objDbDataReader["Path"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objBankCheck.Path = imagePath;
                        }
                        else
                        {
                            objBankCheck.Path = "\\Not Found\\";
                        }

                        objBankCheckList.Add(objBankCheck);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objBankCheckList;
        }
        public CommonResult UpdateBankCheckList(BankCheckList objBankCheckList)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("BankId", objBankCheckList.BankId);
            objDbCommand.AddInParameter("AccountNumber", objBankCheckList.AccountNumber);
            objDbCommand.AddInParameter("ChequeId", objBankCheckList.ChequeId);
            objDbCommand.AddInParameter("Description", objBankCheckList.Description);
            objDbCommand.AddInParameter("BankCheckListId", objBankCheckList.BankCheckListId);
            objDbCommand.AddInParameter("Path", objBankCheckList.Path);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateBankCheckList]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteBankCheckList(BankCheckList objBankCheckList)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("BankCheckListId", objBankCheckList.BankCheckListId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteBankCheckList]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        #endregion

        public string LoanCheckApprove(string loanId)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", loanId);

            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].loan_LoanCheckApprove", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                objDbCommand.Transaction.Commit();
            }
            else
            {
                objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";

        }
        #region Loan Customer Cheque

        public CommonResult SaveLoanCustomersChequeInfo(LoanCustomerChequeInfo obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            objDbCommand.AddInParameter("AccountName", obj.AccountName);
            objDbCommand.AddInParameter("AccountNo", obj.AccountNo);
            objDbCommand.AddInParameter("ChequeNo", obj.ChequeNo);
            objDbCommand.AddInParameter("BankName", obj.BankName);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveLoanCustomerChequeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful Saved.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult UpdateLoanCustomersChequeInfo(LoanCustomerChequeInfo obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanCustomerChequeInfoId", obj.LoanCustomerChequeInfoId);
            objDbCommand.AddInParameter("LoanId", obj.LoanId);
            objDbCommand.AddInParameter("AccountName", obj.AccountName);
            objDbCommand.AddInParameter("AccountNo", obj.AccountNo);
            objDbCommand.AddInParameter("ChequeNo", obj.ChequeNo);
            objDbCommand.AddInParameter("BankName", obj.BankName);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateLoanCustomersChequeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Your Data Updated Sucessfully.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<LoanCustomerChequeInfo> GetAllLoanCustomerChequeInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LoanCustomerChequeInfo> objList = new List<LoanCustomerChequeInfo>();
            LoanCustomerChequeInfo obj = new LoanCustomerChequeInfo();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAllLoanCustomerChequeInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        obj = new LoanCustomerChequeInfo();
                        obj.LoanCustomerChequeInfoId = Convert.ToInt32(objDbDataReader["LoanCustomerChequeInfoId"].ToString());
                        obj.LoanId = Convert.ToString(objDbDataReader["LoanId"]);
                        obj.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        obj.AccountNo = Convert.ToString(objDbDataReader["AccountNo"]);
                        obj.ChequeNo = Convert.ToString(objDbDataReader["ChequeNo"]);
                        obj.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        objList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objList;
        }

        public CommonResult DeleteLoanCustomerChequeInfo(LoanCustomerChequeInfo obj)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanCustomerChequeInfoId", obj.LoanCustomerChequeInfoId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteLoanCustomerChequeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Delete  Sucessfull";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        #endregion

        #region Addtional Information/ comments and signatory 
        public CommonResult SaveLoanCustomerDetails(AddtionalInfoModel objAddtionalInfoModel)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", objAddtionalInfoModel.LoanId);
            objDbCommand.AddInParameter("CustomerDetail", objAddtionalInfoModel.CustomerDetail);
            objDbCommand.AddInParameter("GuarantorsDetail", objAddtionalInfoModel.GuarantorsDetail);
            objDbCommand.AddInParameter("Deviation", objAddtionalInfoModel.Deviation);
            objDbCommand.AddInParameter("ApproverComment", objAddtionalInfoModel.ApproverComment);

            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveAddtionalCustomerInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful Saved.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<AddtionalInfoModel> GetAddtionalInfoData(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AddtionalInfoModel> addtionalInfoDataList = new List<AddtionalInfoModel>();
            AddtionalInfoModel addtionalInfoModel = new AddtionalInfoModel();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetAddtionalInfoData]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        addtionalInfoModel = new AddtionalInfoModel();
                        addtionalInfoModel.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        addtionalInfoModel.LoanId = objDbDataReader["LoanId"].ToString();
                        addtionalInfoModel.CustomerDetail = objDbDataReader["CustomerDetail"].ToString();
                        addtionalInfoModel.GuarantorsDetail = objDbDataReader["GuarantorsDetail"].ToString();
                        addtionalInfoModel.Deviation = objDbDataReader["Deviation"].ToString();
                        addtionalInfoModel.ApproverComment = objDbDataReader["ApproverComment"].ToString();
                        addtionalInfoDataList.Add(addtionalInfoModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return addtionalInfoDataList;
        }


        public string SaveScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            int noOfAffacted = 0;
            string result = "failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", scanDocumentModel.LoanId);
            objDbCommand.AddInParameter("DocumentName", scanDocumentModel.DocumentName);
            objDbCommand.AddInParameter("ImagePath", scanDocumentModel.ImagePath);

            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_SaveScanDocumentInfo]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    result = "failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                result = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return result;
        }

        public string UpdateScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            int noOfAffacted = 0;
            string result = "failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", scanDocumentModel.Id);
            objDbCommand.AddInParameter("LoanId", scanDocumentModel.LoanId);
            objDbCommand.AddInParameter("DocumentName", scanDocumentModel.DocumentName);
            objDbCommand.AddInParameter("ImagePath", scanDocumentModel.ImagePath);

            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_UpdateScanDocumentInfo]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    result = "failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                result = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return result;
        }


        public CommonResult DeleteScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("Id", scanDocumentModel.Id);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteScanDocumentInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Delete Successful.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<ScanDocumentModel> GetScanDocumnetInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<ScanDocumentModel> scanDocuments = new List<ScanDocumentModel>();
            ScanDocumentModel scanDocument = new ScanDocumentModel();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetScanDocumnetInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        scanDocument = new ScanDocumentModel();
                        scanDocument.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        scanDocument.LoanId = objDbDataReader["LoanId"].ToString();
                        scanDocument.DocumentName = objDbDataReader["DocumentName"].ToString();


                        //if (objDbDataReader["ImagePath"] != null)
                        //{
                        //    var imagePath = objDbDataReader["ImagePath"].ToString();
                        //    scanDocument.OldImagePath = imagePath;
                        //    if (imagePath.Contains("Uploads"))
                        //    {
                        //        imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                        //        imagePath = "/" + imagePath;
                        //        scanDocument.ImagePath = imagePath;
                        //    }
                        //    else
                        //    {
                        //        //objCustomerModel.CustomerImage = "\\Not Found\\";
                        //        scanDocument.ImagePath = imagePath;
                        //    }

                        //}
                        //else
                        //{
                        //    scanDocument.ImagePath = "\\Not Found\\";
                        //}
                        /////////////////////////////////////////////////////////////////////////////////
                        var imagePath = objDbDataReader["ImagePath"].ToString();
                        scanDocument.OldImagePath = imagePath;
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            scanDocument.ImagePath = imagePath;
                        }
                        else
                        {
                            //objCustomerModel.CustomerImage = "\\Not Found\\";
                            scanDocument.ImagePath = imagePath;
                        }
                        ///////////////////////////////////////////////////////////////////////
                        scanDocuments.Add(scanDocument);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return scanDocuments;
        }

        public List<SignatoryModel> GetBranchWiseEmployee()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SignatoryModel> employeess = new List<SignatoryModel>();
            SignatoryModel signatory = new SignatoryModel();
            try
            {
                // objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetBranchWiseEmployee]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        signatory = new SignatoryModel();
                        signatory.EmpCode = objDbDataReader["EmpCode"].ToString();
                        signatory.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        signatory.DesignationName = objDbDataReader["DesignationName"].ToString();
                        employeess.Add(signatory);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return employeess;
        }

        public SignatoryModel GetDesignationByEmpCode(string empCode)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            SignatoryModel signatory = new SignatoryModel();
            //InterestRateEntry objInterestRateEntry = new InterestRateEntry();
            try
            {
                objDbCommand.AddInParameter("EmpCode", empCode);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetDesignationByEmpCode]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        signatory = new SignatoryModel();
                        signatory.DesignationName = objDbDataReader["DesignationName"].ToString();
                        signatory.FunctionalPosition = objDbDataReader["FuntionalDesignation"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return signatory;
        }

        public bool GetExitsEmployee(string empCode, string loanId)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("empCode", empCode);
                objDbCommand.AddInParameter("loanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetExitsEmployee]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public CommonResult SaveSignatoryData(SignatoryModel signatory)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", signatory.LoanId);
            objDbCommand.AddInParameter("FunctionalPosition", signatory.FunctionalPosition);
            objDbCommand.AddInParameter("EmpCode", signatory.EmpCode);


            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SaveSignatoryData]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Signatory Data Saved Sucessfull";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult DeleteSignatoryData(SignatoryModel signatory)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", signatory.LoanId);
            objDbCommand.AddInParameter("EmpCode", signatory.EmpCode);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_DeleteSignatoryData]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Signatory Data Delete Sucessfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult UpdateSignatoryData(SignatoryModel signatory)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("Id", signatory.Id);
            objDbCommand.AddInParameter("LoanId", signatory.LoanId);
            objDbCommand.AddInParameter("FunctionalPosition", signatory.FunctionalPosition);
            objDbCommand.AddInParameter("EmpCode", signatory.EmpCode);


            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateSignatoryData]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Signatory Data Update Sucessfull.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<SignatoryModel> GetSignatoryDataForDataTable(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<SignatoryModel> signatoryList = new List<SignatoryModel>();
            SignatoryModel signatory = new SignatoryModel();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetSignatoryDataForDataTable]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        signatory = new SignatoryModel();
                        signatory.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        signatory.LoanId = objDbDataReader["LoanId"].ToString();
                        signatory.FunctionalPosition = objDbDataReader["FunctionalPosition"].ToString();
                        signatory.EmpCode = objDbDataReader["EmpCode"].ToString();
                        signatory.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        signatory.DesignationName = objDbDataReader["DesignationName"].ToString();
                        signatoryList.Add(signatory);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return signatoryList;
        }

        public CommonResult UpdateCustomerDeviationInfo(AddtionalInfoModel addtionalInfoModel)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("Id", addtionalInfoModel.Id);
            objDbCommand.AddInParameter("LoanId", addtionalInfoModel.LoanId);
            objDbCommand.AddInParameter("CustomerDetail", addtionalInfoModel.CustomerDetail);
            objDbCommand.AddInParameter("GuarantorsDetail", addtionalInfoModel.GuarantorsDetail);
            objDbCommand.AddInParameter("Deviation", addtionalInfoModel.Deviation);
            objDbCommand.AddInParameter("ApproverComment", addtionalInfoModel.ApproverComment);

            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_UpdateCustomerDeviationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Your Data Updated Sucessfully.";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        #endregion

        public CommonResult CheckLoanApplication(string loanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", loanId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_CheckLoanApplication]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Checked";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult ReturnCheckLoanApplication(string loanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", loanId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_ReturnCheckLoanApplication]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Checked";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult SubmitForCheck(string loanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", loanId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_SubmitForCheck]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Checked";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult RejectLoanApplication(string loanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", loanId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_RejectLoanApplication]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Rejected";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult CloseLoan(string loanNo)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("loanNo", loanNo);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_CloseLoan]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Closed";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult ReopenLoan(string loanId)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", loanId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_ReopenLoan]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Sucessfully Reopened";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }


        public CommonResult ApproveLoanInfo(LoanModel objLoan)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("LoanId", objLoan.LoanId);
            objDbCommand.AddInParameter("LoanNo", objLoan.LoanNo);
            objDbCommand.AddInParameter("AccountSetupId", objLoan.AccountSetupId);
            objDbCommand.AddInParameter("CustomerId", objLoan.CustomerId);
            objDbCommand.AddInParameter("LoanDate", objLoan.LoanDate);
            objDbCommand.AddInParameter("LoanExpiryDate", objLoan.LoanExpiryDate);
            objDbCommand.AddInParameter("LoanAmount", objLoan.LoanAmount);
            objDbCommand.AddInParameter("InterestRate", objLoan.InterestRate);
            objDbCommand.AddInParameter("InterestRateToDisplay", objLoan.InterestRateToDisplay);
            objDbCommand.AddInParameter("InterestAmount", objLoan.InterestAmount);
            objDbCommand.AddInParameter("InstallmentAmount", objLoan.InstallmentAmount);
            objDbCommand.AddInParameter("InstallmentTypeId", objLoan.InstallmentTypeId);
            objDbCommand.AddInParameter("NumberOfInstallment", objLoan.NumberOfInstallment);
            objDbCommand.AddInParameter("TotalInterest", objLoan.TotalInterest);
            objDbCommand.AddInParameter("IsApproved", objLoan.IsApproved);
            objDbCommand.AddInParameter("CreatedBy", objLoan.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objLoan.CreatedDate);
            objDbCommand.AddInParameter("BranchId", objLoan.BranchId);
            objDbCommand.AddInParameter("CompanyId", objLoan.CompanyId);
            objDbCommand.AddInParameter("COAId", objLoan.COAId);

            objDbCommand.AddInParameter("MonthlyInstallmentAmount", objLoan.MonthlyInstallmentAmount);
            objDbCommand.AddInParameter("MonthlyDbr", objLoan.MonthlyDbr);
            objDbCommand.AddInParameter("MonthlyInstallmentNumber", objLoan.MonthlyInstallmentNumber);
            objDbCommand.AddInParameter("WeeklyInstallmentAmount", objLoan.WeeklyInstallmentAmount);
            objDbCommand.AddInParameter("WeeklyDbr", objLoan.WeeklyDbr);
            objDbCommand.AddInParameter("WeeklyInstallmentNumber", objLoan.WeeklyInstallmentNumber);
            objDbCommand.AddInParameter("Equity", objLoan.Equity);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Loan_ApproveLoanInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public LoanModel GetExpiryDate(LoanModel objLoan)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            //   List<MonthlyIncome> objMonthlyIncomeList = new List<MonthlyIncome>();
            LoanModel obj = new LoanModel();
            try
            {
                objDbCommand.AddInParameter("NumberOfInstallment", objLoan.NumberOfInstallment);
                objDbCommand.AddInParameter("LoanDate", objLoan.LoanDate);
                objDbCommand.AddInParameter("InstallmentTypeId", objLoan.InstallmentTypeId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Loan_GetExpireDate]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        obj.LoanExpiryDate = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(objDbDataReader["FinalExpiryDate"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return obj;
        }

    }
}