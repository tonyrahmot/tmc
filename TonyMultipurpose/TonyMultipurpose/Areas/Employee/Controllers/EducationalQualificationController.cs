﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class EducationalQualificationController : Controller
    {
        // GET: Employee/Education
        

        [HttpGet]
        public ActionResult Save()
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
            ViewBag.employeeName = EmployeeInfo;
            return View();
        }

        [HttpPost]

        public ActionResult Save(EducationalQualification objEducationalQualification)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                objEducationalQualificationBLL.SaveEducationalQualification(objEducationalQualification);
                status = true;
            }
            return new JsonResult { Data = new { status = status, JsonRequestBehavior.AllowGet } };
        }

        public JsonResult GetAllEmployeeNameById(int employeeId)
        {
            try
            {
                EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                if (employeeId > 0)
                {
                    List<EducationalQualification> employee = objEducationalQualificationBLL.GetAllEmployeeNameById(employeeId);
                    return Json(employee, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
                    return Json(EmployeeInfo, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                ViewBag.message = ex.Message;
            }
            
            return Json(JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            var educationalQualification = objEducationalQualificationBLL.GetAllEducationalQualification(id);
            return View(educationalQualification);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EducationalQualification objEducationalQualification)
        {
            if (ModelState.IsValid)
            {
                EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                objEducationalQualificationBLL.UpadateEducationalQualification(objEducationalQualification);
            }
            return RedirectToAction("Save", "EducationalQualification");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            var educationalQualification = objEducationalQualificationBLL.GetAllEducationalQualification(id);
            return View(educationalQualification);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteEducationalQualification(int id)
        {
            if (ModelState.IsValid)
            {
                EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                objEducationalQualificationBLL.DeleteEducationalQualification(id);
            }
            return RedirectToAction("Save", "EducationalQualification");

        }
    }
}