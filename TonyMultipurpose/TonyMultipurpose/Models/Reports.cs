﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Models
{
    public class Reports
    {  
        public string ReportType { get; set; }
        
        public string ReportName { get; set; }

        public string CustomerId { get; set; }

        public string AccountTypeId { get; set; }

        public int AccountType { get; set; }

        public string AccountNumber { get; set; }

        public string BranchId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public decimal? Amount { get; set; }

        public bool IsActive { get; set; }

        public string LoanId { get; set; }
        public string LoanNo { get; set; }

        public string AccountCode { get; set; }

        public string ReportFormat { get; set; }
    }
}