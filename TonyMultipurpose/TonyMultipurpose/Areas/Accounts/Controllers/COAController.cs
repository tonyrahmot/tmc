﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Accounts.Controllers
{
    [AuthenticationFilter]
    public class COAController : Controller
    {
        // GET: Accounts/COA
        COABLL objCOABLL = new COABLL();
       COA objCOA = new COA();
        public ActionResult Index()
        {
            //List<COA> objCOAList = new List<COA>();
            //objCOAList = objCOABLL.GetCOAList();
            //return View(objCOAList);

            List<ChartofAccountsViewModel> objCOAList = new List<ChartofAccountsViewModel>();
            var masterAccounts = objCOABLL.GetCOAList();
            var childs = objCOABLL.GetSubAccounts();
            foreach (var i in masterAccounts)
            {
                var od = childs.Where(a => a.SubCodeId.Equals(i.COAId)).ToList();
                objCOAList.Add(new ChartofAccountsViewModel { masterAccount = i, subAccounts = od });
            }
            return View(objCOAList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            
            AccountGroupBLL oAccountGroupBLL = new AccountGroupBLL();
            var model = new COA
            {
                AccountGroups = oAccountGroupBLL.GetAllAccountGroupInfo()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(COA objCOA)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objCOA.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objCOA.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objCOA.CreatedDate = DateTime.Now;
                objCOABLL.SaveCOAInfo(objCOA);
                status = true;
            }
            else
            {
                return View(objCOA);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "COA");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            AccountGroupBLL oAccountGroupBLL = new AccountGroupBLL();
            COACategoryBLL objCOACategoryBLL = new COACategoryBLL();
            var getCOA = objCOABLL.GetCOAInfo(id);
            getCOA.AccountGroups = oAccountGroupBLL.GetAllAccountGroupInfo();
            //getCOA.COACategoryInfo = objCOABLL.GetCategoryNameGroupWise();
            //GetBalanceSheetNameByGroupAndCategoryId(string groupId, string categoryName)
            
            
            

            return View(getCOA);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(COA objCOA)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objCOA.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objCOA.UpdatedDate = DateTime.Now;
                objCOABLL.UpdateCOAInfo(objCOA);
                status = true;
            }
            else
            {
                return View(objCOA);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "COA");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var getCOA = objCOABLL.GetCOAInfo(id);
            return View(getCOA);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteBranch(int id)
        {
            objCOABLL.DeleteCOA(id);
            return RedirectToAction("Index", "COA");
        }

        public JsonResult GetCategoryNameGroupWise(string groupId)
        {
            var result = objCOABLL.GetCategoryNameGroupWise(groupId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubCodeCategoryNameGroupWise(string groupId,string categoryName)
        {
            var result = objCOABLL.GetSubCodeCategoryNameGroupWise(groupId, categoryName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetBalanceSheetNameByGroupAndCategoryId(string groupId, string categoryName)
        {
            var result = objCOABLL.GetBalanceSheetNameByGroupAndCategoryId(groupId, categoryName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccountNameCheck(string accountName)
        {
            bool exsits = objCOABLL.GetAccountNameCheck(accountName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetAccountCodeCheck(string accountCode)
        {
            bool exsits = objCOABLL.GetAccountCodeCheck(accountCode);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        


    }
}