﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.Models
{
    public class COACategory:CommonModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        public string GroupId { get; set; }
        public IEnumerable<AccountGroupModel> AccountGroupInfo { get; set; }
    }
}