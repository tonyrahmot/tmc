﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    public class ClientInfoController : Controller
    {
        ClientInfoBLL objClientInfoBLL = new ClientInfoBLL();
        CommonResult oCommonResult = new CommonResult();
        public JsonResult Get_COAaccountCodeInfo()
        {
            AccountBLL objAccountBll = new AccountBLL();
            var data = objAccountBll.Get_COAaccountCodeInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ClientInfoSave()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ClientInfoSave(ClientInfo objClientInfo)
        {
            oCommonResult = new CommonResult();
            objClientInfoBLL = new ClientInfoBLL();
            oCommonResult = objClientInfoBLL.ClientInfoSave(objClientInfo);
            return Json(new { data = oCommonResult }, JsonRequestBehavior.AllowGet);
        }

    }
}