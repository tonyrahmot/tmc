﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
    [AuthenticationFilter]
    public class LoanClosingController : Controller
    {
        CommonResult oCommonResult;
        LoanModel objLoan;
        LoanBLL objLoanBLL;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetClosingLoanInfo()
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = true;
            var loans = objLoanBLL.GetAllLoanInfoForClosing(objLoan);
            return Json(new { data = loans }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CloseLoan(string loanNo)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.CloseLoan(loanNo);
            return RedirectToAction("Index");
            //return new JsonResult { Data = new { oCommonResult } };
        }
        public void PrintLoanStatusReport(string loanNo)
        {
            ReportObject.ReportName = "LoanAccountWiseInstallmentReport";
            ReportObject.LoanId = loanNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }
    }
}