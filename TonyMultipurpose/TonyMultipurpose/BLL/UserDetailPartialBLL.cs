﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using System.Text;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.BLL
{
    //created by ataur
    public class UserDetailPartialBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForUserDetailPartial(DbDataReader objDataReader, UserDetailPartial objUserDetailPartial)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "UserDetailId":
                        if (!Convert.IsDBNull(objDataReader["UserDetailId"]))
                        {
                            objUserDetailPartial.UserDetailId = Convert.ToByte(objDataReader["UserDetailId"]);
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objUserDetailPartial.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objUserDetailPartial.BranchId = Convert.ToInt16(objDataReader["BranchId"]);
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objUserDetailPartial.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objUserDetailPartial.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objUserDetailPartial.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objUserDetailPartial.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objUserDetailPartial.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objUserDetailPartial.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objUserDetailPartial.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objUserDetailPartial.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objUserDetailPartial.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objUserDetailPartial.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objUserDetailPartial.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //Get UserInfo by UserId for Edit
        public UserDetailPartial GetUserInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            UserDetailPartial objUserDetailPartial = new UserDetailPartial();
            List<UserDetailPartial> objUserInfoPartialList = new List<UserDetailPartial>();

            try
            {
                objDbCommand.AddInParameter("UserDetailId", id);
                //objDbCommand.AddInParameter("AdminUserId", SessionUtility.STSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetUserInfoForEdit]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objUserDetailPartial = new UserDetailPartial();
                        this.BuildModelForUserDetailPartial(objDbDataReader, objUserDetailPartial);

                        objUserInfoPartialList.Add(objUserDetailPartial);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objUserDetailPartial;
        }

        // Update Specific UserInfo
        public string UpdateUserInfo(UserDetailPartial objUserDetailPartial)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("UserDetailId", objUserDetailPartial.UserDetailId);
            objDbCommand.AddInParameter("Username", objUserDetailPartial.Username);
            objDbCommand.AddInParameter("IsActive", objUserDetailPartial.IsActive);
            objDbCommand.AddInParameter("BranchId", objUserDetailPartial.BranchId);
            objDbCommand.AddInParameter("CompanyId", objUserDetailPartial.CompanyId);
            objDbCommand.AddInParameter("UpdatedBy", objUserDetailPartial.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", objUserDetailPartial.UpdatedDate);

            //updated by which Admin!
            //objDbCommand.AddInParameter("UpdatedByAdminUserId", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateUserInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //Delete Specific UserInfo
        public string DeleteUserInfo(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("UserDetailId", id);
            objDbCommand.AddInParameter("IsDeleted", 1);
            objDbCommand.AddInParameter("UpdatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            objDbCommand.AddInParameter("UpdatedDate", DateTime.Now);
            //objDbCommand.AddInParameter("CreatedBy", objAdminUser.CreatedBy);         

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteUserInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string ChangeUserPassword(UserDetailPartial objUserDetailPartial)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("UserDetailId", objUserDetailPartial.UserDetailId);
            objDbCommand.AddInParameter("Password", SHA512PasswordGenerator(objUserDetailPartial.Password));
            objDbCommand.AddInParameter("UpdatedBy", objUserDetailPartial.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", objUserDetailPartial.UpdatedDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspChangeUserPassword]", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }


        //Encrypt Password using SHA512 Algorithm
        private string SHA512PasswordGenerator(string strInput)
        {
            SHA512 sha512 = new SHA512CryptoServiceProvider();
            byte[] arrHash = sha512.ComputeHash(Encoding.UTF8.GetBytes(strInput));
            StringBuilder sbHash = new StringBuilder();
            for (int i = 0; i < arrHash.Length; i++)
            {
                sbHash.Append(arrHash[i].ToString("x2"));
            }
            return sbHash.ToString();
        }
    }
}