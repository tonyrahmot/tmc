﻿
    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "CSS/MatureEncashment/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });
$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});

//load data
$("#AccountNumber").change(function() {
    var AccountNumber = $("#AccountNumber").val();
    var json = {
        AccountNumber: AccountNumber
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "CSS/PreMatureEncashment/GetPreMatureAccountInfoByAccountNo",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function(data) {

            $('#CustomerName').val(data.CustomerName);
            $('#CustomerId').val(data.CustomerId);
            $('#InterestRate').val(data.InterestRate);
            $('#InstallmentNumber').val(data.InstallmentNumber);
            $('#AccruedBalance').val(data.AccruedBalance);
            $('#Balance').val(data.Balance);
            $('#CustomerImage').attr('src', data.CustomerImage);
            $('#hdfAccruedWithBalance').val(parseFloat(data.AccruedBalance) + parseFloat(data.Balance));


            //--calculate company benefit
            var customerBalance = parseFloat($('#Balance').val());
            var interestRate = parseFloat($('#InterestRate').val()) / 1200;
            var preMatureBalance = customerBalance * interestRate * data.InstallmentNumber + customerBalance;
            $('#PreMatureAmount').val(preMatureBalance);
            var companyAccruedAmount = (parseFloat($('#hdfAccruedWithBalance').val()) - parseFloat($('#PreMatureAmount').val()));
            $('#hdfCompanyBenefit').val(parseFloat(companyAccruedAmount));

        }
    });
});


//--when interest rate will be changed
$("#InterestRate").keyup(function() {
    var customerBalance = parseFloat($('#Balance').val());
    var interestRate = parseFloat($('#InterestRate').val()) / 1200;
    var duration = parseInt($('#InstallmentNumber').val());
    var preMatureBalance = customerBalance * interestRate * duration + customerBalance;
    $('#PreMatureAmount').val(preMatureBalance);
    var companyAccruedAmount = (parseFloat($('#hdfAccruedWithBalance').val()) - parseFloat($('#PreMatureAmount').val()));
    $('#hdfCompanyBenefit').val(parseFloat(companyAccruedAmount));

});


//--after withdraw button click

$('#submit').click(function() {

    var isAllDataValid = true;

    if ($('#AccountNumber').val() == '') {
        $('#AccountNumber').siblings('span.error').css('visibility', 'visible');
        isAllDataValid = false;
    } else {
        $('#AccountNumber').siblings('span.error').css('visibility', 'hidden');
    }

    if ($('#InterestRate').val() == '') {
        $('#InterestRate').siblings('span.error').css('visibility', 'visible');
        isAllDataValid = false;
    } else {
        $('#InterestRate').siblings('span.error').css('visibility', 'hidden');
    }

    if ($('#PreMatureAmount').val() == '') {
        $('#PreMatureAmount').siblings('span.error').css('visibility', 'visible');
        isAllDataValid = false;
    } else {
        $('#PreMatureAmount').siblings('span.error').css('visibility', 'hidden');
    }
    if ($('#ChartOfAccount').val().trim() == 0) {
        $('#ChartOfAccount').siblings('span.error').css('visibility', 'visible');
        isAllDataValid = false;
    } else {
        $('#ChartOfAccount').siblings('span.error').css('visibility', 'hidden');
    }

    if (isAllDataValid) {

        var data = {
            AccountNumber: $('#AccountNumber').val(),
            CustomerName: $('#CustomerName').val(),
            CustomerId: $('#CustomerId').val(),
            InterestRate: $('#InterestRate').val(),
            Balance: parseFloat($('#Balance').val()),
            InstallmentNumber: $('#InstallmentNumber').val(),
            PreMatureAmount: $('#PreMatureAmount').val(),
            CompanyAmount: $('#hdfCompanyBenefit').val(),
            COAId: $('#ChartOfAccount option:selected').val()
        }

        $.ajax({
            url: window.applicationBaseUrl + "CSS/PreMatureEncashment/Save",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function(d) {
                if (d.status === true) {
                    alert('Successfully done.');

                    $("#AccountNumber").val('');
                    $("#CustomerName").val('');
                    $("#CustomerId").val('');
                    $("#InterestRate").val('');
                    $("#InstallmentNumber").val('');
                    $('#PreMatureAmount').val('');
                    $('#hdfCompanyBenefit').val('');
                    $('#AccruedBalance').val('');
                    $('#Balance').val('');
                    $('#ChartOfAccount').val('0');

                } else {
                    alert('Failed');
                }
                $('#submit').val('Save');
                window.location.reload();

            },
            error: function() {
                alert('Error. Please try again.');
                //$('#submit').val('Save');
            }
        });

    }


});


        function IsDouble(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                return false;
            } else if (charCode == 13) {
                return false;
            }
            status = "";
            return true;
        }
  