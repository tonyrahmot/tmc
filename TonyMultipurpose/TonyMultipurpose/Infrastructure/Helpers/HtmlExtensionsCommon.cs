﻿using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsCommon
    {
        public static void AddName(TagBuilder tb,
      string name, string id)
        {

            if (!string.IsNullOrWhiteSpace(name))
            {
                name = TagBuilder.CreateSanitizedId(name);

                if (string.IsNullOrWhiteSpace(id))
                {
                    tb.GenerateId(name);
                }
                else
                {
                    tb.MergeAttribute("id",
                      TagBuilder.CreateSanitizedId(id));
                }
            }

            tb.MergeAttribute("name", name);
        }
    }
}