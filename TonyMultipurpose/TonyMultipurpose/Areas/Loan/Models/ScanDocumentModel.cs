﻿using System;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class ScanDocumentModel:CommonModel
    {
        public int Id { get; set; }
        public string LoanId { get; set; }
        public string DocumentName { get; set; }
        public string ImagePath { get; set; }

        public string OldImagePath { get; set; }
        public HttpPostedFileBase Imagefile { get; set; }
    }
}