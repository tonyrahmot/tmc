﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.BLL
{
    //--ataur
    public class RoleManagementBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForAssignUserRole(DbDataReader objDataReader, AssignUserRole objAssignUserRole)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {

                    case "UrmId":
                        if (!Convert.IsDBNull(objDataReader["UrmId"]))
                        {
                            objAssignUserRole.UrmId = Convert.ToInt32(objDataReader["UrmId"]);
                        }
                        break;
                    case "RoleId":
                        if (!Convert.IsDBNull(objDataReader["RoleId"]))
                        {
                            objAssignUserRole.RoleId = Convert.ToByte(objDataReader["RoleId"]);
                        }
                        break;
                    case "RoleName":
                        if (!Convert.IsDBNull(objDataReader["RoleName"]))
                        {
                            objAssignUserRole.RoleName = objDataReader["RoleName"].ToString();
                        }
                        break;

                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objAssignUserRole.BranchId = Convert.ToInt16(objDataReader["BranchId"].ToString());
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objAssignUserRole.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "UserDetailId":
                        if (!Convert.IsDBNull(objDataReader["UserDetailId"]))
                        {
                            objAssignUserRole.UserDetailId = Convert.ToInt16(objDataReader["UserDetailId"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objAssignUserRole.Username = objDataReader["Username"].ToString();
                        }
                        break;


                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objAssignUserRole.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objAssignUserRole.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objAssignUserRole.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objAssignUserRole.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objAssignUserRole.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objAssignUserRole.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;

                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objAssignUserRole.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }



        /// <summary>
        /// get all user role information for index
        /// </summary>
        /// <returns></returns>
        public List<AssignUserRole> GetUserRoleMappingInfo(string searchQuery = "")
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AssignUserRole> objAssignUserRoleList = new List<AssignUserRole>();
            AssignUserRole objAssignUserRole;

            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);
                objDbCommand.AddInParameter("SearchString", searchQuery);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllUserAssignRoleInfo", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAssignUserRole = new AssignUserRole();
                        this.BuildModelForAssignUserRole(objDbDataReader, objAssignUserRole);
                        objAssignUserRoleList.Add(objAssignUserRole);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAssignUserRoleList;
        }

  
        /// <summary>
        /// save user and brach wise role 
        /// </summary>
       /// <returns></returns>
        public string AssignUserRoleInfo(AssignUserRole objAssignUserRole)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("UserDetailId", objAssignUserRole.UserDetailId);
            objDbCommand.AddInParameter("RoleId", objAssignUserRole.RoleId);
            objDbCommand.AddInParameter("BranchId", objAssignUserRole.BranchId);
            objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspAssignUserRoleInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }


        /// <summary>
        /// get assign user role info by UrmId for edit 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ///  
        public AssignUserRole GetAssignRoleInfoById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            //List<AssignUserRole> objUserList = new List<AssignUserRole>();
            AssignUserRole objAssignUserRole = new AssignUserRole();

            try
            {
                objDbCommand.AddInParameter("UrmId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetUserAssignRoleInfoById", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAssignUserRole = new AssignUserRole();

                        BuildModelForAssignUserRole(objDbDataReader, objAssignUserRole);

                        //objUserList.Add(objAssignUserRole);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAssignUserRole;
        }


        /// <summary>
        /// Update assign user role information
        /// </summary>
        /// <param name="objAssignUserRole"></param>
        /// <returns></returns>
        public string UpdateAssignUserRole(AssignUserRole objAssignUserRole)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("UrmId", objAssignUserRole.UrmId);
            objDbCommand.AddInParameter("UserDetailId", objAssignUserRole.UserDetailId);
            objDbCommand.AddInParameter("RoleId", objAssignUserRole.RoleId);
            objDbCommand.AddInParameter("BranchId", objAssignUserRole.BranchId);
            objDbCommand.AddInParameter("IsActive", objAssignUserRole.IsActive);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateAssignUserRole", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<RoleInfo> GetRoleListForDropDownForSave()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<RoleInfo> objRoleList = new List<RoleInfo>();
            RoleInfo objRoleInfo;

            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetRoleInfoList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objRoleInfo = new RoleInfo();

                        objRoleInfo.RoleId = Convert.ToByte(objDbDataReader["RoleId"]);
                        objRoleInfo.RoleName = objDbDataReader["RoleName"].ToString();

                        objRoleList.Add(objRoleInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objRoleList;
        }

        public string CreateRoleMenuMapping(int menuId, int roleId)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("MenuId", menuId);
            objDbCommand.AddInParameter("RoleId", roleId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateRoleMenuMapping", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string ResetRoleMenuMapping(int roleId)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("RoleId", roleId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspResetRoleMenuMapping", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string CreateUserMenuMapping(int item, int roleId, int userId)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("MenuId", item);
            objDbCommand.AddInParameter("RoleId", roleId);
            objDbCommand.AddInParameter("UserId", userId);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateUserMenuMapping", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string ResetUserMenuMapping(int roleId, int userId)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("RoleId", roleId);
            objDbCommand.AddInParameter("UserId", userId);
            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspResetUserMenuMapping", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<RoleInfo> GetRoleByUserId(int userDetailId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<RoleInfo> objList = new List<RoleInfo>();
            RoleInfo obj= new RoleInfo();
            try
            {
                objDbCommand.AddInParameter("UserDetailId", userDetailId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetRoleByUserId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        obj = new RoleInfo();
                        if (objDbDataReader["RoleId"] != null)
                        {
                            obj.RoleId = Convert.ToInt16(objDbDataReader["RoleId"]);
                            obj.RoleName = Convert.ToString(objDbDataReader["RoleName"]);
                            objList.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objList;
        }
    }
}