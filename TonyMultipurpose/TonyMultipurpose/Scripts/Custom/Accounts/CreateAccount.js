﻿$(function () {
    $("#tabs").tabs();
});


var nominees = [];
var position, name, mobile, nominee, $row;
(function () {
    $('#NomineeDateOfBirth').datepicker({
        dateFormat: 'dd/M/yy', changeMonth: true,
        changeYear: true,
        yearRange: "-60:+60"
    }).click(function () { $(this).focus(); });
    $('#OpeningDate').datepicker({
        dateFormat: 'dd/M/yy', changeMonth: true,
        changeYear: true,
        yearRange: "-60:+60"
    }).click(function () { $(this).focus(); });
    $('#edit').hide();


    $(function () {
        $("#AccountType").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/GetAccontTypeByAccountTitle",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#AccountType").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
                });
            }
        });
    });

    // hide show on selected AccountType
    $("#AccountType").change(function () {

        $("#InterestRate").val('');
        $("#DepositAmount").val('');
        //$('#CustomerId').val('');
        $('#COA_accountName').val('0');
        $('#Duration').val('');
        $('#DurationYear').val('');
        $('#MatureDate').val('');
        $('#MaturedAmount').val('');
        $('#InterestAmount').val('');
        $('#InterestAmountM').val('');
        $('#Selectduration').val('');


        var AccountType = $("#AccountType").val();
        if (AccountType === '1') {
            $("#COA_ACName").show();
            // $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType === '2') {
            $("#COA_ACName").show();
            //$("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }
        else if (AccountType === '3') {
            $("#COA_ACName").show();
            //$("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType === '4') {
            // fixed deposit
            $("#COA_ACName").show();
            // $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").show();
            $("#IAmountM").hide();
            $('#InterestRate').removeAttr("readonly");
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").show();
            $("#DurationY").hide();
            $("#DurationSelect").show();

            $("#Selectduration").change(function () {
                var duration = parseInt($("#Selectduration option:selected").text());
                var dt = new Date();
                var day = dt.getDate();
                var Month = dt.getMonth() + 1 + duration;
                var year = dt.getFullYear();
                //(year);
                var totalMonth = Month;
                if (totalMonth > 12) {
                    year = year + 1
                    Month = totalMonth - 12
                }
                var mm = monthNumToName(Month);
                var date = day + '/' + mm + '/' + year;
                $('#MatureDate').val(date);
                duration = parseInt($("#Selectduration option:selected").text());
                var depositAmount = parseFloat($("#DepositAmount").val());
                var interestrate = parseFloat($('#InterestRate').val()) / 1200;
                var total = depositAmount * interestrate * duration;
                $('#InterestAmount').val(total);
            });

            $("#DepositAmount").keyup(function () {
                var duration = parseInt($("#Selectduration option:selected").text());
                var depositAmount = parseFloat($("#DepositAmount").val());
                var interestrate = parseFloat($('#InterestRate').val()) / 1200;
                var total = depositAmount * interestrate * duration;
                $('#InterestAmount').val(total);
            });

            $("#InterestRate").keyup(function () {
                var duration = parseInt($("#Selectduration option:selected").text());
                var depositAmount = parseFloat($("#DepositAmount").val());
                var interestrate = parseFloat($('#InterestRate').val()) / 1200;
                var total = depositAmount * interestrate * duration;
                $('#InterestAmount').val(total);
            });
        }

        else if (AccountType === '5') {
            // Double Benefit
            $("#COA_ACName").show();
            // $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").show();
            $("#DurationM").show();
            $("#MaturedDate").show();
            $("#DurationY").show();
            $("#DurationSelect").hide();

            $("#DurationYear").keyup(function () {

                var durationYear = parseFloat($("#DurationYear").val());
                var month = durationYear * 12;
                $('#Duration').val(month);
                var years = parseInt(durationYear);
                var dt = new Date();
                var day = dt.getDate();
                var Month = dt.getMonth() + 1;
                var year = dt.getFullYear() + years;
                var afterYear = durationYear - years;
                var tMonth = afterYear * 12;
                var totalMonth = Month + tMonth;
                if (totalMonth > 12) {
                    year = year + 1
                    Month = totalMonth - 12
                }
                var mm = monthNumToName(Month);
                var date = day + '/' + mm + '/' + year;
                $('#MatureDate').val(date);

                var IR = 100 / durationYear;
                var interestRate = IR.toFixed(2);
                $('#InterestRate').val(interestRate);

            });

            $("#DepositAmount").keyup(function () {

                var depositAmount = parseFloat($("#DepositAmount").val()) * 2;
                $('#MaturedAmount').val(parseFloat(depositAmount));
            });
        }

        else if (AccountType === '6') {
            // Monthly Benefit

            $("#InterestRate").val('');


            $("#COA_ACName").show();
            // $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").show();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").show();
            $("#MaturedDate").show();
            $("#DurationY").show();
            $("#DurationSelect").hide();

            $("#InterestRate").keyup(function () {
                var depositAmount = $("#DepositAmount").val();
                var interestrate = parseFloat($('#InterestRate').val() / 100);
                var interestAmount = parseInt(depositAmount * interestrate / 12);
                $('#InterestAmountM').val(interestAmount);
            });

            $("#DepositAmount").keyup(function () {
                var depositAmount = $("#DepositAmount").val();
                var interestrate = parseFloat($('#InterestRate').val() / 100);
                var interestAmount = depositAmount * interestrate / 12;
                $('#InterestAmountM').val(interestAmount);
            });

            $("#DurationYear").keyup(function () {

                var durationYear = parseInt($("#DurationYear").val());
                var month = durationYear * 12;
                $('#Duration').val(month);
                var dt = new Date();
                var day = dt.getDate();
                var month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var Year = year + durationYear;
                var mm = monthNumToName(month);
                var date = day + '/' + mm + '/' + Year;
                $('#MatureDate').val(date);
            });

            $("#InterestAmountM").keyup(function () {
                var depositAmount = $("#DepositAmount").val();
                var interestAmount = $('#InterestAmountM').val();
                var IR = interestAmount * 12 * 100 / depositAmount;
                var interestRate = IR.toFixed(2);
                //alert(interestAmount);
                $('#InterestRate').val(interestRate);
            });

        }

        else if (AccountType === '7') {
            $("#COA_ACName").show();
            /// $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType === '8') {
            $("#COA_ACName").show();
            //$("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType === '9') {

            $("#COA_ACName").show();
            //$("#COA_ACNameCompany").show();
            $("#IRate").hide();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType === '10') {
            // CSS
            $("#COA_ACName").show();
            //$("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $('#InterestRate').removeAttr("readonly");
            $("#DAmount").show();
            $("#MatureAmount").show();
            $("#DurationM").show();
            $("#DurationY").show();
            $("#MaturedDate").show();
            $("#DurationSelect").hide();

            $("#DurationYear").keyup(function () {
                var DurationYear = parseInt($("#DurationYear").val());
                var month = DurationYear * 12;
                $('#Duration').val(month);
                var dt = new Date();
                var day = dt.getDate();
                var Month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var Year = year + DurationYear;
                var mm = monthNumToName(Month);
                var date = day + '/' + mm + '/' + Year;
                $('#MatureDate').val(date);

                var Durationmonth = parseInt($("#Duration").val());
                var monthlyAmount = parseFloat($("#DepositAmount").val());
                var Interestrate = parseFloat($('#InterestRate').val()) / 100;
                var total = monthlyAmount;
                for (var i = 1; i < Durationmonth; i++) {
                    total += total * (Interestrate / 12) + monthlyAmount;
                }
                $('#MaturedAmount').val(parseInt(total));
            });

            $("#InterestRate").keyup(function () {
                var DurationYear = parseInt($("#DurationYear").val());
                var month = DurationYear * 12;
                $('#Duration').val(month);
                var dt = new Date();
                var day = dt.getDate();
                var Month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var Year = year + DurationYear;
                var mm = monthNumToName(Month);
                var date = day + '/' + mm + '/' + Year;
                $('#MatureDate').val(date);

                var Durationmonth = parseInt($("#Duration").val());
                var monthlyAmount = parseFloat($("#DepositAmount").val());
                var Interestrate = parseFloat($('#InterestRate').val()) / 100;
                var total = monthlyAmount;
                for (var i = 1; i < Durationmonth; i++) {
                    total += total * (Interestrate / 12) + monthlyAmount;
                }
                $('#MaturedAmount').val(parseInt(total));
            });

            $("#DepositAmount").keyup(function () {
                var DurationYear = parseInt($("#DurationYear").val());
                var month = DurationYear * 12;
                $('#Duration').val(month);
                var dt = new Date();
                var day = dt.getDate();
                var Month = dt.getMonth() + 1;
                var year = dt.getFullYear();
                var Year = year + DurationYear;
                var mm = monthNumToName(Month);
                var date = day + '/' + mm + '/' + Year;
                $('#MatureDate').val(date);

                var Durationmonth = parseInt($("#Duration").val());
                var monthlyAmount = parseFloat($("#DepositAmount").val());
                var Interestrate = parseFloat($('#InterestRate').val()) / 100;
                var total = monthlyAmount;
                for (var i = 1; i < Durationmonth; i++) {
                    total += total * (Interestrate / 12) + monthlyAmount;
                }
                $('#MaturedAmount').val(parseInt(total));
            });
        }

        else if (AccountType === '11') {
            $("#COA_ACName").show();
            // $("#COA_ACNameCompany").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $('#InterestRate').attr('readonly', 'readonly');
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else {
            $("#COA_ACName").hide();
            // $("#COA_ACNameCompany").hide();
            $("#IRate").hide();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").hide();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();


        }

    });


    //Load COA code
    $(function () {
        var AccountTitle = $("#AccountTitle").val();
        $("#COA_accountName").empty();
        var json = {
            AccountTitle: AccountTitle
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/Get_COAaccountCodeInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $("#COA_accountName").append('<option value="0">--Select Code--</option>');
                $.each(data, function (key, value) {
                    //$("#COA_accountName").append('<option value=' + value.AccountCode + '>' + value.AccountName + '</option>');
                    $("#COA_accountName").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });
    $(function () {
        $("#ChartOfAccount").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });



    //Load interest rate
    $("#AccountType").change(function () {
        var AccountType = $("#AccountType").val();
        var json = {
            AccountType: AccountType
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/GetInterestRateByAccountType",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#InterestRate').val(data.InterestRate);
            }
        });
    });

    //customer id autocomplete
    $("#CustomerId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Accounts/Account/AutoCompleteCustomerId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Accounts/Account/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    //Load Account Info
    $("#AccountNumber").change(function () {
        var id = $("#AccountNumber").val();
        var url = window.applicationBaseUrl + "Accounts/Account/Edit?id=" + id;
        window.location.href = url;
    });

    //Load Customer Information
    $("#CustomerId").change(function () {
        ClearCustomerData();
        var customerId = $("#CustomerId").val();
        var json = { customerId: customerId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/GetCustomerInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                BindCustomerData(data);
            }
        });
    });

    //var images=[], nomineePositions = [];


    $('#add').click(function () {
        if (nominees.length === 2) {
            alert('Maximun two nominees can be added');
            return;
        }
        var duplicatePosition = false;
        if (nominees.length > 0) {
            $.each(nominees, function () {
                if (this.NomineePosition === $('#NomineePosition').val().trim()) {
                    alert('There is already a nominee at ' + this.NomineePosition + ' position');
                    duplicatePosition = true;
                    return;
                }
            });
        }
        if (duplicatePosition === true) {
            return false;
        }

        var isValidNominee = CheckNomineeFieldsValidation();

        //Add item to list if valid
        if (isValidNominee) {
            var imageStatus = null;
            var file = document.getElementById('fileUpload').files[0];
            if (file != null) {
                imageStatus = 'True';
            }
            nominees.push({
                NomineePosition: $("#NomineePosition option:selected").val(),
                NomineeName: $('#NomineeName').val().trim(),
                NomineeFatherName: $('#NomineeFatherName').val().trim(),
                NomineeMotherName: $('#NomineeMotherName').val().trim(),
                NomineeSpouseName: $('#NomineeSpouseName').val().trim(),
                NomineeNationality: $('#NomineeNationality').val().trim(),
                NomineeNationalId: $('#NomineeNationalId').val().trim(),
                NomineeDateOfBirth: $('#NomineeDateOfBirth').val().trim(),
                NomineeMobile: $('#NomineeMobile').val().trim(),
                NomineeOccupation: $('#NomineeOccupation').val().trim(),
                NomineeDesignation: $('#NomineeDesignation').val().trim(),
                Relation: $('#Relation').val().trim(),
                NomineeAddress: $('#NomineeAddress').val().trim(),
                Imagefile: file,
                NomineeImage: imageStatus
            });
            //populate order items
            GenerateNomineeTable(nominees);
            ClearNomineeFields();
        }
    });

    $('#clear').click(function () {
        ClearNomineeFields();
    });

    $('#edit').click(function () {
        var duplicatePosition = false;
        if (nominees.length > 0) {
            $.each(nominees, function () {
                if (this.NomineePosition === $('#NomineePosition').val().trim() && this.NomineePosition != nominee[0].NomineePosition) {
                    alert('There is already a nominee at ' + this.NomineePosition + ' position');
                    duplicatePosition = true;
                    return;
                }
            });
        }
        if (duplicatePosition === true) {
            return false;
        }
        var isValidNominee = CheckNomineeFieldsValidation();
        if (isValidNominee) {
            var file = document.getElementById('fileUpload').files[0];
            if (nominee != null) {
                $.each(nominees, function () {
                    if (this.NomineeName === nominee[0].NomineeName && this.NomineeMobile === nominee[0].NomineeMobile) {
                        this.NomineePosition = $("#NomineePosition option:selected").val(),
                        this.NomineeName = $('#NomineeName').val().trim(),
                        this.NomineeFatherName = $('#NomineeFatherName').val().trim(),
                        this.NomineeMotherName = $('#NomineeMotherName').val().trim(),
                        this.NomineeSpouseName = $('#NomineeSpouseName').val().trim(),
                        this.NomineeNationality = $('#NomineeNationality').val().trim(),
                        this.NomineeNationalId = $('#NomineeNationalId').val().trim(),
                        this.NomineeDateOfBirth = $('#NomineeDateOfBirth').val().trim(),
                        this.NomineeMobile = $('#NomineeMobile').val().trim(),
                        this.NomineeOccupation = $('#NomineeOccupation').val().trim(),
                        this.NomineeDesignation = $('#NomineeDesignation').val().trim(),
                        this.Relation = $('#Relation').val().trim(),
                        this.NomineeAddress = $('#NomineeAddress').val().trim()
                    }
                    if (this.NomineeName === nominee[0].NomineeName && this.NomineeMobile === nominee[0].NomineeMobile && file != null) {
                        this.Imagefile = file;
                        this.NomineeImage = 'True';
                    }
                });
            }
            //populate order items
            GenerateNomineeTable(nominees);
            ClearNomineeFields();
        }
    });

    //Save Account Info
    $('#submit').click(function () {
        var isValid = CheckValidation(nominees);
        var accNo = $('#AccountNumber').val();
        if (isValid) {
            if (accNo === '****<<< New Entry >>>****') {
                SaveData();
            } else if (accNo === null || accNo === '') {
                document.getElementById('showErrorMessage').innerHTML = 'Press New Button or Insert Account No.';
            }
            else {
                UpdateData();
            }
        }
    });
    $("#fileUpload").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imageDisplay').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $("#btnNewEntry").on("click", function () {
        location.reload();
    });
    $("#btnFind").on("click", function () {
        clearFields();
        makeFieldsReadOnly();
        $('#AccountNumber').removeAttr('readonly');
        $('#AccountNumber').focus();
        $('#CustomerId').attr('readonly', 'readonly');
    });
})();

function SaveData() {
    var data = bindData();
    console.log(data);
    $.ajax({
        url: window.applicationBaseUrl + "Accounts/Account/Save",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (d) {
            if (d.status === true) {
                $('#AccountNumber').val(d.oCommonResult.Id);
                $('#AccountNumber').focus();
                if (d.nominees != null) {
                    d.nominees.forEach(function (item) {
                        if (item.NomineeImage != null) {
                            saveNomineeImage(item);
                        }
                    });
                }
                makeFieldsReadOnly();
                document.getElementById('showErrorMessage').style.color = "green";
                //clearFields();
                //alert('Successfully done.');

            }
            else {
                clearFields();
                $('#AccountNumber').val('****<<< New Entry >>>****');
            }
            //$('#showErrorMessage').val(d.oCommonResult.Message);
            if (d.oCommonResult.Message === 'This customer already have a savings account.') {
                document.getElementById('showErrorMessage').innerHTML = d.oCommonResult.Message;
            } else {
                alert("Successfully Saved AccountNumber = " + d.oCommonResult.Id);
            }


        },
        error: function () {
            document.getElementById('showErrorMessage').innerHTML = 'Error. Please try again.';
            //alert('Error. Please try again.');
        }

    });
}

function UpdateData() {
    var data = {
        AccountNumber: $('#AccountNumber').val(),
        CustomerId: $('#CustomerId').val(),
        AccountType: $('#AccountType option:selected').val(),
        SpecialInstructions: $('#SpecialInstructions').val(),
        Nominees: nominees
    }

    $.ajax({
        url: window.applicationBaseUrl + "Accounts/Account/Edit",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (d) {
            if (d.status === true) {
                if (d.nominees != null) {
                    d.nominees.forEach(function (item) {
                        if (item.NomineeImage != null) {
                            saveNomineeImage(item);
                        }
                    });
                }
                makeFieldsReadOnly();
                //alert('Successfully done.');
                document.getElementById('showErrorMessage').style.color = "green";
            } else {
                //alert('Failed');
            }
            //$('#submit').val('Save');
            document.getElementById('showErrorMessage').innerHTML = d.oCommonResult.Message;
        },
        error: function () {
            document.getElementById('showErrorMessage').innerHTML = 'Error. Please try again.';
        }
    });
}

function saveNomineeImage(item) {
    nominees.forEach(function (i) {
        if (item.NomineePosition === i.NomineePosition) {
            var fd = new FormData();
            fd.append('Path', item.NomineeImage);
            fd.append('file', i.Imagefile);

            $.ajax({
                url: window.applicationBaseUrl + "Accounts/Account/SaveNomineeImageFile",
                type: "POST",
                data: fd,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (obj) {
                    if (obj.status === true) {
                        return true;
                    } else {
                        return false;
                        //alert("failed");
                    }
                },
                error: function () {
                    //alert('Error. Please try again.');
                }
            });
        }
    });
}

function bindData() {
    var data = {
        CustomerId: $('#CustomerId').val(),
        AccountNumber: $('#hiddenAccountNumber').val(),
        AccountType: $('#AccountType option:selected').val(),
        AccountCode: $('#COA_accountName option:selected').val(),
        COAId: $('#ChartOfAccount option:selected').val(),
        SpecialInstructions: $('#SpecialInstructions').val(),
        InterestRate: $('#InterestRate').val(),
        DepositAmount: $('#DepositAmount').val(),
        OpeningDate: $('#OpeningDate').val(),
        DurationYear: $('#DurationYear').val(),
        MonthlyBenefit: $('#InterestAmountM').val(),
        InterestAmount: $('#InterestAmount').val(),
        MatureDate: $('#MatureDate').val(),
        MatureAmount: $('#MaturedAmount').val(),
        Nominees: nominees
    }
    if ($('#AccountType').val() === '4') {
        data.Duration = $('#Selectduration option:selected').text();
    }
    else {
        data.Duration = $('#Duration').val();
    }
    return data;
}

function makeFieldsReadOnly() {
    $('#AccountNumber').attr('readonly', 'readonly');
    $('#CustomerId').attr('readonly', 'readonly');
    $('#AccountType').attr('readonly', 'readonly');
    $('#COA_accountName').attr('readonly', 'readonly');
    $('#ChartOfAccount').attr('readonly', 'readonly');
    $('#InterestRate').attr('readonly', 'readonly');
    $('#DepositAmount').attr('readonly', 'readonly');
    $('#DurationYear').attr('readonly', 'readonly');
    $('#Duration').attr('readonly', 'readonly');
    $('#Selectduration').attr('readonly', 'readonly');
    $('#MatureDate').attr('readonly', 'readonly');
    $('#InterestAmountM').attr('readonly', 'readonly');
    $('#InterestAmount').attr('readonly', 'readonly');
    $('#MaturedAmount').attr('readonly', 'readonly');
}

function clearFields() {
    $('#AccountNumber').val('');
    $('#imageDisplay').attr('src', '');
    $("#AccountTitle").val('');
    $("#AccountType").val('0');
    $("#InterestRate").val('');
    $("#DepositAmount").val('');
    $('#CustomerId').val('');
    $('#COA_accountName').val('0');
    $('#Duration').val('');
    $('#DurationYear').val('');
    $('#MatureDate').val('');
    $('#MaturedAmount').val('');
    $('#InterestAmount').val(''),
    $('#Selectduration').val('')
    $('#InterestAmountM').val(''),
    nominees = [];
    ClearCustomerData();
    ClearNomineeFields();
    $('#Nominees').empty();
    //$('#CustomerId').removeAttr('readonly');
}

function ClearCustomerData() {
    $('#CustomerName').val('');
    $('#FatherName').val('');
    $('#MotherName').val('');
    $('#SpouseName').val('');
    $('#DateOfBirth').val('');
    $('#Nationality').val('');
    $('#NationalId').val('');
    $('#PassportNo').val('');
    $('#PassportExpireDate').val('');
    $('#OccupationAndPosition').val('');
    $('#CustomerIncomeSource').val('');
    $('#PresentAddress').val('');
    $('#PermanentAddress').val('');
    $('#Office').val('');
    $('#Home').val('');
    $('#Mobile').val('');
    $('#customerImage').attr('src', '');

    $('#CustomerInfo').hide();
    document.getElementById('LabelCustomerName').innerHTML = '';
    document.getElementById('LabelFatherName').innerHTML = '';
    document.getElementById('LabelMotherName').innerHTML = '';
    document.getElementById('LabelSpouseName').innerHTML = '';
    document.getElementById('LabelDateOfBirth').innerHTML = '';
    document.getElementById('LabelNationality').innerHTML = '';
    document.getElementById('LabelNationalId').innerHTML = '';
    document.getElementById('LabelOccupationAndPosition').innerHTML = '';
    document.getElementById('LabelPresentAddress').innerHTML = '';
    document.getElementById('LabelPermanentAddress').innerHTML = '';
    document.getElementById('LabelHome').innerHTML = '';
    document.getElementById('LabelMobile').innerHTML = ''
}

function ClearNomineeFields() {
    $('#add').show();
    $('#edit').hide();
    $('.nomineeFields').val('');
    $('#imageDisplay').attr('src', '');
}

function BindCustomerData(data) {
    $('#CustomerName').val(data.CustomerName);
    $('#FatherName').val(data.FatherName);
    $('#MotherName').val(data.MotherName);
    $('#SpouseName').val(data.SpouseName);
    $('#DateOfBirth').val(FormateDate(data.DateOfBirth));
    $('#Nationality').val(data.Nationality);
    $('#NationalId').val(data.NationalId);
    $('#PassportNo').val(data.PassportNo);
    $('#PassportExpireDate').val(data.PassportExpireDate === null ? "" : FormateDate(data.PassportExpireDate));
    $('#OccupationAndPosition').val(data.OccupationAndPosition);
    $('#CustomerIncomeSource').val(data.CustomerIncomeSource);
    $('#PresentAddress').val(data.PresentAddress);
    $('#PermanentAddress').val(data.PermanentAddress);
    $('#Office').val(data.Office);
    $('#Home').val(data.Home);
    $('#Mobile').val(data.Mobile);
    $('#customerImage').attr('src', data.CustomerImage);
    //// added by Nasir
    $('#CustomerInfo').show();
    document.getElementById('LabelCustomerName').innerHTML = $('#CustomerName').val();
    document.getElementById('LabelFatherName').innerHTML = $('#FatherName').val();
    document.getElementById('LabelMotherName').innerHTML = $('#MotherName').val();
    document.getElementById('LabelSpouseName').innerHTML = $('#SpouseName').val();
    document.getElementById('LabelDateOfBirth').innerHTML = $('#DateOfBirth').val();
    document.getElementById('LabelNationality').innerHTML = $('#Nationality').val();
    document.getElementById('LabelNationalId').innerHTML = $('#NationalId').val();
    //document.getElementById('LabelPassportNo').innerHTML = $('#PassportNo').val();
    //document.getElementById('LabelPassportExpireDate').innerHTML = $('#PassportExpireDate').val();
    document.getElementById('LabelOccupationAndPosition').innerHTML = $('#OccupationAndPosition').val();
    //document.getElementById('LabelCustomerIncomeSource').innerHTML = $('#CustomerIncomeSource').val();
    document.getElementById('LabelPresentAddress').innerHTML = $('#PresentAddress').val();
    document.getElementById('LabelPermanentAddress').innerHTML = $('#PermanentAddress').val();
    //document.getElementById('LabelOffice').innerHTML = $('#Office').val();
    document.getElementById('LabelHome').innerHTML = $('#Home').val();
    document.getElementById('LabelMobile').innerHTML = $('#Mobile').val();
}

var months = [
'Jan', 'Feb', 'Mar', 'Apr', 'May',
'Jun', 'Jul', 'Aug', 'Sep',
'Oct', 'Nov', 'Dec'
];

function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}

function FormateDate(jsonDate) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

//Check validaion for nominee
function CheckNomineeFieldsValidation() {
    var isValidNominee = true;
    if ($('#NomineePosition').val().trim() === '') {
        isValidNominee = false;
        $('#NomineePosition').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineePosition').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeFatherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeFatherName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeFatherName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeMotherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMotherName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeMotherName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeNationality').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeNationality').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeNationality').parent().next().find('span').css('visibility', 'hidden');
    }
    //if ($('#NomineeNationalId').val().trim() === '') {
    //    isValidNominee = false;
    //    $('#NomineeNationalId').parent().next().find('span').css('visibility', 'visible');
    //}
    if ($('#NomineeDateOfBirth').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeDateOfBirth').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeDateOfBirth').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeMobile').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        isValidNominee = mobileNumber($('#NomineeMobile').val());
    }
    if ($('#NomineeAddress').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeAddress').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeAddress').parent().next().find('span').css('visibility', 'hidden');
    }
    //if ($('#fileUpload').val().trim() === '') {
    //    isValidNominee = false;
    //    $('#fileUpload').parent().next().find('span').css('visibility', 'visible');
    //}
    return isValidNominee;
}

//Check validaion for Save
function CheckValidation(nominees) {
    var isValid = true;
    var AccountType = $("#AccountType").val();
    if ($('#AccountNumber').val().trim() === '') {
        isValid = false;
        document.getElementById('showErrorMessage').innerHTML = 'Press New Button or Insert Account No.';
    }
    else {
        document.getElementById('showErrorMessage').innerHTML = '';
    }
    if ($('#AccountType').val().trim() === '0') {
        isValid = false;
        $('#AccountType').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#AccountType').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#InterestRate').val().trim() === '') {
        isValid = false;
        $('#InterestRate').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#InterestRate').parent().next().find('span').css('visibility', 'hidden');
    }
    //if (AccountType != '5' || AccountType != '6') {
    //    if ($('#InterestRate').val().trim() === '') {
    //        isValid = false;
    //        $('#InterestRate').parent().next().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#InterestRate').parent().next().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#DurationYear').val().trim() === '') {
    //        isValid = false;
    //        $('#DurationYear').parent().next().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#DurationYear').parent().next().find('span').css('visibility', 'hidden');
    //    }
    //    if ($('#Duration').val().trim() === '') {
    //        isValid = false;
    //        $('#Duration').parent().next().find('span').css('visibility', 'visible');
    //    }
    //    else {
    //        $('#Duration').parent().next().find('span').css('visibility', 'hidden');
    //    }
    //}
    if (AccountType === '4') {
        var a = $('#Selectduration').val();
        if ($('#Selectduration').val().trim() === '') {
            isValid = false;
            $('#Selectduration').parent().next().find('span').css('visibility', 'visible');
        }
        else {
            $('#Selectduration').parent().next().find('span').css('visibility', 'hidden');
        }
    }
    if (AccountType === '5' || AccountType === '6' || AccountType === '10') {

        if ($('#DurationYear').val().trim() === '') {
            isValid = false;
            $('#DurationYear').parent().next().find('span').css('visibility', 'visible');
        }
        else {
            $('#DurationYear').parent().next().find('span').css('visibility', 'hidden');
        }
        if ($('#Duration').val().trim() === '') {
            isValid = false;
            $('#Duration').parent().next().find('span').css('visibility', 'visible');
        }
        else {
            $('#Duration').parent().next().find('span').css('visibility', 'hidden');
        }
    }

    if ($('#DepositAmount').val().trim() === '') {
        isValid = false;
        $('#DepositAmount').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#DepositAmount').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#COA_accountName').val() === '0') {
        isValid = false;
        $('#COA_accountName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#COA_accountName').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#ChartOfAccount').val() === '0') {
        isValid = false;
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#CustomerId').val().trim() === '') {
        isValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#DateOfBirth').val().trim() === '') {
        isValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#OpeningDate').val().trim() === '') {
        isValid = false;
        $('#OpeningDate').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#OpeningDate').parent().next().find('span').css('visibility', 'hidden');
    }
    //if (nominees.length === 0) {
    //    isValid = false;
    //    alert('Add atleast one nominee');
    //}
    return isValid;
}

//function for show added items in table
function GenerateNomineeTable(Nominees) {
    if (Nominees.length > 0) {
        var $table = $('<table id="myTable" class="table table-striped table-hover" width="100%"/>');
        $table.append('<thead><tr class="success">' +
            '<th>Position</th>' +
            '<th>Name</th>' +
            '<th>Date of Birth</th>' +
            '<th>Mobile</th>' +
            '<th>Address</th>' +
            '<th>Edit</th>' +
            '<th>Delete</th>' +
            '</tr></thead>');

        var $tbody = $('<tbody/>');
        $.each(Nominees, function (i, val) {
            var $row = $('<tr class="active"/>');
            $row.append($('<td id="Position"/>').html(val.NomineePosition));
            $row.append($('<td id="Name"/>').html(val.NomineeName));
            $row.append($('<td id="DoB"/>').html(val.NomineeDateOfBirth));
            $row.append($('<td id="Mobile"/>').html(val.NomineeMobile));
            $row.append($('<td id="Address"/>').html(val.NomineeAddress));
            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='LoadNominee(this)'>" +
                "<span class='glyphicon glyphicon-pencil'></span>" +
                "</button>"));

            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='DeleteNominee(this)'>" +
                "<span class='glyphicon glyphicon-remove'></span>" +
                "</button>"));
            $tbody.append($row);
        });

        $table.append($tbody);

        $('#Nominees').html($table);
    }
}

//Remove Nominee
function DeleteNominee(btn) {
    $row = $(btn).closest("tr");
    position = $row
       .find("#Position")
       .text();
    name = $row
        .find("#Name")
        .text();

    mobile = $row
        .find("#Mobile")
        .text();
    $row.remove();
    nominees = nominees.filter(function (item) {
        return (item.NomineeName !== name) && (item.NomineePosition !== position) && (item.NomineeMobile !== mobile);
    });
    ClearNomineeFields();
    //console.log(nominees);
}

function LoadNominee(btn) {
    $row = $(btn).closest("tr");
    position = $row
       .find("#Position")
       .text();
    name = $row
        .find("#Name")
        .text();
    mobile = $row
        .find("#Mobile")
        .text();
    nominee = nominees.filter(function (item) {
        return (item.NomineeName === name) && (item.NomineePosition === position) && (item.NomineeMobile === mobile);
    });
    if (nominee[0].NomineeName != null) {
        //console.log(nominee[0]);
        $('#NomineePosition').val(nominee[0].NomineePosition);
        $('#NomineeName').val(nominee[0].NomineeName);
        $('#NomineeFatherName').val(nominee[0].NomineeFatherName);
        $('#NomineeMotherName').val(nominee[0].NomineeMotherName);
        $('#NomineeSpouseName').val(nominee[0].NomineeSpouseName);
        $('#NomineeNationality').val(nominee[0].NomineeNationality);
        $('#NomineeNationalId').val(nominee[0].NomineeNationalId);
        $('#NomineeDateOfBirth').val(nominee[0].NomineeDateOfBirth);
        $('#NomineeMobile').val(nominee[0].NomineeMobile);
        $('#NomineeOccupation').val(nominee[0].NomineeOccupation);
        $('#NomineeDesignation').val(nominee[0].NomineeDesignation);
        $('#Relation').val(nominee[0].Relation);
        $('#NomineeAddress').val(nominee[0].NomineeAddress);
        //('#fileUpload').val(nominee[0].Imagefile);
        if (nominee[0].Imagefile != null) {
            readURL(nominee[0].Imagefile);
        }
        $('#add').hide();
        $('#edit').show();
    }
}
function readURL(file) {
    if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageDisplay').attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    }
}
function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode === 13) {
        return false;
    }
    status = "";
    return true;
}

function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode === 13 || charCode === 46) {
        return false;
    }
    status = "";
    return true;
}

function mobileNumber(inputtxt) {
    var mobileNo = /^(?:\+88|01)?\d{11}$/;
    if (inputtxt.match(mobileNo)) {
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'hidden');
        return true;
    }
    else {
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'visible'); //alert("Not a valid Mobile Number");
        return false;
    }
}