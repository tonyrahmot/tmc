$(function () {
    document.getElementById('showMessage').innerHTML = '';
});

$(function () {
    $('#UpdateChequeInfo').hide();
    $('#deleteChequeInfo').hide();
    var oChequeInfoTable;
    LoadChequeInfoTable();
    function LoadChequeInfoTable() {
        oChequeInfoTable = $('#ChequeInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllLoanCustomerChequeInfo",
                "type": "get",
                "data": {
                    "LoanId": $('#ChequeInfo_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "className": "dt-center"
                },
                { "data": "AccountName" },
                { "data": "AccountNo" },
                { "data": "ChequeNo" },
                { "data": "BankName" }
            ],
            "order": [[0, "asc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#ChequeInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oChequeInfoTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataChequeInfo(oChequeInfoTable.row(this).data());
        });
    }

    oChequeInfoTable.on('order.dt search.dt', function () {
        oChequeInfoTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    function bindDataChequeInfo(data) {
        $('#hdfLoanCustomerChequeInfoId').val(data.LoanCustomerChequeInfoId);
        $('#ChequeInfo_AccountName').val(data.AccountName);
        $('#ChequeInfo_AccountNo').val(data.AccountNo);
        $('#ChequeInfo_ChequeNo').val(data.ChequeNo);
        $('#ChequeInfo_BankName').val(data.BankName);
        $('#saveChequeInfo').show();
        //$('#UpdateChequeInfo').show();
        $('#deleteChequeInfo').show();
    }
    // caler function
    $('#ChequeInfo_Clear').click(function () {
        ChequeInfoClearAll();
    });
    function ChequeInfoClearAll() {
        $('#ChequeInfo_AccountName').val('');
        $('#ChequeInfo_AccountNo').val('');
        $('#ChequeInfo_ChequeNo').val('');
        $('#ChequeInfo_BankName').val('');
        $('#saveChequeInfo').show();
        $('#deleteChequeInfo').hide();
        //$('#UpdateChequeInfo').hide();
        document.getElementById('showMessage').innerHTML = '';
    }
});