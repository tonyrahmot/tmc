﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class GuarantorViewModel
    {
        [Key]
        public int GuarantorId { get; set; }

        [Required]
        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }

        [Display(Name = "Guarantor Roll")]
        public string GuarantorRoll { get; set; }

        [Display(Name = "Guarantor Name")]
        public string GuarantorName { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }

        [Display(Name = "Mother's Name")]
        public string MotherName { get; set; }

        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }

        [Display(Name = "Relation with Applicant")]
        public string RelationWithApplicant { get; set; }

        [Display(Name = "Date of Birth")]
        public string DateofBirth { get; set; }

        public string Occupation { get; set; }

        public string Designation { get; set; }

        [Display(Name = "Institute Name")]
        public string InstituteName { get; set; }

        [Display(Name = "Institute / Business Address")]
        public string InstituteAddress { get; set; }

        [Required]
        [Display(Name = "Monthly Income")]
        public decimal? MonthlyIncome { get; set; }

        [Display(Name = "Present Address")]
        public string CurrentAddress { get; set; }

        [Display(Name = "Residential Condition")]
        public byte? ResidentialConditionId { get; set; }

        [Display(Name = "Permanent Address")]
        public string PermanentAddress { get; set; }

        [Display(Name = "Mobile No")]
        public string ContactNo { get; set; }

        [Display(Name = "NID No")]
        public string NID { get; set; }

        [Display(Name = "Loan Status with TMC")]
        public string LoanStatusWithTmc { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }

        public string ImagePath { get; set; }

        public string OldImagePath { get; set; }

        public IEnumerable<SelectListItem> GuarantorRollSelectListItems { get; set; }
        public IEnumerable<SelectListItem> ResidentialConditionSelectListItems { get; set; }

    }
}
