﻿using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class AddtionalInfoViewModel : CommonModel
    {
        public int Id { get; set; }
        public string LoanId { get; set; }
        public string CustomerDetail { get; set; }
        public string GuarantorsDetail { get; set; }
        public string Deviation { get; set; }
        public string ApproverComment { get; set; }
       
    }
}