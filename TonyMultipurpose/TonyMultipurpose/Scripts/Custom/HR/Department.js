﻿(function () {

    $('#submit1').hide();
    departmentTable();

    var oTable;
    //============  Bind Department Table =========================//
    function departmentTable() {
        oTable = $('#DepartmentTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveDepartment",//'@Url.Action("SaveDepartment", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "DepartmentName" },
                { "data": "DeptCode" }
            ],
            "order": [[0, "desc"]]
        });
        $('#DepartmentTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }
    // ========== Get Data From Department Table ==============
    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        $('#DepartmentName').val(data.DepartmentName);
        $('#DeptCode').val(data.DeptCode);
        $('#hdfDeptId').val(data.DeptId);
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    // ======= Save data in Department Table ======
    $('#submit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                DepartmentName: $('#DepartmentName').val(),
                DeptCode: $('#DeptCode').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveDepartment",//'@Url.Action("SaveDepartment", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#DepartmentName').val('');
                        $('#DeptCode').val('');
                        $('#hdfDeptId').val(d.oCommonResult.DeptId);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    // ========= Update data in Department Table =======
    $('#submit1').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                DepartmentName: $('#DepartmentName').val(),
                DeptCode: $('#DeptCode').val(),
                DeptId: $('#hdfDeptId').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateDepartment",//'@Url.Action("UpdateDepartment", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#DepartmentName').val('');
                        $('#DeptCode').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanId').val('');
                }
            });
        }
    });

    // ============ Required Field Check Before Save & Update Data in Depertment Table =====
    function checkValidation() {
        var isValid = true;
        if ($('#DeptCode').val().trim() === '') {
            isValid = false;
            $('#DeptCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DeptCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#DepartmentName').val().trim() === '') {
            isValid = false;
            $('#DepartmentName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DepartmentName').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    // ======== Duplicate Check Department Name ===========
    $("#DepartmentName").keyup(function () {
        var departmentName = $("#DepartmentName").val();
        var status = $("#showMessage");
        var user = $.trim(departmentName);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/HR/GetDepartmentNameCheck", { departmentName: departmentName },//'@Url.Action("GetDepartmentNameCheck", "HR", new {Area = "HR" })', { departmentName: departmentName },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + departmentName + "</b>' is not exist !</font>");
                                $('#submit').attr('disabled', false);
                                $('#submit1').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + departmentName + "</b>' is exist !</font>");
                                $('#submit').attr('disabled', true);
                                $('#submit1').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }
    });

    // ==== Duplicate Check Department Code =====
    $("#DeptCode").keyup(function () {
        var deptCode = $("#DeptCode").val();
        var status = $("#showMessage");
        var user = $.trim(deptCode);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/HR/GetDepartmentCodeCheck", { deptCode: deptCode },//'@Url.Action("GetDepartmentCodeCheck", "HR", new {Area = "HR" })', { deptCode: deptCode },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + deptCode + "</b>' is not exist !</font>");
                                $('#submit').attr('disabled', false);
                                $('#submit1').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + deptCode + "</b>' is exist !</font>");
                                $('#submit').attr('disabled', true);
                                $('#submit1').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }
    });
})();