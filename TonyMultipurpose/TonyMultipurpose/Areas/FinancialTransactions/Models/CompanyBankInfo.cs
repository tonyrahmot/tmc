﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class CompanyBankInfo : CommonModel
    {
        [Key]
        public int BankId { get; set; }

        [Required]
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Required]
        [Display(Name = "Bank Account No")]
        public string BankAccountNo { get; set; }

        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }
        [Display(Name = "COA Code")]
        [Required]
        public int COAId { get; set; }


        [Display(Name = "Account Code")]
        public string AccountCode { get; set; }


        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        public IEnumerable<COA> CoaInfo { get; set; }
    }
}