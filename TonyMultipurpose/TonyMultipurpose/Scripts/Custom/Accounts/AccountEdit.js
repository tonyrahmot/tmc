﻿
       $(function () {
           $("#tabs").tabs();
       });

    var nominees = [];
var position, name, mobile, nominee, $row;
(function () {
    $('#NomineeDateOfBirth').datepicker({ dateFormat: 'dd/M/yy', changeMonth: true,
        changeYear: true,
        yearRange: "-60:+60" }).click(function () { $(this).focus(); });
    $('#edit').hide();


    //Load Account Type Dropdown
    $("#AccountTitle").change(function () {
        var AccountTitle = $("#AccountTitle").val();
        $("#AccountType").empty();
        var json = {
            AccountTitle: AccountTitle
        };
        $.ajax({
            type: "POST",
            url:window.applicationBaseUrl + "Accounts/Account/GetAccontTypeByAccountTitle",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $("#AccountType").append('<option value="0">Select Type</option>');
                $.each(data, function (key, value) {
                    $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
                });
                $("#AccountType").val(window.accountType);
            }
        });
    });

    if ($('#AccountTitle').val() !== '') {
        $('#AccountTitle').change();
    }

    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url:window.applicationBaseUrl + "Accounts/Account/AutoCompleteAccountNumber",// '@Url.Action("AutoCompleteAccountNumber", "Account", new { Area = "Accounts" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    //Load Account Info
    $("#AccountNumber").change(function () {
        var id = $("#AccountNumber").val();
        var url = window.applicationBaseUrl + "Accounts/Account/Edit/"+id;//@Url.Action("Edit", "Account", new { Area = "Accounts" })?id=" + id;
        window.location.href = url;
    });

    //customer id autocomplete
    $("#CustomerId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url:window.applicationBaseUrl + "Accounts/Account/AutoCompleteCustomerId",// "~/Accounts/Account/AutoCompleteCustomerId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    //Load Customer Information
    $("#CustomerId").change(function () {
        ClearCustomerData();
        var customerId = $("#CustomerId").val();
        var json = { customerId: customerId };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Accounts/Account/GetCustomerInfo",// '@Url.Action("GetCustomerInfo", "Account", new { Area = "Accounts" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                BindCustomerData(data);
                $("#AccountType").change();
            }
        });
    });

    if ($('#CustomerId').val() !== '') {
        $('#CustomerId').change();
    }

    $("#AccountType").change(function () {
        var  duration = $('#Duration').val()/12;
        $('#DurationYear').val(duration);
        var AccountType = $("#AccountType").val();
        if (AccountType == '1') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '2') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }
        else if (AccountType == '3') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '4') {
            // fixed deposit
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").show();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").show();
            $("#DurationY").hide();
            $("#DurationSelect").show();
        }
        else if (AccountType == '5') {
            // Double Benefit
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").show();
            $("#DurationM").show();
            $("#MaturedDate").show();
            $("#DurationY").show();
            $("#DurationSelect").hide();
        }
        else if (AccountType == '6') {
            // Monthly Benefit
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").show();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").show();
            $("#MaturedDate").show();
            $("#DurationY").show();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '7') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '8') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '9') {

            $("#COA_ACName").show();
            $("#IRate").hide();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else if (AccountType == '10') {
            // CSS
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#DAmount").show();
            $("#MatureAmount").show();
            $("#DurationM").show();
            $("#DurationY").show();
            $("#MaturedDate").show();
            $("#DurationSelect").hide();


        }

        else if (AccountType == '11') {
            $("#COA_ACName").show();
            $("#IRate").show();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").show();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

        else {
            $("#COA_ACName").hide();
            $("#IRate").hide();
            $("#IAmount").hide();
            $("#IAmountM").hide();
            $("#DAmount").hide();
            $("#MatureAmount").hide();
            $("#DurationM").hide();
            $("#MaturedDate").hide();
            $("#DurationY").hide();
            $("#DurationSelect").hide();
        }

    });

    //Add Nominees to Nominee Table
    var existingNominees = window.existingNominees;
    if (existingNominees != null) {
        $.each(existingNominees, function(i,item) {
            var dob = item.NomineeDateOfBirth;
            if (dob.indexOf('Date') > -1) {
                dob = FormateDate(dob);
            }
            existingNominees[i].NomineeDateOfBirth = dob;
            existingNominees[i].NewImage = false;
        });
        nominees = existingNominees;
        GenerateNomineeTable(nominees);
    }

    $('#add').click(function () {
        if (nominees.length == 2) {
            alert('Maximun two nominees can be added');
            return;
        }
        var duplicatePosition = false;
        if (nominees.length > 0) {
            $.each(nominees, function () {
                if (this.NomineePosition === $('#NomineePosition').val().trim()) {
                    alert('There is already a nominee at ' + this.NomineePosition + ' position');
                    duplicatePosition = true;
                    return;
                }
            });
        }
        if (duplicatePosition == true) {
            return false;
        }

        var isValidNominee = CheckNomineeFieldsValidation();

        //Add item to list if valid
        if (isValidNominee) {
            var imageStatus = null;
            var newImage = false;
            var file = document.getElementById('fileUpload').files[0];
            if (file != null) {
                imageStatus = 'True';
                newImage = true;
            }
            nominees.push({
                NomineePosition: $("#NomineePosition option:selected").val(),
                NomineeName: $('#NomineeName').val().trim(),
                NomineeFatherName: $('#NomineeFatherName').val().trim(),
                NomineeMotherName: $('#NomineeMotherName').val().trim(),
                NomineeSpouseName: $('#NomineeSpouseName').val().trim(),
                NomineeNationality: $('#NomineeNationality').val().trim(),
                NomineeNationalId: $('#NomineeNationalId').val().trim(),
                NomineeDateOfBirth: $('#NomineeDateOfBirth').val().trim(),
                NomineeMobile: $('#NomineeMobile').val().trim(),
                NomineeOccupation: $('#NomineeOccupation').val().trim(),
                NomineeDesignation: $('#NomineeDesignation').val().trim(),
                NomineeAddress: $('#NomineeAddress').val().trim(),
                Imagefile: file,
                NomineeImage: imageStatus,
                NewImage : newImage
            });
            //populate order items
            GenerateNomineeTable(nominees);
            ClearNomineeFields();
        }
    });

    $('#clear').click(function () {
        ClearNomineeFields();
    });

    $('#edit').click(function () {
        var duplicatePosition = false;
        if (nominees.length > 0) {
            $.each(nominees, function () {
                if (this.NomineePosition === $('#NomineePosition').val().trim() && this.NomineePosition != nominee[0].NomineePosition) {
                    alert('There is already a nominee at ' + this.NomineePosition + ' position');
                    duplicatePosition = true;
                    return;
                }
            });
        }
        if (duplicatePosition == true) {
            return false;
        }
        var isValidNominee = CheckNomineeFieldsValidation();
        if (isValidNominee) {
            var file = document.getElementById('fileUpload').files[0];
            if (nominee != null) {
                $.each(nominees, function () {
                    if (this.NomineeName === nominee[0].NomineeName && this.NomineeMobile === nominee[0].NomineeMobile) {
                        this.NomineePosition = $("#NomineePosition option:selected").val(),
                        this.NomineeName = $('#NomineeName').val().trim(),
                        this.NomineeFatherName = $('#NomineeFatherName').val().trim(),
                        this.NomineeMotherName = $('#NomineeMotherName').val().trim(),
                        this.NomineeSpouseName = $('#NomineeSpouseName').val().trim(),
                        this.NomineeNationality = $('#NomineeNationality').val().trim(),
                        this.NomineeNationalId = $('#NomineeNationalId').val().trim(),
                        this.NomineeDateOfBirth = $('#NomineeDateOfBirth').val().trim(),
                        this.NomineeMobile = $('#NomineeMobile').val().trim(),
                        this.NomineeOccupation = $('#NomineeOccupation').val().trim(),
                        this.NomineeDesignation = $('#NomineeDesignation').val().trim(),
                        this.NomineeAddress = $('#NomineeAddress').val().trim()
                    }
                    if (this.NomineeName === nominee[0].NomineeName && this.NomineeMobile === nominee[0].NomineeMobile && file != null) {
                        this.Imagefile = file;
                        this.NomineeImage = 'True';
                        this.NewImage = true;
                    }
                });
            }
            //populate order items
            GenerateNomineeTable(nominees);
            ClearNomineeFields();
            $('#add').show();
            $('#edit').hide();
        }
    });

    //Save Account Info
    $('#submit').click(function () {
        var isValid = CheckValidation(nominees);
        if (isValid) {
            var data = {
                AccountNumber: $('#AccountNumber').val(),
                CustomerId: $('#CustomerId').val(),
                AccountType: $('#AccountType option:selected').val(),
                SpecialInstructions:$('#SpecialInstructions').val(),
                Nominees: nominees
            }

            $.ajax({
                url: window.applicationBaseUrl + "Accounts/Account/Edit",// '@Url.Action("Edit", "Account", new { Area = "Accounts" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function(d) {
                    if (d.status === true) {
                        if(d.nominees != null){
                            d.nominees.forEach(function (item) {
                                if (item.NomineeImage != null && item.NewImage == true) {
                                    saveNomineeImage(item);
                                }
                            });
                        }
                        $('#AccountNumber').attr('readonly', 'readonly');
                        alert('Successfully done.');

                    } else {
                        alert('Failed');
                    }
                },
                error: function() {
                    alert('Error. Please try again.');
                }
            });
        };
    });

    function saveNomineeImage(item) {
        nominees.forEach(function (i) {
            if (item.NomineePosition === i.NomineePosition) {
                var fd = new FormData();
                fd.append('Path', item.NomineeImage);
                fd.append('file', i.Imagefile);
                $.ajax({
                    url: window.applicationBaseUrl + "Accounts/Account/SaveNomineeImageFile",// '@Url.Action("SaveNomineeImageFile", "Account", new { Area = "Accounts" })',
                    type: "POST",
                    data: fd,
                    dataType: "JSON",
                    contentType: false,
                    processData: false,
                    success: function (obj) {
                        if (obj.status === true) {
                            return true;
                        } else {
                            alert("failed");
                        }
                    },
                    error: function () {
                        alert('Error. Please try again.');
                    }
                });
            }
        });
    }

    $("#fileUpload").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imageDisplay').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    $("#btnNewEntry").on("click", function () {
        window.location.href = window.applicationBaseUrl + "Accounts/Account/Save"; //'@Url.Action("Save", "Account", new { Area = "Accounts" })';
    });
    $("#btnFind").on("click", function () {
        clearFields();
        $('#AccountNumber').removeAttr('readonly');
        $('#AccountNumber').focus();
    });

})();

function clearFields() {
    $('#AccountNumber').val('');
    $('#imageDisplay').attr('src', '');
    $("#AccountTitle").val('');
    $("#AccountType").val('0');
    $("#InterestRate").val('');
    $("#DepositAmount").val('');
    $('#CustomerId').val('');
    $('#COA_accountName').val('0');
    $('#Duration').val('');
    $('#DurationYear').val('');
    $('#MatureDate').val('');
    $('#MaturedAmount').val('');
    $('#InterestAmount').val(''),
    $('#Selectduration').val('')
    $('#InterestAmountM').val(''),
    nominees = [];
    ClearCustomerData();
    ClearNomineeFields();
    $('#Nominees').empty();
    //$('#CustomerId').removeAttr('readonly');
}
function ClearCustomerData() {
    $('#CustomerName').val('');
    $('#FatherName').val('');
    $('#MotherName').val('');
    $('#SpouseName').val('');
    $('#DateOfBirth').val('');
    $('#Nationality').val('');
    $('#NationalId').val('');
    $('#PassportNo').val('');
    $('#PassportExpireDate').val('');
    $('#OccupationAndPosition').val('');
    $('#CustomerIncomeSource').val('');
    $('#PresentAddress').val('');
    $('#PermanentAddress').val('');
    $('#Office').val('');
    $('#Home').val('');
    $('#Mobile').val
    $('#customerImage').attr('src', '');
}

function ClearNomineeFields() {
    $('#add').show();
    $('#edit').hide();
    $('.nomineeFields').val('');
    $('#imageDisplay').attr('src', '');
}

function BindCustomerData(data) {
    $('#CustomerName').val(data.CustomerName);
    $('#FatherName').val(data.FatherName);
    $('#MotherName').val(data.MotherName);
    $('#SpouseName').val(data.SpouseName);
    $('#DateOfBirth').val(FormateDate(data.DateOfBirth));
    $('#Nationality').val(data.Nationality);
    $('#NationalId').val(data.NationalId);
    $('#PassportNo').val(data.PassportNo);
    $('#PassportExpireDate').val(data.PassportExpireDate == null ? '' : FormateDate(data.PassportExpireDate));
    $('#OccupationAndPosition').val(data.OccupationAndPosition);
    $('#CustomerIncomeSource').val(data.CustomerIncomeSource);
    $('#PresentAddress').val(data.PresentAddress);
    $('#PermanentAddress').val(data.PermanentAddress);
    $('#Office').val(data.Office);
    $('#Home').val(data.Home);
    $('#Mobile').val(data.Mobile);
    $('#customerImage').attr('src', data.CustomerImage);
}

function FormateDate(jsonDate) {
    var monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

//Check validaion for nominee
function CheckNomineeFieldsValidation() {
    var isValidNominee = true;
    if ($('#NomineePosition').val().trim() === '') {
        isValidNominee = false;
        $('#NomineePosition').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineePosition').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeFatherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeFatherName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeFatherName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeMotherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMotherName').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeMotherName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeNationality').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeNationality').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeNationality').parent().next().find('span').css('visibility', 'hidden');
    }
    //if ($('#NomineeNationalId').val().trim() === '') {
    //    isValidNominee = false;
    //    $('#NomineeNationalId').parent().next().find('span').css('visibility', 'visible');
    //}
    if ($('#NomineeDateOfBirth').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeDateOfBirth').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeDateOfBirth').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#NomineeMobile').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        isValidNominee = mobileNumber($('#NomineeMobile').val());
    }
    if ($('#NomineeAddress').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeAddress').parent().next().find('span').css('visibility', 'visible');
    }
    else {
        $('#NomineeAddress').parent().next().find('span').css('visibility', 'hidden');
    }
    //if ($('#fileUpload').val().trim() === '') {
    //    isValidNominee = false;
    //    $('#fileUpload').parent().next().find('span').css('visibility', 'visible');
    //}
    return isValidNominee;
}

//Check validaion for Save
function CheckValidation(nominees) {
    var isValid = true;
    if ($('#AccountNumber').val().trim() === '') {
        isValid = false;
        $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
    }
    else{
        $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#AccountTitle').val().trim() === '') {
        isValid = false;
        $('#AccountTitle').parent().next().find('span').css('visibility', 'visible');
    }
    else{
        $('#AccountTitle').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#AccountType').val().trim() === '0') {
        isValid = false;
        $('#AccountType').parent().next().find('span').css('visibility', 'visible');
    }
    else{
        $('#AccountType').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#CustomerId').val().trim() === '') {
        isValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    }
    else{
        $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#DateOfBirth').val().trim() === '') {
        isValid = false;
        alert('Insert correct account Number and load information');
    }
    //if (nominees.length === 0) {
    //    isValid = false;
    //    alert('Add atleast one nominee');
    //}
    return isValid;
}

//function for show added items in table
function GenerateNomineeTable(Nominees) {
    if (Nominees.length > 0) {
        var $table = $('<table id="myTable" class="table table-striped table-hover" width="100%"/>');
        $table.append('<thead><tr class="success">' +
            '<th>Position</th>' +
            '<th>Name</th>' +
            '<th>Date of Birth</th>' +
            '<th>Mobile</th>' +
            '<th>Address</th>' +
            '<th>Edit</th>' +
            '<th>Delete</th>' +
            '</tr></thead>');

        var $tbody = $('<tbody/>');
        $.each(Nominees, function(i, val) {
            var dob = val.NomineeDateOfBirth;
            //console.log(dob);
            if (dob.indexOf('Date') > -1) {
                dob = FormateDate(val.NomineeDateOfBirth);
            }
            var $row = $('<tr class="active"/>');
            $row.append($('<td id="Position"/>').html(val.NomineePosition));
            $row.append($('<td id="Name"/>').html(val.NomineeName));
            $row.append($('<td id="DoB"/>').html(dob));
            $row.append($('<td id="Mobile"/>').html(val.NomineeMobile));
            $row.append($('<td id="Address"/>').html(val.NomineeAddress));
            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='LoadNominee(this)'>" +
                "<span class='glyphicon glyphicon-pencil'></span>" +
                "</button>"));

            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='DeleteNominee(this)'>" +
                "<span class='glyphicon glyphicon-remove'></span>" +
                "</button>"));
            $tbody.append($row);
        });

        $table.append($tbody);

        $('#Nominees').html($table);
    }
}

//Remove Nominee
function DeleteNominee(btn) {
    //$(btn).parent().parent().remove();
    $row = $(btn).closest("tr");
    position = $row
        .find("#Position")
        .text();

    name = $row
        .find("#Name")
        .text();

    mobile = $row
        .find("#Mobile")
        .text();

    $row.remove();
    nominees = nominees.filter(function(item) {
        return (item.NomineeName !== name) && (item.NomineePosition !== position) && (item.NomineeMobile !== mobile);
    });
}

function LoadNominee(btn) {
    $row = $(btn).closest("tr");
    position = $row
        .find("#Position")
        .text();
    name = $row
        .find("#Name")
        .text();
    mobile = $row
        .find("#Mobile")
        .text();
    nominee = nominees.filter(function(item) {
        return (item.NomineeName == name) && (item.NomineePosition === position) && (item.NomineeMobile == mobile);
    });
    if (nominee[0].NomineeName != null) {
        var dob = nominee[0].NomineeDateOfBirth;
        if (dob.indexOf('Date') > -1) {
            dob = FormateDate(nominee[0].NomineeDateOfBirth);
        }
        $('#NomineePosition').val(nominee[0].NomineePosition);
        $('#NomineeName').val(nominee[0].NomineeName);
        $('#NomineeFatherName').val(nominee[0].NomineeFatherName);
        $('#NomineeMotherName').val(nominee[0].NomineeMotherName);
        $('#NomineeSpouseName').val(nominee[0].NomineeSpouseName);
        $('#NomineeNationality').val(nominee[0].NomineeNationality);
        $('#NomineeNationalId').val(nominee[0].NomineeNationalId);
        $('#NomineeDateOfBirth').val(dob);
        $('#NomineeMobile').val(nominee[0].NomineeMobile);
        $('#NomineeOccupation').val(nominee[0].NomineeOccupation);
        $('#NomineeDesignation').val(nominee[0].NomineeDesignation);
        $('#NomineeAddress').val(nominee[0].NomineeAddress);
        if (nominee[0].Imagefile != null) {
            readURL(nominee[0].Imagefile);
        }
        else if(nominee[0].NomineeImage != null){
            loadImageFromPath(nominee[0].NomineeImage)
        }
        var image = nominee[0].NomineeImage;
        $('#add').hide();
        $('#edit').show();
    }
}

function readURL(file) {
    if (file) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageDisplay').attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    }
}

function loadImageFromPath(url){
    if (url.indexOf("Uploads") !== -1)
    {
        url = url.substring(url.indexOf("Uploads"));
        url = "/" + url;
        $('#imageDisplay').attr('src', url);
    }
}

function mobileNumber(inputtxt) {
    var mobileNo = /^(?:\+88|01)?\d{11}$/;
    if (inputtxt.match(mobileNo)) {
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'hidden');
        return true;
    }
    else {
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'visible'); //alert("Not a valid Mobile Number");
        return false;
    }
}

