﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.ViewModels;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class ArrierModel:ArrierViewModel
    {
        public int ArrierId { get; set; }

        public string EmpCode { get; set; }

        public string ArrierDate { get; set; }

        public decimal ArrierAmount { get; set; }

        public string Description { get; set; }

        public short CompanyId { get; set; }

        public bool IsApproved { get; set; }

        public bool IsRejected { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}