﻿(function () {
    customerTable();
    var oTable;
    function customerTable() {
        oTable = $('#CustomerTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Setup/SMS/CustomersmsemailList",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                {
                    "data": "CustomerId",
                    "render": function (data) {
                        return '<input id="CustomerId" name="CustomerId" type="checkbox"  value = ' + data + '>';
                    }
                },
                { "data": "CustomerName" }
            ],
            "order": [[0, "desc"]]
        });
    }

    $('#example-select-all').on('click', function () {
        // Get all rows with search applied
        var rows = oTable.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });
})();


