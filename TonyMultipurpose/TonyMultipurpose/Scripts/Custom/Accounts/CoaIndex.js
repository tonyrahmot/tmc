﻿$(document).ready(function () {
    var size = $("#main #gridT > thead > tr >th").length; // get total column
    $("#main #gridT > thead > tr >th").last().remove(); // remove last column
    $("#main #gridT > thead > tr").prepend("<th></th>"); // add one column at first for collapsible column
    $("#main #gridT > tbody > tr").each(function (i, el) {
        $(this).prepend(
            $("<td style='width:20px;'></td>")
            .addClass("expand")
            .addClass("hoverEff")
            .attr('title', "click for show/hide")
        );

        //Now get sub table from last column and add this to the next new added row
        var table = $("table", this).parent().html();
        //add new row with this subtable
        $(this).after("<tr><td></td><td style='padding:5px; margin:0px;' colspan='" + (size - 1) + "'>" + table + "</td></tr>");
        $("table", this).parent().remove();
        // ADD CLICK EVENT FOR MAKE COLLAPSIBLE
        $(".hoverEff", this).click(function () {
            $(this).parent().closest("tr").next().slideToggle(100);
            $(this).toggleClass("expand collapse");
        });

    });

    //by default make all subgrid in collapse mode
    $("#main #gridT > tbody > tr td.expand").each(function (i, el) {
        $(this).clearQueue();
        $(this).toggleClass("expand collapse");
        $(this).slideToggle(100);
        $(this).parent().closest("tr").next().slideToggle(100);
    });

});