﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Areas.Setup.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    public class CompanyBankInfoController : Controller
    {
        CompanyBankInfoBLL objBll = new CompanyBankInfoBLL();
        COABLL objCOABLL = new COABLL();
        public ActionResult Index()
        {
            List<CompanyBankInfo> info = objBll.GetAllCompanyBankInfo();

            return View(info);
        }
        [HttpGet]
        public ActionResult Save()
        {
            
            var model = new CompanyBankInfo()
            {
                CoaInfo = objCOABLL.GetCOAList()
            }
            ;          
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(CompanyBankInfo objCompanyBankInfo)
        {
            if (ModelState.IsValid)
            {
                objBll.SaveCompanyBankInfo(objCompanyBankInfo);
            }

            return RedirectToAction("Index", "CompanyBankInfo");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            CompanyBankInfo info = objBll.GetAllCompanyBankInfo().Where(a => a.BankId == id).FirstOrDefault();
            ViewBag.CoaInfo = objCOABLL.GetCOAList();
            return View(info);
        }
        [HttpPost]
        public ActionResult Edit(CompanyBankInfo objCompanyBankInfo)
        {
            if (ModelState.IsValid)
            {
                objBll.UpdateCompanyBankInfo(objCompanyBankInfo);
            }

            return RedirectToAction("Index", "CompanyBankInfo");
        }
        public ActionResult Delete(int id)
        {
            objBll.DeleteCompanyBankInfo(id);
            return RedirectToAction("Index");
        }
    }
}