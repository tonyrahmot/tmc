(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });
    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + 'Loan/ApprovedLoanList/GetApprovedLoanList', 
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "LoanNo" },
                { "data": "AccountTypeName" },
                { "data": "LoanDate" },
                { "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                { "data": "InterestRate" },
                { "data": "InstallmentAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" },
                {
                    "data": "LoanId",
                    "width": "50px",
                    "render": function (data) {
                        return '<a class="btn btn-info" target="_blank" href="' + window.applicationBaseUrl + 'Loan/ApprovedLoanList/GetLoanDetail?loanId=' + data + '">Detail</a>';
                    }
                },
                {
                    "data": "LoanId",
                    "width": "50px",
                    "render": function (data) {
                        return '<a class="btn btn-info" target="_blank" href="' + window.applicationBaseUrl + 'Loan/ApprovedLoanList/PrintLoanApplication?loanId=' + data + '">Print</a>';
                    }
                },
                {
                    "data": "LoanNo",
                    "width": "50px",
                    "render": function (data) {
                        return '<a class="btn btn-info" target="_blank" href="' + window.applicationBaseUrl + 'Loan/ApprovedLoanList/PrintLoanApproval?loanId=' + data + '">Print</a>';
                    }
                }
            ],
            "order": [[0, "desc"]],
        });
    }


})();