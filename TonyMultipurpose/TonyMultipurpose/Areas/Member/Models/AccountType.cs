﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class AccountType:CommonModel
    {
        [Key]
        public byte AccountTypeId { get; set; }


        [Required]
        [Display(Name = "Account Type")]
        public string AccountTypeName { get; set; }
    }
}