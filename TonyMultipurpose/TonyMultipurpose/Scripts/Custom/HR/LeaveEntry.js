﻿(function () {

    $('#UpdateLeave').hide();
    var oLeaveTable;
    LeaveTable();
    function LeaveTable() {
        oLeaveTable = $('#LeaveTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/Leave/GetAllLeaveInfo",// '@Url.Action("GetAllLeaveInfo", "Leave", new { Area = "HR" })',
                "type": "get",
            },
            "columns": [
                { "data": "EmpCode" },
                { "data": "EmployeeName" },
                { "data": "LeaveTypeName" },
                { "data": "Description" },
                {
                    "data": "FromDateShow"
                },
                { "data": "ToDateShow" },
                { "data": "LeaveDays" },
            ],
            "order": [[0, "desc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#LeaveTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oLeaveTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindLeaveEntry(oLeaveTable.row(this).data());
        });
    }
    function bindLeaveEntry(data) {
        $('#EmpCode').val(data.EmpCode).attr("readonly","readonly");
        $('#EmployeeName').val(data.EmployeeName);
        $('#LTypeId').val(data.LTypeId);
        $('#Description').val(data.Description);
        $('#FromDate').val(data.FromDateShow);
        $('#ToDate').val(data.ToDateShow);
        $('#LeaveDays').val(data.LeaveDays);
        $('#submit').hide();
        $('#UpdateLeave').show();
        //$('#deleteMonthlyExpense').show();
    }



    $('#FromDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#ToDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    GetLeaveType();
    function GetLeaveType() {
        $('#LTypeId').empty();
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "HR/Leave/GetLeaveType",//'@Url.Action("GetLeaveType", "Leave", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            //data: JSON.stringify(json),
            success: function (data) {
                $("#LTypeId").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#LTypeId").append('<option value=' + value.LTypeId + '>' + value.LeaveTypeName + '</option>');
                });
            }
        });
    }
    function ClearData() {
        $('#EmpCode').val('').attr("readonly", false);
        $('#EmployeeName').val('');
        $('#LTypeId').val('');
        $('#Description').val('');
        $('#FromDate').val('');
        $('#ToDate').val('');
        $('#submit').show();
        $('#UpdateLeave').hide();
    }

    $('#clear').click(function() {
        ClearData();
    });


    $('#ToDate').change(function () {
        var fromDate = $('#FromDate').val();
        var toDate = $('#ToDate').val();
        var data = { fromDate: fromDate, toDate: toDate };
        $.ajax({
            url: window.applicationBaseUrl + "HR/Leave/GetHolidayInformation",//'@Url.Action("GetHolidayInformation", "Leave", new { Area = "HR" })',
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                document.getElementById('TotalLeaveCount').innerHTML = data + '  Days';
            },
            error: function () {
            }
        });

    });
    $('#submit').click(function () {
        var isValid = CheckLeaveValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpCode').val(),
                EmployeeName: $('#EmployeeName').val(),
                LTypeId: $('#LTypeId option:selected').val(),
                Description: $('#Description').val(),
                FromDate: $('#FromDate').val(),
                ToDate: $('#ToDate').val(),
            }
            $.ajax({
                url: window.applicationBaseUrl + "HR/Leave/SaveLeaveEntry",//'@Url.Action("SaveLeaveEntry", "Leave", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.Status == true) {
                        //oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearData();
                    }
                    document.getElementById('showMessage').innerHTML = d.Message;
                    window.location.reload();
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';

                }
            });

        }
    });
    $('#UpdateLeave').click(function () {
        var isValid = CheckLeaveValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpCode').val(),
                EmployeeName: $('#EmployeeName').val(),
                LTypeId: $('#LTypeId option:selected').val(),
                Description: $('#Description').val(),
                FromDate: $('#FromDate').val(),
                ToDate: $('#ToDate').val(),
            }
            $.ajax({
                url: window.applicationBaseUrl + "HR/Leave/UpdateLeaveEntry",//'@Url.Action("UpdateLeaveEntry", "Leave", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.Status == true) {
                        //oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearData();
                    }
                    document.getElementById('showMessage').innerHTML = d.Message;
                    window.location.reload();
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';

                }
            });

        }
    });
    $("#EmpCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/Leave/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "Leave", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });
    $("#EmpCode").change(function () {
        $('#EmployeeName').empty();
        var EmpCode = $("#EmpCode").val();
        var json = { EmpCode: EmpCode };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/Leave/GetEmployeeNameByEmpCode",//'@Url.Action("GetEmployeeNameByEmpCode", "Leave", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#EmployeeName').val(data.EmployeeName);

            }
        });
    });
       

        function CheckLeaveValidation() {
            var isValidLeave = true;
            if ($('#EmpCode').val().trim() === '') {
                isValidLeave = false;
                $('#EmpCode').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#EmpCode').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#LTypeId').val().trim() === '0') {
                isValidLeave = false;
                $('#LTypeId').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#LTypeId').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#FromDate').val().trim() === '') {
                isValidLeave = false;
                $('#FromDate').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#FromDate').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isValidLeave = false;
                $('#ToDate').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#ToDate').parent().prev().find('span').css('visibility', 'hidden');
            }

            return isValidLeave;
        }


})();