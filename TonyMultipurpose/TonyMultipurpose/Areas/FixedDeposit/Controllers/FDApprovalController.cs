﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FixedDeposit.BLL;
using TonyMultipurpose.Areas.FixedDeposit.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FixedDeposit.Controllers
{
    [AuthenticationFilter]
    public class FDApprovalController : Controller
    {
        private readonly FixedDepositBLL _objFixedDepositBll;

        public FDApprovalController()
        {
            _objFixedDepositBll = new FixedDepositBLL();
        }

        // GET: FixedDeposit/FDApproval
        public ActionResult Index()
        {
            var objFixedDepositModel = new FixedDepositModel
            {
                IsApproved = false,
                CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId)
            };
            var listOfFixedDeposit = _objFixedDepositBll.GetUnapprovedFixedDeposit(objFixedDepositModel);
            return View(listOfFixedDeposit);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("Id");
            if(ch != null)
            {
                _objFixedDepositBll.ApprovedFdTransaction(ch);
            }
            return RedirectToAction("Index", "FDApproval");

        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<FixedDepositModel> transactions = new List<FixedDepositModel>();
            FixedDepositModel objFixedDepositModel;
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objFixedDepositModel = new FixedDepositModel();
                    objFixedDepositModel.TransactionId = ids[i];
                    objFixedDepositModel.RejectReason = form.GetValues(objFixedDepositModel.TransactionId)[0];
                    transactions.Add(objFixedDepositModel);
                }
                _objFixedDepositBll.RejectFixedDepositeTransaction(transactions);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Detail(int Id)
        {
            if (Id == null) return null;
            var fixedDepositData = _objFixedDepositBll.GetFixedDepositTransactionsById(Id);
            return View(fixedDepositData);
        }
    }
}