﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class CompanyBankInfoBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForCompanyBankInfo(DbDataReader objDataReader, CompanyBankInfo objCompanyBankInfo)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "BankId":
                        if (!Convert.IsDBNull(objDataReader["BankId"]))
                        {
                            objCompanyBankInfo.BankId = Convert.ToInt32(objDataReader["BankId"]);
                        }
                        break;
                    case "COAId":
                        if (!Convert.IsDBNull(objDataReader["COAId"]))
                        {
                            objCompanyBankInfo.COAId = Convert.ToInt32(objDataReader["COAId"]);
                        }
                        break;
                    case "AccountCode":
                        if (!Convert.IsDBNull(objDataReader["AccountCode"]))
                        {
                            objCompanyBankInfo.AccountCode = objDataReader["AccountCode"].ToString();
                        }
                        break;
                    case "AccountName":
                        if (!Convert.IsDBNull(objDataReader["AccountName"]))
                        {
                            objCompanyBankInfo.AccountName = objDataReader["AccountName"].ToString();
                        }
                        break;
                    case "BankName":
                        if (!Convert.IsDBNull(objDataReader["BankName"]))
                        {
                            objCompanyBankInfo.BankName = objDataReader["BankName"].ToString();
                        }
                        break;
                    case "BankAccountNo":
                        if (!Convert.IsDBNull(objDataReader["BankAccountNo"]))
                        {
                            objCompanyBankInfo.BankAccountNo = objDataReader["BankAccountNo"].ToString();
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objCompanyBankInfo.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                   
                    default:
                        break;
                }
            }
        }
        public List<CompanyBankInfo> GetAllCompanyBankInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CompanyBankInfo> objCompanyBankInfoList = new List<CompanyBankInfo>();
            CompanyBankInfo objCompanyBankInfo;
            try
            {
                //objDbCommand.AddInParameter("IsDeleted", 0);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[FT_GetAllCompanyBankInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCompanyBankInfo = new CompanyBankInfo();
                        this.BuildModelForCompanyBankInfo(objDbDataReader, objCompanyBankInfo);
                        objCompanyBankInfoList.Add(objCompanyBankInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCompanyBankInfoList;
        }

        public string SaveCompanyBankInfo(CompanyBankInfo objCompanyBankInfo)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("COAId", objCompanyBankInfo.COAId);
            objDbCommand.AddInParameter("BankName", objCompanyBankInfo.BankName);
            objDbCommand.AddInParameter("BranchName", objCompanyBankInfo.BranchName);
            objDbCommand.AddInParameter("BankAccountNo", objCompanyBankInfo.BankAccountNo);
            //objDbCommand.AddInParameter("IsDeleted", objCompanyBankInfo.IsDeleted);
            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            //objDbCommand.AddInParameter("CreatedDate", objCompanyBankInfo.CreatedDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FT_SaveCompanyBankInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string UpdateCompanyBankInfo(CompanyBankInfo objCompanyBankInfo)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("BankId", objCompanyBankInfo.BankId);
            objDbCommand.AddInParameter("COAId", objCompanyBankInfo.COAId);
            objDbCommand.AddInParameter("BankName", objCompanyBankInfo.BankName);
            objDbCommand.AddInParameter("BranchName", objCompanyBankInfo.BranchName);
            objDbCommand.AddInParameter("BankAccountNo", objCompanyBankInfo.BankAccountNo);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FT_UpdateCompanyBankInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteCompanyBankInfo(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("BankId", id);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].FT_DeleteCompanyBankInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}