﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class MonthlyExpense
    {
        public long MonthlyExpenseId { get; set; }

        public string LoanId { get; set; }

        public decimal FamilyExpense { get; set; }

        public decimal ExistingEMI { get; set; }

        public decimal ChildrenEducation { get; set; }

        public decimal OthersExpense { get; set; }

        public short CreatedBy { get; set; }
        public decimal TotalExpense { get; set; }
        
    }
}