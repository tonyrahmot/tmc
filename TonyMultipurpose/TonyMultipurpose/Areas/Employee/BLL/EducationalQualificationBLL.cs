﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Employee.BLL
{
    public class EducationalQualificationBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForDepartment(DbDataReader objDataReader, EducationalQualification objEducationalQualification)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "EmployeeId":
                        if (!Convert.IsDBNull(objDataReader["EmployeeId"]))
                        {
                            objEducationalQualification.EmployeeId = Convert.ToByte(objDataReader["EmployeeId"]);
                        }
                        break;
                    case "EmployeeName":
                        if (!Convert.IsDBNull(objDataReader["EmployeeName"]))
                        {
                            objEducationalQualification.EmployeeName = objDataReader["EmployeeName"].ToString();
                        }
                        break;
                    case "EQualificationId":
                        if (!Convert.IsDBNull(objDataReader["EQualificationId"]))
                        {
                            objEducationalQualification.EQualificationId = Convert.ToInt16(objDataReader["EQualificationId"]);
                        }
                        break;
                    case "ExamName":
                        if (!Convert.IsDBNull(objDataReader["ExamName"]))
                        {
                            objEducationalQualification.ExamName = objDataReader["ExamName"].ToString();
                        }
                        break;
                    case "InstitutionName":
                        if (!Convert.IsDBNull(objDataReader["InstitutionName"]))
                        {
                            objEducationalQualification.InstitutionName =objDataReader["InstitutionName"].ToString();
                        }
                        break;
                    case "Board_University":
                        if (!Convert.IsDBNull(objDataReader["Board_University"]))
                        {
                            objEducationalQualification.Board_University = objDataReader["Board_University"].ToString();
                        }
                        break;
                    case "PassingYear":
                        if (!Convert.IsDBNull(objDataReader["PassingYear"]))
                        {
                            objEducationalQualification.PassingYear = objDataReader["PassingYear"].ToString();
                        }
                        break;
                    case "Grade_GPA":
                        if (!Convert.IsDBNull(objDataReader["Grade_GPA"]))
                        {
                            objEducationalQualification.Grade_GPA =Convert.ToDouble(objDataReader["Grade_GPA"]);
                        }
                        break;
                    case "Session":
                        if (!Convert.IsDBNull(objDataReader["Session"]))
                        {
                            objEducationalQualification.Session = objDataReader["Session"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objEducationalQualification.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objEducationalQualification.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objEducationalQualification.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objEducationalQualification.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objEducationalQualification.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objEducationalQualification.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objEducationalQualification.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objEducationalQualification.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        public List<EmployeeDetail> GetEmployeeDetailName()
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeDetail> objEmployeeDetailNameList = new List<EmployeeDetail>();
            EmployeeDetail objEmployeeDetailName;

            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetEmployeeNameList",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeDetailName = new EmployeeDetail();

                        objEmployeeDetailName.EmployeeId = Convert.ToByte(objDbDataReader["EmployeeId"].ToString());
                        objEmployeeDetailName.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objEmployeeDetailNameList.Add(objEmployeeDetailName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objEmployeeDetailNameList;
        }

        public string SaveEducationalQualification(EducationalQualification objEducationalQualification)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", objEducationalQualification.EmployeeId);
            objDbCommand.AddInParameter("ExamName", objEducationalQualification.ExamName);
            objDbCommand.AddInParameter("InstitutionName", objEducationalQualification.InstitutionName);
            objDbCommand.AddInParameter("Board_University", objEducationalQualification.Board_University);
            objDbCommand.AddInParameter("Session", objEducationalQualification.Session);
            objDbCommand.AddInParameter("PassingYear", objEducationalQualification.PassingYear);
            objDbCommand.AddInParameter("Grade_GPA", objEducationalQualification.Grade_GPA);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateEducationalQualification", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<EducationalQualification> GetAllEmployeeNameById(int employeeId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<EducationalQualification> objEducationalQualificationList = new List<EducationalQualification>();

            EducationalQualification objEducationalQualification = new EducationalQualification();

            try
            {
                objDbCommand.AddInParameter("EmployeeId", employeeId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllEmployeeNameById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEducationalQualification = new EducationalQualification();
                        objEducationalQualification.EQualificationId = Convert.ToInt16(objDbDataReader["EQualificationId"].ToString());
                        objEducationalQualification.EmployeeId = Convert.ToInt16(objDbDataReader["EmployeeId"].ToString());
                        objEducationalQualification.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objEducationalQualification.ExamName = objDbDataReader["ExamName"].ToString();
                        objEducationalQualification.InstitutionName = objDbDataReader["InstitutionName"].ToString();
                        objEducationalQualification.Board_University = objDbDataReader["Board_University"].ToString();
                        objEducationalQualification.Session = objDbDataReader["Session"].ToString();
                        objEducationalQualification.PassingYear = objDbDataReader["PassingYear"].ToString();
                        objEducationalQualification.Grade_GPA = Convert.ToDouble(objDbDataReader["Grade_GPA"]);

                        objEducationalQualificationList.Add(objEducationalQualification);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objEducationalQualificationList;
        }

        public EducationalQualification GetAllEducationalQualification(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<EducationalQualification> objEducationalQualificationList = new List<EducationalQualification>();

            EducationalQualification objEducationalQualification = new EducationalQualification();

            try
            {
                objDbCommand.AddInParameter("EQualificationId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetEducationalQualification]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEducationalQualification = new EducationalQualification();
                        this.BuildModelForDepartment(objDbDataReader, objEducationalQualification);
                        objEducationalQualificationList.Add(objEducationalQualification);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objEducationalQualification;
        }

        public string UpadateEducationalQualification(EducationalQualification objEducationalQualification)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EQualificationId", objEducationalQualification.EQualificationId);
            objDbCommand.AddInParameter("ExamName", objEducationalQualification.ExamName);
            objDbCommand.AddInParameter("InstitutionName", objEducationalQualification.InstitutionName);
            objDbCommand.AddInParameter("Board_University", objEducationalQualification.Board_University);
            objDbCommand.AddInParameter("Session", objEducationalQualification.Session);
            objDbCommand.AddInParameter("PassingYear", objEducationalQualification.PassingYear);
            objDbCommand.AddInParameter("Grade_GPA", objEducationalQualification.Grade_GPA);
            objDbCommand.AddInParameter("IsActive", objEducationalQualification.IsActive);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateEducationalQualification", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteEducationalQualification(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EQualificationId", id);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteEducationalQualificationById", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}