﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Share.BLL;
using TonyMultipurpose.Areas.Share.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.Controllers
{
    public class ShareEntryController : Controller
    {
        // GET: Share/ShareEntry
        CommonResult  _oCommonResult;
         ShareEntryBLL _oShareEntryBll;
        AuthorizedShareInfo _objAuthorizedShareInfoModel;
        public ActionResult Index()
        {
            //_objAuthorizedShareInfoModel = new AuthorizedShareInfo();
            //_oShareEntryBll = new ShareEntryBLL();
            //_objAuthorizedShareInfoModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            //_objAuthorizedShareInfoModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
            //List<AuthorizedShareInfo> objShareList = _oShareEntryBll.GetAllAuthorizedShareInfos(_objAuthorizedShareInfoModel);
            //return View(objShareList);
            return View();
        }

        public ActionResult SaveShareEntry(AuthorizedShareInfo objAuthorizedShareInfo)
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = _oShareEntryBll.SaveShareEntry(objAuthorizedShareInfo);
            }
            return new JsonResult { Data = new { _oCommonResult } };
        }

        [HttpGet]
        public JsonResult GetAllAuthorizeShareInfo()
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            var asi = _oShareEntryBll.GetAllAuthorizedShareInfos();
            return Json(new { data = asi }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateShareEntry(AuthorizedShareInfo objAuthorizedShareInfo)
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = _oShareEntryBll.UpdateShareEntry(objAuthorizedShareInfo);
            }
            return new JsonResult { Data = new { _oCommonResult } };
        }

        public JsonResult GetAuthorizeShare()
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            var data = _oShareEntryBll.GetAuthorizeShareData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetAllIssuedShareInfo()
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            var asi = _oShareEntryBll.GetAllIssuedShareInfos();
            return Json(new { data = asi }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRemainingIssued(int authorizedShareId)
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            var data = _oShareEntryBll.GetRemainingIssued(authorizedShareId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveIssuedShareInfo(IssuedShareInfo objIssuedShareInfo)
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = _oShareEntryBll.SaveIssuedShare(objIssuedShareInfo);
            }
            return new JsonResult { Data = new { _oCommonResult } };
        }

        public ActionResult UpdateIssuedShareInfo(IssuedShareInfo objIssuedShareInfo)
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = _oShareEntryBll.UpdateIssuedShare(objIssuedShareInfo);
            }
            return new JsonResult { Data = new { _oCommonResult } };
        }

        public JsonResult GetPaidUpShareInfo()
        {
            _oCommonResult = new CommonResult();
            _oShareEntryBll = new ShareEntryBLL();
            var asi = _oShareEntryBll.GetPaidUpShareInfos();
            return Json(new { data = asi }, JsonRequestBehavior.AllowGet);
        }
    }
}