﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Areas.DoubleBenefit.Models;

namespace TonyMultipurpose.Areas.DoubleBenefit.BLL
{
    public class DoubleBenefitBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public List<string> GetDBAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objDBAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetDBAccountNumberForAutocomplete]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objDBAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objDBAccountNumberList;
        }

        public Doublebenefit GetDBAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            Doublebenefit objDoublebenefit = new Doublebenefit();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetDoubleBenefitAccountInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objDoublebenefit.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objDoublebenefit.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objDoublebenefit.DurationofYear = objDbDataReader["DurationofYear"].ToString();
                            objDoublebenefit.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                            objDoublebenefit.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                            objDoublebenefit.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            objDoublebenefit.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                            objDoublebenefit.AccruedBalance = Convert.ToDecimal(objDbDataReader["AccruedBalance"].ToString());
                            objDoublebenefit.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objDoublebenefit.CustomerImage = imagePath;
                            }
                            else
                            {
                                objDoublebenefit.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objDoublebenefit;
        }

        public void SaveDBMatureEncashmentInfo(Doublebenefit objdoubleBenefit)
        {
            int noOfAffacted = 0;
            objdoubleBenefit.TransactionId = GenerateTransactionId(objdoubleBenefit);

            if (objdoubleBenefit.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", objdoubleBenefit.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", objdoubleBenefit.CustomerName);
                objDbCommand.AddInParameter("CustomerId", objdoubleBenefit.CustomerId);
                objDbCommand.AddInParameter("DurationofYear", objdoubleBenefit.DurationofYear);
                objDbCommand.AddInParameter("MatureDate", objdoubleBenefit.MatureDate);
                objDbCommand.AddInParameter("MatureAmount", objdoubleBenefit.MatureAmount);
           //     objDbCommand.AddInParameter("AccruedBalance", objdoubleBenefit.AccruedBalance);
                objDbCommand.AddInParameter("Amount", objdoubleBenefit.Amount);
                objDbCommand.AddInParameter("COAId", objdoubleBenefit.COAId);
                objDbCommand.AddInParameter("transactionId", objdoubleBenefit.TransactionId);
                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveDBMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }
        public void SaveDBPreMatureEncashmentInfo(Doublebenefit objdoubleBenefit)
        {
            int noOfAffacted = 0;
            objdoubleBenefit.TransactionId = GenerateTransactionId(objdoubleBenefit);

            if (objdoubleBenefit.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", objdoubleBenefit.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", objdoubleBenefit.CustomerName);
                objDbCommand.AddInParameter("CustomerId", objdoubleBenefit.CustomerId);
                objDbCommand.AddInParameter("InterestRate", objdoubleBenefit.InterestRate);
                objDbCommand.AddInParameter("AccruedBalance", objdoubleBenefit.AccruedBalance);
                objDbCommand.AddInParameter("PreMatureAmount", objdoubleBenefit.PreMatureAmount);
                objDbCommand.AddInParameter("Amount", objdoubleBenefit.Amount);
                objDbCommand.AddInParameter("transactionId", objdoubleBenefit.TransactionId);
                objDbCommand.AddInParameter("COAId", objdoubleBenefit.COAId);
                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
           

                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveDBPreMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }

        private string GenerateTransactionId(Doublebenefit objdoubleBenefit)
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            if (objdoubleBenefit.AccountNumber != null)
            {
                objDbDataReader = null;
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                int Id = 0;

                string year = Convert.ToString(DateTime.Now.Year);
                string days = Convert.ToString(DateTime.Now.DayOfYear);
                //var prefix = string.Concat(year + days);
                string prefix = year + days;

                objDbCommand.AddInParameter("FieldName", "TransactionId");
                objDbCommand.AddInParameter("TableName", "DoubleBenefit");
                objDbCommand.AddInParameter("Prefix", prefix);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                    CommandType.StoredProcedure);
                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        transactionId = prefix;
                        Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        transactionId += Convert.ToString(Id).PadLeft(4, '0');
                    }
                }
                objDbDataReader.Close();
            }
            return transactionId;
        }




        public List<Doublebenefit> GetDoublebenefitTransactions(Doublebenefit DoublebenefitModel)
        {
            DbDataReader objDbDataReader = null;
            var listOfDoublebenefit = new List<Doublebenefit>();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("IsApproved", DoublebenefitModel.IsApproved);
            objDbCommand.AddInParameter("CompanyId", DoublebenefitModel.CompanyId);
            if (objDbCommand == null) return null;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand,
                    "[dbo].[uspGetDoublebenefitTransactions]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        var objModel = new Doublebenefit();
                        BuildModelForDoubleBenefit(objDbDataReader, objModel);
                        listOfDoublebenefit.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfDoublebenefit;
        }


        //public List<Doublebenefit> GetDoublebenefitTransactions(Doublebenefit objDoublebenefit)
        //{
        //    objDataAccess = DataAccess.NewDataAccess();
        //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
        //    DbDataReader objDbDataReader = null;
        //    List<Doublebenefit> DBTransactions = new List<Doublebenefit>();
        //    Doublebenefit objDB;
        //    try
        //    {
        //        //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
        //        objDbCommand.AddInParameter("IsApproved", objDoublebenefit.IsApproved);
        //        //objDbCommand.AddInParameter("IsActive", oCSSModel.IsActive);
        //        objDbCommand.AddInParameter("CompanyId", objDoublebenefit.CompanyId);
        //        objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetDoublebenefitTransactions]", CommandType.StoredProcedure);

        //        if (objDbDataReader.HasRows)
        //        {
        //            while (objDbDataReader.Read())
        //            {
        //                objDB = new Doublebenefit();
        //                this.BuildModelForDoubleBenefit(objDbDataReader, objDB);
        //                DBTransactions.Add(objDB);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error : " + ex.Message);
        //    }
        //    finally
        //    {
        //        if (objDbDataReader != null)
        //        {
        //            objDbDataReader.Close();
        //        }
        //        objDataAccess.Dispose(objDbCommand);
        //    }
        //    return DBTransactions;
        //}





        private void BuildModelForDoubleBenefit(IDataReader objDataReader, Doublebenefit objDoublebenefitModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            if (objDataTable == null) return;
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDataReader["TransactionId"]))
                        {
                            objDoublebenefitModel.TransactionId = objDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objDoublebenefitModel.Id = Convert.ToInt64(objDataReader["Id"].ToString());
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDataReader["TransactionDate"]))
                        {
                            objDoublebenefitModel.TransactionDate = Convert.ToDateTime(objDataReader["TransactionDate"].ToString());
                        }
                        break;

                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objDoublebenefitModel.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDataReader["TransactionType"]))
                        {
                            objDoublebenefitModel.TransactionType = objDataReader["TransactionType"].ToString();
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objDoublebenefitModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objDoublebenefitModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;

                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objDoublebenefitModel.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objDoublebenefitModel.BranchId = Convert.ToInt16(objDataReader["BranchId"].ToString());
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objDoublebenefitModel.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objDoublebenefitModel.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objDoublebenefitModel.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;
                    case "Debit":
                        if (!Convert.IsDBNull(objDataReader["Debit"]))
                        {
                            objDoublebenefitModel.Debit = Convert.ToDecimal(objDataReader["Debit"].ToString());
                        }
                        break;
                    case "Credit":
                        if (!Convert.IsDBNull(objDataReader["Credit"]))
                        {
                            objDoublebenefitModel.Credit = Convert.ToDecimal(objDataReader["Credit"].ToString());
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objDoublebenefitModel.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "AccruedBalance":
                        if (!Convert.IsDBNull(objDataReader["AccruedBalance"]))
                        {
                            objDoublebenefitModel.AccruedBalance = Convert.ToDecimal(objDataReader["AccruedBalance"].ToString());
                        }
                        break;
                    case "Balance":
                        if (!Convert.IsDBNull(objDataReader["Balance"]))
                        {
                            objDoublebenefitModel.Balance = Convert.ToDecimal(objDataReader["Balance"].ToString());
                        }
                        break;

                    case "Particular":
                        if (!Convert.IsDBNull(objDataReader["Particular"]))
                        {
                            objDoublebenefitModel.Particular = objDataReader["Particular"].ToString();
                        }
                        break;
                    case "IsApproved":
                        if (!Convert.IsDBNull(objDataReader["IsApproved"]))
                        {
                            objDoublebenefitModel.IsApproved = Convert.ToBoolean(objDataReader["IsApproved"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objDoublebenefitModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objDoublebenefitModel.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDataReader["Amount"]))
                        {
                            objDoublebenefitModel.Amount = Convert.ToDecimal(objDataReader["Amount"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }


        //private void BuildModelForDoubleBenefit(DbDataReader objDbDataReader, Doublebenefit objDoublebenefit)
        //{
        //    DataTable objDataTable = objDbDataReader.GetSchemaTable();
        //    foreach (DataRow dr in objDataTable.Rows)
        //    {
        //        String column = dr.ItemArray[0].ToString();
        //        switch (column)
        //        {
        //            case "TransactionId":
        //                if (!Convert.IsDBNull(objDbDataReader["TransactionId"]))
        //                {
        //                    objDoublebenefit.TransactionId = objDbDataReader["TransactionId"].ToString();
        //                }
        //                break;

        //            case "TransactionDate":
        //                if (!Convert.IsDBNull(objDbDataReader["TransactionDate"]))
        //                {
        //                    objDoublebenefit.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
        //                }
        //                break;
        //            case "TransactionType":
        //                if (!Convert.IsDBNull(objDbDataReader["TransactionType"]))
        //                {
        //                    objDoublebenefit.TransactionType = objDbDataReader["TransactionType"].ToString();
        //                }
        //                break;
        //            case "AccountNumber":
        //                if (!Convert.IsDBNull(objDbDataReader["AccountNumber"]))
        //                {
        //                    objDoublebenefit.AccountNumber = objDbDataReader["AccountNumber"].ToString();
        //                }
        //                break;

        //            case "CustomerId":
        //                if (!Convert.IsDBNull(objDbDataReader["CustomerId"]))
        //                {
        //                    objDoublebenefit.CustomerId = objDbDataReader["CustomerId"].ToString();
        //                }
        //                break;
        //            case "CustomerName":
        //                if (!Convert.IsDBNull(objDbDataReader["CustomerName"]))
        //                {
        //                    objDoublebenefit.CustomerName = objDbDataReader["CustomerName"].ToString();
        //                }
        //                break;
        //            case "InterestRate":
        //                if (!Convert.IsDBNull(objDbDataReader["InterestRate"]))
        //                {
        //                    objDoublebenefit.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
        //                }
        //                break;
        //            case "Mobile":
        //                if (!Convert.IsDBNull(objDbDataReader["Mobile"]))
        //                {
        //                    objDoublebenefit.Mobile = objDbDataReader["Mobile"].ToString();
        //                }
        //                break;
        //            case "MatureAmount":
        //                if (!Convert.IsDBNull(objDbDataReader["MatureAmount"]))
        //                {
        //                    objDoublebenefit.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
        //                }
        //                break;
        //            case "MatureDate":
        //                if (!Convert.IsDBNull(objDbDataReader["MatureDate"]))
        //                {
        //                    objDoublebenefit.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
        //                }
        //                break;

        //            case "Amount":
        //                if (!Convert.IsDBNull(objDbDataReader["Amount"]))
        //                {
        //                    objDoublebenefit.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
        //                }
        //                break;

        //            case "Username":
        //                if (!Convert.IsDBNull(objDbDataReader["Username"]))
        //                {
        //                    objDoublebenefit.Username = objDbDataReader["Username"].ToString();
        //                }
        //                break;
        //            case "DurationofYear":
        //                if (!Convert.IsDBNull(objDbDataReader["DurationofYear"]))
        //                {
        //                    objDoublebenefit.DurationofYear = objDbDataReader["DurationofYear"].ToString();
        //                }
        //                break;
        //            case "DurationofMonth":
        //                if (!Convert.IsDBNull(objDbDataReader["DurationofMonth"]))
        //                {
        //                    objDoublebenefit.DurationofMonth = objDbDataReader["DurationofMonth"].ToString();
        //                }
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        public string ApprovedDBTransaction(string[] ch)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            try
            {
                foreach (var id in ch)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("TransactionId", id);
                    objDbCommand.AddInParameter("IsApproved", true);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApprovedDBTransaction]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";

            //objDataAccess = DataAccess.NewDataAccess();

            //foreach (var id in ch)
            //{
            //    objDbCommand = null;
            //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            //    objDbCommand.AddInParameter("Id", id);
            //    objDbCommand.AddInParameter("IsApproved", true);
            //    var noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApprovedDBTransaction]", CommandType.StoredProcedure);
            //    if (noOfAffacted > 0)
            //    {
            //        objDbCommand.Transaction.Commit();
            //    }
            //    else
            //    {
            //        objDbCommand.Transaction.Rollback();
            //    }
            //}
        }

        public string RejectDoubleBenefitTransaction(List<Doublebenefit> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspRejectDoubleBenefitTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public Doublebenefit GetDoublebenefitTransactionsById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            var listOfDBTransaction = new List<Doublebenefit>();

            var objDoublebenefitModel = new Doublebenefit();

            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetDoublebenefitTransactionsById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objDoublebenefitModel = new Doublebenefit();
                        BuildModelForDoubleBenefit(objDbDataReader, objDoublebenefitModel);
                        listOfDBTransaction.Add(objDoublebenefitModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }

            return objDoublebenefitModel;
        }

        public bool GetUnAppovedDBList(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[DoubleBenefit_GetUnAppovedDBList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetCloseDBAccount(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[DoubleBenefit_GetCloseDBAccount]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
    }
}