﻿using System.Linq;
using System.Web.Mvc;

namespace TonyMultipurpose.App_Start
{
    //Added By: Nayeem
    //Date: 20-May-2017
    //This class added for filtering cshtmls only
    public class FilteredRazorViewEngine : RazorViewEngine
    {
        private string _extension;
        public FilteredRazorViewEngine(string viewTypeExtension)
        : base()
        {
            _extension = viewTypeExtension;
            AreaMasterLocationFormats = Filter(base.AreaMasterLocationFormats);
            AreaPartialViewLocationFormats = Filter(base.AreaPartialViewLocationFormats);
            AreaViewLocationFormats = Filter(base.AreaViewLocationFormats);
            FileExtensions = Filter(base.FileExtensions);
            MasterLocationFormats = Filter(base.MasterLocationFormats);
            PartialViewLocationFormats = Filter(base.PartialViewLocationFormats);
            ViewLocationFormats = Filter(base.ViewLocationFormats);
        }
        private string[] Filter(string[] source)
        {
            return source.Where(
                s =>
                    s.Contains(_extension)).ToArray();
        }
    }
}