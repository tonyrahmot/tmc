﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class ClientInfoBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public CommonResult ClientInfoSave(ClientInfo objClientInfo)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("ClientCode", objClientInfo.ClientCode);
            objDbCommand.AddInParameter("ClientName", objClientInfo.ClientName);
            objDbCommand.AddInParameter("Company", objClientInfo.Company);
            objDbCommand.AddInParameter("Phone", objClientInfo.Phone);
            objDbCommand.AddInParameter("COAId", objClientInfo.COAId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].Loan_ClientInfoSave",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
    }
}