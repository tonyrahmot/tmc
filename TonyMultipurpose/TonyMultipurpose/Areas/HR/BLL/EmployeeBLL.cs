﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Areas.HR.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.BLL
{
    public class EmployeeBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        #region BasicInfo
        public List<Department> GetDepartments()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Department> departments = new List<Department>();
            Department oDepartment;
            try
            {
                {
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetDepartments]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            oDepartment = new Department();
                            if (objDbDataReader["DeptId"] != null)
                            {
                                oDepartment.DeptId = Convert.ToInt32(objDbDataReader["DeptId"]);
                                oDepartment.DepartmentName = Convert.ToString(objDbDataReader["DepartmentName"]);
                                departments.Add(oDepartment);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return departments;
        }

        public List<Designation> GetDesignations()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Designation> designations = new List<Designation>();
            Designation oDesignation;
            try
            {
                {
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetDesignations]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            oDesignation = new Designation();
                            if (objDbDataReader["DesigId"] != null)
                            {
                                oDesignation.DesigId = Convert.ToInt32(objDbDataReader["DesigId"]);
                                oDesignation.DesignationName = Convert.ToString(objDbDataReader["DesignationName"]);
                                designations.Add(oDesignation);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return designations;
        }

        public List<Designation> GetFunctionalDesignations()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Designation> designations = new List<Designation>();
            Designation oDesignation;
            try
            {
                {
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetFunctionalDesignations]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            oDesignation = new Designation();
                            if (objDbDataReader["DesigId"] != null)
                            {
                                oDesignation.DesigId = Convert.ToInt32(objDbDataReader["DesigId"]);
                                oDesignation.FunctionalDesignation = Convert.ToString(objDbDataReader["FuntionalDesignation"]);
                                designations.Add(oDesignation);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return designations;
        }

        public CommonResult SaveEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("EmpCode", oEmployeeInformation.EmpCode);
            objDbCommand.AddInParameter("FirstName", oEmployeeInformation.FirstName);
            objDbCommand.AddInParameter("LastName", oEmployeeInformation.LastName);
            objDbCommand.AddInParameter("DesigId", oEmployeeInformation.DesigId);
            objDbCommand.AddInParameter("FunctionalId", oEmployeeInformation.FunctionalId);
            objDbCommand.AddInParameter("DeptId", oEmployeeInformation.DeptId);
            objDbCommand.AddInParameter("BranchId", oEmployeeInformation.BranchId);
            objDbCommand.AddInParameter("CompanyId", oEmployeeInformation.CompanyId);
            objDbCommand.AddInParameter("CurrentSalary", oEmployeeInformation.CurrentSalary);
            objDbCommand.AddInParameter("JoiningDate", oEmployeeInformation.JoiningDate);
            objDbCommand.AddInParameter("DOB", oEmployeeInformation.DOB);
            objDbCommand.AddInParameter("Religion", oEmployeeInformation.Religion);
            objDbCommand.AddInParameter("Gender", oEmployeeInformation.Gender);
            objDbCommand.AddInParameter("MaritalStatus", oEmployeeInformation.MaritalStatus);
            objDbCommand.AddInParameter("FatherName", oEmployeeInformation.FatherName);
            objDbCommand.AddInParameter("MotherName", oEmployeeInformation.MotherName);
            objDbCommand.AddInParameter("NID", oEmployeeInformation.NID);
            objDbCommand.AddInParameter("EmailId", oEmployeeInformation.EmailId);
            objDbCommand.AddInParameter("MobileNo", oEmployeeInformation.MobileNo);
            objDbCommand.AddInParameter("PresentAddress", oEmployeeInformation.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", oEmployeeInformation.PermanentAddress);
            objDbCommand.AddInParameter("ImagePath", oEmployeeInformation.ImagePath);
            objDbCommand.AddInParameter("IsActive", oEmployeeInformation.IsActive);
            objDbCommand.AddInParameter("IsApproved", oEmployeeInformation.IsApproved);
            objDbCommand.AddInParameter("IsDeleted", oEmployeeInformation.IsDeleted);
            objDbCommand.AddInParameter("CreatedBy", oEmployeeInformation.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", oEmployeeInformation.CreatedDate);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SaveEmployeeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                    oCommonResult.Id = oEmployeeInformation.EmpCode;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult UpdateEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmpId", oEmployeeInformation.EmpId);
            objDbCommand.AddInParameter("EmpCode", oEmployeeInformation.EmpCode);
            objDbCommand.AddInParameter("FirstName", oEmployeeInformation.FirstName);
            objDbCommand.AddInParameter("LastName", oEmployeeInformation.LastName);
            objDbCommand.AddInParameter("DesigId", oEmployeeInformation.DesigId);
            objDbCommand.AddInParameter("FunctionalId", oEmployeeInformation.FunctionalId);
            objDbCommand.AddInParameter("DeptId", oEmployeeInformation.DeptId);
            objDbCommand.AddInParameter("BranchId", oEmployeeInformation.BranchId);
            objDbCommand.AddInParameter("CompanyId", oEmployeeInformation.CompanyId);
            objDbCommand.AddInParameter("CurrentSalary", oEmployeeInformation.CurrentSalary);
            objDbCommand.AddInParameter("JoiningDate", oEmployeeInformation.JoiningDate);
            objDbCommand.AddInParameter("DOB", oEmployeeInformation.DOB);
            objDbCommand.AddInParameter("Religion", oEmployeeInformation.Religion);
            objDbCommand.AddInParameter("Gender", oEmployeeInformation.Gender);
            objDbCommand.AddInParameter("MaritalStatus", oEmployeeInformation.MaritalStatus);
            objDbCommand.AddInParameter("FatherName", oEmployeeInformation.FatherName);
            objDbCommand.AddInParameter("MotherName", oEmployeeInformation.MotherName);
            objDbCommand.AddInParameter("NID", oEmployeeInformation.NID);
            objDbCommand.AddInParameter("EmailId", oEmployeeInformation.EmailId);
            objDbCommand.AddInParameter("MobileNo", oEmployeeInformation.MobileNo);
            objDbCommand.AddInParameter("PresentAddress", oEmployeeInformation.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", oEmployeeInformation.PermanentAddress);
            objDbCommand.AddInParameter("ImagePath", oEmployeeInformation.ImagePath);
            objDbCommand.AddInParameter("UpdatedBy", oEmployeeInformation.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", oEmployeeInformation.UpdatedDate);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_UpdateEmployeeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Update Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult DeleteEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmpId", oEmployeeInformation.EmpId);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_DeleteEmployeeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Delete Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }


        public List<EmployeeInformationViewModel> GetAllEmployeeInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeInformationViewModel> employees = new List<EmployeeInformationViewModel>();
            EmployeeInformationViewModel objEmployeeInformationViewModel;
            try
            {
                {
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetAllEmployeeInfo]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            objEmployeeInformationViewModel = new EmployeeInformationViewModel();
                            objEmployeeInformationViewModel.EmpId = Convert.ToInt32(objDbDataReader["EmpId"]);
                            objEmployeeInformationViewModel.EmpCode = Convert.ToString(objDbDataReader["EmpCode"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["CurrentSalary"])))
                            {
                                objEmployeeInformationViewModel.CurrentSalary = Convert.ToDecimal(objDbDataReader["CurrentSalary"].ToString());
                            }
                            objEmployeeInformationViewModel.FirstName = Convert.ToString(objDbDataReader["FirstName"]);
                            objEmployeeInformationViewModel.LastName = Convert.ToString(objDbDataReader["LastName"]);
                            objEmployeeInformationViewModel.DesigId = Convert.ToInt32( objDbDataReader["DesigId"]);
                            objEmployeeInformationViewModel.DeptId = Convert.ToInt32( objDbDataReader["DeptId"]);
                            objEmployeeInformationViewModel.FunctionalId = Convert.ToInt32(objDbDataReader["FunctionalId"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["JoiningDate"])))
                            {
                                objEmployeeInformationViewModel.JoiningDate = Convert.ToString(objDbDataReader["JoiningDate"]);
                            }
                            objEmployeeInformationViewModel.MaritalStatus = Convert.ToString(objDbDataReader["MaritalStatus"]);
                            objEmployeeInformationViewModel.Gender = Convert.ToString(objDbDataReader["Gender"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["DOB"])))
                            {
                                objEmployeeInformationViewModel.DOB = Convert.ToString(objDbDataReader["DOB"]);
                            }
                            objEmployeeInformationViewModel.EmailId = Convert.ToString(objDbDataReader["EmailId"]);
                            objEmployeeInformationViewModel.Religion = Convert.ToString(objDbDataReader["Religion"]);
                            objEmployeeInformationViewModel.FatherName = Convert.ToString(objDbDataReader["FatherName"]);
                            objEmployeeInformationViewModel.MotherName = Convert.ToString(objDbDataReader["MotherName"]);
                            objEmployeeInformationViewModel.MobileNo = Convert.ToString(objDbDataReader["MobileNo"]);
                            objEmployeeInformationViewModel.NID = Convert.ToString(objDbDataReader["NID"]);
                            objEmployeeInformationViewModel.PresentAddress = Convert.ToString(objDbDataReader["PresentAddress"]);
                            objEmployeeInformationViewModel.PermanentAddress = Convert.ToString(objDbDataReader["PermanentAddress"]);
                            if (!Convert.IsDBNull(objDbDataReader["ImagePath"]))
                            {
                                var imagePath = objDbDataReader["ImagePath"].ToString();
                                objEmployeeInformationViewModel.OldImagePath = imagePath;
                                if (imagePath.Contains("Uploads"))
                                {
                                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                    imagePath = "/" + imagePath;
                                    //imagePath = serverPath + imagePath;
                                    objEmployeeInformationViewModel.ImagePath = imagePath;
                                }
                                else
                                {
                                    objEmployeeInformationViewModel.ImagePath = imagePath;
                                }

                            }
                            else
                            {
                                objEmployeeInformationViewModel.ImagePath = "\\Not Found\\";
                            }
                            objEmployeeInformationViewModel.BranchId = Convert.ToInt16(objDbDataReader["BranchId"]);
                            objEmployeeInformationViewModel.CompanyId = Convert.ToInt16(objDbDataReader["CompanyId"]);
                            objEmployeeInformationViewModel.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"]);
                            objEmployeeInformationViewModel.CreatedDate = Convert.ToDateTime(objDbDataReader["CreatedDate"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["UpdatedBy"])))
                            {
                                objEmployeeInformationViewModel.UpdatedBy = Convert.ToInt16(objDbDataReader["UpdatedBy"]);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["UpdatedDate"])))
                            {
                                objEmployeeInformationViewModel.UpdatedDate = Convert.ToDateTime(objDbDataReader["UpdatedDate"]);
                            }
                            objEmployeeInformationViewModel.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                            objEmployeeInformationViewModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"]);
                            objEmployeeInformationViewModel.EmployeeName = Convert.ToString(objDbDataReader["EmployeeName"]);
                            objEmployeeInformationViewModel.DepartmentName = Convert.ToString(objDbDataReader["DepartmentName"]);
                            objEmployeeInformationViewModel.DesignationName = Convert.ToString(objDbDataReader["DesignationName"]);
                            objEmployeeInformationViewModel.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                            objEmployeeInformationViewModel.FuntionalDesignation = Convert.ToString(objDbDataReader["FuntionalDesignation"]);
                            employees.Add(objEmployeeInformationViewModel);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return employees;
        }

        public List<EmployeeInformationViewModel> GetUnApprovedEmployeeList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeInformationViewModel> employees = new List<EmployeeInformationViewModel>();
            EmployeeInformationViewModel objEmployeeInformationViewModel;
            try
            {
                {
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetUnApprovedEmployeeList]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            objEmployeeInformationViewModel = new EmployeeInformationViewModel();
                            objEmployeeInformationViewModel.EmpId = Convert.ToInt32(objDbDataReader["EmpId"]);
                            objEmployeeInformationViewModel.EmpCode = Convert.ToString(objDbDataReader["EmpCode"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["CurrentSalary"])))
                            {
                                objEmployeeInformationViewModel.CurrentSalary = Convert.ToDecimal(objDbDataReader["CurrentSalary"].ToString());
                            }
                            objEmployeeInformationViewModel.FirstName = Convert.ToString(objDbDataReader["FirstName"]);
                            objEmployeeInformationViewModel.LastName = Convert.ToString(objDbDataReader["LastName"]);
                            objEmployeeInformationViewModel.DesigId = Convert.ToInt32(objDbDataReader["DesigId"]);
                            objEmployeeInformationViewModel.FunctionalId = Convert.ToInt32(objDbDataReader["FunctionalId"]);
                            objEmployeeInformationViewModel.DeptId = Convert.ToInt32(objDbDataReader["DeptId"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["JoiningDate"])))
                            {
                                objEmployeeInformationViewModel.JoiningDate = Convert.ToString(objDbDataReader["JoiningDate"]);
                            }
                            objEmployeeInformationViewModel.MaritalStatus = Convert.ToString(objDbDataReader["MaritalStatus"]);
                            objEmployeeInformationViewModel.Gender = Convert.ToString(objDbDataReader["Gender"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["DOB"])))
                            {
                                objEmployeeInformationViewModel.DOB = Convert.ToString(objDbDataReader["DOB"]);
                            }
                            objEmployeeInformationViewModel.EmailId = Convert.ToString(objDbDataReader["EmailId"]);
                            objEmployeeInformationViewModel.Religion = Convert.ToString(objDbDataReader["Religion"]);
                            objEmployeeInformationViewModel.FatherName = Convert.ToString(objDbDataReader["FatherName"]);
                            objEmployeeInformationViewModel.MotherName = Convert.ToString(objDbDataReader["MotherName"]);
                            objEmployeeInformationViewModel.MobileNo = Convert.ToString(objDbDataReader["MobileNo"]);
                            objEmployeeInformationViewModel.NID = Convert.ToString(objDbDataReader["NID"]);
                            objEmployeeInformationViewModel.PresentAddress = Convert.ToString(objDbDataReader["PresentAddress"]);
                            objEmployeeInformationViewModel.PermanentAddress = Convert.ToString(objDbDataReader["PermanentAddress"]);
                            if (!Convert.IsDBNull(objDbDataReader["ImagePath"]))
                            {
                                var imagePath = objDbDataReader["ImagePath"].ToString();
                                if (imagePath.Contains("Uploads"))
                                {
                                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                    imagePath = "/" + imagePath;
                                    objEmployeeInformationViewModel.ImagePath = imagePath;
                                }
                                else
                                {
                                    objEmployeeInformationViewModel.ImagePath = imagePath;
                                }

                            }
                            else
                            {
                                objEmployeeInformationViewModel.ImagePath = "\\Not Found\\";
                            }
                            objEmployeeInformationViewModel.BranchId = Convert.ToInt16(objDbDataReader["BranchId"]);
                            objEmployeeInformationViewModel.CompanyId = Convert.ToInt16(objDbDataReader["CompanyId"]);
                            objEmployeeInformationViewModel.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"]);
                            objEmployeeInformationViewModel.CreatedDate = Convert.ToDateTime(objDbDataReader["CreatedDate"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["UpdatedBy"])))
                            {
                                objEmployeeInformationViewModel.UpdatedBy = Convert.ToInt16(objDbDataReader["UpdatedBy"]);
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["UpdatedDate"])))
                            {
                                objEmployeeInformationViewModel.UpdatedDate = Convert.ToDateTime(objDbDataReader["UpdatedDate"]);
                            }
                            objEmployeeInformationViewModel.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                            objEmployeeInformationViewModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"]);
                            objEmployeeInformationViewModel.EmployeeName = Convert.ToString(objDbDataReader["EmployeeName"]);
                            objEmployeeInformationViewModel.DepartmentName = Convert.ToString(objDbDataReader["DepartmentName"]);
                            objEmployeeInformationViewModel.DesignationName = Convert.ToString(objDbDataReader["DesignationName"]);
                            objEmployeeInformationViewModel.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                            employees.Add(objEmployeeInformationViewModel);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return employees;
        }

        public bool GetEmpCodeCheck(string EmpCode)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("EmpCode", EmpCode);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[HR_GetEmpCodeCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetEmpNIDCheck(string nid)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("NID", nid);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[HR_GetEmpNIDCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
        #endregion

        #region Educational Info
        public CommonResult SaveEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmpCode", oEmployeeEducation.EmpCode);
            objDbCommand.AddInParameter("PassingYear", oEmployeeEducation.PassingYear);
            objDbCommand.AddInParameter("Grade", oEmployeeEducation.Grade);
            objDbCommand.AddInParameter("InstituteName", oEmployeeEducation.InstituteName);
            objDbCommand.AddInParameter("Description", oEmployeeEducation.Description);
            objDbCommand.AddInParameter("DegreeName", oEmployeeEducation.DegreeName);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SaveEmployeeEducationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<EmployeeEducation> GetEmployeeEducationalInfo(string EmpCode)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeEducation> educations = new List<EmployeeEducation>();
            EmployeeEducation oEmployeeEducation;
            try
            {
                {
                    objDbCommand.AddInParameter("EmpCode", EmpCode);
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetEmployeeEducationalInfo]", CommandType.StoredProcedure);
                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            oEmployeeEducation = new EmployeeEducation();
                            oEmployeeEducation.Id = Convert.ToInt64(objDbDataReader["Id"]);
                            oEmployeeEducation.EmpCode = Convert.ToString(objDbDataReader["EmpCode"]);
                            oEmployeeEducation.PassingYear = Convert.ToInt16(objDbDataReader["PassingYear"]);
                            oEmployeeEducation.Grade = Convert.ToString(objDbDataReader["Grade"]);
                            oEmployeeEducation.InstituteName = Convert.ToString(objDbDataReader["InstituteName"]);
                            oEmployeeEducation.Description = Convert.ToString(objDbDataReader["Description"]);
                            oEmployeeEducation.DegreeName = Convert.ToString(objDbDataReader["DegreeName"]);
                            educations.Add(oEmployeeEducation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return educations;
        }

        public CommonResult UpdateEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", oEmployeeEducation.Id);
            objDbCommand.AddInParameter("EmpCode", oEmployeeEducation.EmpCode);
            objDbCommand.AddInParameter("PassingYear", oEmployeeEducation.PassingYear);
            objDbCommand.AddInParameter("Grade", oEmployeeEducation.Grade);
            objDbCommand.AddInParameter("InstituteName", oEmployeeEducation.InstituteName);
            objDbCommand.AddInParameter("Description", oEmployeeEducation.Description);
            objDbCommand.AddInParameter("DegreeName", oEmployeeEducation.DegreeName);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_UpdateEmployeeEducationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", oEmployeeEducation.Id);
            objDbCommand.AddInParameter("EmpCode", oEmployeeEducation.EmpCode);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_DeleteEmployeeEducationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        #endregion

        #region Experience Info
        public CommonResult SaveEmployeeExperinceInfo(EmployeeExperience oEmployeeExperience)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmpCode", oEmployeeExperience.EmpCode);
            objDbCommand.AddInParameter("InstituteName", oEmployeeExperience.InstituteName);
            objDbCommand.AddInParameter("YearOfExperience", oEmployeeExperience.YearOfExperience);
            objDbCommand.AddInParameter("Position", oEmployeeExperience.Position);
            objDbCommand.AddInParameter("From", oEmployeeExperience.From);
            objDbCommand.AddInParameter("To", oEmployeeExperience.To);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SaveEmployeeExperinceInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public List<EmployeeExperience> GetEmployeeExperinceInfo(string EmpCode)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeExperience> experiences = new List<EmployeeExperience>();
            EmployeeExperience oEmployeeExperience;
            try
            {
                {
                    objDbCommand.AddInParameter("EmpCode", EmpCode);
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetEmployeeExperinceInfo]", CommandType.StoredProcedure);
                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            oEmployeeExperience = new EmployeeExperience();
                            oEmployeeExperience.Id = Convert.ToInt64(objDbDataReader["Id"]);
                            oEmployeeExperience.EmpCode = Convert.ToString(objDbDataReader["EmpCode"]);
                            oEmployeeExperience.InstituteName = Convert.ToString(objDbDataReader["InstituteName"]);
                            oEmployeeExperience.YearOfExperience = Convert.ToString(objDbDataReader["YearOfExperience"]);
                            oEmployeeExperience.Position = Convert.ToString(objDbDataReader["Position"]);
                            oEmployeeExperience.From = Convert.ToString(objDbDataReader["From"]);
                            oEmployeeExperience.To = Convert.ToString(objDbDataReader["To"]);
                            experiences.Add(oEmployeeExperience);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return experiences;
        }

        public CommonResult UpdateEmployeeExperinceInfo(EmployeeExperience oEmployeeExperience)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", oEmployeeExperience.Id);
            objDbCommand.AddInParameter("EmpCode", oEmployeeExperience.EmpCode);
            objDbCommand.AddInParameter("InstituteName", oEmployeeExperience.InstituteName);
            objDbCommand.AddInParameter("YearOfExperience", oEmployeeExperience.YearOfExperience);
            objDbCommand.AddInParameter("Position", oEmployeeExperience.Position);
            objDbCommand.AddInParameter("From", oEmployeeExperience.From);
            objDbCommand.AddInParameter("To", oEmployeeExperience.To);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_UpdateEmployeeExperinceInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteEmployeeExperienceInfo(EmployeeExperience oEmployeeExperience)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", oEmployeeExperience.Id);
            objDbCommand.AddInParameter("EmpCode", oEmployeeExperience.EmpCode);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_DeleteEmployeeExperienceInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        #endregion

        public string ApproveEmployee(string[] ch)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("EmpId", id);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[HR_ApproveEmployee]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public string GetEmployeeCode()
        {
            var empcode = string.Empty;
            int empId = 0;
            string data = "100";
            int affectedRows = DeleteExistingTempCode(data);
            var year = (DateTime.Now.Year.ToString());
            
            var month = DateTime.Now.Month.ToString();
            month = month.PadLeft(2, '0');
            var day = DateTime.Now.Day.ToString();
            day = day.PadLeft(2, '0');
            var prefix = string.Concat(year, month, day);
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            //objDbCommand.AddInParameter("Prefix", prefix);
            //objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetEmployeeCode]",
            //            CommandType.StoredProcedure);

            objDbCommand.AddInParameter("FieldName", "EmpCode");
            objDbCommand.AddInParameter("TableName", "EmployeeInformation");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                        CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {

                    //empcode = objDbDataReader["EmpCode"].ToString();
                    //if(empcode == "")
                    //{
                    //    empcode = string.Concat(prefix, "01");
                    //    empId = Convert.ToInt32(empcode);
                    //    empcode = empId.ToString();
                    //}
                    //else
                    //{
                    //    var epc = empcode.Substring(0,8);
                    //    if (epc == prefix)
                    //    {
                    //        empId = Convert.ToInt32(empcode) + 01;
                    //        empcode = empId.ToString();
                    //    }
                    //    else
                    //    {
                    //        empcode = string.Concat(prefix, "01");
                    //        empId = Convert.ToInt32(empcode);
                    //        empcode = empId.ToString();
                    //    }
                    //}

                    empcode = prefix;
                    empId = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    empcode += Convert.ToString(empId).PadLeft(2, '0');
                }
            }
            affectedRows = SavempCode(empcode, data);
            return empcode;
        }

        private int DeleteExistingTempCode(string data)
        {
            int rowsAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            try
            {
                objDbCommand.AddInParameter("data", data);
                rowsAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_DeleteExistingTempCode]", CommandType.StoredProcedure);
                if (rowsAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return rowsAffected;
        }

        public int SavempCode(string empCode, string CreatedBy)
        {
            int rowsAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            try
            {
                objDbCommand.AddInParameter("empCode", empCode);
                objDbCommand.AddInParameter("CreatedBy", CreatedBy);
                rowsAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SavempCode]", CommandType.StoredProcedure);
                if (rowsAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return rowsAffected;
        }

    }
}