﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    public class NoticeInfoBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private CommonResult oCommonResult;
        public List<NoticeInfo> GetAllNoticeInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<NoticeInfo> infoList = new List<NoticeInfo>();
            NoticeInfo objI;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Settings_GetAllNoticeInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objI = new NoticeInfo();
                        objI.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        objI.NoticeTitle = Convert.ToString(objDbDataReader["NoticeTitle"]);
                        objI.NoticeContent = Convert.ToString(objDbDataReader["NoticeContent"]);
                        objI.NoticeFileName = Convert.ToString(objDbDataReader["NoticeFileName"]);
                        objI.NoticeFileExtension = Convert.ToString(objDbDataReader["NoticeFileExtension"]);
                        objI.RoleId = Convert.ToInt32(Convert.ToString(objDbDataReader["RoleId"]));
                        infoList.Add(objI);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return infoList;
        }
        public CommonResult UpdateNoticeInfo(NoticeInfo obj)
        {
            oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int rowAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", obj.Id);
            objDbCommand.AddInParameter("NoticeTitle", obj.NoticeTitle);
            objDbCommand.AddInParameter("NoticeContent", obj.NoticeContent);
            objDbCommand.AddInParameter("NoticeFileName", obj.NoticeFileName);
            objDbCommand.AddInParameter("NoticeFileExtension", obj.NoticeFileExtension);
            objDbCommand.AddInParameter("UpdatedBy", Convert.ToByte(SessionUtility.TMSessionContainer.UserID));
            objDbCommand.AddInParameter("UpdatedDate", DateTime.Now);
            objDbCommand.AddInParameter("RoleId", obj.RoleId);
            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Settings_UpdateNoticeInfo]",
                    CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public CommonResult DeleteNoticeInfo(int id)
        {
            oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("Id", id);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Settings_DeleteNoticeInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult SaveNoticeInfo(NoticeInfo obj)
        {
            oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int rowAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("NoticeTitle", obj.NoticeTitle);
            objDbCommand.AddInParameter("NoticeContent", obj.NoticeContent);
            objDbCommand.AddInParameter("NoticeFileName", obj.NoticeFileName);
            objDbCommand.AddInParameter("NoticeFileExtension", obj.NoticeFileExtension);
            objDbCommand.AddInParameter("CreatedBy", Convert.ToByte(SessionUtility.TMSessionContainer.UserID));
            objDbCommand.AddInParameter("CreatedDate", DateTime.Now);
            objDbCommand.AddInParameter("RoleId", obj.RoleId);
            try
            {
                rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[Settings_SaveNoticeInfo]",
                    CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

    }
}