﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.Areas.Member.BLL;
using TonyMultipurpose.Areas.Member.Models;

namespace TonyMultipurpose.Areas.Member.Controllers
{
    //ataur
    public class FamilyController : Controller
    {
        // GET: Member/Family
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save()
        {
            return View("Index");
        }

        public JsonResult GetAllFamilyDetailByAcc(string accountNumber)
        {
            try
            {
                FamilyDetailBLL objFamilyDetailBLL = new FamilyDetailBLL();
                if (accountNumber != null)
                {
                    List<FamilyDetail> family = objFamilyDetailBLL.GetAllFamilyDetailByAcc(accountNumber);
                    return Json(family, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit()
        {
            return View();
        }
        public ActionResult Delete()
        {
            return View();
        }
    }
}