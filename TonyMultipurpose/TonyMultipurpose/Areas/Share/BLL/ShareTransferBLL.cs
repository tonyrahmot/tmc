﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using TonyMultipurpose.Areas.Share.ViewModels;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Share.BLL
{
    public class ShareTransferBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;

        private void BuildJpModel<T>(DbDataReader objDataReader, T item) where T : new()
        {
            for (var inc = 0; inc < objDataReader.FieldCount; inc++)
            {
                var type = item.GetType();
                var prop = type.GetProperty(objDataReader.GetName(inc));
                var val = objDataReader.GetValue(inc) is DBNull ? null : objDataReader.GetValue(inc);
                prop?.SetValue(item, val, null);
            }
        }

        public ShareHoldingViewModel GetShareHoldingInfo(int companyId)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var model = new ShareHoldingViewModel();
            try
            {
                _objDbCommand.AddInParameter("CompanyId", companyId);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand,
                    "[dbo].[Share_GetShareHoldingInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        BuildJpModel(objDbDataReader, model);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                _objDataAccess.Dispose(_objDbCommand);
            }
            return model;
        }

        public List<CustomerViewModel> GetCustomerInfoWithExistingShareQtyByCustomerId(CustomerViewModel customerModel)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objCustomerModelList = new List<CustomerViewModel>();
            try
            {
                _objDbCommand.AddInParameter("CustomerId", customerModel.CustomerId);
                _objDbCommand.AddInParameter("CompanyId", customerModel.CompanyId);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand,
                    "[dbo].[Share_GetCustomerInfoWithExistingShareQtyByCustomerId]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        var objCustomerModel = new CustomerViewModel();
                        BuildJpModel(objDbDataReader, objCustomerModel);
                        var imagePath = objCustomerModel.CustomerImage;

                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objCustomerModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objCustomerModel.CustomerImage = imagePath;
                        }
                        objCustomerModelList.Add(objCustomerModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                _objDataAccess.Dispose(_objDbCommand);
            }
            return objCustomerModelList;
        }

        public string SaveShareTransaction(ShareTransferViewModel objShareTransaction)
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var Id = 0;

            var year = Convert.ToString(DateTime.Now.Year);
            var days = Convert.ToString(DateTime.Now.DayOfYear);
            var prefix = string.Concat(year, days);

            _objDbCommand.AddInParameter("FieldName", "TransactionId");
            _objDbCommand.AddInParameter("TableName", "ShareTransaction");
            _objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("CompanyId", objShareTransaction.CompanyId);
            _objDbCommand.AddInParameter("TransactionId", transactionId);
            _objDbCommand.AddInParameter("FromCustomerId", objShareTransaction.FromCustomerId);
            _objDbCommand.AddInParameter("ToCustomerId", objShareTransaction.ToCustomerId);
            _objDbCommand.AddInParameter("PricePerShare", objShareTransaction.PricePerShare);
            _objDbCommand.AddInParameter("Qty", objShareTransaction.Qty);
            _objDbCommand.AddInParameter("IssuedDate", objShareTransaction.IssuedDate);
            _objDbCommand.AddInParameter("VoucherNo", objShareTransaction.VoucherNo);
            _objDbCommand.AddInParameter("FromParticular", objShareTransaction.FromParticular);
            _objDbCommand.AddInParameter("ToParticular", objShareTransaction.ToParticular);
            _objDbCommand.AddInParameter("BranchId", objShareTransaction.BranchId);
            _objDbCommand.AddInParameter("CreatedBy", objShareTransaction.CreatedBy);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_SaveShareTransferInfo]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    return "Saved";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }


            catch (Exception ex)
            {
                _objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
        }

        public List<string> GetAllCustomerIdForAutocomplete(string term)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objCustomerList = new List<string>();
            try
            {
                _objDbCommand.AddInParameter("CustomerId", term);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[Autocomplete_ApprovedCustomerId]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["CustomerId"] != null)
                        {
                            objCustomerList.Add(objDbDataReader["CustomerId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objCustomerList;
        }
    }
}