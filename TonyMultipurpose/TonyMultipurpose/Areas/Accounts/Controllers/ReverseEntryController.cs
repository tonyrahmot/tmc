﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Accounts.Controllers
{
    [AuthenticationFilter]
    public class ReverseEntryController : Controller
    {
        private readonly ReverseEntryBLL _onjReverseEntryBll;
        private AccountsViewModel _objAccountViewModel = null;
        public ReverseEntryController()
        {
            _onjReverseEntryBll = new ReverseEntryBLL();
        }

        [HttpGet]
        public ActionResult Index()
        {
            _objAccountViewModel = new AccountsViewModel
            {
                CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId),
                TransactionDate=DateTime.Now.Date
            };
            var allAccountTransactions = _onjReverseEntryBll.GetAllTransactions(_objAccountViewModel).ToList();
            return View(allAccountTransactions);
        }

        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            List<AccountsViewModel> allAccountTransactions = null;
            var transDate = Request.Form["TransactionDate"];
            if (string.IsNullOrEmpty(transDate))
                transDate = DateTime.Now.ToString("dd/MMM/yyyy");
            ViewBag.TransactionDate = transDate;
            _objAccountViewModel = new AccountsViewModel
            {
                CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId),
                TransactionDate = DateTime.ParseExact(transDate, "dd/MMM/yyyy", null)
            };
            allAccountTransactions = _onjReverseEntryBll.GetAllTransactions(_objAccountViewModel).ToList();
            return View(allAccountTransactions);
        }

        [HttpPost]
        public JsonResult SaveReverseEntry(string transactionIdToReverse)
        {
            bool status = true;

            string transactionId = transactionIdToReverse.Split('_')[0].ToString();
            string accountTypeId = transactionIdToReverse.Split('_')[1].ToString();
            int createdBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            _onjReverseEntryBll.SaveTransactionForReverseEntry(transactionId, accountTypeId, createdBy);

            return new JsonResult { Data = new { status = status } };
        }
    }
}