﻿namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class LoanTypeViewModel
    {
        public byte LoanTypeId { get; set; }
        public string LoanTypeName { get; set; }
    }
}