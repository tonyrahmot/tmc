$(function () {
    $("#tabs").tabs();
});
(function () {

    $('#clear').click(function () {
        window.location.reload();
    });



    $('#gaurantorInfoTab').on('click', function () {
        $('#gaurantorInfo').load(window.applicationBaseUrl + "Loan/ApprovedLoanList/Guarantor?loanId=" + $('#hdfLoanId').val());
    });


    $('#commentsAndSignatoryInfoTab').on('click', function () {
        $('#CommentsAndSignatory').load(window.applicationBaseUrl + "Loan/ApprovedLoanList/CommentsAndSignatory?loanId=" + $('#hdfLoanId').val());
    });

    $('#customersChequeInfoTab').on('click', function () {
        $('#CustomersCheque').load(window.applicationBaseUrl + "Loan/ApprovedLoanList/CustomersChequeInfo?loanId=" + $('#hdfLoanId').val());
    });


    $('#financialInfoTab').on('click', function () {
        $('#Financial').load(window.applicationBaseUrl + "Loan/ApprovedLoanList/FinancialJustification?loanId=" + $('#hdfLoanId').val());
    });



})();
