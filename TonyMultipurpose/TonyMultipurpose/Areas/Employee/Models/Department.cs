﻿using TonyMultipurpose.Models;
using System.ComponentModel.DataAnnotations;


namespace TonyMultipurpose.Areas.Employee.Models
{
    public class Department : CommonModel
    {
        [Key]
        public byte DepartmentId { get; set; }

        [Required]
        [Display(Name = "Department Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Department Name should between at least 2 to 100 characters long.")]
        public string DepartmentName { get; set; }
    }
}