    var emiTotal;
var oAdditionalTable;
(function () {
    var z;
    ShowData();
    function ShowData() {
        oAdditionalTable = $('#AdditionalTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/ApprovedLoanList/GetAdditionalLoanInfoById",
                "type": "get",
                "data": {
                    "LoanId": $('#MonthlyIncome_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": "AdditionalLoanDetailId",
                    "width":"25px",
                    "render": function (data) {
                        return '<input class="form-control" id="AdditionalLoanDetailId" name="AdditionalLoanDetailId" type="text" readonly="readonly"  value = "' + data + '"  >';
                    }
                },
                {
                    "data": "InstituteName",
                    "width": "130px",
                    "render": function (data) {
                        return '<input class="form-control" id="InstituteName" name="InstituteName" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "LoanAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanAmount" name="LoanAmount" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "LoanType",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanType" name="LoanType" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "Installment",
                    "render": function (data) {
                        return '<input class="form-control" id="Installment" name="Installment" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "AccountNo",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountNo" name="AccountNo" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "AccountName",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountName" name="AccountName" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "StartDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        // bindDatePickerforstdate();
                        return '<input class="form-control" id="StartDate" name="StartDate" type="text" readonly="readonly" value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "ExpireDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        //  bindDatePickerforexdate();
                        return '<input class="form-control" id="ExpireDate" name="ExpireDate" type="text" readonly="readonly" value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "PresentOutAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="PresentOutAmount" name="PresentOutAmount" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                }
            ],
            ordering: false,
            searching: false,
            paging: false,
            "bInfo": false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                emiTotal = api
                    .column(4, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                var PresentOut = api
                    .column(9, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                    emiTotal
                );

                $(api.column(9).footer()).html(
                    PresentOut
                );
                z = emiTotal;
            }
        });
    };

    Finance();

})();

/**********************************************************************/

function Finance() {
    var data = {
        LoanId: $('#MonthlyIncome_LoanId').val()
    }
    $.ajax({
        url: window.applicationBaseUrl + "Loan/ApprovedLoanList/Finance", 
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (obj) {
            if (obj.loanHistoryInfo.InstallmentTypeId === 1) {
                document.getElementById('labelmonthlyInstallmentType').innerHTML = 'Monthly';
                document.getElementById('labelweeklyInstallmentType').innerHTML = obj.loanHistoryInfo.InstallmentTypeName;
            }

            if (obj.loanHistoryInfo.InstallmentTypeId === 2) {
                document.getElementById('labelweeklyInstallmentType').innerHTML = 'Weekly';
                document.getElementById('labelmonthlyInstallmentType').innerHTML = obj.loanHistoryInfo.InstallmentTypeName;
            }
            document.getElementById('labelSurplusIncome').innerHTML = parseFloat(obj.surplusIncome);

            document.getElementById('labelweeklyLoanAmount').innerHTML = obj.loanHistoryInfo.LoanAmount;
            document.getElementById('labelweeklyInterestRate').innerHTML = obj.loanHistoryInfo.InterestRate + '%';
            document.getElementById('labelweeklyNoInstallment').innerHTML = obj.loanHistoryInfo.WeeklyInstallmentNumber;
            document.getElementById('labelweeklyInstallmentAmount').innerHTML = obj.loanHistoryInfo.WeeklyInstallmentAmount;
            document.getElementById('labelweeklyDbr1').innerHTML = obj.loanHistoryInfo.WeeklyDbr + '%';
            document.getElementById('labelweeklyEquity1').innerHTML = obj.loanHistoryInfo.Equity + '%';

            document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.loanHistoryInfo.LoanAmount;
            document.getElementById('labelmonthlyInterestRate').innerHTML = obj.loanHistoryInfo.InterestRate + '%';
            document.getElementById('labelmonthlyNoInstallment').innerHTML = obj.loanHistoryInfo.MonthlyInstallmentNumber;
            document.getElementById('labelmonthlyInstallmentAmount').innerHTML = obj.loanHistoryInfo.MonthlyInstallmentAmount;
            document.getElementById('labelmonthlyDbr1').innerHTML = obj.loanHistoryInfo.MonthlyDbr + '%';
            document.getElementById('labelmonthlyEquity1').innerHTML = obj.loanHistoryInfo.Equity + '%';


            document.getElementById('managementLoanNo').innerHTML = obj.info.LoanNo;
            document.getElementById('managementInterestRate').innerHTML = obj.info.InterestRate + '%';
            document.getElementById('managementLoanAmount').innerHTML = obj.info.LoanAmount;
            document.getElementById('managementInstallmentAmount').innerHTML = obj.info.InstallmentAmount;
            document.getElementById('managementInstallmentNo').innerHTML = obj.info.NumberOfInstallment;
            document.getElementById('LoanDisburseDate').innerHTML = obj.info.LoanDate;
            document.getElementById('LoanExpiryDate').innerHTML = obj.info.LoanExpiryDate;
            document.getElementById('managementInstallmentType').innerHTML = obj.info.InstallmentTypeName;

        },
        error: function () {
        }
    });
}

    var oBankperformanceTable;
BankperformanceTable();

function BankperformanceTable() {
    oBankperformanceTable = $('#BankperformanceTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/ApprovedLoanList/GetAllBankperformanceInfo", 
            "type": "get",
            "data": {
                "LoanId": $('#Bankperformance_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            {
                "data": "BankperformanceId",
                "width":"30px",
                //"visible": false,
                "render": function (data) {
                    return '<input class="form-control" id="BankperformanceId" name="BankperformanceId" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "NgoName",
                "width": "130px",
                "render": function (data) {
                    return '<input class="form-control" id="NgoName" type="text" name="NgoName" readonly="readonly" value =" ' + data + '">';
                }
            },
            {
                "data": "AccNo",
                "render": function (data) {
                    return '<input class="form-control" id="AccNo" name="AccNo" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "FromDate",
                //"visible": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    return '<input class="form-control" id="FromDate" name="FromDate" type="text"  readonly="readonly" value = "' + datadate + ' " >';
                }
            },
            {
                "data": "ToDate",
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    return '<input class="form-control" id="ToDate" name="ToDate" type="text" readonly="readonly"  value = "' + datadate + ' " >';
                }
            },
            {
                "data": "DebitAmount",
                "render": function (data) {
                    return '<input class="form-control" id="DebitAmount" name="DebitAmount" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "CreditAmount",
                "render": function (data) {
                    return '<input class="form-control" id="CreditAmount" name="CreditAmount" type="text" readonly="readonly" value =" ' + data + '">';
                }
            },
            {
                "data": "Avg",
                "render": function (data) {
                    return '<input class="form-control" id="Avg" name="Avg" type="text" readonly="readonly"  value =" ' + Math.round(data) + '">';
                }
            }
        ],
        //"order": [[0, "desc"]],
        ordering: false,
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            var drTotal = api
                .column(5, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var crTotal = api
                .column(6, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var avgTotal = api
                .column(7, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $(api.column(5).footer()).html(
                drTotal
            );
            $(api.column(6).footer()).html(
                crTotal
            );
            $(api.column(7).footer()).html(
                Math.round(avgTotal)
            );
        }
    });
}



    var incomeTotal;
var x;

MonthlyInformatiom();
/// ======== Monthly Informtion========
function MonthlyInformatiom()
{
    var loanId = $('#MonthlyIncome_LoanId').val();
    var data = { loanId: loanId};
    $.ajax({
        url: window.applicationBaseUrl + "Loan/ApprovedLoanList/GetAllMonthlyIncomeInfo", 
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            $('#Amount').val(data.data[0].Amount);
            $('#IncomeType').val(data.data[0].IncomeType);
            $('#OtherIncomeAmount').val(data.data[0].OtherIncomeAmount);
            $('#SpouseAmount').val(data.data[0].SpouseAmount);
            $('#SpouseIncomeType').val(data.data[0].SpouseIncomeType);
            $('#ForeignRemitAmount').val(data.data[0].ForeignRemitAmount);
            $('#TotalAmount').val(data.data[0].TotalIncome);

        }
    });
}


var months = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May',
    'Jun', 'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec'
];

function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}


    var expenseTotal;
var y;

MonthlyExpenseInformatiom();
function MonthlyExpenseInformatiom() {
    var loanId = $('#MonthlyExpense_LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/ApprovedLoanList/GetAllMonthlyExpenseInfo", 
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data1) {
            $('#FamilyExpense').val(data1.data[0].FamilyExpense);
            $('#ChildrenEducation').val(data1.data[0].ChildrenEducation);
            $('#ExistingEMI').val(data1.data[0].ExistingEMI);
            $('#OthersExpense').val(data1.data[0].OthersExpense);
            $('#TotalExpenseAmount').val(data1.data[0].TotalExpense);
        }
    });
}


function NetIncomeCalculation() {
    var Amount = parseFloat($('#Amount').val());
    var SpouseAmount = parseFloat($('#SpouseAmount').val());
    var OtherIncomeAmount = parseFloat($('#OtherIncomeAmount').val());
    var ForeignRemitAmount = parseFloat($('#ForeignRemitAmount').val());
    var totalAmount = parseFloat(SpouseAmount + Amount + OtherIncomeAmount + ForeignRemitAmount);
    $('#TotalAmount').val(totalAmount);
}

function NetExpenseCalculation() {
    var FamilyExpense = parseFloat($('#FamilyExpense').val());
    var ChildrenEducation = parseFloat($('#ChildrenEducation').val());
    var ExistingEMI = parseFloat($('#ExistingEMI').val());
    var OthersExpense = parseFloat($('#OthersExpense').val());
    var totalAmount = parseFloat(FamilyExpense + ChildrenEducation + ExistingEMI + OthersExpense);
    $('#TotalExpenseAmount').val(totalAmount);
}

