$(document).ready(function () {
    var oTable = $('#myDatatable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/LoanInstallment/GetInstallmentInformation",
            "type": "get",
            "datatype": "json"
        },

        "columns": [
            { "data": "Id" },
            { "data": "LoanId" },
            { "data": "InstallmentDate" },
            { "data": "Particular" },
            { "data": "PrincipleAmount" },
            { "data": "InterestAmount" },
            { "data": "InstallmentAmount" },
            { "data": "Outstanding" },
            {
                "data": "Id",
                "width": "50px",
                "render": function (data) {
                    var url = window.applicationBaseUrl + "Loan/LoanInstallment/Edit/" + data;
                    return '<a class="popup" href="' + url + '">Edit</a>';
                }
            },
            {
                "data": "Id",
                "width": "50px",
                "render": function (data) {
                    var deleteUrl = window.applicationBaseUrl + "Accounts/AccountHead/Delete/" + data;
                    return '<a class="popup" href="' + deleteUrl + '">Delete</a>';
                }
            }
        ]
    });
    $('.tablecontainer').on('click', 'a.popup', function (e) {
        e.preventDefault();
        OpenPopup($(this).attr('href'));
    });

    function OpenPopup(pageUrl) {
        var $pageContent = $('<div/>');
        $pageContent.load(pageUrl, function () {
            $('#popupForm', $pageContent).removeData('validator');
            $('#popupForm', $pageContent).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');

        });

        $dialog = $('<div class="popupWindow"  style="color:Black;"></div>')
            .html($pageContent)
            .dialog({
                draggable: true,
                autoOpen: false,
                resizable: true,
                model: true,

                title: '@ViewBag.Title',
                height: 650,
                width: 600,
                close: function () {

                    $dialog.dialog('destroy').remove();
                }
            });

        $('.popupWindow').on('submit', '#popupForm', function (e) {
            var url = $('#popupForm')[0].action;
            $.ajax({
                type: "POST",
                url: url,
                data: $('#popupForm').serialize(),
                success: function (data) {
                    if (data.status) {
                        $dialog.dialog('close');
                        oTable.ajax.reload();
                    }
                }
            });

            e.preventDefault();
        });

        $dialog.dialog('open');
    }
})