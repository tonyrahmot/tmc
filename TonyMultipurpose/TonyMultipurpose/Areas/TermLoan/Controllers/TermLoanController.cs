﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.TermLoan.BLL;
using TonyMultipurpose.Areas.TermLoan.Models;

namespace TonyMultipurpose.Areas.TermLoan.Controllers
{
    public class TermLoanController : Controller
    {
        // GET: TermLoan/Loan
        TermLoanBLL objTermLoanBLL = new TermLoanBLL();
        public ActionResult Index()
        {
            // UniqidGenerate part


            string UniqueNumber = string.Empty;

            string data = "temp";            
            int RowAffacted = objTermLoanBLL.DeleteHasTempRow(data);

            string loanUnique = "LD";           
            string CurrentYear = (DateTime.Now.Year.ToString()).Substring(2, 2);
            string CurrentDayThisYear = DateTime.Now.DayOfYear.ToString();
            string existingCheck = string.Concat(loanUnique, CurrentYear, CurrentDayThisYear);
            string count = objTermLoanBLL.ExistingCheckUniqueNumber(existingCheck);
            if (!string.IsNullOrEmpty(count))
            {
                int Number = Convert.ToInt16(count.Substring(3, 1)) + 1;
                string lastNumber = string.Concat("000", Number);
                UniqueNumber = string.Concat(loanUnique, CurrentYear, CurrentDayThisYear, lastNumber);
                RowAffacted = objTermLoanBLL.UniqueNumberSave(UniqueNumber, data);
            }
            else
            {
                string StartUniqueNumber = "0001".ToString();
                UniqueNumber = string.Concat(loanUnique, CurrentYear, CurrentDayThisYear, StartUniqueNumber);
                RowAffacted = objTermLoanBLL.UniqueNumberSave(UniqueNumber, data);
            }
            ViewBag.showData = UniqueNumber;

            return View();
        }


        [HttpPost]
        public ActionResult SaveGuarantor(Guarantor objGuarantor)
        {
            objTermLoanBLL.SaveGuarontorInfo(objGuarantor);
            bool status = true;
            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public JsonResult AutoCompleteCustomerIdForGuarantor(string term)
        {
            var result = objTermLoanBLL.GetAutoCompleteCustomerIdForGuarantor(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetGuarantorInfoByCustomerId(string customerId)
        {
            var data = objTermLoanBLL.GetGuarantorInfoByCustomerId(customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Edit(int? GuarantorId)
        {
            var employee = objTermLoanBLL.GetGuarantorInfoForUpdate(GuarantorId);
            ViewData["MyProduct"] = employee;
            return View("Index");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken()]
        //public ActionResult Edit(EmployeeDetail objEmployeeInfo)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();

        //        objEmployeeDetailBLL.UpdateEmployeeDetailInfo(objEmployeeInfo);

        //    }
        //    return RedirectToAction("Index", "EmployeeDetail");
        //}



    }
}