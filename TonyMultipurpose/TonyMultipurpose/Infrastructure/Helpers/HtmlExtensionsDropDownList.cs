﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsDropDownList
    {


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          object htmlAttributes = null
          )
        {
            return BootstrapDropDownListFor(htmlHelper, expression,
                selectListItems, null, null, false,
                false, null, htmlAttributes);

        }


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          string optionalLabel,
          object htmlAttributes = null
          )
        {
            return BootstrapDropDownListFor(htmlHelper, expression,
                selectListItems, optionalLabel, null, false,
                false, null, htmlAttributes);

        }


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          string optionalLabel,
          string title,
          object htmlAttributes = null
          )
        {
            return BootstrapDropDownListFor(htmlHelper, expression,
                selectListItems, optionalLabel, title, false,
                false, null, htmlAttributes);

        }


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          string optionalLabel,
          string title,
          bool isRequired,
          object htmlAttributes = null
          )
        {
            return BootstrapDropDownListFor(htmlHelper, expression,
                selectListItems, optionalLabel, title, isRequired,
                false, null, htmlAttributes);

        }


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          string optionalLabel,
          string title,
          bool isRequired,
          bool isAutoFocus,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            return BootstrapDropDownListFor(htmlHelper, expression,
                selectListItems, optionalLabel, title, isRequired,
                isAutoFocus, null, htmlAttributes);

        }


        public static MvcHtmlString BootstrapDropDownListFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          IEnumerable<SelectListItem> selectListItems,
          string optionalLabel,
          string title,
          bool isRequired,
          bool isAutoFocus,
          string cssClass,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // Add all other attributes below here
            if (!string.IsNullOrWhiteSpace(title))
            {
                rvd.Add("title", title);
            }
            if (isRequired)
            {
                rvd.Add("required", "required");
            }
            if (isAutoFocus)
            {
                rvd.Add("autofocus", "autofocus");
            }
            if (string.IsNullOrWhiteSpace(cssClass))
            {
                cssClass = "form-control input-sm ";
            }
            else
            {
                cssClass = "form-control input-sm " + cssClass;
            }
            rvd.Add("class", cssClass);

            if (string.IsNullOrEmpty(optionalLabel))
            {
                return htmlHelper.DropDownListFor(expression, selectListItems, rvd);
            }
            return htmlHelper.DropDownListFor(expression, selectListItems, optionalLabel, rvd);
        }
    }
}