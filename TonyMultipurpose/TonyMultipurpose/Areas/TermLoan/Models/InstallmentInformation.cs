﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.TermLoan.Models
{
    public class InstallmentInformation : CommonModel
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }
        [Display(Name = "Entry Date")]
        public string EntryDate { get; set; }
        [Display(Name = "Installment Date")]
        public string InstallmentDate { get; set; }
        public string Particular { get; set; }
        public string Outstanding { get; set; }
        [Display(Name = "Installment Amount")]
        public decimal? InstallmentAmount { get; set; }
        [Display(Name = "Interest Amount")]
        public decimal? InterestAmount { get; set; }
        [Display(Name = "Deposited Amount")]
        public decimal? DepositedAmount { get; set; }
        [Display(Name = "Principle Amount")]
        public decimal? PrincipleAmount { get; set; }
        public short? BranchId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string FatherName { get; set; }
        public string CustomerImage { get; set; }
        public decimal? ExtraFee { get; set; }
        public byte NumberOfInstallment { get; set; }
    }
}