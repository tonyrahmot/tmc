﻿using System;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.BLL;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Controllers
{
    [AuthenticationFilter]
    public class ReportsController : Controller
    {
        // GET: Reports
        ReportsBLL objReportsBLL = new ReportsBLL();
        public ActionResult Index()
        {
            BranchBLL objBranchBll = new BranchBLL();
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();

            //load the report information

            return View();
        }
        [HttpPost]
        public void Index(Reports objReportModel)
        {
            if (ModelState.IsValid)
            {
                var accountCode = string.Empty;
                if (!string.IsNullOrEmpty(objReportModel.AccountCode))
                {
                    accountCode = objReportModel.AccountCode.Contains("-") ? objReportModel.AccountCode.Substring(0, objReportModel.AccountCode.IndexOf("-", StringComparison.Ordinal)) : objReportModel.AccountCode;
                }
                ReportObject.ReportName = objReportModel.ReportName;
                ReportObject.CustomerId = objReportModel.CustomerId;
                ReportObject.BranchId = objReportModel.BranchId;
                ReportObject.AccountNumber = objReportModel.AccountNumber;
                ReportObject.AccountType = objReportModel.AccountType;
                ReportObject.FromDate = Convert.ToString(objReportModel.FromDate);
                ReportObject.ToDate = Convert.ToString(objReportModel.ToDate);
                ReportObject.LoanId = objReportModel.LoanId;
                ReportObject.LoanNo = objReportModel.LoanNo;
                ReportObject.AccountCode = accountCode;
                ReportObject.ReportFormat = objReportModel.ReportFormat;

                Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
            }
            //return View();
        }
        public JsonResult GetAccontTypeByAccountTitle()
        {
            var data = objReportsBLL.GetAccontTypeByAccountTitle();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult AutoCompleteCustomerId(string term)
        {
            var result = objReportsBLL.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objReportsBLL.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult AutoCompleteAccountCode(string term)
        {
            var result = objReportsBLL.AutoCompleteAccountCode(term);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult AutoCompleteLoanId(string term)
        {
            var result = objReportsBLL.GetAllLoanIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        public JsonResult GetAccountNoByCustomerId(string CustomerId)
        {
            var data = objReportsBLL.GetAccountNoByCustomerId(CustomerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}