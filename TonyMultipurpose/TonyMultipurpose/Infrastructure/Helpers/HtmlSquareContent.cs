﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlSquareContent : IDisposable
    {
        private readonly ViewContext _viewContext;

        public HtmlSquareContent(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public void Dispose()
        {
            _viewContext.Writer.Write(
                "</div>" +
                "</div>" +
                "</a>" +
                "</div>"
            );
        }
    }
}