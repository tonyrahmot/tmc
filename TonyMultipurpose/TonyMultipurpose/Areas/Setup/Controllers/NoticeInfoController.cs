﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.BLL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    public class NoticeInfoController : Controller
    {
        // GET: Setup/NoticeInfo

        CommonResult oCommonResult;
        NoticeInfoBLL objNoticeBll;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            objNoticeBll = new NoticeInfoBLL();
            var model = objNoticeBll.GetAllNoticeInfo().FirstOrDefault(n => n.Id == id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(NoticeInfo obj)
        {
            oCommonResult = new CommonResult();
            objNoticeBll = new NoticeInfoBLL();
            if (ModelState.IsValid)
            {
                obj.RoleId = obj.RoleId == 0 ? null : obj.RoleId;
                if (obj.Id == 0)
                {
                    if (obj.NoticeFile != null)
                    {
                        var rootPath = Server.MapPath("~/Uploads/Notices/");
                        var fileName = Guid.NewGuid().ToString();
                        var file = Path.GetFileName(obj.NoticeFile.FileName);
                        var fileExtension = Path.GetExtension(file);
                        fileName += fileExtension;
                        var path = Path.Combine(rootPath, fileName);
                        obj.NoticeFile.SaveAs(path);
                        obj.NoticeFileName = fileName;
                        obj.NoticeFileExtension = fileExtension;
                    }
                    oCommonResult = objNoticeBll.SaveNoticeInfo(obj);
                }
                else
                {
                    if (obj.NoticeFile != null)
                    {
                        var rootPath = Server.MapPath("~/Uploads/Notices/");
                        var fileName = Guid.NewGuid().ToString();
                        var file = Path.GetFileName(obj.NoticeFile.FileName);
                        var fileExtension = Path.GetExtension(file);
                        fileName += fileExtension;
                        var path = Path.Combine(rootPath, fileName);
                        obj.NoticeFile.SaveAs(path);
                        obj.NoticeFileName = fileName;
                        obj.NoticeFileExtension = fileExtension;
                    }
                    oCommonResult = objNoticeBll.UpdateNoticeInfo(obj);
                }
            }
            else
            {
                oCommonResult.CustomErrorMesg = "Please insert valid data.";
            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteNoticeInfo(int id)
        {
            objNoticeBll = new NoticeInfoBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objNoticeBll.DeleteNoticeInfo(id);
            return new JsonResult { Data = new { oCommonResult } };
        }

        public JsonResult GetAllNoticeInfo()
        {
            objNoticeBll = new NoticeInfoBLL();
            List<NoticeInfo> info = objNoticeBll.GetAllNoticeInfo();
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoleData()
        {
            RoleBLL objRoleBll = new RoleBLL();
            var data = objRoleBll.GetAllRoleInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}