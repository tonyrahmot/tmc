﻿(function () {

    $('#submit1').hide();
    mobileBillTable();
    var oTable;

    function mobileBillTable() {
        oTable = $('#MobileBillTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveMobileBillInfo",//'@Url.Action("SaveMobileBillInfo", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                  { "data": "EmployeeName" },
                { "data": "MobileBill" },
                {
                    "data": "BillDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "Description" }
            ],
            "order": [[0, "desc"]]
        });
        $('#MobileBillTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        document.getElementById("EmpId").readOnly = true;
        $('#EmpId').val(data.EmpCode);
        $('#MobileBill').val(data.MobileBill);

        var date = new Date(data.BillDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var billDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#BillDate').val(billDate);
        $('#Description').val(data.Description);
        $("#EmpId").change();
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    $('#submit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                MobileBill: $('#MobileBill').val(),
                BillDate: $('#BillDate').val(),
                Description: $('#Description').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveMobileBillInfo",//'@Url.Action("SaveMobileBillInfo", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#MobileBill').val('');
                        $('#BillDate').val('');
                        $('#Description').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#submit1').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                MobileBill: $('#MobileBill').val(),
                BillDate: $('#BillDate').val(),
                Description: $('#Description').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateEmpMobileBillInfo",//'@Url.Action("UpdateEmpMobileBillInfo", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#MobileBill').val('');
                        $('#BillDate').val('');
                        $('#Description').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    //document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });

    $("#EmpId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpId").change(function () {
        $('#EmployeeName').empty();
        var EmpId = $("#EmpId").val();
        var json = { EmpId: EmpId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('EmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('Designation').innerHTML = data.LastName;
                document.getElementById('Center').innerHTML = data.FirstName;
                document.getElementById('NID').innerHTML = data.NID;
            }
        });
    });


    function checkValidation() {
        var isValid = true;
        if ($('#EmpId').val().trim() === '') {
            isValid = false;
            $('#EmpId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#MobileBill').val().trim() === '') {
            isValid = false;
            $('#MobileBill').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#MobileBill').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#BillDate').val().trim() === '') {
            isValid = false;
            $('#BillDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#BillDate').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });

    $('#BillDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();