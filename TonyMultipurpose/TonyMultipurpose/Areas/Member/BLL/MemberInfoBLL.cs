﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.Areas.Member.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Member.BLL
{
    //ataur
    public class MemberInfoBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForMemberInfo(DbDataReader objDataReader, MemberInfo objMemberInfo)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "MemberId":
                        if (!Convert.IsDBNull(objDataReader["MemberId"]))
                        {
                            objMemberInfo.MemberId = Convert.ToInt32(objDataReader["MemberId"]);
                        }
                        break;
                    case "MemberName":
                        if (!Convert.IsDBNull(objDataReader["MemberName"]))
                        {
                            objMemberInfo.MemberName = objDataReader["MemberName"].ToString();
                        }
                        break;
                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objMemberInfo.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "AccountTitle":
                        if (!Convert.IsDBNull(objDataReader["AccountTitle"]))
                        {
                            objMemberInfo.AccountTitle = objDataReader["AccountTitle"].ToString();
                        }
                        break;
                    case "AccountTypeId":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeId"]))
                        {
                            objMemberInfo.AccountTypeId = Convert.ToByte(objDataReader["AccountTypeId"].ToString());
                        }
                        break;
                    case "AccountTypeName":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeName"]))
                        {
                            objMemberInfo.AccountTypeName = objDataReader["AccountTypeName"].ToString();
                        }
                        break;

                    case "TradeLicenceNo":
                        if (!Convert.IsDBNull(objDataReader["TradeLicenceNo"]))
                        {
                            objMemberInfo.TradeLicenceNo = objDataReader["TradeLicenceNo"].ToString();
                        }
                        break;
                    case "TradeLicenceIssueDate":
                        if (!Convert.IsDBNull(objDataReader["TradeLicenceIssueDate"]))
                        {
                            objMemberInfo.TradeLicenceIssueDate = objDataReader["TradeLicenceIssueDate"].ToString();
                        }
                        break;
                    case "TradeLicenceIssueDateString":
                        if (!Convert.IsDBNull(objDataReader["TradeLicenceIssueDateString"]))
                        {
                            objMemberInfo.TradeLicenceIssueDateString = objDataReader["TradeLicenceIssueDateString"].ToString();
                        }
                        break;
                    case "RegistrationNumber":
                        if (!Convert.IsDBNull(objDataReader["RegistrationNumber"]))
                        {
                            objMemberInfo.RegistrationNumber = objDataReader["RegistrationNumber"].ToString();
                        }
                        break;
                    case "RegistrationAuthority":
                        if (!Convert.IsDBNull(objDataReader["RegistrationAuthority"]))
                        {
                            objMemberInfo.RegistrationAuthority = objDataReader["RegistrationAuthority"].ToString();
                        }
                        break;
                    case "InvolvementId":
                        if (!Convert.IsDBNull(objDataReader["InvolvementId"]))
                        {
                            objMemberInfo.InvolvementId = Convert.ToByte(objDataReader["InvolvementId"].ToString());
                        }
                        break;
                    case "InvolvementType":
                        if (!Convert.IsDBNull(objDataReader["InvolvementType"]))
                        {
                            objMemberInfo.InvolvementType = objDataReader["InvolvementType"].ToString();
                        }
                        break;
                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objMemberInfo.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "MotherName":
                        if (!Convert.IsDBNull(objDataReader["MotherName"]))
                        {
                            objMemberInfo.MotherName = objDataReader["MotherName"].ToString();
                        }
                        break;
                    case "SpouseName":
                        if (!Convert.IsDBNull(objDataReader["SpouseName"]))
                        {
                            objMemberInfo.SpouseName = objDataReader["SpouseName"].ToString();
                        }
                        break;
                    case "Nationality":
                        if (!Convert.IsDBNull(objDataReader["Nationality"]))
                        {
                            objMemberInfo.Nationality = objDataReader["Nationality"].ToString();
                        }
                        break;
                    case "NID":
                        if (!Convert.IsDBNull(objDataReader["NID"]))
                        {
                            objMemberInfo.NID = objDataReader["NID"].ToString();
                        }
                        break;
                    case "DateOfBirth":
                        if (!Convert.IsDBNull(objDataReader["DateOfBirth"]))
                        {
                            objMemberInfo.DateOfBirth = objDataReader["DateOfBirth"].ToString();
                        }
                        break;
                    case "Gender":
                        if (!Convert.IsDBNull(objDataReader["Gender"]))
                        {
                            objMemberInfo.Gender = Convert.ToByte(objDataReader["Gender"].ToString());
                        }
                        break;
                    case "GenderName":
                        if (!Convert.IsDBNull(objDataReader["GenderName"]))
                        {
                            objMemberInfo.GenderName = objDataReader["GenderName"].ToString();
                        }
                        break;
                    case "Occupation":
                        if (!Convert.IsDBNull(objDataReader["Occupation"]))
                        {
                            objMemberInfo.Occupation = objDataReader["Occupation"].ToString();
                        }
                        break;
                    case "PassportNumber":
                        if (!Convert.IsDBNull(objDataReader["PassportNumber"]))
                        {
                            objMemberInfo.PassportNumber = objDataReader["PassportNumber"].ToString();
                        }
                        break;
                    case "PassportExpireDate":
                        if (!Convert.IsDBNull(objDataReader["PassportExpireDate"]))
                        {
                            objMemberInfo.PassportExpireDate = objDataReader["PassportExpireDate"].ToString();
                        }
                        break;
                    case "PassportExpireDateString":
                        if (!Convert.IsDBNull(objDataReader["PassportExpireDateString"]))
                        {
                            objMemberInfo.PassportExpireDateString = objDataReader["PassportExpireDateString"].ToString();
                        }
                        break;
                    case "TINNumber":
                        if (!Convert.IsDBNull(objDataReader["TINNumber"]))
                        {
                            objMemberInfo.TINNumber = objDataReader["TINNumber"].ToString();
                        }
                        break;
                    case "DrivingLicenseNumber":
                        if (!Convert.IsDBNull(objDataReader["DrivingLicenseNumber"]))
                        {
                            objMemberInfo.DrivingLicenseNumber = objDataReader["DrivingLicenseNumber"].ToString();
                        }
                        break;
                    case "PresentAddress":
                        if (!Convert.IsDBNull(objDataReader["PresentAddress"]))
                        {
                            objMemberInfo.PresentAddress = objDataReader["PresentAddress"].ToString();
                        }
                        break;
                    case "ParmanentAdress":
                        if (!Convert.IsDBNull(objDataReader["ParmanentAdress"]))
                        {
                            objMemberInfo.ParmanentAdress = objDataReader["ParmanentAdress"].ToString();
                        }
                        break;
                    case "ContactNumber":
                        if (!Convert.IsDBNull(objDataReader["ContactNumber"]))
                        {
                            objMemberInfo.ContactNumber = objDataReader["ContactNumber"].ToString();
                        }
                        break;
                    case "Email":
                        if (!Convert.IsDBNull(objDataReader["Email"]))
                        {
                            objMemberInfo.Email = objDataReader["Email"].ToString();
                        }
                        break;
                    case "IncomeSource":
                        if (!Convert.IsDBNull(objDataReader["IncomeSource"]))
                        {
                            objMemberInfo.IncomeSource = objDataReader["IncomeSource"].ToString();
                        }
                        break;

                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objMemberInfo.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objMemberInfo.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objMemberInfo.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objMemberInfo.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objMemberInfo.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objMemberInfo.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objMemberInfo.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objMemberInfo.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<MemberInfo> GetAllMemberInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MemberInfo> objMemberInfoList=new List<MemberInfo>();
            MemberInfo objMemberInfo;

            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllMemberInfoList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objMemberInfo=new MemberInfo();
                        this.BuildModelForMemberInfo(objDbDataReader, objMemberInfo);
                        objMemberInfoList.Add(objMemberInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objMemberInfoList;
        }

        public string SaveMemberInfo(MemberInfo objMemberInfo)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("MemberName", objMemberInfo.MemberName);
            //objDbCommand.AddInParameter("AccountNumber", objMemberInfo.AccountNumber);
            objDbCommand.AddInParameter("AccountTitle", objMemberInfo.AccountTitle);
            objDbCommand.AddInParameter("AccountTypeId", objMemberInfo.AccountTypeId);
            objDbCommand.AddInParameter("TradeLicenceNo", objMemberInfo.TradeLicenceNo);
            objDbCommand.AddInParameter("TradeLicenceIssueDate", objMemberInfo.TradeLicenceIssueDate);
            objDbCommand.AddInParameter("RegistrationNumber", objMemberInfo.RegistrationNumber);
            objDbCommand.AddInParameter("RegistrationAuthority", objMemberInfo.RegistrationAuthority);
            objDbCommand.AddInParameter("InvolvementId", objMemberInfo.InvolvementId);
            objDbCommand.AddInParameter("FatherName", objMemberInfo.FatherName);
            objDbCommand.AddInParameter("MotherName", objMemberInfo.MotherName);
            objDbCommand.AddInParameter("SpouseName", objMemberInfo.SpouseName);
            objDbCommand.AddInParameter("Nationality", objMemberInfo.Nationality);
            objDbCommand.AddInParameter("NID", objMemberInfo.NID);
            objDbCommand.AddInParameter("DateOfBirth", objMemberInfo.DateOfBirth);
            objDbCommand.AddInParameter("Gender", objMemberInfo.Gender);
            objDbCommand.AddInParameter("Occupation", objMemberInfo.Occupation);
            objDbCommand.AddInParameter("PassportNumber", objMemberInfo.PassportNumber);
            objDbCommand.AddInParameter("PassportExpireDate", objMemberInfo.PassportExpireDate);
            objDbCommand.AddInParameter("TINNumber", objMemberInfo.TINNumber);
            objDbCommand.AddInParameter("DrivingLicenseNumber", objMemberInfo.DrivingLicenseNumber);
            objDbCommand.AddInParameter("PresentAddress", objMemberInfo.PresentAddress);
            objDbCommand.AddInParameter("ParmanentAdress", objMemberInfo.ParmanentAdress);
            objDbCommand.AddInParameter("ContactNumber", objMemberInfo.ContactNumber);
            objDbCommand.AddInParameter("Email", objMemberInfo.Email);
            objDbCommand.AddInParameter("IncomeSource", objMemberInfo.IncomeSource);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateMemberInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        //get memberinfo for Edit and Delete
        public MemberInfo GetMemberInfoForById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MemberInfo> objMemberInfoList=new List<MemberInfo>();
            MemberInfo objMemberInfo=new MemberInfo();
            try
            {
                objDbCommand.AddInParameter("MemberId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetMemberInfoById", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                      objMemberInfo= new MemberInfo();
                        this.BuildModelForMemberInfo(objDbDataReader, objMemberInfo);
                       objMemberInfoList.Add(objMemberInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objMemberInfo;
        }

        // update MemberInfo
        public string UpdateMemberInfo(MemberInfo objMemberInfo)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("MemberId", objMemberInfo.MemberId);
            objDbCommand.AddInParameter("MemberName", objMemberInfo.MemberName);
            //objDbCommand.AddInParameter("AccountNumber", objMemberInfo.AccountNumber);
            objDbCommand.AddInParameter("AccountTitle", objMemberInfo.AccountTitle);
            objDbCommand.AddInParameter("AccountTypeId", objMemberInfo.AccountTypeId);
            objDbCommand.AddInParameter("TradeLicenceNo", objMemberInfo.TradeLicenceNo);
            objDbCommand.AddInParameter("TradeLicenceIssueDate", objMemberInfo.TradeLicenceIssueDate);
            objDbCommand.AddInParameter("RegistrationNumber", objMemberInfo.RegistrationNumber);
            objDbCommand.AddInParameter("RegistrationAuthority", objMemberInfo.RegistrationAuthority);
            objDbCommand.AddInParameter("InvolvementId", objMemberInfo.InvolvementId);
            objDbCommand.AddInParameter("FatherName", objMemberInfo.FatherName);
            objDbCommand.AddInParameter("MotherName", objMemberInfo.MotherName);
            objDbCommand.AddInParameter("SpouseName", objMemberInfo.SpouseName);
            objDbCommand.AddInParameter("Nationality", objMemberInfo.Nationality);
            objDbCommand.AddInParameter("NID", objMemberInfo.NID);
            objDbCommand.AddInParameter("DateOfBirth", objMemberInfo.DateOfBirth);
            objDbCommand.AddInParameter("Gender", objMemberInfo.Gender);
            objDbCommand.AddInParameter("Occupation", objMemberInfo.Occupation);
            objDbCommand.AddInParameter("PassportNumber", objMemberInfo.PassportNumber);
            objDbCommand.AddInParameter("PassportExpireDate", objMemberInfo.PassportExpireDate);
            objDbCommand.AddInParameter("TINNumber", objMemberInfo.TINNumber);
            objDbCommand.AddInParameter("DrivingLicenseNumber", objMemberInfo.DrivingLicenseNumber);
            objDbCommand.AddInParameter("PresentAddress", objMemberInfo.PresentAddress);
            objDbCommand.AddInParameter("ParmanentAdress", objMemberInfo.ParmanentAdress);
            objDbCommand.AddInParameter("ContactNumber", objMemberInfo.ContactNumber);
            objDbCommand.AddInParameter("Email", objMemberInfo.Email);
            objDbCommand.AddInParameter("IncomeSource", objMemberInfo.IncomeSource);
            objDbCommand.AddInParameter("IsActive", objMemberInfo.IsActive);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateMemberInfo", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

       
    }
}