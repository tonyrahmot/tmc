﻿(function () {
    $('#TransactionDate').datepicker({ dateFormat: 'dd-M-yy', type: Text }).click(function () { $(this).focus(); });
    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "MonthlyBenefit/MB/AutoCompleteMBAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $(function () {
        $("#ChartOfAccount").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/MB/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });

    //load data
    $("#AccountNumber").change(function () {
        UnAppovedMBList();
        var AccountNumber = $("#AccountNumber").val();
        var json = {
            AccountNumber: AccountNumber
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/MB/GetMBAccountInfoByAccountNo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {

                $('#hdfOpeningDate').val(formateDate(data.OpeningDate));
                /// check if month is less < 1
                var oDate = $('#hdfOpeningDate').val();
                var cDate = $('#TransactionDate').val();
                // usrDate = opening date
                var usrDate = new Date(oDate);
                var curDate = new Date(cDate);
                var usrYear, usrMonth = usrDate.getMonth() + 1;
                var curYear, curMonth = curDate.getMonth() + 1;
                if ((usrYear = usrDate.getFullYear()) < (curYear = curDate.getFullYear())) {
                    curMonth += (curYear - usrYear) * 12;
                }
                var diffMonths = curMonth - usrMonth;
                if (usrDate > curDate) diffMonths--;
                if (data.AccruedBalance == 0 || diffMonths < 1) {
                    alert("Not enough balance !!!");
                } else {
                    $('#CustomerName').val(data.CustomerName);
                    $('#BAmount').val(data.AccruedBalance);
                    $('#hdfCustomerId').val(data.CustomerId);
                    $('#hdfMatureDate').val(formateDate(data.MatureDate));
                    $('#hdfOpeningDate').val(formateDate(data.OpeningDate));
                    $('#CustomerImage').attr('src', data.CustomerImage);
                }
            }
        });
    });

    function UnAppovedMBList() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/MB/GetUnAppovedMBList",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $("#submit").removeAttr('disabled');
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
                }
                else {
                    CloseMBAccount();
                }

            },

        });
    }

    function CloseMBAccount() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/MB/GetCloseMBAccount",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account was closed !!!');
                }
            },
        });
    }

    $('#submit').click(function () {
        var isAllValid = true;
        if ($('#TransactionDate').val().trim() == '') {
            $('#TransactionDate').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#TransactionDate').siblings('span.error').css('display', 'none');
        }
        if ($('#AccountNumber').val().trim() == '') {
            $('#AccountNumber').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#AccountNumber').siblings('span.error').css('display', 'none');
        }

        if ($('#ChartOfAccount').val().trim() == 0) {
            $('#ChartOfAccount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').siblings('span.error').css('display', 'none');
        }

        if (isAllValid) {

            var amount = parseFloat($('#Amount').val());
            var bAmount = parseFloat($('#BAmount').val());

            /// check if month is less < 1
            var oDate = $('#hdfOpeningDate').val();
            var cDate = $('#TransactionDate').val();
            // usrDate = opening date
            var usrDate = new Date(oDate);
            var curDate = new Date(cDate);
            var usrYear, usrMonth = usrDate.getMonth() + 1;
            var curYear, curMonth = curDate.getMonth() + 1;
            if ((usrYear = usrDate.getFullYear()) < (curYear = curDate.getFullYear())) {
                curMonth += (curYear - usrYear) * 12;
            }
            var diffMonths = curMonth - usrMonth;
            if (usrDate > curDate) diffMonths--;

            if (amount > bAmount || diffMonths < 1) {
                $('#submit').attr("disabled", "disabled");
                alert("invalid withdraw amount !!!");
            } else {
                var data = {
                    AccountNumber: $('#AccountNumber').val(),
                    TransactionDate: $('#TransactionDate').val(),
                    MonthlyBenefitAmount: bAmount,
                    Balance: $('#Amount').val(),
                    CustomerName: $('#CustomerName').val(),
                    CustomerId: $('#hdfCustomerId').val(),
                    MatureDate: $('#hdfMatureDate').val(),
                    COAId: $('#ChartOfAccount option:selected').val()
                }
                //   console.log(data);
                $.ajax({
                    type: "POST",
                    url: window.applicationBaseUrl + "MonthlyBenefit/MB/Save",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.status === true) {
                            alert('Successfully done.');
                            $("#AccountNumber").val('');
                            $("#TransactionDate").val('');
                            $('#Amount').val('');
                            $('#CustomerName').val('');
                            $("#hdfCustomerId").val('');
                            $('#ChartOfAccount').val('0');
                        } else {
                            alert('Failed');
                        }
                        $('#submit').val('Save');
                        window.location.reload();
                    },
                    error: function () {
                        alert('Error. Please try again.');
                    }
                });
            }

        }
    });

    var months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
    ];

    //convert datetime
    function formateDate(jsonDate) {
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = months[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '-' + mm + '-' + yyyy;
        return formmatedDate;
    }

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

    function monthNameToNum(monthname) {
        var month = months.indexOf(monthname);
        return month ? month + 1 : 0;
    }
})();
function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}