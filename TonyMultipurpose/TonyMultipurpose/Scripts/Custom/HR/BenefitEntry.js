﻿(function () {
    $('#submit1').hide();
    empBenefitTable();
    var oTable;

    function empBenefitTable() {
        oTable = $('#EmpBenefitTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveBenefit",//'@Url.Action("SaveBenefit", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpId" },
                { "data": "BenefitTypeName" },
                { "data": "BenefitAmount" },
                {
                    "data": "BenefitDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "Description" }
            ],
            ordering: false,
            sorting: false
        });
        $('#EmpBenefitTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        document.getElementById("EmpId").readOnly = true;
        $('#EmpId').val(data.EmpId);
        var date = new Date(data.BenefitDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var benefitDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#BenefitType').val(data.BenefitType);
        $('#BenefitDate').val(benefitDate);
        $('#Description').val(data.Description);
        $('#BenefitAmount').val(data.BenefitAmount);
        $("#EmpId").change();
    }
    $('#submit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpId: $('#EmpId').val(),
                BenefitType: $('#BenefitType option:Selected').val(),
                BenefitDate: $('#BenefitDate').val(),
                Description: $('#Description').val(),
                BenefitAmount: $('#BenefitAmount').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveBenefit",//'@Url.Action("SaveBenefit", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#BenefitType').val('0');
                        $('#BenefitDate').val('');
                        $('#Description').val('');
                        $('#EmpId').val('');
                        $('#BenefitAmount').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });

    $('#submit1').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpId: $('#EmpId').val(),
                BenefitType: $('#BenefitType option:Selected').val(),
                BenefitDate: $('#BenefitDate').val(),
                Description: $('#Description').val(),
                BenefitAmount: $('#BenefitAmount').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateBenefit",//'@Url.Action("UpdateBenefit", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#BenefitType').val('0');
                        $('#BenefitDate').val('');
                        $('#Description').val('');
                        $('#EmpId').val('');
                        $('#BenefitAmount').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanId').val('');
                }
            });

        }
    });

    $("#EmpId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpId").change(function () {
        $('#EmployeeName').empty();
        var empId = $("#EmpId").val();
        var json = { EmpId: empId };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('EmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('Designation').innerHTML = data.LastName;
                document.getElementById('Center').innerHTML = data.FirstName;
                document.getElementById('NID').innerHTML = data.NID;
            }
        });
    });

    $('#clear').click(function () {
        window.location.reload();
    });

    function checkValidation() {
        var isValid = true;
        if ($('#EmpId').val().trim() === '') {
            isValid = false;
            $('#EmpId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#BenefitType').val().trim() === '0') {
            isValid = false;
            $('#BenefitType').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#BenefitType').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#BenefitDate').val().trim() === '') {
            isValid = false;
            $('#BenefitDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#BenefitDate').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#BenefitAmount').val().trim() === '') {
            isValid = false;
            $('#BenefitAmount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#BenefitAmount').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });

    $('#BenefitDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });
})();

function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode === 13 || charCode === 46) {
        return false;
    }
    return true;
}