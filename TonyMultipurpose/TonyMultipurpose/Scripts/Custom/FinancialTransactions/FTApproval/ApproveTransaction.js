﻿
(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        oTable = $('#TransactionTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "FinancialTransactions/FTApproval/ApprovedTransactionList",//'@Url.Action("ApprovedTransactionList", "FTApproval", new {Area = "FinancialTransactions"})',
                "type": "get",
                "datatype": "json"
            },

            "columns": [
                { "data": "TransactionId" },
                {
                    "data": "TransactionDate",
                    "render": function (data) {
                        return FormateDate(data);
                    }
                },
                { "data": "VoucherNo" },
                { "data": "VoucherType" },
                { "data": "Particular" },
                { "data": "BankName" },
                { "data": "ClientName" },
                { "data": "SupplierName" },
                { "data": "AccountCode" },
                { "data": "AccountName" },
                { "data": "CategoryName" },
                { "data": "GroupName" },
                { "data": "Credit" },
                { "data": "Debit" },
                {
                    "data": "VoucherNo",
                    "width": "50px",
                    "render": function (voucherNo, type, data) {
                        return '<a class="btn btn-info" target="_blank" href="' + window.applicationBaseUrl + 'FinancialTransactions/FTApproval/PaymentVoucherReport?VoucherNo=' + voucherNo + '&VoucherType=' + data.VoucherType + '">Print</a>';
                    }
                }
            ],
            "order": [[1, 'asc']]
        });
    }


    function FormateDate(jsonDate) {
        var monthNames = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

})();
