﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class Guarantor
    {
        [Key]
        public int GuarantorId { get; set; }
        public string LoanId { get; set; }
        public string GuarantorName { get; set; }
        public string NID { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Occupation { get; set; }
        public string Designation { get; set; }
        public string InstituteName { get; set; }
        public string InstituteAddress { get; set; }
        public string CurrentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string ContactNo { get; set; }

        public string GuarantorRoll { get; set; }
        public string SpouseName { get; set; }
        public string RelationWithApplicant { get; set; }
        public string DateofBirth { get; set; }
        public decimal? MonthlyIncome { get; set; }
        public string BusinessAddress { get; set; }
        public string LoanStatusWithTmc { get; set; }

        public byte? ResidentialConditionId { get; set; }
        public string ResidentialConditionType { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
        public string ImagePath { get; set; }

        public string OldImagePath { get; set; }


    }
}
