﻿
(function () {

    $('.js-example-basic-single').select2();

    $('#btnPreview').prop('disabled', true);

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    LoadAccountCode();
    LoadAccountToCode();
    $('#checkInfo').hide();
    $('#AccountFromBankInfo').hide();
    $('#AccountToBankInfo').hide();
    $('#CheckEntry').change(function () {
        if (this.checked) {
            $('#checkInfo').show();
        }
        else {
            $('#checkInfo').hide();
        }
    });


    function LoadAccountCode() {
        $("#AccountCode").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAllAccountCode",//'@Url.Action("GetAllAccountCode", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#AccountCode").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#AccountCode").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    }

    function LoadAccountToCode() {
        $("#AccountToCode").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAllAccountCode",//'@Url.Action("GetAllAccountCode", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#AccountToCode").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#AccountToCode").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    }

    $('#AccountCode').change(function () {
        var coaId = $("#AccountCode").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAccountCodeInfo",//'@Url.Action("GetAccountCodeInfo", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#hdnAccountCode').val(data.AccountCode);
                $('#hdnAccountName').val(data.AccountName);
                $('#hdnGroupId').val(data.GroupId);
                $('#hdnGroupName').val(data.GroupName);
                $('#hdnCategoryId').val(data.COACategoryId);
                $('#hdnCategoryName').val(data.CategoryName);
                $('#hdnClientCode').val(data.ClientCode);
                $('#hdnClientName').val(data.ClientName);
                $('#hdnSupplierCode').val(data.SupplierCode);
                $('#hdnSupplierName').val(data.SupplierName);
                $('#AcountFromCodeBalance').text(data.Balance);
                $('#InwordsAcountFromCodeBalance').val(data.BalanceInwords + ' TK ONLY.');
                if (data.IsBankAccount === true) {
                    $('#AccountFromBankInfo').show();
                    $('#BankName').val(data.BankName);
                    $('#BranchName').val(data.BranchName);
                    $('#BankAccountNo').val(data.BankAccountNo);
                } else {
                    $('#AccountFromBankInfo').hide();
                    $('#BankName').val('');
                    $('#BranchName').val('');
                    $('#BankAccountNo').val('');
                }
            }
        });

    });

    $('#AccountToCode').change(function () {
        var coaId = $("#AccountToCode").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAccountCodeInfo",//'@Url.Action("GetAccountCodeInfo", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#hdnAccountToCode').val(data.AccountCode);
                $('#hdnAccountToName').val(data.AccountName);
                $('#hdnAccountToGroupId').val(data.GroupId);
                $('#hdnAccountToGroupName').val(data.GroupName);
                $('#hdnAccountToCategoryId').val(data.COACategoryId);
                $('#hdnAccountToCategoryName').val(data.CategoryName);
                $('#hdnAccountToClientCode').val(data.ClientCode);
                $('#hdnAccountToClientName').val(data.ClientName);
                $('#hdnAccountToSupplierCode').val(data.SupplierCode);
                $('#hdnAccountToSupplierName').val(data.SupplierName);
                $('#AcountToCodeBalance').text(data.Balance);
                $('#InwordsAcountToCodeBalance').val(data.BalanceInwords + ' TK ONLY.');
                if (data.IsBankAccount === true) {
                    $('#AccountToBankInfo').show();
                    $('#AccountToBankName').val(data.BankName);
                    $('#AccountToBranchName').val(data.BranchName);
                    $('#AccountToBankAccountNo').val(data.BankAccountNo);
                } else {
                    $('#AccountToBankInfo').hide();
                    $('#AccountToBankName').val('');
                    $('#AccountToBranchName').val('');
                    $('#AccountToBankAccountNo').val('');
                }
            }
        });
    });

    function checkValidation() {
        var isValid = true;
        if ($('#AccountCode').val() === '0') {
            isValid = false;
            $('#AccountCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AccountCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AccountToCode').val() === '0') {
            isValid = false;
            $('#AccountToCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AccountToCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#Amount').val() === '') {
            isValid = false;
            $('#Amount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Amount').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#DebitReason').val() === '') {
            isValid = false;
            $('#DebitReason').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DebitReason').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#Particular').val() === '') {
            isValid = false;
            $('#Particular').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Particular').parent().prev().find('span').css('visibility', 'hidden');
        }
        if (parseFloat($('#Amount').val()) > parseFloat($('#AcountFromCodeBalance').text())) {
            isValid = false;
            alert('Amount should be less than account from balance');
        }
        if ($('#CheckEntry').is(":checked")) {
            if ($('#ChequeNo').val() === '') {
                isValid = false;
                $('#ChequeNo').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#ChequeNo').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#IssueDate').val() === '') {
                isValid = false;
                $('#IssueDate').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#IssueDate').parent().prev().find('span').css('visibility', 'hidden');
            }
            //if ($('#BankName').val() === '') {
            //    isValid = false;
            //    $('#BankName').parent().prev().find('span').css('visibility', 'visible');
            //}
            //else {
            //    $('#BankName').parent().prev().find('span').css('visibility', 'hidden');
            //}
            //if ($('#BankAccountNo').val() === '') {
            //    isValid = false;
            //    $('#BankAccountNo').parent().prev().find('span').css('visibility', 'visible');
            //}
            //else {
            //    $('#BankAccountNo').parent().prev().find('span').css('visibility', 'hidden');
            //}
        }
        return isValid;
    }

    function BindData() {
        var data = [];
        var isChequePayment = false;
        if ($('#CheckEntry').is(":checked")) {
            isChequePayment = true;
        }
        /**********************************/
        var accountFromGroupId = $('#hdnGroupId').val();
        var accountToGroupId = $('#hdnAccountToGroupId').val();
        var amount = $('#Amount').val();
        var accountFromEntryType = '', accountToEntryType = '';

        if ((accountFromGroupId === '1' || accountFromGroupId === '3' || accountFromGroupId === '6') && (accountToGroupId === '1' || accountToGroupId === '3' || accountToGroupId === '6')) {
            accountFromEntryType = 'Credit';
            accountToEntryType = 'Debit';
        } else if ((accountFromGroupId === '1' || accountFromGroupId === '3' || accountFromGroupId === '6') && (accountToGroupId === '2' || accountToGroupId === '4' || accountToGroupId === '5')) {
            accountFromEntryType = 'Credit';
            accountToEntryType = 'Credit';
        } else if ((accountFromGroupId === '2' || accountFromGroupId === '4' || accountFromGroupId === '5') && (accountToGroupId === '2' || accountToGroupId === '4' || accountToGroupId === '5')) {
            accountFromEntryType = 'Debit';
            accountToEntryType = 'Credit';
        } else if ((accountFromGroupId === '2' || accountFromGroupId === '4' || accountFromGroupId === '5') && (accountToGroupId === '1' || accountToGroupId === '3' || accountToGroupId === '6')) {
            accountFromEntryType = 'Debit';
            accountToEntryType = 'Debit';
        }
        /***********************************/
        data.push({
            VoucherType: $('#VoucherType').val(),
            TransactionDate: $('#TransactionDate').val(),
            VoucherCode: $('#VoucherCode').val(),
            EntryType: accountFromEntryType,
            COAId: $('#AccountCode').val(),
            AccountCode: $('#hdnAccountCode').val(),
            AccountName: $('#hdnAccountName').val(),
            Particular: $('#Particular').val(),
            Debit: accountFromEntryType === 'Credit' ? 0 : amount,
            Credit: accountFromEntryType === 'Credit' ? amount : 0,
            Remarks: '',
            IsChequePayment: isChequePayment,
            ChequeNo: $('#ChequeNo').val(),
            IssueDate: $('#IssueDate').val(),
            BankName: $('#BankName').val(),
            BranchName: $('#BranchName').val(),
            BankAccountNo: $('#BankAccountNo').val(),
            GroupId: $('#hdnGroupId').val(),
            GroupName: $('#hdnGroupName').val(),
            CategoryId: $('#hdnCategoryId').val(),
            CategoryName: $('#hdnCategoryName').val(),
            ClientCode: $('#hdnClientCode').val(),
            ClientName: $('#hdnClientName').val(),
            SupplierCode: $('#hdnSupplierCode').val(),
            SupplierName: $('#hdnSupplierName').val(),
            EntryPosition: 'From'
        });

        data.push({
            VoucherType: $('#VoucherType').val(),
            TransactionDate: $('#TransactionDate').val(),
            VoucherCode: $('#VoucherCode').val(),
            EntryType: accountToEntryType,
            COAId: $('#AccountToCode').val(),
            AccountCode: $('#hdnAccountCode').val(),
            AccountName: $('#hdnAccountToName').val(),
            Particular: $('#DebitReason').val(),
            Debit: accountToEntryType === 'Credit' ? 0 : amount,
            Credit: accountToEntryType === 'Credit' ? amount : 0,
            Remarks: '',
            IsChequePayment: isChequePayment,
            ChequeNo: $('#ChequeNo').val(),
            IssueDate: $('#IssueDate').val(),
            BankName: $('#AccountToBankName').val(),
            BranchName: $('#AccountToBranchName').val(),
            BankAccountNo: $('#AccountToBankAccountNo').val(),
            GroupId: $('#hdnAccountToGroupId').val(),
            GroupName: $('#hdnAccountToGroupName').val(),
            CategoryId: $('#hdnAccountToCategoryId').val(),
            CategoryName: $('#hdnAccountToCategoryName').val(),
            ClientCode: $('#hdnAccountToClientCode').val(),
            ClientName: $('#hdnAccountToClientName').val(),
            SupplierCode: $('#hdnAccountToSupplierCode').val(),
            SupplierName: $('#hdnAccountToSupplierName').val(),
            EntryPosition: 'To'
        });
        return data;
    }

    $('#btnSave').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var transactions = BindData();
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/Save",//'@Url.Action("Save", "FinancialTransaction", new { Area = "FinancialTransactions" })',
                type: "POST",
                data: JSON.stringify(transactions),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        document.getElementById('showMessage').style.color = "green";
                        $('#VoucherNo').val(d.VoucherNo);
                        $('#btnSave').prop('disabled', true);
                        $('#btnPreview').prop('disabled', false);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#VoucherNo').val('');
                }
            });
        }
    });

    $('#btnNew').click(function () {
        location.reload();
    });

    $("#Amount").keyup(function () {
        //removeErrorMessage(this);
        var amount = parseInt($('#Amount').val().replace(/,/g, ''));  //parseFloat($('#Amount').val())
        var status = $("#InwordsAmount"); //DIV object to display the status message
        var user = $.trim(amount);
        if (user.length >= 1) {
            status.val("Checking....");
            //jQuery AJAX Post request
            $.post(window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetConvertNumberToWord", { number: amount },//'@Url.Action("GetConvertNumberToWord", "FinancialTransaction", new { Area = "FinancialTransactions" })', { number: amount },
                        function (data) {
                            var money = data + ' Tk Only';
                            status.val(money);
                        });
        } else {
            status.val("Need more characters...");
        }
    });
})();
