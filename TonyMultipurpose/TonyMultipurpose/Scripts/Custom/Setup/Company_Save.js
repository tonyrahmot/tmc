﻿$("#fileUpload").change(function () {
    readURL(this);
    var fd = new FormData();
    fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
    $.ajax({
        url: window.applicationBaseUrl + "Setup/Company/SaveImageFile",
        type: "POST",
        data: fd,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function (obj) {
            $('#hiddenCustomerId').val(obj.customerId);
            $('#hiddenImagePath').val(obj.imagePath);
        },
        error: function () {
            alert('Error. Please try again.');
        }
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageDisplay').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

});