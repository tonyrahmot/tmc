﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TonyMultipurpose.Areas.Share.ViewModels
{
    public class ShareTransferViewModel
    {
        public string TransactionId { get; set; }
        [Display(Name = "Issue Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy")]
        public DateTime IssuedDate { get; set; }
        [Display(Name = "From Customer")]
        [Required]
        public string FromCustomerId { get; set; }
        [Display(Name = "To Customer")]
        [Required]
        public string ToCustomerId { get; set; }
        [Display(Name = "Price/Share")]
        public decimal? PricePerShare { get; set; }
        [Display(Name = "Transfer Quantity")]
        [Range(1, Int32.MaxValue)]
        [Required]
        public int Qty { get; set; }
        //        [Display(Name = "Transfer Amount")]
        //        [Required]
        //        public int Amount { get; set; }
        [Display(Name = "Voucher No")]
        [Required]
        public string VoucherNo { get; set; }
        [Display(Name = "From Particular")]
        public string FromParticular { get; set; }
        [Display(Name = "To Particular")]
        public string ToParticular { get; set; }
        public int? BranchId { get; set; }
        public int? CompanyId { get; set; }
        public short CreatedBy { get; set; }
        //        public List<CustomerViewModel> FromCustomerDetails { get; set; }
        //        public List<CustomerViewModel> ToCustomerDetails { get; set; }
    }
}