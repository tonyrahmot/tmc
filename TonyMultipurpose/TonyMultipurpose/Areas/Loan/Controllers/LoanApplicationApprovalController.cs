﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;
using ReportObject = TonyMultipurpose.ReportsViewer.ReportObject;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
   [AuthenticationFilter]
    public class LoanApplicationApprovalController : Controller
    {
        CommonResult oCommonResult;
        LoanModel objLoan;
        LoanBLL objLoanBLL;
        // GET: Loan/LoanApplicationApproval
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetCheckedLoanInfo()
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = false;
            objLoan.IsRejected = false;
            //objLoan.AccountSetupId = loanTypesetupId;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == true);
            return Json(new { data = loans }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult FinancialJustification(string loanId)
        {
            MonthlyIncome objGuarantor = new MonthlyIncome();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }

        [HttpPost]
        public ActionResult ApproveLoan(LoanModel objLoan)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                objLoan.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objLoan.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objLoan.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objLoan.CreatedDate = DateTime.Now.ToString();
                objLoan.IsApproved = true;
                oCommonResult = objLoanBLL.ApproveLoanInfo(objLoan);
            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult RejectLoan(string loanId)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.RejectLoanApplication(loanId);
            return new JsonResult { Data = new { oCommonResult } };
        }
        public JsonResult GetLoanNo(string loanType)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetLoanNo(loanType);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetExpiryDate(LoanModel obj)
        {
            objLoanBLL = new LoanBLL();

            var info = objLoanBLL.GetExpiryDate(obj);
            return new JsonResult { Data = new { info } };
        }

        #region Guarantor
        [HttpGet]
        public ActionResult Guarantor(string loanId)
        {
            Guarantor objGuarantor = new Guarantor();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }
        [HttpGet]
        public JsonResult GetAllGurantorInfo(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<Guarantor> Guarantor = objLoanBLL.GetAllGurantorInfo(loanId);
            return Json(new { data = Guarantor }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Comments & Signatory

        [HttpGet]
        public ActionResult CommentsAndSignatory(string loanId)
        {
            AddtionalInfoModel addtionalInfoModel = new AddtionalInfoModel();
            addtionalInfoModel.LoanId = loanId;
            return View(addtionalInfoModel);
        }

        [HttpGet]
        public JsonResult GetScanDocumnetInfo(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            return Json(new { data = scanDocuments }, JsonRequestBehavior.AllowGet);
        }

        #region deviation
        public JsonResult GetAddtionalInfoData(string loanId)
        {
            objLoanBLL = new LoanBLL();
            var addtionalInfos = objLoanBLL.GetAddtionalInfoData(loanId);
            return Json(new { data = addtionalInfos }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLoanCustomerDetails(AddtionalInfoModel objAddtionalInfoModel)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            if (objAddtionalInfoModel.Id == 0)
            {
                oCommonResult = objLoanBLL.SaveLoanCustomerDetails(objAddtionalInfoModel);
            }
            else
            {
                oCommonResult = objLoanBLL.UpdateCustomerDeviationInfo(objAddtionalInfoModel);
            }

            return new JsonResult { Data = new { oCommonResult } };
        }

        public ActionResult UpdateCustomerDeviationInfo(AddtionalInfoModel addtionalInfoModel)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.UpdateCustomerDeviationInfo(addtionalInfoModel);
            return new JsonResult { Data = new { oCommonResult } };
        }
        #endregion

        #region signatories
        public JsonResult GetBranchWiseEmployee()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetBranchWiseEmployee();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignationByEmpCode(string EmpCode)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetDesignationByEmpCode(EmpCode);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveSignatoryData(SignatoryModel signatory)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SaveSignatoryData(signatory);
            return new JsonResult { Data = new { oCommonResult } };
        }

        public JsonResult GetSignatoryDataForDataTable(string loanId)
        {
            objLoanBLL = new LoanBLL();
            //List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            List<SignatoryModel> signatory = objLoanBLL.GetSignatoryDataForDataTable(loanId);
            return Json(new { data = signatory }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateSignatoryData(SignatoryModel signatory)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.UpdateSignatoryData(signatory);
            return new JsonResult { Data = new { oCommonResult } };
        }

        public JsonResult ExitsEmployeeSignatoriesTable(string empCode, string loanId)
        {
            objLoanBLL = new LoanBLL();
            bool exsits = objLoanBLL.GetExitsEmployee(empCode, loanId);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        #endregion

        #endregion

        #region Additional loan info

        [HttpGet]
        public JsonResult GetAdditionalLoanInfoById(string LoanId)
        {
            objLoanBLL = new LoanBLL();
            var result = objLoanBLL.GetAdditionalLoanInfoById(LoanId).ToList();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region finance
        public JsonResult Finance(string loanId)
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = false;
            objLoan.IsRejected = false;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == true);
            var info = loans.Where(a => a.LoanId == loanId).FirstOrDefault();
            var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).FirstOrDefault();
            var netExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId).FirstOrDefault();
            return new JsonResult { Data = new { netIncome = netIncome, netExpense = netExpense, info = info } };
        }
        #endregion

        #region Bank Performance
        public JsonResult GetAllBankperformanceInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<BankPerformance> info = objLoanBLL.GetAllBankperformanceInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Monthly Income
        public JsonResult GetAllMonthlyIncomeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyIncome> MonthlyIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId);
            //var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).ToList().Sum(x => x.Amount);
            return Json(new { data = MonthlyIncome }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region monthly expense
        public JsonResult GetAllMonthlyExpenseInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyExpense> MonthlyExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId);
            return Json(new { data = MonthlyExpense }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public void PrintLoanApplication(string loanId)
        {
            ReportObject.ReportName = "LoanApplication";
            ReportObject.LoanApplication = loanId;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }

        [HttpGet]
        public ActionResult CustomersChequeInfo(string loanId)
        {
            LoanCustomerChequeInfo obj = new LoanCustomerChequeInfo();
            obj.LoanId = loanId;
            return View(obj);


        }

        public JsonResult GetAllLoanCustomerChequeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<LoanCustomerChequeInfo> info = objLoanBLL.GetAllLoanCustomerChequeInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccountCodeInfo(int coaId)
        {
            COABLL objCOABLL = new COABLL();
            var data = objCOABLL.GetCOAInfo(coaId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}