﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.HR.BLL;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Areas.HR.ViewModels;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Controllers
{
    [AuthenticationFilter]
    public class EmployeeController : Controller
    {
        // GET: HR/Employee

        EmployeeBLL _oEmployeeBll;
        CommonResult _oCommonResult;
       
        public ActionResult Index()
        {
            return View();
        }
       
        public ActionResult Save()
        {
            return View();
        }

        #region Basic Info
        [HttpGet]
        public ActionResult EmployeeBasicInfo()
        {
            _oEmployeeBll = new EmployeeBLL();
            var employees = _oEmployeeBll.GetAllEmployeeInfo();
            return Json(new { data = employees }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            var path = string.Empty;
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            oEmployeeInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            oEmployeeInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            oEmployeeInformation.CreatedDate = DateTime.Now;
            oEmployeeInformation.IsActive = true;
            oEmployeeInformation.IsApproved = false;
            oEmployeeInformation.IsDeleted = false;
            if (oEmployeeInformation.ImageFile != null)
            {
                var rootPath = "~\\Uploads\\Employee\\";
                string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                if (!Directory.Exists(rootFolderPath))
                {
                    Directory.CreateDirectory(rootFolderPath);
                }
                path = Path.Combine(rootFolderPath, string.Concat(oEmployeeInformation.EmpCode, ".jpg"));
                string filesToDelete = oEmployeeInformation.EmpCode + ".jpg";
                string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                foreach (string file in fileList)
                {
                    System.IO.File.Delete(file);
                }
                oEmployeeInformation.ImagePath = path;
            }
            _oCommonResult = _oEmployeeBll.SaveEmployeeBasicInfo(oEmployeeInformation);
            if(_oCommonResult.Status == true && path != string.Empty)
            {
                try
                {
                    oEmployeeInformation.ImageFile?.SaveAs(path);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            var path = string.Empty;
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            oEmployeeInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            oEmployeeInformation.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            oEmployeeInformation.UpdatedDate = DateTime.Now;
            if (oEmployeeInformation.ImageFile != null)
            {
                var rootPath = "~\\Uploads\\Employee\\";
                string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                if (!Directory.Exists(rootFolderPath))
                {
                    Directory.CreateDirectory(rootFolderPath);
                }
                path = Path.Combine(rootFolderPath, string.Concat(oEmployeeInformation.EmpCode, ".jpg"));
                string filesToDelete = oEmployeeInformation.EmpCode + ".jpg";
                string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                foreach (string file in fileList)
                {
                    System.IO.File.Delete(file);
                }
                oEmployeeInformation.ImagePath = path;
            }
            _oCommonResult = _oEmployeeBll.UpdateEmployeeBasicInfo(oEmployeeInformation);
            if (_oCommonResult.Status == true && path != string.Empty)
            {
                try
                {
                    oEmployeeInformation.ImageFile?.SaveAs(path);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteEmployeeBasicInfo(EmployeeInformation oEmployeeInformation)
        {
            
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();            
            _oCommonResult = _oEmployeeBll.DeleteEmployeeBasicInfo(oEmployeeInformation);
            
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
      
        public JsonResult GetEmpCodeCheck(string empCode)
        {
            _oEmployeeBll = new EmployeeBLL();
            bool exsits = _oEmployeeBll.GetEmpCodeCheck(empCode);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetEmpNidCheck(string nid)
        {
            _oEmployeeBll = new EmployeeBLL();
            bool exsits = _oEmployeeBll.GetEmpNIDCheck(nid);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetBranches()
        {
            BranchBLL objBranchBll = new BranchBLL();
            var result = objBranchBll.GetAllBranchInfo();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDepartments()
        {
            _oEmployeeBll = new EmployeeBLL();
            var result = _oEmployeeBll.GetDepartments();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignations()
        {
            _oEmployeeBll = new EmployeeBLL();
            var result = _oEmployeeBll.GetDesignations();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFunctionalDesignations()
        {
            _oEmployeeBll = new EmployeeBLL();
            var result = _oEmployeeBll.GetFunctionalDesignations();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Educational Info
        [HttpPost]
        public ActionResult SaveEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.SaveEmployeeEducationInfo(oEmployeeEducation);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
        [HttpGet]
        public JsonResult GetEmployeeEducationalInfo(string empCode)
        {
            _oEmployeeBll = new EmployeeBLL();
            var result = _oEmployeeBll.GetEmployeeEducationalInfo(empCode);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.UpdateEmployeeEducationInfo(oEmployeeEducation);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteEmployeeEducationInfo(EmployeeEducation oEmployeeEducation)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.DeleteEmployeeEducationInfo(oEmployeeEducation);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
        #endregion

        #region Experience Info
        [HttpPost]
        public ActionResult SaveEmployeeExperinceInfo(EmployeeExperience oEmployeeExperience)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.SaveEmployeeExperinceInfo(oEmployeeExperience);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
        [HttpGet]
        public JsonResult GetEmployeeExperinceInfo(string EmpCode)
        {
            _oEmployeeBll = new EmployeeBLL();
            var result = _oEmployeeBll.GetEmployeeExperinceInfo(EmpCode);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateEmployeeExperinceInfo(EmployeeExperience oEmployeeExperience)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.UpdateEmployeeExperinceInfo(oEmployeeExperience);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteEmployeeExperienceInfo(EmployeeExperience oEmployeeExperience)
        {
            _oCommonResult = new CommonResult();
            _oEmployeeBll = new EmployeeBLL();
            _oCommonResult = _oEmployeeBll.DeleteEmployeeExperienceInfo(oEmployeeExperience);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
        #endregion

        #region Approval

        [HttpGet]
        public ActionResult Approval()
        {
            _oEmployeeBll = new EmployeeBLL();
            var model = new EmployeeInfoViewBoth
            {
                UnApprovedEmployeeInfo = _oEmployeeBll.GetUnApprovedEmployeeList().Where(e => e.IsApproved == false).ToList(),
                ApprovedEmployeeInfo = _oEmployeeBll.GetUnApprovedEmployeeList().Where(e => e.IsApproved).ToList()
            };
            return View(model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            _oEmployeeBll=new EmployeeBLL();
            var ch = form.GetValues("Id");
            if(ch != null)
            {
                _oEmployeeBll.ApproveEmployee(ch);
            }
            return RedirectToAction("Approval");
        }
        #endregion

        public JsonResult GetEmployeeCode()
        {
            _oEmployeeBll = new EmployeeBLL();
            var data = _oEmployeeBll.GetEmployeeCode();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}