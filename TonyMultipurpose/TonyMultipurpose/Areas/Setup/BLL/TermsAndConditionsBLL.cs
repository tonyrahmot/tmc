﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    public class TermsAndConditionsBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public CommonResult SaveTermsAndCondition(TermsAndConditions oTermsAndConditions)
        {
            CommonResult oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noRowCount = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountTypeId", oTermsAndConditions.AccountTypeId);
            objDbCommand.AddInParameter("TermsAndConditionsContent", oTermsAndConditions.TermsAndConditionsContent);
            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[setup_SaveTermsAndCondition]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    oCommonResult.Status = false;
                    oCommonResult.Message = "Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public TermsAndConditions GetTermsAndCondition(int accountTypeId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            TermsAndConditions termsAndConditions = new TermsAndConditions();
            try
            {
                objDbCommand.AddInParameter("AccountTypeId", accountTypeId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[setup_GetTermsAndCondition]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        termsAndConditions.TermsAndConditionsContent = Convert.ToString(objDbDataReader["TermsAndConditions"]);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return termsAndConditions;
        }
    }
}