$(function () {
    $("#tabs").tabs();
});

(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });

    $('#submitChecked').attr('disabled', 'disabled');

    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetCheckedLoanInfo",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "AccountTypeName" },
                { "data": "LoanDate" },
                //{ "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                { "data": "InterestRate" },
                { "data": "InstallmentAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" },
                {
                    "data": "LoanId",
                    "width": "50px",
                    "render": function (data) {
                        var url = window.applicationBaseUrl + "Loan/LoanApplicationApproval/PrintLoanApplication?loanId=" + data;
                        return '<a class="btn btn-info" target="_blank" href="' + url + '">Print</a>';
                    }
                }
            ],
            "order": [[0, "desc"]],
        });
        $('#LoanTable tbody').on('click', 'tr', function () {

            $('#submitChecked').removeAttr('disabled');

            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());

        });
    }

    function bindDataToControls(data) {

        $('#hdfLoanId').val(data.LoanId);
        document.getElementById('CustomerName').innerHTML = data.CustomerName;
        document.getElementById('AccountNumber').innerHTML = data.AccountNumber;
        document.getElementById('LoanType').innerHTML = data.AccountTypeName;
        document.getElementById('RefId').innerHTML = data.LoanId;
        document.getElementById('RefName').innerHTML = data.ReferenceName;
        document.getElementById('LoanDate').innerHTML = data.LoanDate;
        document.getElementById('LoanAmount').innerHTML = data.LoanAmount;
        document.getElementById('InterestRate').innerHTML = data.InterestRate;
        document.getElementById('InstallmentTypeId').innerHTML = data.InstallmentTypeName;
        document.getElementById('NumberOfInstallment').innerHTML = data.NumberOfInstallment;
        document.getElementById('ExtraFee').innerHTML = data.ExtraFee;
        document.getElementById('PurposeOfLoan').innerHTML = data.PurposeOfLoan;
        document.getElementById('TotalPurchaseAmount').innerHTML = data.TotalPurchaseAmount;
        document.getElementById('TotalAssetCost').innerHTML = data.TotalAssetCost;
        document.getElementById('DistributorName').innerHTML = data.DistributorName;
        //document.getElementById('COA_accountName').innerHTML = data.AccountCode;
        document.getElementById('ResidentialCondition').innerHTML = data.ResidentialConditionType;
        if (data.IsDepositsFromSaving == true) {
            $("#IsDepositsFromSaving").attr('checked', true);
        }
        else {
            $("#IsDepositsFromSaving").attr('checked', false);
        }
        document.getElementById('CustomerId').innerHTML = data.CustomerId;
        $("#CustomerId").change();
        document.getElementById('labelweeklyLoanAmount').innerHTML = data.LoanAmount;
        document.getElementById('labelweeklyInterestRate').innerHTML = data.InterestRate;
        document.getElementById('labelweeklyNoInstallment').innerHTML = data.NumberOfInstallment;
        document.getElementById('labelweeklyInstallmentAmount').innerHTML = data.InstallmentAmount;
        document.getElementById('labelweeklyInstallmentType').innerHTML = data.InstallmentTypeName;

        $('#submit').val('Update');
    }

    // Get customer Information
    $("#CustomerId").change(function () {
        //   $('#AccountNumber').empty();
        var customerId = document.getElementById('CustomerId').innerHTML;
        var json = { customerId: customerId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetCustomerInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#CustomerInfo').show();
                $('#customerImage').attr('src', data.CustomerImage);
                $('#CustomerName').val(data.CustomerName);
            }
        });
    });


    $('#clear').click(function () {
        window.location.reload();
    });

    $('#gaurantorInfoTab').on('click', function () {
        $('#gaurantorInfo').load(window.applicationBaseUrl + "Loan/LoanApplicationApproval/Guarantor?loanId=" + $('#hdfLoanId').val());
    });

    $('#commentsAndSignatoryInfoTab').on('click', function () {
        $('#CommentsAndSignatory').load(window.applicationBaseUrl + "Loan/LoanApplicationApproval/CommentsAndSignatory?loanId=" + $('#hdfLoanId').val());
    });

    $('#customersChequeInfoTab').on('click', function () {
        $('#CustomersCheque').load(window.applicationBaseUrl + "Loan/LoanApplicationApproval/CustomersChequeInfo?loanId=" + $('#hdfLoanId').val());
    });

    $('#financialInfoTab').on('click', function () {
        $('#Financial').load(window.applicationBaseUrl + "Loan/LoanApplicationApproval/FinancialJustification?loanId=" + $('#hdfLoanId').val());
    });


})();