﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class ClientInfo: CommonModel
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public int COAId { get; set; }
    }
}