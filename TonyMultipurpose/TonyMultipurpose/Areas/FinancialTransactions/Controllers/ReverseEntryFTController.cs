﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    [AuthenticationFilter]
    public class ReverseEntryFTController : Controller
    {
        FinancialTransactionsModel _objFTModel;
        FinancialTransactionsBLL _objFinancialTransationsBLL;
        // GET: FinancialTransactions/ReverseEntryFT


        public ActionResult Index()
        {
            _objFTModel = new FinancialTransactionsModel();
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            _objFTModel.TransactionDate = DateTime.Now.Date;
            _objFTModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            List<FinancialTransactionsModel> FinancialTransactionsList = _objFinancialTransationsBLL.GetNonReverseEntryFinancialTransactionslist(_objFTModel);
            return View(FinancialTransactionsList);
        }

        [HttpPost]
        public ActionResult Index(FormCollection formCollection)
        {
            List<FinancialTransactionsModel> allFinancialTransactions;
            _objFTModel = new FinancialTransactionsModel();
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            var transDate = Request.Form["TransactionDate"];
            if (string.IsNullOrEmpty(transDate))
                transDate = DateTime.Now.ToString("dd/MMM/yyyy");
            ViewBag.TransactionDate = transDate;
            _objFTModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            _objFTModel.TransactionDate = DateTime.ParseExact(transDate, "dd/MMM/yyyy", null);
            allFinancialTransactions = _objFinancialTransationsBLL.GetNonReverseEntryFinancialTransactionslist(_objFTModel).ToList();
            return View(allFinancialTransactions);
        }

        [HttpPost]
        public JsonResult ReverseEntryFinancialTransaction(string transactionIdToReverse)
        {
            bool status = true;
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            string transactionId = transactionIdToReverse.Split('_')[0].ToString();
            string voucherType = transactionIdToReverse.Split('_')[1].ToString();
            int createdBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            _objFinancialTransationsBLL.ReverseEntryFinancialTransaction(transactionId, voucherType, createdBy);

            return new JsonResult { Data = new { status = status } };
        }
    }
}