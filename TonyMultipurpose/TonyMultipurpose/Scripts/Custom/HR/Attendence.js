﻿(function () {

    $(document).ready(function () {

        // 1st replace first column header text with checkbox
        checkBoxHeader();

        //3rd click event for checkbox of each row
        $("input[name='ids']").click(function () {
            var totalRows = $("#checkableGrid td :checkbox").length;
            var checked = $("#checkableGrid td :checkbox:checked").length;

            if (checked == totalRows) {
                $("#checkableGrid").find("input:checkbox").each(function () {
                    this.checked = true;
                });
            }
            else {
                $("#cbSelectAll").removeAttr("checked");
            }
        });

        $("#search").keyup(function () {
            filter = new RegExp($(this).val(), 'i');
            $("#checkableGrid tbody tr").filter(function () {
                $(this).each(function () {
                    found = false;
                    $(this).children().each(function () {
                        content = $(this).html();
                        if (content.match(filter)) {
                            found = true;
                        }
                    });
                    if (!found) {
                        $(this).hide();
                    }
                    else {
                        $(this).show();
                    }
                });
            });
        });


    });
    function checkBoxHeader() {
        $("#checkableGrid th").each(function () {
            if ($.trim($(this).text().toString().toLowerCase()) === "{checkall}") {
                $(this).text('');
                $("<input/>", { type: "checkbox", id: "cbSelectAll", value: "" }).appendTo($(this));
                $(this).append("<span>Select All</span>");
            }
        });

        $("#cbSelectAll").on("click", function () {
            var ischecked = this.checked;
            $('#checkableGrid').find("input:checkbox").each(function () {
                this.checked = ischecked;
            });
        });
    }

    $("#InTimeInput").change(function () {
        $("#checkableGrid tbody td").each(function () {
            var a = $('#InTimeInput').val();
            $('#checkableGrid input:text').each(function () {
                $("input.InTime:text").val(a);
            });
        });
    });
    $("#OutTimeInput").change(function () {
        $("#checkableGrid tbody td").each(function () {
            var b = $('#OutTimeInput').val();
            $('#checkableGrid input:text').each(function () {
                $("input.OutTime:text").val(b);
            });
        });
    });

    $("#AttendanceDate").change(function () {
        updateGrid();
    });

    function updateGrid() {
        //$("#checkableGrid").swhgLoad("/Attendance/Attendance?AttendanceDate=" + $("#AttendanceDate").val(), "#checkableGrid");
        var a = $("#AttendanceDate").val();
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "HR/Attendance/Attendance",//'@Url.Action("Attendance", "Attendance")',
            contentType: "application/json; charset=utf-8",
            data: { AttendanceDate: $("#AttendanceDate").val() },
            success: function (d) {
                $('html body').html(d);
                $("#AttendanceDate").val(a);
            }
        });
    }


    $('#submit').click(function () {
        var isAllDataValid = IsValidCheck();
        if (isAllDataValid) {
            var data = bindAttendanceData();
            if (data.length <= 0) {
                return false;
            }
            $.ajax({
                url: window.applicationBaseUrl + "HR/Attendance/Attendance",//'@Url.Action("Attendance", "Attendance", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.Status === true) {
                        $("#showMessage").html(d.Message);
                        $.ajax({
                            type: "GET",
                            url: window.applicationBaseUrl + "Attendance/Attendance",//'@Url.Action("Attendance", "Attendance")',
                            data: JSON.stringify(data.AttendanceDate),
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                            }
                        });
                    } else {
                        alert('Failed');
                    }
                    $('#submit').val('Save');


                },
                error: function () {
                    alert('Error. Please try again.');
                    //$('#submit').val('Save');
                }
            });
        }
    });

    function bindAttendanceData() {
        var data = [];
        $("#checkableGrid tbody tr").each(function () {
            var attendance = {};
            if ($(this).find("#AttendanceStatus").is(":checked")) {
                attendance["AttendanceStatus"] = 1;
            } else {
                attendance["AttendanceStatus"] = 2;
            }
            attendance["EmpId"] = $(this).find("#EmpId").val();
            attendance["InTime"] = $(this).find("#InTime").val();
            attendance["OutTime"] = $(this).find("#OutTime").val();
            attendance["AttendanceDate"] = $("#AttendanceDate").val();
            attendance["Comments"] = $(this).find("#Comments").val();
            data.push(attendance);

        });
        return data;
    }
    function IsValidCheck() {
        var isAllDataValid = true;
        $("#checkableGrid tbody tr").each(function () {

            if ($(this).find("#InTime").val() == '' && $(this).find("#AttendanceStatus").is(":checked") || $(this).find("#InTime").val() == '**') {
                $(this).find("#InTime").val("**").css('color', 'red');
                isAllDataValid = false;

            }

            if ($(this).find("#OutTime").val() == '' && $(this).find("#AttendanceStatus").is(":checked") || $(this).find("#OutTime").val() == '**') {
                $(this).find("#OutTime").val("**").css('color', 'red');
                isAllDataValid = false;
            }
        });
        return isAllDataValid;
    }

    function clear() {
        $("#checkableGrid tbody tr").each(function () {
            $(this).find("#AttendanceStatus").prop('checked', false);
            $(this).find("#InTime").val('');
            $(this).find("#OutTime").val('');
            $(this).find("#Comments").val('');
        });
    }

    $('#FromDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#ToDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#AttendanceDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });
})();