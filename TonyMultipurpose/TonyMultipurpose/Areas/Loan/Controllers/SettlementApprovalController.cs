﻿using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
    //--ata
    public class SettlementApprovalController : Controller
    {
        // GET: Loan/SettlementApproval

        SattlementBLL objSattlementBll = new SattlementBLL();
        public ActionResult Index()
        {
            SettlementModel settlementModel = new SettlementModel();
            List<SettlementModel> UnApprovedSettlementList = objSattlementBll.GetUnApprovedSettlementList(settlementModel);
            return View(UnApprovedSettlementList);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("SettlementId");
            objSattlementBll.ApproveSettlement(ch);
            return RedirectToAction("Index");
        }

        public ActionResult Detail(int id)
        {
            SettlementModel settlement = objSattlementBll.GetSettlementInfoById(id);
            return View(settlement);
        }
    }
}