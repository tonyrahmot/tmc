﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class PaymentDetailsModel : CommonModel
    {      
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public long VoucherNo { get; set; }
        public string VoucherType { get; set; }
        public string VoucherCode { get; set; }
        public string DebitReason { get; set; }
        public string CreditReason { get; set; }
        public decimal? Amount { get; set; }
        public string ChequeNo { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BankAccountNo { get; set; }
        public string AccountToBankName { get; set; }
        public string AccountToBranchName { get; set; }
        public string AccountToBankAccountNo { get; set; }
        public string Remarks { get; set; }
        public string PayTo { get; set; }
        public string ReceiveFrom { get; set; }       
        public DateTime ChequeIssueDate { get; set; }

        public bool IsChequePayment { get; set; }
    }
}