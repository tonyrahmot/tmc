﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;
using System.Web.Mvc;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class MonthlyIncomeViewModel
    {
        public long MonthlyIncomeId { get; set; }

        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }
        public string IncomeType { get; set; }

        public decimal Amount { get; set; }
        public string SpouseIncomeType { get; set; }

        public decimal? SpouseAmount { get; set; }

        [Display(Name = "Foreign Remittance")]
        public decimal ForeignRemitAmount { get; set; }

        [Display(Name = "Other Income")]

        public decimal OtherIncomeAmount { get; set; }

        public short? CreatedBy { get; set; }

        [Display(Name = "Total")]
        public decimal TotalIncome { get; set; }
        public IEnumerable<SelectListItem> IncomeTypeItems { get; set; }

    }
}