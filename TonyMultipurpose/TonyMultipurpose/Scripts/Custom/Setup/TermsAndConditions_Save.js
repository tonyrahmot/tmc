﻿
$(function () {
    CKEDITOR.replace('TermsAndConditionsContent', {
        height: 400
    });
    $("#AccountTypeId").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Setup/TermsAndConditions/GetAccontTypeByAccountTitle",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#AccountTypeId").append('<option value="0">--Select Type--</option>');
            $.each(data, function (key, value) {
                $("#AccountTypeId").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
            });
        }
    });

    $("#AccountTypeId").change(function () {
        CKEDITOR.instances['TermsAndConditionsContent'].setData('');
        var json = {
            accountTypeId: $('#AccountTypeId').val()
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Setup/TermsAndConditions/GetTermsAndCondition",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                console.log(data);
                CKEDITOR.instances['TermsAndConditionsContent'].setData(data.TermsAndConditionsContent);
            }
        });

    });

    $('#btnSave').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var desc = CKEDITOR.instances['TermsAndConditionsContent'].getData();
            var data = {
                AccountTypeId: $('#AccountTypeId').val(),
                TermsAndConditionsContent: desc
            }
            $.ajax({
                url: window.applicationBaseUrl + "Setup/TermsAndConditions/Save",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        clearFields();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }

    });

    function checkValidation() {
        var isValid = true;
        if ($('#AccountTypeId').val() === '0') {
            isValid = false;
            $('#AccountTypeId').parent().next().find('span').css('visibility', 'visible');
        }
        else {
            $('#AccountTypeId').parent().next().find('span').css('visibility', 'hidden');
        }
        var desc = CKEDITOR.instances['TermsAndConditionsContent'].getData();
        if (desc === '') {
            isValid = false;
            $('#TermsAndConditionsContent').parent().next().find('span').css('visibility', 'visible');
        }
        else {
            $('#TermsAndConditionsContent').parent().next().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    function clearFields() {
        $('#AccountTypeId').val(0);
        CKEDITOR.instances['TermsAndConditionsContent'].setData('');
    }
});
