﻿using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Member.BLL;
using TonyMultipurpose.Areas.Member.Models;

namespace TonyMultipurpose.Areas.Member.Controllers
{
    //ataur
    public class MemberInfoController : Controller
    {
        // GET: Member/MemberInfo
        public ActionResult Index()
        {
            MemberInfoBLL objMemberInfoBll = new MemberInfoBLL();
            List<MemberInfo> objMemberInfoList = objMemberInfoBll.GetAllMemberInfo();
            return View(objMemberInfoList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            AccountTypeBLL objAccountTypeBll = new AccountTypeBLL();
            InvolvementInfoBLL objInvolvementInfoBll = new InvolvementInfoBLL();
            var model = new MemberInfo()
            {
                accountTypeNames = objAccountTypeBll.GetAllAccountTypeNameList(),
                involvementTypes = objInvolvementInfoBll.GetAllInvolvementTypeList()

            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Save(MemberInfo objMemberInfo)
        {
            if (ModelState.IsValid)
            {
                MemberInfoBLL objMemberInfoBll=new MemberInfoBLL();
                objMemberInfoBll.SaveMemberInfo(objMemberInfo);
            }
            return RedirectToAction("Index","MemberInfo");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            MemberInfoBLL objMemberInfoBll=new MemberInfoBLL();
            AccountTypeBLL objAccountTypeBll=new AccountTypeBLL();
            InvolvementInfoBLL objInvolvementInfoBll=new InvolvementInfoBLL();
            ViewBag.AccountType = objAccountTypeBll.GetAllAccountTypeNameList();
            ViewBag.Involvement = objInvolvementInfoBll.GetAllInvolvementTypeList();
            var memberinfo=objMemberInfoBll.GetMemberInfoForById(id);

            return View(memberinfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MemberInfo objMemberInfo)
        {
            if (ModelState.IsValid)
            {
                MemberInfoBLL objMemberInfoBll = new MemberInfoBLL();
                objMemberInfoBll.UpdateMemberInfo(objMemberInfo);
            }
            return RedirectToAction("Index", "MemberInfo");
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            MemberInfoBLL objMemberInfoBll = new MemberInfoBLL();
            AccountTypeBLL objAccountTypeBll = new AccountTypeBLL();
            InvolvementInfoBLL objInvolvementInfoBll = new InvolvementInfoBLL();
            ViewBag.AccountType = objAccountTypeBll.GetAllAccountTypeNameList();
            ViewBag.Involvement = objInvolvementInfoBll.GetAllInvolvementTypeList();
            var memberinfo = objMemberInfoBll.GetMemberInfoForById(id);

            return View(memberinfo);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ActionName("Delete")]
        //public ActionResult DeleteMemberInfo(int id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        MemberInfoBLL objMemberInfoBll=new MemberInfoBLL();
        //        objMemberInfoBll.DeleteMemberInfo(id);
        //    }
        //    return RedirectToAction("Index", "MemberInfo");
        //}
    }
}