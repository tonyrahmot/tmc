﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    //--ataur
    public class AccountTypeSetupBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForAccountTypeSetup(DbDataReader objDataReader, AccountTypeSetup objAccountTypeSetup)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "AccountSetupId":
                        if (!Convert.IsDBNull(objDataReader["AccountSetupId"]))
                        {
                            objAccountTypeSetup.AccountSetupId = Convert.ToByte(objDataReader["AccountSetupId"]);
                        }
                        break;
                    case "AccountTitleId":
                        if (!Convert.IsDBNull(objDataReader["AccountTitleId"]))
                        {
                            objAccountTypeSetup.AccountTitleId = objDataReader["AccountTitleId"].ToString();
                        }
                        break;
                    case "AccountTypeName":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeName"]))
                        {
                            objAccountTypeSetup.AccountTypeName= objDataReader["AccountTypeName"].ToString();
                        }
                        break;
                    case "AccountTypeCode":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeCode"]))
                        {
                            objAccountTypeSetup.AccountTypeCode = objDataReader["AccountTypeCode"].ToString();
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objAccountTypeSetup.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;

                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objAccountTypeSetup.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objAccountTypeSetup.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objAccountTypeSetup.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objAccountTypeSetup.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objAccountTypeSetup.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objAccountTypeSetup.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objAccountTypeSetup.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }


        //get all account set up information for index
        public List<AccountTypeSetup> GetAllAccountTypeSetupList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetupList = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllAccountTypeSetupList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objAccountTypeSetup=new AccountTypeSetup();
                        this.BuildModelForAccountTypeSetup(objDbDataReader, objAccountTypeSetup);
                       objAccountTypeSetupList.Add(objAccountTypeSetup);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objAccountTypeSetupList;
        }

        //save accountTypeSetup information
        public string CreateAccountTypeSetup(AccountTypeSetup objAccountTypeSetup)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountTitleId", objAccountTypeSetup.AccountTitleId);
            objDbCommand.AddInParameter("AccountTypeName", objAccountTypeSetup.AccountTypeName);
            objDbCommand.AddInParameter("AccountTypeCode", objAccountTypeSetup.AccountTypeCode);
            //objDbCommand.AddInParameter("CompanyId", objAccountTypeSetup.CompanyId);

            //objDbCommand.AddInParameter("CreatedBy", objAccountTypeSetup.CreatedBy);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateAccountTypeSetup", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //get account type setup by id for Edit and Delete
        public AccountTypeSetup GetAccountTypeSetupInfoById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetups = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup=new AccountTypeSetup();
            try
            {
                objDbCommand.AddInParameter("AccountSetupId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAccountTypeSetupInfoById", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objAccountTypeSetup=new AccountTypeSetup();
                        this.BuildModelForAccountTypeSetup(objDbDataReader, objAccountTypeSetup);
                       objAccountTypeSetups.Add(objAccountTypeSetup);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objAccountTypeSetup;
        }

        //update account type setup info
        public string UpdateAccountTypeSetup(AccountTypeSetup objAccountTypeSetup)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountSetupId", objAccountTypeSetup.AccountSetupId);
            objDbCommand.AddInParameter("AccountTitleId", objAccountTypeSetup.AccountTitleId);
            objDbCommand.AddInParameter("AccountTypeName", objAccountTypeSetup.AccountTypeName);
            objDbCommand.AddInParameter("AccountTypeCode", objAccountTypeSetup.AccountTypeCode);
            objDbCommand.AddInParameter("IsActive", objAccountTypeSetup.IsActive);
            //objDbCommand.AddInParameter("CompanyId", objAccountTypeSetup.CompanyId);

            //objDbCommand.AddInParameter("CreatedBy", objAccountTypeSetup.CreatedBy);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateAccountTypeSetup", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Update Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //Delete as IsDeleted=0
        public string DeleteAccountTypeSetup(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountSetupId", id);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteAccountTypeSetup", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}