﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.BLL
{
    public class LeaveInformationBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public List<LeaveInformation> GetLeaveType()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            LeaveInformation objLeaveInformation;
            List<LeaveInformation> objLeaveInformationList = new List<LeaveInformation>();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetLeaveType]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLeaveInformation = new LeaveInformation();
                        objLeaveInformation.LTypeId = Convert.ToInt32(objDbDataReader["LTypeId"].ToString());
                        objLeaveInformation.LeaveTypeName = objDbDataReader["LeaveTypeName"].ToString();
                        objLeaveInformationList.Add(objLeaveInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objLeaveInformationList;
        }

        public CommonResult SaveLeaveEntry(LeaveInformation objLeaveInformation)
        {
            int IsRowAffacted = 0;

            CommonResult objCommonResult = new CommonResult();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objLeaveInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CreatedBy", objLeaveInformation.CreatedBy);
            objDbCommand.AddInParameter("EmpCode", objLeaveInformation.EmpCode);
            objDbCommand.AddInParameter("LTypeId", objLeaveInformation.LTypeId);
            objDbCommand.AddInParameter("Description", objLeaveInformation.Description);
            objDbCommand.AddInParameter("FromDate", string.Format("{0:dd/MMM/yyyy}", objLeaveInformation.FromDate));
            objDbCommand.AddInParameter("ToDate", string.Format("{0:dd/MMM/yyyy}", objLeaveInformation.ToDate));
            try
            {
                IsRowAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SaveLeaveEntry]",
                   CommandType.StoredProcedure);

                if (IsRowAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    objCommonResult.Status = true;
                    objCommonResult.Message = "Saved Successfully";
                }
                else
                {
                    objCommonResult.Status = false;
                    objCommonResult.Message = "Saved Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                // throw new Exception("Database Error Occured", ex);
                objCommonResult.Message = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return objCommonResult;
        }
        public CommonResult UpdateLeaveEntry(LeaveInformation objLeaveInformation)
        {
            int IsRowAffacted = 0;

            CommonResult objCommonResult = new CommonResult();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objLeaveInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CreatedBy", objLeaveInformation.CreatedBy);
            objDbCommand.AddInParameter("EmpCode", objLeaveInformation.EmpCode);
            objDbCommand.AddInParameter("LTypeId", objLeaveInformation.LTypeId);
            objDbCommand.AddInParameter("Description", objLeaveInformation.Description);
            objDbCommand.AddInParameter("FromDate", string.Format("{0:dd/MMM/yyyy}", objLeaveInformation.FromDate));
            objDbCommand.AddInParameter("ToDate", string.Format("{0:dd/MMM/yyyy}", objLeaveInformation.ToDate));
            try
            {
                IsRowAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_UpdateLeaveEntry]",
                   CommandType.StoredProcedure);

                if (IsRowAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    objCommonResult.Status = true;
                    objCommonResult.Message = "Saved Successfully";
                }
                else
                {
                    objCommonResult.Status = false;
                    objCommonResult.Message = "Saved Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                // throw new Exception("Database Error Occured", ex);
                objCommonResult.Message = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return objCommonResult;
        }
        public List<string> GetEmployeeCodeForAutoComplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objEmployeeCodeList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("EmpId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetEmployeeCodeForAutoComplete]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["EmpCode"] != null)
                        {
                            objEmployeeCodeList.Add(objDbDataReader["EmpCode"].ToString());
                            //objEmployeeCodeList.Add(objDbDataReader["EmpId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objEmployeeCodeList;
        }

        public EmployeeInformation GetEmployeeNameByEmpCode(string EmpCode)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            EmployeeInformation objEmployeeInformation = new EmployeeInformation();
            List<EmployeeInformation> objEmployeeInformationList = new List<EmployeeInformation>();
            try
            {
                objDbCommand.AddInParameter("EmpCode", EmpCode);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetEmployeeNameByEmployeeCode]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeInformation = new EmployeeInformation();

                        objEmployeeInformation.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objEmployeeInformationList.Add(objEmployeeInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objEmployeeInformation;
        }
        public List<LeaveInformation> GetAllLeaveInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LeaveInformation> objLeaveInformationList = new List<LeaveInformation>();
            LeaveInformation objLeaveInformation = new LeaveInformation();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetAllLeaveInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLeaveInformation = new LeaveInformation();
                        objLeaveInformation.LeaveDays = Convert.ToInt32(objDbDataReader["LeaveDays"].ToString());
                        objLeaveInformation.EmpCode = objDbDataReader["EmpCode"].ToString();
                        objLeaveInformation.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objLeaveInformation.LeaveTypeName = objDbDataReader["LeaveTypeName"].ToString();
                        objLeaveInformation.Description = objDbDataReader["Description"].ToString();
                        objLeaveInformation.FromDate = Convert.ToDateTime(objDbDataReader["FromDate"].ToString());
                        objLeaveInformation.ToDate = Convert.ToDateTime(objDbDataReader["ToDate"].ToString());
                        objLeaveInformationList.Add(objLeaveInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objLeaveInformationList;
        }
        public List<LeaveInformation> GetAllLeaveInfoList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LeaveInformation> objLeaveInformationList = new List<LeaveInformation>();
            LeaveInformation objLeaveInformation = new LeaveInformation();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetAllLeaveInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLeaveInformation = new LeaveInformation();
                        objLeaveInformation.LeaveDays = Convert.ToInt32(objDbDataReader["LeaveDays"].ToString());
                        objLeaveInformation.EmpCode = objDbDataReader["EmpCode"].ToString();
                        objLeaveInformation.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objLeaveInformation.LeaveTypeName = objDbDataReader["LeaveTypeName"].ToString();
                        objLeaveInformation.LTypeId = Convert.ToInt32(objDbDataReader["LTypeId"].ToString());
                        objLeaveInformation.Description = objDbDataReader["Description"].ToString();
                        objLeaveInformation.FromDateShow = string.Format("{0:dd/MMM/yyyy}", objDbDataReader["FromDate"]);
                        objLeaveInformation.ToDateShow = string.Format("{0:dd/MMM/yyyy}", objDbDataReader["ToDate"]);
                        objLeaveInformationList.Add(objLeaveInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objLeaveInformationList;
        }
        public int GetHolidayInformation(string fromDate, string toDate)
        {
            int TotalLeaveCount = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            //LeaveInformation objLeaveInformation;
            //List<LeaveInformation> objLeaveInformationList = new List<LeaveInformation>();
            try
            {
                objDbCommand.AddInParameter("fromDate", fromDate);
                objDbCommand.AddInParameter("toDate", toDate);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetHolidayInformation]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        //objLeaveInformation = new LeaveInformation();
                        TotalLeaveCount = Convert.ToInt32(objDbDataReader["TotalLeaveCount"]);
                        //objLeaveInformation.Holiday =string.Format("{0:dd/MMM/yyyy}", objDbDataReader["Holiday"]);
                        //objLeaveInformationList.Add(objLeaveInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return TotalLeaveCount;
        }
        public string LeaveApproval(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                foreach (var EmpCode in ch)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("EmpCode", EmpCode);
                    objDbCommand.AddInParameter("IsApproved", true);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_LeaveApproval]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";
        }
        public LeaveInformation GetAllLeaveInfoDetailsByEmpCode(string EmpCode)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<LeaveInformation> objLeaveInformationList = new List<LeaveInformation>();
            LeaveInformation objLeaveInformation = new LeaveInformation();
            try
            {
                objDbCommand.AddInParameter("EmpCode", EmpCode);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetAllLeaveInfoDetailsByEmpCode]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objLeaveInformation = new LeaveInformation();
                        objLeaveInformation.LeaveDays = Convert.ToInt32(objDbDataReader["LeaveDays"].ToString());
                        objLeaveInformation.EmpCode = objDbDataReader["EmpCode"].ToString();
                        objLeaveInformation.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objLeaveInformation.LeaveTypeName = objDbDataReader["LeaveTypeName"].ToString();
                        objLeaveInformation.Description = objDbDataReader["Description"].ToString();
                        objLeaveInformation.FromDate = Convert.ToDateTime(objDbDataReader["FromDate"].ToString());
                        objLeaveInformation.ToDate = Convert.ToDateTime(objDbDataReader["ToDate"].ToString());
                        objLeaveInformationList.Add(objLeaveInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objLeaveInformation;
        }
        public string ApprovedLeaveInfoDetailsByEmpCode(LeaveInformation objLeaveInformation)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("EmpCode", objLeaveInformation.EmpCode);
                objDbCommand.AddInParameter("FromDate", objLeaveInformation.FromDate);
                objDbCommand.AddInParameter("ToDate", objLeaveInformation.ToDate);
                objDbCommand.AddInParameter("IsApproved", true);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_ApprovedLeaveInfoDetailsByEmpCode]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";
        }
    }
}