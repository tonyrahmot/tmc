﻿(function () {
    CKEDITOR.replace('NoticeContent', {
        height: 400,
        htmlEncodeOutput: true
    });

    noticeInfoTable();
    $('#submit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = bindData();
            $.ajax({
                url: window.applicationBaseUrl + "Setup/NoticeInfo/Index",
                type: "POST",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oNoticeInfoTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#NoticeTitle').val('');
                        CKEDITOR.instances['NoticeContent'].setData('');
                        //$('#hdfNoticeInfoId').val(d.oCommonResult.Id);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });


    function bindData() {
        var fd = new FormData();
        fd.append('Id', $('#hdfNoticeInfoId').val());
        fd.append('NoticeTitle', $('#NoticeTitle').val());
        fd.append('NoticeContent', CKEDITOR.instances['NoticeContent'].getData());
        fd.append('RoleId', $('#RoleId').val());
        fd.append('NoticeFile', document.getElementById('NoticeFile').files[0]);


        //            var data = {
        //                Id: $('#hdfNoticeInfoId').val(),
        //                NoticeTitle: $('#NoticeTitle').val(),
        //                NoticeContent: $('#NoticeContent').val()
        //            }
        return fd;
    }

    $('#delete').click(function () {
        var Id = $('#hdfNoticeInfoId').val();
        var json = {
            id: Id
        };
        $.ajax({
            url: window.applicationBaseUrl + "Setup/NoticeInfo/DeleteNoticeInfo",
            type: "POST",
            data: JSON.stringify(json),
            dataType: "JSON",
            contentType: "application/json",
            success: function () {
                oNoticeInfoTable.ajax.reload();
                document.getElementById('showMessage').innerHTML = 'Delete Sucessfully.';
            },
            error: function () {
                document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                oNoticeInfoTable.ajax.reload();
            }
        });
    });

    var oNoticeInfoTable;

    function noticeInfoTable() {
        oNoticeInfoTable = $('#NoticeInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Setup/NoticeInfo/GetAllNoticeInfo",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "NoticeTitle" },
                { "data": "NoticeContent" },
                {
                    "data": "Id",
                    render: function (data) {
                        var url = window.applicationBaseUrl + "Setup/NoticeInfo/Details/"+ data;
                        return "<a href='"+ url +"' class='btn btn-xs btn-info'>Details</a>";
                    }
                }
            ],
            "order": [[0, "desc"]]
        });
        $('#NoticeInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oNoticeInfoTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oNoticeInfoTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit').show();
        $('#NoticeTitle').val(data.NoticeTitle);
        CKEDITOR.instances['NoticeContent'].setData(data.NoticeContent);
        $('#hdfNoticeInfoId').val(data.Id);
        $('#RoleId').val(data.RoleId);
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    function CheckValidation() {
        var isValid = true;
        if ($('#NoticeTitle').val().trim() === '') {
            isValid = false;
            $('#NoticeTitle').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#NoticeTitle').parent().prev().find('span').css('visibility', 'hidden');
        }
        if (CKEDITOR.instances['NoticeContent'].getData().trim() === '') {
            isValid = false;
            $('#NoticeContent').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#NoticeContent').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }


})();

$(function () {
    $("#RoleId").empty();
    $.ajax({
        type: "GET",
        url: window.applicationBaseUrl + "Setup/NoticeInfo/GetRoleData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#RoleId").append('<option value="0">--All Roles--</option>');
            $.each(data, function (key, value) {
                $("#RoleId").append('<option value=' + value.RoleId + '>' + value.RoleName + '</option>');
            });
        }
    });
});