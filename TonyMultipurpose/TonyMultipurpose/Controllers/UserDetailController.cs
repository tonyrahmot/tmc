﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.BLL;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Controllers
{
    [AuthenticationFilter]
    public class UserDetailController : Controller
    {
        private BranchBLL objBranchBll = new BranchBLL();
        private CompanyBLL objCompanyBll = new CompanyBLL();

        // GET: UserDetail
        public ActionResult Index()
        {
            UserDetailBLL objUserDetailBll = new UserDetailBLL();
            List<UserDetail> obUserDetailList = objUserDetailBll.GetAllUserInfo();
            return View(obUserDetailList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            ViewBag.CompanyInfo = objCompanyBll.GetAllCompanyInfo();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserDetail objUserDetail)
        {
            if (ModelState.IsValid)
            {
                UserDetailBLL objUserDetailBll = new UserDetailBLL();
                objUserDetail.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objUserDetail.CreatedDate = DateTime.Now;
                objUserDetail.IsActive = true;
                objUserDetail.IsDeleted = false;
                objUserDetailBll.CreateUserInfo(objUserDetail);
            }
            return RedirectToAction("Index", "UserDetail");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
            var userInfo = objUserDetailPartialBll.GetUserInfo(id);
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            ViewBag.CompanyInfo = objCompanyBll.GetAllCompanyInfo();
            return View(userInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserDetailPartial objUserDetailPartial)
        {
            if (ModelState.IsValid)
            {
                UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
                objUserDetailPartial.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objUserDetailPartial.UpdatedDate = DateTime.Now;
                objUserDetailPartial.IsDeleted = false;
                objUserDetailPartialBll.UpdateUserInfo(objUserDetailPartial);
            }
            return RedirectToAction("Index", "UserDetail");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
            var userInfo = objUserDetailPartialBll.GetUserInfo(id);
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            ViewBag.CompanyInfo = objCompanyBll.GetAllCompanyInfo();
            return View(userInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteUserInfo(int id)
        {
            if (ModelState.IsValid)
            {
                UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
                objUserDetailPartialBll.DeleteUserInfo(id);
            }
            return RedirectToAction("Index", "UserDetail");

        }

        #region Reset User Password

        public ActionResult ResetUserPassword()
        {
            UserDetailBLL objUserDetailBll = new UserDetailBLL();
            List<UserDetail> obUserDetailList = objUserDetailBll.GetUserInfoForResetPassword();
            return View(obUserDetailList);
        }

        [HttpGet]
        public ActionResult EditUserPassword(int id)
        {
            UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
            var userInfo = objUserDetailPartialBll.GetUserInfo(id);
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            ViewBag.CompanyInfo = objCompanyBll.GetAllCompanyInfo();
            return View(userInfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserPassword(UserDetailPartial objUserDetailPartial)
        {
            UserDetailPartialBLL objUserDetailPartialBll = new UserDetailPartialBLL();
            objUserDetailPartial.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
            objUserDetailPartial.UpdatedDate = DateTime.Now;
            objUserDetailPartialBll.ChangeUserPassword(objUserDetailPartial);

            return RedirectToAction("ResetUserPassword", "UserDetail");
        }
        #endregion


    }
}