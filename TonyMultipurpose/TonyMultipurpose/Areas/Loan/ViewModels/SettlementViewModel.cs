﻿using System;
using System.Security.AccessControl;
using TonyMultipurpose.Areas.Loan.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    //--ata
    public class SettlementViewModel
    {
        public long SettlementId { get; set; }
        public string LoanNo { get; set; }
        public string LoanId { get; set; }
        public short SettlementTypeId { get; set; }
        public string SettlementTypeName { get; set; }
        public string SettlementDate { get; set; }
        public string SettlementDateShow { get; set; }
        public string Reason { get; set; }
        public bool? IsRejected { get; set; }
        public short BranchId { get; set; }
        public short CompnayId { get; set; }
        public string CreatedName { get; set; }
        public decimal CurrentInterest { get; set; }
        public decimal CurrentInstallmentAmount { get; set; }
        public decimal UpdatedInterest { get; set; }
        public decimal UpdatedInstallmentAmount { get; set; }
        public decimal BeginningBalance { get; set; }
        public decimal EndingBalance { get; set; }
        public decimal Principle { get; set; }
        public decimal Interest { get; set; }
        public int InstallmentNo { get; set; }

        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Gender { get; set; }
        public string CustomerImage { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public decimal LoanAmount { get; set; }
        public string SetTypeId { get; set; }
        public string RejectReason { get; set; }
    }
}