﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.HR.BLL;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.HR.Controllers
{
    [AuthenticationFilter]
    public class AttendanceController : Controller
    {
        // GET: HR/Attendance
        AttendanceBLL objAttendanceBLL = new AttendanceBLL();
        public ActionResult Attendance(string AttendanceDate)
        {
            if (AttendanceDate == null) AttendanceDate = DateTime.Now.ToString();
            var ActiveEployeeInformation = objAttendanceBLL.GetAllAttendenceInfoByAttendanceDate(AttendanceDate);
            return View(ActiveEployeeInformation);
        }

        [HttpPost]
        public ActionResult Attendance(List<AttendanceModel> attendances)
        {
            DateTime attendenceDate= Convert.ToDateTime(attendances[0].AttendanceDate);        
            objAttendanceBLL.ExistDateAndWiseDeleteFromAttendence(attendenceDate);
            var ActiveEployeeInformationsave = objAttendanceBLL.SaveEmployeeAttendenceInformation(attendances);
            return Json(ActiveEployeeInformationsave, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllAttendenceInfoByAttendanceDate(string AttendanceDate)
        {
            var AttendenceInfo = objAttendanceBLL.GetAllAttendenceInfoByAttendanceDate(AttendanceDate);
            return Json(AttendenceInfo, JsonRequestBehavior.AllowGet);
        }
    }
}