﻿(function () {

    $('#submit1').hide();
    arrierTable();
    var oTable;

    function arrierTable() {
        oTable = $('#ArrierTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveEmpArrierBill",//'@Url.Action("SaveEmpArrierBill", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                 { "data": "EmployeeName" },

                {
                    "data": "ArrierDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "ArrierAmount" },
                { "data": "Description" }
            ],
            "order": [[0, "desc"]]
        });
        $('#ArrierTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        document.getElementById("EmpId").readOnly = true;
        $('#EmpId').val(data.EmpCode);


        var date = new Date(data.ArrierDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var arrierDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#ArrierDate').val(arrierDate);
        $('#ArrierAmount').val(data.ArrierAmount);
        $('#Description').val(data.Description);
        $("#EmpId").change();
    }

    $('#clear').click(function () {
        window.location.reload();
    });


    $('#submit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                ArrierDate: $('#ArrierDate').val(),
                ArrierAmount: $('#ArrierAmount').val(),
                Description: $('#Description').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveEmpArrierBill",//'@Url.Action("SaveEmpArrierBill", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#ArrierDate').val('');
                        $('#ArrierAmount').val('');
                        $('#Description').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#submit1').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                ArrierDate: $('#ArrierDate').val(),
                ArrierAmount: $('#ArrierAmount').val(),
                Description: $('#Description').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateEmpArrierBill",//'@Url.Action("UpdateEmpArrierBill", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#ArrierDate').val('');
                        $('#ArrierAmount').val('');
                        $('#Description').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    //document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });

    $("#EmpId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpId").change(function () {
        $('#EmployeeName').empty();
        var EmpId = $("#EmpId").val();
        var json = { EmpId: EmpId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('EmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('Designation').innerHTML = data.LastName;
                document.getElementById('Center').innerHTML = data.FirstName;
                document.getElementById('NID').innerHTML = data.NID;
            }
        });
    });


    function CheckValidation() {
        var isValid = true;
        if ($('#EmpId').val().trim() === '') {
            isValid = false;
            $('#EmpId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#ArrierDate').val().trim() === '') {
            isValid = false;
            $('#ArrierDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ArrierDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#ArrierAmount').val().trim() === '') {
            isValid = false;
            $('#ArrierAmount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ArrierAmount').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    function FormateDate(jsonDate) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#ArrierDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });



    var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }


})();