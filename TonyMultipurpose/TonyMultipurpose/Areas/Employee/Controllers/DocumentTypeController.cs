﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class DocumentTypeController : Controller
    {
        // GET: Employee/DocumentType
        public ActionResult Index()
        {
            DocumentTypeBLL objDocumentTypeBll = new DocumentTypeBLL();
            List<DocumentType> ocumentTypeList = objDocumentTypeBll.GetAllDocumentType();
            return View(ocumentTypeList);
        }
        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(DocumentType objDocumentType)
        {
            if (ModelState.IsValid)
            {
                DocumentTypeBLL objDocumentTypeBll = new DocumentTypeBLL();
                objDocumentTypeBll.CreateDocumentType(objDocumentType);
            }
            return RedirectToAction("Index", "DocumentType");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            DocumentTypeBLL objDocumentTypeBLL = new DocumentTypeBLL();

            var documentType = objDocumentTypeBLL.GetAllDocumentType().Where(a => a.DocumentTypeId == id).FirstOrDefault();
            return View(documentType);
            
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DocumentType objDocumentType)
        {
            if (ModelState.IsValid)
            {
                DocumentTypeBLL objDocumentTypeBll = new DocumentTypeBLL();
                objDocumentTypeBll.UpadateDocumentType(objDocumentType);
            }
            return RedirectToAction("Index", "DocumentType");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            DocumentTypeBLL objDocumentTypeBLL = new DocumentTypeBLL();

            var documentType = objDocumentTypeBLL.GetAllDocumentType().Where(a => a.DocumentTypeId == id).FirstOrDefault();
            return View(documentType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteDocumentType(int id)
        {
            if (ModelState.IsValid)
            {
                DocumentTypeBLL objDocumentTypeBll = new DocumentTypeBLL();
                objDocumentTypeBll.DeleteDocumentType(id);
            }
            return RedirectToAction("Index", "DocumentType");

        }
    }
}