﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class TrainingController : Controller
    {
        // GET: Employee/Training
        public ActionResult Index()
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
            ViewBag.employeeName = EmployeeInfo;
            return View();
        }

        [HttpGet]
        public ActionResult Save()
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
            ViewBag.employeeName = EmployeeInfo;
            return View();
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(Training objTraining)
        {
            if (ModelState.IsValid)
            {
                TrainingBLL objTrainingBLL = new TrainingBLL();
                objTrainingBLL.SaveTraining(objTraining);
            }
            return RedirectToAction("Save", "Training");
        }

       
        public JsonResult GetAllEmployeeTrainingInfoById(int employeeId)
        {
            try
            {
                TrainingBLL objTrainingBLL = new TrainingBLL();
                if (employeeId >0)
                {
                    List<Training> training = objTrainingBLL.GetAllEmployeeTrainingInfoById(employeeId);
                    return Json(training, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                    List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
                    return Json(EmployeeInfo, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TrainingBLL objTrainingBLL = new TrainingBLL();
            var Training = objTrainingBLL.GetAllTraining(id);
            return View(Training);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Training objTraining)
        {
            if (ModelState.IsValid)
            {
                TrainingBLL objTrainingBLL = new TrainingBLL();
                objTrainingBLL.UpadateTraining(objTraining);
            }
            return RedirectToAction("Save", "Training");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            TrainingBLL objTrainingBLL = new TrainingBLL();
            var Training = objTrainingBLL.GetAllTraining(id);
            return View(Training);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteTraining(int id)
        {
            if (ModelState.IsValid)
            {
                TrainingBLL objTrainingBLL = new TrainingBLL();
                objTrainingBLL.DeleteTraining(id);
            }
            return RedirectToAction("Save", "Training");

        }
    }
}