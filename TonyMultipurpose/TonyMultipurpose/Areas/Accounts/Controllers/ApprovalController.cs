﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalDecisions.CrystalReports.Engine;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Accounts.Controllers
{
    [AuthenticationFilter]
    public class ApprovalController : Controller
    {
        // GET: Accounts/Approval

        AccountBLL objAccountBLL;
        AccountsViewModel objAccountViewModel;
        public ActionResult Index()
        {
            objAccountViewModel = new AccountsViewModel();
            objAccountBLL = new AccountBLL();
            //objAccountViewModel.IsApproved = false;
            //objAccountViewModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            //List<AccountsViewModel> allAccountTransactions = objAccountBLL.GetAllTransactions(objAccountViewModel);
            List<AccountsViewModel> allAccountTransactions = objAccountBLL.GetUnapproveTransactionNumber();
            return View(allAccountTransactions);
        }
        public ActionResult Approve(FormCollection form)
        {
            objAccountBLL = new AccountBLL();
            var ch = form.GetValues("Id");
            if(ch != null)
            objAccountBLL.ApproveAllTransaction(ch);
            return RedirectToAction("Index");
        }


        public ActionResult UnApprove(string ac)
        {
            if (ac == "Saving With Interest")
            {
                return (RedirectToAction("Index", "SavingsApproval", new { Area = "Transaction",ac }));
            }
            if (ac == "Saving Without Interest")
            {
                return RedirectToAction("Index", "SavingsApproval", new { Area = "Transaction",ac });
            }
            if (ac == "Deposit")
            {
                return RedirectToAction("Index", "SavingsApproval", new { Area = "Transaction",ac });
            }
            if (ac == "Withdraw")
            {
                return RedirectToAction("Index", "SavingsApproval", new { Area = "Transaction",ac });
            }
            if (ac == "SB Closing")
            {
                return RedirectToAction("Index", "SavingsApproval", new { Area = "Transaction", ac = "Closing" });
            }
            if (ac == "css")
            {
                return RedirectToAction("Index", "CSSApproval", new { Area = "CSS" });
            }
            if (ac == "FD")
            {
                return RedirectToAction("Index", "FDApproval", new { Area = "FixedDeposit" });
            }
            if (ac == "DB")
            {
                return RedirectToAction("Index", "ApprovalDB", new { Area = "DoubleBenefit" });
            }
            if (ac == "MB")
            {
                return RedirectToAction("Index", "Approval", new { Area = "MonthlyBenefit" });
            }
            if (ac == "Customer")
            {
                return RedirectToAction("Index", "Approval", new { Area = "Customer" });
            }
            //if (ac == "Term Loan")
            //{
            //    return RedirectToAction("Index", "LoanApplicationApproval", new { Area = "Loan" });
            //}

            //if (ac == "Continuous Loan")
            //{
            //    return RedirectToAction("Index", "LoanApplicationApproval", new { Area = "Loan" });
            //}

            if (ac == "UnApproved Loan")
            {
                return RedirectToAction("Index", "LoanApplicationApproval", new { Area = "Loan" });
            }

            if (ac == "Term Loan Installment")
            {
                return RedirectToAction("Index", "LoanInstallmentApproval", new { Area = "Loan" });
            }

            if (ac == "Continuous Loan Transaction")
            {
                return RedirectToAction("ContinuousLoanApprovalIndex", "ContinuousLoan", new { Area = "Loan" });
            }
            return RedirectToAction("Index");
        }
    }
}