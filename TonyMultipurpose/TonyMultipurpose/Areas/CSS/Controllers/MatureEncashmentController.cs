﻿using System.Web.Mvc;
using TonyMultipurpose.Areas.CSS.BLL;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.CSS.Controllers
{
    //--ata
    [AuthenticationFilter]
    public class MatureEncashmentController : Controller
    {
        // GET: CSS/MatureEncashment
        CSSBLL objCssBll=new CSSBLL();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objCssBll.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objCssBll.GetAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(CSSModel cssModel)
        {
            bool status = true;

            objCssBll.SaveMatureEncashmentInfo(cssModel);
           
            return new JsonResult { Data = new { status = status } };
        }
    }
}