﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class AutoDebitCreditBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;
        public List<AutoDebitCreditModel> GetAllAutoDebitCreditInfo(AutoDebitCreditModel _objAutoDebitCredit)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<AutoDebitCreditModel> objAutoDebitCreditList = new List<AutoDebitCreditModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[Accounts_GetAllAutoDebitCreditInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {  
                        _objAutoDebitCredit = new AutoDebitCreditModel();
                        _objAutoDebitCredit.Id = Convert.ToInt32(dr["Id"].ToString());
                        _objAutoDebitCredit.AccountFrom = dr["AccountFrom"].ToString();
                        _objAutoDebitCredit.AccountTo = dr["AccountTo"].ToString();
                        _objAutoDebitCredit.Amount = Convert.ToDecimal(dr["Amount"].ToString());
                        _objAutoDebitCredit.IsAuto = Convert.ToBoolean(dr["IsAuto"].ToString());
                        _objAutoDebitCredit.UserName = dr["Username"].ToString();
                        objAutoDebitCreditList.Add(_objAutoDebitCredit);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objAutoDebitCreditList;
        }

        public CommonResult SaveAutoDebitCreditInfo(AutoDebitCreditModel objAutoDebitCredit)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("AccountFrom", objAutoDebitCredit.AccountFrom);
            _objDbCommand.AddInParameter("AccountTo", objAutoDebitCredit.AccountTo);
            _objDbCommand.AddInParameter("Amount", objAutoDebitCredit.Amount);
            _objDbCommand.AddInParameter("IsAuto", objAutoDebitCredit.IsAuto);
            _objDbCommand.AddInParameter("IsActive", objAutoDebitCredit.IsActive);
            _objDbCommand.AddInParameter("Remarks", objAutoDebitCredit.Remarks);
            _objDbCommand.AddInParameter("CompanyId", objAutoDebitCredit.CompanyId);
            _objDbCommand.AddInParameter("IsDeleted", objAutoDebitCredit.IsDeleted);
            _objDbCommand.AddInParameter("CreatedBy", objAutoDebitCredit.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objAutoDebitCredit.CreatedDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Accounts_SaveAutoDebitCreditInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult UpdateAutoDebitCreditInfo(AutoDebitCreditModel objAutoDebitCredit)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("Id", objAutoDebitCredit.Id);
            _objDbCommand.AddInParameter("AccountFrom", objAutoDebitCredit.AccountFrom);
            _objDbCommand.AddInParameter("AccountTo", objAutoDebitCredit.AccountTo);
            _objDbCommand.AddInParameter("Amount", objAutoDebitCredit.Amount);
            _objDbCommand.AddInParameter("IsAuto", objAutoDebitCredit.IsAuto);
            _objDbCommand.AddInParameter("UpdatedBy", objAutoDebitCredit.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objAutoDebitCredit.UpdatedDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Accounts_UpdateAutoDebitCreditInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }

        public List<string> GetActiveAccountNumbers(string term, string type)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                _objDbCommand.AddInParameter("AccountNumber", term);
                _objDbCommand.AddInParameter("Type", type);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[uspGetActiveAccountNumbers]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objAccountNumberList;
        }

        public string GetAccountTypeByNumber(string AccountNumber)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            string accType = string.Empty;
            try
            {
                _objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[GetAccountTypeByNumber]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountType"] != null)
                        {
                            accType = objDbDataReader["AccountType"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return accType;
        }
    }
}