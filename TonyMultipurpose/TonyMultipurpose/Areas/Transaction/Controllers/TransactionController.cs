﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.Areas.Transaction.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Transaction.Controllers
{
    [AuthenticationFilter]
    public class TransactionController : Controller
    {
        TermLoanBLL objTermLoanBLL = new TermLoanBLL();
        private TransactionBLL objTransactionBll = new TransactionBLL();
        // GET: Transaction/Transaction
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexWithdraw()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(TransactionModel objTransactionModel)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objTransactionModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objTransactionModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objTransactionModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objTransactionModel.CreatedDate = DateTime.Now;
                objTransactionModel.IsActive = true;
                objTransactionModel.IsApproved = false;

                objTransactionBll.SaveTransaction(objTransactionModel);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };


        }

        [HttpPost]
        public ActionResult SaveInstallmentInformation(InstallmentInformation objSaveInstallmentInformation)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objSaveInstallmentInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objSaveInstallmentInformation.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objSaveInstallmentInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objSaveInstallmentInformation.CreatedDate = DateTime.Now;
                objSaveInstallmentInformation.IsApproved = false;
                objSaveInstallmentInformation.IsRejected = false;
                objSaveInstallmentInformation.IsEntry = true;
                objTermLoanBLL.SaveInstallmentInformation(objSaveInstallmentInformation);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public ActionResult SaveTermLoanDuePayment(InstallmentInformation objSaveInstallmentInformation)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objSaveInstallmentInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objSaveInstallmentInformation.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objSaveInstallmentInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objSaveInstallmentInformation.CreatedDate = DateTime.Now;
                objSaveInstallmentInformation.IsApproved = false;
                objSaveInstallmentInformation.IsRejected = false;
               var result = objTermLoanBLL.SaveTermLoanDuePayment(objSaveInstallmentInformation);
                if (result)
                {
                    status = true;
                }
                
            }
            return new JsonResult { Data = new { status = status } };
        }

        //[HttpGet]
        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objTransactionBll.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerInfoByAccountNumber(string id)
        {
            var result = objTransactionBll.GetCustomerInfoByAccountNumber(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerInfoByAccountNumberForWithdraw(string id)
        {
            var result = objTransactionBll.GetCustomerInfoByAccountNumberForWithdraw(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetConvertNumberToWord(int number)
        {
            var exsits = objTransactionBll.GetConvertNumberToWord(number);
            return new JsonResult { Data = exsits };
        }

        public JsonResult GetTransactionalCoaData()
        {
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUnApprovedTransactionByAcNo(string id)
        {
            bool exsits = objTransactionBll.GetUnApprovedTransactionByAcNo(id);
            if (exsits)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCloseAccountTransaction(string id)
        {
            bool exsits = objTransactionBll.GetCloseAccountTransaction(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        [HttpPost]
        public JsonResult GetAccountCodeInfo(int coaId)
        {
            COABLL objCOABLL = new COABLL();
            var data = objCOABLL.GetCOAInfo(coaId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomerAccountInfo(string id)
        {
            var model = objTransactionBll.GetSingleCustomerInfoByAccountNumberForWithdraw(id);
            return PartialView("_AccountInfo", model);
        }
    }
}