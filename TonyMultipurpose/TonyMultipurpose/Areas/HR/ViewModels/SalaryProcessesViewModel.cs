﻿using System;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class SalaryProcessesViewModel:CommonModel
    {
        public int SalaryId { get; set; }
        public string DlBookNumber { get; set; }
        public string ProcessDate { get; set; }
        public int EmpId { get; set; }
        public string EmployeeName { get; set; }
        public decimal? BasicSalary { get; set; }
        public string SalaryMonth { get; set; }
        public decimal? Gross { get; set; }
        public decimal? TA { get; set; }
        public decimal? DA { get; set; }
        public decimal? HRA { get; set; }
        public decimal? TellBill { get; set; }
        public decimal? Conveyance { get; set; }
        public decimal? TeleBillAndConveyance { get; set; }
        public decimal? TotalSalary { get; set; }
        public int MonthDays { get; set; }
        public int WorkingDays { get; set; }
        public int LeaveDays { get; set; }
        public int AbsentDays { get; set; }
        public decimal? AbsentAmount { get; set; }
        public decimal? PF { get; set; }
        public decimal? EFund { get; set; }
        public decimal? TotalDeduction { get; set; }
        public decimal? NetSalary { get; set; }
        
    }
}