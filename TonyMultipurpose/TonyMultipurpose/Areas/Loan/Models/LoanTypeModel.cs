﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class LoanTypeModel
    {
        public byte LoanTypeId { get; set; }
        public string LoanTypeName { get; set; }
    }
}