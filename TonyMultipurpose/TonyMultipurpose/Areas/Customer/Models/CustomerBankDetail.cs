﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Customer.Models
{
    public class CustomerBankDetail
    {
        public int CustomerDetailId { get; set; }
        public string CustomerId { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }

     //   [ForeignKey("CustomerId")]
     //   public virtual CustomerModel Customer { get; set; }
        public string AccountType { get; set; }        
        //public CustomerBankDetail()
        //{
        //    Customer = new CustomerModel();
        //}
    }
}