﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.DoubleBenefit.BLL;
using TonyMultipurpose.Areas.DoubleBenefit.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.DoubleBenefit.Controllers
{
    [AuthenticationFilter]
    public class ApprovalDBController : Controller
    {
        private readonly DoubleBenefitBLL _objDoubleBenefitBLL;
        Doublebenefit objDoublebenefit;
        public ApprovalDBController()
        {
            _objDoubleBenefitBLL = new DoubleBenefitBLL();
        }
        // GET: DoubleBenefit/ApprovalDB
        public ActionResult Index()
        {
            objDoublebenefit = new Doublebenefit();
            objDoublebenefit.IsApproved = false;
            //oCSSModel.IsActive = true;
            objDoublebenefit.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            List<Doublebenefit> DBTransactions = _objDoubleBenefitBLL.GetDoublebenefitTransactions(objDoublebenefit);
            return View(DBTransactions);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("Id");
            if(ch != null)
            {
                _objDoubleBenefitBLL.ApprovedDBTransaction(ch);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<Doublebenefit> transactions = new List<Doublebenefit>();
            Doublebenefit objDoublebenefit;
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objDoublebenefit = new Doublebenefit();
                    objDoublebenefit.TransactionId = ids[i];
                    objDoublebenefit.RejectReason = form.GetValues(objDoublebenefit.TransactionId)[0];
                    transactions.Add(objDoublebenefit);
                }
                _objDoubleBenefitBLL.RejectDoubleBenefitTransaction(transactions);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            if (id == null) return null;
            var doubleBenefitData = _objDoubleBenefitBLL.GetDoublebenefitTransactionsById(id); 
            return View(doubleBenefitData);
        }
       
    }
}