﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.TermLoan.Models
{
    public class Guarantor
    {
        public int? GuarantorId { get; set; }
        public string Name { get; set; }
        public string NID { get; set; }
        public string PhoneNo { get; set; }
        public string CustomerId { get; set; }
    }
}