﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class LoanModel
    {
        public string LoanId { get; set; }
        public string LoanNo { get; set; }
        public byte LoanTypeId { get; set; }
        public string CustomerId { get; set; }
        public string AccountNumber { get; set; }
        public string LoanDate { get; set; }
        public string LoanExpiryDate { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? InterestRateToDisplay { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? TotalAmountWithInterest { get; set; }
        public decimal? InstallmentAmount { get; set; }
        public byte? InstallmentTypeId { get; set; }
        public byte? NumberOfInstallment { get; set; }
        public decimal? ExtraFee { get; set; }
        public string TotalPurchaseAmount { get; set; }
        public decimal? TotalAssetCost { get; set; }
        public string PurposeOfLoan { get; set; }
        public string ReferenceName { get; set; }
        public string DistributorName { get; set; }
        public string Message { get; set; }
        public string Remarks { get; set; }
        public string RejectReason { get; set; }
        public bool IsEntry { get; set; }
        public bool IsChecked { get; set; }
        public bool IsApproved { get; set; }

        public bool IsCleared { get; set; }
        public bool IsSubmittedToCheck { get; set; }
        public bool IsRejected { get; set; }
        public bool IsClosed { get; set; }
        public Int16 CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public Int16? UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public Int16 BranchId { get; set; }
        public Int16 CompanyId { get; set; }

        public string CustomerName { get; set; }
        public string InstallmentTypeName { get; set; }
        public string LoanTypeName { get; set; }
        public string UserName { get; set; }
        public int? COAId { get; set; }
        public string AccountCode { get; set; }
        public  bool IsDepositsFromSaving { get; set; }

        /****************************/

        public decimal? TotalInterest { get; set; }
        public decimal? BeginningBalance { get; set; }
        public decimal? EndingBalance { get; set; }
        public decimal? Principle { get; set; }
        public decimal? Interest { get; set; }
        public decimal? DBRatio { get; set; }
        public decimal? NetIncome { get; set; }
        public decimal? ExistingEmi { get; set; }

        /****************************/

        public byte? ResidentialConditionId { get; set; }
        public string ResidentialConditionType { get; set; }

        public int AccountSetupId { get; set; }
        public string AccountTypeName { get; set; }

        /****************************************************/

        public decimal? WeeklyInstallmentAmount { get; set; }
        public decimal? MonthlyInstallmentAmount { get; set; }
        public byte? WeeklyInstallmentNumber { get; set; }
        public byte? MonthlyInstallmentNumber { get; set; }
        public decimal? WeeklyDbr { get; set; }
        public decimal? MonthlyDbr { get; set; }

        public decimal? Equity { get; set; }
        public string FullName { get; set; }
    }
}