﻿(function () {
    $("#search").keyup(function () {
        filter = new RegExp($(this).val(), 'i');
        $("#checkableGrid tbody tr").filter(function () {
            $(this).each(function () {
                found = false;
                $(this).children().each(function () {
                    content = $(this).html();
                    if (content.match(filter)) {
                        found = true;
                    }
                });
                if (!found) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });
    });
})();

function ConfirmDelete() {

    var result = confirm("Are you sure?");
    if (result) {
        return true;
    } else {
        return false;
    }
}

