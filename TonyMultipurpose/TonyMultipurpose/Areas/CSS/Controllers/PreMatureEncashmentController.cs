﻿using System.Web.Mvc;
using TonyMultipurpose.Areas.CSS.BLL;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.CSS.Controllers
{
    //--ata
    [AuthenticationFilter]
    public class PreMatureEncashmentController : Controller
    {
        // GET: CSS/PreMatureEncashment
        CSSBLL objCssbll=new CSSBLL();
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetPreMatureAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objCssbll.GetPreMatureAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(CSSModel cssModel)
        {
            bool status = true;
            CSSBLL objCssBll=new CSSBLL();
            objCssBll.SavePreMatureEncashmentInfo(cssModel);

            return new JsonResult { Data = new { status = status } };
        }
    }
}