﻿
   //Account No autocomplete
   $("#AccountNumber").autocomplete({
       source: function(request, response) {
           $.ajax({
               url: window.applicationBaseUrl + "CSS/MatureEncashment/AutoCompleteAccountNumber",
               type: "GET",
               dataType: "json",
               data: { term: request.term },
               success: function (data) {
                   response($.map(data, function(item) {
                       return { label: item, value: item };
                   }));
               }
           });
       },
       messages: {
           noResults: "",
           results: ""
       }
   });
$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});

//load data
$("#AccountNumber").change(function() {
    var AccountNumber = $("#AccountNumber").val();
    var json = {
        AccountNumber: AccountNumber
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "CSS/MatureEncashment/GetAccountInfoByAccountNo",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function(data) {
            //$("#AccountType").append('<option value="0">--Select Type--</option>');
            //$.each(data, function (key, value) {
            //    $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
            //});
            $('#CustomerName').val(data.CustomerName);
            $('#CustomerId').val(data.CustomerId);
            $('#InterestRate').val(data.InterestRate);
            $('#Duration').val(data.Duration);
            //var date = data.MatureDate;
            $('#MatureDate').val(FormateDate(data.MatureDate));

            $('#MatureAmount').val(data.MatureAmount);
            $('#InstallmentNumber').val(data.InstallmentNumber);
            $('#CustomerImage').attr('src', data.CustomerImage);
        }
    });
});


$('#submit').click(function() {

    var isAllValid = true;


    if ($('#AccountNumber').val().trim() == '') {
        $('#AccountNumber').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#AccountNumber').siblings('span.error').css('visibility', 'hidden');
    }

    if ($('#MatureAmount').val().trim() == '') {
        $('#MatureAmount').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#MatureAmount').siblings('span.error').css('visibility', 'hidden');
    }
    if ($('#InstallmentNumber').val().trim() != $('#Duration').val().trim()) {
        //$('#MatureAmount').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
        alert("Your account is not mature");
    } else if ($('#InstallmentNumber').val().trim() == '') {
        isAllValid = false;
    }
    if ($('#ChartOfAccount').val().trim() == 0) {
        $('#ChartOfAccount').siblings('span.error').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#ChartOfAccount').siblings('span.error').css('visibility', 'hidden');
    }
    //else if ($('#InstallmentNumber').val().trim() == $('#Duration').val().trim()) {
    //    isAllValid = true;
    //    //$('#MatureAmount').siblings('span.error').css('visibility', 'hidden');
    //    //alert("Your account is not mature");
    //}



    if (isAllValid) {

        var data = {
            AccountNumber: $('#AccountNumber').val(),
            CustomerName: $('#CustomerName').val(),
            CustomerId: $('#CustomerId').val(),
            InterestRate: $('#InterestRate').val(),
            Duration: $('#Duration').val(),
            MatureDate: $('#MatureDate').val(),
            MatureAmount: $('#MatureAmount').val(),
            COAId: $('#ChartOfAccount option:selected').val()

        }

        $.ajax({
            url: window.applicationBaseUrl + "CSS/MatureEncashment/Save",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function(d) {
                if (d.status === true) {
                    alert('Successfully done.');

                    $("#AccountNumber").val('');
                    $("#CustomerName").val('');
                    $("#CustomerId").val('');
                    $("#InterestRate").val('');
                    $("#Duration").val('');
                    $('#MatureDate').val('');
                    $('#MatureAmount').val('');
                    $('#ChartOfAccount').val('0');

                } else {
                    alert('Failed');
                }
                $('#submit').val('Save');
                window.location.reload();

            },
            error: function() {
                alert('Error. Please try again.');
                //$('#submit').val('Save');
            }
        });
    }
});


//convert datetime
function FormateDate(jsonDate) {
    var monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

    function IsDouble(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
            return false;
        }
        else if (charCode == 13) {
            return false;
        }
        status = "";
        return true;
    }
