﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class InstallmentTypeModel
    {
        public byte InstallmentTypeId { get; set; }
        public string InstallmentTypeName { get; set; }
    }
}