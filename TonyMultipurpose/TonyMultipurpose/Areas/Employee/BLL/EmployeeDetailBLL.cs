﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Employee.BLL
{
    public class EmployeeDetailBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForEmployeeDetail(DbDataReader objDataReader, EmployeeDetail objEmployeeInfo)
        {

            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "EmployeeId":
                        if (!Convert.IsDBNull(objDataReader["EmployeeId"]))
                        {
                            objEmployeeInfo.EmployeeId = Convert.ToInt16(objDataReader["EmployeeId"]);
                        }
                        break;
                    case "EmployeeName":
                        if (!Convert.IsDBNull(objDataReader["EmployeeName"]))
                        {
                            objEmployeeInfo.EmployeeName = objDataReader["EmployeeName"].ToString();
                        }
                        break;
                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objEmployeeInfo.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "MotherName":
                        if (!Convert.IsDBNull(objDataReader["MotherName"]))
                        {
                            objEmployeeInfo.MotherName = objDataReader["MotherName"].ToString();
                        }
                        break;
                    case "SpouseName":
                        if (!Convert.IsDBNull(objDataReader["SpouseName"]))
                        {
                            objEmployeeInfo.SpouseName = objDataReader["SpouseName"].ToString();
                        }
                        break;
                    case "PresentAddress":
                        if (!Convert.IsDBNull(objDataReader["PresentAddress"]))
                        {
                            objEmployeeInfo.PresentAddress = objDataReader["PresentAddress"].ToString();
                        }
                        break;
                    case "PermanentAddress":
                        if (!Convert.IsDBNull(objDataReader["PermanentAddress"]))
                        {
                            objEmployeeInfo.PermanentAddress = objDataReader["PermanentAddress"].ToString();
                        }
                        break;
                    case "DesignationId":
                        if (!Convert.IsDBNull(objDataReader["DesignationId"]))
                        {
                            objEmployeeInfo.DesignationId = Convert.ToByte(objDataReader["DesignationId"]);
                        }
                        break;
                    case "DesignationName":
                        if (!Convert.IsDBNull(objDataReader["DesignationName"]))
                        {
                            objEmployeeInfo.DesignationName = objDataReader["DesignationName"].ToString();
                        }
                        break;
                    case "DepartmentId":
                        if (!Convert.IsDBNull(objDataReader["DepartmentId"]))
                        {
                            objEmployeeInfo.DepartmentId = Convert.ToByte(objDataReader["DepartmentId"]);
                        }
                        break;
                    case "DepartmentName":
                        if (!Convert.IsDBNull(objDataReader["DepartmentName"]))
                        {
                            objEmployeeInfo.DepartmentName = objDataReader["DepartmentName"].ToString();
                        }
                        break;
                    case "Phone":
                        if (!Convert.IsDBNull(objDataReader["Phone"]))
                        {
                            objEmployeeInfo.Phone =
                                objDataReader["Phone"].ToString();
                        }
                        break;
                    case "Email":
                        if (!Convert.IsDBNull(objDataReader["Email"]))
                        {
                            objEmployeeInfo.Email = objDataReader["Email"].ToString();
                        }
                        break;
                    
                    case "Gender":
                        if (!Convert.IsDBNull(objDataReader["Gender"]))
                        {
                            objEmployeeInfo.Gender = objDataReader["Gender"].ToString();
                        }
                        break;
                    case "BloodGroup":
                        if (!Convert.IsDBNull(objDataReader["BloodGroup"]))
                        {
                            objEmployeeInfo.BloodGroup = Convert.ToByte(objDataReader["BloodGroup"].ToString());
                        }
                        break;
                    case "MaritalStatus":
                        if (!Convert.IsDBNull(objDataReader["MaritalStatus"]))
                        {
                            objEmployeeInfo.MaritalStatus = objDataReader["MaritalStatus"].ToString();
                        }
                        break;
                    case "Religion":
                        if (!Convert.IsDBNull(objDataReader["Religion"]))
                        {
                            objEmployeeInfo.Religion =
                                (Religion)Enum.Parse(typeof(Religion), objDataReader["Religion"].ToString());
                        }
                        break;
                    case "DateOfBirth":
                        if (!Convert.IsDBNull(objDataReader["DateOfBirth"]))
                        {
                            objEmployeeInfo.DateOfBirth = Convert.ToDateTime(objDataReader["DateOfBirth"].ToString());
                        }
                        break;
                    case "JoiningDate":
                        if (!Convert.IsDBNull(objDataReader["JoiningDate"]))
                        {
                            objEmployeeInfo.JoiningDate = Convert.ToDateTime(objDataReader["JoiningDate"].ToString());
                        }
                        break;
                    case "EndingDate":
                        if (!Convert.IsDBNull(objDataReader["EndingDate"]))
                        {
                            objEmployeeInfo.EndingDate = Convert.ToDateTime(objDataReader["EndingDate"].ToString());
                        }
                        break;
                    case "Nationality":
                        if (!Convert.IsDBNull(objDataReader["Nationality"]))
                        {
                            objEmployeeInfo.Nationality = Convert.ToByte(objDataReader["Nationality"].ToString());
                        }
                        break;
                    case "NationalityShow":
                        if (!Convert.IsDBNull(objDataReader["NationalityShow"]))
                        {
                            objEmployeeInfo.NationalityShow = objDataReader["NationalityShow"].ToString();
                        }
                        break;
                    case "NationalId":
                        if (!Convert.IsDBNull(objDataReader["NationalId"]))
                        {
                            objEmployeeInfo.NationalId = objDataReader["NationalId"].ToString();
                        }
                        break;
                    case "ProbationPeriod":
                        if (!Convert.IsDBNull(objDataReader["ProbationPeriod"]))
                        {
                            objEmployeeInfo.ProbationPeriod = objDataReader["ProbationPeriod"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objEmployeeInfo.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objEmployeeInfo.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objEmployeeInfo.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objEmployeeInfo.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objEmployeeInfo.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objEmployeeInfo.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objEmployeeInfo.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objEmployeeInfo.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        public string SaveEmployeeInfo(EmployeeDetail objEmployeeInfo)
        {

            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeName", objEmployeeInfo.EmployeeName);
            objDbCommand.AddInParameter("FatherName ", objEmployeeInfo.FatherName);
            objDbCommand.AddInParameter("MotherName ", objEmployeeInfo.MotherName);
            objDbCommand.AddInParameter("SpouseName ", objEmployeeInfo.SpouseName);
            objDbCommand.AddInParameter("DesignationId", objEmployeeInfo.DesignationId);
            objDbCommand.AddInParameter("DepartmentId", objEmployeeInfo.DepartmentId);
            objDbCommand.AddInParameter("PresentAddress", objEmployeeInfo.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", objEmployeeInfo.PermanentAddress);
            objDbCommand.AddInParameter("Gender", objEmployeeInfo.Gender);
            objDbCommand.AddInParameter("BloodGroup", objEmployeeInfo.BloodGroup);
            objDbCommand.AddInParameter("Religion ", objEmployeeInfo.Religion);
            objDbCommand.AddInParameter("MaritalStatus ", objEmployeeInfo.MaritalStatus);
            objDbCommand.AddInParameter("Phone", objEmployeeInfo.Phone);
            objDbCommand.AddInParameter("Email", objEmployeeInfo.Email);
            objDbCommand.AddInParameter("Nationality", objEmployeeInfo.Nationality);
            objDbCommand.AddInParameter("NationalId", objEmployeeInfo.NationalId);
            objDbCommand.AddInParameter("DateOfBirth", objEmployeeInfo.DateOfBirth);
            objDbCommand.AddInParameter("EndingDate", objEmployeeInfo.EndingDate);
            objDbCommand.AddInParameter("JoiningDate", objEmployeeInfo.JoiningDate);
            objDbCommand.AddInParameter("ProbationPeriod", objEmployeeInfo.ProbationPeriod);
            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateEmployeeDetail", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        public List<EmployeeDetail> GetAllEmployee()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeDetail> objEmployeeDetailList = new List<EmployeeDetail>();
            EmployeeDetail objEmployeeDetail;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetEmployeeDetailList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeDetail = new EmployeeDetail();
                        this.BuildModelForEmployeeDetail(objDbDataReader, objEmployeeDetail);
                        objEmployeeDetailList.Add(objEmployeeDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objEmployeeDetailList;
        }

        public List<EmployeeDetail> GetAllEmployeeList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<EmployeeDetail> objEmployeeDetailList = new List<EmployeeDetail>();
            EmployeeDetail objEmployeeDetail;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllEmployeeList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeDetail = new EmployeeDetail();
                        this.BuildModelForEmployeeDetail(objDbDataReader, objEmployeeDetail);
                        objEmployeeDetailList.Add(objEmployeeDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objEmployeeDetailList;
        }

        public string UpdateEmployeeDetailInfo(EmployeeDetail objEmployeeInfo)
        {

            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", objEmployeeInfo.EmployeeId);
            objDbCommand.AddInParameter("EmployeeName", objEmployeeInfo.EmployeeName);
            objDbCommand.AddInParameter("FatherName ", objEmployeeInfo.FatherName);
            objDbCommand.AddInParameter("MotherName ", objEmployeeInfo.MotherName);
            objDbCommand.AddInParameter("SpouseName ", objEmployeeInfo.SpouseName);
            objDbCommand.AddInParameter("DesignationId", objEmployeeInfo.DesignationId);
            objDbCommand.AddInParameter("DepartmentId", objEmployeeInfo.DepartmentId);
            objDbCommand.AddInParameter("PresentAddress", objEmployeeInfo.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", objEmployeeInfo.PermanentAddress);
            objDbCommand.AddInParameter("Gender", objEmployeeInfo.Gender);
            objDbCommand.AddInParameter("BloodGroup", objEmployeeInfo.BloodGroup);
            objDbCommand.AddInParameter("Religion ", objEmployeeInfo.Religion);
            objDbCommand.AddInParameter("MaritalStatus ", objEmployeeInfo.MaritalStatus);
            objDbCommand.AddInParameter("Phone", objEmployeeInfo.Phone);
            objDbCommand.AddInParameter("Email", objEmployeeInfo.Email);
            objDbCommand.AddInParameter("Nationality", objEmployeeInfo.Nationality);
            objDbCommand.AddInParameter("NationalId", objEmployeeInfo.NationalId);
            objDbCommand.AddInParameter("DateOfBirth", objEmployeeInfo.DateOfBirth);
            objDbCommand.AddInParameter("EndingDate", objEmployeeInfo.EndingDate);
            objDbCommand.AddInParameter("JoiningDate", objEmployeeInfo.JoiningDate);
            objDbCommand.AddInParameter("ProbationPeriod", objEmployeeInfo.ProbationPeriod);
            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateEmployeeDetail", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteEmployeeDetailInfo(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteEmployeeDetail", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<EmployeeDetail> GetEmployeeAllInformation(int EmployeeId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("EmployeeId", EmployeeId);
            DbDataReader objDbDataReader = null;
            List<EmployeeDetail> objEmployeeDetailList = new List<EmployeeDetail>();
            EmployeeDetail objEmployeeDetail;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetEmployeeInformation", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeDetail = new EmployeeDetail();
                        this.BuildModelForEmployeeDetail(objDbDataReader, objEmployeeDetail);
                        objEmployeeDetailList.Add(objEmployeeDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objEmployeeDetailList;
        }

    }
}