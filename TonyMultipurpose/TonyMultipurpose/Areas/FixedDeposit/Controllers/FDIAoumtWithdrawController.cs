﻿using System.Web.Mvc;
using TonyMultipurpose.Areas.FixedDeposit.BLL;
using TonyMultipurpose.Areas.FixedDeposit.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.FixedDeposit.Controllers
{
    [AuthenticationFilter]
    public class FDIAoumtWithdrawController : Controller
    {
        // GET: FixedDeposit/FDIAoumtWithdraw
        FixedDepositBLL objFixedDepositBll = new FixedDepositBLL();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objFixedDepositBll.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFDAccountInfoByAccountNoForInterestWithdraw(string AccountNumber)
        {
            var data = objFixedDepositBll.GetFDAccountInfoByAccountNoForInterestWithdraw(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(FixedDepositModel objFixedDepositModel)
        {
            bool status = true;

            objFixedDepositBll.SaveFDWithdrawInterestInfo(objFixedDepositModel);

            return new JsonResult { Data = new { status = status } };
        }

        public JsonResult GetTransactionalCoaData()
        {
            TransactionBLL objTransactionBll = new TransactionBLL();
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}