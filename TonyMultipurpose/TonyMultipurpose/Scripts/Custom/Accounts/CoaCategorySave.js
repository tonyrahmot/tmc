﻿$(document).ready(function () {
    $("#CategoryName").change(function () {
        var name = $("#CategoryName").val();
        var status = $("#showErrorMessage");
        var user = $.trim(name);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "Accounts/COACategory/GetCategoryNameCheck",{ CategoryName: name },
                function (data) {
                    if (data === true) {
                        status.html("<font color=green>'<b>" + name + "</b>' is not exist !</font>");
                        $('#btn_submit').attr('disabled', false);
                    } else {
                        status.html("<font color=red>'<b>" + name + "</b>' is exist !</font>");
                        $('#btn_submit').attr('disabled', true);
                    }
                });
        } else {
            status.html("");
        }
    });

    $('#btn_submit').click(function () {
        var name = $("#CategoryName").val();
        var groupId = $("#GroupId").val();
        var status = $("#showErrorMessage");
        var allData = true;
        if (groupId == '') {
            allData = false;
            status.html("Please Select Group Name");
            return allData;
        }
        else if (name == '') {
            allData = false;
            status.html("Please Type Chart of Accounts Category Name");
            return allData;
        }
        else {
            return allData;
        }
    });
});