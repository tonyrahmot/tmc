$(function () {
    $('#EntryDate').datepicker({
        dateFormat: 'dd/MM/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });
    $('#InstallmentDate').datepicker({
        dateFormat: 'dd/MM/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });
});