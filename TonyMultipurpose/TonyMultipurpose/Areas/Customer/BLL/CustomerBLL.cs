﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Customer.BLL
{
    public class CustomerBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForCustomer(DbDataReader objDataReader, CustomerModel objCustomerModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objCustomerModel.Id = Convert.ToInt32(objDataReader["Id"]);
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objCustomerModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objCustomerModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objCustomerModel.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objCustomerModel.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "MotherName":
                        if (!Convert.IsDBNull(objDataReader["MotherName"]))
                        {
                            objCustomerModel.MotherName = objDataReader["MotherName"].ToString();
                        }
                        break;
                    case "CustomerEntryDate":
                        if (!Convert.IsDBNull(objDataReader["CustomerEntryDate"]))
                        {
                            objCustomerModel.CustomerEntryDate = Convert.ToDateTime(objDataReader["CustomerEntryDate"].ToString());
                        }
                        break;
                    case "SpouseName":
                        if (!Convert.IsDBNull(objDataReader["SpouseName"]))
                        {
                            objCustomerModel.SpouseName = objDataReader["SpouseName"].ToString();
                        }
                        break;
                    case "TradeLicenseNo":
                        if (!Convert.IsDBNull(objDataReader["TradeLicenseNo"]))
                        {
                            objCustomerModel.TradeLicenseNo = objDataReader["TradeLicenseNo"].ToString();
                        }
                        break;
                    case "TradeLicAuthority":
                        if (!Convert.IsDBNull(objDataReader["TradeLicAuthority"]))
                        {
                            objCustomerModel.TradeLicAuthority = objDataReader["TradeLicAuthority"].ToString();
                        }
                        break;
                    case "TradeLicenseDate":
                        if (!Convert.IsDBNull(objDataReader["TradeLicenseDate"]))
                        {
                            objCustomerModel.TradeLicenseDate = Convert.ToDateTime(objDataReader["TradeLicenseDate"].ToString());
                        }
                        break;
                    case "RegistrationNo":
                        if (!Convert.IsDBNull(objDataReader["RegistrationNo"]))
                        {
                            objCustomerModel.RegistrationNo = objDataReader["RegistrationNo"].ToString();
                        }
                        break;
                    case "RegistrationAuthority":
                        if (!Convert.IsDBNull(objDataReader["RegistrationAuthority"]))
                        {
                            objCustomerModel.RegistrationAuthority = objDataReader["RegistrationAuthority"].ToString();
                        }
                        break;
                    case "Nationality":
                        if (!Convert.IsDBNull(objDataReader["Nationality"]))
                        {
                            objCustomerModel.Nationality = objDataReader["Nationality"].ToString();
                        }
                        break;
                    case "NationalId":
                        if (!Convert.IsDBNull(objDataReader["NationalId"]))
                        {
                            objCustomerModel.NationalId = objDataReader["NationalId"].ToString();
                        }
                        break;
                    case "BirthCertificateNo":
                        if (!Convert.IsDBNull(objDataReader["BirthCertificateNo"]))
                        {
                            objCustomerModel.BirthCertificateNo = objDataReader["BirthCertificateNo"].ToString();
                        }
                        break;
                    case "Position":
                        if (!Convert.IsDBNull(objDataReader["Position"]))
                        {
                            objCustomerModel.Position = objDataReader["Position"].ToString();
                        }
                        break;
                    case "AnotherCellNo":
                        if (!Convert.IsDBNull(objDataReader["AnotherCellNo"]))
                        {
                            objCustomerModel.AnotherCellNo = objDataReader["AnotherCellNo"].ToString();
                        }
                        break;
                    case "DateOfBirth":
                        if (!Convert.IsDBNull(objDataReader["DateOfBirth"]))
                        {
                            objCustomerModel.DateOfBirth = Convert.ToDateTime(objDataReader["DateOfBirth"].ToString());
                        }
                        break;
                    case "Gender":
                        if (!Convert.IsDBNull(objDataReader["Gender"]))
                        {
                            objCustomerModel.Gender = objDataReader["Gender"].ToString();
                        }
                        break;
                    case "OccupationAndPosition":
                        if (!Convert.IsDBNull(objDataReader["OccupationAndPosition"]))
                        {
                            objCustomerModel.OccupationAndPosition = objDataReader["OccupationAndPosition"].ToString();
                        }
                        break;
                    case "PassportNo":
                        if (!Convert.IsDBNull(objDataReader["PassportNo"]))
                        {
                            objCustomerModel.PassportNo = objDataReader["PassportNo"].ToString();
                        }
                        break;
                    case "PassportExpireDate":
                        if (!Convert.IsDBNull(objDataReader["PassportExpireDate"]))
                        {
                            objCustomerModel.PassportExpireDate = Convert.ToDateTime(objDataReader["PassportExpireDate"].ToString());
                        }
                        break;
                    case "TINNo":
                        if (!Convert.IsDBNull(objDataReader["TINNo"]))
                        {
                            objCustomerModel.TINNo = objDataReader["TINNo"].ToString();
                        }
                        break;
                    case "DrivingLicenseNo":
                        if (!Convert.IsDBNull(objDataReader["DrivingLicenseNo"]))
                        {
                            objCustomerModel.DrivingLicenseNo = objDataReader["DrivingLicenseNo"].ToString();
                        }
                        break;
                    case "PresentAddress":
                        if (!Convert.IsDBNull(objDataReader["PresentAddress"]))
                        {
                            objCustomerModel.PresentAddress = objDataReader["PresentAddress"].ToString();
                        }
                        break;
                    case "PermanentAddress":
                        if (!Convert.IsDBNull(objDataReader["PermanentAddress"]))
                        {
                            objCustomerModel.PermanentAddress = objDataReader["PermanentAddress"].ToString();
                        }
                        break;
                    case "OfficeAddress":
                        if (!Convert.IsDBNull(objDataReader["OfficeAddress"]))
                        {
                            objCustomerModel.OfficeAddress = objDataReader["OfficeAddress"].ToString();
                        }
                        break;
                    case "Home":
                        if (!Convert.IsDBNull(objDataReader["Home"]))
                        {
                            objCustomerModel.Home = objDataReader["Home"].ToString();
                        }
                        break;
                    case "Mobile":
                        if (!Convert.IsDBNull(objDataReader["Mobile"]))
                        {
                            objCustomerModel.Mobile = objDataReader["Mobile"].ToString();
                        }
                        break;
                    case "Office":
                        if (!Convert.IsDBNull(objDataReader["Office"]))
                        {
                            objCustomerModel.Office = objDataReader["Office"].ToString();
                        }
                        break;
                    case "Email":
                        if (!Convert.IsDBNull(objDataReader["Email"]))
                        {
                            objCustomerModel.Email = objDataReader["Email"].ToString();
                        }
                        break;
                    case "CustomerIncomeSource":
                        if (!Convert.IsDBNull(objDataReader["CustomerIncomeSource"]))
                        {
                            objCustomerModel.CustomerIncomeSource = objDataReader["CustomerIncomeSource"].ToString();
                        }
                        break;
                    case "CustomerImage":
                        if (!Convert.IsDBNull(objDataReader["CustomerImage"]))
                        {
                            var imagePath = objDataReader["CustomerImage"].ToString();
                            objCustomerModel.OldImage = imagePath;
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = imagePath.Replace(@"\", "/");
                                imagePath = string.Concat("/", imagePath);
                                //imagePath = string.Concat(HttpContext.Current.Request.Url,"/",imagePath);
                                objCustomerModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                //objCustomerModel.CustomerImage = "\\Not Found\\";
                                imagePath = imagePath.Replace(@"\", "/");
                                imagePath = string.Concat("/", imagePath);
                                objCustomerModel.CustomerImage = imagePath;
                            }

                        }
                        else
                        {
                            objCustomerModel.CustomerImage = "\\Not Found\\";
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objCustomerModel.CompanyId = Convert.ToInt16(objDataReader["CompanyId"]);
                        }
                        break;
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objCustomerModel.BranchId = Convert.ToInt16(objDataReader["BranchId"]);
                        }
                        break;
                    case "IsApproved":
                        if (!Convert.IsDBNull(objDataReader["IsApproved"]))
                        {
                            objCustomerModel.IsApproved = Convert.ToBoolean(objDataReader["IsApproved"].ToString());
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objCustomerModel.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objCustomerModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objCustomerModel.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objCustomerModel.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objCustomerModel.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objCustomerModel.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objCustomerModel.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objCustomerModel.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objCustomerModel.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void BuildModelForCustomerBankDetail(DbDataReader objDataReader, CustomerBankDetail objCustomerBankDetail)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "CustomerDetailId":
                        if (!Convert.IsDBNull(objDataReader["CustomerDetailId"]))
                        {
                            objCustomerBankDetail.CustomerDetailId = Convert.ToInt32(objDataReader["CustomerDetailId"]);
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objCustomerBankDetail.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "BankName":
                        if (!Convert.IsDBNull(objDataReader["BankName"]))
                        {
                            objCustomerBankDetail.BankName = objDataReader["BankName"].ToString();
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objCustomerBankDetail.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "AccountType":
                        if (!Convert.IsDBNull(objDataReader["AccountType"]))
                        {
                            objCustomerBankDetail.AccountType = objDataReader["AccountType"].ToString();
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //public string SaveCustomer(CustomerModel Customer)
        //{
        //    int noOfAffacted = 0;
        //    //   var customerId = GenerateCustomerId();
        //    string result = "failed";
        //    // Insert Customer

        //    objDataAccess = DataAccess.NewDataAccess();
        //    objDbCommand = null;
        //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
        //    objDbCommand.AddInParameter("CustomerImage", Customer.CustomerImage);
        //    objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
        //    objDbCommand.AddInParameter("CustomerName", Customer.CustomerName);
        //    objDbCommand.AddInParameter("CustomerEntryDate", Customer.CustomerEntryDate);
        //    objDbCommand.AddInParameter("FatherName", Customer.FatherName);
        //    objDbCommand.AddInParameter("MotherName", Customer.MotherName);
        //    objDbCommand.AddInParameter("SpouseName", Customer.SpouseName);
        //    objDbCommand.AddInParameter("TradeLicenseNo", Customer.TradeLicenseNo);
        //    objDbCommand.AddInParameter("TradeLicenseDate", Customer.TradeLicenseDate);
        //    objDbCommand.AddInParameter("RegistrationNo", Customer.RegistrationNo);

        //    objDbCommand.AddInParameter("RegistrationAuthority", Customer.RegistrationAuthority);
        //    objDbCommand.AddInParameter("Nationality", Customer.Nationality);
        //    objDbCommand.AddInParameter("NationalId", Customer.NationalId);
        //    objDbCommand.AddInParameter("DateOfBirth", Customer.DateOfBirth);
        //    objDbCommand.AddInParameter("Gender", Customer.Gender);
        //    objDbCommand.AddInParameter("OccupationAndPosition", Customer.OccupationAndPosition);
        //    objDbCommand.AddInParameter("PassportNo", Customer.PassportNo);
        //    objDbCommand.AddInParameter("PassportExpireDate", Customer.PassportExpireDate);

        //    objDbCommand.AddInParameter("TINNo", Customer.TINNo);
        //    objDbCommand.AddInParameter("DrivingLicenseNo", Customer.DrivingLicenseNo);
        //    objDbCommand.AddInParameter("PresentAddress", Customer.PresentAddress);
        //    objDbCommand.AddInParameter("PermanentAddress", Customer.PermanentAddress);
        //    objDbCommand.AddInParameter("OfficeAddress", Customer.OfficeAddress);
        //    objDbCommand.AddInParameter("Home", Customer.Home);
        //    objDbCommand.AddInParameter("Mobile", Customer.Mobile);
        //    objDbCommand.AddInParameter("Office", Customer.Office);
        //    objDbCommand.AddInParameter("Email", Customer.Email);
        //    objDbCommand.AddInParameter("CustomerIncomeSource", Customer.CustomerIncomeSource);
        //    objDbCommand.AddInParameter("CompanyId", Customer.CompanyId);
        //    objDbCommand.AddInParameter("BranchId", Customer.BranchId);

        //    try
        //    {

        //        noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspCreateCustomer]", CommandType.StoredProcedure);

        //        if (noOfAffacted > 0)
        //        {
        //            objDbCommand.Transaction.Commit();
        //            //Save Bank Info  
        //            if (Customer.CustomerBankDetails != null)
        //            {
        //                noOfAffacted = 0;
        //                foreach (CustomerBankDetail objCustomerBankDetail in Customer.CustomerBankDetails)
        //                {
        //                    //objDataAccess = DataAccess.NewDataAccess();
        //                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
        //                    objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
        //                    objDbCommand.AddInParameter("BankName", objCustomerBankDetail.BankName);
        //                    objDbCommand.AddInParameter("BranchName", objCustomerBankDetail.BranchName);
        //                    objDbCommand.AddInParameter("AccountType", objCustomerBankDetail.AccountType);

        //                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCustomerBankDetail]", CommandType.StoredProcedure);
        //                    if (noOfAffacted > 0)
        //                    {
        //                        objDbCommand.Transaction.Commit();
        //                    }
        //                    else
        //                    {
        //                        objDbCommand.Transaction.Rollback();
        //                    }
        //                }
        //            }

        //            //return "Saved";
        //            result = "Saved";
        //        }
        //        else
        //        {
        //            objDbCommand.Transaction.Rollback();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objDbCommand.Transaction.Rollback();
        //       // throw new Exception("Database Error Occured", ex);
        //    }

        //    finally
        //    {
        //        objDataAccess.Dispose(objDbCommand);
        //    }
        //    return result;
        //}
        public string SaveCustomer(CustomerModel Customer)
        {
            int noOfAffacted = 0;
            //   var customerId = GenerateCustomerId();
            string result = "failed";
            // Insert Customer

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CustomerImage", Customer.CustomerImage);
            objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
            objDbCommand.AddInParameter("CustomerName", Customer.CustomerName);
            objDbCommand.AddInParameter("CustomerEntryDate", Customer.CustomerEntryDate);
            objDbCommand.AddInParameter("FatherName", Customer.FatherName);
            objDbCommand.AddInParameter("MotherName", Customer.MotherName);
            objDbCommand.AddInParameter("SpouseName", Customer.SpouseName);
            objDbCommand.AddInParameter("Nationality", Customer.Nationality);
            objDbCommand.AddInParameter("NationalId", Customer.NationalId);
            objDbCommand.AddInParameter("BirthCertificateNo", Customer.BirthCertificateNo);
            objDbCommand.AddInParameter("Position", Customer.Position);
            objDbCommand.AddInParameter("Mobile", Customer.Mobile);
            objDbCommand.AddInParameter("AnotherCellNo", Customer.AnotherCellNo);
            objDbCommand.AddInParameter("DateOfBirth", Customer.DateOfBirth);
            objDbCommand.AddInParameter("Gender", Customer.Gender);
            objDbCommand.AddInParameter("OccupationAndPosition", Customer.OccupationAndPosition);
            objDbCommand.AddInParameter("PresentAddress", Customer.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", Customer.PermanentAddress);
            objDbCommand.AddInParameter("Email", Customer.Email);
            objDbCommand.AddInParameter("CustomerIncomeSource", Customer.CustomerIncomeSource);

            objDbCommand.AddInParameter("TradeLicenseNo", Customer.TradeLicenseNo);
            objDbCommand.AddInParameter("TradeLicAuthority", Customer.TradeLicAuthority);
            objDbCommand.AddInParameter("TradeLicenseDate", Customer.TradeLicenseDate);
            objDbCommand.AddInParameter("RegistrationNo", Customer.RegistrationNo);
            objDbCommand.AddInParameter("RegistrationAuthority", Customer.RegistrationAuthority);
            objDbCommand.AddInParameter("PassportNo", Customer.PassportNo);
            objDbCommand.AddInParameter("PassportExpireDate", Customer.PassportExpireDate);
            objDbCommand.AddInParameter("TINNo", Customer.TINNo);
            objDbCommand.AddInParameter("DrivingLicenseNo", Customer.DrivingLicenseNo);
            objDbCommand.AddInParameter("OfficeAddress", Customer.OfficeAddress);
            objDbCommand.AddInParameter("Home", Customer.Home);
            objDbCommand.AddInParameter("Office", Customer.Office);

            objDbCommand.AddInParameter("CompanyId", Customer.CompanyId);
            objDbCommand.AddInParameter("BranchId", Customer.BranchId);
            objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));

            try
            {

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspCreateCustomer]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    //Customer.Imagefile.SaveAs(Customer.CustomerImage);

                    objDbCommand.Transaction.Commit();
                    //Save Bank Info  
                    if (Customer.CustomerBankDetails != null)
                    {
                        noOfAffacted = 0;
                        foreach (CustomerBankDetail objCustomerBankDetail in Customer.CustomerBankDetails)
                        {
                            //objDataAccess = DataAccess.NewDataAccess();
                            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                            objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
                            objDbCommand.AddInParameter("BankName", objCustomerBankDetail.BankName);
                            objDbCommand.AddInParameter("BranchName", objCustomerBankDetail.BranchName);
                            objDbCommand.AddInParameter("AccountType", objCustomerBankDetail.AccountType);

                            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCustomerBankDetail]", CommandType.StoredProcedure);
                            if (noOfAffacted > 0)
                            {
                                objDbCommand.Transaction.Commit();
                            }
                            else
                            {
                                objDbCommand.Transaction.Rollback();
                            }
                        }
                    }

                    //return "Saved";
                    result = "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    result = "failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                // throw new Exception("Database Error Occured", ex);
                result = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return result;
        }
        public string GenerateCustomerId()
        {
            int Id = 0;
            string customerId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);

            //Get Max Id
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand.AddInParameter("FieldName", "CustomerId");
            objDbCommand.AddInParameter("TableName", "Customer");
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxId]", CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    customerId = Convert.ToString(Id).PadLeft(7, '0');
                }
            }
            objDbDataReader.Close();
            return customerId;
        }

        public List<CustomerModel> GetAllCustomerInfo(CustomerModel Customer)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CustomerModel> objCustomerModelList = new List<CustomerModel>();
            CustomerModel objCustomerModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllCustomerInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCustomerModel = new CustomerModel();
                        this.BuildModelForCustomer(objDbDataReader, objCustomerModel);

                        objCustomerModelList.Add(objCustomerModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCustomerModelList;
        }

        public List<CustomerModel> GetAllCustomerListInfo(CustomerModel Customer, string searchQuery = "")
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CustomerModel> objCustomerModelList = new List<CustomerModel>();
            CustomerModel objCustomerModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbCommand.AddInParameter("SearchString", searchQuery);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Customer_GetAllCustomerInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCustomerModel = new CustomerModel();
                        this.BuildModelForCustomer(objDbDataReader, objCustomerModel);

                        objCustomerModelList.Add(objCustomerModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCustomerModelList;
        }

        public List<CustomerBankDetail> GetCustomerWiseBankInfo(string id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CustomerBankDetail> objCustomerBankDetailList = new List<CustomerBankDetail>();
            CustomerBankDetail objCustomerBankDetail;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("CustomerId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCustomerBankInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCustomerBankDetail = new CustomerBankDetail();
                        this.BuildModelForCustomerBankDetail(objDbDataReader, objCustomerBankDetail);

                        objCustomerBankDetailList.Add(objCustomerBankDetail);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCustomerBankDetailList;
        }

        public string UpdateCustomer(CustomerModel Customer)
        {
            int noOfAffacted = 0;
            string result = "failed";
            // Insert Customer

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
            objDbCommand.AddInParameter("CustomerImage", Customer.CustomerImage);
            objDbCommand.AddInParameter("CustomerName", Customer.CustomerName);
            objDbCommand.AddInParameter("CustomerEntryDate", Customer.CustomerEntryDate);
            objDbCommand.AddInParameter("FatherName", Customer.FatherName);
            objDbCommand.AddInParameter("MotherName", Customer.MotherName);
            objDbCommand.AddInParameter("SpouseName", Customer.SpouseName);
            objDbCommand.AddInParameter("TradeLicenseNo", Customer.TradeLicenseNo);
            objDbCommand.AddInParameter("TradeLicAuthority", Customer.TradeLicAuthority);
            objDbCommand.AddInParameter("TradeLicenseDate", Customer.TradeLicenseDate);
            objDbCommand.AddInParameter("RegistrationNo", Customer.RegistrationNo);

            objDbCommand.AddInParameter("RegistrationAuthority", Customer.RegistrationAuthority);
            objDbCommand.AddInParameter("Nationality", Customer.Nationality);
            objDbCommand.AddInParameter("NationalId", Customer.NationalId);
            objDbCommand.AddInParameter("BirthCertificateNo", Customer.BirthCertificateNo);
            objDbCommand.AddInParameter("Position", Customer.Position);
            objDbCommand.AddInParameter("AnotherCellNo", Customer.AnotherCellNo);
            objDbCommand.AddInParameter("DateOfBirth", Customer.DateOfBirth);
            objDbCommand.AddInParameter("Gender", Customer.Gender);
            objDbCommand.AddInParameter("OccupationAndPosition", Customer.OccupationAndPosition);
            objDbCommand.AddInParameter("PassportNo", Customer.PassportNo);
            objDbCommand.AddInParameter("PassportExpireDate", Customer.PassportExpireDate);

            objDbCommand.AddInParameter("TINNo", Customer.TINNo);
            objDbCommand.AddInParameter("DrivingLicenseNo", Customer.DrivingLicenseNo);
            objDbCommand.AddInParameter("PresentAddress", Customer.PresentAddress);
            objDbCommand.AddInParameter("PermanentAddress", Customer.PermanentAddress);
            objDbCommand.AddInParameter("OfficeAddress", Customer.OfficeAddress);
            objDbCommand.AddInParameter("Home", Customer.Home);
            objDbCommand.AddInParameter("Mobile", Customer.Mobile);
            objDbCommand.AddInParameter("Office", Customer.Office);
            objDbCommand.AddInParameter("Email", Customer.Email);
            objDbCommand.AddInParameter("CustomerIncomeSource", Customer.CustomerIncomeSource);
            objDbCommand.AddInParameter("CompanyId", Customer.CompanyId);
            objDbCommand.AddInParameter("BranchId", Customer.BranchId);
            objDbCommand.AddInParameter("UpdatedBy", Customer.UpdatedBy);
            //objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);

            try
            {

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspUpdateCustomer]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    //Save Bank Info  
                    if (Customer.CustomerBankDetails != null)
                    {
                        noOfAffacted = 0;
                        foreach (CustomerBankDetail objCustomerBankDetail in Customer.CustomerBankDetails)
                        {
                            //objDataAccess = DataAccess.NewDataAccess();
                            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                            objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
                            objDbCommand.AddInParameter("BankName", objCustomerBankDetail.BankName);
                            objDbCommand.AddInParameter("BranchName", objCustomerBankDetail.BranchName);
                            objDbCommand.AddInParameter("AccountType", objCustomerBankDetail.AccountType);

                            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCustomerBankDetail]", CommandType.StoredProcedure);
                            if (noOfAffacted > 0)
                            {
                                objDbCommand.Transaction.Commit();
                            }
                            else
                            {
                                objDbCommand.Transaction.Rollback();
                            }
                        }
                    }

                    //  return "Saved";
                    result = "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                // throw new Exception("Database Error Occured", ex);
                result = "failed";
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return result;
        }

        public string DeleteCustomer(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CustomerId", id);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteCustomer", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string ApproveCustomer(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                objDbCommand.AddInParameter("CustomerId", id);

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApproveCustomer]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";

        }

        //public string SaveCustomerBankInfo(CustomerBankDetail objCustomerBankDetail)
        //{
        //    string result = "failed";
        //    int noRowCount = 0;

        //    objDataAccess = DataAccess.NewDataAccess();
        //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
        //    objDbCommand.AddInParameter("CustomerId", objCustomerBankDetail.CustomerId);
        //    objDbCommand.AddInParameter("BankName", objCustomerBankDetail.BankName);
        //    objDbCommand.AddInParameter("BranchName", objCustomerBankDetail.BranchName);
        //    objDbCommand.AddInParameter("AccountType", objCustomerBankDetail.AccountType);

        //    //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

        //    try
        //    {
        //        noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspSaveCustomerBankDetail", CommandType.StoredProcedure);

        //        if (noRowCount > 0)
        //        {
        //            objDbCommand.Transaction.Commit();
        //            result = "Saved";
        //        }
        //        else
        //        {
        //            objDbCommand.Transaction.Rollback();
        //            result = "failed";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objDbCommand.Transaction.Rollback();
        //        throw new Exception("Database Error Occured", ex);
        //    }
        //    finally
        //    {
        //        objDataAccess.Dispose(objDbCommand);
        //    }
        //    return result;
        //}

        public bool GetNIDCheck(string NID)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("NationalId", NID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetNIDCheck", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
        public DataTable GetAllCustomerInfoForFDF(CustomerModel Customer)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var dt = new DataTable();
            List<CustomerModel> objCustomerModelList = new List<CustomerModel>();
            CustomerModel objCustomerModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                //objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbCommand.AddInParameter("CustomerId", Customer.CustomerId);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetAllCustomerInfoForFDF]", CommandType.StoredProcedure);

                if (dt.Rows.Count > 0)
                {
                    var imagePath = Convert.ToString(dt.Rows[0]["CustomerImage"]);
                    if (imagePath.Contains("Uploads"))
                    {
                        imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                        imagePath = "/" + imagePath;
                        Customer.CustomerImage = imagePath;
                    }
                    else
                    {
                        Customer.CustomerImage = "\\Not Found\\";
                        dt.Rows[0]["CustomerImage"] = dt.Rows[0]["CustomerId"];
                    }
                }


            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }

        public List<string> GetAllCustomerIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objCustomerList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("CustomerId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCustomerIdForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["CustomerId"] != null)
                        {
                            objCustomerList.Add(objDbDataReader["CustomerId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCustomerList;
        }
    }
}