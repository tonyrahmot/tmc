﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class MemberInfo:CommonModel
    {
        [Key]
        public int MemberId { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Required]
        [Display(Name = "Account Title")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Account Title should between at least 2 to 35 characters long.")]
        public string AccountTitle { get; set; }


        [Required]
        [Display(Name = "Member Name")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Member Name should between at least 3 to 100 characters long.")]
        public string MemberName { get; set; }



        [Required]
        [Display(Name = "Account Type")]
        public byte AccountTypeId { get; set; }
        [Display(Name = "Account Type")]
        public string AccountTypeName { get; set; }
        public List<AccountType> accountTypeNames { get; set; }



        [Display(Name = "Trade Licence No.")]
        public string TradeLicenceNo { get; set; }


        [Display(Name = "Licence Issue Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yy}", ApplyFormatInEditMode = true)]
        public string TradeLicenceIssueDate { get; set; }
        public string TradeLicenceIssueDateString { get; set; }


        [Display(Name = "Registration No.")]
        public string RegistrationNumber { get; set; }


        [Display(Name = "Reg. Authority")]
        public string RegistrationAuthority { get; set; }




        [Display(Name = "Involvement with the A/C as")]
        public byte InvolvementId { get; set; }

        public string InvolvementType { get; set; }
        public List<InvolvementInfo> involvementTypes { get; set; }




        [Required]
        [Display(Name="Father's Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Father's Name should between at least 2 to 100 characters long.")]
        public string FatherName { get; set; }

        [Required]
        [Display(Name = "Mother's Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Mother's Name should between at least 2 to 100 characters long.")]
        public string MotherName { get; set; }


        [Display(Name = "Spouse Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Spouse Name should between at least 2 to 100 characters long.")]
        public string SpouseName { get; set; }


        [Required]
        [Display(Name = "Nationality")]
        [StringLength(12, MinimumLength = 2, ErrorMessage = "Nationality should between at least 2 to 12 characters long.")]
        public string Nationality { get; set; }



        [Required]
        [Display(Name = "NID")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "NID No cannot be longer than 25 characters.")]
        public string NID { get; set; }


        [Required]
        [Display(Name = "Birth Date")]
        public string DateOfBirth { get; set; }


        [Required]
        [Display(Name = "Gender")]
        public byte Gender { get; set; }
        public string GenderName { get; set; }


        [Display(Name = "Occupation")]
        public string Occupation { get; set; }


        [Display(Name = "Passport Number")]
        public string PassportNumber { get; set; }


        [Display(Name="Passport Expire Date")]

        [DisplayFormat(DataFormatString = "{0:dd/MMM/yy}", ApplyFormatInEditMode = true)]
        public string PassportExpireDate { get; set; }
        public string PassportExpireDateString { get; set; }

        [Display(Name = "TIN Number")]
        public string TINNumber { get; set; }


        [Display(Name = "Driving Licence Number")]
        public string DrivingLicenseNumber { get; set; }

        [Required]
        [Display(Name = "Present Address")]
        [StringLength(500, MinimumLength = 5, ErrorMessage = "Present Address should between at least 5 to 500 characters long.")]
        public string PresentAddress { get; set; }

        [Required]
        [Display(Name = "Parmanent Address")]
        [StringLength(500, MinimumLength = 5, ErrorMessage = "Parmanent Address should between at least 5 to 500 characters long.")]
        public string ParmanentAdress { get; set; }

        [Required]
        [Display(Name="Contact Number")]
        [RegularExpression("^[0-9,+,-]+$", ErrorMessage = "Digits Only.")]
        public string ContactNumber { get; set; }


        [Display(Name = "Email")]
        [StringLength(50, ErrorMessage = "Enter Valid Email Address !", MinimumLength = 3)]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", ErrorMessage = "Enter Valid Email Address !")]
        public string Email { get; set; }


        [Display(Name = "Income Source")]
        public string IncomeSource { get; set; }

    }
}