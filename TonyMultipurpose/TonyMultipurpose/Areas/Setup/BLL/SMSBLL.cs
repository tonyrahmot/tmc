﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    public class SMSBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        #region All Data Cutomer Table For SMS & Email
        public List<CustomerModel> GetAllCustomerInfoSMSEmail(CustomerModel _objCustomerSMSEmail)
        {
           
            DataTable objDataTable = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            
            List<CustomerModel> _objCustomerSMSEmailList = new List<CustomerModel>();

            try
            {
                objDataTable = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[Setup_GetAllCustomerInfoSMSEmail]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        _objCustomerSMSEmail = new CustomerModel();
                        _objCustomerSMSEmail.CustomerName = Convert.ToString(dr["CustomerName"]);
                        _objCustomerSMSEmail.CustomerId = Convert.ToString(dr["CustomerId"]);

                        _objCustomerSMSEmailList.Add(_objCustomerSMSEmail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return _objCustomerSMSEmailList;
        }
        #endregion All Data Cutomer Table For SMS & Email
    }
}