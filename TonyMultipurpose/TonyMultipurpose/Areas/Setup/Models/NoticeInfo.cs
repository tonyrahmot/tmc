﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.Models
{
    public class NoticeInfo : CommonModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string NoticeTitle { get; set; }

        public string NoticeContent { get; set; }
        public string NoticeFileName { get; set; }
        public string NoticeFileExtension { get; set; }
        public bool? IsPublished { get; set; }
        public bool? IsHidden { get; set; }
        public int? RoleId { get; set; }
        public HttpPostedFileBase NoticeFile { get; set; }
    }
}