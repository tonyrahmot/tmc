$('#UpdateNotification').hide();
$('#deleteNotification').hide();
// Load Function function
var oNotificationTable;
NotificationTable();
function NotificationTable() {
    oNotificationTable = $('#NotificationTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllNotificationInfo",
            "type": "get",
            "data": {
                "LoanId": $('#NotificationCheck_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "Notification" },
            { "data": "Msg" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false
    });
    $('#NotificationTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            oNotificationTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        bindDataNotification(oNotificationTable.row(this).data());
    });
}
function bindDataNotification(data) {
    $('#hdfNotificationCheckId').val(data.NotificationCheckId);
    $('#NotificationCheck_Notification').val(data.Notification);
    $('#NotificationCheck_LoanId').val(data.LoanId);
    $('#hdfNotificationStatus').val(data.Status);
    $('#Status').val($('#hdfNotificationStatus').val());
    $('#saveNotification').hide();
    $('#UpdateNotification').show();
    $('#deleteNotification').show();
}
function NotificationClearAll() {
    $('#NotificationCheck_Notification').val('');
    //$('#Status option:selected').val("false");
    $('#saveNotification').show();
    $('#UpdateNotification').hide();
    $('#deleteNotification').hide();
}
// caler function
$('#Notification_Clear').click(function () {

    NotificationClearAll();
});

// Save function
$('#saveNotification').click(function () {
    var isAllDataValid = true;
    if (isAllDataValid) {
        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/SaveNotification",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.oCommonResult.Status == true) {
                    oNotificationTable.ajax.reload();
                    NotificationClearAll();
                    $('#saveNotification').show();
                    $('#UpdateNotification').hide();
                    $('#deleteNotification').hide();
                    document.getElementById('Notification_showMessage').style.color = "green";
                }
                document.getElementById('Notification_showMessage').innerHTML = d.oCommonResult.Message;
            },
            error: function () {
                document.getElementById('Notification_showMessage').innerHTML = 'Error. Please try again.';
            }
        });
    }
});

function Binddata() {
    var data = {
        LoanId: $('#NotificationCheck_LoanId').val(),
        Notification: $('#NotificationCheck_Notification').val(),
        NotificationCheckId: $('#hdfNotificationCheckId').val(),
        Status: $('#Status').val()
    }
    return data;
}

$('#UpdateNotification').click(function () {

    var isAllDataValid = true;

    if (isAllDataValid) {

        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateNotification",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    NotificationClearAll();
                    oNotificationTable.ajax.reload();
                    $('#saveNotification').show();
                    $('#UpdateNotification').hide();
                    $('#deleteNotification').hide();
                }
            },
            error: function () {

            }
        });
    }
});
$('#deleteNotification').click(function () {
    if (confirm("Are you sure?")) {
        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/DeleteNotification",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    oNotificationTable.ajax.reload();
                    NotificationClearAll();
                    $('#saveNotification').show();
                    $('#UpdateNotification').hide();
                    $('#deleteNotification').hide();
                }
            },
            error: function () {

            }
        });
    }
    return false;
});