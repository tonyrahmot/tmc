﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.ViewModels;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class PromotionModel:PromotionViewModel
    {
        public int PromotionId { get; set; }

        public string EmpCode { get; set; }

        public int DesigId { get; set; }
        public int FunctionalId { get; set; }

        public string EffectiveDate { get; set; }

        public string Description { get; set; }

        public short CompanyId { get; set; }

        public bool IsApproved { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short? UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}