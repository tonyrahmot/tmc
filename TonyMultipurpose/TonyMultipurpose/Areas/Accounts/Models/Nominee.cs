﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace TonyMultipurpose.Areas.Accounts.Models
{
    public class Nominee
    {
        public long Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountNumber { get; set; }
        public string NomineeName { get; set; }
        public string NomineeFatherName { get; set; }
        public string NomineeMotherName { get; set; }
        public string NomineeSpouseName { get; set; }
        public string NomineeNationality { get; set; }
        public string NomineeNationalId { get; set; }
        public DateTime NomineeDateOfBirth { get; set; }
        public string NomineeMobile { get; set; }
        public string NomineeOccupation { get; set; }
        public string NomineeDesignation { get; set; }
        public string NomineeAddress { get; set; }
        public string NomineeImage { get; set; }
        public string NomineePosition { get; set; }
        public HttpPostedFileBase Imagefile { get; set; }
        public int AccountTypeSetUp { get; set; }

        public bool NewImage { get; set; }
        public string Relation { get; set; }
    }
}