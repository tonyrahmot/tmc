﻿$(document).ready(function () {
    BankDetailItems = [];
    var banks = window.CustomerBankDetails;
    if (banks != null) {
        BankDetailItems = banks;
        GeneratedItemsTable();
    }
    function GeneratedItemsTable() {
        if (BankDetailItems.length > 0) {
            var $table = $('<table id="myTable" class="table table-striped table-hover" width="100%"/>');
            $table.append('<thead><tr class="success">' +
                '<th>Bank Name</th>' +
                '<th>Branch Name</th>' +
                '<th>Account Type</th>' +
                //'<th>Action</th>' +
                //'<th></th>' +
                '</tr></thead>');

            var $tbody = $('<tbody/>');
            $.each(BankDetailItems, function (i, val) {
                var $row = $('<tr class="active"/>');
                $row.append($('<td/>').html(val.BankName));
                $row.append($('<td/>').html(val.BranchName));
                $row.append($('<td/>').html(val.AccountType));

                $tbody.append($row);
            });

            $table.append($tbody);

            $('#BankItems').html($table);
        }
    }

});