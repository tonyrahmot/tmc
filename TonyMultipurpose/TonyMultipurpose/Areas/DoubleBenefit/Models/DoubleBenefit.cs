﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.DoubleBenefit.Models
{
    public class Doublebenefit : CommonModel
    {
        public long Id { get; set; }
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
      
        public string TransactionType { get; set; }

        [Display(Name = "A/C No")]
        public string AccountNumber { get; set; }

       

        public string CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public string FatherName { get; set; }

        public short? BranchId { get; set; }
        public string BranchName { get; set; }
        public string CompanyName { get; set; }

        public int? AccountSetupId { get; set; }

        
        public string AccountTypeName { get; set; }

       
        public decimal Debit { get; set; }

       
        public decimal Credit { get; set; }

        [Display(Name = "Balance")]
        public decimal? Balance { get; set; }


        [Display(Name = "Particular")]
        public string Particular { get; set; }

        [Required]
        public decimal? Amount { get; set; }

        [Display(Name = "Phone")]
        public string Mobile { get; set; }
        public decimal? InterestRate { get; set; }

        public string DurationofMonth { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? InstallmentYear { get; set; }
        public string DurationofYear { get; set; }

        [Display(Name = "Mature Amount")]
        public decimal? MatureAmount { get; set; }

        [Display(Name = "Mature Date")]
        public DateTime? MatureDate { get; set; }
        public string CustomerImage { get; set; }

        public string Username { get; set; }
        public decimal? AccruedBalance { get; set; }
        public decimal PreMatureAmount { get; set; }
        public decimal CompanyAmount { get; set; }
        public string RejectReason { get; set; }

        public int COAId { get; set; }
    }
}