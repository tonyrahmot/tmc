﻿namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class InstallmentTypeViewModel
    {
        public byte InstallmentTypeId { get; set; }
        public string InstallmentTypeName { get; set; }
    }
}