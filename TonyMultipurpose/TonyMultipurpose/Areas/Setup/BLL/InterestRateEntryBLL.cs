﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    //--ataur
    public class InterestRateEntryBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForInterestRateEntry(DbDataReader objDataReader, InterestRateEntry objInterestRateEntry)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objInterestRateEntry.Id = Convert.ToInt32(objDataReader["Id"]);
                        }
                        break;
                    case "AccountSetupId":
                        if (!Convert.IsDBNull(objDataReader["AccountSetupId"]))
                        {
                            objInterestRateEntry.AccountSetupId = Convert.ToByte(objDataReader["AccountSetupId"]);
                        }
                        break;
                    case "AccountTitleId":
                        if (!Convert.IsDBNull(objDataReader["AccountTitleId"]))
                        {
                            objInterestRateEntry.AccountTitleId = objDataReader["AccountTitleId"].ToString();
                        }
                        break;
                    case "AccountTypeName":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeName"]))
                        {
                            objInterestRateEntry.AccountTypeName = objDataReader["AccountTypeName"].ToString();
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objInterestRateEntry.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "Date":
                        if (!Convert.IsDBNull(objDataReader["Date"]))
                        {
                            objInterestRateEntry.Date = Convert.ToDateTime(objDataReader["Date"].ToString());
                        }
                        break;
                        case "ShowDate":
                        if (!Convert.IsDBNull(objDataReader["ShowDate"]))
                        {
                            objInterestRateEntry.ShowDate =string.Format("{0:dd/MMM/yyyy}", objDataReader["ShowDate"]);
                        }
                        break;
                    case "Duration":
                        if (!Convert.IsDBNull(objDataReader["Duration"]))
                        {
                            objInterestRateEntry.Duration =objDataReader["Duration"].ToString();
                        }
                        break;

                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objInterestRateEntry.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    

                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objInterestRateEntry.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objInterestRateEntry.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objInterestRateEntry.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objInterestRateEntry.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objInterestRateEntry.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objInterestRateEntry.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objInterestRateEntry.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Save Interst rate 
        /// </summary>
        /// <param name="objInterestRateEntry"></param>
        /// <returns></returns>
        public string AddInterestEntry(InterestRateEntry objInterestRateEntry)
        {

            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountSetupId", objInterestRateEntry.AccountType);
            objDbCommand.AddInParameter("InterestRate", objInterestRateEntry.InterestRate);
            objDbCommand.AddInParameter("Date", objInterestRateEntry.Date);
            objDbCommand.AddInParameter("Duration", objInterestRateEntry.Duration);

            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);

            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateInterestRateEntry", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        /// <summary>
        /// Get all Interest rate entry information for index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<InterestRateEntry> GetInterestRateEntryInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InterestRateEntry>objInterestRateEntrieList=new List<InterestRateEntry>();
            InterestRateEntry objInterestRateEntry;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetInterestRateEntryInfo", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objInterestRateEntry=new InterestRateEntry();
                        this.BuildModelForInterestRateEntry(objDbDataReader, objInterestRateEntry);
                        objInterestRateEntrieList.Add(objInterestRateEntry);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objInterestRateEntrieList;
        }



        /// <summary>
        /// update interest rate entry information by id
        /// </summary>
        /// <param name="objInterestRateEntry"></param>
        /// <returns></returns>
        public string UpdateInterestRateEntryInfo(InterestRateEntry objInterestRateEntry)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", objInterestRateEntry.Id);
            objDbCommand.AddInParameter("AccountSetupId", objInterestRateEntry.AccountSetupId);
            objDbCommand.AddInParameter("InterestRate", objInterestRateEntry.InterestRate);
            objDbCommand.AddInParameter("Date", objInterestRateEntry.ShowDate);
            objDbCommand.AddInParameter("Duration", objInterestRateEntry.Duration);
            objDbCommand.AddInParameter("IsActive", objInterestRateEntry.IsActive);

            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);

            objDbCommand.AddInParameter("UpdatedBy", SessionUtility.TMSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateInterestRateEntry", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }




        /// <summary>
        /// get interest rate info for edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public InterestRateEntry GetInterestRateEntryInfoById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
           List<InterestRateEntry> objInterestRateEntrieList=new List<InterestRateEntry>();
            InterestRateEntry objInterestRateEntry=new InterestRateEntry();
            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetInterestRateInfoById", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objInterestRateEntry=new InterestRateEntry();
                        this.BuildModelForInterestRateEntry(objDbDataReader, objInterestRateEntry);
                      objInterestRateEntrieList.Add(objInterestRateEntry);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objInterestRateEntry;
        }


        public List<AccountTypeSetup> GetAccontTypeByAccountTitle( string accountTitle)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetupList = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup = new AccountTypeSetup();
            try
            {
                objDbCommand.AddInParameter("AccountTitleId", accountTitle);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountTypeNamesByAccountTitle]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountTypeSetup = new AccountTypeSetup();
                        if (objDbDataReader["AccountSetupId"] != null)
                        {
                            objAccountTypeSetup.AccountSetupId = Convert.ToInt16(objDbDataReader["AccountSetupId"]);
                            objAccountTypeSetup.AccountTypeName = Convert.ToString(objDbDataReader["AccountTypeName"]);
                            objAccountTypeSetupList.Add(objAccountTypeSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountTypeSetupList;
        }
    }
}