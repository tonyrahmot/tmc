﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.Models
{
    //--ataur
    public class AccountTypeSetup:CommonModel
    {
        public int AccountSetupId { get; set; }

        [Required]
        [Display(Name = "Account Title")]
        public string AccountTitleId { get; set; }

        [Required]
        [Display(Name = "Account Type Name")]
        public string AccountTypeName { get; set; }

        [Required]
        [Display(Name = "Account Code")]
        public string AccountTypeCode { get; set; }

        //[Display(Name = "Company Name")]
        //public int? CompanyId { get; set; }
    }
}