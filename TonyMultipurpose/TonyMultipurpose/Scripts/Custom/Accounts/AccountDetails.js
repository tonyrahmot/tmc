﻿
            (function () {
                $('#NomineeDateOfBirth').datepicker({ dateFormat: 'dd/M/yy', type: Text }).click(function () { $(this).focus(); });

                //Load Account Type Dropdown
                $("#AccountTitle").change(function () {
                    var AccountTitle = $("#AccountTitle").val();
                    $("#AccountType").empty();
                    var json = {
                        AccountTitle: AccountTitle
                    };
                    $.ajax({
                        type: "POST",
                        url:window.applicationBaseUrl + "Account/Account/GetAccontTypeByAccountTitle",// '@Url.Action("GetAccontTypeByAccountTitle", "Account")',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json),
                        success: function (data) {
                            $("#AccountType").append('<option value="0">Select Type</option>');
                            $.each(data, function (key, value) {
                                $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
                            });
                            $("#AccountType").val(window.accountType);
                        }
                    });
                });

                if ($('#AccountTitle').val() !== '') {
                    $('#AccountTitle').change();
                }

                //Enable Customer Id Text Box
                $('#AccountType').on('change', function () {
                    if ($('#AccountType').val() === "0") {
                        $('#CustomerId').attr('readonly', 'readonly');
                    } else {
                        $('#CustomerId').removeAttr("readonly");
                    }
                });


                //customer id autocomplete
                $("#CustomerId").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url:window.applicationBaseUrl + "Accounts/Account/AutoCompleteCustomerId",
                            type: "GET",
                            dataType: "json",
                            data: { term: request.term },
                            success: function (data) {
                                response($.map(data, function (item) {
                                    return { label: item, value: item };
                                }));
                            }
                        });
                    },
                    messages: {
                        noResults: "",
                        results: ""
                    }
                });

                //Load Customer Information
                $("#CustomerId").change(function () {
                    ClearCustomerData();
                    var customerId = $("#CustomerId").val();
                    var json = { customerId: customerId };

                    $.ajax({
                        type: "POST",
                        url:window.applicationBaseUrl + "Accounts/Account/GetCustomerInfo",// '@Url.Action("GetCustomerInfo", "Account",new {Area= "Accounts" })',
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(json),
                        success: function (data) {
                            BindCustomerData(data);
                        }
                    });
                });

                if ($('#CustomerId').val() !== '') {
                    $('#CustomerId').change();
                }
                //Add Nominees to Nominee Table
                var nominees = [];

                var existingNominees = window.existingNominees;
                if (existingNominees != null) {
                    nominees = existingNominees;
                    GenerateNomineeTable(nominees);
                }

                $('#add').click(function () {
                    var isValidNominee = CheckNomineeFieldsValidation();

                    //Add item to list if valid
                    if (isValidNominee) {
                        nominees.push({
                            NomineePosition: $("#NomineePosition option:selected").val(),
                            NomineeName: $('#NomineeName').val().trim(),
                            NomineeFatherName: $('#NomineeFatherName').val().trim(),
                            NomineeMotherName: $('#NomineeMotherName').val().trim(),
                            NomineeSpouseName: $('#NomineeSpouseName').val().trim(),
                            NomineeNationality: $('#NomineeNationality').val().trim(),
                            NomineeNationalId: $('#NomineeNationalId').val().trim(),
                            NomineeDateOfBirth: $('#NomineeDateOfBirth').val().trim(),
                            NomineeMobile: $('#NomineeMobile').val().trim(),
                            NomineeOccupation: $('#NomineeOccupation').val().trim(),
                            NomineeDesignation: $('#NomineeDesignation').val().trim(),
                            NomineeAddress: $('#NomineeAddress').val().trim()
                        });
                        //populate order items
                        GenerateNomineeTable(nominees);
                        ClearNomineeFields();
                    }
                });

                //Save Account Info
                $('#submit').click(function () {
                    var isValid = CheckValidation(nominees);

                    if (isValid) {
                        var data = {
                            CustomerId: $('#CustomerId').val(),
                            AccountType: $('#AccountType option:selected').val(),
                            SpecialInstructions:$('#SpecialInstructions').val(),
                            Nominees: nominees
                        }

                        $.ajax({
                            url:window.applicationBaseUrl + "Accounts/Account/Edit",
                            type: "POST",
                            data: JSON.stringify(data),
                            dataType: "JSON",
                            contentType: "application/json",
                            success: function (d) {
                                if (d.status === true) {
                                    alert('Successfully done.');
                                    //Clear All Fields
                                    $('#AccountNumber').val(d.oCommonResult.Id);
                                    $('#AccountNumber').focus();
                                    $("#AccountTitle").val('');
                                    $("#AccountType").val('');
                                    $('#CustomerId').val('');
                                    $('#CustomerId').attr('readonly', 'readonly');
                                    nominees = [];
                                    ClearCustomerData();
                                    ClearNomineeFields();
                                    $('#Nominees').empty();
                                } else {
                                    alert('Failed');
                                }
                                $('#submit').val('Save');
                            },
                            error: function () {
                                alert('Error. Please try again.');
                                $('#submit').val('Save');
                            }
                        });
                    };
                });

            })();

function ClearCustomerData() {
    $('#CustomerName').val('');
    $('#FatherName').val('');
    $('#MotherName').val('');
    $('#SpouseName').val('');
    $('#DateOfBirth').val('');
    $('#Nationality').val('');
    $('#NationalId').val('');
    $('#PassportNo').val('');
    $('#PassportExpireDate').val('');
    $('#OccupationAndPosition').val('');
    $('#CustomerIncomeSource').val('');
    $('#PresentAddress').val('');
    $('#PermanentAddress').val('');
    $('#Office').val('');
    $('#Home').val('');
    $('#Mobile').val('');
    $('#SpecialInstructions').val('');
}

function ClearNomineeFields() {
    $('.nomineeFields').val('');
}

function BindCustomerData(data) {
    $('#CustomerName').val(data.CustomerName);
    $('#FatherName').val(data.FatherName);
    $('#MotherName').val(data.MotherName);
    $('#SpouseName').val(data.SpouseName);
    $('#DateOfBirth').val(FormateDate(data.DateOfBirth));
    $('#Nationality').val(data.Nationality);
    $('#NationalId').val(data.NationalId);
    $('#PassportNo').val(data.PassportNo);
    $('#PassportExpireDate').val(FormateDate(data.PassportExpireDate));
    $('#OccupationAndPosition').val(data.OccupationAndPosition);
    $('#CustomerIncomeSource').val(data.CustomerIncomeSource);
    $('#PresentAddress').val(data.PresentAddress);
    $('#PermanentAddress').val(data.PermanentAddress);
    $('#Office').val(data.Office);
    $('#Home').val(data.Home);
    $('#Mobile').val(data.Mobile);
}

function FormateDate(jsonDate) {
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

//Check validaion for nominee
function CheckNomineeFieldsValidation() {
    var isValidNominee = true;
    if ($('#NomineePosition').val().trim() === '') {
        isValidNominee = false;
        $('#NomineePosition').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeName').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeFatherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeFatherName').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeMotherName').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMotherName').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeNationality').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeNationality').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeNationalId').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeNationalId').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeDateOfBirth').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeDateOfBirth').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeMobile').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeMobile').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#NomineeAddress').val().trim() === '') {
        isValidNominee = false;
        $('#NomineeAddress').parent().next().find('span').css('visibility', 'visible');
    }
    return isValidNominee;
}

//Check validaion for Save
function CheckValidation(nominees) {
    var isValid = true;
    if ($('#AccountTitle').val().trim() === '') {
        isValid = false;
        $('#AccountTitle').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#AccountType').val().trim() === '0') {
        isValid = false;
        $('#AccountType').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#CustomerId').val().trim() === '') {
        isValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#DateOfBirth').val().trim() === '') {
        isValid = false;
        alert('Insert currect Customer Id and load customer information');
    }
    if (nominees.length === 0) {
        isValid = false;
        alert('Add atleast one nominee');
    }
    return isValid;
}

//function for show added items in table
function GenerateNomineeTable(Nominees) {
    if (Nominees.length > 0) {
        var $table = $('<table id="myTable" class="table table-striped table-hover" width="100%"/>');
        $table.append('<thead><tr class="success">' +
            '<th>Position</th>' +
            '<th>Name</th>' +
            '<th>Date of Birth</th>' +
            '<th>Mobile</th>' +
            '<th>Address</th>' +
            '</tr></thead>');

        var $tbody = $('<tbody/>');
        $.each(Nominees, function (i, val) {
            var dob = val.NomineeDateOfBirth;
            console.log(dob);
            if (dob.indexOf('Date') > -1) {
                dob = FormateDate(val.NomineeDateOfBirth);
            }
            var $row = $('<tr class="active"/>');
            $row.append($('<td/>').html(val.NomineePosition));
            $row.append($('<td/>').html(val.NomineeName));
            $row.append($('<td/>').html(dob));
            $row.append($('<td/>').html(val.NomineeMobile));
            $row.append($('<td/>').html(val.NomineeAddress));

            $tbody.append($row);
        });

        $table.append($tbody);

        $('#Nominees').html($table);
    }
}
