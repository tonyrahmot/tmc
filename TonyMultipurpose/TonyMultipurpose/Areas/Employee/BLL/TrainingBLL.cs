﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Employee.BLL
{
    public class TrainingBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForTraining(DbDataReader objDataReader, Training objTraining)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "TrainingId":
                        if (!Convert.IsDBNull(objDataReader["TrainingId"]))
                        {
                            objTraining.TrainingId = Convert.ToByte(objDataReader["TrainingId"]);
                        }
                        break;
                    case "EmployeeId":
                        if (!Convert.IsDBNull(objDataReader["EmployeeId"]))
                        {
                            objTraining.EmployeeId = Convert.ToByte(objDataReader["EmployeeId"]);
                        }
                        break;
                    case "EmployeeName":
                        if (!Convert.IsDBNull(objDataReader["EmployeeName"]))
                        {
                            objTraining.EmployeeName = objDataReader["EmployeeName"].ToString();
                        }
                        break;
                    case "OrganizationName":
                        if (!Convert.IsDBNull(objDataReader["OrganizationName"]))
                        {
                            objTraining.OrganizationName = objDataReader["OrganizationName"].ToString();
                        }
                        break;
                    case "TrainingTitle":
                        if (!Convert.IsDBNull(objDataReader["TrainingTitle"]))
                        {
                            objTraining.TrainingTitle = objDataReader["TrainingTitle"].ToString();
                        }
                        break;
                    case "TrainingDuration":
                        if (!Convert.IsDBNull(objDataReader["TrainingDuration"]))
                        {
                            objTraining.TrainingDuration = objDataReader["TrainingDuration"].ToString();
                        }
                        break;

                    case "TrainingYear":
                        if (!Convert.IsDBNull(objDataReader["TrainingYear"]))
                        {
                            //objTraining.TrainingYear = Convert.ToDateTime(objDataReader["TrainingYear"]);
                            objTraining.TrainingYear =  objDataReader["TrainingYear"].ToString();
                        }
                        break;
                    
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objTraining.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objTraining.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objTraining.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objTraining.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objTraining.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objTraining.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objTraining.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objTraining.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<Training> GetAllEmployeeTrainingInfoById(int employeeId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<Training> objTrainingList = new List<Training>();

            Training objTraining = new Training();

            try
            {
                objDbCommand.AddInParameter("EmployeeId", employeeId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllEmployeeTrainingInfoById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTraining = new Training();
                        objTraining.TrainingId = Convert.ToInt16(objDbDataReader["TrainingId"].ToString());
                        objTraining.EmployeeId = Convert.ToInt16(objDbDataReader["EmployeeId"].ToString());
                        objTraining.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objTraining.OrganizationName = objDbDataReader["OrganizationName"].ToString();
                        objTraining.TrainingTitle = objDbDataReader["TrainingTitle"].ToString();
                        objTraining.TrainingDuration = objDbDataReader["TrainingDuration"].ToString();
                        objTraining.TrainingYear = objDbDataReader["TrainingYear"].ToString();                        
                        objTrainingList.Add(objTraining);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objTrainingList;
        }

        public List<Training> GetAllTraining()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Training> objTrainingList = new List<Training>();
            Training objTraining;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetTrainingList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTraining = new Training();
                        this.BuildModelForTraining(objDbDataReader, objTraining);
                        objTrainingList.Add(objTraining);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objTrainingList;
        }

        public string SaveTraining(Training objTraining)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", objTraining.EmployeeId);
            objDbCommand.AddInParameter("OrganizationName", objTraining.OrganizationName);
            objDbCommand.AddInParameter("TrainingTitle", objTraining.TrainingTitle);
            objDbCommand.AddInParameter("TrainingDuration", objTraining.TrainingDuration);
            objDbCommand.AddInParameter("TrainingYear", objTraining.TrainingYear);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateTraining", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public Training GetAllTraining(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<Training> objTrainingList = new List<Training>();

            Training objTraining = new Training();

            try
            {
                objDbCommand.AddInParameter("TrainingId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTrainingById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTraining = new Training();
                        this.BuildModelForTraining(objDbDataReader, objTraining);
                        objTrainingList.Add(objTraining);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objTraining;
        }

        public string UpadateTraining(Training objTraining)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("TrainingId", objTraining.TrainingId);
            objDbCommand.AddInParameter("EmployeeId", objTraining.EmployeeId);
            objDbCommand.AddInParameter("OrganizationName", objTraining.OrganizationName);
            objDbCommand.AddInParameter("TrainingTitle", objTraining.TrainingTitle);
            objDbCommand.AddInParameter("TrainingDuration", objTraining.TrainingDuration);
            objDbCommand.AddInParameter("TrainingYear", objTraining.TrainingYear);
            objDbCommand.AddInParameter("IsActive", objTraining.IsActive);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateTraining", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }


        public string DeleteTraining(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("TrainingId", id);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteTrainingById", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}

