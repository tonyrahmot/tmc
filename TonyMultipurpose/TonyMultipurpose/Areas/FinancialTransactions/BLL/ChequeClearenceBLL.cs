﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class ChequeClearenceBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<FinancialTransactionsModel> GetChequeClearenceDetails()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<FinancialTransactionsModel> objFinancialTransactionsModelList = new List<FinancialTransactionsModel>();
            FinancialTransactionsModel objFinancialTransactionsModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetChequeClearenceDetails]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objFinancialTransactionsModel = new FinancialTransactionsModel();
                        objFinancialTransactionsModel.Id = Convert.ToInt32(objDbDataReader["id"].ToString());
                        //objFinancialTransactionsModel.IsCleared = Convert.ToBoolean(objDbDataReader["IsCleared"].ToString());
                        objFinancialTransactionsModel.ChequeIssueDate =Convert.ToDateTime( objDbDataReader["ChequeIssueDate"].ToString());
                        objFinancialTransactionsModel.VoucherType = objDbDataReader["VoucherType"].ToString();
                        objFinancialTransactionsModel.VoucherCode = objDbDataReader["VoucherCode"].ToString();
                        objFinancialTransactionsModel.Particular = objDbDataReader["Particular"].ToString();
                        objFinancialTransactionsModel.AccountCode = objDbDataReader["AccountCode"].ToString();
                        objFinancialTransactionsModel.Remarks = objDbDataReader["Remarks"].ToString();
                        objFinancialTransactionsModel.ChequeNo = objDbDataReader["ChequeNo"].ToString();
                        objFinancialTransactionsModel.AccountName = objDbDataReader["AccountName"].ToString();
                        objFinancialTransactionsModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objFinancialTransactionsModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        objFinancialTransactionsModel.BankName =objDbDataReader["BankName"].ToString();
                        objFinancialTransactionsModel.BranchName = objDbDataReader["BranchName"].ToString();
                        objFinancialTransactionsModel.BankAccountNo = objDbDataReader["BankAccountNo"].ToString();
                        objFinancialTransactionsModel.Credit =Convert.ToDecimal( objDbDataReader["Credit"].ToString());
                        objFinancialTransactionsModel.Debit = Convert.ToDecimal(objDbDataReader["Debit"].ToString());
                        objFinancialTransactionsModel.UserStatus =objDbDataReader["Username"].ToString();
                        objFinancialTransactionsModel.CreatedBy = Convert.ToByte(objDbDataReader["CreatedBy"].ToString());
                        objFinancialTransactionsModelList.Add(objFinancialTransactionsModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objFinancialTransactionsModelList;
        }

        public string SavingsChequeClearence(string[] ids)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            try
            {
                foreach (var id in ids)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("Id", id);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSavingsChequeClearence]",
                    CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";
        }

        public FinancialTransactionsModel GetChequeClearenceDetailsById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<FinancialTransactionsModel> objFinancialTransactionsModelList = new List<FinancialTransactionsModel>();

            FinancialTransactionsModel objFinancialTransactionsModel = new FinancialTransactionsModel();

            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetChequeClearenceDetailsById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {

                        objFinancialTransactionsModel = new FinancialTransactionsModel();
                        objFinancialTransactionsModel.Id = Convert.ToInt32(objDbDataReader["id"].ToString());
                        objFinancialTransactionsModel.IsCleared = Convert.ToBoolean(objDbDataReader["IsCleared"].ToString());
                        objFinancialTransactionsModel.ChequeIssueDate = Convert.ToDateTime(objDbDataReader["ChequeIssueDate"].ToString());
                        objFinancialTransactionsModel.VoucherType = objDbDataReader["VoucherType"].ToString();
                        objFinancialTransactionsModel.VoucherCode = objDbDataReader["VoucherCode"].ToString();
                        objFinancialTransactionsModel.Particular = objDbDataReader["Particular"].ToString();
                        objFinancialTransactionsModel.AccountCode = objDbDataReader["AccountCode"].ToString();
                        objFinancialTransactionsModel.Remarks = objDbDataReader["Remarks"].ToString();
                        objFinancialTransactionsModel.ChequeNo = objDbDataReader["ChequeNo"].ToString();
                        objFinancialTransactionsModel.AccountName = objDbDataReader["AccountName"].ToString();
                        objFinancialTransactionsModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objFinancialTransactionsModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        objFinancialTransactionsModel.BankName = objDbDataReader["BankName"].ToString();
                        objFinancialTransactionsModel.BranchName = objDbDataReader["BranchName"].ToString();
                        objFinancialTransactionsModel.BankAccountNo = objDbDataReader["BankAccountNo"].ToString();
                        objFinancialTransactionsModel.Credit = Convert.ToDecimal(objDbDataReader["Credit"].ToString());
                        objFinancialTransactionsModel.Debit = Convert.ToDecimal(objDbDataReader["Debit"].ToString());
                        objFinancialTransactionsModel.EntryType = objDbDataReader["EntryType"].ToString();
                        objFinancialTransactionsModel.IsAdvancePayment = Convert.ToBoolean(objDbDataReader["IsAdvancePayment"].ToString());
                        objFinancialTransactionsModel.ClientCode = objDbDataReader["ClientCode"].ToString();
                        objFinancialTransactionsModel.ClientName = objDbDataReader["ClientName"].ToString();
                        objFinancialTransactionsModel.SupplierCode = objDbDataReader["SupplierCode"].ToString();
                        objFinancialTransactionsModel.SupplierName = objDbDataReader["SupplierName"].ToString();
                        objFinancialTransactionsModel.CategoryName = objDbDataReader["CategoryName"].ToString();
                        objFinancialTransactionsModel.GroupName = objDbDataReader["GroupName"].ToString();
                        objFinancialTransactionsModel.IsChequePayment = Convert.ToBoolean(objDbDataReader["IsChequePayment"].ToString());
                        objFinancialTransactionsModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"].ToString());
                        objFinancialTransactionsModel.UserStatus = objDbDataReader["Username"].ToString();
                        objFinancialTransactionsModel.CreatedBy = Convert.ToByte(objDbDataReader["CreatedBy"].ToString());
                        objFinancialTransactionsModelList.Add(objFinancialTransactionsModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }

            return objFinancialTransactionsModel;
        }
    }
}