﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using TonyMultipurpose.Enums;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class JpHtmlHelper
    {
        #region Table, Widget, Modal, Square Box, Panel

        public static HtmlWidgetBox DefaultWidget(this HtmlHelper html, string title, string additionalClassName, string additionalTool, string widgetContentId)
        {
            string classWidgetBox = "<div class=\"widget-box\">";
            string icon = "<i class=\"ace-icon fa fa-chevron-up\"></i>";
            if (!string.IsNullOrEmpty(additionalClassName))
            {
                classWidgetBox = $"<div class=\"widget-box {additionalClassName}\">";
                if (additionalClassName.Contains("collapsed"))
                {
                    icon = "<i class=\"ace-icon fa fa-chevron-down\"></i>";
                }
            }

            string widgetMain = "<div class=\"widget-main\">";
            if (!string.IsNullOrEmpty(widgetContentId))
            {
                widgetMain = $"<div class=\"widget-main\" id=\"{widgetContentId}\">";
            }

            additionalTool = string.IsNullOrWhiteSpace(additionalTool) ? "" : additionalTool + " ";

            string widgetToolbar = "<div class=\"widget-toolbar\">" +
                                   additionalTool +
                                   "<a href=\"#\" data-action=\"collapse\">" +
                                   icon +
                                   "</a>" +
                                   "</div>";

            html.ViewContext.Writer.Write(
                classWidgetBox +
                "<div class=\"widget-header\">" +
                "<h5 class=\"widget-title\">" +
                title +
                "</h5>" +
                widgetToolbar +
                "</div>" +
                "<div class=\"widget-body\">" +
                widgetMain
            );

            return new HtmlWidgetBox(html.ViewContext);
        }

        public static HtmlWidgetBox DefaultWidget(this HtmlHelper html, string title, string additionalClassName, string additionalTool)
        {
            return DefaultWidget(html, title, additionalClassName, additionalTool, null);
        }

        public static HtmlWidgetBox DefaultWidget(this HtmlHelper html, string title, string additionalClassName)
        {
            return DefaultWidget(html, title, additionalClassName, null, null);
        }

        public static HtmlWidgetBox DefaultWidget(this HtmlHelper html, string title)
        {
            return DefaultWidget(html, title, null);
        }

        public static HtmlWidgetBox DefaultModal(this HtmlHelper html,
            string title, string modalId)
        {
            return DefaultModal(html, title, modalId, null);
        }

        public static HtmlWidgetBox DefaultModal(this HtmlHelper html,
            string title, string modalId, string modalClass)
        {
            string modalLabelId = modalId + "Label";
            string modalType = $"<div class=\"modal-dialog {modalClass}\" role=\"document\">";
            html.ViewContext.Writer.Write(
                "<div class=\"modal fade\" id=\"" +
                modalId +
                "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"" +
                modalLabelId +
                "\" data-backdrop=\"static\"> " +
                modalType +
                "<div class=\"modal-content\">" +
                "<div class=\"modal-header\"> " +
                "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> " +
                "<span class=\"modal-title\" id=\"" +
                modalLabelId +
                "\">" +
                title +
                "</span>" +
                "</div> "
            );

            return new HtmlWidgetBox(html.ViewContext);
        }

        public static HtmlPanel DefaultPanel(this HtmlHelper html,
            string heading, string panelClass = "panel-default")
        {
            panelClass = "panel " + panelClass;
            html.ViewContext.Writer.Write(
                "<div class=\"" +
                panelClass +
                "\">" +
                "<div class=\"panel-heading\">" +
                "<strong>" +
                heading +
                "</strong>" +
                "</div>" +
                "<div class=\"panel-body\">"
            );

            return new HtmlPanel(html.ViewContext);
        }

        public static HtmlDefaultBox DefaultBox(this HtmlHelper html,
            string heading, string boxClass = "box-default")
        {
            boxClass = "box " + boxClass + " box-solid";
            html.ViewContext.Writer.Write(
                "<div class=\"" +
                boxClass +
                "\">" +
                "<div class=\"box-header with-border\">" +
                "<h3 class=\"box-title\">" +
                heading +
                "</h3>" +
                "<div class=\"box-tools pull-right\">" +
                "<button type=\"button\" class=\"btn btn-box-tool\" data-widget=\"collapse\"><i class=\"fa fa-minus\"></i></button>" +
                "</div>" +
                "</div>" +
                "<div class=\"box-body\">"
            );

            return new HtmlDefaultBox(html.ViewContext);
        }

        public static HtmlSquareContent DefaultSquareBox(this HtmlHelper html,
            string title, string navigationLink)
        {
            html.ViewContext.Writer.Write(
                "<div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">" +
                "<a href=\"" + navigationLink + "\">" +
                "<div class=\"square-box\">" +
                "<div class=\"square-content\">" +
                "<h2>" + title + "</h2>"
            );

            return new HtmlSquareContent(html.ViewContext);
        }

        public static HtmlColorBoxContent DefaultColorBox(this HtmlHelper html,
            string title = "Empty", string body = "Not Found", string color = "aqua", string navigationLink = "#")
        {
            html.ViewContext.Writer.Write(
                "<div class=\"col-md-4 col-sm-6 col-xs-12\">" +
                "<div class=\"small-box bg-" + color + "\">" +
                "<div class=\"inner\">" +
                "<h3>" + title + "</h3>" +
                "<p>" + body + "</p>" +
                "</div>" +
                "<a href=\"" + navigationLink + "\" class=\"small-box-footer\">"
            );

            return new HtmlColorBoxContent(html.ViewContext);
        }

        public static HtmlDataTable CdaDataTable(this HtmlHelper html,
            string tableId,
            string tableClass,
            string tableTollContainerId,
            string tableHeader,
            MvcHtmlString addLink = null,
            MvcHtmlString searchBox = null)
        {
            var htmlString = string.Empty;
            var clearfixString = string.Empty;
            var tablHeading = string.Empty;

            if (!string.IsNullOrEmpty(tableTollContainerId)
                && addLink != null && searchBox != null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  "<div class=\"row\"> " +
                                  "<div class=\"col-md-5\">" +
                                  searchBox +
                                  "</div>" +
                                  "<div class=\"col-md-2\">" +
                                  addLink +
                                  "</div>" +
                                  "<div class=\"col-md-5\">" +
                                  "<div class=\"pull-right\" id=\"" +
                                  tableTollContainerId +
                                  "\"></div> " +
                                  "</div>" +
                                  "</div>" +
                                  "</div>";
            }
            else if (!string.IsNullOrEmpty(tableTollContainerId)
                     && addLink != null && searchBox == null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  "<div class=\"row\"> " +
                                  "<div class=\"col-sm-6\">" +
                                  addLink +
                                  "</div>" +
                                  "<div class=\"col-sm-6\">" +
                                  "<div class=\"pull-right\" id=\"" +
                                  tableTollContainerId +
                                  "\"></div> " +
                                  "</div>" +
                                  "</div>" +
                                  "</div>";
            }
            else if (!string.IsNullOrEmpty(tableTollContainerId)
                     && addLink == null && searchBox != null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  "<div class=\"row\"> " +
                                  "<div class=\"col-sm-6\">" +
                                  searchBox +
                                  "</div>" +
                                  "<div class=\"col-sm-6\">" +
                                  "<div class=\"pull-right\" id=\"" +
                                  tableTollContainerId +
                                  "\"></div> " +
                                  "</div>" +
                                  "</div>" +
                                  "</div>";
            }
            else if (!string.IsNullOrEmpty(tableTollContainerId)
                     && addLink == null && searchBox == null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  "<div class=\"pull-right\" id=\"" +
                                  tableTollContainerId +
                                  "\"></div> " +
                                  "</div>";
            }
            else if (string.IsNullOrEmpty(tableTollContainerId)
                     && addLink != null && searchBox != null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  "<div class=\"row\"> " +
                                  "<div class=\"col-sm-5\">" +
                                  searchBox +
                                  "</div>" +
                                  "<div class=\"col-sm-2\">" +
                                  addLink +
                                  "</div>" +
                                  "</div>" +
                                  "</div>";
            }
            else if (string.IsNullOrEmpty(tableTollContainerId)
                     && addLink != null && searchBox == null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  addLink +
                                  "</div>";
            }
            else if (string.IsNullOrEmpty(tableTollContainerId)
                     && addLink == null && searchBox != null)
            {
                clearfixString += "<div class=\"clearfix\">" +
                                  searchBox +
                                  "</div>";
            }

            if (!string.IsNullOrEmpty(tableHeader))
            {
                tablHeading += $"<div class=\"table-header\" id=\"{tableId + "Header"}\">";
                tablHeading += tableHeader;
                tablHeading += "</div>";
            }

            htmlString += clearfixString;
            htmlString += tablHeading;
            htmlString += "<div>";
            htmlString += "<table";
            htmlString += string.IsNullOrEmpty(tableId) ? "" : " id=\"" + tableId + "\"";
            htmlString += " class=\"";
            htmlString += string.IsNullOrEmpty(tableClass) ? "" : tableClass + " ";
            htmlString += "table table-striped table-bordered table-hover display nowrap\"";
            htmlString += " style=\"min-width: 100%;\">";

            html.ViewContext.Writer.Write(htmlString);

            return new HtmlDataTable(html.ViewContext);
        }

        #endregion Table, Widget, Modal, Square Box, Panel

        #region Link Related Components

        public static HtmlEmpyEnd LinkHtmlIcon(this HtmlHelper html,
           EnumOperationName operationName,
           string url = "javascript:void(0);",
           string customClassName = "", string dataId = "")
        {
            var tooltipClass = string.Empty;
            var tooltipTitle = string.Empty;
            var iconName = string.Empty;
            var iconColor = string.Empty;

            switch (operationName)
            {
                case EnumOperationName.Edit:
                    tooltipClass = "success";
                    tooltipTitle = "Edit";
                    iconName = "pencil";
                    iconColor = "green";
                    break;

                case EnumOperationName.Details:
                    tooltipClass = "info";
                    tooltipTitle = "See Details";
                    iconName = "search-plus";
                    iconColor = "blue";
                    break;

                case EnumOperationName.Delete:
                    tooltipClass = "error";
                    tooltipTitle = "Edit";
                    iconName = "trash";
                    iconColor = "red";
                    break;
            }

            var className = string.IsNullOrEmpty(customClassName) ? "" : " " + customClassName;
            var id = string.IsNullOrEmpty(dataId) ? "" : "data-id=\"" + dataId + "\"";

            html.ViewContext.Writer.Write(
                $"<a href =\"{url}\" class=\"tooltip-{tooltipClass}{className}\" " +
                $"data-rel=\"tooltip\" data-placement=\"top\" title=\"{tooltipTitle}\" " +
                $"data-original-title=\"{tooltipTitle}\"{id}>" +
                $"<i class=\"ace-icon fa fa-{iconName} bigger-175 {iconColor}\"></i></a>"
            );
            return new HtmlEmpyEnd(html.ViewContext);
        }

        public static MvcHtmlString AddModalLink(this HtmlHelper html)
        {
            return html.BootstrapButton("<i class='fa fa-plus'></i> Add", "btn-success btn-xs",
                new { data_toggle = "modal", data_target = "#createModal" });
        }

        public static MvcHtmlString AddModalByfunctionCall(this HtmlHelper html)
        {
            return html.BootstrapButton("<i class='fa fa-plus'></i> Add", "btn-success btn-xs",
                new { onclick = "loadCreateModal()" });
        }

        #region Menu Related Component

        public static HtmlEmpyEnd JpMenuLinkByAction(this HtmlHelper html, string actionName, string controllerName, string textToShow)
        {
            return JpMenuLinkByAction(html, actionName, controllerName, null, textToShow, null);
        }

        public static HtmlEmpyEnd JpMenuLinkByAction(this HtmlHelper html, string actionName, string controllerName, string areaName, string textToShow)
        {
            return JpMenuLinkByAction(html, actionName, controllerName, areaName, textToShow, null);
        }

        public static HtmlEmpyEnd JpMenuLinkByAction(this HtmlHelper html, string actionName, string controllerName, string areaName, string textToShow, string iconClass)
        {
            if (string.IsNullOrEmpty(textToShow))
            {
                textToShow = "Dashboard";
            }

            if (string.IsNullOrEmpty(actionName))
            {
                actionName = "Index";
            }

            if (string.IsNullOrEmpty(areaName))
            {
                areaName = "";
            }

            if (string.IsNullOrEmpty(controllerName))
            {
                controllerName = "CdaHome";
                textToShow = "Dashboard";
                iconClass = "fa-circle-o";
            }

            if (string.IsNullOrEmpty(iconClass))
            {
                iconClass = "fa-circle-o";
            }

            string liClass = string.Empty;
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;
            var currentController = routeData.Values["controller"];
            var currentAction = routeData.Values["action"];
            var currentArea = routeData.DataTokens["area"];

            if (string.Equals(actionName, currentAction as string,
                    StringComparison.OrdinalIgnoreCase)
                &&
                string.Equals(controllerName, currentController as string,
                    StringComparison.OrdinalIgnoreCase) //)
                            &&
                            string.Equals(areaName, currentArea as string,
                                StringComparison.OrdinalIgnoreCase))

            {
                liClass = "active";
            }

            // Set the context
            var context = new RequestContext(new HttpContextWrapper(HttpContext.Current),
                new RouteData());
            var urlHelper = new UrlHelper(context);

            // Set the url
            var url = urlHelper.Action(actionName, controllerName,
                new RouteValueDictionary(new { area = areaName }));

            //            var virtualPathData = RouteTable.Routes.GetVirtualPath
            //            (
            //                ((MvcHandler)HttpContext.Current.CurrentHandler).RequestContext,
            //                new RouteValueDictionary
            //                (
            //                    new
            //                    {
            //                        controller = controllerName,
            //                        action = actionName
            //                    }
            //                )
            //            );
            //            var url = virtualPathData != null ? virtualPathData.VirtualPath : "#";
            html.ViewContext.Writer.Write(
                    $"<li class=\"{liClass}\"><a href=\"{url}\">" +
                    $"<i class=\"fa {iconClass}\">" +
                    $"</i> {textToShow} " +
                    "</a></li>"
                );

            return new HtmlEmpyEnd(html.ViewContext);
        }

        public static string IsLinkActive(this HtmlHelper helper, string action, string controller)
        {
            var routeData = helper.ViewContext.RouteData.Values;
            var currentController = routeData["controller"];
            var currentAction = routeData["action"];

            if (string.Equals(action, currentAction as string,
                    StringComparison.OrdinalIgnoreCase)
                &&
                string.Equals(controller, currentController as string,
                    StringComparison.OrdinalIgnoreCase))

            {
                return "active";
            }

            return "";
        }

        public static MvcHtmlString MenuLink(
            this HtmlHelper helper,
            string text, string action, string controller)
        {
            var routeData = helper.ViewContext.RouteData.Values;
            var currentController = routeData["controller"];
            var currentAction = routeData["action"];

            if (string.Equals(action, currentAction as string,
                    StringComparison.OrdinalIgnoreCase)
                &&
                string.Equals(controller, currentController as string,
                    StringComparison.OrdinalIgnoreCase))

            {
                return helper.ActionLink(
                    text, action, controller, null,
                    new { @class = $"currentMenuItem" }
                );
            }
            return helper.ActionLink(text, action, controller);
        }

        public static HtmlEmpyEnd CdaMenuLinkByAddress(this HtmlHelper html,
           string navLink,
           string textToShow,
           string liClass)
        {
            return CdaMenuLinkByAddress(html, navLink, textToShow, liClass, null);
        }

        public static HtmlEmpyEnd CdaMenuLinkByAddress(this HtmlHelper html,
           string navLink,
           string textToShow,
           string liClass,
           string iconClass)
        {
            if (string.IsNullOrEmpty(navLink))
            {
                navLink = "#";
            }

            if (string.IsNullOrEmpty(iconClass))
            {
                iconClass = "fa-caret-right";
            }

            html.ViewContext.Writer.Write(
                $"<li class=\"{liClass}\"><a href=\"{navLink}\">" +
                $"<i class=\"menu-icon fa {iconClass}\">" +
                $"</i><span class=\"menu-text\"> {textToShow} " +
                "</span></a><b class=\"arrow\"></b></li>"
            );

            return new HtmlEmpyEnd(html.ViewContext);
        }

        public static HtmlDropDownLi CdaMenuDropDownLi(this HtmlHelper html,
           string textToShow,
           string iconClass)
        {
            html.ViewContext.Writer.Write(
                "<li class=\"dropdown-li\">" +
                "<a href=\"#\" class=\"dropdown-toggle\">" +
                $"<i class=\"menu-icon fa {iconClass}\"></i>" +
                $"<span class=\"menu-text\"> {textToShow} </span>" +
                "<b class=\"arrow fa fa-angle-down\"></b></a>" +
                "<b class=\"arrow\"></b><ul class=\"submenu\">"
            );

            return new HtmlDropDownLi(html.ViewContext);
        }

        #endregion Menu Related Component

        #endregion Link Related Components
    }
}