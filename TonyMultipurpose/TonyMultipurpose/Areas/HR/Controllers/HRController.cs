﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.HR.BLL;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Areas.HR.ViewModels;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Controllers
{
    [AuthenticationFilter]
    public class HRController : Controller
    {
        // GET: HR/HR
        CommonResult _oCommonResult;

        #region Company Designation
        public ActionResult Designation()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveDesignation()
        {

            Designation objDesignation = new Designation();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllDesignationInfo(objDesignation);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveDesignation(Designation objDesignation)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = objHr.SaveDesignationInfo(objDesignation);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateDesignation(Designation objDesignation)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = objHr.UpdateDesignationInfo(objDesignation);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public JsonResult GetDesignationCodeCheck(string designationCode)
        {
            HRBLL objHr = new HRBLL();
            bool exsits = objHr.GetDesignationCodeCheck(designationCode);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetDesignationNameCheck(string designationName)
        {
            HRBLL objHr = new HRBLL();
            bool exsits = objHr.GetDesignationNameCheck(designationName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        #endregion Company Designation

        #region Company Department
        public ActionResult Department()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveDepartment()
        {

            Department objDepartment = new Department();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllDepartmentInfo(objDepartment);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveDepartment(Department objDepartment)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = objHr.SaveDepartmentInfo(objDepartment);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateDepartment(Department objDepartment)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = objHr.UpdateDepartmentInfo(objDepartment);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public JsonResult GetDepartmentCodeCheck(string deptCode)
        {
            HRBLL objHr = new HRBLL();
            bool exsits = objHr.GetDepartmentCodeCheck(deptCode);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetDepartmentNameCheck(string departmentName)
        {
            HRBLL objHr = new HRBLL();
            bool exsits = objHr.GetDepartmentNameCheck(departmentName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        #endregion Company Department

        #region Employee Transfer
        public ActionResult Transferentry()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Save()
        {

            TransferRecordModel objTransferRecord = new TransferRecordModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllTransferRecordInfo(objTransferRecord);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(TransferRecordModel objTransferRecord)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objTransferRecord.IsStatus = false;
                objTransferRecord.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objTransferRecord.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveTranferRecordInfo(objTransferRecord);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult Update(TransferRecordModel objTransferRecord)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objTransferRecord.IsStatus = false;
                objTransferRecord.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objTransferRecord.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateTranferRecordInfo(objTransferRecord);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult TransferApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TransferUnApprovaList()
        {
            TransferRecordModel objTransferRecord = new TransferRecordModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllTransferRecordInfo(objTransferRecord);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TransferRecordApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.TransferRecordApproval(id);
            return RedirectToAction("TransferApproval");
        }
        #endregion Employee Transfer

        #region Employee Info
        public JsonResult GetEmployeeInfoByEmpCode(string empId)
        {
            HRBLL objHr = new HRBLL();
            var result = objHr.GetEmployeeInfoByEmpCode(empId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Employee Info

        #region Auto Load Employee Code 
        public JsonResult AutoCompleteEmployeeCode(string term)
        {
            LeaveInformationBLL objLeaveInformationBll = new LeaveInformationBLL();
            var result = objLeaveInformationBll.GetEmployeeCodeForAutoComplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Auto Load Employee Code 

        #region Branch Info 
        public JsonResult GetBranchInfo()
        {
            BranchBLL objBranchBll = new BranchBLL();
            var result = objBranchBll.GetAllBranchInfo();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Branch Info 

        #region Employee Benefit 
        public ActionResult BenefitEntry()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveBenefit()
        {
            BenefitModel objEmpBenefit = new BenefitModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpBenefitInfo(objEmpBenefit);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveBenefit(BenefitModel objEmpBenefit)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpBenefit.IsApproved = false;
                objEmpBenefit.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpBenefit.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objEmpBenefit.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveEmpBenefitInfo(objEmpBenefit);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateBenefit(BenefitModel objEmpBenefit)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpBenefit.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpBenefit.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateEmpBenefitInfo(objEmpBenefit);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult BenefitApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BenefitUnApprovaList()
        {
            BenefitModel objEmpBenefit = new BenefitModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpBenefitInfo(objEmpBenefit);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmpBenefitApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpBenefitApproval(id);
            return RedirectToAction("BenefitApproval");
        }

        #endregion Employee Benefit 

        #region Employee Advance Adjustment
        public ActionResult AdvanceAdjustmentEntry()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveAdvanceAdjustmentInfo()
        {

            AdvanceAdjustmentModel objEmpAdvanceAdjustment = new AdvanceAdjustmentModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpAdvanceAdjustmentInfo(objEmpAdvanceAdjustment);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAdvanceAdjustmentInfo(AdvanceAdjustmentModel objEmpAdvanceAdjustment)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpAdvanceAdjustment.IsApproved = false;
                objEmpAdvanceAdjustment.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpAdvanceAdjustment.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objEmpAdvanceAdjustment.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveAdvanceAdjustmentInfo(objEmpAdvanceAdjustment);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateEmpAdvanceAdjustmentInfo(AdvanceAdjustmentModel objEmpAdvanceAdjustment)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpAdvanceAdjustment.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpAdvanceAdjustment.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateEmpAdvanceAdjustmentInfo(objEmpAdvanceAdjustment);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult AdvanceAdjustmentApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpAdvanceAdjustmentUnApprovaList()
        {
            AdvanceAdjustmentModel objEmpAdvanceAdjustment = new AdvanceAdjustmentModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpAdvanceAdjustmentInfo(objEmpAdvanceAdjustment);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmpAdvanceAdjustmentApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpAdvanceAdjustmentApproval(id);
            return RedirectToAction("AdvanceAdjustmentApproval");
        }
        #endregion Employee Advance Adjustment 

        #region Employee Mobile Bill
        public ActionResult MobileBillEntry()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveMobileBillInfo()
        {

            MobileBillModel objEmpMobileBill = new MobileBillModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpMobileBillInfo(objEmpMobileBill);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveMobileBillInfo(MobileBillModel objEmpMobileBill)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpMobileBill.IsApproved = false;
                objEmpMobileBill.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpMobileBill.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objEmpMobileBill.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveEmpMobileBillInfo(objEmpMobileBill);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateEmpMobileBillInfo(MobileBillModel objEmpMobileBill)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpMobileBill.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpMobileBill.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateEmpMobileBillInfo(objEmpMobileBill);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult MobileBillApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpMobileBillUnApprovaList()
        {
            MobileBillModel objEmpMobileBill = new MobileBillModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpMobileBillInfo(objEmpMobileBill);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmpMobileBillApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpMobileBillApproval(id);
            return RedirectToAction("MobileBillApproval");
        }

        #endregion Employee Mobile Bill

        #region Salary Information
        public ActionResult SalaryInformation()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmployeeSalaryInfo()
        {

            SalaryModel objSalary = new SalaryModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetEmployeeSalaryInfo(objSalary);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveEmployeeSalaryInformation(int salary,int da, int hra, int convence, string empCode, string date,int cSalary, int grosssalary)
        {
            HRBLL objHr = new HRBLL();
            string result = objHr.SaveEmployeeSalaryInformation(salary, da, hra, convence,empCode,date,cSalary, grosssalary);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeeSalaryInfoByEmpCode(string empId)
        {
            HRBLL objHr = new HRBLL();
            var result = objHr.GetEmployeeeSalaryInfoByEmpCode(empId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateEmployeeSalaryInformation(SalaryModel objEmpSalary)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = objHr.UpdateEmployeeSalaryInformation(objEmpSalary);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult SalaryApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpSalaryUnApprovaList()
        {
            SalaryModel objSalary = new SalaryModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetEmployeeSalaryInfo(objSalary);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmpSalaryApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpSalaryApproval(id);
            return RedirectToAction("SalaryApproval");
        }
        #endregion Salary Information

        #region Employee Promotion
        public ActionResult EmpPromotion()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveEmpPromotion()
        {

            PromotionModel objEmpPromotion = new PromotionModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpPromotionInfo(objEmpPromotion);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEmpPromotion(PromotionModel objEmpPromotion)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpPromotion.IsApproved = false;
                objEmpPromotion.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpPromotion.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objEmpPromotion.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveEmpPromotionInfo(objEmpPromotion);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateEmpPromotion(PromotionModel objEmpPromotion)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpPromotion.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpPromotion.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateEmpPromotionInfo(objEmpPromotion);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult EmpPromotionApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpPromotionUnApprovaList()
        {
            PromotionModel objEmpPromotion = new PromotionModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpPromotionInfo(objEmpPromotion);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PromotionApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpPromotionApproval(id);
            return RedirectToAction("EmpPromotionApproval");
        }
        #endregion Employee Promotion 

        #region Company Holiday
        public ActionResult HoliDay()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveHoliDay()
        {
            HolidayModel objHoliday = new HolidayModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpHolidayInfo(objHoliday);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHoliDay(HolidayModel objHoliday)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objHoliday.IsApproved = false;
                objHoliday.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objHoliday.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objHoliday.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveHolidayInfo(objHoliday);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateHolidayInfo(HolidayModel objHoliday)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objHoliday.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objHoliday.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateHolidayInfo(objHoliday);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult HolidayApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult HolidayUnApprovaList()
        {
            HolidayModel objHoliday = new HolidayModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpHolidayInfo(objHoliday);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HolidayInfoApproval(int id)
        {
            HRBLL objHr = new HRBLL();
            objHr.HolidayApproval(id);
            return RedirectToAction("HolidayApproval");
        }
        #endregion Employee Holiday

        #region Employee Arrier Bill
        public ActionResult EmpArrierBill()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveEmpArrierBill()
        {

            ArrierModel objEmpArrierBill = new ArrierModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpArrierBillInfo(objEmpArrierBill);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveEmpArrierBill(ArrierModel objEmpArrierBill)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpArrierBill.IsApproved = false;
                objEmpArrierBill.IsRejected = false;
                objEmpArrierBill.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpArrierBill.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objEmpArrierBill.CreatedDate = DateTime.Now;
                _oCommonResult = objHr.SaveEmpArrierBillInfo(objEmpArrierBill);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateEmpArrierBill(ArrierModel objEmpArrierBill)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHr = new HRBLL();
            if (ModelState.IsValid)
            {
                objEmpArrierBill.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objEmpArrierBill.UpdatedDate = DateTime.Now;
                _oCommonResult = objHr.UpdateEmpArrierBillInfo(objEmpArrierBill);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        public ActionResult EmpArrierBillApproval()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmpArrierBillUnApprovaList()
        {
            ArrierModel objEmpArrierBill = new ArrierModel();
            HRBLL objHr = new HRBLL();
            var hr = objHr.GetAllEmpArrierBillInfo(objEmpArrierBill);
            return Json(new { data = hr }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ArrierBillApproval(string id)
        {
            HRBLL objHr = new HRBLL();
            objHr.EmpArrierBillApproval(id);
            return RedirectToAction("EmpArrierBillApproval");
        }
        #endregion Employee Arrier Bill

        #region Salary Processes

        [HttpGet]
        public ActionResult SalaryProcesses()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveSalaryProcesses(SalaryProcessesViewModel salaryProcesses)
        {
            _oCommonResult = new CommonResult();
            HRBLL objHrbll = new HRBLL();
            _oCommonResult = objHrbll.SaveSalaryProcesses(salaryProcesses);
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }


        [HttpGet]
        public ActionResult GetAllSalaryProcessInfo()
        {
            SalaryProcessesViewModel salaryProcesses=new SalaryProcessesViewModel();
            HRBLL objHrbll=new HRBLL();
            var allData = objHrbll.GetAllUnApprovedSalaryProcessInfo(salaryProcesses);
            return Json(new { data = allData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProcessDateWiseSalaryProcessInfo(string processDate)
        {
           
            HRBLL objHrbll = new HRBLL();
            var allData = objHrbll.GetProcessDateWiseSalaryProcessInfo(processDate);
            return Json(new { data = allData }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SalaryProcessesApproval()
        {
            SalaryProcessesViewModel salaryProcesses = new SalaryProcessesViewModel();
            
            HRBLL objHrbll = new HRBLL();
            List<SalaryProcessesViewModel> salaryProcessesList = objHrbll.GetAllUnApprovedSalaryProcessInfo(salaryProcesses);
            return View(salaryProcessesList);
        }

        [HttpPost]
        public ActionResult SalaryProcessesApproval(FormCollection form)
        {
            HRBLL objHrbll=new HRBLL();
            var ch = form.GetValues("Id");
            objHrbll.SalaryProcessesApproval(ch);
            return RedirectToAction("SalaryProcessesApproval", "HR");
        }

        #endregion

    }
}