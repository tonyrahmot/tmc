﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Accounts.Controllers
{
    [AuthenticationFilter]
    public class COACategoryController : Controller
    {
        COACategoryBLL objCOACategoryBLL = new COACategoryBLL();
        // GET: Accounts/COACategory
        public ActionResult Index()
        {
            List<COACategory> objCOACategoryList = new List<COACategory>();
            objCOACategoryList = objCOACategoryBLL.GetCOACategoryList();
            return View(objCOACategoryList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            AccountGroupBLL _objAccountGroupBLL = new AccountGroupBLL();
            var model = new COACategory
            {
                AccountGroupInfo = _objAccountGroupBLL.GetAllAccountGroupInfo()
            };
            return View(model);
            //var objCOACategory = new COACategory();
            //return View(objCOACategory);
        }

        [HttpPost]
        public ActionResult Save(COACategory objCOACategory)
        {

            bool status = false;
            if (ModelState.IsValid)
            {
                //objCOACategory.IsDeleted = false;
                //objCOACategory.CreatedBy = 1;
                //objCOACategory.CreatedDate = DateTime.Now;
                objCOACategoryBLL.SaveCOACategoryInfo(objCOACategory);
                status = true;
            }
            else
            {
                return View(objCOACategory);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "COACategory");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            AccountGroupBLL _objAccountGroupBLL = new AccountGroupBLL();         
            var coaCategory = objCOACategoryBLL.GetCOACategoryInfo(id);
            coaCategory.AccountGroupInfo = _objAccountGroupBLL.GetAllAccountGroupInfo();
            return View(coaCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(COACategory objCOACategory)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                //objBranch.UpdatedBy = 1;
                //objBranch.UpdatedDate = DateTime.Now;
                objCOACategoryBLL.UpdateCOACategoryInfo(objCOACategory);
                status = true;
            }
            else
            {
                return View(objCOACategory);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "COACategory");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var coaCategory = objCOACategoryBLL.GetCOACategoryInfo(id);
            return View(coaCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteCOACategory(int id)
        {
            objCOACategoryBLL.DeleteCOACategory(id);
            return RedirectToAction("Index", "COACategory");
        }

        public JsonResult GetCategoryNameCheck(string CategoryName)
        {
            bool exsits = objCOACategoryBLL.GetCategoryNameCheck(CategoryName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
    }
}