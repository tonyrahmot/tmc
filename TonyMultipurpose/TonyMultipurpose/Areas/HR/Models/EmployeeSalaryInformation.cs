﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class EmployeeSalaryInformation
    {
        public int SalaryId { get; set; }

        public string EmpCode { get; set; }

        public decimal? CurrentSalary { get; set; }

        public decimal? UpdatedSalary { get; set; }

        public string EfficientDate { get; set; }

        public string AllowanceCode { get; set; }

        public decimal? AllowanceAmount { get; set; }

        public bool? IsActive { get; set; }
    }
}