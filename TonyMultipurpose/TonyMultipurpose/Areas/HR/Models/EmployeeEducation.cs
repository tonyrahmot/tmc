﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class EmployeeEducation
    {
        public long Id { get; set; }

        public string EmpCode { get; set; }

        public short? PassingYear { get; set; }

        public string Grade { get; set; }

        public string InstituteName { get; set; }

        public string Description { get; set; }

        public string DegreeName { get; set; }
    }
}