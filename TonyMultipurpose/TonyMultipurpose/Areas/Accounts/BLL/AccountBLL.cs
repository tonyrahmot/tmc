﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.BLL
{
    public class AccountBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        #region CRUD
        public CommonResult SaveAccount(Account account)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            oCommonResult.Id = string.Empty;
            oCommonResult.CustomErrorMesg = string.Empty;

            int noOfAffacted = 0;

            string transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);

            try
            {
                if (string.IsNullOrEmpty(account.AccountNumber))
                {
                    account.AccountNumber = GenerateAccountNumber(account);
                }


                //Generate Transaction Id
                if (account.DepositAmount > 0)
                {
                    objDbDataReader = null;
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                    int Id = 0;

                    string year = Convert.ToString(DateTime.Now.Year);
                    string days = Convert.ToString(DateTime.Now.DayOfYear);
                    //var prefix = string.Concat(year + days);
                    string prefix = year + days;

                    objDbCommand.AddInParameter("FieldName", "TransactionId");
                    switch (account.AccountType)
                    {
                        case 2:
                            objDbCommand.AddInParameter("TableName", "Savings");
                            break;
                        case 4:
                            objDbCommand.AddInParameter("TableName", "FixedDeposite");
                            break;
                        case 5:
                            objDbCommand.AddInParameter("TableName", "DoubleBenefit");
                            break;
                        case 6:
                            objDbCommand.AddInParameter("TableName", "MonthlyBenefit");
                            break;
                        case 10:
                            objDbCommand.AddInParameter("TableName", "Css");
                            break;
                        case 11:
                            objDbCommand.AddInParameter("TableName", "Savings");
                            break;
                        default:
                            break;
                    }

                    objDbCommand.AddInParameter("Prefix", prefix);
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                        CommandType.StoredProcedure);
                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            transactionId = prefix;
                            Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                            transactionId += Convert.ToString(Id).PadLeft(4, '0');
                        }
                    }
                    objDbDataReader.Close();


                }

                if (account.AccountType == 2 || account.AccountType == 11)
                {
                    objDbDataReader = null;
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                    objDbCommand.AddInParameter("CustomerId", account.CustomerId);
                    objDbCommand.AddInParameter("AccountTypeId", account.AccountType);
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspCheckSavingAccountExists]",
                        CommandType.StoredProcedure);
                    if (objDbDataReader.HasRows)
                    {
                        oCommonResult.Status = false;
                        oCommonResult.Message = "This customer already have a savings account.";
                        return oCommonResult;
                    }
                    objDbDataReader.Close();
                }






                if (!string.IsNullOrEmpty(account.AccountNumber))
                {
                    //Save Account Info
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                    objDbCommand.AddInParameter("CustomerId", account.CustomerId);
                    objDbCommand.AddInParameter("AccountNumber", account.AccountNumber);
                    objDbCommand.AddInParameter("AccountTypeId", account.AccountType);
                    objDbCommand.AddInParameter("AccountCode", account.AccountCode);
                    objDbCommand.AddInParameter("COAId", account.COAId);

                    objDbCommand.AddInParameter("TransactionId", transactionId);
                    objDbCommand.AddInParameter("InterestRate", account.InterestRate);
                    objDbCommand.AddInParameter("DepositAmount", account.DepositAmount);
                    objDbCommand.AddInParameter("TransactionDate", account.OpeningDate);
                    objDbCommand.AddInParameter("Duration", account.Duration);
                    objDbCommand.AddInParameter("MatureDate", account.MatureDate);
                    objDbCommand.AddInParameter("MatureAmount", account.MatureAmount);
                    objDbCommand.AddInParameter("MonthlyBenefit", account.MonthlyBenefit);
                    objDbCommand.AddInParameter("DurationYear", account.DurationYear);
                    objDbCommand.AddInParameter("DurationMonth", account.DurationMonth);
                    objDbCommand.AddInParameter("InterestAmount", account.InterestAmount);


                    objDbCommand.AddInParameter("SpecialInstructions", account.SpecialInstructions);
                    objDbCommand.AddInParameter("IsApproved", account.IsApproved);
                    objDbCommand.AddInParameter("IsActive", account.IsActive);
                    objDbCommand.AddInParameter("IsDeleted", account.IsDeleted);
                    objDbCommand.AddInParameter("BranchId", account.BranchId);
                    objDbCommand.AddInParameter("CompanyId", account.CompanyId);
                    objDbCommand.AddInParameter("OpeningDate", account.OpeningDate);
                    objDbCommand.AddInParameter("CreatedBy", account.CreatedBy);
                    objDbCommand.AddInParameter("CreatedDate", account.CreatedDate);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspCreateAccount]",
                        CommandType.StoredProcedure);

                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        oCommonResult.Status = true;
                        oCommonResult.Message = "Saved Successfully";
                        oCommonResult.Id = account.AccountNumber;
                        //Save Nominee Info  
                        if (account.Nominees.Count > 0)
                        {
                            noOfAffacted = 0;
                            foreach (Nominee nominee in account.Nominees)
                            {
                                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                                objDbCommand.AddInParameter("CustomerId", account.CustomerId);
                                objDbCommand.AddInParameter("AccountNumber", account.AccountNumber);
                                objDbCommand.AddInParameter("NomineeName", nominee.NomineeName);
                                objDbCommand.AddInParameter("NomineeFatherName", nominee.NomineeFatherName);
                                objDbCommand.AddInParameter("NomineeMotherName", nominee.NomineeMotherName);
                                objDbCommand.AddInParameter("NomineeSpouseName", nominee.NomineeSpouseName);
                                objDbCommand.AddInParameter("NomineeNationality", nominee.NomineeNationality);
                                objDbCommand.AddInParameter("NomineeNationalId", nominee.NomineeNationalId);
                                objDbCommand.AddInParameter("NomineeDateOfBirth", nominee.NomineeDateOfBirth);
                                objDbCommand.AddInParameter("NomineeMobile", nominee.NomineeMobile);
                                objDbCommand.AddInParameter("NomineeOccupation", nominee.NomineeOccupation);
                                objDbCommand.AddInParameter("NomineeDesignation", nominee.NomineeDesignation);
                                objDbCommand.AddInParameter("NomineeAddress", nominee.NomineeAddress);
                                objDbCommand.AddInParameter("NomineeImagePath", nominee.NomineeImage);
                                objDbCommand.AddInParameter("NomineePosition", nominee.NomineePosition);
                                objDbCommand.AddInParameter("Relation", nominee.Relation);

                                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand,
                                    "[dbo].[uspSaveNominee]", CommandType.StoredProcedure);
                                if (noOfAffacted > 0)
                                {
                                    objDbCommand.Transaction.Commit();
                                }
                                else
                                {
                                    objDbCommand.Transaction.Rollback();
                                }
                            }

                            if (noOfAffacted > 0)
                            {
                                oCommonResult.Status = true;
                                oCommonResult.Message = "Saved Successfully";
                            }
                        }
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }
        public string GenerateAccountNumber(Account objAccount)
        {
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var accountTypeCode = string.Empty;

            string accountNumber = string.Empty;
            objDbCommand.AddInParameter("AccountSetupId", objAccount.AccountType);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountTypeCode]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    accountTypeCode = objDbDataReader["AccountTypeCode"].ToString();
                }
            }

            objDbDataReader.Close();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;
            var branchCode = SessionUtility.TMSessionContainer.BranchCode;
            var prefix = string.Concat(branchCode + accountTypeCode);
            //objDbCommand.AddInParameter("FieldName", "AccountNumber");
            //objDbCommand.AddInParameter("TableName", "Accounts");
            //objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdForAccount]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    accountNumber = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    accountNumber += Convert.ToString(Id).PadLeft(6, '0');
                }
            }
            objDbDataReader.Close();
            return accountNumber;
        }

        internal CommonResult UpdateAccount(Account account)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = true;
            oCommonResult.Message = "Updated Successfully";
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();

            try
            {
                //Update Nominee Info
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                objDbCommand.AddInParameter("CustomerId", account.CustomerId);
                objDbCommand.AddInParameter("AccountNumber", account.AccountNumber);
                objDbCommand.AddInParameter("UpdatedBy", account.UpdatedBy);
                objDbCommand.AddInParameter("UpdatedDate", account.UpdatedDate);
                objDbCommand.AddInParameter("CompanyId", account.CompanyId);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteNominees]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();

                    if (account.Nominees.Count > 0)
                    {
                        noOfAffacted = 0;
                        foreach (Nominee nominee in account.Nominees)
                        {
                            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                            objDbCommand.AddInParameter("CustomerId", account.CustomerId);
                            objDbCommand.AddInParameter("AccountNumber", account.AccountNumber);
                            objDbCommand.AddInParameter("NomineeName", nominee.NomineeName);
                            objDbCommand.AddInParameter("NomineeFatherName", nominee.NomineeFatherName);
                            objDbCommand.AddInParameter("NomineeMotherName", nominee.NomineeMotherName);
                            objDbCommand.AddInParameter("NomineeSpouseName", nominee.NomineeSpouseName);
                            objDbCommand.AddInParameter("NomineeNationality", nominee.NomineeNationality);
                            objDbCommand.AddInParameter("NomineeNationalId", nominee.NomineeNationalId);
                            objDbCommand.AddInParameter("NomineeDateOfBirth", nominee.NomineeDateOfBirth);
                            objDbCommand.AddInParameter("NomineeMobile", nominee.NomineeMobile);
                            objDbCommand.AddInParameter("NomineeOccupation", nominee.NomineeOccupation);
                            objDbCommand.AddInParameter("NomineeDesignation", nominee.NomineeDesignation);
                            objDbCommand.AddInParameter("NomineeAddress", nominee.NomineeAddress);
                            objDbCommand.AddInParameter("NomineeImagePath", nominee.NomineeImage);
                            objDbCommand.AddInParameter("NomineePosition", nominee.NomineePosition);
                            objDbCommand.AddInParameter("Relation", nominee.Relation);

                            noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand,
                                "[dbo].[uspSaveNominee]", CommandType.StoredProcedure);
                            if (noOfAffacted > 0)
                            {
                                objDbCommand.Transaction.Commit();
                            }
                            else
                            {
                                oCommonResult.Status = false;
                                oCommonResult.Message = "Unsuccessful";
                                objDbCommand.Transaction.Rollback();
                            }
                        }

                        if (noOfAffacted > 0)
                        {
                            oCommonResult.Status = true;
                            oCommonResult.Message = "Updated Successfully";
                        }
                    }
                }
                else
                {
                    oCommonResult.Status = false;
                    oCommonResult.Message = "Failed";
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        internal string DeleteAccount(string id)
        {
            int noRowCount = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("@AccountNumber", id);
            objDbCommand.AddInParameter("@IsDeleted", true);
            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteAccount]", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        internal List<AccountsViewModel> GetAllAccounts(Account objAccount)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("CompanyId", objAccount.CompanyId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllAccount]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountsViewModel = new AccountsViewModel();
                        this.BuildModelForAccountViewModel(objDbDataReader, objAccountsViewModel);

                        accountViewModels.Add(objAccountsViewModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return accountViewModels;
        }
        internal Account GetAccountInfoById(string id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            Account objAccount = new Account();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountInfoById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        this.BuildModelForAccount(objDbDataReader, objAccount);
                    }
                }

                if (objAccount.CustomerId != null)
                {
                    //Get Nominees
                    objDbDataReader.Close();
                    objDbDataReader = null;
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                    Nominee objNominee;
                    objDbCommand.AddInParameter("AccountNumber", id);
                    objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetNomineeInfoByAccountId]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)
                    {
                        while (objDbDataReader.Read())
                        {
                            objNominee = new Nominee();
                            this.BuildModelForNominee(objDbDataReader, objNominee);
                            objAccount.Nominees.Add(objNominee);
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objAccount;
        }

        private void BuildModelForAccountViewModel(DbDataReader objDataReader, AccountsViewModel objAccountsViewModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objAccountsViewModel.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "AccountType":
                        if (!Convert.IsDBNull(objDataReader["AccountType"]))
                        {
                            objAccountsViewModel.AccountType = objDataReader["AccountType"].ToString();
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objAccountsViewModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objAccountsViewModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "CustomerImage":
                        if (!Convert.IsDBNull(objDataReader["CustomerImage"]))
                        {
                            var imagePath = objDataReader["CustomerImage"].ToString();
                            objAccountsViewModel.OldImage = imagePath;
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objAccountsViewModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                //objCustomerModel.CustomerImage = "\\Not Found\\";
                                objAccountsViewModel.CustomerImage = imagePath;
                            }

                        }
                        else
                        {
                            objAccountsViewModel.CustomerImage = "\\Not Found\\";
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objAccountsViewModel.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "DepositAmount":
                        if (!Convert.IsDBNull(objDataReader["DepositAmount"]))
                        {
                            objAccountsViewModel.DepositAmount = Convert.ToDecimal(objDataReader["DepositAmount"].ToString());
                        }
                        break;
                    case "AccountBalance":
                        if (!Convert.IsDBNull(objDataReader["AccountBalance"]))
                        {
                            objAccountsViewModel.AccountBalance = Convert.ToDecimal(objDataReader["AccountBalance"].ToString());
                        }
                        break;

                    case "OpeningDate":
                        if (!Convert.IsDBNull(objDataReader["OpeningDate"]))
                        {
                            objAccountsViewModel.OpeningDate = objDataReader["OpeningDate"].ToString();
                        }
                        break;
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDataReader["TransactionId"]))
                        {
                            objAccountsViewModel.TransactionId = objDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDataReader["TransactionDate"]))
                        {
                            objAccountsViewModel.TransactionDate = Convert.ToDateTime(objDataReader["TransactionDate"].ToString());
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDataReader["TransactionType"]))
                        {
                            objAccountsViewModel.TransactionType = objDataReader["TransactionType"].ToString();
                        }
                        break;

                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objAccountsViewModel.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"].ToString());
                        }
                        break;


                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objAccountsViewModel.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "Mobile":
                        if (!Convert.IsDBNull(objDataReader["Mobile"]))
                        {
                            objAccountsViewModel.Mobile = objDataReader["Mobile"].ToString();
                        }
                        break;
                    case "MatureAmount":
                        if (!Convert.IsDBNull(objDataReader["MatureAmount"]))
                        {
                            objAccountsViewModel.MatureAmount = Convert.ToDecimal(objDataReader["MatureAmount"].ToString());
                        }
                        break;
                    case "MatureDate":
                        if (!Convert.IsDBNull(objDataReader["MatureDate"]))
                        {
                            objAccountsViewModel.MatureDate = Convert.ToDateTime(objDataReader["MatureDate"].ToString());
                        }
                        break;
                    case "InstallmentNumber":
                        if (!Convert.IsDBNull(objDataReader["InstallmentNumber"]))
                        {
                            objAccountsViewModel.InstallmentNumber = Convert.ToInt32(objDataReader["InstallmentNumber"].ToString());
                        }
                        break;
                    case "InstallmentAmount":
                        if (!Convert.IsDBNull(objDataReader["InstallmentAmount"]))
                        {
                            objAccountsViewModel.InstallmentAmount = Convert.ToDecimal(objDataReader["InstallmentAmount"].ToString());
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDataReader["Amount"]))
                        {
                            objAccountsViewModel.Amount = Convert.ToDecimal(objDataReader["Amount"].ToString());
                        }
                        break;
                    case "Fine":
                        if (!Convert.IsDBNull(objDataReader["Fine"]))
                        {
                            objAccountsViewModel.Fine = Convert.ToDecimal(objDataReader["Fine"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objAccountsViewModel.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    case "InstallmentMontWithYear":
                        if (!Convert.IsDBNull(objDataReader["InstallmentMontWithYear"]))
                        {
                            //objAccountsViewModel.Particular = objDataReader["InstallmentMontWithYear"].ToString();
                        }
                        break;
                    case "AccountTypeId":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeId"]))
                        {
                            objAccountsViewModel.AccountTypeId =Convert.ToInt32( objDataReader["AccountTypeId"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objAccountsViewModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void BuildModelForAccount(DbDataReader objDataReader, Account objAccount)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objAccount.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;

                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objAccount.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;

                    case "AccountTitleId":
                        if (!Convert.IsDBNull(objDataReader["AccountTitleId"]))
                        {
                            objAccount.AccountTitle = objDataReader["AccountTitleId"].ToString();
                        }
                        break;
                    case "AccountTypeId":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeId"]))
                        {
                            objAccount.AccountType = Convert.ToInt16(objDataReader["AccountTypeId"].ToString());
                        }
                        break;
                    case "AccountCode":
                        if (!Convert.IsDBNull(objDataReader["AccountCode"]))
                        {
                            objAccount.AccountCode = objDataReader["AccountCode"].ToString();
                        }
                        break;
                    case "AccountName":
                        if (!Convert.IsDBNull(objDataReader["AccountName"]))
                        {
                            objAccount.AccountName = objDataReader["AccountName"].ToString();
                        }
                        break;

                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objAccount.BranchId = Convert.ToInt16(objDataReader["BranchId"].ToString());
                        }
                        break;
                    case "SpecialInstructions":
                        if (!Convert.IsDBNull(objDataReader["SpecialInstructions"]))
                        {
                            objAccount.SpecialInstructions = objDataReader["SpecialInstructions"].ToString();
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objAccount.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "DepositAmount":
                        if (!Convert.IsDBNull(objDataReader["DepositAmount"]))
                        {
                            objAccount.DepositAmount = Convert.ToDecimal(objDataReader["DepositAmount"].ToString());
                        }
                        break;
                    case "Duration":
                        if (!Convert.IsDBNull(objDataReader["Duration"]))
                        {
                            objAccount.Duration = objDataReader["Duration"].ToString();
                        }
                        break;
                    case "MatureDate":
                        if (!Convert.IsDBNull(objDataReader["MatureDate"]))
                        {
                            objAccount.MatureDate = Convert.ToDateTime(objDataReader["MatureDate"].ToString());
                        }
                        break;
                    case "OpeningDate":
                        if (!Convert.IsDBNull(objDataReader["OpeningDate"]))
                        {
                            objAccount.OpeningDate = Convert.ToDateTime(objDataReader["OpeningDate"].ToString());
                        }
                        break;
                    case "COA":
                        if (!Convert.IsDBNull(objDataReader["COA"]))
                        {
                            objAccount.COAcode = objDataReader["COA"].ToString();
                        }
                        break;
                    case "MonthlyBenefit":
                        if (!Convert.IsDBNull(objDataReader["MonthlyBenefit"]))
                        {
                            objAccount.MonthlyBenefit = Convert.ToDecimal(objDataReader["MonthlyBenefit"].ToString());
                        }
                        break;
                    case "MatureAmount":
                        if (!Convert.IsDBNull(objDataReader["MatureAmount"]))
                        {
                            objAccount.MatureAmount = Convert.ToDecimal(objDataReader["MatureAmount"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void BuildModelForNominee(DbDataReader objDataReader, Nominee objNominee)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objNominee.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;

                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objNominee.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;

                    case "NomineeName":
                        if (!Convert.IsDBNull(objDataReader["NomineeName"]))
                        {
                            objNominee.NomineeName = objDataReader["NomineeName"].ToString();
                        }
                        break;
                    case "NomineeFatherName":
                        if (!Convert.IsDBNull(objDataReader["NomineeFatherName"]))
                        {
                            objNominee.NomineeFatherName = objDataReader["NomineeFatherName"].ToString();
                        }
                        break;
                    case "NomineeMotherName":
                        if (!Convert.IsDBNull(objDataReader["NomineeMotherName"]))
                        {
                            objNominee.NomineeMotherName = objDataReader["NomineeMotherName"].ToString();
                        }
                        break;
                    case "NomineeSpouseName":
                        if (!Convert.IsDBNull(objDataReader["NomineeSpouseName"]))
                        {
                            objNominee.NomineeSpouseName = objDataReader["NomineeSpouseName"].ToString();
                        }
                        break;
                    case "NomineeNationality":
                        if (!Convert.IsDBNull(objDataReader["NomineeNationality"]))
                        {
                            objNominee.NomineeNationality = objDataReader["NomineeNationality"].ToString();
                        }
                        break;

                    case "NomineeNationalId":
                        if (!Convert.IsDBNull(objDataReader["NomineeNationalId"]))
                        {
                            objNominee.NomineeNationalId = objDataReader["NomineeNationalId"].ToString();
                        }
                        break;

                    case "NomineeDateOfBirth":
                        if (!Convert.IsDBNull(objDataReader["NomineeDateOfBirth"]))
                        {
                            objNominee.NomineeDateOfBirth = Convert.ToDateTime(objDataReader["NomineeDateOfBirth"].ToString());
                        }
                        break;
                    case "NomineeMobile":
                        if (!Convert.IsDBNull(objDataReader["NomineeMobile"]))
                        {
                            objNominee.NomineeMobile = objDataReader["NomineeMobile"].ToString();
                        }
                        break;
                    case "NomineeOccupation":
                        if (!Convert.IsDBNull(objDataReader["NomineeOccupation"]))
                        {
                            objNominee.NomineeOccupation = objDataReader["NomineeOccupation"].ToString();
                        }
                        break;
                    case "NomineeDesignation":
                        if (!Convert.IsDBNull(objDataReader["NomineeDesignation"]))
                        {
                            objNominee.NomineeDesignation = objDataReader["NomineeDesignation"].ToString();
                        }
                        break;
                    case "NomineeAddress":
                        if (!Convert.IsDBNull(objDataReader["NomineeAddress"]))
                        {
                            objNominee.NomineeAddress = objDataReader["NomineeAddress"].ToString();
                        }
                        break;
                    case "NomineeImagePath":
                        if (!Convert.IsDBNull(objDataReader["NomineeImagePath"]))
                        {
                            objNominee.NomineeImage = objDataReader["NomineeImagePath"].ToString();
                        }
                        break;
                    case "NomineePosition":
                        if (!Convert.IsDBNull(objDataReader["NomineePosition"]))
                        {
                            objNominee.NomineePosition = objDataReader["NomineePosition"].ToString();
                        }
                        break;
                    case "Relation":
                        if (!Convert.IsDBNull(objDataReader["Relation"]))
                        {
                            objNominee.Relation = objDataReader["Relation"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Get associate Info
        public List<AccountTypeSetup> GetAccontTypeByAccountTitle()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetupList = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup = new AccountTypeSetup();
            try
            {
                objDbCommand.AddInParameter("AccountTitle", "Deposit");
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountypeByTitle]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountTypeSetup = new AccountTypeSetup();
                        if (objDbDataReader["AccountSetupId"] != null)
                        {
                            objAccountTypeSetup.AccountSetupId = Convert.ToInt16(objDbDataReader["AccountSetupId"]);
                            objAccountTypeSetup.AccountTypeName = Convert.ToString(objDbDataReader["AccountTypeName"]);
                            objAccountTypeSetupList.Add(objAccountTypeSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountTypeSetupList;
        }

        public List<string> GetAllCustomerIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objCustomerList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("CustomerId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Autocomplete_ApprovedCustomerId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["CustomerId"] != null)
                        {
                            objCustomerList.Add(objDbDataReader["CustomerId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCustomerList;
        }

        public List<string> GetAccountNumbersForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> accountNumbers = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbCommand.AddInParameter("IsApproved", 1);
                objDbCommand.AddInParameter("IsActive", 1);
                objDbCommand.AddInParameter("IsDeleted", 0);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountNumbersForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            accountNumbers.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return accountNumbers;
        }

        #endregion

        /// <summary>
        /// get interest rate by account type
        /// </summary>
        /// ataur
        /// <returns></returns>
        public InterestRateEntry GetInterestRateByAccountType(string accountType)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            InterestRateEntry objInterestRateEntry = new InterestRateEntry();
            try
            {
                objDbCommand.AddInParameter("AccountSetupId", accountType);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetInterestRateByAccountType]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objInterestRateEntry = new InterestRateEntry();
                        if (objDbDataReader["AccountSetupId"] != null)
                        {
                            objInterestRateEntry.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objInterestRateEntry;
        }

        public List<Account> GetAllAccountNumber()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Account> objAccountList = new List<Account>();
            Account objAccount;

            try
            {
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAccountNumberList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccount = new Account();
                        this.BuildModelForAccount(objDbDataReader, objAccount);
                        objAccountList.Add(objAccount);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountList;
        }



        /// <summary>
        /// get 
        /// </summary>
        /// <returns></returns>
        public List<COA> Get_COAaccountCodeInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objCoaList = new List<COA>();
            COA objCoa;

            try
            {
                //objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].Get_COAaccountCodeInfo", CommandType.StoredProcedure);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTransactionalCoaData]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCoa = new COA();
                        objCoa.COAId = Convert.ToInt32(objDbDataReader["COAId"].ToString());
                        //objCoa.AccountName = objDbDataReader["AccountName"].ToString();
                        objCoa.AccountCode = objDbDataReader["AccountCode"].ToString();
                        objCoaList.Add(objCoa);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCoaList;
        }

        // All Accounts Transactions date 03/08/2017

        public List<AccountsViewModel> GetAllTransactions(AccountsViewModel objAccountViewModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> objAllTransactions = new List<AccountsViewModel>();
            AccountsViewModel objAccountModel;
            try
            {
                objDbCommand.AddInParameter("IsApproved", objAccountViewModel.IsApproved);
                objDbCommand.AddInParameter("CompanyId", objAccountViewModel.CompanyId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllUnApproveTransactionInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountModel = new AccountsViewModel();
                        //this.BuildModelForAccountViewModel(objDbDataReader, objAccountModel);
                        objAccountModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objAccountModel.TransactionType = objDbDataReader["TransactionType"].ToString();
                        objAccountModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        objAccountModel.InstallmentNumber = Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                        objAccountModel.InstallmentMonth = objDbDataReader["InstallmentMontWithYear"].ToString();
                        objAccountModel.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                        objAccountModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objAccountModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objAccountModel.Mobile = objDbDataReader["Mobile"].ToString();
                        objAccountModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objAccountModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                        objAccountModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                        objAccountModel.AccountSetupId = Convert.ToInt32(objDbDataReader["AccountTypeId"].ToString());
                        objAccountModel.AccountType = objDbDataReader["AccountTypeName"].ToString();
                        objAccountModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        objAccountModel.Fine = Convert.ToDecimal(objDbDataReader["Fine"].ToString());
                        objAccountModel.Username = objDbDataReader["Username"].ToString();
                        objAllTransactions.Add(objAccountModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objAllTransactions;
        }


        public List<AccountsViewModel> GetUnapproveTransactionNumber()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> objAllTransactions = new List<AccountsViewModel>();
            AccountsViewModel objAccountModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetUnapproveTransactionNumber]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountModel = new AccountsViewModel();
                        objAccountModel.Productname = objDbDataReader["Productname"].ToString();
                        objAccountModel.TotalUnapproved = Convert.ToInt32(objDbDataReader["TotalUnapproved"].ToString());

                        objAllTransactions.Add(objAccountModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objAllTransactions;
        }

        public string ApproveAllTransaction(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                objDbCommand.AddInParameter("Id", id.Split('_')[0]);
                objDbCommand.AddInParameter("AccountTypeId", id.Split('_')[1]);
                objDbCommand.AddInParameter("IsApproved", true);

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[uspApproveAllTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";

        }

        public List<AccountCountViewModel> GetAllLiveClosedAccountInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountCountViewModel> listOfAccountsCount = new List<AccountCountViewModel>();
            AccountCountViewModel accountCountModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[GetLiveClosedAccountInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        accountCountModel = new AccountCountViewModel();
                        accountCountModel.LiveSavingsWI = Convert.ToInt32(objDbDataReader["LiveSavingsWI"].ToString());
                        accountCountModel.ClosedSavingsWI = Convert.ToInt32(objDbDataReader["ClosedSavingsWI"].ToString());
                        accountCountModel.LiveSavingsWithoutI = Convert.ToInt32(objDbDataReader["LiveSavingsWithoutI"].ToString());
                        accountCountModel.ClosedSavingsWithoutI = Convert.ToInt32(objDbDataReader["ClosedSavingsWithoutI"].ToString());
                        accountCountModel.LiveFD = Convert.ToInt32(objDbDataReader["LiveFD"].ToString());
                        accountCountModel.ClosedFD = Convert.ToInt32(objDbDataReader["ClosedFD"].ToString());
                        accountCountModel.LiveDB = Convert.ToInt32(objDbDataReader["LiveDB"].ToString());
                        accountCountModel.ClosedDB = Convert.ToInt32(objDbDataReader["ClosedDB"].ToString());
                        accountCountModel.LiveMB = Convert.ToInt32(objDbDataReader["LiveMB"].ToString());
                        accountCountModel.ClosedMB = Convert.ToInt32(objDbDataReader["ClosedMB"].ToString());
                        accountCountModel.LiveCSS = Convert.ToInt32(objDbDataReader["LiveCSS"].ToString());
                        accountCountModel.ClosedCSS = Convert.ToInt32(objDbDataReader["ClosedCSS"].ToString());
                        listOfAccountsCount.Add(accountCountModel);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return listOfAccountsCount;
        }

        #region Create Pdf for account detils

        public DataTable SavingWithInterastPDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetAllAccountDetailForPDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        public DataTable FixedDepositPDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {                
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetFixedDepositPDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        public DataTable DoubleBenifitPDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetDoubleBenifitPDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        public DataTable ContributorySavingsSchemePDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetContributorySavingsSchemePDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        public DataTable MonthlyBenefitDepositSchemePDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetMonthlyBenefitDepositSchemePDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        public DataTable SavingswithoutInterestPDF(Account objAccount)
        {
            var dt = new DataTable();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountsViewModel> accountViewModels = new List<AccountsViewModel>();
            AccountsViewModel objAccountsViewModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", objAccount.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", objAccount.IsDeleted);
                objDbCommand.AddInParameter("AccountNumber", objAccount.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[uspGetSavingswithoutInterestPDF]", CommandType.StoredProcedure);

                var imagePath = dt.Rows[0]["CustomerImage"].ToString();
                if (imagePath.Contains("Uploads"))
                {
                    imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    imagePath = "/" + imagePath;
                    objAccount.CustomerImage = imagePath;
                }
                else
                {
                    objAccount.CustomerImage = "\\Not Found\\";
                }

                var firstNomineeimagePath = dt.Rows[0]["FirstNomineeImagePath"].ToString();
                if (firstNomineeimagePath.Contains("Uploads"))
                {
                    firstNomineeimagePath = firstNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    firstNomineeimagePath = "/" + firstNomineeimagePath;
                    objAccount.FirstNomineeImage = firstNomineeimagePath;
                }
                else
                {
                    objAccount.FirstNomineeImage = "\\Not Found\\";
                }
                var secondNomineeimagePath = dt.Rows[0]["SecnondNomineeImagePath"].ToString();
                if (secondNomineeimagePath.Contains("Uploads"))
                {
                    secondNomineeimagePath = secondNomineeimagePath.Substring(imagePath.IndexOf("Uploads"));
                    secondNomineeimagePath = "/" + secondNomineeimagePath;
                    objAccount.SecondNomineeImage = secondNomineeimagePath;
                }
                else
                {
                    objAccount.SecondNomineeImage = "\\Not Found\\";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        #endregion
    }
}