﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.TermLoan.BLL;
using TonyMultipurpose.Areas.TermLoan.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.TermLoan.Controllers
{
    public class LoanInstallmentController : Controller
    {
        TermLoanBLL objTermLoanBLL = new TermLoanBLL();
        // GET: TermLoan/LoanInstallment
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SaveInstallmentInformation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveInstallmentInformation(InstallmentInformation objSaveInstallmentInformation)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objSaveInstallmentInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objSaveInstallmentInformation.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objSaveInstallmentInformation.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objSaveInstallmentInformation.CreatedDate = DateTime.Now;
                objTermLoanBLL.SaveInstallmentInformation(objSaveInstallmentInformation);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult GetInstallmentInformation()
        {
            var info = objTermLoanBLL.GetInstallmentInformation().ToList();
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteTermLoanId(string term)
        {
            var result = objTermLoanBLL.GetAllTermLoanIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTermLoanInfoByLoanId(string LoanId)
        {
            var data = objTermLoanBLL.GetTermLoanInfoByLoanId(LoanId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}