$(function () {


    $('#UpdateGuarantor').hide();
    $('#deleteGuarantor').hide();
    // Load Function function
    var oGuarantorTable;
    LoadGuarantorTable();
    function LoadGuarantorTable() {
        oGuarantorTable = $('#GuarantorTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllGurantorInfo",
                "type": "get",
                "data": {
                    "LoanId": $('#Guarantor_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "GuarantorRoll" },
                { "data": "GuarantorName" },
                //{ "data": "NID" },
                //{ "data": "FatherName" },
                //{ "data": "MotherName" },
                //{ "data": "Occupation" },
                //{ "data": "Designation" },
                //{ "data": "InstituteName" },
                //{ "data": "InstituteAddress" },
                //{ "data": "CurrentAddress" },
                //{ "data": "PermanentAddress" },
                { "data": "ContactNo" },
            ],
            "order": [[0, "desc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#GuarantorTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oGuarantorTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataGuarantor(oGuarantorTable.row(this).data());
        });
    }
    function bindDataGuarantor(data) {
        $('#hdfGuarantorId').val(data.GuarantorId);
        $('#img-upload').attr('src', data.ImagePath);
        document.getElementById('Guarantor_GuarantorName').innerHTML = data.GuarantorName;
        document.getElementById('Guarantor_NID').innerHTML = data.NID;
        document.getElementById('Guarantor_FatherName').innerHTML = data.FatherName;
        document.getElementById('Guarantor_MotherName').innerHTML = data.MotherName;
        document.getElementById('Guarantor_Occupation').innerHTML = data.Occupation;
        document.getElementById('Guarantor_Designation').innerHTML = data.Designation;
        document.getElementById('Guarantor_InstituteName').innerHTML = data.InstituteName;
        document.getElementById('Guarantor_InstituteAddress').innerHTML = data.InstituteAddress;
        document.getElementById('Guarantor_CurrentAddress').innerHTML = data.CurrentAddress;
        document.getElementById('Guarantor_PermanentAddress').innerHTML = data.PermanentAddress;
        document.getElementById('Guarantor_ContactNo').innerHTML = data.ContactNo;
        document.getElementById('Guarantor_GuarantorRoll').innerHTML = data.GuarantorRoll;
        document.getElementById('Guarantor_SpouseName').innerHTML = data.SpouseName;
        document.getElementById('Guarantor_RelationWithApplicant').innerHTML = data.RelationWithApplicant;
        document.getElementById('Guarantor_DateofBirth').innerHTML = data.DateofBirth;
        document.getElementById('Guarantor_MonthlyIncome').innerHTML = data.MonthlyIncome;
        document.getElementById('Guarantor_BusinessAddress').innerHTML = data.BusinessAddress;
        document.getElementById('Guarantor_LoanStatusWithTmc').innerHTML = data.LoanStatusWithTmc;
        document.getElementById('GResidentialCondition').innerHTML = data.ResidentialConditionType;


        $('#saveGuarantor').hide();
        $('#UpdateGuarantor').show();
        $('#deleteGuarantor').show();
    }
    // caler function
    $('#Guarantor_Clear').click(function () {
        GuarantorClearAll();
    });
    function GuarantorClearAll() {
        //$('#Guarantor_LoanId').val('');
        $('#ImageFile').attr('src', '');
        $('#Guarantor_GuarantorName').val('');
        $('#Guarantor_NID').val('');
        $('#Guarantor_FatherName').val('');
        $('#Guarantor_MotherName').val('');
        $('#Guarantor_Occupation').val('');
        $('#Guarantor_Designation').val('');
        $('#Guarantor_InstituteName').val('');
        $('#Guarantor_InstituteAddress').val('');
        $('#Guarantor_CurrentAddress').val('');
        $('#Guarantor_PermanentAddress').val('');
        $('#Guarantor_ContactNo').val('');
        $('#Guarantor_GuarantorRoll').val('');
        $('#Guarantor_SpouseName').val('');
        $('#Guarantor_RelationWithApplicant').val('');
        $('#Guarantor_DateofBirth').val('');
        $('#Guarantor_MonthlyIncome').val('');
        $('#Guarantor_BusinessAddress').val('');
        $('#Guarantor_LoanStatusWithTmc').val('');
        $('#GResidentialCondition').val(0);
        $('#saveGuarantor').show();
        $('#deleteGuarantor').hide();
        $('#UpdateGuarantor').hide();
    }
});