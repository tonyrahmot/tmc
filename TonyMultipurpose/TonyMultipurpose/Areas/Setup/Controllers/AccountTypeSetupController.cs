﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    [AuthenticationFilter]
    //--ataur
    public class AccountTypeSetupController : Controller
    {
        // GET: Setup/AccountTypeSetup
        public ActionResult Index()
        {
            AccountTypeSetupBLL objAccountTypeSetupBll=new AccountTypeSetupBLL();
            List<AccountTypeSetup> accountTypeSetupList = objAccountTypeSetupBll.GetAllAccountTypeSetupList();
            return View(accountTypeSetupList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(AccountTypeSetup objAccountTypeSetup)
        {
            AccountTypeSetupBLL objAccountTypeSetupBll=new AccountTypeSetupBLL();
            if (ModelState.IsValid)
            {
                objAccountTypeSetupBll.CreateAccountTypeSetup(objAccountTypeSetup);
            }

            return RedirectToAction("Index", "AccountTypeSetup");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            AccountTypeSetupBLL objAccountTypeSetupBll=new AccountTypeSetupBLL();
            var accountTypeInfo = objAccountTypeSetupBll.GetAccountTypeSetupInfoById(id);

            return View(accountTypeInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountTypeSetup objAccountTypeSetup)
        {
            AccountTypeSetupBLL objAccountTypeSetupBll = new AccountTypeSetupBLL();
            if (ModelState.IsValid)
            {
                objAccountTypeSetupBll.UpdateAccountTypeSetup(objAccountTypeSetup);
            }

            return RedirectToAction("Index", "AccountTypeSetup");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            AccountTypeSetupBLL objAccountTypeSetupBll = new AccountTypeSetupBLL();
            var accountTypeInfo = objAccountTypeSetupBll.GetAccountTypeSetupInfoById(id);

            return View(accountTypeInfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteAccountType(int id)
        {
            AccountTypeSetupBLL objAccountTypeSetupBll = new AccountTypeSetupBLL();
            if (ModelState.IsValid)
            {
                objAccountTypeSetupBll.DeleteAccountTypeSetup(id);
            }

            return RedirectToAction("Index", "AccountTypeSetup");
        }
    }
}