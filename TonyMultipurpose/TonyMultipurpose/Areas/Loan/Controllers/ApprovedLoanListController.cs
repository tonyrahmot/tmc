﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Customer.BLL;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.ReportsViewer;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
    [AuthenticationFilter]
    public class ApprovedLoanListController : Controller
    {
        LoanModel objLoan;
        LoanBLL objLoanBLL;
        AdditionalLoanDetail objAdditionalLoanDetail;
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetApprovedLoanList()
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = true;
            objLoan.IsRejected = false;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == true && a.IsApproved == true);
            return Json(new { data = loans }, JsonRequestBehavior.AllowGet);
        }

        #region 
        public ActionResult GetLoanDetail(string loanId)
        {
            ViewBag.LoanId = loanId;
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = true;
            objLoan.IsRejected = false;
            var loan = objLoanBLL.GetAllLoanInfo(objLoan).FirstOrDefault(a => a.IsChecked && a.IsApproved && a.LoanId == loanId);
            CustomerBLL objCustomerBll = new CustomerBLL();
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = true;
            customer.IsDeleted = false;
            ViewBag.CustomerInfo = objCustomerBll.GetAllCustomerInfo(customer).FirstOrDefault(a => a.CustomerId == loan.CustomerId);
            return View(loan);
        }

        #endregion

        #region Guarantor
        [HttpGet]
        public ActionResult Guarantor(string loanId)
        {
            Guarantor objGuarantor = new Guarantor();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }
        [HttpGet]
        public JsonResult GetAllGurantorInfo(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<Guarantor> Guarantor = objLoanBLL.GetAllGurantorInfo(loanId);
            return Json(new { data = Guarantor }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Comments & Signatory

        [HttpGet]
        public ActionResult CommentsAndSignatory(string loanId)
        {
            AddtionalInfoModel addtionalInfoModel = new AddtionalInfoModel();
            addtionalInfoModel.LoanId = loanId;
            return View(addtionalInfoModel);
        }

        [HttpGet]
        public JsonResult GetScanDocumnetInfo(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            return Json(new { data = scanDocuments }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSignatoryDataForDataTable(string loanId)
        {
            objLoanBLL = new LoanBLL();
            //List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            List<SignatoryModel> signatory = objLoanBLL.GetSignatoryDataForDataTable(loanId);
            return Json(new { data = signatory }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult CustomersChequeInfo(string loanId)
        {
            LoanCustomerChequeInfo obj = new LoanCustomerChequeInfo();
            obj.LoanId = loanId;
            return View(obj);
        }

        public JsonResult GetAllLoanCustomerChequeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<LoanCustomerChequeInfo> info = objLoanBLL.GetAllLoanCustomerChequeInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult FinancialJustification(string loanId)
        {
            MonthlyIncome objGuarantor = new MonthlyIncome();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }

        [HttpGet]
        public JsonResult GetAdditionalLoanInfoById(string LoanId)
        {
            objLoanBLL = new LoanBLL();
            var result = objLoanBLL.GetAdditionalLoanInfoById(LoanId).ToList();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllBankperformanceInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<BankPerformance> info = objLoanBLL.GetAllBankperformanceInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllMonthlyIncomeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyIncome> MonthlyIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId);
            //var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).ToList().Sum(x => x.Amount);
            return Json(new { data = MonthlyIncome }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllMonthlyExpenseInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyExpense> MonthlyExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId);
            return Json(new { data = MonthlyExpense }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Finance(string loanId)
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = true;
            objLoan.IsRejected = false;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked);
            var info = loans.FirstOrDefault(a => a.LoanId == loanId);
            var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).Select(a => a.TotalIncome).FirstOrDefault();
            var netExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId).Select(a => a.TotalExpense).FirstOrDefault();
            var loanHistoryInfo = objLoanBLL.GetLoanInfoHistory().FirstOrDefault(a => a.LoanId == loanId);
            var surplusIncome = (netIncome - netExpense);
            return new JsonResult { Data = new {info = info , loanHistoryInfo = loanHistoryInfo , surplusIncome =surplusIncome} };
        }

        #endregion

        #region LoanApplication detils
        public void PrintLoanApplication(string loanId)
        {
            ReportObject.ReportName = "LoanApplication";
            ReportObject.LoanApplication = loanId;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }

        public void PrintLoanApproval(string loanNo)
        {
            ReportObject.ReportName = "LoanApprovalReport";
            ReportObject.LoanNo = loanNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }
        #endregion
    }
}