﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtenstionsValidationMessage
    {


        public static MvcHtmlString BootstrapValidationMessageFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // Add all other attributes below here


            rvd.Add("class", "text-danger ");

            return htmlHelper.ValidationMessageFor(expression, "", rvd);
        }


        public static MvcHtmlString BootstrapValidationMessageFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          string message,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // Add all other attributes below here


            rvd.Add("class", "text-danger ");

            return htmlHelper.ValidationMessageFor(expression, message, rvd);
        }


        public static MvcHtmlString BootstrapValidationMessageFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          string message,
          string cssClass,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // Add all other attributes below here

            if (string.IsNullOrWhiteSpace(cssClass))
            {
                cssClass = "text-danger ";
            }
            else
            {
                cssClass = "text-danger " + cssClass;
            }
            rvd.Add("class", cssClass);

            return htmlHelper.ValidationMessageFor(expression, message, rvd);
        }
    }
}