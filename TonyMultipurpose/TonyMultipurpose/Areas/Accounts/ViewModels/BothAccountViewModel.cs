﻿using System.Collections.Generic;

namespace TonyMultipurpose.Areas.Accounts.ViewModels
{
    public class BothAccountViewModel
    {
        public IEnumerable<AccountsViewModel> ActiveAccounts { get; set; }
        public IEnumerable<AccountsViewModel> ClosedAccounts { get; set; }
        public IEnumerable<AccountCountViewModel> AccountCountViewModels { get; set; }
    }
}