﻿
$(function () {
    $("#AccountType").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Reports/GetAccontTypeByAccountTitle",//'@Url.Action("GetAccontTypeByAccountTitle", "Reports")',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#AccountType").append('<option value="0">--Select Type--</option>');
            $.each(data, function (key, value) {
                $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
            });
        }
    });


    $("#CustomerId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Accounts/Account/AutoCompleteCustomerId",//'@Url.Action("AutoCompleteCustomerId", "Account",new { Area = "Accounts"})',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });


    $("#CustomerId").change(function () {
        var CustomerId = $("#CustomerId").val();
        var json = {
            CustomerId: CustomerId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Reports/GetAccountNoByCustomerId",//'@Url.Action("GetAccountNoByCustomerId", "Reports")',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $("#AccountNumber").append('<option value="0">--Select A/C No--</option>');
                $.each(data, function (key, value) {
                    $("#AccountNumber").append('<option value=' + value.AccountNumber + '>' + value.AccountNumber + '</option>');
                });

            }
        });
    });


    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Reports/AutoCompleteAccountNumber",//'@Url.Action("AutoCompleteAccountNumber", "Reports")',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#AccounCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Reports/AutoCompleteAccountCode",//'@Url.Action("AutoCompleteAccountCode", "Reports")',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#LoanId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Reports/AutoCompleteLoanId",//'@Url.Action("AutoCompleteLoanId", "Reports")',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });
    $("#LoanNo").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Reports/AutoCompleteLoanId",//'@Url.Action("AutoCompleteLoanId", "Reports")',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    ////account number auto complete for account closing
    //$("#AccountNumberForClosing").autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: "/Reports/AutoCompleteAccountNumberForAccountClosing",
    //            type: "GET",
    //            dataType: "json",
    //            data: { term: request.term },
    //            success: function (data) {
    //                response($.map(data, function (item) {
    //                    return { label: item, value: item };
    //                }));
    //            }
    //        });
    //    },
    //    messages: {
    //        noResults: "",
    //        results: ""
    //    }
    //});
    $(function () {
        $('#FromDate').datepicker({
            dateFormat: 'dd/M/yy', changeMonth: true,
            changeYear: true,
            yearRange: "-80:+80"
        }).click(function () { $(this).focus(); });
        $('#ToDate').datepicker({
            dateFormat: 'dd/M/yy', changeMonth: true,
            changeYear: true,
            yearRange: "-80:+80"
        }).click(function () { $(this).focus(); });
    });

    $('input[type=date]').each(function () {
        this.type = "text";
    });

    $('#ReportType').change(function () {
        $("#ReportName").val('');
        var reportType = $('#ReportType').val();
        if (reportType === 'Deposit') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").show();
            $("select option[value*='DateWiseAllTransactionReport']").show();
            $("select option[value*='DateWiseDepositReport']").show();
            $("select option[value*='AccountNumberWiseReport']").show();
            $("select option[value*='DepositStatement']").show();
            $("select option[value*='CustomerwiseTransactionReport']").show();
            $("select option[value*='IrregularInstallmentReport']").show();
            $("select option[value*='NextMonthMaturedAccountReport']").show();
            $("select option[value*='AccountOpeningReport']").show();
            $("select option[value*='ReverseEntryReport']").show();
            $("select option[value*='AccountClosingReport']").show();
            $("select option[value*='DateWiseWithdrawReport']").show();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").hide();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").hide();
            $("select option[value*='LoanAccountWiseInstallmentReport']").hide();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").hide();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").hide();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").hide();
            $("select option[value*='PeriodicDateWiseTrialBalance']").hide();
            $("select option[value*='ExpenseStatementReport']").hide();
            $("select option[value*='IncomeStatementReport']").hide();
            $("select option[value*='ProfitAndLostStatementReport']").hide();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").hide();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").hide();
            $("select option[value*='EmployeeAttendanceReport']").hide();
            $("select option[value*='SalaryProcessReport']").hide();
            $("select option[value*='MonthlyPayslipReport']").hide();
            $("select option[value*='LoanApprovalReport']").hide();
            $("select option[value*='ShareHoldingPosition']").hide();
            $("select option[value*='UserListReport']").hide();


            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
            $("#ReportName").change(function () {
                var reportSubType = $("#ReportName").val();
                //                alert(reportSubType);
                if (reportSubType === "AccountNumberWiseReport") {
                    $("#Date").show();
                } else if (reportSubType === "DepositStatement") {
                    $("#divBranch").show();
                }
            });

        } else if (reportType === 'Loan') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").hide();
            $("select option[value*='DateWiseAllTransactionReport']").hide();
            $("select option[value*='DateWiseDepositReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").show();
            $("select option[value*='LoanApprovalReport']").show();
            $("select option[value*='LoanWiseInstallmentReport']").show();
            $("select option[value*='AccountNumberWiseReport']").hide();
            $("select option[value*='DepositStatement']").hide();
            $("select option[value*='CustomerwiseTransactionReport']").hide();
            $("select option[value*='IrregularInstallmentReport']").hide();
            $("select option[value*='NextMonthMaturedAccountReport']").hide();
            $("select option[value*='AccountOpeningReport']").hide();
            $("select option[value*='ReverseEntryReport']").hide();
            $("select option[value*='AccountClosingReport']").hide();
            $("select option[value*='DateWiseWithdrawReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").show();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").show();
            $("select option[value*='LoanAccountWiseInstallmentReport']").show();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").show();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").show();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").hide();
            $("select option[value*='PeriodicDateWiseTrialBalance']").hide();
            $("select option[value*='ExpenseStatementReport']").hide();
            $("select option[value*='IncomeStatementReport']").hide();
            $("select option[value*='ProfitAndLostStatementReport']").hide();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").hide();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").hide();
            $("select option[value*='EmployeeAttendanceReport']").hide();
            $("select option[value*='SalaryProcessReport']").hide();
            $("select option[value*='MonthlyPayslipReport']").hide();
            $("select option[value*='ShareHoldingPosition']").hide();
            $("select option[value*='UserListReport']").hide();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportType === 'Accounts') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").hide();
            $("select option[value*='DateWiseAllTransactionReport']").hide();
            $("select option[value*='DateWiseDepositReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").show();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='AccountNumberWiseReport']").hide();
            $("select option[value*='DepositStatement']").hide();
            $("select option[value*='CustomerwiseTransactionReport']").hide();
            $("select option[value*='IrregularInstallmentReport']").hide();
            $("select option[value*='NextMonthMaturedAccountReport']").hide();
            $("select option[value*='AccountOpeningReport']").hide();
            $("select option[value*='ReverseEntryReport']").hide();
            $("select option[value*='AccountClosingReport']").hide();
            $("select option[value*='DateWiseWithdrawReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").hide();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").hide();
            $("select option[value*='LoanAccountWiseInstallmentReport']").hide();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").hide();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").hide();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").show();
            $("select option[value*='PeriodicDateWiseTrialBalance']").show();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").show();
            $("select option[value*='ExpenseStatementReport']").show();
            $("select option[value*='IncomeStatementReport']").show();
            $("select option[value*='ProfitAndLostStatementReport']").show();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").show();
            $("select option[value*='SalaryProcessReport']").hide();
            $("select option[value*='MonthlyPayslipReport']").hide();
            $("select option[value*='LoanApprovalReport']").hide();
            $("select option[value*='EmployeeAttendanceReport']").hide();
            $("select option[value*='ShareHoldingPosition']").hide();
            $("select option[value*='UserListReport']").hide();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportType === 'HR') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").hide();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").hide();
            $("select option[value*='EmployeeAttendanceReport']").show();
            $("select option[value*='SalaryProcessReport']").show();
            $("select option[value*='MonthlyPayslipReport']").show();
            $("select option[value*='DateWiseAllTransactionReport']").hide();
            $("select option[value*='DateWiseDepositReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='AccountNumberWiseReport']").hide();
            $("select option[value*='DepositStatement']").hide();
            $("select option[value*='CustomerwiseTransactionReport']").hide();
            $("select option[value*='IrregularInstallmentReport']").hide();
            $("select option[value*='NextMonthMaturedAccountReport']").hide();
            $("select option[value*='AccountOpeningReport']").hide();
            $("select option[value*='ReverseEntryReport']").hide();
            $("select option[value*='AccountClosingReport']").hide();
            $("select option[value*='DateWiseWithdrawReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").hide();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").hide();
            $("select option[value*='LoanAccountWiseInstallmentReport']").hide();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").hide();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").hide();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").hide();
            $("select option[value*='PeriodicDateWiseTrialBalance']").hide();
            $("select option[value*='ExpenseStatementReport']").hide();
            $("select option[value*='IncomeStatementReport']").hide();
            $("select option[value*='ProfitAndLostStatementReport']").hide();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").hide();
            $("select option[value*='LoanApprovalReport']").hide();
            $("select option[value*='ShareHoldingPosition']").hide();
            $("select option[value*='UserListReport']").hide();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportType === 'System') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").hide();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").hide();
            $("select option[value*='EmployeeAttendanceReport']").hide();
            $("select option[value*='SalaryProcessReport']").hide();
            $("select option[value*='MonthlyPayslipReport']").hide();
            $("select option[value*='DateWiseAllTransactionReport']").hide();
            $("select option[value*='DateWiseDepositReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='AccountNumberWiseReport']").hide();
            $("select option[value*='DepositStatement']").hide();
            $("select option[value*='CustomerwiseTransactionReport']").hide();
            $("select option[value*='IrregularInstallmentReport']").hide();
            $("select option[value*='NextMonthMaturedAccountReport']").hide();
            $("select option[value*='AccountOpeningReport']").hide();
            $("select option[value*='ReverseEntryReport']").hide();
            $("select option[value*='AccountClosingReport']").hide();
            $("select option[value*='DateWiseWithdrawReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").hide();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").hide();
            $("select option[value*='LoanAccountWiseInstallmentReport']").hide();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").hide();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").hide();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").hide();
            $("select option[value*='PeriodicDateWiseTrialBalance']").hide();
            $("select option[value*='ExpenseStatementReport']").hide();
            $("select option[value*='IncomeStatementReport']").hide();
            $("select option[value*='ProfitAndLostStatementReport']").hide();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").hide();
            $("select option[value*='LoanApprovalReport']").hide();
            $("select option[value*='ShareHoldingPosition']").hide();
            $("select option[value*='UserListReport']").show();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
        }
        else if (reportType === 'Share') {
            $("#divReportName").show();
            $("select option[value*='DetailListReport']").hide();
            $("select option[value*='PeriodicDateWiseBalanceSheet']").hide();
            $("select option[value*='EmployeeAttendanceReport']").hide();
            $("select option[value*='SalaryProcessReport']").hide();
            $("select option[value*='MonthlyPayslipReport']").hide();
            $("select option[value*='DateWiseAllTransactionReport']").hide();
            $("select option[value*='DateWiseDepositReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='LoanWiseInstallmentReport']").hide();
            $("select option[value*='AccountNumberWiseReport']").hide();
            $("select option[value*='DepositStatement']").hide();
            $("select option[value*='CustomerwiseTransactionReport']").hide();
            $("select option[value*='IrregularInstallmentReport']").hide();
            $("select option[value*='NextMonthMaturedAccountReport']").hide();
            $("select option[value*='AccountOpeningReport']").hide();
            $("select option[value*='ReverseEntryReport']").hide();
            $("select option[value*='AccountClosingReport']").hide();
            $("select option[value*='DateWiseWithdrawReport']").hide();
            $("select option[value*='DateWisePaidInstallmentReport']").hide();
            $("select option[value*='DateWiseUnPaidInstallmentReport']").hide();
            $("select option[value*='LoanAccountWiseInstallmentReport']").hide();
            $("select option[value*='ConLoanTransactionLoanIdWiseReport']").hide();
            $("select option[value*='ContinuousLoanLimitAmountWiseReport']").hide();
            $("select option[value*='PeriodicDateWiseDetailsLedeger']").hide();
            $("select option[value*='PeriodicDateWiseTrialBalance']").hide();
            $("select option[value*='ExpenseStatementReport']").hide();
            $("select option[value*='IncomeStatementReport']").hide();
            $("select option[value*='ProfitAndLostStatementReport']").hide();
            $("select option[value*='PeriodicTrialBalanceDetailsReport']").hide();
            $("select option[value*='LoanApprovalReport']").hide();
            $("select option[value*='ShareHoldingPosition']").show();
            $("select option[value*='UserListReport']").hide();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
        } else {
            $("#divReportName").hide();
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
    });


    $("#ReportName").change(function () {
        var reportName = $("#ReportName").val();
        $('#CustomerId').val('');
        $('#AccountNumber').val('');
        $('#LoanId').val('');
        $('#AccountType').val('0');
        $('#FromDate').val('');
        $('#ToDate').val('');
        $('#Amount').val('');
        $('#BranchName').val('');
        $('#Status').val('');
        $('#AccounCode').val('');
        $('#LoanNo').val('');
        $('.to').show();
        document.getElementById('lblDateFrom').innerHTML = "Date";

        //if (reportName == 'CustomerEntryReport') {
        //    $("#divCustomerId").show();
        //    $("#divAccountNo").hide();
        //    $("#divAccountTypeName").hide();
        //    $("#divBranch").hide();
        //    $("#Date").hide();
        //    $("#divAmount").hide();
        //    $("#divStatus").hide();
        //    $('#divLoanId').hide();
        //    $('#divAccounCode').hide();
        //} else
        if (reportName === 'DepositStatement') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").show();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'CustomerwiseTransactionReport') {
            $("#divCustomerId").show();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'DateWiseAllTransactionReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").show();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'DateWiseWithdrawReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").show();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'DateWiseDepositReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").show();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'IrregularInstallmentReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").show();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } //NextMonthMaturedAccountReport

        else if (reportName === 'NextMonthMaturedAccountReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#Date").show();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'AccountOpeningReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").show();
            $("#divAccountTypeName").hide();
            $("#Date").hide();
            $("#divBranch").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'AccountClosingReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").show();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'ReverseEntryReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").show();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'AccountNumberWiseReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").show();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'LoanWiseInstallmentReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').show();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'DateWisePaidInstallmentReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").show();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').show();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'DateWiseUnPaidInstallmentReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").show();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').show();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'LoanAccountWiseInstallmentReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').show();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'ConLoanTransactionLoanIdWiseReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').show();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'PeriodicDateWiseDetailsLedeger') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').show();

        }
        else if (reportName === 'PeriodicTrialBalanceDetailsReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }

        else if (reportName === 'PeriodicDateWiseTrialBalance') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'ExpenseStatementReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'IncomeStatementReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
            $('.to').hide();
            document.getElementById('lblDateFrom').innerHTML = "Date";
        } else if (reportName === 'ProfitAndLostStatementReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        } else if (reportName === 'EmployeeAttendanceReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'PeriodicDateWiseBalanceSheet') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'SalaryProcessReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'MonthlyPayslipReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'LoanApprovalReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').show();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'DetailListReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
            $('.to').hide();
            document.getElementById('lblDateFrom').innerHTML = "Date";
        }
        else if (reportName === 'ShareHoldingPosition') {
            $("#divCustomerId").show();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").show();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
        else if (reportName === 'UserListReport') {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").show();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
            $('.to').hide();
        }

        else {
            $("#divCustomerId").hide();
            $("#divAccountNo").hide();
            $("#divAccountTypeName").hide();
            $("#divBranch").hide();
            $("#Date").hide();
            $("#divAmount").hide();
            $("#divStatus").hide();
            $('#divLoanId').hide();
            $('#divLoanNo').hide();
            $('#divAccounCode').hide();
        }
    });

    $('#submit').click(function () {
        var isAllDataValid = true;

        if ($('#ReportType').val().trim() === '') {
            isAllDataValid = false;
            $('#ReportType').parent().next().find('span').css('visibility', 'visible');
            return isAllDataValid;
        } else {
            $('#ReportType').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === '') {
            isAllDataValid = false;
            $('#ReportName').parent().next().find('span').css('visibility', 'visible');
            return isAllDataValid;
        } else {
            $('#ReportName').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'AccountNumberWiseReport') {
            if ($('#AccountNumber').val().trim() === '') {
                isAllDataValid = false;
                $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'AccountOpeningReport') {
            if ($('#AccountNumber').val().trim() === '') {
                isAllDataValid = false;
                $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'IncomeStatementReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#DateFrom').parent().next().find('span').css('visibility', 'hidden');
        }


        if ($('#ReportName').val().trim() === 'DateWiseWithdrawReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'DepositStatement') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'DateWiseAllTransactionReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'PeriodicDateWiseBalanceSheet') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }


        if ($('#ReportName').val().trim() === 'PeriodicDateWiseDetailsLedeger') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }




        if ($('#ReportName').val().trim() === 'DateWiseDepositReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'LoanWiseInstallmentReport') {
            if ($('#LoanId').val().trim() === '') {
                isAllDataValid = false;
                $('#LoanId').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
        }


        if ($('#ReportName').val().trim() === 'ConLoanTransactionLoanIdWiseReport') {
            if ($('#LoanId').val().trim() === '') {
                isAllDataValid = false;
                $('#LoanId').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
        }


        if ($('#ReportName').val().trim() === 'LoanAccountWiseInstallmentReport') {
            if ($('#LoanId').val().trim() === '') {
                isAllDataValid = false;
                $('#LoanId').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#LoanId').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'LoanApprovalReport') {
            if ($('#LoanNo').val().trim() === '') {
                isAllDataValid = false;
                $('#LoanNo').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#LoanNo').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#LoanNo').parent().next().find('span').css('visibility', 'hidden');
        }

        //if ($('#AccountNumber').val().trim() === '') {
        //    isAllDataValid = false;
        //    $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
        //    return isAllDataValid;
        //}
        //else {
        //    $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
        //}


        if ($('#ReportName').val().trim() === 'PeriodicDateWiseTrialBalance') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }


        if ($('#ReportName').val().trim() === 'PeriodicTrialBalanceDetailsReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'ReverseEntryReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#ReportName').val().trim() === 'AccountNumberWiseReport') {
            if ($('#FromDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
            if ($('#ToDate').val().trim() === '') {
                isAllDataValid = false;
                $('#ToDate').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#ToDate').parent().next().find('span').css('visibility', 'hidden');
        }
        if ($('#ReportName').val().trim() === 'ShareHoldingPosition') {
            if ($('#CustomerId').val().trim() === '') {
                isAllDataValid = false;
                $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
                return isAllDataValid;
            }
            else {
                $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
            }
        }
        else {
            $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
        }
        if ($('#ReportName').val().trim() === 'UserListReport') {
           
                return isAllDataValid;
        }
        else {
            $('#LoanNo').parent().next().find('span').css('visibility', 'hidden');
        }

    });
});
function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}
