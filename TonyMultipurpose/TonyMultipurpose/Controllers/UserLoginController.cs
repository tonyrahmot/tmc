﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.BLL;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Controllers
{
    [AllowAnonymous]
    public class UserLoginController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            BranchBLL objBranchBll = new BranchBLL();
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(UserLogin userLogin, string returnUrl)
        {
            UserLoginBLL objUserLoginBll = new UserLoginBLL();

            
            if (ModelState.IsValid)
            {
                if (userLogin.BranchId == null)
                {

                    var adminInfo = objUserLoginBll.IsLoginAdmin(userLogin);
                    if (adminInfo != null)
                    {
                        if (adminInfo.r == 2 && adminInfo.BranchId == 2)
                        {
                            userLogin.BranchId = 2;
                            var loginInfo = objUserLoginBll.IsValid(userLogin);

                            if (loginInfo != null)
                            {
                                SessionUtility.TMSessionContainer.UserID = loginInfo.UserDetailId;
                                SessionUtility.TMSessionContainer.UserName = loginInfo.Username;
                                SessionUtility.TMSessionContainer.BranchId = loginInfo.BranchId;
                                SessionUtility.TMSessionContainer.BranchName = loginInfo.BranchName;
                                SessionUtility.TMSessionContainer.BranchCode = loginInfo.BranchCode;
                                SessionUtility.TMSessionContainer.CompanyId = loginInfo.CompanyId;
                                SessionUtility.TMSessionContainer.CompanyName = loginInfo.CompanyName;
                                SessionUtility.TMSessionContainer.UserRole = loginInfo.UserRole;
                                FormsAuthentication.SetAuthCookie(loginInfo.Username, true);

                                return RedirectToLocal(returnUrl);
                            }
                            else
                            {
                                ModelState.AddModelError("", "Login data is incorrect!");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Login data is incorrect!");
                        }
                    }
                    
                    else
                    {
                        ModelState.AddModelError("", "Login data is incorrect!");
                    }
                   
                }
                else
                {
                    var loginInfo = objUserLoginBll.IsValid(userLogin);

                    if (loginInfo != null)
                    {
                        SessionUtility.TMSessionContainer.UserID = loginInfo.UserDetailId;
                        SessionUtility.TMSessionContainer.UserName = loginInfo.Username;
                        SessionUtility.TMSessionContainer.BranchId = loginInfo.BranchId;
                        SessionUtility.TMSessionContainer.BranchName = loginInfo.BranchName;
                        SessionUtility.TMSessionContainer.BranchCode = loginInfo.BranchCode;
                        SessionUtility.TMSessionContainer.CompanyId = loginInfo.CompanyId;
                        SessionUtility.TMSessionContainer.CompanyName = loginInfo.CompanyName;
                        SessionUtility.TMSessionContainer.UserRole = loginInfo.UserRole;
                        FormsAuthentication.SetAuthCookie(loginInfo.Username, true);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("", "Login data is incorrect!");
                    }
                }



               
            }
            BranchBLL objBranchBll = new BranchBLL();
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();
            return View("Login");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            //if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            //{
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();
            //}
            return Redirect("~/UserLogin/Login");
        }
    }
}