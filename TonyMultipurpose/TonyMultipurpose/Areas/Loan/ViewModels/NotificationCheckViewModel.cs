﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class NotificationCheckViewModel : CommonModel
    {
        [Key]
        public long NotificationCheckId { get; set; }
        public string LoanId { get; set; }
        public string Notification { get; set; }
        public bool Status { get; set; }
        public string Msg { get; set; }
    }
}