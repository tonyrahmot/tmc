﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlDataTable : IDisposable
    {
        private readonly ViewContext _viewContext;
        public HtmlDataTable(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }
        public void Dispose()
        {
            _viewContext.Writer.Write(
            "</table>" +
            "</div>"
            );
        }
    }
}