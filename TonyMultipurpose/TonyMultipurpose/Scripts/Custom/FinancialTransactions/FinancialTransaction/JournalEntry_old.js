﻿
(function () {
    $('#btnUpdate').hide();
    $('#btnDelete').hide();
    var transactions = [];
    var transactionTable;
    var rowNo = 0;

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    LoadAccountCode();
    $('.cheque').hide();

    $('#CheckEntry').change(function () {
        if (this.checked) {
            $('.cheque').show();
        }
        else {
            $('.cheque').hide();
        }
    });

    var debit = $('#Debit');
    var credit = $('#Credit');
    $('#EntryType').change(function () {
        debit.val('');
        credit.val('');
        var entryType = $('#EntryType').val();
        if (entryType == "Debit") {
            debit.removeAttr('readonly');
            credit.attr('readonly', 'readonly');
        }
        else if (entryType == "Credit") {
            credit.removeAttr('readonly');
            debit.attr('readonly', 'readonly');
        }
        else {
            debit.attr('readonly', 'readonly');
            credit.attr('readonly', 'readonly');
        }
    });

    function LoadAccountCode() {
        $("#AccountCode").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAllAccountCode",//'@Url.Action("GetAllAccountCode", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#AccountCode").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#AccountCode").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    }

    $('#AccountCode').change(function () {
        //if (checkDuplicateAccountCode()) {
        var coaId = $("#AccountCode").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAccountCodeInfo",//'@Url.Action("GetAccountCodeInfo", "FinancialTransaction", new { Area = "FinancialTransactions" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#AccountName').val(data.AccountName);
                $('#BankName').val(data.BankName);
                $('#BranchName').val(data.BranchName);
                $('#BankAccountNo').val(data.BankAccountNo);
                $('#hdnAccountCode').val(data.AccountCode);
                $('#hdnGroupId').val(data.GroupId);
                $('#hdnGroupName').val(data.GroupName);
                $('#hdnCategoryId').val(data.COACategoryId);
                $('#hdnCategoryName').val(data.CategoryName);
                $('#hdnClientCode').val(data.ClientCode);
                $('#hdnClientName').val(data.ClientName);
                $('#hdnSupplierCode').val(data.SupplierCode);
                $('#hdnSupplierName').val(data.SupplierName);
            }
        });
        //}
    });

    function CheckTransactionFieldsValidation() {
        var isValid = true;
        if ($('#EntryType').val() === '') {
            isValid = false;
            $('#EntryType').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EntryType').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AccountCode').val() === '0') {
            isValid = false;
            $('#AccountCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AccountCode').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#Particular').val() === '') {
            isValid = false;
            $('#Particular').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Particular').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#EntryType').val() === 'Debit') {
            if ($('#Debit').val() === '') {
                isValid = false;
                $('#Debit').parent().prev().find('span').css('visibility', 'visible');
            }
            else if (parseInt($('#Debit').val()) === 0) {
                isValid = false;
                $('#Debit').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#Debit').parent().prev().find('span').css('visibility', 'hidden');
            }
        }

        if ($('#EntryType').val() === 'Credit') {
            if ($('#Credit').val() === '') {
                isValid = false;
                $('#Credit').parent().prev().find('span').css('visibility', 'visible');
            }
            else if (parseInt($('#Credit').val()) === 0) {
                isValid = false;
                $('#Credit').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#Credit').parent().prev().find('span').css('visibility', 'hidden');
            }
        }

        if ($('#CheckEntry').is(":checked")) {
            if ($('#ChequeNo').val() === '') {
                isValid = false;
                $('#ChequeNo').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#ChequeNo').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#IssueDate').val() === '') {
                isValid = false;
                $('#IssueDate').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#IssueDate').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#BankName').val() === '') {
                isValid = false;
                $('#BankName').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#BankName').parent().prev().find('span').css('visibility', 'hidden');
            }
            if ($('#BankAccountNo').val() === '') {
                isValid = false;
                $('#BankAccountNo').parent().prev().find('span').css('visibility', 'visible');
            }
            else {
                $('#BankAccountNo').parent().prev().find('span').css('visibility', 'hidden');
            }
        }
        return isValid;
    }

    function checkDuplicateAccountCode() {
        isUnique = true;
        if (transactions.length > 0) {
            var accountCode = $('#AccountCode').val();
            transactions.forEach(function (item) {
                if (accountCode === item.COAId) {
                    isUnique = false;
                    alert('Duplicate Account Code.')
                }
            });
        }
        return isUnique;
    }
    $('#btnAdd').click(function () {
        var isValidTransaction = CheckTransactionFieldsValidation();

        if (isValidTransaction && checkDuplicateAccountCode()) {
            var isChequePayment = false;
            if ($('#CheckEntry').is(":checked")) {
                isChequePayment = true;
            }
            transactions.push({
                RowNo: ++rowNo,
                EntryType: $("#EntryType option:selected").val(),
                COAId: $('#AccountCode').val(),
                AccountCode: $('#hdnAccountCode').val(),
                AccountName: $('#AccountName').val(),
                Particular: $('#Particular').val(),
                Debit: $('#Debit').val(),
                Credit: $('#Credit').val(),
                Remarks: $('#Remarks').val(),
                IsChequePayment: isChequePayment,
                ChequeNo: $('#ChequeNo').val(),
                IssueDate: $('#IssueDate').val(),
                BankName: $('#BankName').val(),
                BranchName: $('#BranchName').val(),
                BankAccountNo: $('#BankAccountNo').val(),
                GroupId: $('#hdnGroupId').val(),
                GroupName: $('#hdnGroupName').val(),
                CategoryId: $('#hdnCategoryId').val(),
                CategoryName: $('#hdnCategoryName').val(),
                ClientCode: $('#hdnClientCode').val(),
                ClientName: $('#hdnClientName').val(),
                SupplierCode: $('#hdnSupplierCode').val(),
                SupplierName: $('#hdnSupplierName').val()
            });

            if (transactions.length === 1) {
                LoadTrnsactionTable(transactions);
            }
            else {
                transactionTable.destroy();
                LoadTrnsactionTable(transactions);
            }
            clearTransactionFields();
        }
    });

    function LoadTrnsactionTable(transactions) {
        transactionTable = $('#tblTransaction').DataTable({
            data: transactions,
            "columns": [
            { "data": "RowNo" },
            { "data": "EntryType" },
            { "data": "AccountCode" },
            { "data": "AccountName" },
            { "data": "Particular" },
            { "data": "Debit" },
            { "data": "Credit" },
            { "data": "ChequeNo" },
            { "data": "BankName" },
            { "data": "BranchName" },
            { "data": "BankAccountNo" }
            ],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#tblTransaction tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                transactionTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindTransactionData(transactionTable.row(this).data());
        });
    }

    function bindTransactionData(data) {
        $('#hdnRowId').val(data.RowNo);
        $('#EntryType').val(data.EntryType);
        $('#EntryType').change();
        $('#AccountCode').val(data.COAId);
        $('#AccountCode').change();
        $('#Particular').val(data.Particular);
        $('#Debit').val(data.Debit);
        $('#Credit').val(data.Credit);
        $('#Remarks').val(data.Remarks);
        if (data.IsChequePayment == true) {
            $('#CheckEntry').prop('checked', true);
            $('#CheckEntry').change();
            $('#ChequeNo').val(data.ChequeNo);
            $('#IssueDate').val(data.IssueDate);
        }
        $('#btnUpdate').show();
        $('#btnDelete').show();
        $('#btnAdd').hide();
    }

    function clearTransactionFields() {
        $('#EntryType').val('');
        $('#EntryType').change();
        $('#AccountCode').val('0');
        $('#AccountName').val('');
        $('#Particular').val('');
        $('#Debit').val('');
        $('#Credit').val('');
        $('#Remarks').val('');
        $('#CheckEntry').prop('checked', false);
        $('#CheckEntry').change();
        $('#ChequeNo').val('');
        $('#IssueDate').val('');
        $('#BankName').val('');
        $('#BranchName').val('');
        $('#BankAccountNo').val('');
        $('#hdnAccountCode').val('');
        $('#hdnGroupId').val('');
        $('#hdnGroupName').val('');
        $('#hdnCategoryId').val('');
        $('#hdnCategoryName').val('');
        $('#hdnClientCode').val('');
        $('#hdnClientName').val('');
        $('#hdnSupplierCode').val('');
        $('#hdnSupplierName').val('');
        $('#btnUpdate').hide();
        $('#btnDelete').hide();
        $('#btnAdd').show();
    }

    function checkDuplicateAccountCodeForUpdate() {
        isUnique = true;
        if (transactions.length > 0) {
            var accountCode = $('#AccountCode').val();
            var rowNo = parseInt($('#hdnRowId').val());
            transactions.forEach(function (item) {
                if (accountCode === item.COAId && item.RowNo !== rowNo) {
                    isUnique = false;
                    alert('Duplicate Account Code.')
                }
            });
        }
        return isUnique;
    }

    $('#btnUpdate').click(function () {
        var isValidTransaction = CheckTransactionFieldsValidation();
        if (isValidTransaction && checkDuplicateAccountCodeForUpdate()) {
            var isChequePayment = false;
            if ($('#CheckEntry').is(":checked")) {
                isChequePayment = true;
            }
            transactions.forEach(function (item) {
                if (item.RowNo === parseInt($('#hdnRowId').val()) && (item.COAId === $('#AccountCode').val())) {
                    item.EntryType = $("#EntryType option:selected").val(),
                    item.COAId = $('#AccountCode').val(),
                    item.AccountCode = $('#hdnAccountCode').val(),
                    item.AccountName = $('#AccountName').val(),
                    item.Particular = $('#Particular').val(),
                    item.Debit = $('#Debit').val(),
                    item.Credit = $('#Credit').val(),
                    item.Remarks = $('#Remarks').val(),
                    item.IsChequePayment = isChequePayment,
                    item.ChequeNo = $('#ChequeNo').val(),
                    item.IssueDate = $('#IssueDate').val(),
                    item.BankName = $('#BankName').val(),
                    item.BranchName = $('#BranchName').val(),
                    item.BankAccountNo = $('#BankAccountNo').val(),
                    item.GroupId = $('#hdnGroupId').val(),
                    item.GroupName = $('#hdnGroupName').val(),
                    item.CategoryId = $('#hdnCategoryId').val(),
                    item.CategoryName = $('#hdnCategoryName').val(),
                    item.ClientCode = $('#hdnClientCode').val(),
                    item.ClientName = $('#hdnClientName').val(),
                    item.SupplierCode = $('#hdnSupplierCode').val(),
                    item.SupplierName = $('#hdnSupplierName').val()
                }
            });
            clearTransactionFields();
            transactionTable.destroy();
            LoadTrnsactionTable(transactions);
        }
    });

    $('#btnDelete').click(function () {
        if (confirm("Are you sure?")) {
            transactions = transactions.filter(function (item) {
                return (item.RowNo !== parseInt($('#hdnRowId').val())) && (item.COAId !== $('#AccountCode').val());
            });
            rowNo = transactions.length;
            clearTransactionFields();
            transactionTable.destroy();
            LoadTrnsactionTable(transactions);
        }
        return false;
    });

    $('#btnClear').click(function () {
        clearTransactionFields();
    });

    function checkValidation() {
        var isvalid = true;
        if ($('#TransactionDate').val().trim() === '') {
            isValid = false;
            $('#TransactionDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#TransactionDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        if (transactions.length < 1) {
            isvalid = false;
            alert('Add some transactios');
            return isvalid;
        }
        var totalDebit = 0, totalCredit = 0;
        transactions.forEach(function (item) {
            if (item.Debit !== '') {
                totalDebit = totalDebit + parseFloat(item.Debit)
            }
            if (item.Credit !== '') {
                totalCredit = totalCredit + parseFloat(item.Credit)
            }
        });
        if (totalDebit !== totalCredit) {
            isvalid = false;
            alert('Debit and Credit Amount should be equal.');
        }
        return isvalid;
    }

    $('#btnSave').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            transactions.forEach(function (item) {
                item.VoucherType = $('#VoucherType').val();
                item.TransactionDate = $('#TransactionDate').val();
                item.VoucherCode = $('#VoucherCode').val();
            });
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/Save",//'@Url.Action("Save", "FinancialTransaction", new { Area = "FinancialTransactions" })',
                type: "POST",
                data: JSON.stringify(transactions),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        document.getElementById('showMessage').style.color = "green";
                        $('#VoucherNo').val(d.VoucherNo);
                        $('#btnSave').prop('disabled', true);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#VoucherNo').val('');
                }
            });
        }
    });

    $('#btnNew').click(function () {
        location.reload();
    });

})();
