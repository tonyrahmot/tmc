﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.MonthlyBenefit.BLL;
using TonyMultipurpose.Areas.MonthlyBenefit.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.MonthlyBenefit.Controllers
{
    [AuthenticationFilter]
    public class ApprovalController : Controller
    {
        private MBBLL objMBBLL = new MBBLL();
        // GET: MonthlyBenefit/Approval
        public ActionResult Index()
        {
            // return View();


            MBModel monthlyBenefit = new MBModel();
            monthlyBenefit.IsApproved = false;
            monthlyBenefit.IsDeleted = false;
            List<MBModel> customerInfo = objMBBLL.GetAllMBInfo(monthlyBenefit);
            return View(customerInfo);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("Id");
            if(ch != null)
            {
                objMBBLL.ApproveMB(ch);
            }
            return RedirectToAction("Index");

        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<MBModel> transactions = new List<MBModel>();
            MBModel objMBModel;
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objMBModel = new MBModel();
                    objMBModel.TransactionId = ids[i];
                    objMBModel.RejectReason = form.GetValues(objMBModel.TransactionId)[0];
                    transactions.Add(objMBModel);
                }
                objMBBLL.RejectMonthleBenefitTransaction(transactions);
            }
            return RedirectToAction("Index");
        }
    }
}