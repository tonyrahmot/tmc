$(function () {
    $('#EntryDate').datepicker({
        dateFormat: 'dd/MM/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });
    $('#InstallmentDate').datepicker({
        dateFormat: 'dd/MM/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $("#LoanId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanInstallment/AutoCompleteTermLoanId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });


    ////////////////////////////////////////////
    $("#LoanId").change(function () {
        var LoanId = $("#LoanId").val();
        var json = {
            LoanId: LoanId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/LoanInstallment/GetTermLoanInfoByLoanId",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#CustomerId').html(data.CustomerId);
                $('#LabelCustomerName').html(data.CustomerName);
                $('#LabelFatherName').html(data.FatherName);
                $('#LabelExtraFee').html(data.ExtraFee);
                $('#LabelInstallmentAmount').html(data.InstallmentAmount);
                $('#InstallmentAmount').val(data.InstallmentAmount);
                $('#LabelOutstanding').html(data.Outstanding);
                $('#customerImage').attr('src', data.CustomerImage);
                //$('#Outstanding').val(data.Outstanding);
                var iA = parseFloat(data.InstallmentAmount);
                var o = parseFloat(data.Outstanding);
                var rO = parseFloat(o - iA);
                $('#RemainingOutstanding').val(rO);
            }
        });
    });

});