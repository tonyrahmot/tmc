﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class NotificationCheck : CommonModel
    {
        [Key]
        public long NotificationCheckId { get; set; }
        public string LoanId { get; set; }
        public string Notification { get; set; }
        public bool Status { get; set; }
        public string Msg { get; set; }
    }
}