﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class BankPerformance
    {
        public long BankperformanceId { get; set; }
        public string LoanId { get; set; }
        public string NgoName { get; set; }
        public string AccNo  { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmount { get; set; }
        public decimal? Avg { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public short? CreatedBy { get; set; }
    }
}