﻿using System.ComponentModel.DataAnnotations;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class LoanCustomerChequeInfoViewModel
    {
        [Key]
        public int LoanCustomerChequeInfoId { get; set; }

        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }

        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Display(Name = "Account No")]
        public string AccountNo { get; set; }

        [Display(Name = "Cheque No")]
        public string ChequeNo { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

    }
}