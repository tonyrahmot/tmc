﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;

namespace TonyMultipurpose.Areas.Accounts.ViewModels
{
    public class ChartofAccountsViewModel
    {
        public COA masterAccount { get; set; }
        public List<COA> subAccounts { get; set; }
    }
}