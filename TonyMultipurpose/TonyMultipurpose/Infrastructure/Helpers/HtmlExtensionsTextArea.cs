﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsTextArea
    {

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, null,
                null, null, null, false, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                null, null, null, false, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                placeholder, null, null, false, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         int? row,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                placeholder, row, null, false, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         int? row,
         int? column,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                placeholder, row, column, false, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         int? row,
         int? column,
         bool isRequired,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                placeholder, row, column, isRequired, false,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         int? row,
         int? column,
         bool isRequired,
         bool isAutoFocus,
         object htmlAttributes = null
         )
        {
            return BootstrapTextAreaFor(htmlHelper, expression, title,
                placeholder, row, column, isRequired, isAutoFocus,
                null, htmlAttributes);
        }

        public static MvcHtmlString BootstrapTextAreaFor<TModel, TValue>(
         this HtmlHelper<TModel> htmlHelper,
         Expression<Func<TModel, TValue>> expression,
         string title,
         string placeholder,
         int? row,
         int? column,
         bool isRequired,
         bool isAutoFocus,
         string cssClass,
         object htmlAttributes = null
         )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // Add all other attributes below here
            if (!string.IsNullOrWhiteSpace(title))
            {
                rvd.Add("title", title);
            }
            if (!string.IsNullOrWhiteSpace(placeholder))
            {
                rvd.Add("placeholder", placeholder);
            }
            if (row != null && row.Value != 0)
            {
                rvd.Add("row", row);
            }
            if (column != null && column.Value != 0)
            {
                rvd.Add("column", column);
            }
            if (isRequired)
            {
                rvd.Add("required", "required");
            }
            if (isAutoFocus)
            {
                rvd.Add("autofocus", "autofocus");
            }
            if (string.IsNullOrWhiteSpace(cssClass))
            {
                cssClass = "form-control input-sm ";
            }
            else
            {
                cssClass = "form-control input-sm " + cssClass;
            }
            rvd.Add("class", cssClass);

            return htmlHelper.TextAreaFor(expression, rvd);
        }
    }
}