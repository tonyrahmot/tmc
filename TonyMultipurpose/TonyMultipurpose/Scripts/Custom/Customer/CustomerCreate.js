﻿
        $(function () {
            $("#tabs").tabs();
        });

$(function() {
    $('#CustomerEntryDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#DateOfBirth').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#TradeLicenseDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+60",
        type: Text
    }).click(function () { $(this).focus(); });

    $('#PassportExpireDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+60",
        type: Text
    }).click(function() { $(this).focus(); });
});

$('input[type=date]').each(function() {
    this.type = "text";
});

$(document).ready(function() {
    $('#update').hide();
    //customer id autocomplete
    $("#CustomerId").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Customer/Customer/AutoCompleteCustomerId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    //Load Customer Information
    $("#CustomerId").change(function() {
        var id = $("#CustomerId").val();
        var url = window.applicationBaseUrl + "Customer/Customer/Edit/" + id;
        window.location.href = url;
    });

    BankDetailItems = [];
    //Add button click function
    $('#add').click(function() {
        if ($('#BankName').val().trim() !== '' && $('#BranchName').val().trim() !== '' && $("#AccountType option:selected").val() !== 0) {
            BankDetailItems.push({
                AccountType: $("#AccountType option:selected").text(),
                BankName: $('#BankName').val().trim(),
                BranchName: $('#BranchName').val().trim()
            });
            //Clear fields
            $('#AccountType').val('0').focus();
            $('#BankName,#BranchName').val('');
            GeneratedItemsTable();
        } else {
            alert("Add all bank info !!!!");
        }
    });

    $('#update').click(function() {
        if (BankDetailItems.length > 0) {
            $.each(BankDetailItems, function() {
                if (this.BankName === $('#BankName').val().trim() && this.BankName !== BankDetailItem[0].BankName) {
                    // alert('There is already a nominee at ' + this.NomineePosition + ' position');
                    return false;
                }
            });
        }
        var isValidNominee = true;
        if (isValidNominee) {
            //var file = document.getElementById('fileUpload').files[0];
            if (BankDetailItem != null) {
                $.each(BankDetailItems, function() {
                    if (this.BankName === BankDetailItem[0].BankName && this.BranchName === BankDetailItem[0].BranchName && this.AccountType === BankDetailItem[0].AccountType) {
                        this.AccountType = $("#AccountType option:selected").val(),
                            this.BankName = $('#BankName').val().trim(),
                            this.BranchName = $('#BranchName').val().trim();
                    }
                });
            }
            GeneratedItemsTable();
            $('#AccountType').val('0').focus();
            $('#BankName,#BranchName').val('');
            $('#add').show();
            $('#update').hide();
        }
    });

    //Remove Bank Detail
    $('#new').click(function() {
        location.reload();
        //@*var id = $("#CustomerId").val();
        //var url = '@Url.Action("Edit", "Customer", new { Area = "Customer" })?id=' + id;
        //window.location.href = url;*@
        });

    $('#edit').click(function() {
        clearFields();
        $('#CustomerId').removeAttr('readonly');
        $('#CustomerId').val('');
        $('#CustomerId').focus();
        $('#submit').val('Update');
    });

    $('#submit').click(function() {
        if ($('#submit').val() === "Update") {
            var a = $('#hiddenCustomerId').val();
            $('#CustomerId').val(a);
            updateCustomer();
        } else {
            var isAllValid = checkValidation();
            if (isAllValid) {
                var data = bindData();
                $.ajax({
                    url: window.applicationBaseUrl + "Customer/Customer/Create",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function(obj) {
                        if (obj.status === true) {
                            alert("Successfuly saved " + "Customer Id =" + obj.Id + "");
                            $('#hiddenCustomerId').val(obj.Id);
                            $('#CustomerId').val(obj.Id);
                            $('#CustomerId').focus();
                            if (obj.image !== 'arya') {
                                saveImage(obj);
                            }
                            $('#submit').val('Update');
                            //clearFields();
                        } else {
                            alert('Failed');
                        }
                    },
                    error: function() {
                        alert('Error. Please try again.');
                        $('#submit').val('Save');
                    }
                });
            }
        }
    });
});

function GeneratedItemsTable() {
    if (BankDetailItems.length > 0) {
        var $table = $('<table id="myTable" class="table table-striped table-hover" width="100%"/>');
        $table.append('<thead><tr class="success">' +
            '<th>SL.</th>' +
            '<th>Bank Name</th>' +
            '<th>Branch Name</th>' +
            '<th>Account Type</th>' +
            '<th>Edit</th>' +
            '<th>Delete</th>' +
            '</tr></thead>');
        var $tbody = $('<tbody/>');
        var sl = 0;
        $.each(BankDetailItems, function (i, val) {
            sl = sl + 1;
            var $row = $('<tr class="active"/>');
            $row.append($('<td id="Sl"/>').html(sl));
            $row.append($('<td id="Bank"/>').html(val.BankName));
            $row.append($('<td id="Branch"/>').html(val.BranchName));
            $row.append($('<td id="Type"/>').html(val.AccountType));
            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='LoadBankDetail(this)'>" +
                "<span class='glyphicon glyphicon-pencil'></span>" +
                "</button>"));
            $row.append($('<td/>').html("<button type='button' class='btn btn-default' onclick='DeleteBankDetail(this)'>" +
                "<span class='glyphicon glyphicon-remove'></span>" +
                "</button>"));
            $tbody.append($row);
        });
        $table.append($tbody);

        $('#BankItems').html($table);
    }
}

function DeleteBankDetail(btn) {
    $row = $(btn).closest("tr");
    bank = $row
        .find("#Bank")
        .text();
    branch = $row
        .find("#Branch")
        .text();
    type = $row
        .find("#Type")
        .text();
    $row.remove();
    BankDetailItems = BankDetailItems.filter(function (item) {
        return (item.BankName !== bank) && (item.BranchName !== branch) && (item.AccountType !== type);
    });
    $('.bankDetailInput').val('');
    GeneratedItemsTable();
}

function LoadBankDetail(btn) {
    $row = $(btn).closest("tr");
    bank = $row
        .find("#Bank")
        .text();
    branch = $row
        .find("#Branch")
        .text();
    type = $row
        .find("#Type")
        .text();
    BankDetailItem = BankDetailItems.filter(function(item) {
        return (item.BankName === bank) && (item.BranchName === branch) && (item.AccountType === type);
    });
    if (BankDetailItem[0].BranchName != null && BankDetailItem[0].BankName != null && BankDetailItem[0].AccountType != null) {
        $('#BankName').val(BankDetailItem[0].BankName);
        $('#BranchName').val(BankDetailItem[0].BranchName);
        $('#AccountType').val(BankDetailItem[0].AccountType);
        $('#add').hide();
        $('#update').show();
    }
}

function updateCustomer() {
    var isAllValid = checkValidation();
    if (isAllValid) {
        var newImage = 'arya';
        var data = {
            CustomerId: $('#CustomerId').val(),
            CustomerName: $('#CustomerName').val(),
            CustomerEntryDate: $('#CustomerEntryDate').val(),
            Gender: $("#Gender option:selected").text(),
            TradeLicenseNo: $('#TradeLicenseNo').val(),
            TradeLicAuthority:$('#TradeLicAuthority').val(),
            TradeLicenseDate: $('#TradeLicenseDate').val(),
            RegistrationNo: $('#RegistrationNo').val(),
            RegistrationAuthority: $('#RegistrationAuthority').val(),
            FatherName: $('#FatherName').val(),
            MotherName: $('#MotherName').val(),
            SpouseName: $('#SpouseName').val(),
            Nationality: $("#Nationality option:selected").text(),
            NationalId: $('#NationalId').val(),
            DateOfBirth: $('#DateOfBirth').val(),
            OccupationAndPosition: $('#OccupationAndPosition').val(),
            PassportNo: $('#PassportNo').val(),
            PassportExpireDate: $('#PassportExpireDate').val(),
            TINNo: $('#TINNo').val(),
            DrivingLicenseNo: $('#DrivingLicenseNo').val(),
            PresentAddress: $('#PresentAddress').val(),
            PermanentAddress: $('#PermanentAddress').val(),
            OfficeAddress: $('#OfficeAddress').val(),
            Home: $('#Home').val(),
            Office: $('#Office').val(),
            Mobile: $('#Mobile').val(),
            Email: $('#Email').val(),
            CustomerIncomeSource: $('#CustomerIncomeSource').val(),
            AnotherCellNo: $('#AnotherCellNo').val(),
            Position: $('#Position').val(),
            BirthCertificateNo: $('#BirthCertificateNo').val(),
            CustomerImage: $('#hiddenCustomerImagePath').val(),
            OldImage: $('#hiddenCustomerImagePath').val(),
            CustomerBankDetails: BankDetailItems
        }
        var file = document.getElementById('fileUpload').files[0];
        if (file != null) {
            data.CustomerImage = 'Cersi';
            NewImage = 'Cersi';
        } else {
            data.CustomerImage = 'arya';
        }
        $.ajax({
            url: window.applicationBaseUrl + "Customer/Customer/SaveUpdate",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (obj) {
                if (obj.status === true) {
                    alert("Successfuly saved " + "Customer Id =" + obj.Id + "");
                    $('#CustomerId').val(obj.Id);
                    $('#CustomerId').focus();
                    if (NewImage !== 'arya') {
                        var fd = new FormData();
                        fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
                        fd.append("CustomerId", obj.Id);
                        fd.append("CustomerImage", obj.image);
                        $('#hiddenCustomerId').val(obj.Id);
                        $('#hiddenCustomerImagePath').val(obj.image);
                        $('#hiddenCustomerName').val(obj.name);
                        $.ajax({
                            url: window.applicationBaseUrl + "Customer/Customer/SaveImageFile",// '@Url.Action("SaveImageFile", "Customer", new {Area = "Customer"})',
                            type: "POST",
                            data: fd,
                            dataType: "JSON",
                            contentType: false,
                            processData: false,
                            success: function (obj) {
                                if (obj.status == true) {
                                    //alert('successfully saved');
                                    $('#hiddenCustomerImagePath').val(obj.image);
                                } else {
                                    alert("failed");
                                }
                            },
                            error: function () {
                                alert('Error. Please try again.');
                            }
                        });
                    }
                } else {
                    alert('Failed');
                }
                //$('#submit').val('Save');
            },
            error: function () {
                alert('Error. Please try again.');
                //$('#submit').val('Save');
            }
        });
    }
}

function bindData() {
    var data = {
        CustomerId: $('#CustomerId').val(),
        CustomerName: $('#CustomerName').val(),
        CustomerEntryDate: $('#CustomerEntryDate').val(),
        Gender: $("#Gender option:selected").text(),
        TradeLicenseNo: $('#TradeLicenseNo').val(),
        TradeLicAuthority: $('#TradeLicAuthority').val(),
        TradeLicenseDate: $('#TradeLicenseDate').val(),
        RegistrationNo: $('#RegistrationNo').val(),
        RegistrationAuthority: $('#RegistrationAuthority').val(),
        FatherName: $('#FatherName').val(),
        MotherName: $('#MotherName').val(),
        SpouseName: $('#SpouseName').val(),
        Nationality: $("#Nationality option:selected").text(),
        NationalId: $('#NationalId').val(),
        DateOfBirth: $('#DateOfBirth').val(),
        OccupationAndPosition: $('#OccupationAndPosition').val(),
        PassportNo: $('#PassportNo').val(),
        PassportExpireDate: $('#PassportExpireDate').val(),
        TINNo: $('#TINNo').val(),
        DrivingLicenseNo: $('#DrivingLicenseNo').val(),
        PresentAddress: $('#PresentAddress').val(),
        PermanentAddress: $('#PermanentAddress').val(),
        OfficeAddress: $('#OfficeAddress').val(),
        Home: $('#Home').val(),
        Office: $('#Office').val(),
        Mobile: $('#Mobile').val(),
        Email: $('#Email').val(),
        CustomerIncomeSource: $('#CustomerIncomeSource').val(),
        AnotherCellNo: $('#AnotherCellNo').val(),
        Position: $('#Position').val(),
        BirthCertificateNo: $('#BirthCertificateNo').val(),
        CustomerImage: 'arya',
        CustomerBankDetails: BankDetailItems
    }
    var file = document.getElementById('fileUpload').files[0];

    if (file != null) {
        data.CustomerImage = 'Cersi';
    }
    return data;
}

function checkValidation() {
    var isAllValid = true;
    if ($('#CustomerName').val().trim() === '') {
        $('#CustomerName').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#CustomerName').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#FatherName').val().trim() === '') {
        $('#FatherName').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#FatherName').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#MotherName').val().trim() === '') {
        $('#MotherName').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#MotherName').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#CustomerEntryDate').val().trim() === '') {
        $('#CustomerEntryDate').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#CustomerEntryDate').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#DateOfBirth').val().trim() === '') {
        $('#DateOfBirth').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#DateOfBirth').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#Gender').val().trim() === '') {
        $('#Gender').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#Gender').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#Email').val().trim() !== '' && !validateEmailAddress($('#Email').val())) {
        isAllValid = false;
    }
    return isAllValid;
}

function clearFields() {
    BankDetailItems = [];
    $('#CustomerId').val('< New Entry >');
    //$('#CustomerEntryDate').val('');
    $('#imageDisplay').attr('src', '');
    $('#CustomerName').val('');
    $('#TradeLicenseNo').val('');
    $('#TradeLicAuthority').val('');
    $('#TradeLicenseDate').val('');
    $('#RegistrationNo').val('');
    $('#RegistrationAuthority').val('');
    $('#FatherName').val('');
    $('#MotherName').val('');
    $('#SpouseName').val('');
    $('#Nationality').val('');
    $('#NationalId').val('');
    $('#DateOfBirth').val('');
    $('#Gender').val('');
    $('#OccupationAndPosition').val('');
    $('#PassportNo').val('');
    $('#PassportExpireDate').val('');
    $('#TINNo').val('');
    $('#DrivingLicenseNo').val('');
    $('#PresentAddress').val('');
    $('#PermanentAddress').val('');
    $('#OfficeAddress').val('');
    $('#Home').val('');
    $('#Mobile').val('');
    $('#Office').val('');
    $('#Email').val('');
    $('#CustomerIncomeSource').val('');
    $('#AnotherCellNo').val(''),
        $('#Position').val(''),
        $('#BirthCertificateNo').val(''),
        $('#BankItems').empty();
    $('#submit').val('Save');
}

function saveImage(obj) {
    var fd = new FormData();
    fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
    fd.append("CustomerId", obj.Id);
    fd.append("CustomerImage", obj.image);
    $('#hiddenCustomerId').val(obj.Id);
    $('#hiddenCustomerImagePath').val(obj.image);
    $('#hiddenCustomerName').val(obj.name);
    $.ajax({
        url: window.applicationBaseUrl + "Customer/Customer/SaveImageFile",
        type: "POST",
        data: fd,
        dataType: "JSON",
        contentType: false,
        processData: false,
        success: function(obj) {
            if (obj.status == true) {
                $('#hiddenCustomerImagePath').val(obj.image);
            } else {
                alert("failed");
            }
        },
        error: function() {
            alert('Error. Please try again.');
        }
    });
}

$("#fileUpload").change(function() {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imageDisplay').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }

    //    //var file = document.getElementById('fileUpload').files[0];
    //    //if (file.size > 5000000) {
    //    //    alert('File Size Exceeds 5 MB Limit, pls upload image below 200 KB');
    //    //    return;
    //    //}
    //    //else if (!file.type.match(/image.*/)) {
    //    //    alert('Please Upload Image File');
    //    //    return;
    //    //}
});

// mobile number validation

function mobileNumber(inputtxt) {
    var mobileNo = /^(?:\+88|01)?\d{11}$/;
    if (inputtxt.value.match(mobileNo)) {
        return true;
    }
    else {
        alert("Not a valid Mobile Number");
        return false;
    }
}

function validateEmailAddress(emailId) {
    if (emailId === '') {
        return true;
    } else {
        var expr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (expr.test(emailId)) {
        return true;
    } else {
        alert("Not a valid Email Address");
        return false;
    }
    }
    
}

    $(document).ready(function () {
        $("#NationalId").keyup(function () {
            var name = $("#NationalId").val();
            var status = $("#messageForDuplicateNIDCheck");
            var user = $.trim(name);
            if (user.length >= 0) {
                status.html("Checking....");
                $.post(window.applicationBaseUrl + "Customer/Customer/GetNIDCheck",{ NID: name },
                            function (data) {
                                if (data === true) {
                                    status.html("<font color=green>'<b>" + name + "</b>' is not exist !</font>");
                                } else {
                                    status.html("<font color=red>'<b>" + name + "</b>' is exist !</font>");
                                }
                            });
            } else {
                status.html("");
            }
        });
    });
