﻿$(document).ready(function () {
    $("#GroupName").change(function () {
        var name = $("#GroupName").val();
        var status = $("#showErrorMessage");
        var user = $.trim(name);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "FinancialTransactions/AccountGroup/GetGroupNameCheck", { GroupName: name },//'@Url.Action("GetGroupNameCheck", "AccountGroup", new {Area = "FinancialTransactions" })', { GroupName: name },
                function (data) {
                    if (data === true) {
                        status.html("<font color=green>'<b>" + name + "</b>' is not exist !</font>");
                        $('#btn_submit').attr('disabled', false);
                    } else {
                        status.html("<font color=red>'<b>" + name + "</b>' is exist !</font>");
                        $('#btn_submit').attr('disabled', true);
                    }
                });
        } else {
            status.html("");
        }
    });

    $('#btn_submit').click(function () {
        var name = $("#GroupName").val();
        var status = $("#showErrorMessage");
        //var allData = true;
        if (name === '') {
            //allData = false;
            status.html("Please Type Account Group Name");
            return false;
        }
        else {
            return true;
        }
    });
});

