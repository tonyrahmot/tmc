﻿using System;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Share.BLL;
using TonyMultipurpose.Areas.Share.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.Controllers
{
    public class SharePurchaseController : Controller
    {
        // GET: Share/ShareEntry
       public SharePurchaseBLL _objSharePurchaseBll;
        CommonResult _oCommonResult;
        public SharePurchaseController()
        {
            _objSharePurchaseBll = new SharePurchaseBLL();
        }
        public ActionResult Index()
        {
            var shareHoldingViewModel =
               _objSharePurchaseBll.GetShareHoldingInfo(Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            var model = new SharePurchaseViewModel
            {
                IssuedDate = DateTime.Now,
                PricePerShare = shareHoldingViewModel.PricePerShare
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(SharePurchaseViewModel objSharePurchaseViewModel)
        {
            if (ModelState.IsValid)
            {
                objSharePurchaseViewModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objSharePurchaseViewModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objSharePurchaseViewModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                _objSharePurchaseBll.SaveSharePurchase(objSharePurchaseViewModel);
                return RedirectToAction("Index", "Home", new { area = "" });
            }

            return View(objSharePurchaseViewModel);
        }
        public ActionResult SaveSharePurchaseEntry(SharePurchaseViewModel objSharePurchaseViewModel)
        {
            _oCommonResult = new CommonResult();
            _objSharePurchaseBll = new SharePurchaseBLL();
            if (ModelState.IsValid)
            {
                _oCommonResult = _objSharePurchaseBll.SaveSharePurchase(objSharePurchaseViewModel);
            }
            return new JsonResult { Data = new { _oCommonResult } };
        }

        [HttpGet]
        public ActionResult GetCustomerInfo(string id)
        {
            var customer = new CustomerViewModel()
            {
                CustomerId = id,
                CompanyId = Convert.ToInt32(SessionUtility.TMSessionContainer.CompanyId)

            };
            var data = _objSharePurchaseBll.GetCustomerInfoWithExistingShareQtyByCustomerId(customer).FirstOrDefault();

            return PartialView("_Customer", data);
        }

        public JsonResult AutoCompleteCustomerId(string term)
        {
            var result = _objSharePurchaseBll.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCompanyShareInfo()
        {
            _oCommonResult = new CommonResult();
            _objSharePurchaseBll = new SharePurchaseBLL();
            var data = _objSharePurchaseBll.GetCompanyShareInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}