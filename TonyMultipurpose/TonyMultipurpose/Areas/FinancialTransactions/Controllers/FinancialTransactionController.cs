﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
  //  [AuthenticationFilter]
    public class FinancialTransactionController : Controller
    {
        FinancialTransactionsBLL _objFinancialTransactionsBLL = new FinancialTransactionsBLL();
        FinancialTransactionsModel _objFinancialTransactionsModel;
        // GET: FinancialTransactions/AccountsPayable
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        public ActionResult AccountsPayable()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AccountsReceivable()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Details(string VoucherNo)
        {
            _objFinancialTransactionsBLL = new FinancialTransactionsBLL();
            var model = new PaymentDetailsModel();

            model = _objFinancialTransactionsBLL.GetPaymentDetailsByVoucherNo(VoucherNo);

            if (model != null)
            {
                return View("PaymentDetails", model);
            }           

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Bad Request");
        }

        [HttpGet]
        public ActionResult ContraEntry()
        {
            return View();
        }

        [HttpGet]
        public ActionResult JournalEntry()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(List<FinancialTransactionsModel> transactions)
        {
            CommonResult oCommonResult = new CommonResult();
            bool status = false;
            var transactionId = _objFinancialTransactionsBLL.GetTransactionId();
            var voucherNo = _objFinancialTransactionsBLL.GetVoucherNo();            
            foreach (FinancialTransactionsModel _objFinancialTransactionsModel in transactions)
            {
                _objFinancialTransactionsModel.TransactionId = transactionId;
                _objFinancialTransactionsModel.VoucherNo = voucherNo;
                _objFinancialTransactionsModel.IsActive = true;
                _objFinancialTransactionsModel.IsApproved = false;
                _objFinancialTransactionsModel.IsCleared = false;
                _objFinancialTransactionsModel.IsRejected = false;
                _objFinancialTransactionsModel.IsReversed = false;
                _objFinancialTransactionsModel.CreatedDate = DateTime.Now;
                _objFinancialTransactionsModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                _objFinancialTransactionsModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                _objFinancialTransactionsModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                oCommonResult = _objFinancialTransactionsBLL.SaveFinancialTransactionsInfo(_objFinancialTransactionsModel);
            }
            return new JsonResult { Data = new { oCommonResult, VoucherNo = voucherNo } };
        }

        [HttpPost]
        public JsonResult GetAllAccountCode()
        {
            var data = _objFinancialTransactionsBLL.GetAllAccountCode();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAllExpenseAccountCode()
        {
            var coaCatId = new List<int>{1018,1019,1020,1021,2};
            var coaId = new List<int> { 10123, 10125, 10131, 10132};
            var data = _objFinancialTransactionsBLL.GetAllAccountCode().Where(p => (coaCatId.Contains(p.COACategoryId) || coaId.Contains(p.COAId)) && p.IsSubCode == false);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllIsTransactionalAccountCode()
        {
            var data = _objFinancialTransactionsBLL.GetAllAccountCode().Where(a => a.IsTransactional);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAccountCodeInfo(int coaId)
        {
            COABLL objCOABLL = new COABLL();
            var data = objCOABLL.GetCOAInfo(coaId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public void PaymentVoucherReport(string VoucherNo)
        {
            ReportObject.ReportName = "AccountPaymentVoucherReport";
            ReportObject.LoanId = VoucherNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }

        public void AccountsRecievableReport(string VoucherNo)
        {
            ReportObject.ReportName = "AccountsRecievableVoucherReport";
            ReportObject.VoucherNo = VoucherNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }

        public void ContraEntryReport(string VoucherNo)
        {
            ReportObject.ReportName = "ContraEntryReport";
            ReportObject.LoanId = VoucherNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }

        public void JournalEntryReport(string VoucherNo)
        {
            ReportObject.ReportName = "JournalEntryReport";
            ReportObject.VoucherNo = VoucherNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }


        #region Balance Sheet Configuration

     
        public ActionResult BalanceSheet()
        {
            AccountGroupBLL oAccountGroupBLL = new AccountGroupBLL();
            var model = new COA
            {
                AccountGroups = oAccountGroupBLL.GetAllAccountGroupInfo()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveBalanceSheet(COA objCoa)
        {
            FinancialTransactionsBLL objFinancialTransactionsBll=new FinancialTransactionsBLL();
            objFinancialTransactionsBll.SaveBalanceSheetInfo(objCoa);
            return RedirectToAction("BalanceSheet", "FinancialTransaction");
        }

        public ActionResult GetBalanceSheetInfo()
        {
            FinancialTransactionsBLL objFinancialTransactionsBll = new FinancialTransactionsBLL();
            var balanceSheetInfo = objFinancialTransactionsBll.GetBalanceSheetInfo();
            return Json(new { data = balanceSheetInfo }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UpdateBalanceSheet(COA objCoa)
        {
            CommonResult oCommonResult = new CommonResult();
            FinancialTransactionsBLL objFinancialTransactionsBll = new FinancialTransactionsBLL();
            oCommonResult=objFinancialTransactionsBll.UpdateBalanceSheet(objCoa);
            return new JsonResult { Data = new { oCommonResult } };

            //return RedirectToAction("BalanceSheet", "FinancialTransaction");
        }

        public JsonResult GetConvertNumberToWord(int number)
        {
            TransactionBLL objTransactionBll = new TransactionBLL();
            var exsits = objTransactionBll.GetConvertNumberToWord(number);
            return new JsonResult { Data = exsits };
        }

        #endregion
    }
}