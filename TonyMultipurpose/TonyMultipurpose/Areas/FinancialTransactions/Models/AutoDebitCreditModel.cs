﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class AutoDebitCreditModel: CommonModel
    {
        public long? Id { get; set; }

        public string AccountFrom { get; set; }

        public string AccountTo { get; set; }

        public decimal Amount { get; set; }

        public bool? IsAuto { get; set; }

        public string UserName { get; set; }

    }

}