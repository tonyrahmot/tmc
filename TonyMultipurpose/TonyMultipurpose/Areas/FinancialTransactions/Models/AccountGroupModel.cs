﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class AccountGroupModel : CommonModel
    {
        [Key]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
       
    }
}