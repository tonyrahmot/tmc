﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.CSS.BLL;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.CSS.Controllers
{
    [AuthenticationFilter]
    public class CSSApprovalController : Controller
    {
        CSSBLL oCSSBLL;
        CSSModel oCSSModel;
        // GET: CSS/CSSApproval
        public ActionResult Index()
        {
            oCSSModel = new CSSModel();
            oCSSBLL = new CSSBLL();
            oCSSModel.IsApproved = false;
            //oCSSModel.IsActive = true;
            oCSSModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            List<CSSModel> cssTransactions = oCSSBLL.GetCSSTransactions(oCSSModel);
            return View(cssTransactions);
        }

        public ActionResult Details(int Id)
        {
            return View();
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            oCSSBLL = new CSSBLL();
            var ids = form.GetValues("Id");
            if(ids != null)
            {
                oCSSBLL.ApproveCSSTransactions(ids);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<CSSModel> transactions = new List<CSSModel>();
            CSSModel objCSSModel;
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objCSSModel = new CSSModel();
                    objCSSModel.TransactionId = ids[i];
                    objCSSModel.RejectReason = form.GetValues(objCSSModel.TransactionId)[0];
                    transactions.Add(objCSSModel);
                }
                oCSSBLL = new CSSBLL();
                oCSSBLL.RejectCSSTransaction(transactions);
            }
            return RedirectToAction("Index");
        }
    }
}