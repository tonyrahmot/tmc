﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    [AuthenticationFilter]
    public class AccountGroupController : Controller
    {
        AccountGroupBLL _objAccountGroupBLL = new AccountGroupBLL();
        // GET: FinancialTransactions/AccountGroup
        public ActionResult Index()
        {
            List<AccountGroupModel> _objAccountGroupList = new List<AccountGroupModel>();
            _objAccountGroupList = _objAccountGroupBLL.GetAllAccountGroupInfo();
            return View(_objAccountGroupList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(AccountGroupModel _objAccountGroup, HttpPostedFileBase File)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                _objAccountGroup.IsActive = true;
                _objAccountGroup.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                _objAccountGroupBLL.SaveAccountGroupInfo(_objAccountGroup);
                status = true;
            }
            else
            {
                return View(_objAccountGroup);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "AccountGroup");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var _AccountGroup = _objAccountGroupBLL.GetAccountGroupInfo(id);
            return View(_AccountGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountGroupModel _objAccountGroup)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                _objAccountGroupBLL.UpdateAccountGroupInfo(_objAccountGroup);
                status = true;
            }
            else
            {
                return View(_objAccountGroup);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "AccountGroup");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var _AccountGroup = _objAccountGroupBLL.GetAccountGroupInfo(id);
            return View(_AccountGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteAccountGroup(int id)
        {
            _objAccountGroupBLL.DeleteAccountGroup(id);
            return RedirectToAction("Index", "AccountGroup");
        }

        public JsonResult GetGroupNameCheck(string GroupName)
        {
            bool exsits = _objAccountGroupBLL.GetGroupNameCheck(GroupName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
    }
}