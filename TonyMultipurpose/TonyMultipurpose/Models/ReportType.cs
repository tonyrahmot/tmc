﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Models
{
    public class ReportType
    {
        public string PDF { get; set; }
        public string Excel { get; set; }
        public string CSV { get; set; }

    }
}