﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Share.Models;
using TonyMultipurpose.Areas.Share.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.BLL
{
    
    public class ShareEntryBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;
        CommonResult _oCommonResult;
        private DataTable _objDataTable;
        private DbDataReader _objDbDataReader;
        public CommonResult SaveShareEntry(AuthorizedShareInfo objAuthorizedShareInfo)
        {
            var transactionId = GenerateTransactionId();
            
            _oCommonResult = new CommonResult
            {
                Status = false,
                Message = "Failed"
            };
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("AuthorizedAmount", objAuthorizedShareInfo.AuthorizedAmount);
            _objDbCommand.AddInParameter("PricePerShare", objAuthorizedShareInfo.PricePerShare);
            _objDbCommand.AddInParameter("NoOfShare", objAuthorizedShareInfo.NoOfShare);
            _objDbCommand.AddInParameter("Particular", objAuthorizedShareInfo.Particular);
            _objDbCommand.AddInParameter("TransactionDate", objAuthorizedShareInfo.TransactionDate);
            _objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            _objDbCommand.AddInParameter("CreatedDate", DateTime.Now.Date);
            _objDbCommand.AddInParameter("transactionId", transactionId);
            _objDbCommand.AddInParameter("EntryType", "Authorize");
            _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_SaveAuthorizedShareInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    _oCommonResult.Status = true;
                    _oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return _oCommonResult;
        }

        public CommonResult UpdateShareEntry(AuthorizedShareInfo objAuthorizedShareInfo)
        {

            _oCommonResult = new CommonResult
            {
                Status = false,
                Message = "Failed"
            };
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("Id", objAuthorizedShareInfo.Id);
            _objDbCommand.AddInParameter("AuthorizedAmount", objAuthorizedShareInfo.AuthorizedAmount);
            _objDbCommand.AddInParameter("PricePerShare", objAuthorizedShareInfo.PricePerShare);
            _objDbCommand.AddInParameter("NoOfShare", objAuthorizedShareInfo.NoOfShare);
            _objDbCommand.AddInParameter("Particular", objAuthorizedShareInfo.Particular);
            _objDbCommand.AddInParameter("TransactionDate", objAuthorizedShareInfo.TransactionDate);
            _objDbCommand.AddInParameter("UpdatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            _objDbCommand.AddInParameter("UpdatedDate", DateTime.Now.Date);
            //_objDbCommand.AddInParameter("transactionId", objAuthorizedShareInfo.TransactionId);
            _objDbCommand.AddInParameter("EntryType", "Authorize");
            _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_UpdateAuthorizedShareInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    _oCommonResult.Status = true;
                    _oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return _oCommonResult;
        }

        public string GenerateTransactionId()
        {
            string transactionId = string.Empty;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            string prefix = year + days;
            _objDbCommand.AddInParameter("FieldName", "TransactionId");
            _objDbCommand.AddInParameter("TableName", "AuthorizedShareInfo");
            _objDbCommand.AddInParameter("Prefix", prefix);
            _objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (_objDbDataReader.HasRows)
            {
                while (_objDbDataReader.Read())
                {
                    transactionId = prefix;
                    int id = Convert.ToInt32(_objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(id).PadLeft(4, '0');
                }
            }
            _objDbDataReader.Close();
            return transactionId;
        }

        public List<AuthorizeShareViewModel> GetAuthorizeShareData()
        {

            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AuthorizeShareViewModel> authorizedShareInfos = new List<AuthorizeShareViewModel>();
            AuthorizeShareViewModel oAuthorizedShareInfo;
            try
            {
                {
                    objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[Share_GetAuthorizedShareAmount]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)


                    {
                        while (objDbDataReader.Read())
                        {
                            oAuthorizedShareInfo = new AuthorizeShareViewModel();
                            if (objDbDataReader["Id"] != null)
                            {
                                oAuthorizedShareInfo.Id = Convert.ToInt32(objDbDataReader["Id"]);
                                oAuthorizedShareInfo.Total = objDbDataReader["Total"].ToString();
                                //oAuthorizedShareInfo.AuthorizedAmount = Convert.ToDecimal(objDbDataReader["AuthorizedAmount"]);
                                //oAuthorizedShareInfo.TransactionId = Convert.ToString(objDbDataReader["TransactionId"]);
                                authorizedShareInfos.Add(oAuthorizedShareInfo);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return authorizedShareInfos;
        }

        public List<AuthorizeShareViewModel> GetAllAuthorizedShareInfos()
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            //DbDataReader objDbDataReader = null;
            _objDataTable =new DataTable();
            List<AuthorizeShareViewModel> objAuthorizedShareInfoList = new List<AuthorizeShareViewModel>();
            AuthorizeShareViewModel objAuthorizedShareInfo;
            try
            {
                _objDbCommand.AddInParameter("Id",0);
                _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[Share_GetAuthorizedShareInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count>0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objAuthorizedShareInfo = new AuthorizeShareViewModel
                        {
                            TransactionId = Convert.ToString(dr["TransactionId"]),
                            Id = Convert.ToInt64(dr["Id"]),
                            TransactionDate = Convert.ToString(dr["TransactionDate"]),
                            AuthorizedAmount = Convert.ToDecimal(dr["AuthorizedAmount"]),
                            PricePerShare = Convert.ToDecimal(dr["PricePerShare"]),
                            NoOfShare = Convert.ToDecimal(dr["NoOfShare"]),
                            Particular = Convert.ToString(dr["Particular"]),
                            EntryType = Convert.ToString(dr["EntryType"])
                        };
                        objAuthorizedShareInfoList.Add(objAuthorizedShareInfo);
                    }

                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return objAuthorizedShareInfoList;
        }


        public List<IssuedShareViewModel> GetAllIssuedShareInfos()
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            //DbDataReader objDbDataReader = null;
            _objDataTable = new DataTable();
            List<IssuedShareViewModel> objIssuedShareInfoList = new List<IssuedShareViewModel>();
            IssuedShareViewModel objIssuedShareInfo;
            try
            {
                _objDbCommand.AddInParameter("Id", 0);
                _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[Share_GetIssuedShareInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objIssuedShareInfo = new IssuedShareViewModel
                        {
                            TransactionId = Convert.ToString(dr["TransactionId"]),
                            Id = Convert.ToInt32(dr["Id"]),
                            AuthorizedShareInfoId = Convert.ToInt32(dr["AuthorizedShareInfoId"]),
                            IssuedDate = Convert.ToString(dr["IssuedDate"]),
                            IssuedAmount = Convert.ToDecimal(dr["IssuedAmount"]),
                            PricePerShare = Convert.ToDecimal(dr["PricePerShare"]),
                            IssuedQty = Convert.ToDecimal(dr["IssuedQty"]),
                            Particular = Convert.ToString(dr["Particular"]),
                            EntryType = Convert.ToString(dr["EntryType"])
                        };
                        objIssuedShareInfoList.Add(objIssuedShareInfo);
                    }

                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return objIssuedShareInfoList;
        }

        public IssuedShareViewModel GetRemainingIssued(int authorizedShareId)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<IssuedShareViewModel> objIssuedShareInfoList = new List<IssuedShareViewModel>();
            IssuedShareViewModel objIssuedShareInfo = new IssuedShareViewModel();
            _objDbCommand.AddInParameter("AuthorizedShareInfoId", authorizedShareId);
            try
            {
                {
                    objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[Share_GetRemainingShareByAuthorizedShareInfoId]", CommandType.StoredProcedure);

                    if (objDbDataReader.HasRows)


                    {
                        while (objDbDataReader.Read())
                        {
                            objIssuedShareInfo = new IssuedShareViewModel();
                            //if (objDbDataReader["Id"] != null)
                            //{
                                objIssuedShareInfo.RemainingBalance = Convert.ToDecimal(objDbDataReader["RemainingBalance"]) ;
                                objIssuedShareInfoList.Add(objIssuedShareInfo);
                            //}
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objIssuedShareInfo;
        }

        public CommonResult SaveIssuedShare(IssuedShareInfo objIssuedShareInfo)
        {
            var transactionId = GenerateTransactionId();

            _oCommonResult = new CommonResult
            {
                Status = false,
                Message = "Failed"
            };
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("IssuedAmount", objIssuedShareInfo.IssuedAmount);
            _objDbCommand.AddInParameter("AuthorizedShareInfoId", objIssuedShareInfo.AuthorizedShareInfoId);
            _objDbCommand.AddInParameter("PricePerShare", objIssuedShareInfo.PricePerShare);
            _objDbCommand.AddInParameter("IssuedQty", objIssuedShareInfo.IssuedQty);
            _objDbCommand.AddInParameter("Particular", objIssuedShareInfo.Particular);
            _objDbCommand.AddInParameter("IssuedDate", objIssuedShareInfo.IssuedDate);
            _objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            _objDbCommand.AddInParameter("CreatedDate", DateTime.Now.Date);
            _objDbCommand.AddInParameter("transactionId", transactionId);
            _objDbCommand.AddInParameter("EntryType", "Issued");
            _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_SaveIssuedShareInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    _oCommonResult.Status = true;
                    _oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return _oCommonResult;
        }

        public CommonResult UpdateIssuedShare(IssuedShareInfo objIssuedShareInfo)
        {

            _oCommonResult = new CommonResult
            {
                Status = false,
                Message = "Failed"
            };
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("Id", objIssuedShareInfo.Id);
            _objDbCommand.AddInParameter("AuthorizedShareInfoId", objIssuedShareInfo.AuthorizedShareInfoId);
            _objDbCommand.AddInParameter("IssuedAmount", objIssuedShareInfo.IssuedAmount);
            _objDbCommand.AddInParameter("PricePerShare", objIssuedShareInfo.PricePerShare);
            _objDbCommand.AddInParameter("IssuedQty", objIssuedShareInfo.IssuedQty);
            _objDbCommand.AddInParameter("Particular", objIssuedShareInfo.Particular);
            _objDbCommand.AddInParameter("IssuedDate", objIssuedShareInfo.IssuedDate);
            _objDbCommand.AddInParameter("UpdatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            _objDbCommand.AddInParameter("UpdatedDate", DateTime.Now.Date);
            _objDbCommand.AddInParameter("EntryType", "Issued");
            _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_UpdateIssuedShareInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    _oCommonResult.Status = true;
                    _oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return _oCommonResult;
        }

        public List<IssuedShareViewModel> GetPaidUpShareInfos()
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            //DbDataReader objDbDataReader = null;
            _objDataTable = new DataTable();
            List<IssuedShareViewModel> objIssuedShareInfoList = new List<IssuedShareViewModel>();
            IssuedShareViewModel objIssuedShareInfo;
            try
            {
                _objDbCommand.AddInParameter("AuthorizedShareInfoId", 0);
                _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[Share_GetShareHoldingInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objIssuedShareInfo = new IssuedShareViewModel
                        {
                            PricePerShare = Convert.ToDecimal(dr["PricePerShare"]),
                            SoldQty = Convert.ToDecimal(dr["SoldQty"]),
                            PaidUpAmount = Convert.ToDecimal(dr["PaidUpAmount"])
                        };
                        objIssuedShareInfoList.Add(objIssuedShareInfo);
                    }

                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return objIssuedShareInfoList;
        }
    }
}