﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Setup.Models
{
    public class TermsAndConditions
    {
        public int Id { get; set; }

        public int AccountTypeId { get; set; }

        public string TermsAndConditionsContent { get; set; }
    }
}