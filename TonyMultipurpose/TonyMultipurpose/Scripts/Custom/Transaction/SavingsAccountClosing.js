﻿$(document).ready(function () {
    $('#TransactionDate').datepicker({ dateFormat: 'dd-M-yy', type: Text }).click(function () { $(this).focus(); });
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/ClosingSavings/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $(function () {
        $("#ChartOfAccount").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/ClosingSavings/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });

    $("#AccountNumber").change(function () {
        var accNo = $("#AccountNumber").val();
        promiseToCheckLoanTable(accNo).then(function(result) {
            return promiseToCheckUnApprovedTransaction(result);
        }).then(function(result) {
            return promiseToCheckIfAlreadyClosed(result);
        }).then(function (result) {
            getCustomerInfobyAccountNo(result);
        });
    });

    $('#submit').click(function () {

        var isAllValid = true;

        if ($('#AccountNumber').val().trim() === '') {
            $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
        }

        if ($('#TransactionDate').val().trim() === '') {
            $('#TransactionDate').parent().next().find('span').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#TransactionDate').parent().next().find('span').css('visibility', 'hidden');
        }
        if ($('#ChartOfAccount').val().trim() === '0') {
            $('#ChartOfAccount').parent().next().find('span').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').parent().next().find('span').css('visibility', 'hidden');
        }

        if (isAllValid) {
            var message = "Your current balance is " + $('#Balance').val() + " . Are you sure to close?";
            if (!confirm(message)) {
                return false;
            } else {
                var data = {
                    AccountNumber: $('#AccountNumber').val(),
                    TransactionDate: $('#TransactionDate').val(),
                    OpeningDate: $('#OpeningDate').val(),
                    InterestRate: $('#InterestRate').val(),
                    Amount: $('#Balance').val(),
                    AccountSetupId: $('#AccountSetupId').val(),
                    CustomerId: $('#CustomerId').val(),
                    CustomerName: $('#CustomerName').val(),
                    COAId: $('#ChartOfAccount').val()
                }

                $.ajax({
                    url: window.applicationBaseUrl + "Transaction/ClosingSavings/Index",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.status === true) {
                            alert('Successfully done.');
                            $("#AccountNumber").val('');
                            $("#TransactionType").val('');
                            $("#TransactionDate").val('');
                            $("#InterestRate").val('');
                            $("#Amount").val('');
                            $('#CustomerId').val('');
                            $('#AccountSetupId').val('');
                            $('#CustomerName').val('');
                            $("#Balance").val('');
                            $("#OpeningDate").val('');
                            $("#ChartOfAccount").val('');
                        }
                        else {
                            alert('Failed');
                        }
                        $('#submit').val('Save');
                        window.location.reload();
                    },
                    error: function () {
                        alert('Error. Please try again.');
                        $('#submit').val('Save');
                    }
                });
            }
        }
    });

    //convert datetime
});

function formateDate(jsonDate) {
    var months = [
           'January', 'February', 'March', 'April', 'May',
           'June', 'July', 'August', 'September',
           'October', 'November', 'December'
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = months[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '-' + mm + '-' + yyyy;
    return formmatedDate;
}

var promiseToCheckLoanTable = function (accNo) {
    return new Promise(function (resolve, reject) {
        var json = { id: accNo };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/ClosingSavings/ExistLoanAccountNumber",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $("#submit").removeAttr('disabled');
                if (data === false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account will not be closed for having Loan.');
                    reject();
                } else {
                    resolve(accNo);
                }
            }
        });
    });
}

var promiseToCheckUnApprovedTransaction = function (accNo) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/ClosingSavings/GetUnApprovedTransactionByAcNo",
            contentType: "application/json",
            data: JSON.stringify({ id: accNo }),
            success: function (data) {
                if (data === false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
                    reject();
                } else {
                    resolve(accNo);
                }
            }
        });
    });
}

var promiseToCheckIfAlreadyClosed = function (accNo) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/ClosingSavings/GetCloseAccountTransaction",
            contentType: "application/json",
            data: JSON.stringify({ id: accNo }),
            success: function (data) {
                if (data === false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account was closed !!!');
                    reject();
                } else {
                    resolve(accNo);
                }
            }
        });
    });
}

var getCustomerInfobyAccountNo = function (accNo) {
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Transaction/ClosingSavings/GetCustomerInfoByAccountNumber",
        contentType: "application/json",
        data: JSON.stringify({ id: accNo }),
        success: function (result) {
            $('#OpeningDate').val(result[0].OpeningDate == null ? '' : formateDate(result[0].OpeningDate));
            $('#CustomerName').val(result[0].CustomerName);
            $('#AccountTypeName').val(result[0].AccountTypeName);
            $('#Balance').val(result[0].Balance);
            $('#AccountSetupId').val(result[0].AccountSetupId);
            $('#InterestRate').val(result[0].InterestRate);
            $('#CustomerId').val(result[0].CustomerId);
            $('#CustomerImage').attr('src', result[0].CustomerImage);
        }
    });
}

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        return false;
    }
    else if (charCode === 13) {
        return false;
    }
    //status = "";
    return true;
}