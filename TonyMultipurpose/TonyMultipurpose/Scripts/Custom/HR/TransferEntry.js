﻿(function () {

    $('#submit1').hide();
    transferRecordTable();
    var oTable;

    function transferRecordTable() {
        oTable = $('#TransferRecordTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/Save",//'@Url.Action("Save", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpId" },
                { "data": "EmployeeName" },
                { "data": "FromBranch" },
                { "data": "ToBranch" },
                { "data": "Reason" },
                {
                    "data": "TransferDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                }
            ],
            "order": [[0, "desc"]]
        });
        $('#TransferRecordTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        document.getElementById("EmpId").readOnly = true;
        $('#EmpId').val(data.EmpId);
        $('#BranchCode').val(data.BranchCode);
        var date = new Date(data.TransferDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var transferDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#TransferDate').val(transferDate);
        $('#Reason').val(data.Reason);
        $("#EmpId").change();
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    $(function () {
        $("#BranchCode").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetBranchInfo",//'@Url.Action("GetBranchInfo", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
                $("#BranchCode").append('<option value="0">--Select Center--</option>');
                $.each(data, function (key, value) {
                    $("#BranchCode").append('<option value=' + value.BranchCode + '>' + value.BranchName + '</option>');
                });
            }
        });
    });

    $('#submit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpId: $('#EmpId').val(),
                BranchCode: $('#BranchCode option:Selected').val(),
                TransferDate: $('#TransferDate').val(),
                Reason: $('#Reason').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/Save",//'@Url.Action("Save", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#BranchCode').val('0');
                        $('#TransferDate').val('');
                        $('#Reason').val('');
                        $('#EmpId').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });

    $('#submit1').click(function () {
        var isValid = checkValidation;
        if (isValid) {
            var data = {
                EmpId: $('#EmpId').val(),
                BranchCode: $('#BranchCode option:Selected').val(),
                TransferDate: $('#TransferDate').val(),
                Reason: $('#Reason').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/Update",//'@Url.Action("Update", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#BranchCode').val('0');
                        $('#TransferDate').val('');
                        $('#Reason').val('');
                        $('#EmpId').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {

                }
            });
        }
    });

    $("#EmpId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpId").change(function () {
        $('#EmployeeName').empty();
        var empId = $("#EmpId").val();
        var json = { EmpId: empId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('EmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('Designation').innerHTML = data.LastName;
                document.getElementById('Center').innerHTML = data.FirstName;
                document.getElementById('NID').innerHTML = data.NID;
            }
        });
    });

    function checkValidation() {
        var isValid = true;
        if ($('#EmpId').val().trim() === '') {
            isValid = false;
            $('#EmpId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#BranchCode').val().trim() === '0') {
            isValid = false;
            $('#BranchCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#BranchCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#TransferDate').val().trim() === '') {
            isValid = false;
            $('#TransferDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#TransferDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });

    $('#TransferDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }
})();
