﻿namespace TonyMultipurpose.Areas.Accounts.ViewModels
{
    public class AccountCountViewModel
    {
        public int LiveSavingsWI { get; set; }
        public int ClosedSavingsWI { get; set; }
        public int LiveFD { get; set; }
        public int ClosedFD { get; set; }
        public int LiveDB { get; set; }
        public int ClosedDB { get; set; }
        public int LiveMB { get; set; }
        public int ClosedMB { get; set; }
        public int LiveCSS { get; set; }
        public int ClosedCSS { get; set; }
        public int LiveSavingsWithoutI { get; set; }
        public int ClosedSavingsWithoutI { get; set; }

    }
}