﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class FinancialTransactionsModel:CommonModel
    {
        public int Id { get; set; }
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public long VoucherNo { get; set; }
        public string VoucherType { get; set; }
        public string VoucherCode { get; set; }
        public string EntryType { get; set; }
        public string Particular { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public bool IsAdvancePayment { get; set; }
        public bool IsChequePayment { get; set; }
        public bool IsCleared { get; set; }
        public string ChequeNo { get; set; }
        public string IssueDate { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string BankAccountNo { get; set; }
        public string ClientName { get; set; }
        public string ClientCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public int COAId { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public DateTime ChequeIssueDate { get; set; }

        public string EntryPosition { get; set; }
    }
}