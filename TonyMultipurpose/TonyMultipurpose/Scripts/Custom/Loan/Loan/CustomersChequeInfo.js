$(function () {
    document.getElementById('showMessage').innerHTML = '';
});

$(function () {
    $('#UpdateChequeInfo').hide();
    $('#deleteChequeInfo').hide();
    var oChequeInfoTable;
    LoadChequeInfoTable();
    function LoadChequeInfoTable() {
        oChequeInfoTable = $('#ChequeInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetAllLoanCustomerChequeInfo",
                "type": "get",
                "data": {
                    "LoanId": $('#ChequeInfo_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "className": "dt-center"
                },
                { "data": "AccountName" },
                { "data": "AccountNo" },
                { "data": "ChequeNo" },
                { "data": "BankName" }
            ],
            "order": [[0, "asc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#ChequeInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oChequeInfoTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataChequeInfo(oChequeInfoTable.row(this).data());
        });
    }

    oChequeInfoTable.on('order.dt search.dt', function () {
        oChequeInfoTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    function bindDataChequeInfo(data) {
        $('#hdfLoanCustomerChequeInfoId').val(data.LoanCustomerChequeInfoId);
        $('#ChequeInfo_AccountName').val(data.AccountName);
        $('#ChequeInfo_AccountNo').val(data.AccountNo);
        $('#ChequeInfo_ChequeNo').val(data.ChequeNo);
        $('#ChequeInfo_BankName').val(data.BankName);
        $('#saveChequeInfo').show();
        //$('#UpdateChequeInfo').show();
        $('#deleteChequeInfo').show();
    }
    // caler function
    $('#ChequeInfo_Clear').click(function () {
        ChequeInfoClearAll();
    });
    function ChequeInfoClearAll() {

        $('#ChequeInfo_AccountName').val('');
        $('#ChequeInfo_AccountNo').val('');
        $('#ChequeInfo_ChequeNo').val('');
        $('#ChequeInfo_BankName').val('');
        $('#saveChequeInfo').show();
        $('#deleteChequeInfo').hide();
        //$('#UpdateChequeInfo').hide();
        document.getElementById('showMessage').innerHTML = '';
    }

    $('#saveChequeInfo').click(function () {
        var isAllDataValid = checkValidation();

        if (isAllDataValid) {
            var data = bindData();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/CustomersChequeInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oChequeInfoTable.ajax.reload();
                        $('#saveChequeInfo').show();
                        $('#deleteChequeInfo').hide();
                        //$('#UpdateChequeInfo').hide();
                        document.getElementById('showMessage').style.color = "green";
                        ChequeInfoClearAll();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });

    function bindData() {
        var data = {
            LoanId: $('#ChequeInfo_LoanId').val(),
            LoanCustomerChequeInfoId: $('#hdfLoanCustomerChequeInfoId').val(),
            AccountName: $('#ChequeInfo_AccountName').val(),
            AccountNo: $('#ChequeInfo_AccountNo').val(),
            ChequeNo: $('#ChequeInfo_ChequeNo').val(),
            BankName: $('#ChequeInfo_BankName').val()
        }
        return data;
    }

    $('#deleteChequeInfo').click(function () {
        if (confirm("Are you sure?")) {
            var data = {
                LoanCustomerChequeInfoId: $('#hdfLoanCustomerChequeInfoId').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/DeleteLoanCustomerChequeInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oChequeInfoTable.ajax.reload();
                        ChequeInfoClearAll();
                        $('#saveChequeInfo').show();
                        $('#ChequeInfo_Clear').show();
                        $('#deleteChequeInfo').hide();
                        $('#UpdateChequeInfo').hide();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
        return false;
    });


    function checkValidation() {
        var isValid = true;
        if ($('#ChequeInfo_LoanId').val().trim() === '') {
            isValid = false;
            $('#ChequeInfo_LoanId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ChequeInfo_LoanId').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#ChequeInfo_AccountName').val().trim() === '') {
            isValid = false;
            $('#ChequeInfo_AccountName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ChequeInfo_AccountName').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#ChequeInfo_AccountNo').val().trim() === '') {
            isValid = false;
            $('#ChequeInfo_AccountNo').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ChequeInfo_AccountNo').parent().prev().find('span').css('visibility', 'hidden');
        }


        if ($('#ChequeInfo_ChequeNo').val().trim() === '') {
            isValid = false;
            $('#ChequeInfo_ChequeNo').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ChequeInfo_ChequeNo').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#ChequeInfo_BankName').val().trim() === '') {
            isValid = false;
            $('#ChequeInfo_BankName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#ChequeInfo_BankName').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }
});