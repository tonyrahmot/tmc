(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });
    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetrejectLoanList",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "AccountTypeName" },
                { "data": "LoanDate" },
                { "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                { "data": "InterestRate" },
                { "data": "InstallmentAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" },
                {
                    "data": "LoanId",
                    "width": "50px",
                    "render": function (data) {
                        return '<a class="btn btn-danger" onclick="confirmToSubmit(\'' + data + '\')">Reopen</a>';
                    }
                }
            ],
            "order": [[0, "desc"]],
        });
    }


})();

function confirmToSubmit(loanId) {
    var isConfirmed = confirm('Are you sure?');
    if (isConfirmed) {
        window.location.href = window.applicationBaseUrl + "Loan/Loan/ReopenLoan?loanId=" + loanId;
    }
}