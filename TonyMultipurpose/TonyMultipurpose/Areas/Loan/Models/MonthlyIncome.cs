﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class MonthlyIncome
    {
        public long MonthlyIncomeId { get; set; }

        public string LoanId { get; set; }

        public string IncomeType { get; set; }

        public decimal Amount { get; set; }

        public string SpouseIncomeType { get; set; }

        public decimal ForeignRemitAmount { get; set; }

        public decimal OtherIncomeAmount { get; set; }

        public decimal SpouseAmount { get; set; }

        public short? CreatedBy { get; set; }
        //public long MonthlyIncomeId { get; set; }
        //public string LoanId { get; set; }
        //[Required (ErrorMessage ="Income Type is Required")]
        public byte IncomeTypeId { get; set; }
        //public string IncomeType { get; set; }
        //public string Agriculture { get; set; }
        //public string Spouse { get; set; }
        //public decimal AgricultureAmount { get; set; }
        //public decimal SpouseAmount { get; set; }
        //public decimal OtherIncome { get; set; }
        //public decimal ForeignRemittance { get; set; }
        //public decimal Amount { get; set; }
        public decimal TotalIncome { get; set; }

    }
}