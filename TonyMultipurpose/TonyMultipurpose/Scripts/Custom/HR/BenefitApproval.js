﻿(function () {
    var oTable;
    $(document).ready(function () {
        empBenefitTable();
    });

    function empBenefitTable() {
        oTable = $('#EmpBenefitTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/BenefitUnApprovaList",//'@Url.Action("BenefitUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpId" },
                { "data": "EmployeeName" },
                { "data": "BenefitTypeName" },
                { "data": "BenefitAmount" },
                {
                    "data": "BenefitDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "Description" },
                {
                    "data": "EmpId",
                    "render": function (data) {
                        return '<a class="btn btn-primary"  href="' + window.applicationBaseUrl + 'HR/HR/EmpBenefitApproval?id=' + data + '">Approve</a>';//href="@Url.Action("EmpBenefitApproval", "HR", new { Area = "HR" })?id=' + data + '">Approve</a>';
                    }
                }
            ],
            ordering: false,
            sorting: false
        });
    }

    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();