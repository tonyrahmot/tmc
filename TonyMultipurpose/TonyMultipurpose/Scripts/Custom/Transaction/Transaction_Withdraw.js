﻿$(function () {
    $('#btnWithdrawAmount').hide();
    $('#WithdrawForm').show();
    $('#WithdrawForm1').show();
    $('#ContinuousLoanForm').hide();
    $('#divBankAccountNo').hide();
    $('#divBankName').hide();
    $('#divChequeNo').hide();
});

$('#TT').change(function () {
    var v = $('#TT').val();
    if (v === 'Withdraw') {
        $('#ContinuousLoanForm').hide();
        $('#WithdrawForm').show();
        $('#submit').show();
        $('#btnWithdrawAmount').hide();
        $('#WithdrawForm1').show();
    } else {
        $('#ContinuousLoanForm').show();
        $('#btnWithdrawAmount').show();
        $('#submit').hide();
        $('#WithdrawForm').hide();
        $('#WithdrawForm1').show();
    }
});

$(document).ready(function () {

    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/Transaction/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#AccountNumber").change(function () {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetCustomerInfoByAccountNumberForWithdraw",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (result) {
                closeAccountTransaction();
                //  console.log(result[0]);
                $('#AccountInfo').show();
                $('#LoanInfo').hide();
                document.getElementById('CustomerName').innerHTML = result[0].CustomerName;
                document.getElementById('AccountTypeName').innerHTML = result[0].AccountTypeName;
                document.getElementById('Balance').innerHTML = result[0].Balance;
                //$('#CustomerName').val(result[0].CustomerName);
                //$("#CustomerName").attr("readonly", "readonly");
                //$('#AccountTypeName').val(result[0].AccountTypeName);
                //$("#AccountTypeName").attr("readonly", "readonly");
                //$('#Balance').val(result[0].Balance);
                //$("#Balance").attr("readonly", "readonly");
                $('#AccountSetupId').val(result[0].AccountSetupId);
                $('#InterestRate').val(result[0].InterestRate);
                $('#CustomerImage').attr('src', result[0].CustomerImage);

            }
        });

    });

    function unApprovedTransaction() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetUnApprovedTransactionByAcNo",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $("#submit").removeAttr('disabled');
                if (data === false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
                }
                closeAccountTransaction();
            }
        });
    }

    function closeAccountTransaction() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetCloseAccountTransaction",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account was closed !!!!');
                }
            }
        });
    }

    $(function () {
        $("#ChartOfAccount").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });

    $(function () {
        $("#ChartOfAccountForConLoan").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccountForConLoan").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccountForConLoan").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });

    $('#ChartOfAccount').change(function () {
        var coaId = $("#ChartOfAccount").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetAccountCodeInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('ABalance').innerHTML = data.Balance;
                if (data.IsBankAccount === true) {
                    $('#IsCashEntry').attr('checked', false);
                    $('#divBankAccountNo').show();
                    $('#divBankName').show();
                    $('#divChequeNo').show();
                    $('#BankName').val(data.BankName);
                    $('#BankAccountNo').val(data.BankAccountNo);
                } else {
                    $('#IsCashEntry').attr('checked', true);
                    $('#divBankAccountNo').hide();
                    $('#divBankName').hide();
                    $('#divChequeNo').hide();
                    $('#BankName').val('');
                    $('#BankAccountNo').val('');
                }
            }
        });
    });

    //Continuous Loan Transaction Area Start

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#ConTransactionDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    $("#LoanId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/ContinuousLoan/AutoCompleteLoanId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    if (data.length == 0) {
                        alert("No Account Available for Transaction");
                    } else {
                        response($.map(data, function (item) {
                            return { label: item, value: item };
                        }));
                    }
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#LoanId").change(function () {
        var LoanId = $("#LoanId").val();
        var json = { LoanId: LoanId };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/ContinuousLoan/GetContinuousLoanInfo",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (result) {
                $('#AccountInfo').hide();
                $('#LoanInfo').show();
                $('#hdfCustomerId').val(result.CustomerId);
                document.getElementById('LabelCustomerName').innerHTML = result.CustomerName;
                $('#CustomerImage').attr('src', result.CustomerImage);
                //$('#CustomerImage').attr('src', result[0].CustomerImage);
                document.getElementById('LabelFatherName').innerHTML = result.FatherName;
                document.getElementById('LabelAvailableBalance').innerHTML = result.AvailableBalance;
                document.getElementById('LabelOutstanding').innerHTML = result.Outstanding;
                $('#hdfAvailableBalance').val(result.AvailableBalance);
                $('#hdfOutstanding').val(result.Outstanding);
                document.getElementById('LabelCustomerLoanLimit').innerHTML = result.CustomerLoanLimit;
            }
        });
    });

    $('#btnWithdrawAmount').click(function () {

        var isAllValid = true;
        if ($('#LoanId').val().trim() === '') {
            $('#LoanId').parent().next().children('span.error').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#LoanId').parent().next().children('span.error').css('visibility', 'hidden');
        }

        if ($('#TransactionDate').val().trim() === '') {
            $('#TransactionDate').parent().next().children('span.error').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#TransactionDate').parent().next().children('span.error').css('visibility', 'hidden');
        }
        if ($('#Amount').val().trim() === '') {
            $('#Amount').parent().next().children('span.error').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#Amount').parent().next().children('span.error').css('visibility', 'hidden');
        }
        if ($('#ChartOfAccount').val().trim() === '0') {
            $('#ChartOfAccount').parent().next().children('span.error').css('visibility', 'visible');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').parent().next().children('span.error').css('visibility', 'hidden');
        }

        var availableBalance = parseFloat($("#ABalance").text());
        var withdrawAmount = parseFloat($('#Amount').val().replace(/,/g, ''));
        var customerAvailableBalance = $("#LabelAvailableBalance").text() !== '0' ? parseFloat($("#LabelAvailableBalance").text()) : 0;
        if (isAllValid) {
            if (withdrawAmount > availableBalance) {
                alert('InSufficient Balance This Head!!!');
            }
            else if (withdrawAmount > customerAvailableBalance) {
                alert('Not Enough Balance.');
            }
            else {
                var data = {
                    LoanId: $('#LoanId').val(),
                    CustomerId: $("#hdfCustomerId").val(),
                    AvailableBalance: $("#hdfAvailableBalance").val(),
                    Outstanding: $("#hdfOutstanding").val(),
                    TransactionDate: $('#TransactionDate').val(),
                    Particular: $('#Particular').val(),
                    WithdrawAmount: parseFloat($('#Amount').val().replace(/,/g, '')),
                    IsChequeEntry: $("#IsCashEntry").prop('checked'),
                    BankName: $('#BankName').val(),
                    BankAccountNo: $('#BankAccount').val(),
                    ChequeNo: $('#ChequeNo').val(),
                    COAId: $('#ChartOfAccount option:selected').val(),
                    VoucherNo: $('#VoucherNo').val()
                }
                $.ajax({
                    url: window.applicationBaseUrl + "Loan/ContinuousLoan/SaveContinuousLoanWithdraw",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.oCommonResult.Status === true) {
                            $("#LoanId").val('');
                            //$("#TransactionDate").val('');
                            $('#VoucherNo').val('');
                            $("#TT").val('');
                            $("#Particular").val('');
                            $("#BankName").val('');
                            $("#BankAccount").val('');
                            $("#ChequeNo").val('');
                            $("#Amount").val('');
                            $("#hdfAvailableBalance").val('');
                            $('#ChartOfAccount').val('0');
                            $("#ABalance").text('');
                            $('#CustomerImage').attr('src', '');
                            document.getElementById('LabelCustomerLoanLimit').innerHTML = '';
                            document.getElementById('LabelCustomerName').innerHTML = '';
                            document.getElementById('LabelFatherName').innerHTML = '';
                            document.getElementById('LabelAvailableBalance').innerHTML = '';
                            document.getElementById('LabelOutstanding').innerHTML = '';
                            $('#LoanInfo').hide();
                            //document.getElementById('showMessage').style.color = "green";
                            document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                        }
                        else {
                            document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                        }

                    },
                    error: function () {
                        document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    }
                });
            }
        }
    });
});
//Continuous Loan Transaction End


//Submit Transaction Info



$('#submit').click(function () {

    var isAllValid = true;

    if ($('#AccountNumber').val().trim() === '') {
        //$('#AccountNumber').siblings('span.error').css('visibility', 'visible');
        $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#AccountNumber').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#TT').val().trim() === 0) {
        $('#TT').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#TT').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#ChartOfAccount').val().trim() === '0') {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'hidden');
    }

    if ($('#Amount').val().trim() === '') {
        $('#Amount').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#Amount').parent().next().find('span').css('visibility', 'hidden');
    }
    if (parseFloat($('#Amount').val().replace(/,/g, '')) > parseFloat($('#ABalance').text())) {
        isAllValid = false;
        alert('InSufficient Balance This Head!!!');
    }

    var transactionType = $("#TT option:selected").text();
    var amount = parseFloat($('#Amount').val().replace(/,/g, ''));
    var currentBalance = parseFloat($('#Balance').text());
    if (transactionType === 'Withdraw' && (currentBalance - 100) < amount) {
        isAllValid = false;

    }
    if ($('#TransactionDate').val().trim() === '') {
        $('#TransactionDate').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#TransactionDate').parent().next().find('span').css('visibility', 'hidden');
    }

    if (isAllValid) {

        var data = {
            AccountNumber: $('#AccountNumber').val(),
            TransactionType: $('#TT option:selected').val(),
            TransactionDate: $('#TransactionDate').val(),
            InterestRate: $('#InterestRate').val(),
            Amount: parseFloat($('#Amount').val().replace(/,/g, '')),
            AccountSetupId: $('#AccountSetupId').val(),
            IsCashEntry: $("#IsCashEntry").prop('checked'),
            BankName: $('#BankName').val(),
            BankAccountNo: $('#BankAccountNo').val(),
            ChequeNo: $('#ChequeNo').val(),
            //COAId:$('#ChartOfAccount').val()
            COAId: $('#ChartOfAccount option:selected').val(),
            VoucherNo: $('#VoucherNo').val(),
            Particular: $('#Particular').val()
        }
        $.ajax({
            url: window.applicationBaseUrl + "Transaction/Transaction/Index",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status === true) {
                    alert('Successfully done.');
                    $("#AccountNumber").val('');
                    $("#TransactionType").val('');
                    $("#TransactionDate").val('');
                    $("#InterestRate").val('');
                    $("#Amount").val('');
                    $('#CustomerId').val('');
                    $('#AccountSetupId').val('');
                    $('#CustomerName').val('');
                    $("#Balance").val('');
                    $('#ChartOfAccount').val('0');
                }
                else {
                    alert('Failed');
                }
                $('#submit').val('Save');
                window.location.reload();

            },
            error: function () {
                alert('Error. Please try again.');
                $('#submit').val('Save');
            }
        });
    }
});




// --- allow only numbers in amount text box
$('#Amount').on('keyup', function (event) {
    var value = $('#Amount').val();

    if (event.which >= 37 && event.which <= 40) {
        event.preventDefault();
    }
    var newvalue = value.replace(/,/g, '');
    var valuewithcomma = Number(newvalue).toLocaleString('en');
    $('#Amount').val(valuewithcomma);

});

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        return false;
    } else if (charCode === 13) {
        return false;
    }
    status = "";
    return true;
}

//using comma seperation in amount
//$(document).on('keyup', '#Amount', function () {
//    var x = $(this).val();
//    $(this).val(x.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//});


//---checking current balance and withdraw amount

$("#Amount").keyup(function () {
    var currentBalance = parseFloat($('#Balance').text());

    var transactionType = $("#TT option:selected").val();

    var status = $("#MessageForExistChek"); //DIV object to display the status message
    var ttDropdownValue = $.trim(transactionType);
    if (ttDropdownValue === 'Withdraw') {
        var amount = parseFloat($('#Amount').val().replace(/,/g, ''));

        if ((currentBalance - 100) >= amount) {

            status.html("<font color=green><b>" + Number(currentBalance - amount - 100).toLocaleString('en') + "</b>TK. is Available!</font>");
        } else {
            //status.html("<font color=red>'<b>" + amount + "</b>'TK. your balance is not sufficient.! and you have to keep minimum 100TK in your account.</font>");
            status.html("<font color=red>Maxiumum <b>" + Number(currentBalance - 100).toLocaleString('en') + "</b> TK. can be withdrawn </font>" +
                "<br> <font color=red>**Note:You have to keep minimum 100TK in your account.</font>");
            //$("#submit").attr("disabled", "disabled");
        }
    }

});
$("#Amount").keyup(function () {
    var amount = parseFloat($('#Amount').val().replace(/,/g, '')); //parseFloat($('#Amount').val())
    var status = $("#MessageForNumberToWordConvert"); //DIV object to display the status message
    var user = $.trim(amount);
    if (user.length >= 1) {
        status.html("Checking....") //While our Thread works, we will show some message to indicate the progress
        //jQuery AJAX Post request
        $.post(window.applicationBaseUrl + "Transaction/Transaction/GetConvertNumberToWord", { number: amount },
            function (data) {

                status.html("<font color=green>'<b>" + data + "</b>' TK Only</font>");

            });
    } else {
        status.html("Need more characters...");
    }
});