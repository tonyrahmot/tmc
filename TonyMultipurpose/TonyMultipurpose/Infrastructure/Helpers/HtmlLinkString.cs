﻿using TonyMultipurpose.Enums;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlLinkString
    {
        public static string GetIconLink(string linkText, EnumOperationName operationName, string navigationLink = "",
            string clickEvent = "", string customClassName = "", bool isTargetBlank = false)
        {
            string tooltipClass;
            string tooltipTitle;
            string iconName;
            string iconColor;

            switch (operationName)
            {

                case EnumOperationName.Edit:
                    tooltipClass = "success";
                    tooltipTitle = "Edit";
                    iconName = "pencil";
                    iconColor = "green";
                    break;
                case EnumOperationName.Details:
                    tooltipClass = "info";
                    tooltipTitle = "See Details";
                    iconName = "search-plus";
                    iconColor = "blue";
                    break;
                case EnumOperationName.Delete:
                    tooltipClass = "error";
                    tooltipTitle = "Edit";
                    iconName = "trash";
                    iconColor = "red";
                    break;
                case EnumOperationName.Printable:
                    tooltipClass = "info";
                    tooltipTitle = "View Printable";
                    iconName = "print";
                    iconColor = "blue";
                    break;
                default:
                    tooltipClass = "info";
                    tooltipTitle = "Add";
                    iconName = "plus";
                    iconColor = "blue";
                    break;
            }

            linkText = string.IsNullOrWhiteSpace(linkText) ? "" : linkText + " ";

            if (string.IsNullOrWhiteSpace(navigationLink))
            {
                navigationLink = "javascript:void(0);";
            }

            if (!string.IsNullOrWhiteSpace(clickEvent))
            {
                clickEvent = "onclick=" + clickEvent;
            }
            var className = string.IsNullOrWhiteSpace(customClassName) ? "" : " " + customClassName;
            var iconLink = "<a " +
                           $"class=\"tooltip-{tooltipClass}{className}\" data-rel=\"tooltip\" " +
                           $"data-placement=\"top\" href=\"{navigationLink}\" " +
                           $"title=\"{tooltipTitle}\" ";

            if (isTargetBlank)
            {
                iconLink += "target=\"_blank\" ";
            }

            iconLink += $"{clickEvent}>" +
                        linkText +
                        $"<i class=\"ace-icon fa fa-{iconName} {iconColor}\"></i>" +
                        "</a>";

            return iconLink;
        }
    }
}