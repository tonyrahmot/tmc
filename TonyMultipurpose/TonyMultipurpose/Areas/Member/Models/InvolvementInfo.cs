﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class InvolvementInfo:CommonModel
    {
        [Key]
        public byte InvolvementId { get; set; }


        [Required]
        [Display(Name = "Involvement Type")]
        public string InvolvementType { get; set; }
    }
}