﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class MonthlyExpenseViewModel
    {
        public long MonthlyExpenseId { get; set; }

        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }

        [Display(Name = "Family Expense")]
        public decimal FamilyExpense { get; set; }

        [Display(Name = "Existing EMI")]
        public decimal ExistingEMI { get; set; }

        [Display(Name = "Children Education")]
        public decimal ChildrenEducation { get; set; }

        [Display(Name = "Others Expense")]
        public decimal OthersExpense { get; set; }
        public short CreatedBy { get; set; }

        [Display(Name = "Total")]
        public decimal TotalExpense { get; set; }
        public IEnumerable<SelectListItem> IncomeTypeItems { get; set; }

    }
}