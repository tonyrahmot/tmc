﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.ViewModels
{
    public class AccountsViewModel : CommonModel
    {
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string BranchName { get; set; }
        public string OpeningDate { get; set; }
        public string CustomerImage { get; set; }
        public string OldImage { get; set; }
        public decimal DepositAmount { get; set; }
        public decimal AccountBalance { get; set; }

        public string TransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public decimal? InterestRate { get; set; }
        public string Mobile { get; set; }
        public decimal? Amount { get; set; }
        public decimal? InstallmentAmount { get; set; }
        public string InstallmentMonth { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? InstallmentYear { get; set; }
        public int? Duration { get; set; }
        public decimal? MatureAmount { get; set; }
        public decimal? Fine { get; set; }
        public DateTime? MatureDate { get; set; }

        public string Username { get; set; }
        public int AccountSetupId { get; set; }

        public string Productname { get; set; }
        public int TotalUnapproved { get; set; }
        public int AccountTypeId { get; set; }
    }
}