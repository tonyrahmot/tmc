﻿using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    //created by ataur
    public class DesignationController : Controller
    {
        // GET: Employee/Designation
        public ActionResult Index()
        {
            DesignationBLL objDesignationBll = new DesignationBLL();
            List<Designation> designationList = objDesignationBll.GetAllDesignation();
            return View(designationList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Designation objDesignation)
        {
            if (ModelState.IsValid)
            {
                DesignationBLL objDesignationBll = new DesignationBLL();
                objDesignationBll.CreateDesignation(objDesignation);
            }
            return RedirectToAction("Index", "Designation");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            DesignationBLL objDesignationBll = new DesignationBLL();
            var designation = objDesignationBll.GetAllDesignation(id);
            return View(designation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Designation objDesignation)
        {
            if (ModelState.IsValid)
            {
                DesignationBLL objDesignationBll = new DesignationBLL();
                objDesignationBll.UpadateDesignation(objDesignation);
            }
            return RedirectToAction("Index", "Designation");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            DesignationBLL objDesignationBll = new DesignationBLL();
            var designation = objDesignationBll.GetAllDesignation(id);
            return View(designation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteDesignation(int id)
        {
            if (ModelState.IsValid)
            {
                DesignationBLL objDesignationBll = new DesignationBLL();
                objDesignationBll.DeleteDesignation(id);
            }
            return RedirectToAction("Index", "Designation");

        }
    }
}