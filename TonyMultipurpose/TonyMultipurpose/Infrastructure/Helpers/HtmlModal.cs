﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlModal : IDisposable
    {
        private readonly ViewContext _viewContext;

        public HtmlModal(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public void Dispose()
        {
            _viewContext.Writer.Write(
                "</div>" +
                "</div>" +
                "</div>"
            );
        }
    }
}