﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TonyMultipurpose.Areas.Customer.BLL;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Customer.Controllers
{
    [AuthenticationFilter]
    public class ApprovalController : Controller
    {
        private CustomerBLL objCustomerBll = new CustomerBLL();
        public ActionResult Index()
        {
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = false;
            customer.IsDeleted = false;
            List<CustomerModel> customerInfo = objCustomerBll.GetAllCustomerInfo(customer);
            return View(customerInfo);
        }

        public ActionResult Detail(string id)
        {
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = false;
            customer.IsDeleted = false;
            customer = objCustomerBll.GetAllCustomerInfo(customer).Where(a => a.CustomerId == id).FirstOrDefault();
            var customerBankDetails = objCustomerBll.GetCustomerWiseBankInfo(id);
            if (customerBankDetails != null)
            {
                foreach (var customerBank in customerBankDetails)
                {
                    customer.CustomerBankDetails.Add(customerBank);
                }
            }
            return View(customer);
        }

        [HttpPost]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("CustomerId");
            if(ch != null)
            {
                objCustomerBll.ApproveCustomer(ch);
            }
            
            return RedirectToAction("Index");
        }
        
        public JsonResult GetNIDCheck(string NID)
        {
            bool exsits = objCustomerBll.GetNIDCheck(NID);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public ActionResult Delete(int id)
        {
            objCustomerBll.DeleteCustomer(id);
            return RedirectToAction("Index");
        }
    }
}