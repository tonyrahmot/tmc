﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    // created by shovon
    public class DocumentType : CommonModel
    {
        [Key]
        public byte DocumentTypeId { get; set; }

        [Required]
        [Display(Name = "Type Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Type Name should between at least 2 to 100 characters long.")]
        public string TypeName { get; set; }
    }
}