﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class SalaryModel
    {
        public decimal BasicSalary { get; set; }
        public decimal HouseRent { get; set; }
        public decimal Medical { get; set; }
        public decimal Others { get; set; }
        public string EmployeeName { get; set; }
        public decimal CurrentSalary { get; set; }
        public decimal UpdatedSalary { get; set; }
        public string EfficientDate { get; set; }
        public string EmpCode { get; set; }
        public decimal DA { get; set; }
        public decimal HRA { get; set; }
        public decimal Convence { get; set; }

    }
}