﻿$(document).ready(function () {
   
    $('.js-example-basic-single').select2();
    $('#checkInfo').hide();
    $('#BankInfo').hide();
    $('#submit').removeAttr('disabled');
    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    sharePurchaseCustomerAutoComplete("CustomerId");
    loadAccountToCode();
    loadShareHoldInfo();
});

$('#IsChequePayment').change(function () {
    if (this.checked) {
        $('#checkInfo').show();
    }
    else {
        $('#checkInfo').hide();
    }
});

$('#IssuedDate').datepicker({
    dateFormat: 'dd/M/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+60"
}).click(function () { $(this).focus(); });

function loadShareHoldInfo() {
  
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Share/SharePurchase/GetCompanyShareInfo",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#hdIssuedQty').val(data[0].IssuedQty);
            $('#hdBalance').val(data[0].Balance);
        }
    });
}
function loadAccountToCode() {
    $("#COAId").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAllIsTransactionalAccountCode",//'@Url.Action("GetAllAccountCode", "FinancialTransaction", new { Area = "FinancialTransactions" })',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#COAId").append('<option value="0">--Select Type--</option>');
            $.each(data, function (key, value) {
                $("#COAId").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
}

$('#COAId').change(function () {
    var coaId = $("#COAId").val();
    var json = {
        coaId: coaId
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetAccountCodeInfo",//'@Url.Action("GetAccountCodeInfo", "FinancialTransaction", new { Area = "FinancialTransactions" })',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            if (data.IsBankAccount === true) {
                $('#BankInfo').show();
                $('#BankName').val(data.BankName);
                $('#BranchName').val(data.BranchName);
                $('#BankAccountNo').val(data.BankAccountNo);
            } else {
                $('#BankInfo').hide();
                $('#BankName').val('');
                $('#BranchName').val('');
                $('#BankAccountNo').val('');
            }
        }
    });
});

$('#Amount').change(function () {
    var IssuedQty = parseFloat($('#hdIssuedQty').val());
    var Balance = parseFloat($('#hdBalance').val());
    var amount = parseFloat($('#Amount').val());
    var CurrentShareQty = parseFloat($('#CurrentShareQty').val());

    if (amount <= 0) {
        $('#submit').attr('disabled', 'disabled');
        alert('Quantity must be greater than zero.');
    }
    else if ((CurrentShareQty + amount) > (IssuedQty * 0.2)) {
        $('#submit').attr('disabled', 'disabled');
        alert('Quantity can not be greater than ' + (IssuedQty * 0.2 - CurrentShareQty));
    }
    else if (amount > Balance) {
        $('#submit').attr('disabled', 'disabled');
        alert('Quantity can not be greater than  remaining issued share of ' + Balance);
    }
    else {
        $('#submit').removeAttr('disabled');
    }

});

function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode === 13 || charCode === 46) {
        return false;
    }
    return true;
}

$('#Amount').keyup(function () {
    var noOfShare = parseInt($('#Amount').val());   
    var aPricePerShare = parseFloat($('#PricePerShare').val());
    if (!isNaN(aPricePerShare)) {
        var totalAmount = parseFloat(aPricePerShare * noOfShare);
        $('#TotalAmount').val(totalAmount);
    }
});

function removeErrorMessage(control) {
    var $this = $(control);
    if ($this.val().trim() === 0) {
        $this.siblings('span.error').css('display', 'block');
    } else {
        $this.siblings('span.error').css('display', 'none');
    }
}

function sharePurchaseCustomerAutoComplete(controlId) {
//    var excludeCustId = $("#" + excludeDataSource).val();
    $("#" + controlId).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + 'Share/SharePurchase/AutoCompleteCustomerId',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        select: function (event, ui) {
            var customerId = ui.item.value;
            $(this).val(customerId);
            loadShareTransferCustomer(customerId); //excludeCustId
            return false;
        }
    });
}

function loadShareTransferCustomer(customerId) {
    var customerUrl = window.applicationBaseUrl + 'Share/SharePurchase/GetCustomerInfo/' + customerId;
    $("#customerInfo").load(customerUrl);
}