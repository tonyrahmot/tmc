﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class AdditionalLoanDetail
    {
        public long AdditionalLoanDetailId { get; set; }

        public string LoanId { get; set; }

        public string InstituteName { get; set; }

        public decimal LoanAmount { get; set; }

        public string AccountNo { get; set; }

        public string AccountName { get; set; }

        public string LoanType { get; set; }

        public decimal? Installment { get; set; }

        public string StartDate { get; set; }

        public string ExpireDate { get; set; }

        public decimal? PresentOutAmount { get; set; }

        public short? CreatedBy { get; set; }
        //[Key]
        //public long AdditionalLoanDetailId { get; set; }
        //public string LoanId { get; set; }
        //public string InstituteName { get; set; }
        //public decimal LoanAmount { get; set; }
        //public decimal? ExistingEmi { get; set; }

    }
}