﻿using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsImage
    {
        public static MvcHtmlString Image(
         this HtmlHelper htmlHelper,
         string src,
         string altText,
         object htmlAttributes = null)
        {

            return Image(htmlHelper, src, altText,
              string.Empty, string.Empty, string.Empty, htmlAttributes);

        }
        public static MvcHtmlString Image(
          this HtmlHelper htmlHelper,
          string src,
          string altText,
          string cssClass,
          object htmlAttributes = null)
        {

            return Image(htmlHelper, src, altText,
              cssClass, string.Empty, string.Empty, htmlAttributes);
        }

        public static MvcHtmlString Image(
          this HtmlHelper htmlHelper,
          string src,
          string altText,
          string cssClass,
          string name,
          string imageId,
          object htmlAttributes = null)
        {
            TagBuilder tb = new TagBuilder("img");

            HtmlExtensionsCommon.AddName(tb, name, imageId);

            tb.MergeAttribute("src", src);
            tb.MergeAttribute("alt", altText);

            if (!string.IsNullOrWhiteSpace(cssClass))
            {
                tb.AddCssClass(cssClass);
            }

            tb.MergeAttributes(
              HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            // HTML Encode the String
            return MvcHtmlString.Create(
              tb.ToString(TagRenderMode.SelfClosing));
        }
    }
}