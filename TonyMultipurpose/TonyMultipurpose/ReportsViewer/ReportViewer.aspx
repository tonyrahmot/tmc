﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="TonyMultipurpose.ReportsViewer.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report Viewer</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
   
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="panel panel-info" style="margin-top: 12px;">
                <div class="panel-heading text-center">
                    <strong>Report Section</strong>
                </div>
                <div class="panel-body">
                    <CR:CrystalReportViewer ID="crpViewer" runat="server" Width="100%" />

                    <div>
                        <div class="text-center footer">
                            Copyright ©2017 <a href="http://juvenilepacersbd.com/" target="_blank">Juvenile PacersBD</a>. All rights reserved.
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
