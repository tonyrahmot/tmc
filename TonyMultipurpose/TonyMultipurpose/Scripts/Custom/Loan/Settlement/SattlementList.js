(function () {
    var oTable;
    var beginningBalance;
    var endingBalance;
    var principle;
    var interest;
    var interestRate;
    var installmentNo;
    var totalInstallment;
    var numberOfInstallment;
    var installmentAmount;
    var totalInterest;

    $(document).ready(function () {
        loadTable();
    });
    function loadTable() {
        oTable = $('#SettlementTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Settlement/GetUnApprovedSettlementList",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                 { "data": "LoanNo" },
                 { "data": "SettlementTypeName" },
                 { "data": "SettlementDate" },
                 { "data": "CurrentInterest" },
                 { "data": "CurrentInstallmentAmount" },
                 { "data": "UpdatedInterest" },
                 { "data": "UpdatedInstallmentAmount" },
                 { "data": "Reason" },
                 { "data": "CreatedName" }
            ],
            "order": [[0, "desc"]],
        });
        //oTable.on('order.dt search.dt', function () {
        //    oTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
        //        cell.innerHTML = i + 1;
        //    });
        //}).draw();
        $('#SettlementTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#hdfSettlementId').val(data.SettlementId);
        $('#LoanNo').val(data.LoanNo);
        $("#LoanNo").change();
        $('#SettlementType').val(data.SettlementTypeId);
        $('#CurrentInterest').val(data.CurrentInterest);
        $('#CurrentInstallmentAmount').val(data.CurrentInstallmentAmount);
        $('#UpdatedInterest').val(data.UpdatedInterest);
        $('#UpdatedInstallmentAmount').val(data.UpdatedInstallmentAmount);
        $('#hdfLoanNo').val(data.LoanNo);
        $('#SettlementDate').val(FormateDate(data.SettlementDate));
        $('#Reason').val(data.Reason);
        $('#btnSave').hide();
        $('#btnUpdate').show();
        $('#btnDelete').show();
        //$('#submit').val('update');
    }


    // load Settlement Type
    $(function () {
        $("#SettlementType").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Settlement/GetSettlementType",// window.applicationBaseUrl + "Loan/Settlement/GetSettlementType",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#SettlementType").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#SettlementType").append('<option value=' + value.SettlementTypeId + '>' + value.SettlementTypeName + '</option>');
                });
            }
        });
    });


    // AutoComplete Customer Id
    $("#LoanNo").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Settlement/AutoCompleteLoanId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    // Get customer Information by loan id
    $("#LoanNo").change(function () {
        $('#SettlementTypeId').empty();
        var LoanNo = $("#LoanNo").val();
        var json = { LoanNo: LoanNo };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Settlement/GetCustomerInfoByLoanId",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#CustomerInfo').show();
                $('#customerImage').attr('src', data.customerInfo.CustomerImage);
                document.getElementById('CustomerName').innerHTML = data.customerInfo.CustomerName;
                document.getElementById('FatherName').innerHTML = data.customerInfo.FatherName;
                document.getElementById('lblLoanType').innerHTML = data.loanInfo.AccountTypeName;
                document.getElementById('LoanAmount').innerHTML = data.customerInfo.LoanAmount;
                document.getElementById('Mobile').innerHTML = data.customerInfo.Mobile;
                $('#CurrentInterest').val(data.loanInfo.InterestRateToDisplay);
                $('#CurrentInstallmentAmount').val(data.loanInfo.InstallmentAmount);
                $('#hdfInstallmentTypeId').val(data.loanInfo.InstallmentTypeId);
                $('#hdfAccountType').val(data.loanInfo.AccountSetupId);
                totalInstallment = data.loanInfo.NumberOfInstallment;
                installmentNo = data.p.InstallmentNo;
                numberOfInstallment = parseFloat(totalInstallment - (installmentNo - 1));
                if (data.info.BeginningBalance == 0) {
                    beginningBalance = parseFloat(data.info.PrincipleAmount);
                } else {
                    beginningBalance = parseFloat(data.info.EndingBalance);
                }


            }
        });
    });

    $("#UpdatedInterest").change(function () {
        var accType = parseInt($("#hdfAccountType").val());
        var updatedInterest = parseFloat($("#UpdatedInterest").val());
        if (accType === 12) {
            updatedInterest = updatedInterest * 2;
        }
        var installmentTypeId = $("#hdfInstallmentTypeId").val();       
        if (installmentTypeId == 2) {
            interest = Math.round(parseFloat((beginningBalance * updatedInterest) / 1200));
            var r = parseFloat(updatedInterest / 1200);
            var a = parseFloat(beginningBalance * r);
            var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
            var c = 1 - b;
            installmentAmount = Math.round(a / c).toFixed(2);
            $('#UpdatedInstallmentAmount').val(installmentAmount);
            principle = parseFloat(installmentAmount - interest);
            endingBalance = parseFloat(beginningBalance - principle);
            principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
            totalInterest = Math.round(parseFloat(principleWithInterest - beginningBalance));

        }
        if (installmentTypeId == 1) {
            interest = Math.round(parseFloat((beginningBalance * updatedInterest) / 5200));
            //var r = parseFloat($('#UpdatedInterest').val()) / 5200;
            var r = parseFloat(updatedInterest / 5200);
            var a = parseFloat(beginningBalance * r);
            var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
            var c = 1 - b;
            installmentAmount = Math.round(a / c).toFixed(2);
            $('#UpdatedInstallmentAmount').val(installmentAmount);
            principle = Math.round(parseFloat(installmentAmount - interest));
            endingBalance = Math.round(parseFloat(beginningBalance - principle));
            principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
            totalInterest = Math.round(parseFloat(principleWithInterest - beginningBalance));
        }
    });


    //save settlement info
    $('#submit').click(function () {
        var isValid = CheckValidation();

        if (isValid) {           
            var updatedInterest = parseFloat($('#UpdatedInterest').val());
            var currentInterest = $('#CurrentInterest').val();
            if (parseInt($('#hdfAccountType').val()) === 12) {
                updatedInterest = updatedInterest * 2;
                currentInterest = currentInterest * 2;
            } 
            var data = {
                SettlementTypeId: $('#SettlementType option:selected').val(),
                SettlementDate: $('#SettlementDate').val(),
                LoanNo: $('#LoanNo').val(),
                Reason: $('#Reason').val(),
                //CurrentInterest: $('#CurrentInterest').val(),
                //UpdatedInterest: $('#UpdatedInterest').val(),
                CurrentInterest: currentInterest,
                UpdatedInterest: updatedInterest,
                CurrentInstallmentAmount: $('#CurrentInstallmentAmount').val(),
                UpdatedInstallmentAmount: $('#UpdatedInstallmentAmount').val()
                //BeginningBalance: beginningBalance,
                //Principle: principle,
                //Interest: interest,
                //EndingBalance: endingBalance,
                //InstallmentNo: installmentNo

            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Settlement/Save",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        $('#SettlementType').val('0');
                        $('#SettlementDate').val('');
                        $('#hdfSettlementId').val('');
                        $('#CurrentInterest').val('');
                        $('#UpdatedInterest').val('');
                        $('#CurrentInstallmentAmount').val('');
                        $('#UpdatedInstallmentAmount').val('');
                        $('#LoanNo').val('');
                        $('#Reason').val('');
                        document.getElementById("Mobile").innerHTML = '';
                        document.getElementById("CustomerName").innerHTML = '';
                        document.getElementById("lblLoanType").innerHTML = '';
                        document.getElementById("LoanAmount").innerHTML = '';
                        document.getElementById("FatherName").innerHTML = '';
                        $('#hdfAccountType').val('');
                        $('#customerImage').attr("src", "");
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#hdfSettlementId').val(data.SettlementId);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanNo').val('');
                }
            });

        }
    });

    //update settlement info
    $('#update').click(function () {
        var isValid = CheckValidation();
        if (isValid) {

            var data = Bindata();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Settlement/Edit",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        $('#SettlementType').val('0');
                        $('#SettlementDate').val('');
                        $('#hdfSettlementId').val('');
                        $('#CurrentInterest').val('');
                        $('#UpdatedInterest').val('');
                        $('#CurrentInstallmentAmount').val('');
                        $('#UpdatedInstallmentAmount').val('');
                        $('#LoanNo').val('');
                        $('#Reason').val('');
                        document.getElementById("Mobile").innerHTML = '';
                        document.getElementById("CustomerName").innerHTML = '';
                        document.getElementById("lblLoanType").innerHTML = '';
                        document.getElementById("LoanAmount").innerHTML = '';
                        document.getElementById("FatherName").innerHTML = '';
                        $('#hdfAccountType').val('');
                        $('#customerImage').attr("src", "");
                        $('#btnSave').show();
                        $('#btnUpdate').hide();
                        $('#btnDelete').hide();

                        oTable.ajax.reload();

                        document.getElementById('showMessage').style.color = "green";

                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';

                }
            });

        }
    });


    //Delete Settlement
    $('#delete').click(function () {
        if (confirm("Are you sure?")) {
            var data = {
                SettlementId: $('#hdfSettlementId').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Settlement/Delete",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        $('#SettlementType').val('0');
                        $('#SettlementDate').val('');
                        $('#hdfSettlementId').val('');
                        $('#CurrentInterest').val('');
                        $('#UpdatedInterest').val('');
                        $('#CurrentInstallmentAmount').val('');
                        $('#UpdatedInstallmentAmount').val('');
                        $('#LoanNo').val('');
                        $('#Reason').val('');
                        document.getElementById("Mobile").innerHTML = '';
                        document.getElementById("CustomerName").innerHTML = '';
                        document.getElementById("lblLoanType").innerHTML = '';
                        document.getElementById("LoanAmount").innerHTML = '';
                        document.getElementById("FatherName").innerHTML = '';
                        $('#hdfAccountType').val('');
                        $('#customerImage').attr("src", "");
                        $('#btnSave').show();
                        $('#btnUpdate').hide();
                        $('#btnDelete').hide();

                        oTable.ajax.reload();
                        //$('#submit').val('Save');

                        document.getElementById('showMessage').style.color = "green";

                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';

                }
            });
        }
        return false;

    });


    $('#clear').click(function () {
        window.location.reload();
    });

    function Bindata() {
        var updatedInterest = parseFloat($('#UpdatedInterest').val());
        var currentInterest = $('#CurrentInterest').val();
        if (parseInt($('#hdfAccountType').val()) === 12) {
            updatedInterest = interestRateToDisplay * 2;
            currentInterest = currentInterest * 2;
        }
        var data = {
            SettlementId: $('#hdfSettlementId').val(),
            SettlementTypeId: $('#SettlementType option:selected').val(),
            SettlementDate: $('#SettlementDate').val(),
            LoanNo: $('#LoanNo').val(),
            //CurrentInterest: $('#CurrentInterest').val(),
            //UpdatedInterest: $('#UpdatedInterest').val(),
            CurrentInterest: currentInterest,
            UpdatedInterest: updatedInterest,
            CurrentInstallmentAmount: $('#CurrentInstallmentAmount').val(),
            UpdatedInstallmentAmount: $('#UpdatedInstallmentAmount').val(),
            //CustomerId: $('#CustomerId').val(),
            Reason: $('#Reason').val()
        }

        return data;
    }
    function CheckValidation() {
        var isValid = true;


        if ($('#LoanNo').val().trim() === '') {
            isValid = false;
            $('#LoanNo').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#LoanNo').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#SettlementDate').val().trim() === '') {
            isValid = false;
            $('#SettlementDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#SettlementDate').parent().prev().find('span').css('visibility', 'hidden');
        }


        if ($('#SettlementType').val() === '0') {
            isValid = false;
            $('#SettlementType').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#SettlementType').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#Reason').val() === '') {
            isValid = false;
            $('#Reason').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Reason').parent().prev().find('span').css('visibility', 'hidden');
        }


        if ($('#CurrentInterest').val().trim() === '') {
            isValid = false;
            $('#CurrentInterest').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#CurrentInterest').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#CurrentInstallmentAmount').val().trim() === '') {
            isValid = false;
            $('#CurrentInstallmentAmount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#CurrentInstallmentAmount').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#UpdatedInterest').val().trim() === '') {
            isValid = false;
            $('#UpdatedInterest').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#UpdatedInterest').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#UpdatedInstallmentAmount').val().trim() === '') {
            isValid = false;
            $('#UpdatedInstallmentAmount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#UpdatedInstallmentAmount').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    //json to date converter
    function FormateDate(jsonDate) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    //date picker
    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#SettlementDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

})();