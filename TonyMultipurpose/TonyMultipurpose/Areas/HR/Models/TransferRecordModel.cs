﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class TransferRecordModel : CommonModel
    {
        public int Id { get; set; }

        public string EmpId { get; set; }

        public short BranchCode { get; set; }

        public string Reason { get; set; }

        public bool IsStatus { get; set; }
        public string ToBranch { get; set; }
        public string FromBranch { get; set; }
        public string EmployeeName { get; set; }
        public string TransferDate { get; set; }
    }
}