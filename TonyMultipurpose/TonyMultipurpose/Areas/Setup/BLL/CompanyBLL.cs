﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    public class CompanyBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForCompany(DbDataReader objDataReader, Company objCompany)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objCompany.CompanyId = Convert.ToInt16(objDataReader["CompanyId"]);
                        }
                        break;
                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objCompany.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;
                    case "Address":
                        if (!Convert.IsDBNull(objDataReader["Address"]))
                        {
                            objCompany.Address = objDataReader["Address"].ToString();
                        }
                        break;
                    case "Phone":
                        if (!Convert.IsDBNull(objDataReader["Phone"]))
                        {
                            objCompany.Phone = objDataReader["Phone"].ToString();
                        }
                        break;
                    case "Fax":
                        if (!Convert.IsDBNull(objDataReader["Fax"]))
                        {
                            objCompany.Fax = objDataReader["Fax"].ToString();
                        }
                        break;
                    case "Email":
                        if (!Convert.IsDBNull(objDataReader["Email"]))
                        {
                            objCompany.Email = objDataReader["Email"].ToString();
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objCompany.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objCompany.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objCompany.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objCompany.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objCompany.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //get all company information for index 
        public List<Company> GetAllCompanyInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Company> objCompanyList = new List<Company>();
            Company objCompany;
            try
            {
                objDbCommand.AddInParameter("IsDeleted", 0);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllCompanyInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCompany = new Company();
                        this.BuildModelForCompany(objDbDataReader, objCompany);
                        objCompanyList.Add(objCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCompanyList;
        }

        //get company detail by id for Edit and Delete action
        public Company GetCompanyInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            Company objCompany = new Company();

            try
            {
                objDbCommand.AddInParameter("CompanyId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCompanyInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCompany = new Company();
                        this.BuildModelForCompany(objDbDataReader, objCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCompany;
        }

        //save company information
        public string SaveCompanyInfo(Company objCompany)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CompanyName", objCompany.CompanyName);
            objDbCommand.AddInParameter("Address", objCompany.Address);
            objDbCommand.AddInParameter("Phone", objCompany.Phone);
            objDbCommand.AddInParameter("Fax", objCompany.Fax);
            objDbCommand.AddInParameter("Email", objCompany.Email);
            objDbCommand.AddInParameter("Logo", objCompany.Logo);
            objDbCommand.AddInParameter("IsDeleted", objCompany.IsDeleted);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("CreatedDate", objCompany.CreatedDate);;

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCompanyInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //update company detail 
        public string UpdateCompanyInfo(Company objCompany)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CompanyId", objCompany.CompanyId);
            objDbCommand.AddInParameter("CompanyName", objCompany.CompanyName);
            objDbCommand.AddInParameter("Address", objCompany.Address);
            objDbCommand.AddInParameter("Phone", objCompany.Phone);
            objDbCommand.AddInParameter("Fax", objCompany.Fax);
            objDbCommand.AddInParameter("Email", objCompany.Email);
            objDbCommand.AddInParameter("Logo", objCompany.Logo);
            objDbCommand.AddInParameter("UpdatedBy", objCompany.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", objCompany.UpdatedDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspUpdateCompanyInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //Delete Company
        public string DeleteCompany(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CompanyId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteCompany]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Deleted Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}