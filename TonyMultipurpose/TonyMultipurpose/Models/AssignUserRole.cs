﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Models
{
    //--ataur
    //==db table name: UserRoleMapping==//
    public class AssignUserRole:CommonModel
    {
        [Key]
        public int UrmId { get; set; }



        [Required(ErrorMessage = "Username Is Required !")]
        [Display(Name = "Username")]
        public short UserDetailId { get; set; }
        public string Username { get; set; }
        public List<UserDetail> userDetail { get; set; }


        [Display(Name = "Role Name")]
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public List<RoleInfo> roleInfo { get; set; }

        [Required(ErrorMessage = "Branch Name Is Required !")]
        [Display(Name = "Branch Name")]
        public short? BranchId { get; set; }
        public string BranchName { get; set; }
        public List<Branch> branchInfo { get; set; }
    }
}