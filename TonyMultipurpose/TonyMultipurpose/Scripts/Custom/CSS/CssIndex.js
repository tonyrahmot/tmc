﻿
    (function () {

        $("#hideRow").hide();
        $("#hideTable").hide();
        $("#hideTableAll").hide();

        //Account No autocomplete
        $("#AccountNumber").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: window.applicationBaseUrl + "CSS/CSS/AutoCompleteAccountNumber",
                    type: "GET",
                    dataType: "json",
                    data: { term: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return { label: item, value: item };
                        }));
                    }
                });
            },
            messages: {
                noResults: "",
                results: ""
            }
        });

        totalItems = [];
        var totalAmount = 0;
        var checkedWiseFineAmmount = 0;

        //load data
        $("#AccountNumber").change(function () {

            var AccountNumber = $("#AccountNumber").val();
            var json = {
                AccountNumber: AccountNumber
            };
            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "CSS/CSS/GetAccountInfoByAccountNo",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                success: function (data) {
                    $("#allDataShowForCssDeposit tr").detach();
                    if (data.InstallmentNumber == null) {
                        alert("You have unapproved installments!!!");
                    }
                    else if (data.InstallmentNumber == data.Duration) {
                        alert("All installments were deposited!!!");
                    }
                    else {

                        calculateDues(data);
                    }
                }
            });
        });
        $(function () {
            $("#ChartOfAccount").empty();
            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                    $.each(data, function (key, value) {
                        $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                    });
                }
            });
        });

        function calculateDues(data) {
            $("#SaleData").empty();
            $("#hideTable").show();
            $('#CustomerName').val(data.CustomerName);
            $('#FatherName').val(data.FatherName);
            $('#MotherName').val(data.MotherName);
            $('#Mobile').val(data.Mobile);
            $('#InstallmentNumber').val(data.InstallmentNumber + 1);
            var duratinMoth = 0;
            var count = data.InstallmentNumber + 1;

            var today = new Date();
            var monthnow = today.getMonth() + 1;
            var yearNow = today.getFullYear();
            var DurationCheck = data.Duration;

            var month = data.InstallmentMonth,
                year = data.InstallmentYear;

            var totalMonthInOneYear = 12;
            var totalYear = yearNow - year;
            var countYear = totalYear * totalMonthInOneYear;
            var c = countYear + monthnow;
            var duratain = c - month;
            if (duratain <= 0) {
                $("#hideTable").hide();
                $("#submit").attr("disabled", "disabled");
            }
            if (countYear == 0) {
                //duratinMoth = duratain;

                $('#hdfInstallmentYear').val(year);
                $('#hdfCustomerId').val(data.CustomerId);
                $('#hdfInterestRate').val(data.InterestRate);
                $('#hdfDuration').val(data.Duration);
                $('#hdfMatureDate').val(formateDate(data.MatureDate));
                $('#hdfMatureAmount').val(data.MatureAmount);
                $('#CustomerImage').attr('src', data.CustomerImage);
                $('#InstallmentMonth').val(monthNumToName(month) + ' - ' + year);

                var installmentNumber = data.InstallmentNumber + 1;
                var totalDurationMonth = duratain;
                var monthInsert = data.InstallmentMonth + 1;
                if (totalDurationMonth >= 3) {
                    $("#hideRow").show();
                    for (var a = 0 ; a < totalDurationMonth; a++) {
                        if (month == 12 || month > 12) {
                            month = 1;
                            year = year + 1;
                            monthInsert = 1;
                        }
                        else {
                            month = month + 1;
                        }
                        if (DurationCheck >= count) {
                            //$("#SaleData").append('<tr class="borderSet"> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            $("#SaleData").append('<tr class="borderSet"> <td class="borderSet"><input type="checkbox" id=' + installmentNumber + ' class="checkbox" /></td><td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            count++;
                        }
                        totalAmount = totalAmount + data.Amount;
                        if (DurationCheck >= installmentNumber) {
                            totalItems.push({
                                InstallmentNumber: installmentNumber,
                                InstallmentMonth: monthInsert,
                                Fine: 0,
                                AccruedBalance: 0,
                                Amount: data.Amount,
                                AccountNumber: $('#AccountNumber').val(),
                                TransactionDate: $('#TransactionDate').val(),
                                InstallmentYear: year,
                                CustomerName: $('#CustomerName').val(),
                                CustomerId: $('#hdfCustomerId').val(),
                                InterestRate: $('#hdfInterestRate').val(),
                                Duration: $('#hdfDuration').val(),
                                MatureDate: $('#hdfMatureDate').val(),
                                MatureAmount: $('#hdfMatureAmount').val(),
                                //COAId: $('#ChartOfAccount').val(),
                                COAId: $('#ChartOfAccount option:selected').val()
                            });
                        }

                        monthInsert++;
                        installmentNumber++;
                        if ($('#atFine').val().trim() == '') {
                            $('#submit').attr("disabled", "disabled");
                            $('#msgForFine').val('Please give some fine rate');
                        }
                    }
                    //$("#SaleData").append('<tr style="border-top:0px !important;"> <td style="border-top:0px !important;"></td> <td style="border-top:0px !important;"></td><td style="border-top:0px !important; font-weight:bold; ">Total:</td><td style="border-top:0px !important;">' + (totalAmount) + '</td></tr>  ');


                }
                else {
                    for (var a = 0 ; a < totalDurationMonth; a++) {
                        if (month == 12 || month > 12) {
                            month = 1;
                            year = year + 1;
                            monthInsert = 1;
                        }
                        else {
                            month = month + 1;
                        }if (DurationCheck >= count) {
                            //$("#SaleData").append('<tr class="borderSet"> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            $("#SaleData").append('<tr class="borderSet"><td class="borderSet"><input type="checkbox" id=' + installmentNumber + ' class="checkbox"/></td> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            count++;
                        }
                        totalAmount = totalAmount + data.Amount;
                        if (DurationCheck >= installmentNumber) {
                            totalItems.push({
                                InstallmentNumber: installmentNumber,
                                InstallmentMonth: monthInsert,
                                AccruedBalance: 0,
                                Amount: data.Amount,
                                AccountNumber: $('#AccountNumber').val(),
                                TransactionDate: $('#TransactionDate').val(),
                                InstallmentYear: year,
                                CustomerName: $('#CustomerName').val(),
                                CustomerId: $('#hdfCustomerId').val(),
                                InterestRate: $('#hdfInterestRate').val(),
                                Duration: $('#hdfDuration').val(),
                                MatureDate: $('#hdfMatureDate').val(),
                                MatureAmount: $('#hdfMatureAmount').val(),
                                //COAId: $('#ChartOfAccount').val()
                                COAId: $('#ChartOfAccount option:selected').val()
                            });
                        }
                        monthInsert++;
                        installmentNumber++;
                    }
                    //$("#SaleData").append('<tr style="border-top:0px !important;"> <td style="border-top:0px !important;"></td> <td style="border-top:0px !important;"></td><td style="border-top:0px !important; font-weight:bold; ">Total:</td><td style="border-top:0px !important;">' + totalAmount + '</td></tr>  ');
                }
            }
            else {
                duratinMoth = duratain;
                $('#hdfInstallmentYear').val(year);
                $('#hdfCustomerId').val(data.CustomerId);
                $('#hdfInterestRate').val(data.InterestRate);
                $('#hdfDuration').val(data.Duration);
                $('#hdfMatureDate').val(formateDate(data.MatureDate));
                $('#hdfMatureAmount').val(data.MatureAmount);
                $('#CustomerImage').attr('src', data.CustomerImage);
                $('#InstallmentMonth').val(monthNumToName(month) + ' - ' + year);

                var installmentNumber = data.InstallmentNumber + 1;
                var totalDurationMonth = duratinMoth;

                var monthInsert = data.InstallmentMonth + 1;
                if (totalDurationMonth >= 3) {
                    $("#hideRow").show();
                    for (var a = 0 ; a < totalDurationMonth; a++) {
                        if (month == 12 || month > 12) {
                            month = 1;
                            year = year + 1;
                            monthInsert = 1;
                        }
                        else {
                            month = month + 1;
                        }
                        if (DurationCheck >= count) {
                            //$("#SaleData").append('<tr class="borderSet"> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            $("#SaleData").append('<tr class="borderSet"> <td class="borderSet"><input type="checkbox" id=' + installmentNumber + ' class="checkbox" /></td> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            count++;
                        }
                        totalAmount = totalAmount + data.Amount;
                        if (DurationCheck >= installmentNumber) {
                            totalItems.push({
                                InstallmentNumber: installmentNumber,
                                InstallmentMonth: monthInsert,
                                Fine: 0,
                                AccruedBalance: 0,
                                Amount: data.Amount,
                                AccountNumber: $('#AccountNumber').val(),
                                TransactionDate: $('#TransactionDate').val(),
                                InstallmentYear: year,
                                CustomerName: $('#CustomerName').val(),
                                CustomerId: $('#hdfCustomerId').val(),
                                InterestRate: $('#hdfInterestRate').val(),
                                Duration: $('#hdfDuration').val(),
                                MatureDate: $('#hdfMatureDate').val(),
                                MatureAmount: $('#hdfMatureAmount').val(),
                                //COAId: $('#ChartOfAccount').val()
                                COAId: $('#ChartOfAccount option:selected').val()
                            });
                        }
                        monthInsert++;
                        installmentNumber++;
                        if ($('#atFine').val().trim() == '') {
                            $('#submit').attr("disabled", "disabled");
                            $('#msgForFine').val('Please give some fine rate');
                        }
                    }

                    //$("#SaleData").append('<tr style="border-top:0px !important;"> <td style="border-top:0px !important;"></td> <td style="border-top:0px !important;"></td><td style="border-top:0px !important; font-weight:bold; ">Total:</td><td style="border-top:0px !important;">' + (totalAmount) + '</td></tr>  ');

                }
                else {
                    for (var a = 0 ; a < totalDurationMonth; a++) {
                        if (month == 12 || month > 12) {
                            month = 1;
                            year = year + 1;
                            monthInsert = 1;
                        }
                        else {
                            month = month + 1;
                        }if (DurationCheck >= count) {
                            //$("#SaleData").append('<tr class="borderSet"> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            $("#SaleData").append('<tr class="borderSet"> <td class="borderSet"><input type="checkbox" id=' + installmentNumber + ' class="checkbox"/></td> <td class="borderSet">' + installmentNumber + '</td><td class="borderSet">' + monthNumToName(month) + ' - ' + year + '</td><td class="borderSet">' + data.Amount + '</td></tr>  ');
                            count++;
                        }
                        totalAmount = totalAmount + data.Amount;
                        if (DurationCheck >= installmentNumber) {
                            totalItems.push({
                                InstallmentNumber: installmentNumber,
                                InstallmentMonth: monthInsert,
                                Amount: data.Amount,
                                AccruedBalance: 0,
                                AccountNumber: $('#AccountNumber').val(),
                                TransactionDate: $('#TransactionDate').val(),
                                InstallmentYear: year,
                                CustomerName: $('#CustomerName').val(),
                                CustomerId: $('#hdfCustomerId').val(),
                                InterestRate: $('#hdfInterestRate').val(),
                                Duration: $('#hdfDuration').val(),
                                MatureDate: $('#hdfMatureDate').val(),
                                MatureAmount: $('#hdfMatureAmount').val(),
                                //COAId: $('#ChartOfAccount').val()
                                COAId: $('#ChartOfAccount option:selected').val()
                            });
                        }
                        monthInsert++;
                        installmentNumber++;
                    }
                    //$("#SaleData").append('<tr style="border-top:0px !important;"> <td style="border-top:0px !important;"></td> <td style="border-top:0px !important;"></td><td style="border-top:0px !important; font-weight:bold; ">Total:</td><td style="border-top:0px !important;">' + totalAmount + '</td></tr>  ');
                    //$("#SaleData").append('<tr style="border-top:0px !important;"> <td style="border-top:0px !important;"></td> <td style="border-top:0px !important;"></td><td style="border-top:0px !important; font-weight:bold; ">Total:</td><td style="border-top:0px !important;"><span id="spnFine"></td></tr>  ');
                }
            }
        }
        $('#chkAll').click(function (e) {
            var table = $(e.target).closest('table');
            $('td input:checkbox', table).prop('checked', this.checked);
        });

        function getCheckedItems(totalItems) {
            var items = [];
            checkedWiseFineAmmount = 0;
            $('#SaleData').find('input[type="checkbox"]:checked').each(function () {
                var id = this.id;
                totalItems.forEach(function (item) {
                    if (id == item.InstallmentNumber) {
                        item.COAId = $('#ChartOfAccount option:selected').val();
                        items.push(item);
                        checkedWiseFineAmmount += item.Amount;
                    }
                })
            });
            return items;
        }

        $("#atFine").change(function () {
            var finePersent = $('#atFine').val();
            var fine = 0;
            fine = (totalAmount * finePersent) / 100;
            //totalItems[0].Fine = fine;
            //totalItems[0].AccruedBalance = fine;
            var checkedItems = getCheckedItems(totalItems);
            if (checkedItems.length === 0) {
                isAllValid = false;
                alert('Please select atleast one installment.');
            }
            else {
                var finePersent = $('#atFine').val();
                var fine = 0;
                fine = (checkedWiseFineAmmount * finePersent) / 100;
                $('.fineShow').val(fine + '  Tk.');
                $('#submit').removeAttr('disabled');
                $('#msgForFine').hide();
            }

        });

        $("#AccountNumber").change(function () {
            var AccountNumber = $("#AccountNumber").val();
            var json = {
                AccountNumber: AccountNumber
            };
            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "CSS/CSS/GetAllAccountInfoByAccountNo",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                success: function (data) {
                    $.each(data, function (key, value) {
                        $("#hideTableAll").show();
                        $("#allDataShowForCssDeposit").append('<tr class="borderSet"> <td class="borderSet">' + formateDate(value.TransactionDate) + '</td><td class="borderSet">' + value.InstallmentNumber + '</td><td class="borderSet">' + monthNumToName(value.InstallmentMonth) + ' - ' + value.InstallmentYear + '</td><td class="borderSet">' + value.Amount + '</td></tr>  ');
                    });
                }
            });
        });



        $('#submit').click(function () {
            var isAllValid = true;
            if ($('#TransactionDate').val().trim() == '') {
                $('#TransactionDate').siblings('span.error').css('display', 'block');
                isAllValid = false;
            } else {
                $('#TransactionDate').siblings('span.error').css('display', 'none');
            }
            if ($('#AccountNumber').val().trim() == '') {
                $('#AccountNumber').siblings('span.error').css('display', 'block');
                isAllValid = false;
            } else {
                $('#AccountNumber').siblings('span.error').css('display', 'none');
            }
            if ($('#ChartOfAccount').val().trim() == 0) {
                $('#ChartOfAccount').siblings('span.error').css('display', 'block');
                isAllValid = false;
            } else {
                $('#ChartOfAccount').siblings('span.error').css('display', 'none');
            }

            var checkedItems = getCheckedItems(totalItems);
            if (checkedItems.length === 0) {
                isAllValid = false;
                alert('Please select atleast one installment.');
            }
            else {
                var finePersent = $('#atFine').val();
                var fine = 0;
                fine = (checkedWiseFineAmmount * finePersent) / 100;
                checkedItems[0].Fine = fine;
                checkedItems[0].AccruedBalance = fine;
            }
            if (isAllValid) {
                $.ajax({
                    type: "POST",
                    url: window.applicationBaseUrl + "CSS/CSS/Save",
                    data: JSON.stringify(checkedItems),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.status === true) {
                            alert('Successfully done.');
                            $("#AccountNumber").val('');
                            $("#TransactionDate").val('');
                            $("#InstallmentNumber").val('');
                            $("#hdfInstallmentMonth").val('');
                            $("#hdfInstallmentYear").val('');
                            $('#Amount').val('');
                            $('#CustomerName').val('');
                            $('#InstallmentNumber').val('');
                            $('#InstallmentMonth').val('');
                            $("#hdfCustomerId").val('');
                            $("#hdfInterestRate").val('');
                            $("#hdfDuration").val('');
                            $('#hdfMatureDate').val('');
                            $('#hdfMatureAmount').val('');
                            $('#ChartOfAccount').val('0');
                        } else {
                            alert('Failed');
                        }
                        $('#submit').val('Save');
                        window.location.reload();
                    },
                    error: function () {
                        alert('Error. Please try again.');
                    }
                });
            }
        });

        var months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
        ];
        //convert datetime
        function formateDate(jsonDate) {
            var date = new Date(parseInt(jsonDate.substr(6)));
            var dd = date.getDate();
            var mm = months[date.getMonth()];
            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var formmatedDate = dd + '/' + mm + '/' + yyyy;
            return formmatedDate;
        }

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }


    })();

    function IsDouble(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
            return false;
        }
        else if (charCode == 13) {
            return false;
        }
        status = "";
        return true;
    }
