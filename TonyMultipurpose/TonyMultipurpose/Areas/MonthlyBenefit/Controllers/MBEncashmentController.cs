﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.MonthlyBenefit.BLL;
using TonyMultipurpose.Areas.MonthlyBenefit.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.MonthlyBenefit.Controllers
{
    [AuthenticationFilter]
    public class MBEncashmentController : Controller
    {

        MBBLL objMBBLL = new MBBLL();

        // GET: MonthlyBenefit/Encashment
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(MBModel mbModel)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                mbModel.TransactionType = "Mature Encashment";
                mbModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                mbModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId); 
                mbModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID); 
                mbModel.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("dd-MMM-yyyy"));
                mbModel.IsActive = false;
                mbModel.IsApproved = false;
                objMBBLL.SaveMBMatureEncashmentInfo(mbModel);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        public JsonResult GetUnAppovedMBList(string id)
        {
            bool exsits = objMBBLL.GetUnAppovedMBList(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetCloseMBAccount(string id)
        {
            bool exsits = objMBBLL.GetCloseMBAccount(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetTransactionalCoaData()
        {
            TransactionBLL objTransactionBll = new TransactionBLL();
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteMBAccountNumber(string term)
        {
            var result = objMBBLL.GetAllMBAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrematureMbAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objMBBLL.GetPrematureMbAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}