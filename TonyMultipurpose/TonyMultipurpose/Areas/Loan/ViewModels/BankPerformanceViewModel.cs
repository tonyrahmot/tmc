﻿
using System.ComponentModel.DataAnnotations;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class BankPerformanceViewModel
    {
        [Display(Name = "Id")]
        public long BankperformanceId { get; set; }
        public string LoanId { get; set; }

        [Display(Name = "Bank/NGO name")]
        public string NgoName { get; set; }

        [Display(Name = "A/C No")]
        public string AccNo { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        public string ToDate { get; set; }

        [Display(Name = "Debit Amount")]
        public decimal? DebitAmount { get; set; }

        [Display(Name = "Credit Amount")]
        public decimal? CreditAmount { get; set; }

        [Display(Name = "Avg")]
        public decimal? Avg { get; set; }
        public short? CreatedBy { get; set; }
    }
}