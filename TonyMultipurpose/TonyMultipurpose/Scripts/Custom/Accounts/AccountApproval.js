﻿$(document).ready(function () {
    var total = 0;
    $('#checkableGrid tbody tr').each(function () {
        total = total + parseInt($(this).find('td:eq(2)')[0].innerText);
    });
    $('tbody').append('<tr><td></td><td><b>Total</b></td><td><b>' + total + '</b></td></tr>');
});