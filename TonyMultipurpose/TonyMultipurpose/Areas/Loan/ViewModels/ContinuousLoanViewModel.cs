﻿using System;
using TonyMultipurpose.Areas.Loan.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class ContinuousLoanViewModel
    {
        public long Id { get; set; }

        public string TransactionId { get; set; }

        public DateTime? TransactionDate { get; set; }

        public string LoanId { get; set; }

        public string CustomerId { get; set; }

        public string TransactionType { get; set; }

        public string Particular { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public decimal? Outstanding { get; set; }

        public decimal? AvailableBalance { get; set; }
        public decimal WithdrawAmount { get; set; }
        public decimal DepositAmount { get; set; }
        public bool? IsChequeEntry { get; set; }

        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public string ChequeNo { get; set; }

        public int COAId { get; set; }

        public string VoucherNo { get; set; }

        public decimal? CustomerLoanLimit { get; set; }
        public string CustomerName{ get; set; }
        public string CustomerImage { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Mobile { get; set; }
        public string CreatedName { get; set; }
        public string TransactionDateShow { get; set; }
        public string ChequeEntryStatus { get; set; }
        public string RejectReason { get; set; }
    }
}