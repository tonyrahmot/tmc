﻿using System;

namespace TonyMultipurpose.DAL
{
    [Serializable()]
    public class SessionContainer
    {
        public SessionContainer()
        {
            SessionUtility.TMSessionContainer = this;
        }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int BranchCode { get; set; }
        public int UserRole { get; set; }
        public object OBJ_DOC_CLASS { get; set; }
        public object OBJ_Menu_CLASS { get; set; }
        public object OBJ_RPTDOC { get; set; }
        public string ErrorMsg { get; set; }
        public string CompanyAddress { get; set; }
        public DateTime SystemDate { get; set; }
        public bool IsAuthenticatedMenu { get; set; }
    }
}