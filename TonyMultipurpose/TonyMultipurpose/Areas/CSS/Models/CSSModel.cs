﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.CSS.Models
{
    public class CSSModel:CommonModel
    {
        [Key]
        public long Id { get; set; }
        public string TransactionId { get; set; }

        [Display(Name = "Date")]
        public DateTime? TransactionDate { get; set; }

        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }

        [Display(Name = "A/C No")]
        public string AccountNumber { get; set; }

        [Display(Name = "Interest Rate")]
        public decimal? InterestRate { get; set; }

        public string CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

       
        [Display(Name = "Father Name")]
        public string FatherName { get; set; }


        [Display(Name = "Mother Name")]
        public string MotherName { get; set; }

        [Display(Name = "Phone")]
        public string Mobile { get; set; }
        public short? BranchId { get; set; }
        public string BranchName { get; set; }
        public string CompanyName { get; set; }
        public string Username { get; set; }
        public int? AccountSetupId { get; set; }

        [Display(Name = "Account Type Name")]
        public string AccountTypeName { get; set; }

        [Display(Name = "Debit")]
        public decimal? Debit { get; set; }

        [Display(Name = "Credit")]
        public decimal? Credit { get; set; }

        [Display(Name = "Balance")]
        public decimal? Balance { get; set; }

        [Display(Name = "Accrued Balance")]
        public decimal? AccruedBalance { get; set; }

        [Display(Name = "Particular")]
        public string Particular { get; set; }

        [Required]
        public decimal? Amount { get; set; }



        public decimal? InstallmentAmount { get; set; }
        public int? InstallmentMonth { get; set; }
        public int? InstallmentNumber { get; set; }
        public int? InstallmentYear { get; set; }
        public int? Duration { get; set; }

        [Display(Name = "Mature Amount")]
        public decimal? MatureAmount { get; set; }
        public decimal? Fine { get; set; }

        [Display(Name = "Mature Date")]
        public DateTime? MatureDate { get; set; }
        public string CustomerImage { get; set; }

        //--for pre mature encashment 
        public decimal PreMatureAmount { get; set; }
        public decimal CompanyAmount { get; set; }

        //--for pre mature encashment end

        public string RejectReason { get; set; }

        public int COAId { get; set; }

    }
}