﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Setup.BLL
{
    public class BranchBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForBranch(DbDataReader objDataReader, Branch objBranch)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objBranch.BranchId = Convert.ToInt16(objDataReader["BranchId"]);
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objBranch.ComapnyId = Convert.ToInt16(objDataReader["CompanyId"]);
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objBranch.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "BranchCode":
                        if (!Convert.IsDBNull(objDataReader["BranchCode"]))
                        {
                            objBranch.BranchCode = Convert.ToInt16(objDataReader["BranchCode"]);
                        }
                        break;
                    case "Address":
                        if (!Convert.IsDBNull(objDataReader["Address"]))
                        {
                            objBranch.Address = objDataReader["Address"].ToString();
                        }
                        break;
                    case "Phone":
                        if (!Convert.IsDBNull(objDataReader["Phone"]))
                        {
                            objBranch.Phone = objDataReader["Phone"].ToString();
                        }
                        break;
                    case "Fax":
                        if (!Convert.IsDBNull(objDataReader["Fax"]))
                        {
                            objBranch.Fax = objDataReader["Fax"].ToString();
                        }
                        break;
                    case "Email":
                        if (!Convert.IsDBNull(objDataReader["Email"]))
                        {
                            objBranch.Email = objDataReader["Email"].ToString();
                        }
                        break;
                    case "IsDeleted":
                        if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                        {
                            objBranch.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objBranch.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objBranch.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objBranch.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objBranch.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "FullName":
                        if (!Convert.IsDBNull(objDataReader["FullName"]))
                        {
                            objBranch.FullName = objDataReader["FullName"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        public List<Branch> GetAllBranchInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Branch> objBranchList = new List<Branch>();
            Branch objBranch;
            try
            {
                objDbCommand.AddInParameter("IsDeleted", 0);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllBranchInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objBranch = new Branch();
                        this.BuildModelForBranch(objDbDataReader, objBranch);
                        objBranchList.Add(objBranch);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objBranchList;
        }
        public Branch GetBranchInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            Branch objBranch = new Branch();

            try  
            {
                objDbCommand.AddInParameter("BranchId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetBranchInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objBranch = new Branch();
                        this.BuildModelForBranch(objDbDataReader, objBranch);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objBranch;
        }
        public string GetBranchName(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            Branch objBranch = new Branch();

            try  
            {
                objDbCommand.AddInParameter("BranchId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetBranchInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objBranch = new Branch();
                        this.BuildModelForBranch(objDbDataReader, objBranch);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objBranch.BranchName;
        }
        public string SaveBranchInfo(Branch objBranch)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //objDbCommand.AddInParameter("BranchId", objBranch.BranchId);
            objDbCommand.AddInParameter("CompanyName", objBranch.CompanyName);
            objDbCommand.AddInParameter("BranchName", objBranch.BranchName);
            objDbCommand.AddInParameter("BranchCode", objBranch.BranchCode);
            objDbCommand.AddInParameter("Address", objBranch.Address);
            objDbCommand.AddInParameter("Phone", objBranch.Phone);
            objDbCommand.AddInParameter("Fax", objBranch.Fax);
            objDbCommand.AddInParameter("Email", objBranch.Email);
            objDbCommand.AddInParameter("IsDeleted", objBranch.IsDeleted);
            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("CreatedDate", objBranch.CreatedDate);
            objDbCommand.AddInParameter("FullName", objBranch.FullName);
            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveBranchInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
        public string UpdateBranchInfo(Branch objBranch)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("BranchId", objBranch.BranchId);
            objDbCommand.AddInParameter("BranchName", objBranch.BranchName);
            objDbCommand.AddInParameter("BranchCode", objBranch.BranchCode.ToString());
            objDbCommand.AddInParameter("Address", objBranch.Address);
            objDbCommand.AddInParameter("Phone", objBranch.Phone);
            objDbCommand.AddInParameter("Fax", objBranch.Fax);
            objDbCommand.AddInParameter("Email", objBranch.Email);
            objDbCommand.AddInParameter("UpdatedBy", objBranch.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", objBranch.UpdatedDate);
            objDbCommand.AddInParameter("FullName", objBranch.FullName);
            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspUpdateBranchInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
        public string DeleteBranch(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("BranchId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteBranch]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Deleted Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}