﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.Models;

namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class EmployeeInformationViewModel: EmployeeInformation
    {
        public string DesignationName { get; set; }
        public string DepartmentName { get; set; }
        public string BranchName { get; set; }
        public string FuntionalDesignation { get; set; }
        
    }
}