﻿using System;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.Models
{
    public class ShareTransaction:CommonModel
    {
        public int Id { get; set; }
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        public string EntryType { get; set; }
        public decimal? PricePerShare { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? IssuedDate { get; set; }
        public string Particular { get; set; }
        public string VoucherNo { get; set; }
    }
}