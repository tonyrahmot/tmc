
(function () {
    var oTable;
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/LoanClosing/GetClosingLoanInfo",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "LoanNo" },
                { "data": "AccountTypeName" },
                { "data": "LoanDate" },
                { "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                //{ "data": "InterestRate" },
                { "data": "InterestRateToDisplay" },
                { "data": "InstallmentAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" },
                {
                    "data": "LoanNo",
                    "width": "50px",
                    "render": function (data) {
                        var url = window.applicationBaseUrl + "Loan/LoanClosing/PrintLoanStatusReport?loanNo=" + data;
                        return '<a class="btn btn-info" target="_blank" href="' + url + '">Print</a>';
                    }
                },
                {
                    "data": "LoanNo",
                    "width": "50px",
                    "render": function (data) {
                        return '<a class="btn btn-warning" onclick="confirmToSubmit(\'' + data + '\')">Close</a>';
                        }
                }
            ],
            "order": [[0, "desc"]]
        });
    }
})();

function confirmToSubmit(loanNo) {
    var isConfirmed = confirm('Are you sure?');
    if (isConfirmed) {
        window.location.href = window.applicationBaseUrl + "Loan/LoanClosing/CloseLoan?loanNo=" + loanNo;
    }
}