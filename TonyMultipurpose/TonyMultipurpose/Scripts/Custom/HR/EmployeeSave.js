﻿$(function () {
    $("#tabs").tabs();
    getEmployeeCode();
});

$(document).ready(function () {

    //Basic Info
    $('#btnEdit').hide();
    $('#btnDelete').hide();



    var oTable, oEducationTable, oExperienceTable;
    employeeTable();
    function employeeTable() {
        oTable = $('#basicInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/Employee/EmployeeBasicInfo",//'@Url.Action("EmployeeBasicInfo", "Employee", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmployeeName" },
                { "data": "DesignationName" },
                { "data": "DepartmentName" },
                { "data": "BranchName" },
                { "data": "MobileNo" }
            ],
            "order": [[0, "desc"]]
        });
        $('#basicInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#btnEdit').show();
        $('#btnDelete').show();
        $('#btnSave').hide();
        $('#hdfEmpId').val(data.EmpId);
        $('#EmpCode').val(data.EmpCode);
        $('#NID').val(data.NID);
        $('#CurrentSalary').val(data.CurrentSalary);
        var date = new Date(data.DOB);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var dob = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#DOB').val(dob);
        $('#PermanentAddress').val(data.PermanentAddress);
        $('#PresentAddress').val(data.PresentAddress);
        date = new Date(data.JoiningDate);
        month = date.getMonth() + 1;
        mon = monthNumToName(month);
        var joiningDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#JoiningDate').val(joiningDate);
        $('#MobileNo').val(data.MobileNo);
        $('#BranchId').val(data.BranchId);
        $('#EmailId').val(data.EmailId);
        $('#DesigId').val(data.DesigId);
        $('#FunctionalDesignation').val(data.FunctionalId);
        $('#MaritalStatus').val(data.MaritalStatus);
        $('#MotherName').val(data.MotherName);
        $('#FatherName').val(data.FatherName);
        $('#LastName').val(data.LastName);
        $('#FirstName').val(data.FirstName);
        $('#Religion').val(data.Religion);
        $('#Gender').val(data.Gender);
        $('#Reason').val(data.Reason);
        //$('#Reason').val(data.Reason);
        //$('#Reason').val(data.Reason);
        $('#hdfExisitngImagePath').val(data.OldImagePath);
        var num = Math.random();
        var imagePathToDisplay = data.ImagePath + "?v=" + num;
        $('#img-upload').attr('src', imagePathToDisplay);
        $('#DeptId').val(data.DeptId);
        $('#EmpCode').attr('readonly', 'readonly');
        $('#EdEmpCode').val(data.EmpCode);
        $('#ExEmpCode').val(data.EmpCode);
        ClearEducationFields();
        if (oEducationTable != null) {
            oEducationTable.destroy();
        }
        educationTable();
        ClearExperienceFields();
        if (oExperienceTable != null) {
            oExperienceTable.destroy();
        }
        ExperienceTable();
    }

    $('#btnclear').click(function () {
        window.location.reload();
    });

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
    function readUrl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#ImageFile").change(function () {
        readUrl(this);
    });

    $(function () {
        $("#BranchId").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/Employee/GetBranches",//'@Url.Action("GetBranches", "Employee", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
                $("#BranchId").append('<option value="0">--Select--</option>');
                $.each(data, function (key, value) {
                    $("#BranchId").append('<option value=' + value.BranchId + '>' + value.BranchName + '</option>');
                });
            }
        });
    });

    $(function () {
        $("#DeptId").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/Employee/GetDepartments",//'@Url.Action("GetDepartments", "Employee", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
                $("#DeptId").append('<option value="0">--Select--</option>');
                $.each(data, function (key, value) {
                    $("#DeptId").append('<option value=' + value.DeptId + '>' + value.DepartmentName + '</option>');
                });
            }
        });
    });

    $(function () {
        $("#DesigId").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/Employee/GetDesignations",//'@Url.Action("GetDesignations", "Employee", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
                $("#DesigId").append('<option value="0">--Select--</option>');
                $.each(data, function (key, value) {
                    $("#DesigId").append('<option value=' + value.DesigId + '>' + value.DesignationName + '</option>');
                });
            }
        });
    });

    $(function () {
        $("#FunctionalDesignation").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/Employee/GetFunctionalDesignations",//'@Url.Action("GetFunctionalDesignations", "Employee", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
                $("#FunctionalDesignation").append('<option value="0">--Select--</option>');
                $.each(data, function (key, value) {
                    $("#FunctionalDesignation").append('<option value=' + value.DesigId + '>' + value.FunctionalDesignation + '</option>');
                });
            }
        });
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    $('#btnSave').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = bindata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/SaveEmployeeBasicInfo",//'@Url.Action("SaveEmployeeBasicInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {

                    $('#hdfEmpId').val('');
                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#hdfEmpId').val(d.oCommonResult.Id);
                        clearAllData();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfEmpId').val('');
                }
            });
        }
    });


    // Update EmployeeInfo
    $('#btnEdit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = updateBindata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/UpdateEmployeeBasicInfo",//'@Url.Action("UpdateEmployeeBasicInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: data,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#btnSave').show();
                        $('#btnEdit').hide();
                        $('#btnDelete').hide();
                        document.getElementById('showMessage').style.color = "green";
                        clearAllData();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });

    // Delete EmployeeInfo
    $('#btnDelete').click(function () {
        var data = deleteBindata();
        $.ajax({
            url: window.applicationBaseUrl + "HR/Employee/DeleteEmployeeBasicInfo",//'@Url.Action("DeleteEmployeeBasicInfo", "Employee", new { Area = "HR" })',
            type: "POST",
            data: data,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function (d) {
                if (d.oCommonResult.Status === true) {
                    oTable.ajax.reload();
                    $('#btnSave').show();
                    $('#btnEdit').hide();
                    $('#btnDelete').hide();
                    document.getElementById('showMessage').style.color = "green";
                    clearAllData();
                }
                document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
            },
            error: function () {
                document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
            }
        });
    });

    function checkValidation() {
        var isValid = true;
        if ($('#EmpCode').val().trim() === '') {
            isValid = false;
            $('#EmpCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#FirstName').val().trim() === '') {
            isValid = false;
            $('#FirstName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#FirstName').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#DeptId').val().trim() === '0') {
            isValid = false;
            $('#DeptId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DeptId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#DesigId').val().trim() === '0') {
            isValid = false;
            $('#DesigId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DesigId').parent().prev().find('span').css('visibility', 'hidden');
        }
        //if ($('#BranchId').val().trim() === '0') {
        //    isValid = false;
        //    $('#BranchId').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#BranchId').parent().prev().find('span').css('visibility', 'hidden');
        //}
        return isValid;
    }


    // Save Data
    function bindata() {
        var fd = new FormData();
        fd.append('EmpCode', $('#EmpCode').val());
        fd.append('FirstName', $('#FirstName').val());
        fd.append('LastName', $('#LastName').val());
        fd.append('DesigId', $('#DesigId').val());
        fd.append('DeptId', $('#DeptId').val());
        fd.append('FunctionalId', $('#FunctionalDesignation').val());
        fd.append('BranchId', $('#BranchId').val());
        fd.append('CurrentSalary', $('#CurrentSalary').val());
        fd.append('JoiningDate', $('#JoiningDate').val());
        fd.append('DOB', $('#DOB').val());
        fd.append('Religion', $('#Religion').val());
        fd.append('Gender', $('#Gender').val());
        fd.append('MaritalStatus', $('#MaritalStatus').val());
        fd.append('FatherName', $('#FatherName').val());
        fd.append('MotherName', $('#MotherName').val());
        fd.append('NID', $('#NID').val());
        fd.append('EmailId', $('#EmailId').val());
        fd.append('MobileNo', $('#MobileNo').val());
        fd.append('PresentAddress', $('#PresentAddress').val());
        fd.append('PermanentAddress', $('#PermanentAddress').val());
        fd.append('ImageFile', document.getElementById('ImageFile').files[0]);
        return fd;
    }

    // Update Data
    function updateBindata() {
        var fd = new FormData();
        fd.append('EmpId', $('#hdfEmpId').val());
        fd.append('EmpCode', $('#EmpCode').val());
        fd.append('FirstName', $('#FirstName').val());
        fd.append('LastName', $('#LastName').val());
        fd.append('DesigId', $('#DesigId').val());
        fd.append('FunctionalId', $('#FunctionalDesignation').val());
        fd.append('DeptId', $('#DeptId').val());
        fd.append('BranchId', $('#BranchId').val());
        fd.append('CurrentSalary', $('#CurrentSalary').val());
        fd.append('JoiningDate', $('#JoiningDate').val());
        fd.append('DOB', $('#DOB').val());
        fd.append('Religion', $('#Religion').val());
        fd.append('Gender', $('#Gender').val());
        fd.append('MaritalStatus', $('#MaritalStatus').val());
        fd.append('FatherName', $('#FatherName').val());
        fd.append('MotherName', $('#MotherName').val());
        fd.append('NID', $('#NID').val());
        fd.append('EmailId', $('#EmailId').val());
        fd.append('MobileNo', $('#MobileNo').val());
        fd.append('PresentAddress', $('#PresentAddress').val());
        fd.append('PermanentAddress', $('#PermanentAddress').val());
        var file = document.getElementById('ImageFile').files[0];
        if (file != null) {
            fd.append('ImageFile', file);
        } else {
            fd.append('ImagePath', $('#hdfExisitngImagePath').val());
        }
        return fd;
    }

    // Delete EmployeeInfo
    function deleteBindata() {
        var fd = new FormData();
        fd.append('EmpId', $('#hdfEmpId').val());
        return fd;
    }

    // Clear data
    function clearAllData() {
        $('#EmpCode').removeAttr('readonly');
        $('#EmpCode').val('');
        $('#EdEmpCode').val('');
        $('#FirstName').val('');
        $('#LastName').val('');
        $('#DesigId').val('0');
        $('#FunctionalDesignation').val('0');
        $('#DeptId').val('0');
        $('#BranchId').val('0');
        $('#CurrentSalary').val('');
        $('#JoiningDate').val('');
        $('#DOB').val('');
        $('#Religion').val('');
        $('#Gender').val('');
        $('#MaritalStatus').val('');
        $('#FatherName').val('');
        $('#MotherName').val('');
        $('#NID').val('');
        $('#EmailId').val('');
        $('#MobileNo').val('');
        $('#PresentAddress').val('');
        $('#PermanentAddress').val('');
        $('#ImageFile').val('');
        $('#img-upload').attr('src', '');
        $('#hdfExisitngImagePath').val('');
        $('.btn-file :file').parents('.input-group').find(':text').val('');
        ClearEducationFields();
        ClearExperienceFields();
    }

    //Duplicate Check Employee Code
    $("#EmpCode").keyup(function () {
        var empCode = $("#EmpCode").val();
        var status = $("#showMessage");
        var user = $.trim(empCode);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/Employee/GetEmpCodeCheck", { empCode: empCode },//'@Url.Action("GetEmpCodeCheck", "Employee", new {Area = "HR" })', { empCode: empCode },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + empCode + "</b>' is not exist !</font>");
                                $('#btnSave').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + empCode + "</b>' is exist !</font>");
                                $('#btnSave').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }

    });


    // Duplicate Check Employee NID
    $("#NID").keyup(function () {
        var nid = $("#NID").val();
        var status = $("#showMessage");
        var user = $.trim(nid);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "HR/Employee/GetEmpNidCheck", { nid: nid },//'@Url.Action("GetEmpNidCheck", "Employee", new {Area = "HR" })', { nid: nid },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + nid + "</b>' is not exist !</font>");
                                $('#btnSave').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + nid + "</b>' is exist !</font>");
                                $('#btnSave').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }
    });
    //end of basic info

    //education
    $('#btnEditEducation').hide();
    $('#btnDeleteEducation').hide();
    $('#PassingYear').append($('<option />').val('').html('--Select--'));
    for (var i = new Date().getFullYear() ; i > 1960; i--) {
        $('#PassingYear').append($('<option />').val(i).html(i));
    }

    function educationTable() {
        oEducationTable = $('#educationalInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/Employee/GetEmployeeEducationalInfo",//'@Url.Action("GetEmployeeEducationalInfo", "Employee", new {Area = "HR" })',
                "type": "get",
                "data": {
                    "EmpCode": $('#EdEmpCode').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "DegreeName" },
                { "data": "Grade" },
                { "data": "InstituteName" },
                { "data": "PassingYear" }
            ],
            "order": [[0, "desc"]]
        });
        $('#educationalInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oEducationTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindEducationDataToControls(oEducationTable.row(this).data());
        });
    }

    function bindEducationDataToControls(data) {
        $('#hdfEducationId').val(data.Id);
        $('#DegreeName').val(data.DegreeName);
        $('#InstituteName').val(data.InstituteName);
        $('#Grade').val(data.Grade);
        $('#PassingYear').val(data.PassingYear);
        $('#Description').val(data.Description);
        $('#btnEditEducation').show();
        $('#btnDeleteEducation').show();
        $('#btnSaveEducation').hide();
    }

    $('#btnSaveEducation').click(function () {
        var isValid = checkEducationFieldsValidation();
        if (isValid) {
            var data = BindEducationdata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/SaveEmployeeEducationInfo",//'@Url.Action("SaveEmployeeEducationInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oEducationTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearEducationFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnEditEducation').click(function () {
        var isValid = checkEducationFieldsValidation();
        if (isValid) {
            var data = BindEducationdata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/UpdateEmployeeEducationInfo",//'@Url.Action("UpdateEmployeeEducationInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oEducationTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearEducationFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnDeleteEducation').click(function () {
        var isValid = checkEducationFieldsValidation();
        if (isValid) {
            var data = BindEducationdata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/DeleteEmployeeEducationInfo",//'@Url.Action("DeleteEmployeeEducationInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oEducationTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearEducationFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnclearEducation').click(function () {
        ClearEducationFields();
    });

    function checkEducationFieldsValidation() {
        var isValid = true;
        if ($('#EdEmpCode').val().trim() === '') {
            isValid = false;
            document.getElementById('showMessage').innerHTML = 'Select Employee First.';
            return isValid;
        }
        if ($('#DegreeName').val().trim() === '') {
            isValid = false;
            $('#DegreeName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DegreeName').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#InstituteName').val().trim() === '') {
            isValid = false;
            $('#InstituteName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#InstituteName').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#Grade').val().trim() === '') {
            isValid = false;
            $('#Grade').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Grade').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#PassingYear').val().trim() === '') {
            isValid = false;
            $('#PassingYear').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#PassingYear').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    function BindEducationdata() {
        var data = {
            Id: $('#hdfEducationId').val(),
            EmpCode: $('#EdEmpCode').val(),
            DegreeName: $('#DegreeName').val(),
            InstituteName: $('#InstituteName').val(),
            Grade: $('#Grade').val(),
            PassingYear: $('#PassingYear option:selected').val(),
            Description: $('#Description').val()
        }
        return data;
    }

    function ClearEducationFields() {
        $('#hdfEducationId').val('');
        $('#DegreeName').val('');
        $('#InstituteName').val('');
        $('#Grade').val('');
        $('#PassingYear').val('');
        $('#Description').val('');
        $('#btnEditEducation').hide();
        $('#btnDeleteEducation').hide();
        $('#btnSaveEducation').show();
    }
    //end of education

    //experience
    $('#btnEditExperience').hide();
    $('#btnDeleteExperience').hide();

    function ExperienceTable() {
        oExperienceTable = $('#experienceInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/Employee/GetEmployeeExperinceInfo",//'@Url.Action("GetEmployeeExperinceInfo", "Employee", new {Area = "HR" })',
                "type": "get",
                "data": {
                    "EmpCode": $('#ExEmpCode').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "InstituteName" },
                { "data": "YearOfExperience" },
                { "data": "Position" },
                { "data": "From" },
                { "data": "To" }
            ],
            "order": [[0, "desc"]],
        });
        $('#experienceInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oExperienceTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindExperincenDataToControls(oExperienceTable.row(this).data());
        });
    }

    function bindExperincenDataToControls(data) {
        $('#hdfExperienceId').val(data.Id);
        $('#Institute').val(data.InstituteName);
        $('#YearOfExperience').val(data.YearOfExperience);
        $('#Position').val(data.Position);
        $('#From').val(data.From);
        $('#To').val(data.To);
        $('#btnEditExperience').show();
        $('#btnDeleteExperience').show();
        $('#btnSaveExperience').hide();
    }

    $('#btnSaveExperience').click(function () {
        var isValid = CheckExperinceFieldsValidation();
        if (isValid) {
            var data = BindExperiencedata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/SaveEmployeeExperinceInfo",//'@Url.Action("SaveEmployeeExperinceInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oExperienceTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearExperienceFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnEditExperience').click(function () {
        var isValid = CheckExperinceFieldsValidation();
        if (isValid) {
            var data = BindExperiencedata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/UpdateEmployeeExperinceInfo",//'@Url.Action("UpdateEmployeeExperinceInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oExperienceTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearExperienceFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnDeleteExperience').click(function () {
        var isValid = CheckExperinceFieldsValidation();
        if (isValid) {
            var data = BindExperiencedata();
            $.ajax({
                url: window.applicationBaseUrl + "HR/Employee/DeleteEmployeeExperienceInfo",//'@Url.Action("DeleteEmployeeExperienceInfo", "Employee", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oExperienceTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearExperienceFields();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });
        }
    });
    $('#btnclearExperience').click(function () {
        ClearExperienceFields();
    });

    function CheckExperinceFieldsValidation() {
        var isValid = true;
        if ($('#ExEmpCode').val().trim() === '') {
            isValid = false;
            document.getElementById('showMessage').innerHTML = 'Select Employee First.';
            return isValid;
        }
        if ($('#Institute').val().trim() === '') {
            isValid = false;
            $('#Institute').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Institute').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#YearOfExperience').val().trim() === '') {
            isValid = false;
            $('#YearOfExperience').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#YearOfExperience').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#Position').val().trim() === '') {
            isValid = false;
            $('#Position').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Position').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    function BindExperiencedata() {
        var data = {
            Id: $('#hdfExperienceId').val(),
            EmpCode: $('#ExEmpCode').val(),
            InstituteName: $('#Institute').val(),
            YearOfExperience: $('#YearOfExperience').val(),
            Position: $('#Position').val(),
            From: $('#From').val(),
            To: $('#To').val()
        }
        return data;
    }

    function ClearExperienceFields() {
        $('#hdfExperienceId').val('');
        $('#Institute').val('');
        $('#YearOfExperience').val('');
        $('#Position').val('');
        $('#From').val('');
        $('#To').val('');
        $('#btnEditExperience').hide();
        $('#btnDeleteExperience').hide();
        $('#btnSaveExperience').show();
    }
    //end of experience
});

function getEmployeeCode() {
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "HR/Employee/GetEmployeeCode",//'@Url.Action("GetEmployeeCode", "Employee", new { Area = "HR" })',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(),
        success: function (data) {
            $('#EmpCode').val(data);
        }
    });
}


var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
];

function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}