﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.DAL;


namespace TonyMultipurpose.Areas.Employee.BLL
{
    public class ScanDocumentBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForScanDocument(DbDataReader objDataReader, ScanDocument objScanDocument)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "EmployeeId":
                        if (!Convert.IsDBNull(objDataReader["EmployeeId"]))
                        {
                            objScanDocument.EmployeeId = Convert.ToByte(objDataReader["EmployeeId"]);
                        }
                        break;
                    case "EmployeeName":
                        if (!Convert.IsDBNull(objDataReader["EmployeeName"]))
                        {
                            objScanDocument.EmployeeName = objDataReader["EmployeeName"].ToString();
                        }
                        break;
                    case "DocumentTypeId":
                        if (!Convert.IsDBNull(objDataReader["DocumentTypeId"]))
                        {
                            objScanDocument.DocumentTypeId = Convert.ToByte(objDataReader["DocumentTypeId"]);
                        }
                        break;
                    case "TypeName":
                        if (!Convert.IsDBNull(objDataReader["TypeName"]))
                        {
                            objScanDocument.TypeName = objDataReader["TypeName"].ToString();
                        }
                        break;
                    case "Image":
                        if (!Convert.IsDBNull(objDataReader["Image"]))
                        {
                            objScanDocument.Image = (byte[])(objDataReader["Image"]);
                        }
                        break;
                    case "DocumentName":
                        if (!Convert.IsDBNull(objDataReader["DocumentName"]))
                        {
                            objScanDocument.DocumentName = objDataReader["DocumentName"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objScanDocument.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objScanDocument.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objScanDocument.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objScanDocument.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objScanDocument.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objScanDocument.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objScanDocument.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objScanDocument.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public string SaveScanDocument(ScanDocument objScanDocument)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", objScanDocument.EmployeeId);
            //objDbCommand.AddInParameter("ExamName", objScanDocument.EmployeeName);
            objDbCommand.AddInParameter("DocumentTypeId", objScanDocument.DocumentTypeId);
            objDbCommand.AddInParameter("DocumentName", objScanDocument.DocumentName);
            objDbCommand.AddInParameter("Image", objScanDocument.Image);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateScanDocument", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        //public List<ScanDocument> GetAllEmployeeInfoById(int? employeeId)
        //{
        //    objDataAccess = DataAccess.NewDataAccess();
        //    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
        //    DbDataReader objDbDataReader = null;

        //    List<ScanDocument> objScanDocumentList = new List<ScanDocument>();

        //    ScanDocument objScanDocument = new ScanDocument();

        //    try
        //    {
        //        objDbCommand.AddInParameter("EmployeeId", employeeId);
        //        objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllEmployeeNameById]", CommandType.StoredProcedure);

        //        if (objDbDataReader.HasRows)
        //        {
        //            while (objDbDataReader.Read())
        //            {
        //                objScanDocument = new ScanDocument();
        //                objScanDocument.EmployeeId = Convert.ToInt16(objDbDataReader["EmployeeId"].ToString());
        //                objScanDocument.EmployeeName = objDbDataReader["EmployeeName"].ToString();
        //                objScanDocument.DocumentTypeId = Convert.ToByte(objDbDataReader["DocumentTypeId"].ToString());
        //                objScanDocument.TypeName = objDbDataReader["TypeName"].ToString();
        //                objScanDocument.DocumentName = objDbDataReader["DocumentName"].ToString();
        //                //objScanDocument.Image = (byte[])objDbDataReader["Image"];


        //                objScanDocumentList.Add(objScanDocument);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error : " + ex.Message);
        //    }
        //    finally
        //    {
        //        if (objDbDataReader != null)
        //        {
        //            objDbDataReader.Close();
        //        }
        //        objDataAccess.Dispose(objDbCommand);
        //    }

        //    return objScanDocumentList;
        //}
    }
    }
