﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.Areas.MonthlyBenefit.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.MonthlyBenefit.BLL
{
    public class MBBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<string> GetAllMBAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMBAccountNoForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountNumberList;
        }
        private void BuildModelForCustomer(DbDataReader objDataReader, MBModel objCustomerModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objCustomerModel.Id = Convert.ToInt32(objDataReader["Id"]);
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objCustomerModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objCustomerModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    //case "FatherName":
                    //    if (!Convert.IsDBNull(objDataReader["FatherName"]))
                    //    {
                    //        objCustomerModel.FatherName = objDataReader["FatherName"].ToString();
                    //    }
                    //    break;
                    //case "MotherName":
                    //    if (!Convert.IsDBNull(objDataReader["MotherName"]))
                    //    {
                    //        objCustomerModel.MotherName = objDataReader["MotherName"].ToString();
                    //    }
                    //    break;
                    //case "CustomerEntryDate":
                    //    if (!Convert.IsDBNull(objDataReader["CustomerEntryDate"]))
                    //    {
                    //        objCustomerModel.CustomerEntryDate = Convert.ToDateTime(objDataReader["CustomerEntryDate"].ToString());
                    //    }
                    //    break;
                    //case "SpouseName":
                    //    if (!Convert.IsDBNull(objDataReader["SpouseName"]))
                    //    {
                    //        objCustomerModel.SpouseName = objDataReader["SpouseName"].ToString();
                    //    }
                    //    break;
                    //case "TradeLicenseNo":
                    //    if (!Convert.IsDBNull(objDataReader["TradeLicenseNo"]))
                    //    {
                    //        objCustomerModel.TradeLicenseNo = objDataReader["TradeLicenseNo"].ToString();
                    //    }
                    //    break;
                    //case "TradeLicenseDate":
                    //    if (!Convert.IsDBNull(objDataReader["TradeLicenseDate"]))
                    //    {
                    //        objCustomerModel.TradeLicenseDate = Convert.ToDateTime(objDataReader["TradeLicenseDate"].ToString());
                    //    }
                    //    break;
                    //case "RegistrationNo":
                    //    if (!Convert.IsDBNull(objDataReader["RegistrationNo"]))
                    //    {
                    //        objCustomerModel.RegistrationNo = objDataReader["RegistrationNo"].ToString();
                    //    }
                    //    break;
                    //case "RegistrationAuthority":
                    //    if (!Convert.IsDBNull(objDataReader["RegistrationAuthority"]))
                    //    {
                    //        objCustomerModel.RegistrationAuthority = objDataReader["RegistrationAuthority"].ToString();
                    //    }
                    //    break;
                    //case "Nationality":
                    //    if (!Convert.IsDBNull(objDataReader["Nationality"]))
                    //    {
                    //        objCustomerModel.Nationality = objDataReader["Nationality"].ToString();
                    //    }
                    //    break;
                    //case "NationalId":
                    //    if (!Convert.IsDBNull(objDataReader["NationalId"]))
                    //    {
                    //        objCustomerModel.NationalId = objDataReader["NationalId"].ToString();
                    //    }
                    //    break;
                    //case "DateOfBirth":
                    //    if (!Convert.IsDBNull(objDataReader["DateOfBirth"]))
                    //    {
                    //        objCustomerModel.DateOfBirth = Convert.ToDateTime(objDataReader["DateOfBirth"].ToString());
                    //    }
                    //    break;
                    //case "Gender":
                    //    if (!Convert.IsDBNull(objDataReader["Gender"]))
                    //    {
                    //        objCustomerModel.Gender = objDataReader["Gender"].ToString();
                    //    }
                    //    break;
                    //case "OccupationAndPosition":
                    //    if (!Convert.IsDBNull(objDataReader["OccupationAndPosition"]))
                    //    {
                    //        objCustomerModel.OccupationAndPosition = objDataReader["OccupationAndPosition"].ToString();
                    //    }
                    //    break;
                    //case "PassportNo":
                    //    if (!Convert.IsDBNull(objDataReader["PassportNo"]))
                    //    {
                    //        objCustomerModel.PassportNo = objDataReader["PassportNo"].ToString();
                    //    }
                    //    break;
                    //case "PassportExpireDate":
                    //    if (!Convert.IsDBNull(objDataReader["PassportExpireDate"]))
                    //    {
                    //        objCustomerModel.PassportExpireDate = Convert.ToDateTime(objDataReader["PassportExpireDate"].ToString());
                    //    }
                    //    break;
                    //case "TINNo":
                    //    if (!Convert.IsDBNull(objDataReader["TINNo"]))
                    //    {
                    //        objCustomerModel.TINNo = objDataReader["TINNo"].ToString();
                    //    }
                    //    break;
                    //case "DrivingLicenseNo":
                    //    if (!Convert.IsDBNull(objDataReader["DrivingLicenseNo"]))
                    //    {
                    //        objCustomerModel.DrivingLicenseNo = objDataReader["DrivingLicenseNo"].ToString();
                    //    }
                    //    break;
                    //case "PresentAddress":
                    //    if (!Convert.IsDBNull(objDataReader["PresentAddress"]))
                    //    {
                    //        objCustomerModel.PresentAddress = objDataReader["PresentAddress"].ToString();
                    //    }
                    //    break;
                    //case "PermanentAddress":
                    //    if (!Convert.IsDBNull(objDataReader["PermanentAddress"]))
                    //    {
                    //        objCustomerModel.PermanentAddress = objDataReader["PermanentAddress"].ToString();
                    //    }
                    //    break;
                    //case "OfficeAddress":
                    //    if (!Convert.IsDBNull(objDataReader["OfficeAddress"]))
                    //    {
                    //        objCustomerModel.OfficeAddress = objDataReader["OfficeAddress"].ToString();
                    //    }
                    //    break;
                    //case "Home":
                    //    if (!Convert.IsDBNull(objDataReader["Home"]))
                    //    {
                    //        objCustomerModel.Home = objDataReader["Home"].ToString();
                    //    }
                    //    break;
                    //case "Mobile":
                    //    if (!Convert.IsDBNull(objDataReader["Mobile"]))
                    //    {
                    //        objCustomerModel.Mobile = objDataReader["Mobile"].ToString();
                    //    }
                    //    break;
                    //case "Office":
                    //    if (!Convert.IsDBNull(objDataReader["Office"]))
                    //    {
                    //        objCustomerModel.Office = objDataReader["Office"].ToString();
                    //    }
                    //    break;
                    //case "Email":
                    //    if (!Convert.IsDBNull(objDataReader["Email"]))
                    //    {
                    //        objCustomerModel.Email = objDataReader["Email"].ToString();
                    //    }
                    //    break;
                    //case "CustomerIncomeSource":
                    //    if (!Convert.IsDBNull(objDataReader["CustomerIncomeSource"]))
                    //    {
                    //        objCustomerModel.CustomerIncomeSource = objDataReader["CustomerIncomeSource"].ToString();
                    //    }
                    //    break;
                    //case "CustomerImage":
                    //    if (!Convert.IsDBNull(objDataReader["CustomerImage"]))
                    //    {
                    //        var imagePath = objDataReader["CustomerImage"].ToString();
                    //        if (imagePath.Contains("Uploads"))
                    //        {
                    //            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                    //            imagePath = "/" + imagePath;
                    //            objCustomerModel.CustomerImage = imagePath;
                    //        }
                    //        else
                    //        {
                    //            objCustomerModel.CustomerImage = "\\Not Found\\";
                    //        }

                    //    }
                    //    else
                    //    {
                    //        objCustomerModel.CustomerImage = "\\Not Found\\";
                    //    }
                    //    break;
                    //case "CompanyId":
                    //    if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                    //    {
                    //        objCustomerModel.CompanyId = Convert.ToInt16(objDataReader["CompanyId"]);
                    //    }
                    //    break;
                    //case "BranchId":
                    //    if (!Convert.IsDBNull(objDataReader["BranchId"]))
                    //    {
                    //        objCustomerModel.BranchId = Convert.ToInt16(objDataReader["BranchId"]);
                    //    }
                    //    break;
                    //case "IsApproved":
                    //    if (!Convert.IsDBNull(objDataReader["IsApproved"]))
                    //    {
                    //        objCustomerModel.IsApproved = Convert.ToBoolean(objDataReader["IsApproved"].ToString());
                    //    }
                    //    break;
                    //case "IsDeleted":
                    //    if (!Convert.IsDBNull(objDataReader["IsDeleted"]))
                    //    {
                    //        objCustomerModel.IsDeleted = Convert.ToBoolean(objDataReader["IsDeleted"].ToString());
                    //    }
                    //    break;
                    //case "IsActive":
                    //    if (!Convert.IsDBNull(objDataReader["IsActive"]))
                    //    {
                    //        objCustomerModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                    //    }
                    //    break;
                    //case "UserStatus":
                    //    if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                    //    {
                    //        objCustomerModel.UserStatus = objDataReader["UserStatus"].ToString();
                    //    }
                    //    break;
                    //case "CreatedBy":
                    //    if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                    //    {
                    //        objCustomerModel.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                    //    }
                    //    break;
                    //case "CreatedDate":
                    //    if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                    //    {
                    //        objCustomerModel.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                    //    }
                    //    break;
                    //case "UpdatedBy":
                    //    if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                    //    {
                    //        objCustomerModel.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                    //    }
                    //    break;
                    //case "UpdatedDate":
                    //    if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                    //    {
                    //        objCustomerModel.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                    //    }
                    //    break;
                    //case "SortedBy":
                    //    if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                    //    {
                    //        objCustomerModel.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                    //    }
                    //    break;
                    //case "Remarks":
                    //    if (!Convert.IsDBNull(objDataReader["Remarks"]))
                    //    {
                    //        objCustomerModel.Remarks = objDataReader["Remarks"].ToString();
                    //    }
                    //    break;
                    default:
                        break;
                }
            }
        }
        public string ApproveMB(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                objDbCommand.AddInParameter("Id", id);

                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "uspApproveMonthlyBenefit", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";

        }
        public List<MBModel> GetAllMBInfo(MBModel Customer)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<MBModel> objCustomerModelList = new List<MBModel>();
            MBModel objMBModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);
                objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "uspGetAllMonthlyBenefitInfo", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                   
                    while (objDbDataReader.Read())
                    {
                        objMBModel = new MBModel();
                        objMBModel.Particular = objDbDataReader["Particular"].ToString();
                        objMBModel.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                        objMBModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objMBModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objMBModel.TransactionType = objDbDataReader["TransactionType"].ToString();
                        objMBModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objMBModel.Username = objDbDataReader["Username"].ToString();
                        objMBModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        objMBModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                        objCustomerModelList.Add(objMBModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCustomerModelList;
        }
        /// get all mb info for account number
        public MBModel GetMBAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            MBModel objMBModel = new MBModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMBAccountInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                      
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objMBModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objMBModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objMBModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            objMBModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                            objMBModel.OpeningDate = Convert.ToDateTime(objDbDataReader["OpeningDate"].ToString());
                            objMBModel.AccruedBalance = objDbDataReader["AccruedBalance"].ToString();
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objMBModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objMBModel.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objMBModel;
        }        
        /// Save MB Withdraw Transaction        
        public void SaveMBWithdrawTransaction(MBModel mbModel)
        {
            int noOfAffacted = 0;
            mbModel.TransactionId = GenerateTransactionId(mbModel);

            if (mbModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", mbModel.AccountNumber);
                objDbCommand.AddInParameter("TransactionId", mbModel.TransactionId);
                objDbCommand.AddInParameter("TransactionDate", mbModel.TransactionDate);
              //  objDbCommand.AddInParameter("TransactionType", mbModel.TransactionType);
                objDbCommand.AddInParameter("CustomerName", mbModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", mbModel.CustomerId);
                objDbCommand.AddInParameter("MonthlyBenefitAmount", mbModel.MonthlyBenefitAmount);
                objDbCommand.AddInParameter("Balance", mbModel.Balance);
                objDbCommand.AddInParameter("MatureDate", mbModel.MatureDate);
                objDbCommand.AddInParameter("COAId", mbModel.COAId);

                objDbCommand.AddInParameter("BranchId", mbModel.BranchId);
                objDbCommand.AddInParameter("CompanyId", mbModel.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", mbModel.CreatedBy);
                objDbCommand.AddInParameter("CreatedDate", mbModel.CreatedDate);
                objDbCommand.AddInParameter("IsApproved", mbModel.IsApproved);
                objDbCommand.AddInParameter("IsActive", mbModel.IsActive);

                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveMBWithdrawTransaction]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }
        //Generate Transaction Id
        private string GenerateTransactionId(MBModel mbModel)
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            if (mbModel.AccountNumber != null)
            {
                objDbDataReader = null;
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                int Id = 0;

                string year = Convert.ToString(DateTime.Now.Year);
                string days = Convert.ToString(DateTime.Now.DayOfYear);
                //var prefix = string.Concat(year + days);
                string prefix = year + days;

                objDbCommand.AddInParameter("FieldName", "TransactionId");
                objDbCommand.AddInParameter("TableName", "MonthlyBenefit");
                objDbCommand.AddInParameter("Prefix", prefix);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                    CommandType.StoredProcedure);
                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        transactionId = prefix;
                        Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        transactionId += Convert.ToString(Id).PadLeft(4, '0');
                    }
                }
                objDbDataReader.Close();
            }
            return transactionId;
        }
        /// get all premature mb info for account number
        public MBModel GetPrematureMbAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            MBModel objMBModel = new MBModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetPrematureMBAccountInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objMBModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objMBModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objMBModel.DepositAmount = Convert.ToDecimal(objDbDataReader["DepositAmount"].ToString());
                            //string rA = Convert.ToString(objDbDataReader["ReturnAmount"]);
                            //if (string.IsNullOrEmpty(rA))
                            //{
                            //    objMBModel.ReturnAmount =0;
                            //}
                            //objMBModel.ReturnAmount = (rA == "" ? 0 : rA);
                            objMBModel.ReturnAmount = Convert.ToDecimal(objDbDataReader["ReturnAmount"].ToString());
                            objMBModel.MonthlyBenefitAmount = Convert.ToDecimal(objDbDataReader["MonthlyBenefit"].ToString());
                            objMBModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            objMBModel.OpeningDate = Convert.ToDateTime(objDbDataReader["OpeningDate"].ToString());
                            objMBModel.AccruedBalance = objDbDataReader["AccruedBalance"].ToString();
                            //objMBModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                            //objMBModel.DurationofYear = objDbDataReader["DurationofYear"].ToString();
                            objMBModel.DurationofMonth = objDbDataReader["Duration"].ToString();
                            //objCssModel.CustomerImage = objDbDataReader["CustomerImage"].ToString();
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objMBModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objMBModel.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objMBModel;
        }
        // save monthly benefit premature encashment
        public void SaveMBPreMatureEncashmentInfo(MBModel mbModel)
        {
            int noOfAffacted = 0;
            mbModel.TransactionId = GenerateTransactionId(mbModel);

            if (mbModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", mbModel.AccountNumber);
                objDbCommand.AddInParameter("TransactionId", mbModel.TransactionId);
                objDbCommand.AddInParameter("TransactionDate", mbModel.TransactionDate);
                objDbCommand.AddInParameter("TransactionType", mbModel.TransactionType);
                objDbCommand.AddInParameter("CustomerName", mbModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", mbModel.CustomerId);
                objDbCommand.AddInParameter("SavingsInterestRate", Convert.ToDecimal(mbModel.SavingsInterestRate));
                objDbCommand.AddInParameter("ReturnAmount", mbModel.ReturnAmount);
                objDbCommand.AddInParameter("Balance", mbModel.Balance);
                objDbCommand.AddInParameter("MatureDate", mbModel.MatureDate);
                objDbCommand.AddInParameter("DepositAmount", Convert.ToDecimal(mbModel.DepositAmount));
                objDbCommand.AddInParameter("COAId", mbModel.COAId);


                objDbCommand.AddInParameter("BranchId", mbModel.BranchId);
                objDbCommand.AddInParameter("CompanyId", mbModel.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", mbModel.CreatedBy);
                objDbCommand.AddInParameter("CreatedDate", mbModel.CreatedDate);
                objDbCommand.AddInParameter("IsApproved", mbModel.IsApproved);
                objDbCommand.AddInParameter("IsActive", mbModel.IsActive);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveMBPreMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }
        // save mature encashment
        public void SaveMBMatureEncashmentInfo(MBModel mbModel)
        {
            int noOfAffacted = 0;
            mbModel.TransactionId = GenerateTransactionId(mbModel);

            if (mbModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", mbModel.AccountNumber);
                objDbCommand.AddInParameter("TransactionId", mbModel.TransactionId);
                objDbCommand.AddInParameter("TransactionDate", mbModel.TransactionDate);
                objDbCommand.AddInParameter("TransactionType", mbModel.TransactionType);
                objDbCommand.AddInParameter("CustomerName", mbModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", mbModel.CustomerId);
                objDbCommand.AddInParameter("Balance", mbModel.Balance);
                objDbCommand.AddInParameter("AccruedBalance", mbModel.AccruedBalance);
                objDbCommand.AddInParameter("MatureDate", mbModel.MatureDate);
                objDbCommand.AddInParameter("COAId", mbModel.COAId);

                objDbCommand.AddInParameter("BranchId", mbModel.BranchId);
                objDbCommand.AddInParameter("CompanyId", mbModel.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", mbModel.CreatedBy);
                objDbCommand.AddInParameter("CreatedDate", mbModel.CreatedDate);
                objDbCommand.AddInParameter("IsApproved", mbModel.IsApproved);
                objDbCommand.AddInParameter("IsActive", mbModel.IsActive);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveMBMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }
        //reject monthly benefit transaction
        public string RejectMonthleBenefitTransaction(List<MBModel> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspRejectMonthlyBenefitTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public bool GetUnAppovedMBList(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[MonthlyBenefit_GetUnAppovedMBList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetCloseMBAccount(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[MonthlyBenefit_GetCloseMBAccount]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
    }
}