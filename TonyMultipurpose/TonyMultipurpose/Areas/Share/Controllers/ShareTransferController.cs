﻿using System;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Share.BLL;
using TonyMultipurpose.Areas.Share.ViewModels;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Share.Controllers
{
    public class ShareTransferController : Controller
    {
        // GET: Share/ShareEntry
        private readonly ShareTransferBLL _objeShareTransferBll;

        public ShareTransferController()
        {
            _objeShareTransferBll = new ShareTransferBLL();
        }
        public ActionResult Index()
        {
            var shareHoldingViewModel =
                _objeShareTransferBll.GetShareHoldingInfo(Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
            var model = new ShareTransferViewModel
            {
                IssuedDate = DateTime.Now,
                PricePerShare = shareHoldingViewModel.PricePerShare
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ShareTransferViewModel objShareTransferViewModel)
        {
            if (ModelState.IsValid)
            {
                objShareTransferViewModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objShareTransferViewModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objShareTransferViewModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                _objeShareTransferBll.SaveShareTransaction(objShareTransferViewModel);
                return RedirectToAction("Index");
            }

            return View(objShareTransferViewModel);
        }

        [HttpGet]
        public ActionResult GetCustomerInfo(string id, string cType, string excludeCustId)
        {
            var customer = new CustomerViewModel()
            {
                CustomerId = id,
                CompanyId = Convert.ToInt32(SessionUtility.TMSessionContainer.CompanyId)

            };
            var data = _objeShareTransferBll.GetCustomerInfoWithExistingShareQtyByCustomerId(customer).FirstOrDefault();

            return PartialView("_Customer", data);
        }

        public JsonResult AutoCompleteCustomerId(string term, string cType, string excludeCustId)
        {
            var result = _objeShareTransferBll.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}