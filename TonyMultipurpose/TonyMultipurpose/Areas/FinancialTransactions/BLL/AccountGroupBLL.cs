﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class AccountGroupBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<AccountGroupModel> GetAllAccountGroupInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountGroupModel> _objAccountGroupList = new List<AccountGroupModel>();
            AccountGroupModel _objAccountGroup;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[AccountGroup_GetAllInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objAccountGroup = new AccountGroupModel();
                        _objAccountGroup.GroupId = Convert.ToInt16(objDbDataReader["GroupId"]);
                        _objAccountGroup.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        _objAccountGroup.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                        _objAccountGroupList.Add(_objAccountGroup);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return _objAccountGroupList;
        }

        public string SaveAccountGroupInfo(AccountGroupModel _objAccountGroup)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("GroupName", _objAccountGroup.GroupName);
            objDbCommand.AddInParameter("IsActive", _objAccountGroup.IsActive);
            objDbCommand.AddInParameter("CompanyId", _objAccountGroup.CompanyId);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[AccountGroup_SaveInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public AccountGroupModel GetAccountGroupInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            AccountGroupModel _objAccountGroup = new AccountGroupModel();

            try
            {
                objDbCommand.AddInParameter("GroupId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[AccountGroup_GetInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objAccountGroup = new AccountGroupModel();
                        _objAccountGroup.GroupId = Convert.ToInt16(objDbDataReader["GroupId"]);
                        _objAccountGroup.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        _objAccountGroup.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return _objAccountGroup;
        }

        public string UpdateAccountGroupInfo(AccountGroupModel _obAccountGroup)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("GroupId", _obAccountGroup.GroupId);
            objDbCommand.AddInParameter("GroupName", _obAccountGroup.GroupName);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[AccountGroup_UpdateInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteAccountGroup(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("GroupId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[AccountGroup_DeleteInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Deleted Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public bool GetGroupNameCheck(string GroupName)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("GroupName", GroupName);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[AccountGroup_GroupNameCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
    }
}