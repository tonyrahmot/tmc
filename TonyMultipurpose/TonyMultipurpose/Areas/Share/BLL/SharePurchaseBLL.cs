﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using TonyMultipurpose.Areas.Share.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.BLL
{
    public class SharePurchaseBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;
        private DataTable _objDataTable;
        private DbDataReader _objDbDataReader;
        private void BuildJpModel<T>(DbDataReader objDataReader, T item) where T : new()
        {
            for (var inc = 0; inc < objDataReader.FieldCount; inc++)
            {
                var type = item.GetType();
                var prop = type.GetProperty(objDataReader.GetName(inc));
                var val = objDataReader.GetValue(inc) is DBNull ? null : objDataReader.GetValue(inc);
                prop?.SetValue(item, val, null);
            }
        }

        private void BuildModelForSharePurchase<T>(DbDataReader objDataReader, T item) where T : new()
        {
            for (var inc = 0; inc < objDataReader.FieldCount; inc++)
            {
                var type = item.GetType();
                var prop = type.GetProperty(objDataReader.GetName(inc));
                var val = objDataReader.GetValue(inc) is DBNull ? null : objDataReader.GetValue(inc);
                prop?.SetValue(item, val, null);
            }
        }
        public ShareHoldingViewModel GetShareHoldingInfo(int companyId)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var model = new ShareHoldingViewModel();
            try
            {
                _objDbCommand.AddInParameter("CompanyId", companyId);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand,
                    "[dbo].[Share_GetShareHoldingInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        BuildJpModel(objDbDataReader, model);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                _objDataAccess.Dispose(_objDbCommand);
            }
            return model;
        }
        public List<CustomerViewModel> GetCustomerInfoWithExistingShareQtyByCustomerId(CustomerViewModel customerModel)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objCustomerModelList = new List<CustomerViewModel>();
            try
            {
                _objDbCommand.AddInParameter("CustomerId", customerModel.CustomerId);
                _objDbCommand.AddInParameter("CompanyId", customerModel.CompanyId);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand,
                    "[dbo].[Share_GetCustomerInfoWithExistingShareQtyByCustomerId]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        var objCustomerModel = new CustomerViewModel();
                        BuildModelForSharePurchase(objDbDataReader, objCustomerModel);
                        var imagePath = objCustomerModel.CustomerImage;

                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objCustomerModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objCustomerModel.CustomerImage = imagePath;
                        }
                        objCustomerModelList.Add(objCustomerModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                _objDataAccess.Dispose(_objDbCommand);
            }
            return objCustomerModelList;
        }

        public CommonResult SaveSharePurchase(SharePurchaseViewModel objSharePurchase)
        {
            CommonResult oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noRowCount = 0;

            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var Id = 0;

            var year = Convert.ToString(DateTime.Now.Year);
            var days = Convert.ToString(DateTime.Now.DayOfYear);
            var prefix = string.Concat(year, days);

            _objDbCommand.AddInParameter("FieldName", "TransactionId");
            _objDbCommand.AddInParameter("TableName", "ShareTransaction");
            _objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("CompanyId", objSharePurchase.CompanyId);
            _objDbCommand.AddInParameter("TransactionId", transactionId);
            _objDbCommand.AddInParameter("CustomerId", objSharePurchase.CustomerId);
            _objDbCommand.AddInParameter("PricePerShare", objSharePurchase.PricePerShare);
            _objDbCommand.AddInParameter("Amount", objSharePurchase.Amount);
            _objDbCommand.AddInParameter("@Balance", 0);
            _objDbCommand.AddInParameter("IssuedDate", objSharePurchase.IssuedDate);
            _objDbCommand.AddInParameter("VoucherNo", objSharePurchase.VoucherNo);
            _objDbCommand.AddInParameter("Particular", objSharePurchase.Particular);
            _objDbCommand.AddInParameter("BranchId", objSharePurchase.BranchId);
            _objDbCommand.AddInParameter("CreatedBy", objSharePurchase.CreatedBy);
            _objDbCommand.AddInParameter("COAId", objSharePurchase.COAId);
            _objDbCommand.AddInParameter("IsChequeEntry", objSharePurchase.IsChequePayment);
            _objDbCommand.AddInParameter("BranchName", objSharePurchase.BranchName);
            _objDbCommand.AddInParameter("BankAccountNo", objSharePurchase.BankAccountNo);
            _objDbCommand.AddInParameter("BankName", objSharePurchase.BankName);
            _objDbCommand.AddInParameter("ChequeNo", objSharePurchase.ChequeNo);
            _objDbCommand.AddInParameter("ChequeIssuedDate", objSharePurchase.ChequeIssueDate);
            try
            {
                noRowCount = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_SaveCustomerShareSoldInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Save Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                _objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return oCommonResult;
        }

        public List<string> GetAllCustomerIdForAutocomplete(string term)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objCustomerList = new List<string>();
            try
            {
                _objDbCommand.AddInParameter("CustomerId", term);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[Autocomplete_ApprovedCustomerId]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["CustomerId"] != null)
                        {
                            objCustomerList.Add(objDbDataReader["CustomerId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objCustomerList;
        }

        public List<ShareHoldingViewModel> GetCompanyShareInfo()
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            _objDataTable = new DataTable();
            List<ShareHoldingViewModel> ShareHoldingList = new List<ShareHoldingViewModel>();
            ShareHoldingViewModel objShareHolding;
            try
            {
                _objDbCommand.AddInParameter("AuthorizedShareInfoId", 0);
                _objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                _objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[Share_GetShareHoldingInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objShareHolding = new ShareHoldingViewModel
                        {
                            PricePerShare = Convert.ToDecimal(dr["PricePerShare"]),
                            SoldQty = Convert.ToDecimal(dr["SoldQty"]),
                            IssuedQty = Convert.ToDecimal(dr["IssuedQty"]),
                            Balance = Convert.ToDecimal(dr["Balance"]),
                            IssuedAmount = Convert.ToDecimal(dr["IssuedAmount"]),
                            PaidUpAmount = Convert.ToDecimal(dr["PaidUpAmount"])

                        };
                        ShareHoldingList.Add(objShareHolding);
                    }

                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return ShareHoldingList;
        }

    }
}