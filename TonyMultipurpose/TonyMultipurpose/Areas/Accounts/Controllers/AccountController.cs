﻿using Rotativa.Core;
using Rotativa.Core.Options;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.Areas.Customer.BLL;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.Controllers
{
    [AuthenticationFilter]
    public class AccountController : Controller
    {
        AccountBLL objAccountBll = new AccountBLL();
        Account objAccount = new Account();
        Nominee objNomineeModel = new Nominee();

        // GET: Accounts/Account
        public ActionResult Index()
        {
            objAccount.IsApproved = true;
            objAccount.IsDeleted = false;
            objAccount.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            var model = new BothAccountViewModel
            {
                ActiveAccounts = objAccountBll.GetAllAccounts(objAccount).Where(c => c.IsActive).ToList(),
                ClosedAccounts = objAccountBll.GetAllAccounts(objAccount).Where(a => a.IsActive == false).ToList(),
                AccountCountViewModels = objAccountBll.GetAllLiveClosedAccountInfo()
            };
            return View(model);
        }

        public JsonResult GetAccontTypeByAccountTitle()
        {
            var data = objAccountBll.GetAccontTypeByAccountTitle();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInterestRateByAccountType(string AccountType)
        {
            var data = objAccountBll.GetInterestRateByAccountType(AccountType);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AutoCompleteCustomerId(string term)
        {
            var result = objAccountBll.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objAccountBll.GetAccountNumbersForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_COAaccountCodeInfo()
        {
            var data = objAccountBll.Get_COAaccountCodeInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerInfo(string customerId)
        {
            CustomerBLL objCustomerBll = new CustomerBLL();
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = true;
            customer.IsDeleted = false;
            var data = objCustomerBll.GetAllCustomerInfo(customer).Where(a => a.CustomerId == customerId).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save(string id)
        {
            objAccount = new Account();
            return View(objAccount);
        }

        [HttpPost]
        public ActionResult Save(Account objAccount)
        {
            var oCommonResult = new CommonResult();
            var path = string.Empty;
            if (ModelState.IsValid)
            {
                if (objAccount.AccountNumber == null)
                {
                    objAccount.AccountNumber = objAccountBll.GenerateAccountNumber(objAccount);
                }
                objAccount.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objAccount.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objAccount.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objAccount.CreatedDate = DateTime.Now;
                objAccount.IsActive = true;
                objAccount.IsApproved = false;
                objAccount.IsDeleted = false;

                var nomineeCount = objAccount.Nominees.Count();
                if (nomineeCount > 0)
                {
                    for (int i = 0; i < nomineeCount; i++)
                    {
                        if (objAccount.Nominees.ElementAt(i).NomineeImage == "True")
                        {
                            var rootPath = "~\\Uploads\\Nominee\\";
                            string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                            if (!Directory.Exists(rootFolderPath))
                            {
                                Directory.CreateDirectory(rootFolderPath);

                            }
                            path = Path.Combine(rootFolderPath, string.Concat(objAccount.AccountNumber, "_", objAccount.Nominees.ElementAt(i).NomineePosition, ".jpg"));
                            objAccount.Nominees.ElementAt(i).NomineeImage = path;
                        }
                    }
                }
                oCommonResult = objAccountBll.SaveAccount(objAccount);
            }

            return new JsonResult { Data = new { status = oCommonResult.Status, oCommonResult, nominees = objAccount.Nominees } };
            //return Json(oCommonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveNomineeImageFile(string path, HttpPostedFileBase file)
        {

            bool status = false;
            try
            {
                file.SaveAs(path);
                status = true;
            }
            catch (Exception)
            {
                status = false;
            }
            var obj = new { status = status };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            objAccount = objAccountBll.GetAccountInfoById(id);
            if (objAccount.AccountNumber != null)
            {
                return View(objAccount);
            }
            else
            {
                return View("Save");
            }
        }

        [HttpPost]
        public ActionResult Edit(Account objAccount)
        {
            var oCommonResult = new CommonResult();
            var path = string.Empty;
            if (ModelState.IsValid)
            {
                objAccount.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objAccount.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objAccount.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objAccount.UpdatedDate = DateTime.Now;
                var nomineeCount = objAccount.Nominees.Count();
                if (nomineeCount > 0)
                {
                    for (int i = 0; i < nomineeCount; i++)
                    {
                        if (objAccount.Nominees.ElementAt(i).NomineeImage == "True")
                        {
                            var rootPath = "~\\Uploads\\Nominee\\";
                            string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                            if (!Directory.Exists(rootFolderPath))
                            {
                                Directory.CreateDirectory(rootFolderPath);
                            }
                            var imageFile = string.Concat(objAccount.AccountNumber, "_", objAccount.Nominees.ElementAt(i).NomineePosition, ".jpg");
                            string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, imageFile);
                            foreach (string file in fileList)
                            {
                                System.IO.File.Delete(file);
                            }
                            path = Path.Combine(rootFolderPath, imageFile);
                            objAccount.Nominees.ElementAt(i).NomineeImage = path;
                        }
                    }
                }
                oCommonResult = objAccountBll.UpdateAccount(objAccount);
            }
            return new JsonResult { Data = new { status = oCommonResult.Status, oCommonResult, nominees = objAccount.Nominees } };

        }

        public ActionResult Delete(string id)
        {
            objAccountBll.DeleteAccount(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Details(string id)
        {
            objAccount = objAccountBll.GetAccountInfoById(id);
            return View(objAccount);
        }
        #region print 
        public ActionResult CreatePDF(string AccountNumber, int AccountTypeId)
        {
            var root = Server.MapPath("~/PDF/");
            Account account;
            if (AccountTypeId == 2)
            {
                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.SavingWithInterastPDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("SavingWithInterastPDF", account);
            }
            else if (AccountTypeId == 4)
            {
                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.FixedDepositPDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("FixedDepositPDF", account);

            }
            else if (AccountTypeId == 5)
            {
                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.DoubleBenifitPDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("DoubleBenifitPDF", account);
            }
            else if (AccountTypeId == 10)
            {
                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.ContributorySavingsSchemePDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("ContributorySavingsSchemePDF", account);
            }
            else if (AccountTypeId == 6)
            {
                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.MonthlyBenefitDepositSchemePDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("MonthlyBenefitDepositSchemePDF", account);
            }
            else if (AccountTypeId == 11)
            {

                account = new Account();
                objAccount.IsApproved = true;
                objAccount.IsDeleted = false;
                objAccount.AccountNumber = AccountNumber;
                account.dtCommon = objAccountBll.SavingswithoutInterestPDF(objAccount);
                return new Rotativa.MVC.ViewAsPdf("SavingswithoutInterestPDF", account);
            }
            return HttpNotFound();
        }


        #endregion

        public JsonResult GetTransactionalCoaData()
        {
            TransactionBLL objTransactionBll = new TransactionBLL();
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}