var emiTotal;
var oAdditionalTable;
(function () {
    var z;
    ShowData();
    function ShowData() {
        oAdditionalTable = $('#AdditionalTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAdditionalLoanInfoById",
                "type": "get",
                "data": {
                    "LoanId": $('#LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": "AdditionalLoanDetailId",
                    "width": "25px",
                    "render": function (data) {
                        return '<input class="form-control" id="AdditionalLoanDetailId" name="AdditionalLoanDetailId" type="text" readonly="readonly"  value = "' + data + '"  >';
                    }
                },
                {
                    "data": "InstituteName",
                    "width": "130px",
                    "render": function (data) {
                        return '<input class="form-control" id="InstituteName" name="InstituteName" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "LoanAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanAmount" name="LoanAmount" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "LoanType",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanType" name="LoanType" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "Installment",
                    "render": function (data) {
                        return '<input class="form-control" id="Installment" name="Installment" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "AccountNo",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountNo" name="AccountNo" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "AccountName",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountName" name="AccountName" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                },
                {
                    "data": "StartDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        // bindDatePickerforstdate();
                        return '<input class="form-control" id="StartDate" name="StartDate" type="text" readonly="readonly" value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "ExpireDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        //  bindDatePickerforexdate();
                        return '<input class="form-control" id="ExpireDate" name="ExpireDate" type="text" readonly="readonly" value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "PresentOutAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="PresentOutAmount" name="PresentOutAmount" type="text" readonly="readonly" value = "' + data + '"  >';
                    }
                }
            ],
            ordering: false,
            searching: false,
            paging: false,
            "bInfo": false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                emiTotal = api
                    .column(4, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                var PresentOut = api
                    .column(9, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                    emiTotal
                );


                $(api.column(9).footer()).html(
                    Math.round(PresentOut)
                );

                z = emiTotal;
            }
        });
    };

    Finance();

})();

/**********************************************************************/

function Finance() {
    var data = {
        LoanId: $('#MonthlyIncome_LoanId').val()
    }
    $.ajax({
        url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/Finance",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (obj) {
            $('#hdfLoanId').val(obj.info.LoanId);
            $('#hdfAccounts').val(obj.info.AccountSetupId);
            $('#hdfCustomerId').val(obj.info.CustomerId);

            if ($('#hdfAccounts').val() == 3) {
                $('#managementInstallmentAmount').attr('disabled', 'disabled');
                $('#managementInstallmentNo').attr('disabled', 'disabled');
                $('#managementInstallmentType').attr('disabled', 'disabled');
            }

            var netEmi = emiTotal;

            var netIncome = obj.netIncome.TotalIncome;
            var netExpense = obj.netExpense.TotalExpense;

            var surplusIncome = parseFloat(netIncome - netExpense);

            document.getElementById('labelSurplusIncome').innerHTML = surplusIncome;

            if (obj.info.AccountSetupId === 1) {
                if (obj.info.InstallmentTypeId == 1) {
                    document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelweeklyNoInstallment').innerHTML = obj.info.NumberOfInstallment;
                    document.getElementById('labelweeklyInstallmentAmount').innerHTML = obj.info.InstallmentAmount;
                    document.getElementById('labelweeklyInstallmentType').innerHTML = obj.info.InstallmentTypeName;
                    var netEmi = emiTotal;

                    var netIncome = obj.netIncome.TotalIncome;
                    var netExpense = obj.netExpense.TotalExpense;

                    var surplusIncome = parseFloat(netIncome - netExpense);
                    //$('#labelSurplusIncome').val(surplusIncome);
                    document.getElementById('labelSurplusIncome').innerHTML = surplusIncome;
                    var db = 0;
                    if (surplusIncome !== 0) {
                        db = parseFloat((obj.info.InstallmentAmount + netEmi) / surplusIncome);
                    }
                    //var db = parseFloat((obj.info.InstallmentAmount + netEmi) / surplusIncome);
                    var dbR = parseFloat(db * 100).toFixed(2);
                    document.getElementById('labelweeklyDbr').innerHTML = Math.round(dbR);
                    document.getElementById('labelweeklyDbr1').innerHTML = Math.round(dbR) + '%';


                    var eq = 0;
                    if (obj.info.TotalAssetCost !== 0) {
                        eq = parseFloat((obj.info.TotalAssetCost - obj.info.LoanAmount) / obj.info.TotalAssetCost);
                    }
                    var eqR = parseFloat(eq * 100).toFixed(2);
                    document.getElementById('labelweeklyEquity').innerHTML = Math.round(eqR);
                    document.getElementById('labelweeklyEquity1').innerHTML = Math.round(eqR) + '%';

                    /***********************************************************************************************************/

                    document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelmonthlyNoInstallment').innerHTML = 12;
                    document.getElementById('labelmonthlyInstallmentType').innerHTML = 'Monthly';

                    var r = parseFloat(obj.info.InterestRate) / 1200;
                    var a = parseFloat(obj.info.LoanAmount * r);
                    var b = parseFloat(Math.pow(1 + r, -12));
                    var c = 1 - b;
                    var installmentAmount = Math.round(a / c);
                    document.getElementById('labelmonthlyInstallmentAmount').innerHTML = installmentAmount.toFixed(2);

                    var mdb = parseFloat((installmentAmount + netEmi) / surplusIncome);
                    var mdbR = parseFloat(mdb * 100).toFixed(2);
                    document.getElementById('labelmonthlyDbr').innerHTML = Math.round(mdbR);
                    document.getElementById('labelmonthlyEquity').innerHTML = Math.round(eqR);
                    document.getElementById('labelmonthlyDbr1').innerHTML = Math.round(mdbR) + '%';
                    document.getElementById('labelmonthlyEquity1').innerHTML = Math.round(eqR) + '%';
                }

                if (obj.info.InstallmentTypeId == 2) {
                    document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate;
                    document.getElementById('labelmonthlyNoInstallment').innerHTML = obj.info.NumberOfInstallment;
                    document.getElementById('labelmonthlyInstallmentAmount').innerHTML = obj.info.InstallmentAmount;
                    document.getElementById('labelmonthlyInstallmentType').innerHTML = obj.info.InstallmentTypeName;
                    document.getElementById('labelSurplusIncome').innerHTML = surplusIncome;
                    var db = parseFloat((obj.info.InstallmentAmount + netEmi) / surplusIncome);
                    var dbR = parseFloat(db * 100).toFixed(2);
                    document.getElementById('labelmonthlyDbr').innerHTML = Math.round(dbR);
                    document.getElementById('labelmonthlyDbr1').innerHTML = Math.round(dbR) + '%';
                    var eq = parseFloat((obj.info.TotalAssetCost - obj.info.LoanAmount) / obj.info.TotalAssetCost);
                    var eqR = parseFloat(eq * 100).toFixed(2);
                    document.getElementById('labelmonthlyEquity').innerHTML = Math.round(eqR);
                    document.getElementById('labelmonthlyEquity1').innerHTML = Math.round(eqR) + '%';

                    /***********************************************************************************************************/
                    document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelweeklyInstallmentType').innerHTML = 'Weekly';
                    document.getElementById('labelweeklyNoInstallment').innerHTML = 46;

                    var r = parseFloat(obj.info.InterestRate) / 5200;
                    var a = parseFloat(obj.info.LoanAmount * r);
                    var b = parseFloat(Math.pow(1 + r, -46));
                    var c = 1 - b;
                    var installmentAmount = Math.round(a / c);
                    document.getElementById('labelweeklyInstallmentAmount').innerHTML = installmentAmount.toFixed(2);

                    var wdb = parseFloat((installmentAmount + netEmi) / surplusIncome);
                    var wdbR = parseFloat(wdb * 100).toFixed(2);
                    document.getElementById('labelweeklyDbr').innerHTML = Math.round(wdbR);
                    document.getElementById('labelweeklyEquity').innerHTML = Math.round(eqR);

                    document.getElementById('labelweeklyDbr1').innerHTML = Math.round(wdbR) + '%';
                    document.getElementById('labelweeklyEquity1').innerHTML = Math.round(eqR) + '%';
                }
            } else if (obj.info.AccountSetupId === 3) {
                document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate + '%';
            }

        },
        error: function () {
        }
    });
}


var oBankperformanceTable;
BankperformanceTable();

function BankperformanceTable() {
    oBankperformanceTable = $('#BankperformanceTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllBankperformanceInfo",
            "type": "get",
            "data": {
                "LoanId": $('#Bankperformance_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            {
                "data": "BankperformanceId",
                "width": "30px",
                //"visible": false,
                "render": function (data) {
                    return '<input class="form-control" id="BankperformanceId" name="BankperformanceId" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "NgoName",
                "width": "130px",
                "render": function (data) {
                    return '<input class="form-control" id="NgoName" type="text" name="NgoName" readonly="readonly" value =" ' + data + '">';
                }
            },
            {
                "data": "AccNo",
                "render": function (data) {
                    return '<input class="form-control" id="AccNo" name="AccNo" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "FromDate",
                //"visible": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    return '<input class="form-control" id="FromDate" name="FromDate" type="text"  readonly="readonly" value = "' + datadate + ' " >';
                }
            },
            {
                "data": "ToDate",
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    return '<input class="form-control" id="ToDate" name="ToDate" type="text" readonly="readonly"  value = "' + datadate + ' " >';
                }
            },
            {
                "data": "DebitAmount",
                "render": function (data) {
                    return '<input class="form-control" id="DebitAmount" name="DebitAmount" type="text" readonly="readonly"  value =" ' + data + '">';
                }
            },
            {
                "data": "CreditAmount",
                "render": function (data) {
                    return '<input class="form-control" id="CreditAmount" name="CreditAmount" type="text" readonly="readonly" value =" ' + data + '">';
                }
            },
            {
                "data": "Avg",
                "render": function (data) {
                    return '<input class="form-control" id="Avg" name="Avg" type="text" readonly="readonly"  value =" ' + Math.round(data) + '">';
                }
            }
        ],
        //"order": [[0, "desc"]],
        ordering: false,
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            var drTotal = api
                .column(5, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var crTotal = api
                .column(6, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var avgTotal = api
                .column(7, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $(api.column(5).footer()).html(
                drTotal
            );
            $(api.column(6).footer()).html(
                crTotal
            );
            $(api.column(7).footer()).html(
                Math.round(avgTotal)
            );
        }
    });
}




var incomeTotal;
var x;

//====== Load Function function=========
var oMonthlyIncomeTable;
MonthlyIncomeTable();

function MonthlyIncomeTable() {
    oMonthlyIncomeTable = $('#MonthlyIncomeTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllMonthlyIncomeInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyIncome_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "IncomeType" },
            { "data": "Amount" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            incomeTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                incomeTotal
            );
            x = incomeTotal;
        }
    });
    MonthlyInformatiom();
}

/// ======== Monthly Informtion========
function MonthlyInformatiom() {
    var loanId = $('#MonthlyIncome_LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllMonthlyIncomeInfo",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            if (data.data[0].Amount === 0) {
                $('#Amount').val('');
            } else {
                $('#Amount').val(data.data[0].Amount);
            }
            if (data.data[0].SpouseAmount === 0) {
                $('#SpouseAmount').val('');
            } else {
                $('#SpouseAmount').val(data.data[0].SpouseAmount);
            }
            if (data.data[0].OtherIncomeAmount === 0) {
                $('#OtherIncomeAmount').val('');
            } else {
                $('#OtherIncomeAmount').val(data.data[0].OtherIncomeAmount);
            }
            if (data.data[0].ForeignRemitAmount === 0) {
                $('#ForeignRemitAmount').val('');
            } else {
                $('#ForeignRemitAmount').val(data.data[0].ForeignRemitAmount);
            }
            $('#IncomeType').val(data.data[0].IncomeType);
            $('#SpouseIncomeType').val(data.data[0].SpouseIncomeType);
            $('#TotalAmount').val(data.data[0].TotalIncome);
            //console.log(data);
            //$('#Amount').val(data.data[0].Amount);
            //$('#IncomeType').val(data.data[0].IncomeType);
            //$('#OtherIncomeAmount').val(data.data[0].OtherIncomeAmount);
            //$('#SpouseAmount').val(data.data[0].SpouseAmount);
            //$('#SpouseIncomeType').val(data.data[0].SpouseIncomeType);
            //$('#ForeignRemitAmount').val(data.data[0].ForeignRemitAmount);
            //$('#TotalAmount').val(data.data[0].TotalIncome);

        }
    });
}


var months = [
           'Jan', 'Feb', 'Mar', 'Apr', 'May',
           'Jun', 'Jul', 'Aug', 'Sep',
           'Oct', 'Nov', 'Dec'
];

function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}


var expenseTotal;
var y;
//=========== Load Function function ==========
var oMonthlyExpenseTable;
MonthlyExpenseTable();

function MonthlyExpenseTable() {
    oMonthlyExpenseTable = $('#MonthlyExpenseTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllMonthlyExpenseInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyExpense_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "FamilyExpense" },
            { "data": "ChildrenEducation" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            expenseTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                expenseTotal
            );
            y = expenseTotal;
        }
    });
    MonthlyExpenseInformatiom();
}

///// ========= MonthlyExpenseInformatiom ============

function MonthlyExpenseInformatiom() {
    var loanId = $('#MonthlyExpense_LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAllMonthlyExpenseInfo",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            if (data.data[0].FamilyExpense === 0) {
                $('#FamilyExpense').val('');
            } else {
                $('#FamilyExpense').val(data.data[0].FamilyExpense);
            }
            if (data.data[0].ChildrenEducation === 0) {
                $('#ChildrenEducation').val('');
            } else {
                $('#ChildrenEducation').val(data.data[0].ChildrenEducation);
            }
            if (data.data[0].ExistingEMI === 0) {
                $('#ExistingEMI').val('');
            } else {
                $('#ExistingEMI').val(data.data[0].ExistingEMI);
            }
            if (data.data[0].OthersExpense === 0) {
                $('#OthersExpense').val('');
            } else {
                $('#OthersExpense').val(data.data[0].OthersExpense);
            }
            $('#TotalExpenseAmount').val(data.data[0].TotalExpense);
            //$('#FamilyExpense').val(data.data[0].FamilyExpense);
            //$('#ChildrenEducation').val(data.data[0].ChildrenEducation);
            //$('#ExistingEMI').val(data.data[0].ExistingEMI);
            //$('#OthersExpense').val(data.data[0].OthersExpense);
            //$('#TotalExpenseAmount').val(data.data[0].TotalExpense);
        }
    });
}


function NetIncomeCalculation() {
    var Amount = parseFloat($('#Amount').val());
    var SpouseAmount = parseFloat($('#SpouseAmount').val());
    var OtherIncomeAmount = parseFloat($('#OtherIncomeAmount').val());
    var ForeignRemitAmount = parseFloat($('#ForeignRemitAmount').val());
    var totalAmount = parseFloat(SpouseAmount + Amount + OtherIncomeAmount + ForeignRemitAmount);
    $('#TotalAmount').val(totalAmount);
}

function NetExpenseCalculation() {
    var FamilyExpense = parseFloat($('#FamilyExpense').val());
    var ChildrenEducation = parseFloat($('#ChildrenEducation').val());
    var ExistingEMI = parseFloat($('#ExistingEMI').val());
    var OthersExpense = parseFloat($('#OthersExpense').val());
    var totalAmount = parseFloat(FamilyExpense + ChildrenEducation + ExistingEMI + OthersExpense);
    $('#TotalExpenseAmount').val(totalAmount);
}


/*******************************************************************************************************************************************/
$('#LoanDisburseDate').datepicker({
    dateFormat: 'dd/M/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+60",
    type: Text
}).click(function () { $(this).focus(); });
$('#LoanExpiryDate').datepicker({
    dateFormat: 'dd/M/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+60",
    type: Text
}).click(function () { $(this).focus(); });

$(function () {
    $("#COA_accountName").empty();
    $.ajax({
        type: "GET",
        url: window.applicationBaseUrl + "Loan/Loan/Get_COAaccountCodeInfo",
        contentType: "application/json; charset=utf-8",
        //data: JSON.stringify(json),
        success: function (data) {
            $("#COA_accountName").append('<option value="0">--Select Code--</option>');
            $.each(data, function (key, value) {
                $("#COA_accountName").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                //alert(value.AccountCode);
            });
        }
    });
});

$(function () {
    $("#managementInstallmentType").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/Loan/GetInstallmentType",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#managementInstallmentType").append('<option value="0">--Select installment Type--</option>');
            $.each(data, function (key, value) {
                $("#managementInstallmentType").append('<option value=' + value.InstallmentTypeId + '>' + value.InstallmentTypeName + '</option>');
            });
        }
    });
});

$('#managementLoanAmount').change(function () {
    if ($('#hdfAccounts').val() == 1) {
        // CalculateManagementEmi();
    }

});
$('#managementInterestRate').change(function () {
    // CalculateManagementEmi();
});
$('#managementInstallmentNo').change(function () {
    CalculateManagementEmi();
});
$('#managementInstallmentType').change(function () {
    // CalculateManagementEmi();
});


var loanAmount;
var interestRate;
var numberOfInstallment;
var interestAmount;
var installmentAmount;
var principleWithInterest;
var totalInterest;

//masum
//masum
function CalculateManagementEmi() {
    alert('ss');
    loanAmount = parseInt($('#managementLoanAmount').val());
    //interestRate = parseFloat($('#managementInterestRate').val()) / 100;

    if (parseInt($('#hdfAccounts').val()) === 12) {
        /// interestRate = parseFloat($('#managementInterestRate').val()) * 2;
        interestRate = parseFloat($('#managementInterestRate').val());
    } else {
        interestRate = parseFloat($('#managementInterestRate').val());
    }

    numberOfInstallment = parseInt($('#managementInstallmentNo').val());
    if ($('#managementInstallmentType option:selected').val() == 2) {
        //alert(
        interestAmount = Math.round(parseFloat((loanAmount * interestRate) / 1200));
        //var r = parseFloat($('#managementInterestRate').val()) / 1200;
        var r = interestRate / 1200;
        var a = parseFloat(loanAmount * r);
        var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
        var c = 1 - b;
        installmentAmount = Math.round(a / c).toFixed(2);
        $('#managementInstallmentAmount').val(installmentAmount);
        principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
        totalInterest = Math.round(parseFloat(principleWithInterest - loanAmount));

    }


    if ($('#managementInstallmentType option:selected').val() == 1 && parseInt($('#hdfAccounts').val()) === 12) {
        alert('ss');
        var numberOfInstallment = parseInt($('#managementInstallmentNo').val());
        interestAmount = Math.round(parseFloat((loanAmount * interestRate) / 100));
        //var r = parseFloat($('#managementInterestRate').val()) / 5200;
        //var r = interestRate / 5200;
        //var a = parseFloat(loanAmount * r);
        //var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
        //var c = 1 - b;
        //installmentAmount = Math.round(a / c).toFixed(2);
        //console.log(interestAmount+loanAmount)/numberOfInstallment)
        $('#managementInstallmentAmount').val((interestAmount + loanAmount) / numberOfInstallment);
        principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
        totalInterest = Math.round(parseFloat(principleWithInterest - loanAmount));
    }
    else {
        // alert('ss');
        interestAmount = Math.round(parseFloat((loanAmount * interestRate) / 5200));
        //var r = parseFloat($('#managementInterestRate').val()) / 5200;
        var r = interestRate / 5200;
        var a = parseFloat(loanAmount * r);
        var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
        var c = 1 - b;
        installmentAmount = Math.round(a / c).toFixed(2);
        $('#managementInstallmentAmount').val(installmentAmount);
        principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
        totalInterest = Math.round(parseFloat(principleWithInterest - loanAmount));
    }


}



    if ($('#managementInstallmentType option:selected').val() == 1 && parseInt($('#hdfAccounts').val()) === 12) {
        alert('ss');
        var numberOfInstallment = parseInt($('#managementInstallmentNo').val());
        interestAmount = Math.round(parseFloat((loanAmount * interestRate) / 100));
        //var r = parseFloat($('#managementInterestRate').val()) / 5200;
        //var r = interestRate / 5200;
        //var a = parseFloat(loanAmount * r);
        //var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
        //var c = 1 - b;
        //installmentAmount = Math.round(a / c).toFixed(2);
        //console.log(interestAmount+loanAmount)/numberOfInstallment)
        $('#managementInstallmentAmount').val((interestAmount + loanAmount) / numberOfInstallment);
        principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
        totalInterest = Math.round(parseFloat(principleWithInterest - loanAmount));
    }
    else {
        // alert('ss');
        interestAmount = Math.round(parseFloat((loanAmount * interestRate) / 5200));
        //var r = parseFloat($('#managementInterestRate').val()) / 5200;
        var r = interestRate / 5200;
        var a = parseFloat(loanAmount * r);
        var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
        var c = 1 - b;
        installmentAmount = Math.round(a / c).toFixed(2);
        $('#managementInstallmentAmount').val(installmentAmount);
        principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
        totalInterest = Math.round(parseFloat(principleWithInterest - loanAmount));
    }


}


$('#submitApprove').click(function () {
    var LoanId = $('#hdfLoanId').val();
    if (LoanId !== 'NotASavedLoan') {
        var isValid = CheckValidation();

        if (isValid) {
            var data = Bindata();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/ApproveLoan",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    $('#hdfLoanId').val('');
                    if (d.oCommonResult.Status == true) {
                        window.location.reload();

                        document.getElementById('showMessage').style.color = "green";
                        $('#hdfLoanId').val(data.LoanId);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanId').val('');
                }
            });

        }
    } else {
        alert('Plz select data from table');
    }




});


function Bindata() {

    //var loanAmount = parseInt($('#managementLoanAmount').val());
    //var interestRate = parseFloat($('#managementInterestRate').val()) / 100;
    //var numberOfInstallment = parseInt($('#managementInstallmentNo').val());

    //if ($('#managementInstallmentType option:selected').val() == 2) {
    //    var interestAmount = parseFloat((loanAmount * interestRate) / 12);
    //    var r = parseFloat($('#managementInterestRate').val()) / 1200;
    //    var a = parseFloat(loanAmount * r);
    //    var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
    //    var c = 1 - b;
    //    var installmentAmount = (a / c).toFixed(2);
    //    //$('#managementInstallmentAmount').val(installmentAmount);
    //    var principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
    //    var totalInterest = parseFloat(principleWithInterest - loanAmount);

    //}
    //if ($('#managementInstallmentType option:selected').val() == 1) {
    //    var interestAmount = parseFloat((loanAmount * interestRate) / 52);
    //    var r = parseFloat($('#managementInterestRate').val()) / 5200;
    //    var a = parseFloat(loanAmount * r);
    //    var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
    //    var c = 1 - b;
    //    var installmentAmount = (a / c).toFixed(2);
    //    //$('#managementInstallmentAmount').val(installmentAmount);
    //    var principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
    //    var totalInterest = parseFloat(principleWithInterest - loanAmount);
    //}

    CalculateManagementEmi();

    var data = {
        LoanNo: $('#LoanNo').val(),
        AccountSetupId: $('#hdfAccounts').val(),
        LoanId: $('#hdfLoanId').val(),
        LoanExpiryDate: $('#LoanExpiryDate').val(),
        InterestAmount: interestAmount,
        InstallmentAmount: installmentAmount,
        LoanDate: $('#LoanDisburseDate').val(),
        LoanAmount: loanAmount,
        InterestRate: $('#managementInterestRate').val(),
        InstallmentTypeId: $('#managementInstallmentType option:selected').val(),
        NumberOfInstallment: numberOfInstallment,
        COAId: $('#COA_accountName option:selected').val(),
        TotalInterest: totalInterest,
        CustomerId: $('#hdfCustomerId').val(),

        MonthlyInstallmentNumber: document.getElementById('labelmonthlyNoInstallment').innerHTML,
        WeeklyInstallmentNumber: document.getElementById('labelweeklyNoInstallment').innerHTML,
        MonthlyInstallmentAmount: document.getElementById('labelmonthlyInstallmentAmount').innerHTML,
        WeeklyInstallmentAmount: document.getElementById('labelweeklyInstallmentAmount').innerHTML,
        MonthlyDbr: document.getElementById('labelmonthlyDbr').innerHTML.replace('%', ''),
        WeeklyDbr: document.getElementById('labelweeklyDbr').innerHTML.replace('%', ''),
        Equity: document.getElementById('labelweeklyEquity').innerHTML.replace('%', '')

    }

    return data;

}




$('#submitReject').click(function () {
    var LoanId = $('#hdfLoanId').val();
    if (LoanId !== 'NotASavedLoan') {
        var isValid = true;
        if (isValid) {

            var LoanId = $('#hdfLoanId').val();
            var data = { loanId: LoanId };

            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/RejectLoan",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    $('#hdfLoanId').val('');
                    if (d.oCommonResult.Status == true) {
                        document.getElementById('showMessage').style.color = "green";
                        //  $('#hdfLoanId').val(data.LoanId);
                        window.location.reload();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    // $('#hdfLoanId').val('');
                }
            });

        }
    } else {

        alert('Plz select data from table');

    }

});

$('#submitGenerateLoanId').click(function () {
    var LoanId = $('#hdfLoanId').val();
    if (LoanId !== 'NotASavedLoan') {
        var loanType = $("#hdfAccounts").val();

        var json = {
            loanType: loanType
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetLoanNo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#LoanNo').val(data);
                if (loanType === '3') {
                    var mNames = new Array("Jan", "Feb", "Mar",
                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                    "Oct", "Nov", "Dec");

                    var d = new Date();
                    var year = d.getFullYear() + 1;
                    var month = d.getMonth();
                    var day = d.getDate();
                    var c = day + "/" + mNames[month] + "/" + year;
                    $("#LoanExpiryDate").val(c);
                }
            }
        });
    } else {
        alert('Plz select data from table');
    }


});


function CheckValidation() {
    var isValid = true;

    if ($('#LoanDisburseDate').val().trim() === '') {
        isValid = false;
        $('#LoanDisburseDate').parent().prev().find('span').css('visibility', 'visible');
    } else {
        $('#LoanDisburseDate').parent().prev().find('span').css('visibility', 'hidden');
    }
    if ($('#LoanNo').val().trim() === '') {
        isValid = false;
        $('#LoanNo').parent().prev().find('span').css('visibility', 'visible');
    } else {
        $('#LoanNo').parent().prev().find('span').css('visibility', 'hidden');
    }
    if ($('#managementLoanAmount').val().trim() === '') {
        isValid = false;
        $('#managementLoanAmount').parent().prev().find('span').css('visibility', 'visible');
    } else {
        $('#managementLoanAmount').parent().prev().find('span').css('visibility', 'hidden');
    }
    if ($('#managementInterestRate').val().trim() === '') {
        isValid = false;
        $('#managementInterestRate').parent().prev().find('span').css('visibility', 'visible');
    } else {
        $('#managementInterestRate').parent().prev().find('span').css('visibility', 'hidden');
    }
    //if ($('#LoanExpiryDate').val().trim() === '') {
    //    isValid = false;
    //    $('#LoanExpiryDate').parent().prev().find('span').css('visibility', 'visible');
    //} else {
    //    $('#LoanExpiryDate').parent().prev().find('span').css('visibility', 'hidden');
    //}
    if ($('#COA_accountName').val().trim() === '0') {
        isValid = false;
        $('#COA_accountName').parent().prev().find('span').css('visibility', 'visible');
    } else {
        $('#COA_accountName').parent().prev().find('span').css('visibility', 'hidden');
    }

    var availableBalance = parseFloat($("#AccountBalance").text());
    var loanAmount = parseFloat($('#managementLoanAmount').val());
    if (loanAmount > availableBalance) {
        alert('InSufficient Balance!!!');
        isValid = false;
    }
    return isValid;
}



$("#managementInstallmentNo").change(function () {


    var installmentNo = $("#managementInstallmentNo").val();
    var startDate = $("#LoanDisburseDate").val();
    var type = $('#managementInstallmentType option:selected').val();

    var json = {
        NumberOfInstallment: installmentNo,
        LoanDate: startDate,
        InstallmentTypeId: type
    };

    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetExpiryDate",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            $("#LoanExpiryDate").val(data.info.LoanExpiryDate);
        }
    });
});


function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode == 13) {
        return false;
    }
    status = "";
    return true;
}


$('#COA_accountName').change(function () {
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAccountCodeInfo",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ coaId: $('#COA_accountName').val() }),
        success: function (data) {
            $('#AccountBalance').text(data.Balance);
        }
    });
});