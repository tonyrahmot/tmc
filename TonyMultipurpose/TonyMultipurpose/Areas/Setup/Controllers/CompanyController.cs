﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    [AuthenticationFilter]
    public class CompanyController : Controller
    {
        CompanyBLL objCompanyBLL = new CompanyBLL();
        // GET: Setup/Company
        public ActionResult Index()
        {
            List<Company> objCompanyList = new List<Company>();
            objCompanyList = objCompanyBLL.GetAllCompanyInfo();
            return View(objCompanyList);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Company objCompany, HttpPostedFileBase File)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objCompany.IsDeleted = false;
                objCompany.CreatedBy = 1;
                objCompany.CreatedDate = DateTime.Now;
                objCompanyBLL.SaveCompanyInfo(objCompany);
                status = true;
            }
            else
            {
                return View(objCompany);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "Company");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var company = objCompanyBLL.GetCompanyInfo(id);
            return View(company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Company objCompany)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objCompany.UpdatedBy = 1;
                objCompany.UpdatedDate = DateTime.Now;
                objCompanyBLL.UpdateCompanyInfo(objCompany);
                status = true;
            }
            else
            {
                return View(objCompany);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "Company");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var company = objCompanyBLL.GetCompanyInfo(id);
            return View(company);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteCompany(int id)
        {
            objCompanyBLL.DeleteCompany(id);
            return RedirectToAction("Index", "Company");
        }
    }
}