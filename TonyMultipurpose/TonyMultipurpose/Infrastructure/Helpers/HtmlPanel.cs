﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlPanel : IDisposable
    {
        private readonly ViewContext _viewContext;
        public HtmlPanel(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }
        public void Dispose()
        {
            _viewContext.Writer.Write(
            "</div>" +
            "</div>"
            );
        }
    }
}