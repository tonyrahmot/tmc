﻿
$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});


$(document).ready(function () {
    function FormateDate(jsonDate) {
        var monthNames = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + M + '/' + yy;
        return formmatedDate;
    }

    $('#ChartOfAccount').change(function () {
        var coaId = $("#ChartOfAccount").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetAccountCodeInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                if (data.IsBankAccount === true) {
                    $('#IsCashEntry').attr('checked', false);
                    $('#IsChequeEntry').attr('checked', true);
                    $('#ConIsChequeEntry').attr('checked', true);
                    $('#divBankName').show();
                    $('#divBankAccountNo').show();
                    $('#divChequeNo').show();
                    $('#loan_Bank').show();
                    $('#loan_Account').show();
                    $('#loan_Cheque').show();
                    $('#Conloan_Bank').show();
                    $('#Conloan_Account').show();
                    $('#Conloan_Cheque').show();
                    $('#BankName').val(data.BankName);
                    $('#BankAccountNo').val(data.BankAccountNo);
                    $('#loanBankName').val(data.BankName);
                    $('#loanBankAccount').val(data.BankAccountNo);
                    $('#ConloanBankName').val(data.BankName);
                    $('#ConloanBankAccount').val(data.BankAccountNo);
                } else {
                    $('#IsCashEntry').attr('checked', true);
                    $('#IsChequeEntry').attr('checked', false);
                    $('#ConIsChequeEntry').attr('checked', false);
                    $('#divBankName').hide();
                    $('#divBankAccountNo').hide();
                    $('#divChequeNo').hide();
                    $('#loan_Bank').hide();
                    $('#loan_Account').hide();
                    $('#loan_Cheque').hide();
                    $('#Conloan_Bank').hide();
                    $('#Conloan_Account').hide();
                    $('#Conloan_Cheque').hide();
                    $('#BankName').val('');
                    $('#BankAccountNo').val('');
                    $('#loanBankName').val('');
                    $('#loanBankAccount').val('');
                    $('#ConloanBankName').val('');
                    $('#ConloanBankAccount').val('');
                }
            }
        });

    });
    $('.deposit-form').removeClass("hidden");
    $('.continuous-loan').addClass("hidden");
    $('.term-loan').addClass("hidden");

    $('#TT').change(function () {
        $('#TransactionDate').datepicker('disable');
        var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        $('#TransactionDate').val(curr_date + "/" + m_names[curr_month]
            + "/" + curr_year);
        var v = $('#TT').val();
        if (v === 'Deposit') {
            $('.deposit-form').removeClass("hidden");
            $('.continuous-loan').addClass("hidden");
            $('.term-loan').addClass("hidden");
        } else if (v === 'Loan') {

            $('.deposit-form').addClass("hidden");
            $('.continuous-loan').addClass("hidden");
            $('.term-loan').removeClass("hidden");

            $('#TransactionDate').datepicker({ dateFormat: 'dd/M/yy', type: Text }).click(function () { $(this).focus(); });
        } else if (v === 'ContinuousLoanDeposit') {
            $('.deposit-form').addClass("hidden");
            $('.continuous-loan').removeClass("hidden");
            $('.term-loan').addClass("hidden");
        }
    });

    /******************************************************************************/
    $("#LoanNo").autocomplete({
        //alert('autocomplete');
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanInstallment/AutoCompleteTermLoanId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    if (data.length == 0) {
                        alert("No Account Available for Transaction");
                        clearTermLoanInfo();
                        $('#submitInstallment').attr('disabled', 'disabled');
                        $('#TotalPayment').attr('disabled', 'disabled');

                    } else {
                        response($.map(data, function (item) {
                            return { label: item, value: item };
                        }));
                    }
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    var labelBeginningBalance;
    var labelInterest;
    var labelTotalUnpaidPrinciple;
    var labelInstallmentAmount;
    var labelTotalFineInterest;

    $("#LoanNo").change(function () {
        //alert('111');
        clearTermLoanInfo();
        removeErrorMessage(this);
        var LoanNo = $("#LoanNo").val();
        var json = {
            LoanNo: LoanNo
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/LoanInstallment/GetTermLoanInfoByLoanId",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (obj) {
                if (obj.info.IsCleared == true)
                {
                    alert("All Installment Has Been Cleared!!!!");
                    clearTermLoanInfo();
                    $('#submitInstallment').attr('disabled', 'disabled');
                    $('#TotalPayment').attr('disabled', 'disabled');

                } else
                {
                    if (obj.p.InstallmentNo > 1)
                    {
                       // alert('1');
                        $('#submitInstallment').removeAttr('disabled');
                        $('#TotalPayment').removeAttr('disabled');

                        $('#hdfInstallmentTypeId').val(obj.info.InstallmentTypeId);
                        $('#hdfNumberOfInstallment').val(obj.info.NumberOfInstallment);
                        $('#CustomerId').html(obj.info.CustomerId);
                        $('#LabelCustomerName').html(obj.info.CustomerName);
                        $('#LabelFatherName').html(obj.info.FatherName);
                        $('#LabelExtraFee').html(obj.info.ExtraFee);
                        $('#LabelInstallmentNo').html(obj.p.InstallmentNo);
                        //$('#InstallmentDate').val(obj.p.InstallmentDate);
                        $('#TransactionDate').val(obj.p.InstallmentDate);
                        $('#InterestRate').val(obj.info.InterestRate);
                        $('#customerImage').attr('src', obj.info.CustomerImage);
                        $('#LabelInstallmentAmount').html(obj.info.InstallmentAmount);
                        labelInstallmentAmount = obj.info.InstallmentAmount;
                        //$('#LabelFineInterest').html(obj.info.FineInterest);
                        $('#LabelTotalFineInterest').html(obj.info.TotalFineInterest);
                        labelTotalFineInterest = obj.info.TotalFineInterest;
                        //$('#LabelUnpaidPrinciple').html(obj.info.UnpaidPrinciple);
                        $('#LabelTotalUnpaidPrinciple').html(obj.info.TotalUnpaidPrinciple);
                        labelTotalUnpaidPrinciple = obj.info.TotalUnpaidPrinciple;
                        if (obj.info.BeginningBalance == 0) {
                            $('#LabelBeginningBalance').html(obj.info.PrincipleAmount);
                            labelBeginningBalance = obj.info.PrincipleAmount;
                        } else {
                            $('#LabelBeginningBalance').html(obj.info.EndingBalance);
                            labelBeginningBalance = obj.info.EndingBalance;
                        }


                        var interestRate = parseFloat($('#InterestRate').val());
                        var bBalance = parseFloat($('#LabelBeginningBalance').html());
                        if (obj.info.InstallmentTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                            labelInterest = interest.toFixed(2);
                        }
                        if (obj.info.InstallmentTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                            labelInterest = interest.toFixed(2);
                        }

                        if (parseFloat(obj.info.InstallmentAmount) > bBalance) {
                            var totalPayment = parseFloat(bBalance);
                            var principle = parseFloat(bBalance - interest);
                            var eBalance = 00;
                        } else {
                            var totalPayment = parseFloat($('#LabelInstallmentAmount').html());
                            var principle = Math.round(parseFloat(totalPayment - interest));
                            var eBalance = Math.round(parseFloat(bBalance - principle));
                        }
                        //alert($('#LabelInstallmentNo').html());
                        var totalOutstanding = parseFloat(interest.toFixed(2)) + parseFloat($('#LabelBeginningBalance').html()) + parseFloat($('#LabelTotalFineInterest').html()) - parseFloat(principle.toFixed(2) * parseFloat($('#LabelInstallmentNo').html()));


                        if ($('#LabelInstallmentNo').html() == $('#hdfNumberOfInstallment').val()) {
                            $('#TotalPayment').val(totalOutstanding);
                        } else {
                            $('#TotalPayment').val(totalPayment);
                        }

                        $('#LabelOutstanding').html(totalOutstanding.toFixed(2));

                        //$('#LabelEndingBalance').html(eBalance.toFixed(2));
                        //Added by Masum
                        $('#LabelEndingBalance').html(eBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));
                        $("#TotalPayment").keyup();
                    }
                    else
                    {
                       // alert('2');
                        $('#submitInstallment').removeAttr('disabled');
                        $('#TotalPayment').removeAttr('disabled');

                        $('#hdfInstallmentTypeId').val(obj.info.InstallmentTypeId);
                        $('#hdfNumberOfInstallment').val(obj.info.NumberOfInstallment);
                        $('#CustomerId').html(obj.info.CustomerId);
                        $('#LabelCustomerName').html(obj.info.CustomerName);
                        $('#LabelFatherName').html(obj.info.FatherName);
                        $('#LabelExtraFee').html(obj.info.ExtraFee);
                        $('#LabelInstallmentNo').html(obj.p.InstallmentNo);
                        //$('#InstallmentDate').val(obj.p.InstallmentDate);
                        $('#TransactionDate').val(obj.p.InstallmentDate);
                        $('#InterestRate').val(obj.info.InterestRate);
                        $('#customerImage').attr('src', obj.info.CustomerImage);
                        $('#LabelInstallmentAmount').html(obj.info.InstallmentAmount);
                        labelInstallmentAmount = obj.info.InstallmentAmount;
                        //$('#LabelFineInterest').html(obj.info.FineInterest);
                        $('#LabelTotalFineInterest').html(obj.info.TotalFineInterest);
                        labelTotalFineInterest = obj.info.TotalFineInterest;
                        //$('#LabelUnpaidPrinciple').html(obj.info.UnpaidPrinciple);
                        $('#LabelTotalUnpaidPrinciple').html(obj.info.TotalUnpaidPrinciple);
                        labelTotalUnpaidPrinciple = obj.info.TotalUnpaidPrinciple;
                        if (obj.info.BeginningBalance == 0) {
                            $('#LabelBeginningBalance').html(obj.info.PrincipleAmount);
                            labelBeginningBalance = obj.info.PrincipleAmount;
                        } else {
                            $('#LabelBeginningBalance').html(obj.info.EndingBalance);
                            labelBeginningBalance = obj.info.EndingBalance;
                        }


                        var interestRate = parseFloat($('#InterestRate').val());
                        var bBalance = parseFloat($('#LabelBeginningBalance').html());
                        if (obj.info.InstallmentTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                            labelInterest = interest.toFixed(2);
                        }
                        if (obj.info.InstallmentTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                            labelInterest = interest.toFixed(2);
                        }

                        if (parseFloat(obj.info.InstallmentAmount) > bBalance) {
                            var totalPayment = parseFloat(bBalance);
                            var principle = parseFloat(bBalance - interest);
                            var eBalance = 00;
                        } else {
                            var totalPayment = parseFloat($('#LabelInstallmentAmount').html());
                            var principle = Math.round(parseFloat(totalPayment - interest));
                            var eBalance = Math.round(parseFloat(bBalance - principle));
                        }
                        var totalOutstanding = parseFloat(interest.toFixed(2)) + parseFloat($('#LabelBeginningBalance').html()) + parseFloat($('#LabelTotalFineInterest').html());


                        if ($('#LabelInstallmentNo').html() == $('#hdfNumberOfInstallment').val()) {
                            $('#TotalPayment').val(totalOutstanding);
                        } else {
                            $('#TotalPayment').val(totalPayment);
                        }

                        $('#LabelOutstanding').html(totalOutstanding.toFixed(2));

                        $('#LabelEndingBalance').html(eBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));
                        $("#TotalPayment").keyup();
                    }
                    
                }


            }
        });
    });

    $('#Particular').change(function () {
        removeErrorMessage(this);
    });


    $('#TotalPayment').change(function () {
        //console.log(labelBeginningBalance,labelInterest,labelInstallmentAmount,labelTotalUnpaidPrinciple,labelTotalFineInterest);
        removeErrorMessage(this);
        var isMaxInstallment = parseInt($('#LabelInstallmentNo').html()) >= parseInt($('#hdfNumberOfInstallment').val());

        var iTypeId = $('#hdfInstallmentTypeId').val();
        //var bBalance = parseFloat($('#LabelBeginningBalance').html());
        var bBalance = parseFloat(labelBeginningBalance);
        //var interest1 = parseFloat($('#LabelInterest').html());
        var interest1 = parseFloat(labelInterest);

        var interestRate = parseFloat($('#InterestRate').val());
        //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
        var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
        //var regularPayment = parseFloat($('#LabelInstallmentAmount').html());
        var regularPayment = parseFloat(labelInstallmentAmount);
        var ttlPayment = parseFloat($('#TotalPayment').val());
        //var totalFineInterest1 = parseFloat($('#LabelTotalFineInterest').html());
        var totalFineInterest1 = parseFloat(labelTotalFineInterest);
        if (totalUnpaidPrinciple === 0) {
            if (isMaxInstallment) {
                if (ttlPayment <= 0) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount must be greater than zero Tk.');
                } else if (ttlPayment < (bBalance + interest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be smaller than  ' + (bBalance + interest1));
                } else if (ttlPayment > (bBalance + interest1 + totalFineInterest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be greater than ' + (bBalance + interest1));
                } else {
                    $('#submitInstallment').removeAttr('disabled');
                    var extraAmount = Math.round(parseFloat(ttlPayment - regularPayment));
                    $('#LabelExtraPayment').html(extraAmount.toFixed(2));

                    if (iTypeId == 1) {
                        var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                    }
                    if (iTypeId == 2) {
                        var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                    }

                    if ((ttlPayment == (bBalance + interest1 + totalFineInterest1)) || (ttlPayment >= (bBalance + interest1))) {
                        var currentTotalFineInterest = Math.round(parseFloat(ttlPayment - (bBalance + interest1)));
                        var principle = Math.round(parseFloat(ttlPayment - interest - currentTotalFineInterest));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));
                        $('#LabelEndingBalance').html(0);
                        $('#LabelFineInterest').html(parseFloat(totalFineInterest1 - currentTotalFineInterest));
                        $('#LabelTotalFineInterest').html(0);
                        $('#LabelUnpaidPrinciple').html(0);
                        $('#LabelTotalUnpaidPrinciple').html(0);
                    } else {
                        var principle = Math.round(parseFloat(ttlPayment - interest));
                        var eBalance = Math.round(parseFloat(bBalance - principle));
                        $('#LabelEndingBalance').html(eBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));


                        //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                        var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
                        var currentTotalUnpaidPrinciple = totalUnpaidPrinciple;
                        if (iTypeId == 1) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                        }
                        if (iTypeId == 2) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                        }
                        //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                        var totalFineInterest = parseFloat(labelTotalFineInterest);
                        var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

                        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                        $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                        //$('#LabelUnpaidPrinciple').html(currentUnpaidPrinciple.toFixed(2));
                        $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
                    }
                }
            } else {
                if (ttlPayment <= 0) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount must be greater than zero Tk.');
                } else if (ttlPayment > (bBalance + interest1 + totalFineInterest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be greater than  ' + (bBalance + interest1 + totalFineInterest1));
                } else {
                    $('#submitInstallment').removeAttr('disabled');
                    if (ttlPayment < regularPayment) {
                        if (iTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                        }
                        if (iTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                        }
                        var principle = Math.round(parseFloat(ttlPayment - interest));
                        var endingBalance = Math.round(parseFloat(bBalance - principle));
                        $('#LabelEndingBalance').html(endingBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));
                        var unPaidPrinciple = Math.round(parseFloat(regularPayment - ttlPayment));
                        $('#LabelUnpaidPrinciple').html(unPaidPrinciple.toFixed(2));
                        $('#LabelTotalUnpaidPrinciple').html(unPaidPrinciple.toFixed(2));
                        if (iTypeId == 1) {
                            var currentFineInterest = Math.round(parseFloat((unPaidPrinciple * (interestRate + 2)) / 5200));
                        }
                        if (iTypeId == 2) {
                            var currentFineInterest = Math.round(parseFloat((unPaidPrinciple * (interestRate + 2)) / 1200));
                        }
                        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                        $('#LabelTotalFineInterest').html(currentFineInterest.toFixed(2));

                    } else {
                        var extraAmount = Math.round(parseFloat(ttlPayment - regularPayment));
                        $('#LabelExtraPayment').html(extraAmount.toFixed(2));

                        if (iTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                        }
                        if (iTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                        }

                        if ((ttlPayment == (bBalance + interest1 + totalFineInterest1)) || (ttlPayment >= (bBalance + interest1))) {
                            var currentTotalFineInterest = Math.round(parseFloat(ttlPayment - (bBalance + interest1)));
                            var principle = Math.round(parseFloat(ttlPayment - interest - currentTotalFineInterest));
                            $('#LabelPrinciple').html(principle.toFixed(2));
                            $('#LabelInterest').html(interest.toFixed(2));
                            $('#LabelEndingBalance').html(0);
                            $('#LabelFineInterest').html(parseFloat(totalFineInterest1 - currentTotalFineInterest));
                            $('#LabelTotalFineInterest').html(0);
                            $('#LabelUnpaidPrinciple').html(0);
                            $('#LabelTotalUnpaidPrinciple').html(0);
                        } else {
                            var principle = Math.round(parseFloat(ttlPayment - interest));
                            var eBalance = Math.round(parseFloat(bBalance - principle));
                            $('#LabelEndingBalance').html(eBalance.toFixed(2));
                            $('#LabelPrinciple').html(principle.toFixed(2));
                            $('#LabelInterest').html(interest.toFixed(2));


                            //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                            var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
                            var currentTotalUnpaidPrinciple = totalUnpaidPrinciple;
                            if (iTypeId == 1) {
                                var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                            }
                            if (iTypeId == 2) {
                                var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                            }
                            //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                            var totalFineInterest = parseFloat(labelTotalFineInterest);
                            var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

                            $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                            $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                            //$('#LabelUnpaidPrinciple').html(currentUnpaidPrinciple.toFixed(2));
                            $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
                        }

                        //var totalPayment = parseFloat($('#TotalPayment').val());
                        //var interestRate = parseFloat($('#InterestRate').val());
                        //var bBalance = parseFloat($('#LabelBeginningBalance').html());
                        //var iTypeId = $('#hdfInstallmentTypeId').val();
                        //if (iTypeId == 1) {
                        //    var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                        //}
                        //if (iTypeId == 2) {
                        //    var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                        //}
                        //var schedulePayment = parseFloat($('#LabelInstallmentAmount').html());
                        //var principle = Math.round(parseFloat(totalPayment - interest));
                        //var eBalance = Math.round(parseFloat(bBalance - principle));
                        //var extraPayment = Math.round(parseFloat(totalPayment - schedulePayment));
                        //$('#LabelExtraPayment').html(extraPayment.toFixed(2));
                        //$('#LabelEndingBalance').html(eBalance.toFixed(2));
                        //$('#LabelPrinciple').html(principle.toFixed(2));
                        //$('#LabelInterest').html(interest.toFixed(2));
                    }
                }
            }
        }

        if (totalUnpaidPrinciple !== 0) {
            if (isMaxInstallment) {
                if (ttlPayment <= 0) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount must be greater than zero Tk.');
                } else if (ttlPayment < (bBalance + interest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be smaller than  ' + (bBalance + interest1));
                } else if (ttlPayment > (bBalance + interest1 + totalFineInterest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be greater than  ' + (bBalance + interest1));
                } else {
                    $('#submitInstallment').removeAttr('disabled');
                    var extraAmount = Math.round(parseFloat(ttlPayment - regularPayment));
                    $('#LabelExtraPayment').html(extraAmount.toFixed(2));

                    if (iTypeId == 1) {
                        var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                    }
                    if (iTypeId == 2) {
                        var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                    }

                    if ((ttlPayment == (bBalance + interest1 + totalFineInterest1)) || (ttlPayment >= (bBalance + interest1))) {
                        var currentTotalFineInterest = Math.round(parseFloat(ttlPayment - (bBalance + interest1)));
                        var principle = Math.round(parseFloat(ttlPayment - interest - currentTotalFineInterest));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));
                        $('#LabelEndingBalance').html(0);
                        $('#LabelFineInterest').html(parseFloat(totalFineInterest1 - currentTotalFineInterest));
                        $('#LabelTotalFineInterest').html(0);
                        $('#LabelUnpaidPrinciple').html(0);
                        $('#LabelTotalUnpaidPrinciple').html(0);
                    } else {
                        var principle = Math.round(parseFloat(ttlPayment - interest));
                        var eBalance = Math.round(parseFloat(bBalance - principle));
                        $('#LabelEndingBalance').html(eBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));


                        //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                        var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
                        var currentTotalUnpaidPrinciple = totalUnpaidPrinciple;
                        if (iTypeId == 1) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                        }
                        if (iTypeId == 2) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                        }
                        //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                        var totalFineInterest = parseFloat(labelTotalFineInterest);
                        var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

                        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                        $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                        //$('#LabelUnpaidPrinciple').html(currentUnpaidPrinciple.toFixed(2));
                        $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
                    }
                }
            } else {
                if (ttlPayment <= 0) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount must be greater than zero Tk.');
                } else if (ttlPayment > (bBalance + interest1 + totalFineInterest1)) {
                    $('#submitInstallment').attr('disabled', 'disabled');
                    alert('Amount can not be greater than  ' + (bBalance + interest1 + totalFineInterest1));
                } else {
                    $('#submitInstallment').removeAttr('disabled');

                    if (ttlPayment <= regularPayment) {

                        //var totalPayment = parseFloat($('#TotalPayment').val());
                        //var interestRate = parseFloat($('#InterestRate').val());
                        //var bBalance = parseFloat($('#LabelBeginningBalance').html());
                        //var iTypeId = $('#hdfInstallmentTypeId').val();
                        if (iTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                        }
                        if (iTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                        }
                        //var schedulePayment = parseFloat($('#LabelInstallmentAmount').html());
                        var principle = Math.round(parseFloat(ttlPayment - interest));
                        var eBalance = Math.round(parseFloat(bBalance - principle));
                        //var extraPayment = parseFloat(totalPayment - regularPayment);
                        //$('#LabelExtraPayment').html(extraPayment.toFixed(2));
                        $('#LabelEndingBalance').html(eBalance.toFixed(2));
                        $('#LabelPrinciple').html(principle.toFixed(2));
                        $('#LabelInterest').html(interest.toFixed(2));


                        //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                        var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
                        var currentUnpaidPrinciple = Math.round(parseFloat(regularPayment - ttlPayment));
                        var currentTotalUnpaidPrinciple = Math.round(parseFloat(totalUnpaidPrinciple + currentUnpaidPrinciple));
                        if (iTypeId == 1) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                        }
                        if (iTypeId == 2) {
                            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                        }
                        //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                        var totalFineInterest = parseFloat(labelTotalFineInterest);
                        var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

                        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                        $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                        $('#LabelUnpaidPrinciple').html(currentUnpaidPrinciple.toFixed(2));
                        $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));


                    }

                    if (ttlPayment > regularPayment) {
                        var extraAmount = Math.round(parseFloat(ttlPayment - regularPayment));
                        $('#LabelExtraPayment').html(extraAmount.toFixed(2));
                        //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                        //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                        //if (extraAmount >= totalUnpaidPrinciple) {
                        //    $('#LabelUnpaidPrinciple').html(-extraAmount.toFixed(2));
                        //    $('#LabelTotalUnpaidPrinciple').html(0);
                        //    $('#LabelTotalFineInterest').html(0);
                        //    $('#LabelFineInterest').html(0);

                        //}
                        //if (extraAmount < totalUnpaidPrinciple) {
                        //    $('#LabelUnpaidPrinciple').html(-extraAmount.toFixed(2));
                        //    var currentTotalUnpaidPrinciple = Math.round(parseFloat(totalUnpaidPrinciple - extraAmount));

                        //    if (currentTotalUnpaidPrinciple == 0) {
                        //        $('#LabelTotalUnpaidPrinciple').html(0);
                        //        $('#LabelTotalFineInterest').html(totalFineInterest.toFixed(2));
                        //        $('#LabelFineInterest').html(0);
                        //    } else {
                        //        $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
                        //        if (iTypeId == 1) {
                        //            var currentFineInterest =
                        //                Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                        //        }
                        //        if (iTypeId == 2) {
                        //            var currentFineInterest =
                        //                Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                        //        }
                        //        var currentTotalFineInterest = Math.round(parseFloat(currentFineInterest + totalFineInterest));
                        //        $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                        //        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                        //    }
                        //}

                        if (iTypeId == 1) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                        }
                        if (iTypeId == 2) {
                            var interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                        }

                        if ((ttlPayment == (bBalance + interest1 + totalFineInterest1)) || (ttlPayment >= (bBalance + interest1))) {
                            var currentTotalFineInterest = Math.round(parseFloat(ttlPayment - (bBalance + interest1)));
                            var principle = Math.round(parseFloat(ttlPayment - interest - currentTotalFineInterest));
                            $('#LabelPrinciple').html(principle.toFixed(2));
                            $('#LabelInterest').html(interest.toFixed(2));
                            $('#LabelEndingBalance').html(0);
                            $('#LabelFineInterest').html(parseFloat(totalFineInterest1 - currentTotalFineInterest));
                            $('#LabelTotalFineInterest').html(0);
                            $('#LabelUnpaidPrinciple').html(0);
                            $('#LabelTotalUnpaidPrinciple').html(0);
                        } else {
                            var principle = Math.round(parseFloat(ttlPayment - interest));
                            var eBalance = Math.round(parseFloat(bBalance - principle));
                            $('#LabelEndingBalance').html(eBalance.toFixed(2));
                            $('#LabelPrinciple').html(principle.toFixed(2));
                            $('#LabelInterest').html(interest.toFixed(2));


                            //var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
                            var totalUnpaidPrinciple = parseFloat(labelTotalUnpaidPrinciple);
                            var currentTotalUnpaidPrinciple = totalUnpaidPrinciple;
                            if (iTypeId == 1) {
                                var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
                            }
                            if (iTypeId == 2) {
                                var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
                            }
                            //var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
                            var totalFineInterest = parseFloat(labelTotalFineInterest);
                            var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

                            $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
                            $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
                            //$('#LabelUnpaidPrinciple').html(currentUnpaidPrinciple.toFixed(2));
                            $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
                        }
                    }
                }
            }
        }
    });

    function calculateFine() {
        var iTypeId = $('#hdfInstallmentTypeId').val();
        var bBalance = parseFloat($('#LabelBeginningBalance').html());
        var interestRate = parseFloat($('#InterestRate').val());
        var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
        var regularPayment = parseFloat($('#LabelInstallmentAmount').html());
        var ttlPayment = parseFloat($('#TotalPayment').val());


        $('#LabelUnpaidPrinciple').html(0);
        var currentTotalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
        if (iTypeId == 1) {
            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 5200));
        }
        if (iTypeId == 2) {
            var currentFineInterest = Math.round(parseFloat((currentTotalUnpaidPrinciple * (interestRate + 2)) / 1200));
        }
        var totalFineInterest = parseFloat($('#LabelTotalFineInterest').html());
        var currentTotalFineInterest = Math.round(parseFloat(totalFineInterest + currentFineInterest));

        $('#LabelFineInterest').html(currentFineInterest.toFixed(2));
        $('#LabelTotalFineInterest').html(currentTotalFineInterest.toFixed(2));
        $('#LabelTotalUnpaidPrinciple').html(currentTotalUnpaidPrinciple.toFixed(2));
    }

    $('#submitInstallment').click(function () {
        var isAllValid = CheckTermLoanValidation();
        var totalUnpaidPrinciple = parseFloat($('#LabelTotalUnpaidPrinciple').html());
        var regularPayment = parseFloat($('#LabelInstallmentAmount').html());
        var ttlPayment = parseFloat($('#TotalPayment').val());
        //if (totalUnpaidPrinciple !== 0 & regularPayment === ttlPayment) {
        //    calculateFine();
        //}


        if (isAllValid) {
            var data = {
                LoanNo: $('#LoanNo').val(),
                BeginningBalance: $('#LabelBeginningBalance').html(),
                InstallmentAmount: $('#LabelInstallmentAmount').html(),
                TotalAmount: $('#TotalPayment').val(),
                Particular: $('#Particular').val(),
                DueInstallmentAmount: $('#DueInstallmentAmount').val(),
                IsChequeEntry: $("#IsChequeEntry").prop('checked'),
                //InstallmentDate: $('#InstallmentDate').val(),
                InstallmentDate: $('#TransactionDate').val(),
                BankName: $('#loanBankName').val(),
                ChequeNo: $('#loanChequeNo').val(),
                BankAccountNo: $('#loanBankAccount').val(),
                InstallmentNo: $('#LabelInstallmentNo').html(),
                Principle: $('#LabelPrinciple').html(),
                Interest: $('#LabelInterest').html(),
                EndingBalance: $('#LabelEndingBalance').html(),
                COAId: $('#ChartOfAccount option:selected').val(),
                //ExtraFee: $('#LabelExtraFee').html(),
                ExtraFee: $('#ExtraFee').val(),
                FineInterest: $('#LabelFineInterest').html(),
                TotalFineInterest: $('#LabelTotalFineInterest').html(),
                TotalUnpaidPrinciple: $('#LabelTotalUnpaidPrinciple').html(),
                UnpaidPrinciple: $('#LabelUnpaidPrinciple').html(),
                ExtraPayment: $('#LabelExtraPayment').html(),
                VoucherNo: $('#VoucherNo').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/Transaction/SaveInstallmentInformation",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        alert('Successfully done.');
                        clearTermLoanInfo();
                        clearFields();
                    } else {
                        alert('Failed');
                    }
                },
                error: function () {
                    alert('Error. Please try again.');
                }
            });
        } else {
            return false;
        }
    });

    function clearTermLoanInfo() {
       // alert('');
        //$("#LoanNo").val('');
        document.getElementById('LabelBeginningBalance').innerHTML = '';
        document.getElementById('LabelEndingBalance').innerHTML = '';
        document.getElementById('LabelInterest').innerHTML = '';
        document.getElementById('LabelPrinciple').innerHTML = '';
        document.getElementById('LabelUnpaidPrinciple').innerHTML = '';
        document.getElementById('LabelTotalUnpaidPrinciple').innerHTML = '';
        document.getElementById('LabelFineInterest').innerHTML = '';
        document.getElementById('LabelTotalFineInterest').innerHTML = '';
        document.getElementById('LabelExtraPayment').innerHTML = '';
        document.getElementById('LabelInstallmentAmount').innerHTML = '';
        document.getElementById('LabelCustomerName').innerHTML = '';
        document.getElementById('LabelInstallmentNo').innerHTML = '';
        document.getElementById('LabelExtraFee').innerHTML = '';
        document.getElementById('LabelOutstanding').innerHTML = '';
        //$("#LabelBeginningBalance").val('');
        //$("#LabelEndingBalance").val('');
        //$("#LabelInterest").val('');
        //$("#LabelPrinciple").val('');
        $("#InstallmentAmount").val('');
        $("#TotalPayment").val('');
        //$('#InstallmentDate').val('');
        //$('#TransactionDate').val('');
        //$('#LabelUnpaidPrinciple').val('');
        //$('#LabelTotalUnpaidPrinciple').val('');
        //$('#LabelFineInterest').val('');
        //$('#LabelTotalFineInterest').val('');
        //$('#LabelExtraPayment').val('');
        $('#hdfNumberOfInstallment').val('');
        //window.location.reload();
        $('#customerImage').attr('src', '');
    }


    function CheckTermLoanValidation() {
       // alert('te');
        var isAllValid = true;

        if ($('#LoanNo').val().trim() == '') {
            $('#LoanNo').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#LoanNo').siblings('span.error').css('display', 'none');
        }

        if ($('#Particular').val().trim() == 0) {
            $('#Particular').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#Particular').siblings('span.error').css('display', 'none');
        }
        if ($('#ChartOfAccount').val().trim() == 0) {
            $('#ChartOfAccount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').siblings('span.error').css('display', 'none');
        }
        if ($('#TotalPayment').val().trim() == '') {
            $('#TotalPayment').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#TotalPayment').siblings('span.error').css('display', 'none');
        }

        return isAllValid;
    }

    //********************************************************************************/

    $('#divBankName').hide();
    $('#divBankAccountNo').hide();
    $('#divChequeNo').hide();
    $('#loan_Bank').hide();
    $('#loan_Account').hide();
    $('#loan_Cheque').hide();
    $('#Conloan_Bank').hide();
    $('#Conloan_Account').hide();
    $('#Conloan_Cheque').hide();
    /****************************************************************************************************/
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/Transaction/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $('#ChartOfAccount').change(function () {
        removeErrorMessage(this);
    });

    $("#AccountNumber").change(function () {
        removeErrorMessage(this);
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetCustomerInfoByAccountNumber",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (result) {
                $('#CustomerName').text(result[0].CustomerName);
                $('#BranchName').text(result[0].BranchName);
                $('#AccountTypeName').text(result[0].AccountTypeName);
                $('#Balance').text(result[0].Balance);
                CloseAccountTransaction();
                //UnApprovedTransaction();
                //  console.log(result[0]);
                //$('#CustomerName').val(result[0].CustomerName);
                //$("#CustomerName").attr("readonly", "readonly");
                //$('#AccountTypeName').val(result[0].AccountTypeName);
                //$("#AccountTypeName").attr("readonly", "readonly");
                //$('#Balance').val(result[0].Balance);
                //$("#Balance").attr("readonly", "readonly");
                $('#AccountSetupId').val(result[0].AccountSetupId);
                $('#InterestRate').val(result[0].InterestRate);
                $('#CustomerImage').attr('src', result[0].CustomerImage);
            }
        });
    });

    function UnApprovedTransaction() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetUnApprovedTransactionByAcNo",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $("#submit").removeAttr('disabled');
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
                }
                CloseAccountTransaction();
            },
        });
    }

    function CloseAccountTransaction() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetCloseAccountTransaction",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                if (data === false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account was closed !!!');
                }
            }
        });
    }


    //Submit Transaction Info


    $('#submit').click(function () {
        var isAllValid = true;


        if ($('#TransactionDate').val().trim() === '') {
            $('#TransactionDate').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#TransactionDate').siblings('span.error').css('display', 'none');
        }

        if ($('#AccountNumber').val().trim() === '') {
            $('#AccountNumber').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#AccountNumber').siblings('span.error').css('display', 'none');
        }

        //if ($('#TransactionType').val().trim() == 0) {
        //    $('#TransactionType').siblings('span.error').css('display', 'block');
        //    isAllValid = false;
        //} else {
        //    $('#TransactionType').siblings('span.error').css('display', 'none');
        //}
        if ($('#ChartOfAccount').val().trim() == 0) {
            $('#ChartOfAccount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').siblings('span.error').css('display', 'none');
        }

        if ($('#Amount').val().trim() === '') {
            $('#Amount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#Amount').siblings('span.error').css('display', 'none');
        }

        //var transactionType = $("#TransactionType option:selected").text();
        //var amount = parseFloat($('#Amount').val().replace(/,/g, ''));
        //var currentBalance = parseFloat($('#Balance').val());
        //if (transactionType == 'Withdraw' && (currentBalance - 100) < amount) {
        //    isAllValid = false;
        //}
        if (isAllValid) {

            var data = {
                AccountNumber: $('#AccountNumber').val(),
                //TransactionType: $('#TransactionType option:selected').val(),
                TransactionType: 'Deposit',
                TransactionDate: $('#TransactionDate').val(),
                InterestRate: $('#InterestRate').val(),
                Amount: parseFloat($('#Amount').val().replace(/,/g, '')),
                AccountSetupId: $('#AccountSetupId').val(),
                IsCashEntry: $("#IsCashEntry").prop('checked'),
                BankName: $('#BankName').val(),
                BankAccountNo: $('#BankAccountNo').val(),
                ChequeNo: $('#ChequeNo').val(),
                COAId: $('#ChartOfAccount option:selected').val(),
                Particular: $('#DepositParticular').val(),
                VoucherNo: $('#VoucherNo').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/Transaction/Index",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        alert('Successfully done.');
                        clearFields();
                        clearCustomerInfo();
                    } else {
                        alert('Failed');
                    }
                    //$('#submit').val('Save');
                    //window.location.reload();

                },
                error: function () {
                    alert('Error. Please try again.');
                    //$('#submit').val('Save');
                }
            });
        }
    });
});

function clearFields() {
    $('#VoucherNo').val('');
    $("#ChartOfAccount").val("0");
    $("#ChartOfAccount").change();

    $("#LoanNo").val('');
    $("#Particular").val('');
    $('#ExtraFee').val('');
    $("#InwordsTermLoanDepositAmount").val('');

    $("#AccountNumber").val('');
    $("#InterestRate").val('');
    $("#Amount").val('');
    $('#CustomerId').val('');
    $('#AccountSetupId').val('');
    $('#CustomerName').val('');
    $('#DepositParticular').val('');
    $('#MessageForNumberToWordConvert').val('');

    $("#ConLoanId").val('');
    $("#ConParticular").val('');
    $("#ConloanBankName").val('');
    $("#ConloanBankAccount").val('');
    $("#ConloanChequeNo").val('');
    $("#ConDepositAmount").val('');
    $("#hdfAvailableBalance").val('');
    $("#InwordsConDepositAmount").val('');
}

//Deposit Continuous Loan Amount Js Area Start
$('.datepicker').datepicker({
    dateFormat: "dd/M/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+0"
});


$("#ConLoanId").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: window.applicationBaseUrl + "Loan/ContinuousLoan/AutoCompleteLoanId",
            type: "GET",
            dataType: "json",
            data: { term: request.term },
            success: function (data) {
                if (data.length == 0) {
                    alert("No Account Available for Transaction");
                } else {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }

            }
        });
    },
    messages: {
        noResults: "",
        results: ""
    }
});

$("#ConLoanId").change(function () {
    removeErrorMessage(this);
    var LoanId = $("#ConLoanId").val();
    var json = { LoanId: LoanId };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/ContinuousLoan/GetContinuousLoanInfo",
        contentType: "application/json",
        data: JSON.stringify(json),
        success: function (result) {
            //console.log(result);
            document.getElementById('LabelCustomerLoanLimit').innerHTML = result.CustomerLoanLimit;
            document.getElementById('ConLabelCustomerName').innerHTML = result.CustomerName;
            $('#customerImage2').attr('src', result.CustomerImage);
            document.getElementById('ConLabelFatherName').innerHTML = result.FatherName;
            document.getElementById('ConLabelAvailableBalance').innerHTML = result.AvailableBalance;
            document.getElementById('ConLabelOutstanding').innerHTML = result.Outstanding;
            $('#hdfCustomerId').val(result.CustomerId);
            $('#hdfAvailableBalance').val(result.AvailableBalance);
            $('#hdfOutstanding').val(result.Outstanding);
        },
    });

});

$('#btnConDepositAmount').click(function () {

    var isAllValid = true;

    if ($('#TransactionDate').val().trim() === '') {
        $('#TransactionDate').siblings('span.error').css('display', 'block');
        isAllValid = false;
    } else {
        $('#TransactionDate').siblings('span.error').css('display', 'none');
    }

    if ($('#ConLoanId').val().trim() === '') {
        $('#ConLoanId').siblings('span.error').css('display', 'block');
        isAllValid = false;
    } else {
        $('#ConLoanId').siblings('span.error').css('display', 'none');
    }

    //if ($('#ConTransactionDate').val().trim() == '') {
    //    $('#ConTransactionDate').siblings('span.error').css('display', 'block');
    //    isAllValid = false;
    //} else {
    //    $('#ConTransactionDate').siblings('span.error').css('display', 'none');
    //}
    if ($('#ConDepositAmount').val().trim() == '') {
        $('#ConDepositAmount').siblings('span.error').css('display', 'block');
        isAllValid = false;
    } else {
        $('#ConDepositAmount').siblings('span.error').css('display', 'none');
    }

    if ($('#ChartOfAccount').val().trim() == '0') {
        $('#ChartOfAccount').siblings('span.error').css('display', 'block');
        isAllValid = false;
    } else {
        $('#ChartOfAccount').siblings('span.error').css('display', 'none');
    }
    //var availableBalance = parseFloat($("#hdfAvailableBalance").val());
    //var withdrawAmount = parseFloat($('#WithdrawAmount').val());

    //if (withdrawAmount > availableBalance) {
    //    isAllValid = false;
    //    alert('Please Check Available Balance');
    //} else {
    //    isAllValid = true;
    //}

    if (isAllValid) {

        var data = {
            LoanId: $('#ConLoanId').val(),
            CustomerId: $("#hdfCustomerId").val(),
            AvailableBalance: $("#hdfAvailableBalance").val(),
            Outstanding: $("#hdfOutstanding").val(),
            //TransactionDate: $('#ConTransactionDate').val(),
            TransactionDate: $('#TransactionDate').val(),
            Particular: $('#ConParticular').val(),
            DepositAmount: $('#ConDepositAmount').val(),
            IsChequeEntry: $("#ConIsChequeEntry").prop('checked'),
            BankName: $('#ConloanBankName').val(),
            BankAccountNo: $('#ConloanBankAccount').val(),
            ChequeNo: $('#ConloanChequeNo').val(),
            COAId: $('#ChartOfAccount option:selected').val(),
            VoucherNo: $('#VoucherNo').val()
        }
        $.ajax({
            url: window.applicationBaseUrl + "Loan/ContinuousLoan/SaveContinuousLoanDeposit",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.oCommonResult.Status === true) {
                    clearFields();
                    document.getElementById('LabelCustomerLoanLimit').innerHTML = '';
                    document.getElementById('ConLabelCustomerName').innerHTML = '';
                    document.getElementById('ConLabelFatherName').innerHTML = '';
                    document.getElementById('ConLabelAvailableBalance').innerHTML = '';
                    document.getElementById('ConLabelOutstanding').innerHTML = '';
                    $('#customerImage2').attr('src', '');
                    //document.getElementById('showMessage').style.color = "green";
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                } else {
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                }

            },
            error: function () {
                document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
            }
        });
    }
});

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        return false;
    } else if (charCode === 13) {
        return false;
    }
    //status = "";
    return true;
}

//using comma seperation in amount
//$(document).on('keyup', '#Amount', function () {
//    var x = $(this).val();
//    $(this).val(x.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//});


//---checking current balance and withdraw amount

//$("#Amount").keyup(function () {
//    var currentBalance = parseFloat($('#Balance').val());

//    var transactionType = $("#TransactionType option:selected").text();

//    var status = $("#MessageForExistChek"); //DIV object to display the status message
//    var ttDropdownValue = $.trim(transactionType);
//    if (ttDropdownValue == 'Withdraw') {
//        var amount = parseFloat($('#Amount').val().replace(/,/g, ''));

//        if ((currentBalance - 100) >= amount) {

//            status.html("<font color=green><b>" + Number(currentBalance - amount - 100).toLocaleString('en') + "</b>TK. is Available!</font>");
//        }
//        else {
//            //status.html("<font color=red>'<b>" + amount + "</b>'TK. your balance is not sufficient.! and you have to keep minimum 100TK in your account.</font>");
//            status.html("<font color=red>Maxiumum <b>" + Number(currentBalance - 100).toLocaleString('en') + "</b> TK. can be withdrawn </font>" +
//                "<br> <font color=red>**Note:You have to keep minimum 100TK in your account.</font>");
//            //$("#submit").attr("disabled", "disabled");
//        }
//    }
//});

$("#Amount").keyup(function () {
    removeErrorMessage(this);
    var amount = parseInt($('#Amount').val().replace(/,/g, '')); //parseFloat($('#Amount').val())
    var status = $("#MessageForNumberToWordConvert"); //DIV object to display the status message
    var user = $.trim(amount);
    if (user.length >= 1) {
        //status.html("Checking....") //While our Thread works, we will show some message to indicate the progress
        status.val("Checking....");
        //jQuery AJAX Post request
        $.post(window.applicationBaseUrl + "Transaction/Transaction/GetConvertNumberToWord", { number: amount },
            function (data) {
                var money = data + ' Tk Only';
                status.val(money);
                //status.html("<font color=green>'<b>" + data + "</b>' TK Only</font>");
            });
    } else {
        //status.html("Need more characters...");
        status.val("Need more characters...");
    }
});

$("#ConDepositAmount").keyup(function () {
    removeErrorMessage(this);
    var amount = parseInt($(this).val().replace(/,/g, ''));
    var status = $("#InwordsConDepositAmount");
    var user = $.trim(amount);
    if (user.length >= 1) {
        status.val("Checking....");
        //jQuery AJAX Post request
        $.post(window.applicationBaseUrl + "Transaction/Transaction/GetConvertNumberToWord", { number: amount },
            function (data) {
                var money = data + ' Tk Only';
                status.val(money);
            });
    } else {
        status.val("Need more characters...");
    }
});

$("#TotalPayment").keyup(function () {
    removeErrorMessage(this);


    var amount = parseInt($(this).val().replace(/,/g, ''));
    var status = $("#InwordsTermLoanDepositAmount");
    var user = $.trim(amount);
    if (user.length >= 1) {
        status.val("Checking....");
        //jQuery AJAX Post request
        $.post(window.applicationBaseUrl + "Transaction/Transaction/GetConvertNumberToWord", { number: amount },
            function (data) {
                var money = data + ' Tk Only';
                status.val(money);
            });
    } else {
        status.val("Need more characters...");
    }

});

function removeErrorMessage(control) {
    var $this = $(control);
    if ($this.val().trim() === 0) {
        $this.siblings('span.error').css('display', 'block');
    } else {
        $this.siblings('span.error').css('display', 'none');
    }
}

function clearCustomerInfo() {
    $('#CustomerName').text('');
    $('#BranchName').text('');
    $('#AccountTypeName').text('');
    $('#Balance').text('');
    $('#CustomerImage').attr('src', '');
}