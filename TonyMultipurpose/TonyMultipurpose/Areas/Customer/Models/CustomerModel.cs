﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Customer.Models
{
    public class CustomerModel : CommonModel
    {
        [Key]
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public short? BranchId { get; set; }
        [Required(ErrorMessage = "Customer Name is Required")]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Father Name is Required")]
        public string FatherName { get; set; }

        public string Username { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }

        [Required(ErrorMessage = "Gender Required")]
        public string Gender { get; set; }
        public string OldImage { get; set; }
        public string CustomerImage { get; set; }

         [Required(ErrorMessage = "Date of Birth is Required")]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "Entry Date is Required")]
        public DateTime CustomerEntryDate { get; set; }
        public String PassportNo { get; set; }
        public DateTime? PassportExpireDate { get; set; }

        public String OccupationAndPosition { get; set; }
        public String Position { get; set; }

        public String TINNo { get; set; }

        public String DrivingLicenseNo { get; set; }

        public String TradeLicenseNo { get; set; }

       // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? TradeLicenseDate { get; set; }

        public String RegistrationAuthority { get; set; }

        public string TradeLicAuthority { get; set; }
        
        public String RegistrationNo { get; set; }
        public string Mobile { get; set; }
        public string Home { get; set; }
        public string Office { get; set; }
        public string AnotherCellNo { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public string BirthCertificateNo { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string OfficeAddress { get; set; }

        public string CustomerIncomeSource { get; set; }
       
        public virtual ICollection<CustomerBankDetail> CustomerBankDetails { get; set; }

        public HttpPostedFileBase Imagefile { get; set; }
        public CustomerModel()
        {
            CustomerBankDetails = new List<CustomerBankDetail>();
        }
        public DataTable dtCommon { get; set; }
    }
}