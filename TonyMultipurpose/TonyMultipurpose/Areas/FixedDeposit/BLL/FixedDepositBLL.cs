﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.FixedDeposit.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.FixedDeposit.BLL
{
    //--ataur
    public class FixedDepositBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        //Generate Transaction Id
        private string GenerateTransactionId(FixedDepositModel objFixedDepositModel)
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            if (objFixedDepositModel.AccountNumber != null)
            {
                objDbDataReader = null;
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                int Id = 0;

                string year = Convert.ToString(DateTime.Now.Year);
                string days = Convert.ToString(DateTime.Now.DayOfYear);
                //var prefix = string.Concat(year + days);
                string prefix = year + days;

                objDbCommand.AddInParameter("FieldName", "TransactionId");
                objDbCommand.AddInParameter("TableName", "FixedDeposite");
                objDbCommand.AddInParameter("Prefix", prefix);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                    CommandType.StoredProcedure);
                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        transactionId = prefix;
                        Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        transactionId += Convert.ToString(Id).PadLeft(4, '0');
                    }
                }
                objDbDataReader.Close();
            }
            return transactionId;
        }



        /// <summary>
        /// get all account number for autocomplete
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public List<string> GetAllAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetFDAccountNoForAutocomplete]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountNumberList;
        }

        /// <summary>
        /// get fixed deposit account information for mature
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public FixedDepositModel GetFDAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            FixedDepositModel objFixedDepositModel=new FixedDepositModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetFixedDepositAccountInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                     
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objFixedDepositModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objFixedDepositModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objFixedDepositModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                            objFixedDepositModel.DurationofMonth = objDbDataReader["DurationofMonth"].ToString();
                            objFixedDepositModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                            if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["MatureDate"])))
                            {
                                objFixedDepositModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            }
                            objFixedDepositModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                            objFixedDepositModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                            objFixedDepositModel.Capital = Convert.ToDecimal(objDbDataReader["Capital"].ToString());
                            objFixedDepositModel.AccruedBalance = Convert.ToDecimal(objDbDataReader["AccuredBalance"].ToString());
                            objFixedDepositModel.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"].ToString());
                            objFixedDepositModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"].ToString());
                            //objCssModel.CustomerImage = objDbDataReader["CustomerImage"].ToString();
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objFixedDepositModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objFixedDepositModel.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objFixedDepositModel;
        }

        /// <summary>
        /// save fixed deposit mature encashment
        /// </summary>
        /// <param name="objFixedDepositModel"></param>
        public void SaveMatureFixedDepositInfo(FixedDepositModel objFixedDepositModel)
        {
            int noOfAffacted = 0;
            objFixedDepositModel.TransactionId = GenerateTransactionId(objFixedDepositModel);

            if (objFixedDepositModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", objFixedDepositModel.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", objFixedDepositModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", objFixedDepositModel.CustomerId);
                objDbCommand.AddInParameter("Capital", objFixedDepositModel.Capital);
                objDbCommand.AddInParameter("InterestRate", objFixedDepositModel.InterestRate);
                objDbCommand.AddInParameter("DurationofMonth", objFixedDepositModel.DurationofMonth);
                objDbCommand.AddInParameter("MatureAmount", objFixedDepositModel.MatureAmount);
                objDbCommand.AddInParameter("MatureDate", objFixedDepositModel.MatureDate);
                objDbCommand.AddInParameter("AccruedBalance", objFixedDepositModel.AccruedBalance);
                objDbCommand.AddInParameter("COAId", objFixedDepositModel.COAId);


                objDbCommand.AddInParameter("transactionId", objFixedDepositModel.TransactionId);

                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveFixedDepositMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }
        }
        public void SavePreMatureFixedDepositInfo(FixedDepositModel objFixedDepositModel)
        {
            int noOfAffacted = 0;
            objFixedDepositModel.TransactionId = GenerateTransactionId(objFixedDepositModel);

            if (objFixedDepositModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", objFixedDepositModel.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", objFixedDepositModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", objFixedDepositModel.CustomerId);
                objDbCommand.AddInParameter("InterestRate", objFixedDepositModel.InterestRate);
                objDbCommand.AddInParameter("DurationofMonth", objFixedDepositModel.DurationofMonth);
                objDbCommand.AddInParameter("PreMatureAmount", objFixedDepositModel.PreMatureAmount);
                objDbCommand.AddInParameter("MatureDate", objFixedDepositModel.MatureDate);
                objDbCommand.AddInParameter("AccruedBalance", objFixedDepositModel.AccruedBalance);
                objDbCommand.AddInParameter("COAId", objFixedDepositModel.COAId);

                objDbCommand.AddInParameter("transactionId", objFixedDepositModel.TransactionId);

                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveFixedDepositPreMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }
        }
        public void SaveFDWithdrawInterestInfo(FixedDepositModel objFixedDepositModel)
        {
            int noOfAffacted = 0;
            objFixedDepositModel.TransactionId = GenerateTransactionId(objFixedDepositModel);

            if (objFixedDepositModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", objFixedDepositModel.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", objFixedDepositModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", objFixedDepositModel.CustomerId);
                objDbCommand.AddInParameter("InterestRate", objFixedDepositModel.InterestRate);
                objDbCommand.AddInParameter("DurationofMonth", objFixedDepositModel.DurationofMonth);
                objDbCommand.AddInParameter("AccruedBalance", objFixedDepositModel.AccruedBalance);
                objDbCommand.AddInParameter("MatureDate", objFixedDepositModel.MatureDate);
                objDbCommand.AddInParameter("COAId", objFixedDepositModel.COAId);

                objDbCommand.AddInParameter("transactionId", objFixedDepositModel.TransactionId);

                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveFDWithdrawInterestInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }
        }

        //Added By: Nayeem
        //Date: 23-Jul-2017
        //Generate a list of Unapproved List
        public List<FixedDepositModel> GetUnapprovedFixedDeposit(FixedDepositModel fixedDepositModel)
        {
            DbDataReader objDbDataReader = null;
            var listOfFixedDeposit = new List<FixedDepositModel>();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("IsApproved", fixedDepositModel.IsApproved);
            objDbCommand.AddInParameter("CompanyId", fixedDepositModel.CompanyId);
            if (objDbCommand == null) return null;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand,
                    "[dbo].[uspGetFixedDepositUnApprovedList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        var objModel = new FixedDepositModel();
                        BuildModelForFixedDeposit(objDbDataReader, objModel);
                        listOfFixedDeposit.Add(objModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfFixedDeposit;
        }

        //Added By: Nayeem
        //Date: 23-Jul-2017
        private void BuildModelForFixedDeposit(IDataReader objDataReader, FixedDepositModel objFixedDepositModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            if (objDataTable == null) return;
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDataReader["TransactionId"]))
                        {
                            objFixedDepositModel.TransactionId = objDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objFixedDepositModel.Id = Convert.ToInt64(objDataReader["Id"].ToString());
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDataReader["TransactionDate"]))
                        {
                            objFixedDepositModel.TransactionDate = Convert.ToDateTime(objDataReader["TransactionDate"].ToString());
                        }
                        break;

                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objFixedDepositModel.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDataReader["TransactionType"]))
                        {
                            objFixedDepositModel.TransactionType = objDataReader["TransactionType"].ToString();
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objFixedDepositModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objFixedDepositModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objFixedDepositModel.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objFixedDepositModel.BranchId = Convert.ToInt16(objDataReader["BranchId"].ToString());
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objFixedDepositModel.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objFixedDepositModel.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objFixedDepositModel.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;
                    case "Debit":
                        if (!Convert.IsDBNull(objDataReader["Debit"]))
                        {
                            objFixedDepositModel.Debit = Convert.ToDecimal(objDataReader["Debit"].ToString());
                        }
                        break;
                    case "Credit":
                        if (!Convert.IsDBNull(objDataReader["Credit"]))
                        {
                            objFixedDepositModel.Credit = Convert.ToDecimal(objDataReader["Credit"].ToString());
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objFixedDepositModel.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "AccruedBalance":
                        if (!Convert.IsDBNull(objDataReader["AccruedBalance"]))
                        {
                            objFixedDepositModel.AccruedBalance = Convert.ToDecimal(objDataReader["AccruedBalance"].ToString());
                        }
                        break;
                    case "Balance":
                        if (!Convert.IsDBNull(objDataReader["Balance"]))
                        {
                            objFixedDepositModel.Balance = Convert.ToDecimal(objDataReader["Balance"].ToString());
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDataReader["Amount"]))
                        {
                            objFixedDepositModel.Amount = Convert.ToDecimal(objDataReader["Amount"].ToString());
                        }
                        break;

                    case "Particular":
                        if (!Convert.IsDBNull(objDataReader["Particular"]))
                        {
                            objFixedDepositModel.Particular = objDataReader["Particular"].ToString();
                        }
                        break;
                    case "IsApproved":
                        if (!Convert.IsDBNull(objDataReader["IsApproved"]))
                        {
                            objFixedDepositModel.IsApproved = Convert.ToBoolean(objDataReader["IsApproved"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objFixedDepositModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objFixedDepositModel.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        public string ApprovedFdTransaction(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                foreach (var id in ch)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("TransactionId", id);
                    objDbCommand.AddInParameter("IsApproved", true);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApproveFdTransaction]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";








        }
        public FixedDepositModel GetFixedDepositTransactionsById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            var listOfFdTransaction = new List<FixedDepositModel>();

            var objFixedDepositModel = new FixedDepositModel();

            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetFixedDepositTransactionsById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objFixedDepositModel = new FixedDepositModel();
                        BuildModelForFixedDeposit(objDbDataReader, objFixedDepositModel);
                        listOfFdTransaction.Add(objFixedDepositModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }

            return objFixedDepositModel;
        }
        public string RejectFixedDepositeTransaction(List<FixedDepositModel> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspRejectFixedDepositTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public FixedDepositModel GetFDAccountInfoByAccountNoForInterestWithdraw(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            FixedDepositModel objFixedDepositModel = new FixedDepositModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetFDAccountInfoForInterestWithdraw]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                            objFixedDepositModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objFixedDepositModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objFixedDepositModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                            objFixedDepositModel.DurationofMonth = objDbDataReader["DurationofMonth"].ToString();
                            //objFixedDepositModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                            objFixedDepositModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            objFixedDepositModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                            objFixedDepositModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                            objFixedDepositModel.Capital = Convert.ToDecimal(objDbDataReader["Capital"].ToString());
                            //objFixedDepositModel.AccruedBalance = Convert.ToDecimal(objDbDataReader["AccuredBalance"].ToString());
                            objFixedDepositModel.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"].ToString());
                            objFixedDepositModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"].ToString());

                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objFixedDepositModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objFixedDepositModel.CustomerImage = "\\Not Found\\";
                            }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objFixedDepositModel;
        }

        public FixedDepositModel GetFDAccountInfoByAccountNoForPremature(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            FixedDepositModel objFixedDepositModel = new FixedDepositModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetFDAccountInfoForPreMature]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {

                        objFixedDepositModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objFixedDepositModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objFixedDepositModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objFixedDepositModel.DurationofMonth = objDbDataReader["DurationofMonth"].ToString();
                        objFixedDepositModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                        objFixedDepositModel.Capital = Convert.ToDecimal(objDbDataReader["Capital"].ToString());
                        objFixedDepositModel.PreMatureAmount = Convert.ToDecimal(objDbDataReader["PreMatureAmount"].ToString());
                        objFixedDepositModel.AccruedBalance = Convert.ToDecimal(objDbDataReader["AccruedBalance"].ToString());
                        objFixedDepositModel.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"].ToString());
                        objFixedDepositModel.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"].ToString());

                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objFixedDepositModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objFixedDepositModel.CustomerImage = "\\Not Found\\";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objFixedDepositModel;
        }
    }
}