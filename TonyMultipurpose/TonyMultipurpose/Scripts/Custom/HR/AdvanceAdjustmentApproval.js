﻿(function () {

    advanceAdjustmentTable();
    var oTable;

    function advanceAdjustmentTable() {
        oTable = $('#AdvanceAdjustmentTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/EmpAdvanceAdjustmentUnApprovaList",//'@Url.Action("EmpAdvanceAdjustmentUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                { "data": "EmployeeName" },
                {
                    "data": "AdvanceDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "AdvanceAmount" },
                { "data": "Description" },
                {
                    "data": "EffectiveDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "InstallmentNo" },
                {
                    "data": "EmpCode",
                    "render": function (data) {
                        return '<a class="btn btn-primary" href="' + window.applicationBaseUrl + 'HR/HR/EmpAdvanceAdjustmentApproval?id=' + data + '">Approve</a>';
                    }
                }
            ],
            "order": [[0, "desc"]]
        });
    }

    var months = [
         'Jan', 'Feb', 'Mar', 'Apr', 'May',
         'Jun', 'Jul', 'Aug', 'Sep',
         'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();