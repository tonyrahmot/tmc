﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.DoubleBenefit.BLL;
using TonyMultipurpose.Areas.DoubleBenefit.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.DoubleBenefit.Controllers
{
    [AuthenticationFilter]
    public class DBMatureEncahmentController : Controller
    {
        // GET: DoubleBenefit/DBMatureEncahment

        DoubleBenefitBLL objDoubleBenefitBLL = new DoubleBenefitBLL();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objDoubleBenefitBLL.GetDBAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objDoubleBenefitBLL.GetDBAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(Doublebenefit objdoubleBenefit)
        {
            bool status = true;

            objDoubleBenefitBLL.SaveDBMatureEncashmentInfo(objdoubleBenefit);

            return new JsonResult { Data = new { status = status } };
        }

        public JsonResult GetUnAppovedDBList(string id)
        {
            bool exsits = objDoubleBenefitBLL.GetUnAppovedDBList(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetCloseDBAccount(string id)
        {
            bool exsits = objDoubleBenefitBLL.GetCloseDBAccount(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        public JsonResult GetTransactionalCoaData()
        {
            TransactionBLL objTransactionBll = new TransactionBLL();
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}