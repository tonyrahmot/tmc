﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.MonthlyBenefit.Models
{
    public class MBModel : CommonModel
    {
        [Key]
        public long Id { get; set; }
        public string TransactionId { get; set; }
        public string Username { get; set; }

        [Display(Name = "Date")]
        public DateTime? TransactionDate { get; set; }

        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }

        [Display(Name = "A/C No")]
        public string AccountNumber { get; set; }

        [Display(Name = "Interest Amount")]
        public decimal? MonthlyBenefitAmount { get; set; }
            
        public string CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public short? BranchId { get; set; }
        public string BranchName { get; set; }
        public string CompanyName { get; set; }

        public int? AccountSetupId { get; set; }

        [Display(Name = "Account Type Name")]
        public string AccountTypeName { get; set; }

        [Display(Name = "Debit")]
        public decimal? Debit { get; set; }

        [Display(Name = "Credit")]
        public decimal? Credit { get; set; }

        [Display(Name = "Balance")]
        public decimal? Balance { get; set; }
        public string AccruedBalance { get; set; }

        public decimal? SavingsInterestRate { get; set; }

        public DateTime OpeningDate { get; set; }

        [Display(Name = "Particular")]
        public string Particular { get; set; }
        public decimal? DepositAmount { get; set; }
        public decimal? ReturnAmount { get; set; }
        public string DurationofYear { get; set; }
        public string DurationofMonth { get; set; }

        [Display(Name = "Mature Date")]
        public DateTime? MatureDate { get; set; }

        public string CustomerImage { get; set; }
        public string RejectReason { get; set; }
        public int COAId { get; set; }
    }
}