﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Share.ViewModels
{
    public class IssuedShareViewModel
    {
        public int Id { get; set; }
        public int? AuthorizedShareInfoId { get; set; }
        public string TransactionId { get; set; }
        public string EntryType { get; set; }
        public decimal? IssuedQty { get; set; }
        public decimal? PricePerShare { get; set; }
        public decimal? IssuedAmount { get; set; }
        public decimal? SoldQty { get; set; }
        public decimal? Balance { get; set; }
        public string IssuedDate { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsRejected { get; set; }
        public string RejectReason { get; set; }
        public string Particular { get; set; }
        public short? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public short? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public decimal RemainingBalance { get; set; }
        public decimal? PaidUpAmount { get; set; }
    }
}