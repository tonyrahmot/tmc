﻿using System.Web.Mvc;
using TonyMultipurpose.BLL;

namespace TonyMultipurpose.Controllers
{
    public class NavigationController : Controller
    {
        // GET: Navigation
        public ActionResult Index()
        {
            NavigationBLL objNavigationBll = new NavigationBLL();

            var menu = objNavigationBll.GetAllMenu();
            return View(menu);
        }


        // GET: Navigation
        public ActionResult Admin()
        {
            NavigationBLL objNavigationBll = new NavigationBLL();

            var menu = objNavigationBll.GetAllMenu();
            return PartialView("_Admin", menu);
        }
    }
}