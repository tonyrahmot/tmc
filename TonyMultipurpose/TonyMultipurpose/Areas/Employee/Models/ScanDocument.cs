﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    public class ScanDocument : CommonModel
    {
        [Key]
        public short DocumentId { get; set; }

       // [Required]
        [Display(Name = "Employee")]
        public short EmployeeId { get; set; }

        [DataType(DataType.Text)]
       // [Required(ErrorMessage = "Employee Name is Required")]
        [Display(Name = "Employee Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Employee Name cannot be longer than 100 characters.")]
        public string EmployeeName { get; set; }

       // [Required]
        [Display(Name = "Document Type")]
        public byte DocumentTypeId { get; set; }

      //  [Required]
        [Display(Name = "Type Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Type Name should between at least 2 to 100 characters long.")]
        public string TypeName { get; set; }


        [DataType(DataType.Text)]
      //  [Required(ErrorMessage = "Document Name is Required")]
        [Display(Name = "Document Name")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Document Name cannot be longer than 100 characters.")]
        public string DocumentName { get; set; }
        public byte[] Image { get; set; }

        public IEnumerable<EmployeeDetail> EmployeeInfo { get; set; }
        public IEnumerable<DocumentType> DocumentInfo { get; set; }



    }
}