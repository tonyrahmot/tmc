﻿
    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/AutoCompleteAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/GetTransactionalCoaData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});

//load data
$("#AccountNumber").change(function() {
    var AccountNumber = $("#AccountNumber").val();

    var json = {
        AccountNumber: AccountNumber
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/GetAccountInfoByAccountNo",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            UnAppovedDBList();
            $('#CustomerName').val(data.CustomerName);
            $('#CustomerId').val(data.CustomerId);
            $('#Duration').val(data.DurationofYear);
            $('#MatureDate').val(FormateDate(data.MatureDate));
            $('#MatureAmount').val(data.MatureAmount);
            $('#DepositAmount').val(data.Amount);
            $('#CustomerImage').attr('src', data.CustomerImage);
            $('#TransactionDate').val(FormateDate(data.TransactionDate));
            $('#InterestRate').val(data.InterestRate);
            $('#InterestAmount').val(data.AccruedBalance);


            var accruedAmount = $('#InterestAmount').val(data.AccruedBalance);
            var matureAmount = $('#MatureAmount').val(data.MatureAmount);
            //alert(data.AccruedBalance); alert(data.MatureAmount);

            if (parseFloat(matureAmount) <= parseFloat(accruedAmount))
            {
                $('#submit').attr("disabled", false);
            } else {
                document.getElementById('message').innerHTML = "**NB:This Account is not Mature Yet!Plz go to premature menu.";
                $('#submit').hide();
            }



            //var oDate = $('#TransactionDate').val();
            //var cDate = $('#MatureDate').val();
            //// usrDate = opening date
            //var usrDate = new Date(oDate);
            //var curDate = new Date(cDate);
            //var usrYear, usrMonth = usrDate.getMonth() + 1;
            //var curYear, curMonth = curDate.getMonth() + 1;
            //if ((usrYear = usrDate.getFullYear()) < (curYear = curDate.getFullYear())) {
            //    curMonth += (curYear - usrYear) * 12;
            //}
            //var diffMonths = curMonth - usrMonth;
            //if (usrDate.getDate() > curDate.getDate()) diffMonths--;
            ////alert("There are " + diffMonths + " months between " + usrDate + " and " + curDate);
            ////alert(diffMonths);


            //var today = new Date();
            //var dd = today.getDate();
            //var mm = today.getMonth() + 1; //January is 0!
            //var yyyy = today.getFullYear();

            //if (dd < 10) {
            //    dd = '0' + dd
            //}

            //if (mm < 10) {
            //    mm = '0' + mm
            //}
            //var month = monthNumToName(mm);
            //today = dd + '/' + month + '/' + yyyy;

            //var toDate = new Date(today);
            //var toYear, toMonth = toDate.getMonth() + 1;
            //if ((usrYear = usrDate.getFullYear()) < (toYear = toDate.getFullYear())) {
            //    toMonth += (toYear - usrYear) * 12;
            //}
            //var difftoMonths = toMonth - usrMonth;
            //if (usrDate.getDate() > toDate.getDate())
            //    difftoMonths--;

            ////alert(difftoMonths);

            //if (difftoMonths >= diffMonths) {

            //} else {
            //    $('#submit').attr("disabled", true);
            //    alert("This double Benefit Account is not Mature." +
            //        "Please go to menu PreMatureEncashment for Withdraw." +
            //    "This account will be matured at " + cDate + "");
            //}


        }
    });
});

function UnAppovedDBList() {
    var id = $("#AccountNumber").val();
    var json = { id: id };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/GetUnAppovedDBList",
        contentType: "application/json",
        data: JSON.stringify(json),
        success: function (data) {
            $("#submit").removeAttr('disabled');
            if (data == false) {
                $("#submit").attr('disabled', 'disabled');
                alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
            }
            else
            {
                CloseDBAccount();
            }

        },

    });
}

function CloseDBAccount() {
    var id = $("#AccountNumber").val();
    var json = { id: id };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/GetCloseDBAccount",
        contentType: "application/json",
        data: JSON.stringify(json),
        success: function (data) {
            if (data == false) {
                $("#submit").attr('disabled', 'disabled');
                alert('This Account was closed !!!');
            }
        },
    });
}

$('#submit').click(function() {

    var isAllValid = true;
    if ($('#AccountNumber').val().trim() == '') {
        $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#AccountNumber').parent().next().find('span').css('visibility', 'visible');
    }

    if ($('#MatureAmount').val().trim() == '') {
        $('#MatureAmount').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#MatureAmount').parent().next().find('span').css('visibility', 'visible');
    }

    if ($('#MatureDate').val().trim() == '') {
        $('#MatureDate').parent().next().find('span').css('visibility', 'visible');
        isAllValid = false;
    } else {
        $('#MatureDate').parent().next().find('span').css('visibility', 'visible');
    }
    if ($('#InterestRate').val() == '') {
        isAllValid = false;
        $('#InterestRate').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#InterestRate').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#InterestAmount').val() == '') {
        isAllValid = false;
        $('#InterestAmount').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#InterestAmount').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#CustomerName').val() == '') {
        isAllValid = false;
        $('#CustomerName').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#CustomerName').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#CustomerId').val() == '') {
        isAllValid = false;
        $('#CustomerId').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#CustomerId').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#DepositAmount').val() == '') {
        isAllValid = false;
        $('#DepositAmount').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#DepositAmount').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#Duration').val() == '') {
        isAllValid = false;
        $('#Duration').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#Duration').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#TransactionDate').val() == '') {
        isAllValid = false;
        $('#TransactionDate').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#TransactionDate').parent().next().find('span').css('visibility', 'hidden');
    }
    if ($('#ChartOfAccount').val() == 0) {
        isAllValid = false;
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'visible');
    } else {
        $('#ChartOfAccount').parent().next().find('span').css('visibility', 'hidden');
    }



    if (isAllValid) {

        var data = {
            AccountNumber: $('#AccountNumber').val(),
            CustomerName: $('#CustomerName').val(),
            CustomerId: $('#CustomerId').val(),
            DurationofYear: $('#Duration').val(),
            MatureDate: $('#MatureDate').val(),
            MatureAmount: $('#MatureAmount').val(),
            AccruedBalance: $('#InterestAmount').val(),
            Amount: $('#DepositAmount').val(),
            COAId: $('#ChartOfAccount option:selected').val()
        }

        //console.log(data);
        $.ajax({
            url: window.applicationBaseUrl + "DoubleBenefit/DBMatureEncahment/Save",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status === true) {
                    alert('Successfully done.');
                    $("#AccountNumber").val('');
                    $("#CustomerName").val('');
                    $("#CustomerId").val('');
                    $("#Duration").val('');
                    $('#MatureDate').val('');
                    $('#MatureAmount').val('');
                    $('#ChartOfAccount').val('0');

                } else {
                    alert('Failed');
                }
                $('#submit').val('Save');
                window.location.reload();

            },
            error: function () {
                alert('Error. Please try again.');
            }
        });
    }

});

// for Json Date Formate
function FormateDate(jsonDate) {
    var monthNames = [
         "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}


var months = [
    'January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September',
    'October', 'November', 'December'
];


function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}

function monthNameToNum(monthname) {
    var month = months.indexOf(monthname);
    return month ? month + 1 : 0;
}

//Integer number check
function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode === 13 || charCode === 46) {
        return false;
    }
    //status = "";
    return true;
}

