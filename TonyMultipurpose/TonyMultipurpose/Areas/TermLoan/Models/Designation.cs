﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.TermLoan.Models
{
    public class Designation
    {
        public string EmployeeName { get; set; }
        public string DesignationName { get; set; }
        public string House { get; set; }
        public string UserUniqId { get; set; }
        public string UserTempId { get; set; }
    }
}