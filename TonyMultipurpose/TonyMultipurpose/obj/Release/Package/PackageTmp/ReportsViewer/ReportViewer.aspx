﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="TonyMultipurpose.ReportsViewer.ReportViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Viewer</title>
    <style type="text/css">
        html, body {
            height: 100%;
            padding: 0px;
            margin: 0px;
        }

        #rptViewer {
            width: 1000px;
            margin: auto;
        }

        #form1 {
            width: 100%;
            height: 100%;
            padding: 0px;
            margin: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="rptViewer">
            <CR:CrystalReportViewer ID="crpViewer" runat="server" Width="100%" />
        </div>
        <div>
            <div class="text-center footer">
                Copyright © 2017 <a href="http://juvenilepacersbd.com/">Juvenile PacersBD</a>. All rights reserved.
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</body>
</html>
