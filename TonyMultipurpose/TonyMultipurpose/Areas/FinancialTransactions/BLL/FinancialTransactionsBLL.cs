﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.BLL
{
    public class FinancialTransactionsBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public List<FinancialTransactionsModel> GetUnApprovedFinancialTransactionsList(FinancialTransactionsModel _objFTModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<FinancialTransactionsModel> _objFinancialTransactionsList = new List<FinancialTransactionsModel>();
            FinancialTransactionsModel _objFinancialTransactions;
            try
            {
                objDbCommand.AddInParameter("IsApproved", _objFTModel.IsApproved);
                //objDbCommand.AddInParameter("CompanyId", _objFTModel.CompanyId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[FinancialTransactions_GetAllUnApprovedInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objFinancialTransactions = new FinancialTransactionsModel();
                        _objFinancialTransactions.Id = Convert.ToInt32(objDbDataReader["Id"]);
                        _objFinancialTransactions.TransactionId = Convert.ToString(objDbDataReader["TransactionId"]);
                        _objFinancialTransactions.VoucherNo = Convert.ToInt64(objDbDataReader["VoucherNo"]);
                        _objFinancialTransactions.VoucherType = Convert.ToString(objDbDataReader["VoucherType"]);
                        _objFinancialTransactions.VoucherCode = Convert.ToString(objDbDataReader["VoucherCode"]);
                        _objFinancialTransactions.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"]);
                        _objFinancialTransactions.EntryType = Convert.ToString(objDbDataReader["EntryType"]);
                        _objFinancialTransactions.Particular = Convert.ToString(objDbDataReader["Particular"]);
                        _objFinancialTransactions.Debit = Convert.ToInt32(objDbDataReader["Debit"]);
                        _objFinancialTransactions.Credit = Convert.ToInt32(objDbDataReader["Credit"]);
                        _objFinancialTransactions.IsAdvancePayment = Convert.ToBoolean(objDbDataReader["IsAdvancePayment"]);
                        _objFinancialTransactions.IsChequePayment = Convert.ToBoolean(objDbDataReader["IsChequePayment"]);
                        _objFinancialTransactions.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"]);
                        _objFinancialTransactions.IsCleared = Convert.ToBoolean(objDbDataReader["IsCleared"]);
                        _objFinancialTransactions.ChequeNo = Convert.ToString(objDbDataReader["ChequeNo"]);
                        _objFinancialTransactions.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        _objFinancialTransactions.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        _objFinancialTransactions.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        _objFinancialTransactions.ClientName = Convert.ToString(objDbDataReader["ClientName"]);
                        _objFinancialTransactions.ClientCode = Convert.ToString(objDbDataReader["ClientCode"]);
                        _objFinancialTransactions.SupplierName = Convert.ToString(objDbDataReader["SupplierName"]);
                        _objFinancialTransactions.SupplierCode = Convert.ToString(objDbDataReader["SupplierCode"]);
                        _objFinancialTransactions.COAId = Convert.ToInt32(objDbDataReader["COAId"]);
                        _objFinancialTransactions.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                        _objFinancialTransactions.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        _objFinancialTransactions.CategoryId = Convert.ToInt32(objDbDataReader["CategoryId"]);
                        _objFinancialTransactions.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        _objFinancialTransactions.GroupId = Convert.ToInt16(objDbDataReader["GroupId"]);
                        _objFinancialTransactions.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        _objFinancialTransactions.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                        _objFinancialTransactions.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"]);
                        _objFinancialTransactions.CreatedDate = Convert.ToDateTime(objDbDataReader["CreatedDate"]);
                        _objFinancialTransactions.CreatedName = Convert.ToString(objDbDataReader["CreatedName"]);
                        _objFinancialTransactions.Remarks = Convert.ToString(objDbDataReader["Remarks"]);
                        _objFinancialTransactionsList.Add(_objFinancialTransactions);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return _objFinancialTransactionsList;
        }

        public string ApproveFinancialTransactions(string[] ids)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                foreach (var id in ids)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("Id", id);
                    objDbCommand.AddInParameter("IsApproved", true);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FinancialTransactions_Approve]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";
        }

        public string RejectFinancialTransactions(List<FinancialTransactionsModel> _objFinancialTransactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in _objFinancialTransactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FinancialTransactions_Reject]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public List<COA> GetAllAccountCode()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objAccountsList = new List<COA>();
            COA objCOA;
            try
            {
                //objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Account_GetAllAccountCode]", CommandType.StoredProcedure);
                //objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTransactionalCoaData]", CommandType.StoredProcedure);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Account_GetFinancialAccountCode]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        if (objDbDataReader["COAId"] != null)
                        {
                            objCOA.COAId = objDbDataReader["COAId"] != DBNull.Value ? Convert.ToInt32(objDbDataReader["COAId"]) : 0;
                            objCOA.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                            objCOA.GroupId = objDbDataReader["GroupId"] != DBNull.Value ? Convert.ToInt32(objDbDataReader["GroupId"]) : 0;
                            objCOA.IsTransactional = objDbDataReader["IsTransactional"] != DBNull.Value ? Convert.ToBoolean(objDbDataReader["IsTransactional"]) : false;
                            objCOA.IsSubCode = objDbDataReader["IsSubCode"] != DBNull.Value ?  Convert.ToBoolean(objDbDataReader["IsSubCode"]): false;
                            objCOA.COACategoryId = objDbDataReader["COACategoryId"] != DBNull.Value ? Convert.ToInt32(objDbDataReader["COACategoryId"]): 0;
                            objAccountsList.Add(objCOA);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountsList;
        }

        public CommonResult SaveFinancialTransactionsInfo(FinancialTransactionsModel _objFinancialTransaction)
        {
            CommonResult oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("TransactionId", _objFinancialTransaction.TransactionId);
            objDbCommand.AddInParameter("VoucherNo", _objFinancialTransaction.VoucherNo);
            objDbCommand.AddInParameter("VoucherType", _objFinancialTransaction.VoucherType);
            objDbCommand.AddInParameter("VoucherCode", _objFinancialTransaction.VoucherCode);
            objDbCommand.AddInParameter("TransactionDate", _objFinancialTransaction.TransactionDate);
            objDbCommand.AddInParameter("EntryType", _objFinancialTransaction.EntryType);
            objDbCommand.AddInParameter("Particular", _objFinancialTransaction.Particular);
            objDbCommand.AddInParameter("Debit", _objFinancialTransaction.Debit);
            objDbCommand.AddInParameter("Credit", _objFinancialTransaction.Credit);
            objDbCommand.AddInParameter("IsAdvancePayment", _objFinancialTransaction.IsAdvancePayment);
            objDbCommand.AddInParameter("IsApproved", _objFinancialTransaction.IsApproved);
            objDbCommand.AddInParameter("IsActive", _objFinancialTransaction.IsActive);
            objDbCommand.AddInParameter("IsChequePayment", _objFinancialTransaction.IsChequePayment);
            objDbCommand.AddInParameter("IsCleared", _objFinancialTransaction.IsCleared);
            objDbCommand.AddInParameter("ChequeNo", _objFinancialTransaction.ChequeNo);
            objDbCommand.AddInParameter("IssueDate", _objFinancialTransaction.IssueDate);
            objDbCommand.AddInParameter("BankName", _objFinancialTransaction.BankName);
            objDbCommand.AddInParameter("BankAccountNo", _objFinancialTransaction.BankAccountNo);
            objDbCommand.AddInParameter("BranchName", _objFinancialTransaction.BranchName);
            objDbCommand.AddInParameter("ClientCode", _objFinancialTransaction.ClientCode);
            objDbCommand.AddInParameter("ClientName", _objFinancialTransaction.ClientName);
            objDbCommand.AddInParameter("SupplierCode", _objFinancialTransaction.SupplierCode);
            objDbCommand.AddInParameter("SupplierName", _objFinancialTransaction.SupplierName);
            objDbCommand.AddInParameter("COAId", _objFinancialTransaction.COAId);
            objDbCommand.AddInParameter("CategoryId", _objFinancialTransaction.CategoryId);
            objDbCommand.AddInParameter("CategoryName", _objFinancialTransaction.CategoryName);
            objDbCommand.AddInParameter("AccountCode", _objFinancialTransaction.AccountCode);
            objDbCommand.AddInParameter("AccountName", _objFinancialTransaction.AccountName);
            objDbCommand.AddInParameter("GroupId", _objFinancialTransaction.GroupId);
            objDbCommand.AddInParameter("GroupName", _objFinancialTransaction.GroupName);
            objDbCommand.AddInParameter("IsRejected", _objFinancialTransaction.IsRejected);
            objDbCommand.AddInParameter("IsReversed", _objFinancialTransaction.IsReversed);
            objDbCommand.AddInParameter("CreatedBy", _objFinancialTransaction.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", _objFinancialTransaction.CreatedDate);
            objDbCommand.AddInParameter("CompanyId", _objFinancialTransaction.CompanyId);
            objDbCommand.AddInParameter("BranchId", _objFinancialTransaction.BranchId);
            objDbCommand.AddInParameter("Remarks", _objFinancialTransaction.Remarks);
            objDbCommand.AddInParameter("EntryPosition", _objFinancialTransaction.EntryPosition);
            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FinancialTransaction_SaveInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    oCommonResult.Status = false;
                    oCommonResult.Message = "Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }


        public string GetTransactionId()
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;

            objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "FinancialTransaction");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();
            return transactionId;
        }
        public long GetVoucherNo()
        {
            var voucherNo = 0;
            DbDataReader objDbDataReader = null;

            objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            objDbCommand.AddInParameter("FieldName", "VoucherNo");
            objDbCommand.AddInParameter("TableName", "FinancialTransaction");
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxId]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    voucherNo = Convert.ToInt32(objDbDataReader["Id"].ToString());
                }
            }
            objDbDataReader.Close();
            return voucherNo;
        }

        public List<FinancialTransactionsModel> GetNonReverseEntryFinancialTransactionslist(FinancialTransactionsModel _objFTModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objAllFinancialTransactions = new List<FinancialTransactionsModel>();
            FinancialTransactionsModel _objFinancialTransactions;
            try
            {
                objDbCommand.AddInParameter("CompanyId", _objFTModel.CompanyId);
                objDbCommand.AddInParameter("TransactionDate", _objFTModel.TransactionDate);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[FinancialTransactions_GetNonReverseEntryInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objFinancialTransactions = new FinancialTransactionsModel();
                        _objFinancialTransactions.Id = Convert.ToInt32(objDbDataReader["Id"]);
                        _objFinancialTransactions.TransactionId = Convert.ToString(objDbDataReader["TransactionId"]);
                        _objFinancialTransactions.VoucherNo = Convert.ToInt64(objDbDataReader["VoucherNo"]);
                        _objFinancialTransactions.VoucherType = Convert.ToString(objDbDataReader["VoucherType"]);
                        _objFinancialTransactions.VoucherCode = Convert.ToString(objDbDataReader["VoucherCode"]);
                        _objFinancialTransactions.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"]);
                        _objFinancialTransactions.EntryType = Convert.ToString(objDbDataReader["EntryType"]);
                        _objFinancialTransactions.Particular = Convert.ToString(objDbDataReader["Particular"]);
                        _objFinancialTransactions.Debit = Convert.ToInt32(objDbDataReader["Debit"]);
                        _objFinancialTransactions.Credit = Convert.ToInt32(objDbDataReader["Credit"]);
                        _objFinancialTransactions.IsAdvancePayment = Convert.ToBoolean(objDbDataReader["IsAdvancePayment"]);
                        _objFinancialTransactions.IsChequePayment = Convert.ToBoolean(objDbDataReader["IsChequePayment"]);
                        _objFinancialTransactions.IsApproved = Convert.ToBoolean(objDbDataReader["IsApproved"]);
                        _objFinancialTransactions.IsCleared = Convert.ToBoolean(objDbDataReader["IsCleared"]);
                        _objFinancialTransactions.ChequeNo = Convert.ToString(objDbDataReader["ChequeNo"]);
                        _objFinancialTransactions.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        _objFinancialTransactions.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        _objFinancialTransactions.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        _objFinancialTransactions.ClientName = Convert.ToString(objDbDataReader["ClientName"]);
                        _objFinancialTransactions.ClientCode = Convert.ToString(objDbDataReader["ClientCode"]);
                        _objFinancialTransactions.SupplierName = Convert.ToString(objDbDataReader["SupplierName"]);
                        _objFinancialTransactions.SupplierCode = Convert.ToString(objDbDataReader["SupplierCode"]);
                        _objFinancialTransactions.COAId = Convert.ToInt32(objDbDataReader["COAId"]);
                        _objFinancialTransactions.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                        _objFinancialTransactions.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        _objFinancialTransactions.CategoryId = Convert.ToInt32(objDbDataReader["CategoryId"]);
                        _objFinancialTransactions.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        _objFinancialTransactions.GroupId = Convert.ToInt16(objDbDataReader["GroupId"]);
                        _objFinancialTransactions.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        _objFinancialTransactions.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                        _objFinancialTransactions.CreatedBy = Convert.ToInt16(objDbDataReader["CreatedBy"]);
                        _objFinancialTransactions.CreatedDate = Convert.ToDateTime(objDbDataReader["CreatedDate"]);
                        _objFinancialTransactions.Remarks = Convert.ToString(objDbDataReader["Remarks"]);
                        objAllFinancialTransactions.Add(_objFinancialTransactions);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return objAllFinancialTransactions;
        }

        public string ReverseEntryFinancialTransaction(string transactionId, string voucherType, int createdBy)
        {
            string transactionStatus;

            objDataAccess = DataAccess.NewDataAccess();

            try
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("transactionId", transactionId);
                objDbCommand.AddInParameter("voucherType", voucherType);
                objDbCommand.AddInParameter("createdBy", createdBy);
                var noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FinancialTransactions_ReverseEntry]",
                    CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    transactionStatus = "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    transactionStatus = "failed";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return transactionStatus;
        }

        #region Banalce sheet config
        public CommonResult SaveBalanceSheetInfo(COA objCoa)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
           
            objDbCommand.AddInParameter("CategoryId", objCoa.CategoryName);
            objDbCommand.AddInParameter("GroupId", objCoa.GroupId);
            objDbCommand.AddInParameter("BalanceSheetName", objCoa.BalanceSheetName);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FT_SaveBalanceSheetInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Save Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return oCommonResult;

        }

        public CommonResult UpdateBalanceSheet(COA objCoa)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("BalanceSheetId", objCoa.BalanceSheetId);
            objDbCommand.AddInParameter("CategoryId", objCoa.CategoryName);
            objDbCommand.AddInParameter("GroupId", objCoa.GroupId);
            objDbCommand.AddInParameter("BalanceSheetName", objCoa.BalanceSheetName);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[FT_UpdateBalanceSheetInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Update Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return oCommonResult;

        }

        public List<COA> GetBalanceSheetInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objAllFinancialTransactions = new List<FinancialTransactionsModel>();
            List<COA> listOfBalanceSheet=new List<COA>();
            COA objCoa;
            try
            {
               
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[FT_GetBalanceSheetInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCoa=new COA();
                        objCoa.BalanceSheetId = Convert.ToInt32(objDbDataReader["BalanceSheetId"]);
                        objCoa.COACategoryId = Convert.ToInt32(objDbDataReader["CategoryId"]);
                        objCoa.CategoryName = objDbDataReader["CategoryName"].ToString();
                        objCoa.GroupId = Convert.ToInt32(objDbDataReader["GroupId"]);
                        objCoa.GroupName = objDbDataReader["GroupName"].ToString();
                        objCoa.BalanceSheetName = objDbDataReader["BalanceSheetName"].ToString();
                        listOfBalanceSheet.Add(objCoa);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfBalanceSheet;
        }


        #endregion

        public PaymentDetailsModel GetPaymentDetailsByVoucherNo(string VoucherNo)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            PaymentDetailsModel _objPaymentDetailsModel = null; 
            try
            {
                objDbCommand.AddInParameter("VoucherNo", VoucherNo);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[GetPaymentInfoByVoucherNo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objPaymentDetailsModel = new PaymentDetailsModel();
                        _objPaymentDetailsModel.TransactionId = Convert.ToString(objDbDataReader["TransactionId"]);
                        _objPaymentDetailsModel.VoucherNo = Convert.ToInt64(objDbDataReader["VoucherNo"]);
                        _objPaymentDetailsModel.VoucherType = Convert.ToString(objDbDataReader["VoucherType"]);
                        _objPaymentDetailsModel.VoucherCode = Convert.ToString(objDbDataReader["VoucherCode"]);
                        _objPaymentDetailsModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"]);
                        _objPaymentDetailsModel.DebitReason = Convert.ToString(objDbDataReader["DebitReason"]);
                        _objPaymentDetailsModel.CreditReason = Convert.ToString(objDbDataReader["CreditReason"]);
                        _objPaymentDetailsModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"]);
                        _objPaymentDetailsModel.IsChequePayment = Convert.ToBoolean(objDbDataReader["IsChequePayment"]);
                        _objPaymentDetailsModel.ChequeNo = Convert.ToString(objDbDataReader["ChequeNo"]);
                        _objPaymentDetailsModel.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        _objPaymentDetailsModel.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        _objPaymentDetailsModel.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        _objPaymentDetailsModel.Remarks = Convert.ToString(objDbDataReader["Remarks"]);
                        _objPaymentDetailsModel.PayTo = Convert.ToString(objDbDataReader["PayTo"]);
                        _objPaymentDetailsModel.ReceiveFrom = Convert.ToString(objDbDataReader["ReceiveFrom"]);
                        _objPaymentDetailsModel.ChequeIssueDate = Convert.ToDateTime(objDbDataReader["ChequeIssueDate"]);
                        _objPaymentDetailsModel.AccountToBankName = Convert.ToString(objDbDataReader["AccountToBankName"]);
                        _objPaymentDetailsModel.AccountToBankAccountNo = Convert.ToString(objDbDataReader["AccountToBankAccountNo"]);
                        _objPaymentDetailsModel.AccountToBranchName = Convert.ToString(objDbDataReader["AccountToBranchName"]);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return _objPaymentDetailsModel;
        }
    }
}