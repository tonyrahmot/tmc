//json to date converter
function FormateDate(jsonDate) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

//date picker
$('.datepicker').datepicker({
    dateFormat: "dd/M/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+0"
});

$(document).ready(function () {
    $('.js-example-basic-single').select2();
    $('#fileDisplay').hide();
    document.getElementById('showMessage').innerHTML = '';
    //Deviation INFO AREA
    $('#UpdateCustomerData').hide();
    GetAddtionalInfoData();

    function GetAddtionalInfoData() {
        var LoanId = $('#RefId').val();
        var data = { loanId: LoanId };
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/GetAddtionalInfoData",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (data) {
                bindAddtionalInfoData(data);
            }
        });
    }

    function bindAddtionalInfoData(data) {
        $('#hdfLoanIdForAddtioanlInfo').val(data.data[0].LoanId);
        $('#hdfCustomerDeviationId').val(data.data[0].Id);
        $('#CustomerDetail').val(data.data[0].CustomerDetail);
        $('#GuarantorsDetail').val(data.data[0].GuarantorsDetail);
        $('#Deviation').val(data.data[0].Deviation);
        $('#ApproverComment').val(data.data[0].ApproverComment);

        $('#submitCustomerData').show();
        $('#CustomerDatar_Clear').show();
    }

    //Addtional Info Data table end

    $('#submitCustomerData').click(function () {

        var isValid = CheckValidation();
        if (isValid) {
            var data = Bindata();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/SaveLoanCustomerDetails",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });

    function Bindata() {

        var data = {
            Id: $('#hdfCustomerDeviationId').val(),
            LoanId: $('#RefId').val(),
            CustomerDetail: $('#CustomerDetail').val(),
            GuarantorsDetail: $('#GuarantorsDetail').val(),
            Deviation: $('#Deviation').val(),
            ApproverComment: $('#ApproverComment').val()
        }
        return data;
    }

    function ClearAddtionalInfoData() {
        //$('#hdfCustomerDeviationId').val('');
        $('#CustomerDetail').val('');
        $('#GuarantorsDetail').val('');
        $('#Deviation').val('');
        $('#ApproverComment').val('');
        $('#submitCustomerData').show();
        $('#UpdateCustomerData').hide();
        $('#deleteCustomerData').hide();
        document.getElementById('showMessage').innerHTML = '';
    }

    $('#CustomerDatar_Clear').click(function () {
        ClearAddtionalInfoData();
        $('#submitCustomerData').show();
        $('#UpdateCustomerData').hide();
        $('#deleteCustomerData').hide();
        $('#CustomerDatar_Clear').show();
    });


    function CheckValidation() {
        var isValid = true;

        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }


        if ($('#CustomerDetail').val().trim() === '' && $('#Deviation').val().trim() === '' && $('#ApproverComment').val().trim() === '' && $('#ApproverComment').val().trim() === '') {
            isValid = false;
            $('#CustomerDetail').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#CustomerDetail').parent().prev().find('span').css('visibility', 'hidden');
        }

        //if ($('#CustomerDetail').val().trim() === '') {
        //    isValid = false;
        //    $('#CustomerDetail').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#CustomerDetail').parent().prev().find('span').css('visibility', 'hidden');
        //}
        //if ($('#GuarantorsDetail').val().trim() === '') {
        //    isValid = false;
        //    $('#GuarantorsDetail').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#GuarantorsDetail').parent().prev().find('span').css('visibility', 'hidden');
        //}
        //if ($('#Deviation').val().trim() === '') {
        //    isValid = false;
        //    $('#Deviation').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#Deviation').parent().prev().find('span').css('visibility', 'hidden');
        //}
        //if ($('#ApproverComment').val().trim() === '') {
        //    isValid = false;
        //    $('#ApproverComment').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#ApproverComment').parent().prev().find('span').css('visibility', 'hidden');
        //}

        return isValid;
    }



    $('#clear').click(function () {
        window.location.reload();
    });


    //=====================ADDTIONAL INFO AREA END=================


    //----------------------------------------


    // Load Function function
    var oScanDocumentTable;
    ScanDocumentTable();
    function ScanDocumentTable() {
        oScanDocumentTable = $('#ScanDocumentTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetScanDocumnetInfo",
                "type": "get",
                "data": {
                    "LoanId": $('#RefId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "className": "dt-center"
                },
                {
                    "data": "LoanId",
                    "targets": 0
                },
                {
                    "data": "DocumentName",
                    "targets": 0
                }
                //{
                //    "data": "ImagePath",
                //    "render": function (data, type, row) {
                //        return '<img src="' + data + '" width="40px" height="40px"/>';
                //    }
                //}
            ],

            // "order": [[0, "desc"]],
            ordering: false,
            //searching: false,
            sorting: false,
            paging: false,
            "bInfo": false
        });
        $('#ScanDocumentTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oScanDocumentTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataForScanDocumentTable(oScanDocumentTable.row(this).data());
        });
    }

    oScanDocumentTable.on('order.dt search.dt', function () {
        oScanDocumentTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    function bindDataForScanDocumentTable(data) {
        $('#hiddenScanDocumnetId').val(data.Id);
        $('#LoanId').val(data.LoanId);
        $('#imageDisplay').attr('src', '');
        $('#fileDisplay').attr('src', '');
        $('#DocumentName').val(data.DocumentName);
        if (data.ImagePath.indexOf('.jpg') !== -1 || data.ImagePath.indexOf('.jpeg') !== -1 || data.ImagePath.indexOf('.png') !== -1 || data.ImagePath.indexOf('.gif') !== -1) {
            $('#fileDisplay').hide();
            $('#imageDisplay').show();
            $('#imageDisplay').attr('src', data.ImagePath);
        } else {
            $('#imageDisplay').hide();
            $('#fileDisplay').show();
            var imgPath = data.ImagePath;
            imgPath = imgPath.split(/\//g).join("/");
            $('#fileDisplay').attr('src', '/ViewerJS/#../' + imgPath);
        }
        $('#hiddenScanDocumentImagePath').val(data.OldImagePath),
        $('#saveScanDocument').hide();
        $('#updateScanDocument').show();
        $('#DeleteScanDocument').show();
    }

    //scan document save area

    $("#fileUpload").change(function () {
        if (this.files && this.files[0]) {
            $('#fileDisplay').attr('src', '');
            $('#fileDisplay').hide();
            $('#imageDisplay').attr('src', '');
            $('#imageDisplay').show();
            var reader = new FileReader();
            var file = this.files[0];
            var fileType = file["type"];
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if ($.inArray(fileType, validImageTypes) < 0) {
            } else {
                reader.onload = function (e) {
                    $('#imageDisplay').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    //imgage save

    $('#saveScanDocument').click(function () {
        var isAllDataValid = CheckValidationForScanDocument();
        if (isAllDataValid) {
            var fd = new FormData();
            fd.append("LoanId", $('#RefId').val());
            fd.append("DocumentName", $('#DocumentName').val());
            var file = document.getElementById('fileUpload').files[0];
            if (file != null) {
                fd.append("ImagePath", 'Cersi');
                fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
            } else {
                fd.append("ImagePath", 'arya');
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/SaveScanDocumentInfo",
                type: "POST",
                data: fd,
                dataType: "JSON",
                contentType: false,
                processData: false,
                success: function (d) {
                    if (d.status === true) {
                        //document.getElementById('showMessage').innerHTML = 'Save Sucessful';
                        if (d.image !== 'arya') {
                            saveImage(d);
                        }
                    } else {
                        document.getElementById('showMessage').innerHTML = 'Error';
                    }


                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';


                }
            });
        }
    });


    function CheckValidationForScanDocument() {
        var isValid = true;

        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#DocumentName').val().trim() === '') {
            isValid = false;
            $('#DocumentName').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#DocumentName').parent().prev().find('span').css('visibility', 'hidden');
        }
        var file = document.getElementById('fileUpload').files[0];
        if (file == null) {
            isValid = false;
            alert('Select a file first.');
        }
        else {
            isValid = true;
        }
        return isValid;
    }


    function saveImage(d) {
        var fd = new FormData();
        fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
        fd.append("LoanId", d.Id);
        fd.append("ImagePath", d.image);
        $('#hiddenLoanId').val(d.Id);
        $('#hiddenScanDocumentImagePath').val(d.image);
        //$('#hiddenCustomerName').val(obj.name);
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/SaveScanDocumentImageFile",
            type: "POST",
            data: fd,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function (obj) {
                if (obj.status == true) {
                    $('#hiddenScanDocumentImagePath').val(obj.image);
                    oScanDocumentTable.ajax.reload();
                    ClearAllData();
                    document.getElementById('showMessage').innerHTML = 'Save Sucessful';
                } else {
                    document.getElementById('showMessage').innerHTML = 'Upload failed';
                }
            },
            error: function () {
                document.getElementById('showMessage').innerHTML = 'Error. plz try again';
            }
        });
    }

    function ClearAllData() {
        $('#DocumentName').val('');
        $('#LoanId').val('');
        $('#fileUpload').val('');
        $('#imageDisplay').attr('src', '');
        $('#fileDisplay').attr('src', '');
        $('#fileDisplay').hide();
        document.getElementById('showMessage').innerHTML = '';
        $('#updateScanDocument').hide();
        $('#DeleteScanDocument').hide();
    }
    $('#ScanDocument_Clear').click(function () {
        ClearAllData();
        $('#updateScanDocument').hide();
        $('#DeleteScanDocument').hide();
        $('#saveScanDocument').show();
    });

    $('#updateScanDocument').hide();
    $('#DeleteScanDocument').hide();




    //update scan document area
    $('#updateScanDocument').click(function () {
        var isAllDataValid = CheckValidationForScanDocument();
        if (isAllDataValid) {
            var fd = new FormData();
            fd.append("Id", $('#hiddenScanDocumnetId').val());
            fd.append("LoanId", $('#RefId').val());
            fd.append("DocumentName", $('#DocumentName').val());
            fd.append("OldImagePath", $('#hiddenScanDocumentImagePath').val());
            var file = document.getElementById('fileUpload').files[0];
            var newfile = false;
            if (file != null) {
                newfile = true;
                fd.append("ImagePath", 'Cersi');
                fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
            } else {
                fd.append("ImagePath", $('#hiddenScanDocumentImagePath').val());
            }
            if (newfile) {
                $.ajax({
                    url: window.applicationBaseUrl + "Loan/Loan/UpdateScanDocumentInfo",
                    type: "POST",
                    data: fd,
                    dataType: "JSON",
                    contentType: false,
                    processData: false,
                    success: function (d) {
                        if (d.status === true) {
                            //document.getElementById('showMessage').innerHTML = 'Save Sucessful';
                            oScanDocumentTable.ajax.reload();
                            var fd = new FormData();
                            fd.append("Imagefile", document.getElementById('fileUpload').files[0]);
                            fd.append("LoanId", d.loanId);
                            fd.append("ImagePath", d.image);
                            fd.append("Id", d.scanId);
                            $('#hiddenScanDocumnetId').val(d.scanId);
                            $('#hiddenScanDocumentImagePath').val(d.image);
                            $('#hiddenLoanId').val(d.loanId);

                            $.ajax({
                                url: window.applicationBaseUrl + "Loan/Loan/SaveScanDocumentImageFile",
                                type: "POST",
                                data: fd,
                                dataType: "JSON",
                                contentType: false,
                                processData: false,
                                success: function (obj) {
                                    if (obj.status == true) {
                                        $('#hiddenScanDocumentImagePath').val(obj.ImagePath);
                                        oScanDocumentTable.ajax.reload();
                                        ClearAllData();
                                        document.getElementById('showMessage').innerHTML = 'Update Sucessful';
                                    } else {
                                        document.getElementById('showMessage').innerHTML = 'Upload Failed';
                                    }


                                },
                                error: function () {
                                    document.getElementById('showMessage').innerHTML = 'Error. plz try again';

                                }
                            });
                        } else {
                            document.getElementById('showMessage').innerHTML = 'Error';
                            oScanDocumentTable.ajax.reload();
                        }


                    },
                    error: function () {
                        document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                        oScanDocumentTable.ajax.reload();
                    }
                });
            }
        }
    });

    $('#DeleteScanDocument').click(function () {
        var Id = $('#hiddenScanDocumnetId').val();
        var json = {
            Id: Id
        };
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/DeleteScanDocumentInfo",
            type: "POST",
            data: JSON.stringify(json),
            dataType: "JSON",
            contentType: "application/json",
            success: function () {
                oScanDocumentTable.ajax.reload();
                ClearAllData();
                document.getElementById('showMessage').innerHTML = 'Delete Sucessfully.';
            },
            error: function () {
                document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                oScanDocumentTable.ajax.reload();
            }
        });
    });
    //=============== end scan document area===============

    //=======Signatory Area======

    $('#updateSignatories').hide();
    $('#deleteSignatories').hide();


    LoadEmployee();
    function LoadEmployee() {
        $("#EmpCode").empty();
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "Loan/Loan/GetBranchWiseEmployee",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#EmpCode").append('<option value="0">--Select Employee--</option>');
                $.each(data, function (key, value) {
                    $("#EmpCode").append('<option value=' + value.EmpCode + '>' + value.EmployeeName + '</option>');
                });
            }
        });
    }
    // Get Designation
    $("#EmpCode").change(function () {
        var EmpCode = $("#EmpCode").val();
        var json = {
            EmpCode: EmpCode
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetDesignationByEmpCode",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {

                document.getElementById('DesignationName').innerHTML = data.DesignationName;
                document.getElementById('FunctionalPosition').innerHTML = data.FunctionalPosition;
                //exitsEmployeeSignatoriesTable();
            }
        });
    });


    function exitsEmployeeSignatoriesTable(handleData) {
        var empCode = $("#EmpCode").val();
        var loanId = $('#RefId').val();
        var json = { empCode: empCode, loanId: loanId };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/ExitsEmployeeSignatoriesTable",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                //$("#saveSignatories").removeAttr('disabled');
                //$("#updateSignatories").removeAttr('disabled');
                if (data === false) {
                    //$("#saveSignatories").attr('disabled', 'disabled');
                    //$("#updateSignatories").attr('disabled', 'disabled');
                    alert('This Employee already Exists!');
                }
                handleData(data);
            }
        });
    }


    $('#saveSignatories').click(function () {
        var isValid = CheckSignatoryDataValidation();
        if (isValid) {
            var data = BindSignatoryData();
            var myPromise = new Promise(function (resolve, reject) {
                exitsEmployeeSignatoriesTable(function (output) {
                    if (output === true) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
            myPromise.then(function () {
                $.ajax({
                    url: window.applicationBaseUrl + "Loan/Loan/SaveSignatoryData",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.oCommonResult.Status === true) {
                            oSignatoryDataTable.ajax.reload();
                            document.getElementById('showMessage').style.color = "green";
                            ClearSignatoryData();
                        }
                        document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                    },
                    error: function () {
                        document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    }
                });
            }).catch(function () {
                ClearSignatoryData();
            });

        }
    });

    $('#deleteSignatories').click(function () {

        var isValid = CheckSignatoryDataValidation();
        if (isValid) {
            var data = BindSignatoryData();
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/DeleteSignatoryData",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oSignatoryDataTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        ClearSignatoryData();
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    function CheckSignatoryDataValidation() {
        var isValid = true;

        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }

        //if ($('#FunctionalPosition').val().trim() === '') {
        //    isValid = false;
        //    $('#FunctionalPosition').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#FunctionalPosition').parent().prev().find('span').css('visibility', 'hidden');
        //}

        if ($('#EmpCode').val().trim() === '0') {
            isValid = false;
            $('#EmpCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }
    function BindSignatoryData() {

        var data = {
            LoanId: $('#RefId').val(),
            FunctionalPosition: document.getElementById('FunctionalPosition').innerHTML,
            EmpCode: $('#EmpCode option:selected').val()
        }
        return data;
    }
    function ClearSignatoryData() {
        //$('#FunctionalPosition').val('');
        $('#EmpCode').val('0').trigger('change');
        document.getElementById('DesignationName').innerHTML = '';
        document.getElementById('FunctionalPosition').innerHTML = '';
        $('#saveSignatories').show();
        $('#updateSignatories').hide();
        $('#deleteSignatories').hide();
    }
    $('#Signatories_Clear').click(function () {
        ClearSignatoryData();
        $('#updateSignatories').hide();
        $('#deleteSignatories').hide();
        $('#saveSignatories').show();
        $("#saveSignatories").removeAttr('disabled');
        $("#updateSignatories").removeAttr('disabled');
    });



    //signatory data table add
    var oSignatoryDataTable;
    LoadSignatoryDataTable();
    function LoadSignatoryDataTable() {
        oSignatoryDataTable = $('#SignatoryDataTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetSignatoryDataForDataTable",
                "type": "get",
                "data": {
                    "LoanId": $('#RefId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": "LoanId",
                    "targets": 0
                },
                {
                    "data": null,
                    "width": "10px",
                    "className": "dt-center"
                },
                {
                    "data": "FunctionalPosition",
                    "targets": 0
                },
                {
                    "data": "EmployeeName",
                    "targets": 0
                },
                {
                    "data": "DesignationName",
                    "targets": 0
                }
            ],
            //"order": [[0, "desc"]],
            ordering: false,
            //searching: false,
            sorting: false,
            paging: false,
            "bInfo": false
        });
        $('#SignatoryDataTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                oSignatoryDataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindSignatoryData(oSignatoryDataTable.row(this).data());
        });
    }

    oSignatoryDataTable.on('order.dt search.dt', function () {
        oSignatoryDataTable.column(1, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    function bindSignatoryData(data) {
        $('#hdfSignatoryId').val(data.Id);
        $('#hdfLoanIdForSignatory').val(
            data.LoanId);
        $('#EmpCode').val(data.EmpCode).trigger('change');
        //$('#EmpCode').val(data.EmpCode);
        //$('#FunctionalPosition').val(data.FunctionalPosition);
        document.getElementById('FunctionalPosition').innerHTML = data.FunctionalPosition;
        document.getElementById('DesignationName').innerHTML = data.DesignationName;

        $('#saveSignatories').hide();
        $('#updateSignatories').show();
        $('#deleteSignatories').show();
        $('#Signatories_Clear').show();
    }

    //update signatory


    $('#updateSignatories').click(function () {

        var isValid = CheckSignatoryDataValidationForUpdate();
        if (isValid) {
            var data = BindSignatoryDataForUpdate();
            var myPromise = new Promise(function (resolve, reject) {
                exitsEmployeeSignatoriesTable(function (output) {
                    if (output === true) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
            myPromise.then(function () {
                $.ajax({
                    url: window.applicationBaseUrl + "Loan/Loan/UpdateSignatoryData",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: "JSON",
                    contentType: "application/json",
                    success: function (d) {
                        if (d.oCommonResult.Status == true) {
                            oSignatoryDataTable.ajax.reload();
                            document.getElementById('showMessage').style.color = "green";
                            ClearSignatoryData();
                        }
                        document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                    },
                    error: function () {
                        document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    }
                });
            });
        }
    });


    function CheckSignatoryDataValidationForUpdate() {
        var isValid = true;


        if ($('#hdfSignatoryId').val().trim() === '') {
            isValid = false;
            $('#hdfSignatoryId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#hdfSignatoryId').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }

        //if ($('#FunctionalPosition').val().trim() === '') {
        //    isValid = false;
        //    $('#FunctionalPosition').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#FunctionalPosition').parent().prev().find('span').css('visibility', 'hidden');
        //}

        if ($('#EmpCode').val().trim() === '0') {
            isValid = false;
            $('#EmpCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }
    function BindSignatoryDataForUpdate() {

        var data = {
            Id: $('#hdfSignatoryId').val(),
            LoanId: $('#RefId').val(),
            FunctionalPosition: document.getElementById('FunctionalPosition').innerHTML,
            EmpCode: $('#EmpCode option:selected').val()
        }
        return data;
    }

    //=======Signatory Area END======


});