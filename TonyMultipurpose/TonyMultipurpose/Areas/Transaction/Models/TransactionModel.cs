﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Transaction.Models
{
    public class TransactionModel : CommonModel
    {
        [Key]
        public long Id { get; set; }

        public string TransactionId { get; set; }
        public string Username { get; set; }

        [Display(Name = "Date")]
        public DateTime TransactionDate { get; set; }


        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }

        [Display(Name = "A/C No")]
        public string AccountNumber { get; set; }

        [Display(Name = "Interest Rate")]
        public decimal InterestRate { get; set; }

        public string CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public string FatherName { get; set; }

        public short BranchId { get; set; }
        public string BranchName { get; set; }
        public string CompanyName { get; set; }

        public int AccountSetupId { get; set; }

        [Display(Name = "Account Type Name")]
        public string AccountTypeName { get; set; }

        [Display(Name = "Debit")]
        public decimal Debit { get; set; }

        [Display(Name = "Credit")]
        public decimal Credit { get; set; }

        [Display(Name = "Balance")]
        public decimal? Balance { get; set; }

        [Display(Name = "Accrued Balance")]
        public decimal? AccruedBalance { get; set; }

        [Display(Name = "Particular")]
        public string Particular { get; set; }

        [Required]
        public decimal? Amount { get; set; }

        public string CustomerImage { get; set; }

        public DateTime OpeningDate { get; set; }

        public string RejectReason { get; set; }

        [Display(Name = "Cash")]
        public bool IsCashEntry { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Cheque No")]
        public string ChequeNo { get; set; }

        [Display(Name = "Bank Accoun No")]
        public string BankAccountNo { get; set; }
        public int COAId { get; set; }

        public string VoucherNo { get; set; }
    }
}