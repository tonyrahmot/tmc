﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.BLL;

namespace TonyMultipurpose.Controllers
{
    
    public class UserMenuMappingController : Controller
    {
        [AuthenticationFilter]
        // GET: UserMenuMapping
        [HttpGet]
        public ActionResult Save(int BranchId = 0, int userDetailId = 0, int roleId = 0)
        {           

            BranchBLL objBranchBll = new BranchBLL();
            ViewBag.BranchInfo = objBranchBll.GetAllBranchInfo();

            UserDetailBLL objUserDetailBll = new UserDetailBLL();
            ViewBag.UserInfo = objUserDetailBll.GetAllUserInfo();

            RoleManagementBLL objRoleManagementBll = new RoleManagementBLL();
            ViewBag.RoleInfo = objRoleManagementBll.GetRoleListForDropDownForSave().Where(a => a.RoleId == roleId);


            NavigationBLL objNavigationBll = new NavigationBLL();
            var menu = objNavigationBll.GetAllMenuForUserMenuMapping(userDetailId, roleId);

            return View(menu);
        }


        [HttpPost]
        public JsonResult Save(IEnumerable<int> menuId, int roleId, int userId)
        {
            bool status = false;

            RoleManagementBLL objRoleManagementBll = new RoleManagementBLL();
            if (ModelState.IsValid)
            {
                objRoleManagementBll.ResetUserMenuMapping(roleId, userId);
                if (menuId != null)
                {
                    foreach (int item in menuId)
                    {
                        objRoleManagementBll.CreateUserMenuMapping(item, roleId, userId);
                    }
                }
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetUserByBranchId(int branchId)
        {
            UserDetailBLL _objUserDetailBLL = new UserDetailBLL();
            var u = _objUserDetailBLL.GetAllUserInfo().Where(a => a.BranchId == branchId).ToList();
            return Json(u, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetRoleByUserId(int userDetailId)
        {
            RoleManagementBLL objRoleManagementBll = new RoleManagementBLL();
            var u = objRoleManagementBll.GetRoleByUserId(userDetailId).ToList();
            return Json(u, JsonRequestBehavior.AllowGet);
        }
    }
}