﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
   [AuthenticationFilter]
    //--ata
    public class SettlementController : Controller
    {
        LoanModel objLoan = new LoanModel();
        LoanBLL loanBll = new LoanBLL();
        SattlementBLL objSattlementBll=new SattlementBLL();
        TermLoanBLL objTermLoanBLL = new TermLoanBLL();
        private CommonResult oCommonResult;
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AutoCompleteLoanId(string term)
        {
            var data = objSattlementBll.AutoCompleteLoanId(term);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllSattlementInfo()
        {
            var allSattlementData = objSattlementBll.GetAllSattlementInfo();
            return Json(new { data = allSattlementData }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUnApprovedSettlementList()
        {
            var unapprovedSattlementData =  objSattlementBll.GetUnApprovedSettlementList();
            return Json(new { data = unapprovedSattlementData }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerInfoByLoanId(string LoanNo)
        {           
            objLoan.IsApproved = true;
            objLoan.IsRejected = false;
            var loanInfo = loanBll.GetAllLoanInfo(objLoan).Where(x => x.LoanNo == LoanNo).FirstOrDefault();
            var customerInfo = objSattlementBll.GetCustomerInfoByLoanId(LoanNo);
            var info = objTermLoanBLL.GetTermLoanInfoByLoanId().Where(a => a.LoanNo == LoanNo)
                .OrderByDescending(x => x.Id)
                .Take(1)
                .FirstOrDefault();
            var p = objTermLoanBLL.GetInstallmentNoAndDateById(LoanNo);
            return Json(new { customerInfo = customerInfo,loanInfo = loanInfo,info = info,p = p}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSettlementType()
        {
            var data = objSattlementBll.GetSettlementType();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Save(SettlementModel objSettlementModel)
        {
            oCommonResult = new CommonResult();
            if (ModelState.IsValid)
            {
                oCommonResult = objSattlementBll.SaveSettlementInfo(objSettlementModel);
              
            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            SettlementModel settlementList = objSattlementBll.GetSettlementInfoById(id);
            return View(settlementList);
        }

        [HttpPost]
        public ActionResult Edit(SettlementModel settlementModel)
        {
            
            oCommonResult = new CommonResult();
            if (ModelState.IsValid)
            {
                oCommonResult= objSattlementBll.UpdateSettlementInfo(settlementModel);
               
            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult Delete(string SettlementId)
        {
            oCommonResult = new CommonResult();
            
            oCommonResult = objSattlementBll.DeleteSettlementInfo(SettlementId);

           
            return new JsonResult { Data = new { oCommonResult } };
        }


        #region Approval
        public ActionResult ApprovalIndex()
        {
            List<SettlementViewModel> UnApprovedSettlementList = objSattlementBll.GetUnApprovedSettlementList();
            return View(UnApprovedSettlementList);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("SettlementId");
            if (ch != null)
            {
                objSattlementBll.ApproveSettlement(ch);
            }
            
            return RedirectToAction("ApprovalIndex", "Settlement");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<SettlementViewModel> settlements = new List<SettlementViewModel>();
            SettlementViewModel objSettlementViewModel;
            var ids = form.GetValues("SettlementId");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objSettlementViewModel = new SettlementViewModel();
                    objSettlementViewModel.SettlementId = Convert.ToInt64(ids[i]);
                    objSettlementViewModel.RejectReason = form.GetValues(objSettlementViewModel.SettlementId.ToString())[0];
                    settlements.Add(objSettlementViewModel);
                }
                objSattlementBll.RejectSettlements(settlements);
            }
            return RedirectToAction("ApprovalIndex", "Settlement");
        }
        public ActionResult Detail(int id)
        {
            SettlementModel settlement = objSattlementBll.GetSettlementInfoById(id);
            return View(settlement);
        }
        #endregion

    }
}