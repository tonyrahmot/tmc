﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.Areas.Transaction.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Transaction.Controllers
{
    [AuthenticationFilter]
    public class ClosingSavingsController : Controller
    {
        // GET: Transaction/ClosingSavings

        private TransactionBLL objTransactionBll = new TransactionBLL();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(TransactionModel objTransactionModel)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objTransactionModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objTransactionModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objTransactionModel.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objTransactionModel.CreatedDate = DateTime.Now;
                objTransactionModel.IsActive = false;
                objTransactionModel.IsApproved = false;

                 
                objTransactionBll.SaveSavingsAccountClosing(objTransactionModel);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }
        //public JsonResult ExistLoanAccountNumber(string id)
        //{
        //    var result = objTransactionBll.GetAccountnumberLoanTable(id);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult ExistLoanAccountNumber(string id)
        {
            bool exsits = objTransactionBll.GetAccountnumberLoanTable(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        public JsonResult GetTransactionalCoaData()
        {
            var data = objTransactionBll.GetTransactionalCoaData();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUnApprovedTransactionByAcNo(string id)
        {
            bool exsits = objTransactionBll.GetUnApprovedTransactionByAcNo(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        public JsonResult GetCloseAccountTransaction(string id)
        {
            bool exsits = objTransactionBll.GetCloseAccountTransaction(id);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }
        public JsonResult GetCustomerInfoByAccountNumber(string id)
        {
            var result = objTransactionBll.GetCustomerInfoByAccountNumber(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objTransactionBll.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}