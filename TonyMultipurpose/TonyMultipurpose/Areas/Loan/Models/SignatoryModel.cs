﻿using System;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class SignatoryModel:CommonModel
    {
        public int Id { get; set; }
        public string LoanId { get; set; }
        public string FunctionalPosition { get; set; }
        public string EmpCode { get; set; }
        public string EmployeeName { get; set; }
        public string DesignationName { get; set; }
      
    }
}