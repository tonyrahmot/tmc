﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    public class EducationalQualification :CommonModel
    {
        [Key]
        public short EQualificationId { get; set; }

        [Required(ErrorMessage = "Employee name is required")]
        [Display(Name = "Employee Name")]
        public short EmployeeId { get; set; }

        public IEnumerable<EmployeeDetail> EmployeeInfo { get; set; }
        public string EmployeeName { get; set; }

        [Required]
        [Display(Name = "Exam Name")]
        [StringLength(100, ErrorMessage = "Exam name must be geter then 3 and less then 100", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string ExamName { get; set; }

        [Required]
        [Display(Name = "Institution Name")]
        [StringLength(100, ErrorMessage = "Training Title  must be geter then 3 and less then 100", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string InstitutionName { get; set; }


        [Required]
        [Display(Name = "Board of University")]
        [StringLength(100, ErrorMessage = "Training Duration must be geter then 3 and less then 100", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string Board_University { get; set; }

        [Required]
        [Display(Name = "Session")]
        [DataType(DataType.Text)]
        [StringLength(20, ErrorMessage = "Training Duration must be geter then 3 and less then 20", MinimumLength = 3)]
        public string Session { get; set; }

        [Required(ErrorMessage = "Passing Year is required")]
        [Display(Name = "Passing Year")]
        public string PassingYear { get; set; }

        [Required]
        [Display(Name = "Grade GPA")]
        [DataType(DataType.Text)]
        public double Grade_GPA { get; set; }

        public string ShowPassingYear { get; set; }
    }
}