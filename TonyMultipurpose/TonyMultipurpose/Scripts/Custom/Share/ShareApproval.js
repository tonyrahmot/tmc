﻿
$(document).ready(function () {

    // 1st replace first column header text with checkbox
    checkBoxHeader();

    //3rd click event for checkbox of each row
    $("input[name='ids']").click(function () {
        var totalRows = $("#checkableGrid td :checkbox").length;
        var checked = $("#checkableGrid td :checkbox:checked").length;

        if (checked == totalRows) {
            $("#checkableGrid").find("input:checkbox").each(function () {
                this.checked = true;
            });
        } else {
            $("#cbSelectAll").removeAttr("checked");
        }
    });

    $("#search").keyup(function () {
        filter = new RegExp($(this).val(), 'i');
        $("#checkableGrid tbody tr").filter(function () {
            $(this).each(function () {
                found = false;
                $(this).children().each(function () {
                    content = $(this).html();
                    if (content.match(filter)) {
                        found = true;
                    }
                });
                if (!found) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });
    });


});

function checkBoxHeader() {
    $("#checkableGrid th").each(function () {
        if ($.trim($(this).text().toString().toLowerCase()) === "{checkall}") {
            $(this).text('');
            $("<input/>", { type: "checkbox", id: "cbSelectAll", value: "" }).appendTo($(this));
            //            $(this).append("<span>Select All</span>");
        }
    });

    $("#cbSelectAll").on("click", function () {
        var ischecked = this.checked;
        $('#checkableGrid').find("input:checkbox").each(function () {
            this.checked = ischecked;
        });
    });
}


