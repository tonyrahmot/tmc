﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.ViewModels;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class BenefitModel:BenefitViewModel
    {
        public int BenfitId { get; set; }

        public string EmpId { get; set; }

        public string BenefitType { get; set; }

        public decimal BenefitAmount { get; set; }

        public string BenefitDate { get; set; }

        public string Description { get; set; }

        public bool IsApproved { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short? UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public short CompanyId { get; set; }
    }
}