﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    public class DepartmentController : Controller
    {
        // GET: Employee/Department
        public ActionResult Index()
        {
            DepartmentBLL objDepartmentBll = new DepartmentBLL();
            List<Department> DepartmentList = objDepartmentBll.GetAllDepartment();
            return View(DepartmentList);
        }
        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Department objDepartment)
        {
            if (ModelState.IsValid)
            {
                DepartmentBLL objDepartmentBll = new DepartmentBLL();
                objDepartmentBll.SaveDepartment(objDepartment);
            }
            return RedirectToAction("Index", "Department");
        }
        public JsonResult GetDepartmentNameIsExist(string departmentName)
        {
            DepartmentBLL objDepartmentBLL = new DepartmentBLL();
            bool exsits = objDepartmentBLL.GetDepartmentNameIsExist(departmentName);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            DepartmentBLL objDepartmentBll = new DepartmentBLL();
            var Department = objDepartmentBll.GetAllDepartment(id);
            return View(Department);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Department objDepartment)
        {
            if (ModelState.IsValid)
            {
                DepartmentBLL objDepartmentBll = new DepartmentBLL();
                objDepartmentBll.UpadateDepartment(objDepartment);
            }
            return RedirectToAction("Index", "Department");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            DepartmentBLL objDepartmentBll = new DepartmentBLL();
            var Department = objDepartmentBll.GetAllDepartment(id);
            return View(Department);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteDepartment(int id)
        {
            if (ModelState.IsValid)
            {
                DepartmentBLL objDepartmentBll = new DepartmentBLL();
                objDepartmentBll.DeleteDepartment(id);
            }
            return RedirectToAction("Index", "Department");

        }
    }
}