﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.ViewModels;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Accounts.BLL
{
    public class ReverseEntryBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForAccountViewModel(DbDataReader objDataReader, AccountsViewModel objAccountsViewModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            if (objDataTable == null) return;
            foreach (string column in from DataRow dr in objDataTable.Rows select dr.ItemArray[0].ToString())
            {
                switch (column)
                {
                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objAccountsViewModel.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "AccountTypeName":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeName"]))
                        {
                            objAccountsViewModel.AccountType = objDataReader["AccountTypeName"].ToString();
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objAccountsViewModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objAccountsViewModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objAccountsViewModel.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "OpeningDate":
                        if (!Convert.IsDBNull(objDataReader["OpeningDate"]))
                        {
                            objAccountsViewModel.OpeningDate = objDataReader["OpeningDate"].ToString();
                        }
                        break;
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDataReader["TransactionId"]))
                        {
                            objAccountsViewModel.TransactionId = objDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "AccountTypeId":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeId"]))
                        {
                            objAccountsViewModel.AccountSetupId = Convert.ToInt32(objDataReader["AccountTypeId"].ToString());
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDataReader["TransactionDate"]))
                        {
                            objAccountsViewModel.TransactionDate = Convert.ToDateTime(objDataReader["TransactionDate"].ToString());
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDataReader["TransactionType"]))
                        {
                            objAccountsViewModel.TransactionType = objDataReader["TransactionType"].ToString();
                        }
                        break;

                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objAccountsViewModel.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"].ToString());
                        }
                        break;


                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objAccountsViewModel.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "Mobile":
                        if (!Convert.IsDBNull(objDataReader["Mobile"]))
                        {
                            objAccountsViewModel.Mobile = objDataReader["Mobile"].ToString();
                        }
                        break;
                    case "MatureAmount":
                        if (!Convert.IsDBNull(objDataReader["MatureAmount"]))
                        {
                            objAccountsViewModel.MatureAmount = Convert.ToDecimal(objDataReader["MatureAmount"].ToString());
                        }
                        break;
                    case "MatureDate":
                        if (!Convert.IsDBNull(objDataReader["MatureDate"]))
                        {
                            objAccountsViewModel.MatureDate = Convert.ToDateTime(objDataReader["MatureDate"].ToString());
                        }
                        break;
                    case "InstallmentNumber":
                        if (!Convert.IsDBNull(objDataReader["InstallmentNumber"]))
                        {
                            objAccountsViewModel.InstallmentNumber = Convert.ToInt32(objDataReader["InstallmentNumber"].ToString());
                        }
                        break;
                    case "InstallmentAmount":
                        if (!Convert.IsDBNull(objDataReader["InstallmentAmount"]))
                        {
                            objAccountsViewModel.InstallmentAmount = Convert.ToDecimal(objDataReader["InstallmentAmount"].ToString());
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDataReader["Amount"]))
                        {
                            objAccountsViewModel.Amount = Convert.ToDecimal(objDataReader["Amount"].ToString());
                        }
                        break;
                    case "Fine":
                        if (!Convert.IsDBNull(objDataReader["Fine"]))
                        {
                            objAccountsViewModel.Fine = Convert.ToDecimal(objDataReader["Fine"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objAccountsViewModel.Username = objDataReader["Username"].ToString();
                        }
                        break;
                    case "InstallmentMontWithYear":
                        if (!Convert.IsDBNull(objDataReader["InstallmentMontWithYear"]))
                        {
                            //objAccountsViewModel.Particular = objDataReader["InstallmentMontWithYear"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<AccountsViewModel> GetAllTransactions(AccountsViewModel objAccountViewModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objAllTransactions = new List<AccountsViewModel>();
            try
            {
                objDbCommand.AddInParameter("CompanyId", objAccountViewModel.CompanyId);
                objDbCommand.AddInParameter("TransactionDate", objAccountViewModel.TransactionDate);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllTransactionInfoForReverseEntry", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        var objAccountModel = new AccountsViewModel();
                        BuildModelForAccountViewModel(objDbDataReader, objAccountModel);
                        //objAccountModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        //objAccountModel.TransactionType = objDbDataReader["TransactionType"].ToString();
                        //objAccountModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        //objAccountModel.InstallmentNumber = Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                        //objAccountModel.InstallmentMonth = objDbDataReader["InstallmentMontWithYear"].ToString();
                        //objAccountModel.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                        //objAccountModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        //objAccountModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        //objAccountModel.Mobile = objDbDataReader["Mobile"].ToString();
                        ////objAccountModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        //objAccountModel.InterestRate = string.IsNullOrEmpty(Convert.ToString(objDbDataReader["InterestRate"])) ? 0 : Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        //objAccountModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                        //objAccountModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                        ////objAccountModel.AccountSetupId = Convert.ToInt32(objDbDataReader["AccountTypeId"].ToString());
                        //objAccountModel.AccountSetupId = string.IsNullOrEmpty(Convert.ToString(objDbDataReader["AccountTypeId"])) ? 0 : Convert.ToInt32(objDbDataReader["AccountTypeId"].ToString());
                        //objAccountModel.AccountType = objDbDataReader["AccountTypeName"].ToString();
                        ////objAccountModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"]) ;
                        //objAccountModel.Amount = string.IsNullOrEmpty(Convert.ToString(objDbDataReader["Amount"])) ? 0 : Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        //objAccountModel.Fine = Convert.ToDecimal(objDbDataReader["Fine"]);
                        //objAccountModel.Username = objDbDataReader["Username"].ToString();
                        objAllTransactions.Add(objAccountModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return objAllTransactions;
        }

        public string SaveTransactionForReverseEntry(string transactionId, string accountTypeId, int createdBy)
        {
            string transactionStatus;

            objDataAccess = DataAccess.NewDataAccess();

            try
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("transactionId", transactionId);
                objDbCommand.AddInParameter("accountTypeId", accountTypeId);
                objDbCommand.AddInParameter("createdBy", createdBy);
                var noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveReverseTransactionEntry]",
                    CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    transactionStatus = "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    transactionStatus = "failed";
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return transactionStatus;
        }
    }
}