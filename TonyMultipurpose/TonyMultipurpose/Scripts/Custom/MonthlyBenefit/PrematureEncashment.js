﻿(function () {
    $('#SRate').hide();
    $('#TransactionDate').datepicker({ dateFormat: 'dd-Myy', type: Text }).click(function () { $(this).focus(); });
    //Account No autocomplete
    $("#AccountNumber").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/AutoCompleteMBAccountNumber",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {

                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $(function () {
        $("#ChartOfAccount").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/GetTransactionalCoaData",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
                $.each(data, function (key, value) {
                    $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
                });
            }
        });
    });

    //load data
    $("#AccountNumber").change(function () {
        $('#CustomerName').val('');
        $('#DepositAmount').val('');
        $('#DueAmount').val('');
        $('#SavingsInterestRate').val('');
        $('#MBAmount').val('');
        $('#TotalAmount').val('');
        $('#DurationYear').val('');

        var AccountNumber = $("#AccountNumber").val();
        var json = {
            AccountNumber: AccountNumber
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/GetPrematureMbAccountInfoByAccountNo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                UnAppovedMBList();
                $('#CustomerName').val(data.CustomerName);
                $('#DepositAmount').val(data.DepositAmount);
                $('#DurationYear').val(data.DurationofMonth);
                $('#DueAmount').val(data.ReturnAmount);
                $('#MBAmount').val(data.AccruedBalance);
                $('#hdfCustomerId').val(data.CustomerId);
                $('#hdfReturnAmount').val(data.ReturnAmount);
                $('#hdfMatureDate').val(formateDate(data.MatureDate));
                $('#hdfOpeningDate').val(formateDate(data.OpeningDate));
                $('#CustomerImage').attr('src', data.CustomerImage);
                Calculation();
            }
        });
    });

    function UnAppovedMBList() {

        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/GetUnAppovedMBList",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $("#submit").removeAttr('disabled');
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('Please Approve or Reject the Previous Transaction,Otherwise no transaction is allowed.');
                }
                else {
                    CloseMBAccount();
                }
            },

        });
    }

    function CloseMBAccount() {
        var id = $("#AccountNumber").val();
        var json = { id: id };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/GetCloseMBAccount",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                if (data == false) {
                    $("#submit").attr('disabled', 'disabled');
                    alert('This Account was closed !!!');
                }
            },
        });
    }

    function Calculation() {

        var oDate = $('#hdfOpeningDate').val();
        var cDate = $('#TransactionDate').val();
        // usrDate = opening date
        var usrDate = new Date(oDate);
        var curDate = new Date(cDate);
        var usrYear, usrMonth = usrDate.getMonth() + 1;
        var curYear, curMonth = curDate.getMonth() + 1;
        if ((usrYear = usrDate.getFullYear()) < (curYear = curDate.getFullYear())) {
            curMonth += (curYear - usrYear) * 12;
        }
        var diffMonths = curMonth - usrMonth;
        if (usrDate.getDate() > curDate.getDate()) diffMonths--;

        if (diffMonths >= 12) {
            //savings rate calculation
            $('#SRate').show();
            SavingsRateCalculation(diffMonths);

        } else {
            // only get capital
            CapitalCalculation();
        }

    }

    /// savigs calculation

    function SavingsRateCalculation(diffMonths) {

        var depositeA = parseFloat($('#DepositAmount').val());
        var returnA = parseFloat($('#hdfReturnAmount').val());
        $('#SavingsInterestRate').keyup(function () {
            var interestRate = parseFloat($('#SavingsInterestRate').val());
            var benefitAmount = parseFloat(((depositeA * interestRate) / 1200) * diffMonths);
            var interestWithTotal = parseFloat(depositeA + benefitAmount);
            var totalAmount = parseFloat(interestWithTotal - returnA);
            $('#TotalAmount').val(totalAmount);

        });

    }

    // capital calculation

    function CapitalCalculation() {
        var depositeA = parseFloat($('#DepositAmount').val());
        var returnA = parseFloat($('#hdfReturnAmount').val());
        var totalAmount = parseFloat(depositeA - returnA);
        $('#TotalAmount').val(totalAmount);

    }

    $('#submit').click(function () {
        var isAllValid = true;
        if ($('#TransactionDate').val().trim() == '') {
            $('#TransactionDate').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#TransactionDate').siblings('span.error').css('display', 'none');
        }
        if ($('#AccountNumber').val().trim() == '') {
            $('#AccountNumber').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#AccountNumber').siblings('span.error').css('display', 'none');
        }

        if ($('#ChartOfAccount').val().trim() == 0) {
            $('#ChartOfAccount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').siblings('span.error').css('display', 'none');
        }

        if (isAllValid) {

            var data = {
                AccountNumber: $('#AccountNumber').val(),
                TransactionDate: $('#TransactionDate').val(),
                SavingsInterestRate: $('#SavingsInterestRate').val(),
                Balance: $('#TotalAmount').val(),
                CustomerName: $('#CustomerName').val(),
                CustomerId: $('#hdfCustomerId').val(),
                MatureDate: $('#hdfMatureDate').val(),
                ReturnAmount: $('#DueAmount').val(),
                DepositAmount: $('#DepositAmount').val(),
                COAId: $('#ChartOfAccount option:selected').val()
            }
            //console.log(data);
            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "MonthlyBenefit/PreMatureEncashment/Save",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        alert('Successfully done.');
                        $("#AccountNumber").val('');
                        $("#TransactionDate").val('');
                        $('#DepositAmount').val('');
                        $('#CustomerName').val('');
                        $("#hdfCustomerId").val('');
                        $('#DurationYear').val('');
                        $('#MBAmount').val('');
                        $('#hdfReturnAmount').val('');
                        $('#hdfMatureDate').val('');
                        $('#hdfOpeningDate').val('');
                        $('#ChartOfAccount').val('0');

                    } else {
                        alert('Failed');
                    }
                    $('#submit').val('Save');
                    window.location.reload();
                },
                error: function () {
                    alert('Error. Please try again.');
                }
            });
            //        }

        }
    });

    var months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
    ];

    //convert datetime
    function formateDate(jsonDate) {
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = months[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '-' + mm + '-' + yyyy;
        return formmatedDate;
    }

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

    function monthNameToNum(monthname) {
        var month = months.indexOf(monthname);
        return month ? month + 1 : 0;
    }
})();
function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode == 13) {
        return false;
    }
    status = "";
    return true;
}