﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Member.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Member.BLL
{
    //ataur
    public class NomineeDetalBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForNomineeDetail(DbDataReader objDataReader, NomineeDetail objNomineeDetail)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "NomineeId":
                        if (!Convert.IsDBNull(objDataReader["NomineeId"]))
                        {
                            objNomineeDetail.NomineeId = Convert.ToInt32(objDataReader["NomineeId"]);
                        }
                        break;
                    case "MemberId":
                        if (!Convert.IsDBNull(objDataReader["MemberId"]))
                        {
                            objNomineeDetail.MemberId = Convert.ToInt32(objDataReader["MemberId"]);
                        }
                        break;
                    case "MemberName":
                        if (!Convert.IsDBNull(objDataReader["MemberName"]))
                        {
                            objNomineeDetail.MemberName = objDataReader["MemberName"].ToString();
                        }
                        break;
                    case "NomineeName":
                        if (!Convert.IsDBNull(objDataReader["NomineeName"]))
                        {
                            objNomineeDetail.NomineeName = objDataReader["NomineeName"].ToString();
                        }
                        break;
                    case "NomineeFatherName":
                        if (!Convert.IsDBNull(objDataReader["NomineeFatherName"]))
                        {
                            objNomineeDetail.NomineeFatherName = objDataReader["NomineeFatherName"].ToString();
                        }
                        break;
                    case "NomineeMotherName":
                        if (!Convert.IsDBNull(objDataReader["NomineeMotherName"]))
                        {
                            objNomineeDetail.NomineeMotherName = objDataReader["NomineeMotherName"].ToString();
                        }
                        break;
                    case "SpouseName":
                        if (!Convert.IsDBNull(objDataReader["SpouseName"]))
                        {
                            objNomineeDetail.SpouseName = objDataReader["SpouseName"].ToString();
                        }
                        break;
                    case "ContactNumber":
                        if (!Convert.IsDBNull(objDataReader["ContactNumber"]))
                        {
                            objNomineeDetail.ContactNumber = objDataReader["ContactNumber"].ToString();
                        }
                        break;
                    case "Address":
                        if (!Convert.IsDBNull(objDataReader["Address"]))
                        {
                            objNomineeDetail.Address = objDataReader["Address"].ToString();
                        }
                        break;
                    case "NID":
                        if (!Convert.IsDBNull(objDataReader["NID"]))
                        {
                            objNomineeDetail.NID = objDataReader["NID"].ToString();
                        }
                        break;
                    case "Nationality":
                        if (!Convert.IsDBNull(objDataReader["Nationality"]))
                        {
                            objNomineeDetail.Nationality = objDataReader["Nationality"].ToString();
                        }
                        break;
                    case "DateOfBirth":
                        if (!Convert.IsDBNull(objDataReader["DateOfBirth"]))
                        {
                            objNomineeDetail.DateOfBirth = objDataReader["DateOfBirth"].ToString();
                        }
                        break;
                    case "Occupation":
                        if (!Convert.IsDBNull(objDataReader["Occupation"]))
                        {
                            objNomineeDetail.Occupation = objDataReader["Occupation"].ToString();
                        }
                        break;
                    case "Designation":
                        if (!Convert.IsDBNull(objDataReader["Designation"]))
                        {
                            objNomineeDetail.Designation = objDataReader["Designation"].ToString();
                        }
                        break;
                    case "Image":
                        if (!Convert.IsDBNull(objDataReader["Image"]))
                        {
                            objNomineeDetail.Image =(byte[])objDataReader["Image"];
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objNomineeDetail.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objNomineeDetail.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objNomineeDetail.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objNomineeDetail.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objNomineeDetail.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objNomineeDetail.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objNomineeDetail.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objNomineeDetail.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }


        public List<NomineeDetail> GetAllNomineeInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<NomineeDetail> objNomineeDetails=new List<NomineeDetail>();
            NomineeDetail objNomineeDetail;

            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllNomineeInfoList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                       objNomineeDetail=new NomineeDetail();
                        this.BuildModelForNomineeDetail(objDbDataReader, objNomineeDetail);
                       objNomineeDetails.Add(objNomineeDetail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objNomineeDetails;
        }
    }
}