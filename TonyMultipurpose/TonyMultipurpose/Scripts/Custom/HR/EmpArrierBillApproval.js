﻿(function () {
    arrierTable();
    var oTable;

    function arrierTable() {
        oTable = $('#ArrierTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/EmpArrierBillUnApprovaList",//'@Url.Action("EmpArrierBillUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                { "data": "EmployeeName" },

                {
                    "data": "ArrierDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "ArrierAmount" },
                { "data": "Description" },
                {
                    "data": "EmpCode",
                    "render": function (data) {
                        return '<a class="btn btn-primary" href= "' + window.applicationBaseUrl + 'HR/HR/ArrierBillApproval"?id=' + data + '">Approve</a>';
                    }
                }
            ],
            "order": [[0, "desc"]]
        });

    }

    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();