﻿using System;
using System.Data;

namespace TonyMultipurpose.ReportsViewer
{
    public class Class1
    {
        private void DoSomething()
        {
            DataTable dnt = new DataTable();
            dnt.Columns.Add("LoanId", typeof(string));
            dnt.Columns.Add("LoanNo", typeof(string));
            dnt.Columns.Add("AccountSetupId", typeof(int));
            dnt.Columns.Add("CustomerId", typeof(string));
            dnt.Columns.Add("AccountNumber", typeof(string));
            dnt.Columns.Add("LoanDate", typeof(DateTime));
            dnt.Columns.Add("LoanExpiryDate", typeof(DateTime));
            dnt.Columns.Add("LoanAmount", typeof(decimal));
            dnt.Columns.Add("InterestRate", typeof(decimal));
            dnt.Columns.Add("InterestAmount", typeof(decimal));
            dnt.Columns.Add("TotalAmountWithInterest", typeof(decimal));
            dnt.Columns.Add("InstallmentAmount", typeof(decimal));
            dnt.Columns.Add("InstallmentTypeId", typeof(byte));
            dnt.Columns.Add("TotalInterest", typeof(decimal));
            dnt.Columns.Add("NumberOfInstallment", typeof(byte));
            dnt.Columns.Add("PurposeOfLoan", typeof(string));
            dnt.Columns.Add("ReferenceName", typeof(string));
            dnt.Columns.Add("CustomerId1", typeof(string));
            dnt.Columns.Add("CustomerName", typeof(string));
            dnt.Columns.Add("CustomerImage", typeof(string));
            dnt.Columns.Add("FatherName", typeof(string));
            dnt.Columns.Add("MotherName", typeof(string));
            dnt.Columns.Add("SpouseName", typeof(string));
            dnt.Columns.Add("CustomerEntryDate", typeof(DateTime));
            dnt.Columns.Add("DateOfBirth", typeof(DateTime));
            dnt.Columns.Add("OccupationAndPosition", typeof(string));
            dnt.Columns.Add("Position", typeof(string));
            dnt.Columns.Add("PassportExpireDate", typeof(string));


            dnt.Columns.Add("PresentAddress", typeof(string));
            dnt.Columns.Add("PermanentAddress", typeof(string));
            dnt.Columns.Add("OfficeAddress", typeof(string));
            dnt.Columns.Add("TradeLicAuthority", typeof(string));
            dnt.Columns.Add("BranchName", typeof(string));
            dnt.Columns.Add("BranchCode", typeof(string));
            dnt.Columns.Add("InstallmentTypeName", typeof(string));
            dnt.Columns.Add("Username", typeof(string));
            dnt.Columns.Add("FirstNomineeName", typeof(string));
            dnt.Columns.Add("SecnondNomineeName", typeof(string));
            dnt.Columns.Add("FirstGuarantorContactNo", typeof(string));
            dnt.Columns.Add("SecnondGuarantorContactNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("SecondChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("FirstChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("SecondChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("SecondChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoBankName", typeof(string));
            dnt.Columns.Add("SecondChequeInfoBankName", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoBankName", typeof(string));
            dnt.Columns.Add("FirstFunctionalPosition", typeof(string));
            dnt.Columns.Add("SecondFunctionalPosition", typeof(string));
            dnt.Columns.Add("ThirdFunctionalPosition", typeof(string));
            dnt.Columns.Add("ForthFunctionalPosition", typeof(string));


            dnt.Columns.Add("FirstSigDesignation", typeof(string));
            dnt.Columns.Add("SecondSigDesignation", typeof(string));
            dnt.Columns.Add("ThirdSigDesignation", typeof(string));
            dnt.Columns.Add("ForthSigDesignation", typeof(string));
            dnt.Columns.Add("FirstEmployeeName", typeof(string));
            dnt.Columns.Add("SecondEmployeeName", typeof(string));
            dnt.Columns.Add("ThirdEmployeeName", typeof(string));
            dnt.Columns.Add("ForthEmployeeName", typeof(string));
           
        }
        
    }
}