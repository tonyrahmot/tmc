﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class HolidayModel
    {
        public int HolidayId { get; set; }

        public string Holiday { get; set; }

        public string HolidayType { get; set; }

        public bool IsApproved { get; set; }

        public short CompanyId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short? UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}