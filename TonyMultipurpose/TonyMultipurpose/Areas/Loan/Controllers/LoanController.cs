﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Customer.BLL;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
       [AuthenticationFilter]
    public class LoanController : Controller
    {
        CommonResult oCommonResult;
        LoanModel objLoan;
        LoanBLL objLoanBLL;
        AdditionalLoanDetail objAdditionalLoanDetail;

        #region new term loan
        public ActionResult NewTermLoan()
        {
            return View();
        }
        #endregion

        #region Loan Main

        // GET: Loan/Loan
        //public ActionResult Index()
        //{
        //    return View();
        //}


        //public ActionResult NewLoan()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult NewLoan(LoanBasicViewModel viewModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //do save operation

        //        string id = "";
        //        RedirectToAction("UpdateLoan", new {id});
        //    }

        //    return View(viewModel);
        //}


        //public ActionResult UpdateLoan(string id)
        //{
        //    return View();
        //}


        //public ActionResult GuarantorInfo(string id)
        //{
        //    return View();
        //}


        //public ActionResult FinancialInfo(string id)
        //{
        //    return View();
        //}


        //public ActionResult CommentsAndSignatoryInfo(string id)
        //{
        //    return View();
        //}


        //public ActionResult ChequeInfo(string id)
        //{
        //    return View();
        //}

        public JsonResult GetLoanTypeByAccountTitle()
        {

            objLoanBLL = new LoanBLL();

            var data = objLoanBLL.GetLoanTypeByAccountTitle();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLoanType()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetLoanType();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetResidentialConditionInfo()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetResidentialConditionInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInstallmentType()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetInstallmentType();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteCustomerId(string term)
        {
            AccountBLL objAccountBll = new AccountBLL();
            var result = objAccountBll.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLoanId(string loanType)
        {
            AccountBLL objAccountBll = new AccountBLL();
            objLoanBLL = new LoanBLL();
            var interestInfo = objAccountBll.GetInterestRateByAccountType(loanType);
            var loanId = objLoanBLL.GetLoanId(loanType);
            return new JsonResult { Data = new { loanId = loanId, interestInfo = interestInfo } };
        }

        public JsonResult GetRefName(int loanType)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetRefName(loanType);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSavingsAccounNumber(string customerId)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetSavingsAccounNumber(customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerInfo(string customerId)
        {
            CustomerBLL objCustomerBll = new CustomerBLL();
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = true;
            customer.IsDeleted = false;
            var data = objCustomerBll.GetAllCustomerInfo(customer).FirstOrDefault(a => a.CustomerId == customerId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Save()
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = false;
            objLoan.IsRejected = false;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == false && a.IsSubmittedToCheck == false && a.CreatedBy == Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            return Json(new { data = loans }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(LoanModel objLoan)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                objLoan.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objLoan.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objLoan.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objLoan.CreatedDate = DateTime.Now.ToString();
                objLoan.IsEntry = true;
                objLoan.IsChecked = false;
                objLoan.IsApproved = false;
                objLoan.IsRejected = false;
                objLoan.IsClosed = false;
                objLoan.IsSubmittedToCheck = false;
                oCommonResult = objLoanBLL.SaveLoanInfo(objLoan);
            }
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult SubmitForCheck(string loanId)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SubmitForCheck(loanId);
            return new JsonResult { Data = new { oCommonResult } };
        }
        public JsonResult Get_COAaccountCodeInfo()
        {
            AccountBLL objAccountBll = new AccountBLL();
            var data = objAccountBll.Get_COAaccountCodeInfo();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region customer cheque info

        [HttpGet]
        public ActionResult CustomersChequeInfo(string loanId)
        {
            LoanCustomerChequeInfo obj = new LoanCustomerChequeInfo();
            obj.LoanId = loanId;
            return View(obj);
        }

        [HttpPost]
        public ActionResult CustomersChequeInfo(LoanCustomerChequeInfo obj)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            if (obj.LoanCustomerChequeInfoId == 0)
            {
                oCommonResult = objLoanBLL.SaveLoanCustomersChequeInfo(obj);
            }
            else
            {
                oCommonResult = objLoanBLL.UpdateLoanCustomersChequeInfo(obj);
            }

            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteLoanCustomerChequeInfo(LoanCustomerChequeInfo obj)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteLoanCustomerChequeInfo(obj);
            return new JsonResult { Data = new { oCommonResult } };
        }

        public JsonResult GetAllLoanCustomerChequeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<LoanCustomerChequeInfo> info = objLoanBLL.GetAllLoanCustomerChequeInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region Guarantor

        [HttpGet]
        public ActionResult FinancialJustification(string loanId)
        {
            MonthlyIncome objGuarantor = new MonthlyIncome();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }
        public JsonResult GetAllGurantorInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<Guarantor> Guarantor = objLoanBLL.GetAllGurantorInfo(loanId);
            return Json(new { data = Guarantor }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGuarantorRoll(string LoanId)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetAllGurantorInfo(LoanId).Where(a => a.LoanId == LoanId).Select(v => v.GuarantorRoll).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Guarantor(string loanId)
        {
            Guarantor objGuarantor = new Guarantor();
            objGuarantor.LoanId = loanId;
            return View(objGuarantor);
        }
        [HttpPost]
        public ActionResult Guarantor(Guarantor objGuarantor)
        {
            var oCommonResult = new CommonResult();
            var path = string.Empty;
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (objGuarantor.ImageFile != null)
                {
                    var rootPath = "~\\Uploads\\Guarantor\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);
                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objGuarantor.LoanId + "_" + objGuarantor.GuarantorRoll, ".jpg"));
                    string filesToDelete = objGuarantor.LoanId + "_" + objGuarantor.GuarantorRoll + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {
                        System.IO.File.Delete(file);
                    }
                    objGuarantor.ImagePath = path;
                }
                oCommonResult = objLoanBLL.SaveGuarantor(objGuarantor);
                if (oCommonResult.Status == true && path != string.Empty)
                {
                    try
                    {
                        objGuarantor.ImageFile.SaveAs(path);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateGuarantor(Guarantor objGuarantor)
        {
            var path = string.Empty;
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (objGuarantor.ImageFile != null)
                {
                    var rootPath = "~\\Uploads\\Guarantor\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);
                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objGuarantor.LoanId + "_" + objGuarantor.GuarantorRoll, ".jpg"));
                    string filesToDelete = objGuarantor.LoanId + "_" + objGuarantor.GuarantorRoll + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {
                        System.IO.File.Delete(file);
                    }
                    objGuarantor.ImagePath = path;
                }
                oCommonResult = objLoanBLL.UpdateGuarantor(objGuarantor);
                if (oCommonResult.Status == true && path != string.Empty)
                {
                    try
                    {
                        objGuarantor.ImageFile.SaveAs(path);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult DeleteGuaranorInfo(Guarantor objGuarantor)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteGuaranorInfo(objGuarantor);
            return new JsonResult { Data = new { oCommonResult } };
        }

        #endregion

        #region AddtionalLoanInfo


        [HttpPost]
        public ActionResult AddaddintionLoan(string loanId)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objLoanBLL = new LoanBLL();
                objLoanBLL.SaveAdditionalLoanInfo(loanId);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = loanId } };
        }

        [HttpPost]
        public ActionResult DeductAdditonLoan(string loanId)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objLoanBLL = new LoanBLL();
                objLoanBLL.DeductAdditonLoan(loanId);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = loanId } };
        }

        [HttpPost]
        public ActionResult DeleteAdditionalLoanInfo(AdditionalLoanDetail objA)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteAdditionalLoanInfo(objA);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }

        [HttpPost]
        public ActionResult UpdateAdditionalLoanInfo(List<AdditionalLoanDetail> _objALDList)
        {

            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.UpdateAdditionalLoanInfo(_objALDList);
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpGet]
        public JsonResult GetAdditionalLoanInfoById(string LoanId)
        {
            objLoanBLL = new LoanBLL();
            var result = objLoanBLL.GetAdditionalLoanInfoById(LoanId).ToList();

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllAdditionalLoanInfo()
        {
            objLoanBLL = new LoanBLL();
            var info = objLoanBLL.GetAllAdditionalLoanInfo().ToList();
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Monthly Income
        public JsonResult GetAllMonthlyIncomeInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyIncome> MonthlyIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId);
            //var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).ToList().Sum(x => x.Amount);
            return Json(new { data = MonthlyIncome }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetaIncomeAmountByIncomeType(string incomeType, string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            var ExpenseAmount = objLoanBLL.GetaIncomeAmountByIncomeType(incomeType, loanId);
            return Json(new { data = ExpenseAmount.Amount, ExpenseAmount.IncomeType }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveIncome(MonthlyIncome monthlyIncome)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SaveMonthlyIncome(monthlyIncome);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateMonthlyIncome(MonthlyIncome objMonthlyIncome)
        {
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();

            oCommonResult = objLoanBLL.UpdateMonthlyIncome(objMonthlyIncome);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        [HttpPost]
        public ActionResult DeleteMonthlyIncome(MonthlyIncome objMonthlyIncome)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteMonthlyIncome(objMonthlyIncome);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }

        #endregion

        #region Monthly Expense

        public JsonResult GetAllMonthlyExpenseInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<MonthlyExpense> MonthlyExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId);
            return Json(new { data = MonthlyExpense }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetaExpenseAmountByExpenseType(string expenseType, string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            var ExpenseAmount = objLoanBLL.GetaExpenseAmountByExpenseType(expenseType, loanId);
            return Json(new { data = ExpenseAmount.MonthlyExpenseId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveExpense(MonthlyExpense monthlyExpense)
        {
            var oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SaveExpense(monthlyExpense);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateExpense(MonthlyExpense monthlyExpense)
        {
            var oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.UpdateExpense(monthlyExpense);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateMonthlyExpense(MonthlyExpense objMonthlyExpense)
        {
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();

            oCommonResult = objLoanBLL.UpdateMonthlyExpense(objMonthlyExpense);

            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult DeleteMonthlyExpense(MonthlyExpense objMonthlyExpense)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteMonthlyExpense(objMonthlyExpense);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        #endregion

        #region Bank Chaque List
        public JsonResult GetBankName()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetBankName();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BankCheckList(string loanId)
        {
            return View();
        }
        [HttpPost]
        public ActionResult BankCheckListSave(BankCheckList objBankCheckList)
        {
            var path = string.Empty;
            bool status = false;
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (objBankCheckList.Path == "Cersi")
                {
                    var rootPath = "~\\Uploads\\Cheque\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);

                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objBankCheckList.ChequeId, ".jpg"));
                    string filesToDelete = objBankCheckList.ChequeId + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {

                        System.IO.File.Delete(file);
                    }
                    objBankCheckList.Path = path;
                }
                var c = objLoanBLL.BankCheckListSave(objBankCheckList);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = objBankCheckList.ChequeId, image = objBankCheckList.Path } };
        }
        [HttpPost]
        public JsonResult SaveImageFile(BankCheckList objBankCheckList)
        {

            bool status = false;
            try
            {
                objBankCheckList.Imagefile.SaveAs(objBankCheckList.Path);
                status = true;

            }
            catch (Exception ex)
            {
                status = false;
            }

            var obj = new { status = status, image = objBankCheckList.Path };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllBankCheckListInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<BankCheckList> BankCheckList = objLoanBLL.GetAllBankCheckListInfo(loanId);
            return Json(new { data = BankCheckList }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateBankCheckList(BankCheckList objBankCheckList)
        {
            var path = string.Empty;
            bool status = false;
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (objBankCheckList.Path == "Cersi")
                {
                    var rootPath = "~\\Uploads\\Cheque\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);

                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objBankCheckList.ChequeId, ".jpg"));
                    string filesToDelete = objBankCheckList.ChequeId + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {

                        System.IO.File.Delete(file);
                    }
                    objBankCheckList.Path = path;
                }
                oCommonResult = objLoanBLL.UpdateBankCheckList(objBankCheckList);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = oCommonResult.Status, Id = objBankCheckList.ChequeId, image = objBankCheckList.Path } };
        }
        [HttpPost]
        public ActionResult DeleteBankCheckList(BankCheckList objBankCheckList)
        {
            var path = string.Empty;
            bool status = false;
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (objBankCheckList.Path == "Cersi")
                {
                    var rootPath = "~\\Uploads\\Cheque\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);

                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objBankCheckList.ChequeId, ".jpg"));
                    string filesToDelete = objBankCheckList.ChequeId + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {

                        System.IO.File.Delete(file);
                    }
                    objBankCheckList.Path = path;
                }
                oCommonResult = objLoanBLL.DeleteBankCheckList(objBankCheckList);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status } };
        }
        #endregion

        #region NotificationCheck
        public JsonResult GetAllNotificationInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<NotificationCheck> info = objLoanBLL.GetAllNotificationInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NotificationCheckList(string loanId)
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveNotification(NotificationCheck obj)
        {
            var oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            obj.CreatedBy = Convert.ToByte(SessionUtility.TMSessionContainer.UserID);
            oCommonResult = objLoanBLL.SaveNotification(obj);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateNotification(NotificationCheck obj)
        {
            var oCommonResult = new CommonResult();
            LoanBLL objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                obj.CreatedBy = Convert.ToByte(SessionUtility.TMSessionContainer.UserID);
                oCommonResult = objLoanBLL.UpdateNotification(obj);
            }
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        [HttpPost]
        public ActionResult DeleteNotification(NotificationCheck obj)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteNotification(obj);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        #endregion

        #region Comments and Signatory 

        [HttpGet]
        public ActionResult CommentsAndSignatory(string loanId)
        {
            AddtionalInfoModel addtionalInfoModel = new AddtionalInfoModel();
            addtionalInfoModel.LoanId = loanId;
            return View(addtionalInfoModel);
        }


        [HttpPost]
        public ActionResult SaveLoanCustomerDetails(AddtionalInfoModel objAddtionalInfoModel)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            if (objAddtionalInfoModel.Id == 0)
            {
                oCommonResult = objLoanBLL.SaveLoanCustomerDetails(objAddtionalInfoModel);
            }
            else
            {
                oCommonResult = objLoanBLL.UpdateCustomerDeviationInfo(objAddtionalInfoModel);
            }

            return new JsonResult { Data = new { oCommonResult } };
        }

        public ActionResult UpdateCustomerDeviationInfo(AddtionalInfoModel addtionalInfoModel)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.UpdateCustomerDeviationInfo(addtionalInfoModel);
            return new JsonResult { Data = new { oCommonResult } };
        }


        public JsonResult GetAddtionalInfoData(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<AddtionalInfoModel> addtionalInfos = objLoanBLL.GetAddtionalInfoData(loanId);

            return Json(new { data = addtionalInfos }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            var path = string.Empty;
            string filesToDelete = string.Empty;
            bool status = false;
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (scanDocumentModel.ImagePath == "Cersi")
                {
                    var rootPath = "~\\Uploads\\LoanScanDocument\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);

                    }
                    if (scanDocumentModel.Imagefile.ContentType.Contains("image"))
                    {
                        path = Path.Combine(rootFolderPath, string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, ".jpg"));
                        filesToDelete = scanDocumentModel.LoanId + "_" + scanDocumentModel.DocumentName + ".jpg";
                    }
                    else
                    {
                        path = Path.Combine(rootFolderPath, string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, Path.GetExtension(scanDocumentModel.Imagefile.FileName)));
                        filesToDelete = scanDocumentModel.LoanId + "_" + scanDocumentModel.DocumentName + Path.GetExtension(scanDocumentModel.Imagefile.FileName);
                    }
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {

                        System.IO.File.Delete(file);
                    }
                    scanDocumentModel.ImagePath = path;
                }
                var c = objLoanBLL.SaveScanDocumentInfo(scanDocumentModel);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = scanDocumentModel.LoanId, image = scanDocumentModel.ImagePath } };
        }


        [HttpPost]
        public ActionResult UpdateScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            var path = string.Empty;
            string filesToDelete = string.Empty;
            bool status = false;
            objLoanBLL = new LoanBLL();
            if (ModelState.IsValid)
            {
                if (scanDocumentModel.OldImagePath == "arya")
                {
                    if (scanDocumentModel.ImagePath == "Cersi")
                    {
                        var rootPath = "~\\Uploads\\LoanScanDocument\\";
                        string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                        if (!Directory.Exists(rootFolderPath))
                        {
                            Directory.CreateDirectory(rootFolderPath);
                        }

                        if (scanDocumentModel.Imagefile.ContentType.Contains("image"))
                        {
                            path = Path.Combine(rootFolderPath, string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, ".jpg"));
                            filesToDelete = scanDocumentModel.LoanId + "_" + scanDocumentModel.DocumentName + ".jpg";
                        }
                        else
                        {
                            path = Path.Combine(rootFolderPath, string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, Path.GetExtension(scanDocumentModel.Imagefile.FileName)));
                            filesToDelete = scanDocumentModel.LoanId + "_" + scanDocumentModel.DocumentName + Path.GetExtension(scanDocumentModel.Imagefile.FileName);
                        }
                        //path = Path.Combine(rootFolderPath, string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, ".jpg"));
                        //string filesToDelete = scanDocumentModel.LoanId + "_" + scanDocumentModel.DocumentName + ".jpg";
                        string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                        foreach (string file in fileList)
                        {
                            System.IO.File.Delete(file);
                        }
                        scanDocumentModel.ImagePath = path;
                    }
                }
                else
                {
                    if (scanDocumentModel.ImagePath == "Cersi")
                    {
                        string customerImageName = string.Empty;
                        string currentFolderPath = string.Empty;
                        if (!string.IsNullOrEmpty(scanDocumentModel.OldImagePath))
                        {
                            currentFolderPath = System.IO.Path.GetDirectoryName(scanDocumentModel.OldImagePath.ToString());
                        }
                        if (!string.IsNullOrEmpty(currentFolderPath))
                        {
                            //string[] imageFiles = (Directory.GetFiles(currentFolderPath, "*.JPG"));
                            string[] imageFiles = (Directory.GetFiles(currentFolderPath, "*"));
                            List<string> imgFiles = imageFiles.Select(imagePath => Path.GetFileNameWithoutExtension(imagePath)).ToList();
                            int imgCount = imgFiles.Count(i => i.StartsWith(scanDocumentModel.LoanId));
                            if (imgCount > 0)
                            {
                                if (scanDocumentModel.Imagefile.ContentType.Contains("image"))
                                {
                                    customerImageName = string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, "V",
                                    (imgCount + 1).ToString().PadLeft(2, '0'), ".jpg");
                                }
                                else
                                {
                                    customerImageName = string.Concat(scanDocumentModel.LoanId, "_", scanDocumentModel.DocumentName, "V",
                                    (imgCount + 1).ToString().PadLeft(2, '0'), Path.GetExtension(scanDocumentModel.Imagefile.FileName));
                                }
                                path = Path.Combine(currentFolderPath, customerImageName);
                            }
                            scanDocumentModel.ImagePath = path;
                        }
                    }
                }
                var c = objLoanBLL.UpdateScanDocumentInfo(scanDocumentModel);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { scanId = scanDocumentModel.Id, status = status, loanId = scanDocumentModel.LoanId, image = scanDocumentModel.ImagePath } };
        }

        [HttpPost]
        public ActionResult DeleteScanDocumentInfo(ScanDocumentModel scanDocumentModel)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.DeleteScanDocumentInfo(scanDocumentModel);
            return Json(new { data = oCommonResult }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveScanDocumentImageFile(ScanDocumentModel objScan)
        {

            bool status = false;
            try
            {
                objScan.Imagefile.SaveAs(objScan.ImagePath);
                status = true;

            }
            catch (Exception ex)
            {
                status = false;
            }

            var obj = new { status = status, image = objScan.ImagePath };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetScanDocumnetInfo(string loanId)
        {
            objLoanBLL = new LoanBLL();
            List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            return Json(new { data = scanDocuments }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBranchWiseEmployee()
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetBranchWiseEmployee();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDesignationByEmpCode(string EmpCode)
        {
            objLoanBLL = new LoanBLL();
            var data = objLoanBLL.GetDesignationByEmpCode(EmpCode);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExitsEmployeeSignatoriesTable(string empCode, string loanId)
        {
            objLoanBLL = new LoanBLL();
            bool exsits = objLoanBLL.GetExitsEmployee(empCode, loanId);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            else { return Json(true, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        public ActionResult SaveSignatoryData(SignatoryModel signatory)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SaveSignatoryData(signatory);
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult DeleteSignatoryData(SignatoryModel signatory)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.DeleteSignatoryData(signatory);
            return new JsonResult { Data = new { oCommonResult } };
        }

        [HttpPost]
        public ActionResult UpdateSignatoryData(SignatoryModel signatory)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.UpdateSignatoryData(signatory);
            return new JsonResult { Data = new { oCommonResult } };
        }

        public JsonResult GetSignatoryDataForDataTable(string loanId)
        {
            objLoanBLL = new LoanBLL();
            //List<ScanDocumentModel> scanDocuments = objLoanBLL.GetScanDocumnetInfo(loanId);
            List<SignatoryModel> signatory = objLoanBLL.GetSignatoryDataForDataTable(loanId);
            return Json(new { data = signatory }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bank Performance
        public JsonResult GetAllBankperformanceInfo(string loanId)
        {
            LoanBLL objLoanBLL = new LoanBLL();
            List<BankPerformance> info = objLoanBLL.GetAllBankperformanceInfo(loanId);
            return Json(new { data = info }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddbankPerformance(string loanId)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.AddbankPerformance(loanId);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult Deductbankperformance(string loanId)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.Deductbankperformance(loanId);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult SaveBankperformance(BankPerformance obj)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.SaveBankperformance(obj);
            return new JsonResult { Data = new { oCommonResult } };
        }
        [HttpPost]
        public ActionResult UpdateBankperformance(List<BankPerformance> _objBankPerformanceList)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.UpdateBankperformance(_objBankPerformanceList);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        [HttpPost]
        public ActionResult DeleteBankperformance(BankPerformance obj)
        {
            objLoanBLL = new LoanBLL();
            oCommonResult = new CommonResult();
            oCommonResult = objLoanBLL.DeleteBankperformance(obj);
            return new JsonResult { Data = new { status = oCommonResult.Status } };
        }
        #endregion
        public JsonResult Finance(string loanId)
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = false;
            objLoan.IsRejected = false;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == false && a.CreatedBy == Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));
            var info = loans.FirstOrDefault(a => a.LoanId == loanId);
            //var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).ToList().Sum(x => x.Amount);
            //var netEmi = objLoanBLL.GetAdditionalLoanInfoById(loanId).ToList().Sum(x => x.ExistingEmi);
            //var netExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId).ToList().Sum(x => x.Amount);
            var netIncome = objLoanBLL.GetAllMonthlyIncomeInfo(loanId).FirstOrDefault();
            var netExpense = objLoanBLL.GetAllMonthlyExpenseInfo(loanId).FirstOrDefault();
            //var netIncome = 117050;
            //var netExpense = 60000;
            //var netEmi = 3450;
            return new JsonResult { Data = new { netIncome = netIncome, netExpense = netExpense, info = info } };
        }
        #region LoanApplication detils
        public void PrintLoanApplication(string loanId)
        {
            ReportObject.ReportName = "LoanApplication";
            ReportObject.LoanApplication = loanId;
            ReportObject.ReportFormat = "EC";
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }
        public void PrintLoanApproval(string loanNo)
        {
            ReportObject.ReportName = "LoanApprovalReport";
            ReportObject.LoanNo = loanNo;
            Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
        }
        #endregion
        #region
        public ActionResult RejectLoanList()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetrejectLoanList()
        {
            objLoan = new LoanModel();
            objLoanBLL = new LoanBLL();
            objLoan.IsApproved = false;
            objLoan.IsRejected = true;
            var loans = objLoanBLL.GetAllLoanInfo(objLoan).Where(a => a.IsChecked == true && a.IsApproved == false);
            return Json(new { data = loans }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReopenLoan(string loanId)
        {
            oCommonResult = new CommonResult();
            objLoanBLL = new LoanBLL();
            oCommonResult = objLoanBLL.ReopenLoan(loanId);
            return RedirectToAction("RejectLoanList");
            //return new JsonResult { Data = new { oCommonResult } };
        }
        #endregion
    }
}