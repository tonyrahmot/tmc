﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.ReportsViewer
{
    public static class ReportObject
    {
        public static string FromDate { get; set; }
        public static string ToDate { get; set; }
        public static string ReportType { get; set; }
        public static string ReportName { get; set; }
        public static string AccountNumber { get; set; }
        public static string CustomerId { get; set; }
        public static string BranchId { get; set; }
        public static int AccountType { get; set; }
        public static decimal? Ammount { get; set; }
        public static bool IsActive { get; set; }

        public static string LoanId { get; set; }
        public static string LoanNo { get; set; }
        public static string VoucherNo { get; set; }

        public static string AccountCode { get; set; }

        public static string LoanApplication { get; set; }
        public static string ReportFormat { get; set; }
    }
}