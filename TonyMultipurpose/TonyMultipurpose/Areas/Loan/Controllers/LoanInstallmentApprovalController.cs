﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
  [AuthenticationFilter]
    public class LoanInstallmentApprovalController : Controller
    {
        TermLoanBLL objTermLoanBLL;
        InstallmentInformation objInstallmentInformation;
        public ActionResult Index()
        {
            objInstallmentInformation = new InstallmentInformation();
            objTermLoanBLL = new TermLoanBLL();
            objInstallmentInformation.IsApproved = false;
            objInstallmentInformation.IsRejected = false;
            objInstallmentInformation.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            objInstallmentInformation.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
            List<InstallmentInformation> installmentInformation = objTermLoanBLL.GetInstallmentInformation(objInstallmentInformation);
            return View(installmentInformation);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            var ch = form.GetValues("Id");
            objTermLoanBLL = new TermLoanBLL();
            if (ch!=null)
            {
                objTermLoanBLL.ApproveInstallment(ch);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            objTermLoanBLL = new TermLoanBLL();
            List<InstallmentInformation> objList = new List<InstallmentInformation>();
            InstallmentInformation objI;
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    objI = new InstallmentInformation();
                    objI.Id = Convert.ToInt32(ids[i]);
                    objI.RejectReason = form.GetValues(objI.Id.ToString())[0];
                    objList.Add(objI);
                }
                objTermLoanBLL.RejectInstallment(objList);
            }
            return RedirectToAction("Index");
        }
    }
}