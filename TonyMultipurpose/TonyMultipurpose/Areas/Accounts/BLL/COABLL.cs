﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.DAL;


namespace TonyMultipurpose.Areas.Accounts.BLL
{
    public class COABLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private TransactionBLL objTransactionBll = new TransactionBLL();
        private void BuildModelForCOA(DbDataReader objDataReader, COA objCOA)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "COAId":
                        if (!Convert.IsDBNull(objDataReader["COAId"]))
                        {
                            objCOA.COAId = Convert.ToInt32(objDataReader["COAId"]);
                        }
                        break;
                    case "AccountCode":
                        if (!Convert.IsDBNull(objDataReader["AccountCode"]))
                        {
                            objCOA.AccountCode = objDataReader["AccountCode"].ToString();
                        }
                        break;
                    case "AccountName":
                        if (!Convert.IsDBNull(objDataReader["AccountName"]))
                        {
                            objCOA.AccountName = objDataReader["AccountName"].ToString();
                        }
                        break;
                    case "Description":
                        if (!Convert.IsDBNull(objDataReader["Description"]))
                        {
                            objCOA.Description = objDataReader["Description"].ToString();
                        }
                        break;
                    case "CategoryName":
                        if (!Convert.IsDBNull(objDataReader["CategoryName"]))
                        {
                            objCOA.CategoryName = objDataReader["CategoryName"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objCOA.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objCOA.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objCOA.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objCOA.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objCOA.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objCOA.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objCOA.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objCOA.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<COA> GetCOAList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objCOAList = new List<COA>();
            COA objCOA;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetCOAList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        //this.BuildModelForCOA(objDbDataReader, objCOA);
                        objCOA.COAId = Convert.ToInt32(objDbDataReader["COAId"]);
                        objCOA.GroupId = Convert.ToInt32(objDbDataReader["GroupId"]);
                        objCOA.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        objCOA.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                       
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["BalanceSheetName"])))
                        {
                            objCOA.BalanceSheetName = Convert.ToString(objDbDataReader["BalanceSheetName"]);
                        }
                        objCOA.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                        objCOA.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        objCOA.Description = Convert.ToString(objDbDataReader["Description"]);
                        objCOA.Debit = Convert.ToInt32(objDbDataReader["Debit"]);
                        objCOA.Credit = Convert.ToInt32(objDbDataReader["Credit"]);
                        objCOA.SupplierName = Convert.ToString(objDbDataReader["SupplierName"]);
                        objCOA.SupplierCompany = Convert.ToString(objDbDataReader["SupplierCompany"]);
                        objCOA.SupplierCode = Convert.ToString(objDbDataReader["SupplierCode"]);
                        objCOA.ClientName = Convert.ToString(objDbDataReader["ClientName"]);
                        objCOA.ClientCode = Convert.ToString(objDbDataReader["ClientCode"]);
                        objCOA.ClientCompany = Convert.ToString(objDbDataReader["ClientCompany"]);
                        objCOA.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        objCOA.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        objCOA.IsSubCode = Convert.ToBoolean(objDbDataReader["IsSubCode"]);
                        objCOA.SubCodeId = Convert.ToInt32(objDbDataReader["SubCodeId"]);
                        objCOA.Phone = Convert.ToString(objDbDataReader["Phone"]);
                        objCOA.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        objCOA.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                        objCOA.IsTransactional = Convert.ToBoolean(objDbDataReader["IsTransactional"]);
                        objCOA.IsBankAccount = Convert.ToBoolean(objDbDataReader["IsBankAccount"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["OpeningBalanceDate"])))
                        {
                            objCOA.OpeningBalanceDate = Convert.ToDateTime(objDbDataReader["OpeningBalanceDate"]);
                        }
                        
                        objCOAList.Add(objCOA);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOAList;
        }

        public List<COA> GetSubAccounts()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objCOAList = new List<COA>();
            COA objCOA;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetSubAccounts]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        //this.BuildModelForCOA(objDbDataReader, objCOA);
                        objCOA.COAId = Convert.ToInt16(objDbDataReader["COAId"]);
                        objCOA.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        objCOA.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        objCOA.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                        objCOA.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        objCOA.Description = Convert.ToString(objDbDataReader["Description"]);
                        objCOA.Debit = Convert.ToInt32(objDbDataReader["Debit"]);
                        objCOA.Credit = Convert.ToInt32(objDbDataReader["Credit"]);
                        objCOA.SupplierName = Convert.ToString(objDbDataReader["SupplierName"]);
                        objCOA.SupplierCompany = Convert.ToString(objDbDataReader["SupplierCompany"]);
                        objCOA.SupplierCode = Convert.ToString(objDbDataReader["SupplierCode"]);
                        objCOA.ClientName = Convert.ToString(objDbDataReader["ClientName"]);
                        objCOA.ClientCode = Convert.ToString(objDbDataReader["ClientCode"]);
                        objCOA.ClientCompany = Convert.ToString(objDbDataReader["ClientCompany"]);
                        objCOA.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        objCOA.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        objCOA.IsSubCode = Convert.ToBoolean(objDbDataReader["IsSubCode"]);
                        objCOA.SubCodeId = Convert.ToInt32(objDbDataReader["SubCodeId"]);
                        objCOA.Phone = Convert.ToString(objDbDataReader["Phone"]);
                        objCOA.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        objCOA.IsTransactional = Convert.ToBoolean(objDbDataReader["IsTransactional"]);
                        objCOA.IsBankAccount = Convert.ToBoolean(objDbDataReader["IsBankAccount"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["OpeningBalanceDate"])))
                        {
                            objCOA.OpeningBalanceDate = Convert.ToDateTime(objDbDataReader["OpeningBalanceDate"]);
                        }
                        objCOAList.Add(objCOA);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOAList;
        }
        public string SaveCOAInfo(COA objCOA)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountCode", objCOA.AccountCode);
            objDbCommand.AddInParameter("CompanyId", objCOA.CompanyId);
            objDbCommand.AddInParameter("AccountName", objCOA.AccountName);
            objDbCommand.AddInParameter("Description", objCOA.Description);
            objDbCommand.AddInParameter("CategoryId", objCOA.CategoryName);
            objDbCommand.AddInParameter("BalanceSheetId", objCOA.BalanceSheetId);
            objDbCommand.AddInParameter("BankAccountNo", objCOA.BankAccountNo);
            objDbCommand.AddInParameter("BankName", objCOA.BankName);
            objDbCommand.AddInParameter("BranchName", objCOA.BranchName);
            objDbCommand.AddInParameter("ClientCode", objCOA.ClientCode);
            objDbCommand.AddInParameter("ClientCompany", objCOA.ClientCompany);
            objDbCommand.AddInParameter("ClientName", objCOA.ClientName);
            objDbCommand.AddInParameter("IsSubCode", objCOA.IsSubCode);
            objDbCommand.AddInParameter("SubCodeId", objCOA.SubCodeId);
            objDbCommand.AddInParameter("SupplierCode", objCOA.SupplierCode);
            objDbCommand.AddInParameter("SupplierCompany", objCOA.SupplierCompany);
            objDbCommand.AddInParameter("SupplierName", objCOA.SupplierName);
            objDbCommand.AddInParameter("IsActive", objCOA.IsActive);
            objDbCommand.AddInParameter("GroupId", objCOA.GroupId);
            objDbCommand.AddInParameter("Debit", objCOA.Debit);
            objDbCommand.AddInParameter("Phone", objCOA.Phone);
            objDbCommand.AddInParameter("CPhone", objCOA.CPhone);
            objDbCommand.AddInParameter("Credit", objCOA.Credit);
            objDbCommand.AddInParameter("CreatedBy", objCOA.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objCOA.CreatedDate);
            objDbCommand.AddInParameter("IsTransactional", objCOA.IsTransactional);
            objDbCommand.AddInParameter("IsBankAccount", objCOA.IsBankAccount);
            objDbCommand.AddInParameter("OpeningBalanceDate", objCOA.OpeningBalanceDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCOAInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        public COA GetCOAInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            COA objCOA = new COA();

            try
            {
                objDbCommand.AddInParameter("COAId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCOAInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        //this.BuildModelForCOA(objDbDataReader, objCOA);
                        objCOA.COAId = Convert.ToInt16(objDbDataReader["COAId"]);
                        objCOA.GroupId = Convert.ToInt16(objDbDataReader["GroupId"]);
                        objCOA.GroupName = Convert.ToString(objDbDataReader["GroupName"]);
                        objCOA.COACategoryId = Convert.ToInt32(objDbDataReader["COACategoryId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["BalanceSheetId"])))
                        {
                            objCOA.BalanceSheetId = Convert.ToInt32(objDbDataReader["BalanceSheetId"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["BalanceSheetName"])))
                        {
                            objCOA.BalanceSheetName = Convert.ToString(objDbDataReader["BalanceSheetName"]);
                        }
                        //objCOA.BalanceSheetId = Convert.ToInt32(objDbDataReader["BalanceSheetId"]);
                        //objCOA.BalanceSheetName = Convert.ToString(objDbDataReader["BalanceSheetName"]);
                        objCOA.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        objCOA.AccountCode = Convert.ToString(objDbDataReader["AccountCode"]);
                        objCOA.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        objCOA.Description = Convert.ToString(objDbDataReader["Description"]);
                        objCOA.Debit = Convert.ToInt32(objDbDataReader["Debit"]);
                        objCOA.Credit = Convert.ToInt32(objDbDataReader["Credit"]);
                        objCOA.SupplierName = Convert.ToString(objDbDataReader["SupplierName"]);
                        objCOA.SupplierCompany = Convert.ToString(objDbDataReader["SupplierCompany"]);
                        objCOA.SupplierCode = Convert.ToString(objDbDataReader["SupplierCode"]);
                        objCOA.ClientName = Convert.ToString(objDbDataReader["ClientName"]);
                        objCOA.ClientCode = Convert.ToString(objDbDataReader["ClientCode"]);
                        objCOA.ClientCompany = Convert.ToString(objDbDataReader["ClientCompany"]);
                        objCOA.BankName = Convert.ToString(objDbDataReader["BankName"]);
                        objCOA.BankAccountNo = Convert.ToString(objDbDataReader["BankAccountNo"]);
                        objCOA.IsSubCode = Convert.ToBoolean(objDbDataReader["IsSubCode"]);
                        objCOA.SubCodeId = Convert.ToInt32(objDbDataReader["SubCodeId"]);
                        objCOA.Phone = Convert.ToString(objDbDataReader["Phone"]);
                        objCOA.CPhone = Convert.ToString(objDbDataReader["CPhone"]);
                        objCOA.BranchName = Convert.ToString(objDbDataReader["BranchName"]);
                        objCOA.IsActive = Convert.ToBoolean(objDbDataReader["IsActive"]);
                        objCOA.IsTransactional = Convert.ToBoolean(objDbDataReader["IsTransactional"]);
                        objCOA.IsBankAccount = Convert.ToBoolean(objDbDataReader["IsBankAccount"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(objDbDataReader["OpeningBalanceDate"])))
                        {
                            objCOA.OpeningBalanceDate = Convert.ToDateTime(objDbDataReader["OpeningBalanceDate"]);
                        }
                        objCOA.Balance = Convert.ToDecimal(objDbDataReader["Balance"]);
                        objCOA.BalanceInwords = objTransactionBll.GetConvertNumberToWord(Convert.ToInt64(objCOA.Balance));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCOA;
        }

        public COA GetAccountCodeBalanceInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            COA objCOA = new COA();

            try
            {
                objDbCommand.AddInParameter("COAId", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[accounts_getAccountCodeWiseBalance]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        objCOA.Credit = Convert.ToInt32(objDbDataReader["Balance"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCOA;
        }

        public string UpdateCOAInfo(COA objCOA)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("COAId", objCOA.COAId);
            objDbCommand.AddInParameter("GroupId", objCOA.GroupId);
            objDbCommand.AddInParameter("CategoryName", objCOA.CategoryName);
            objDbCommand.AddInParameter("BalanceSheetId", objCOA.BalanceSheetId);
            objDbCommand.AddInParameter("AccountName", objCOA.AccountName);
            objDbCommand.AddInParameter("IsSubCode", objCOA.IsSubCode);
            objDbCommand.AddInParameter("SubCodeId", objCOA.SubCodeId);
            objDbCommand.AddInParameter("BankName", objCOA.BankName);
            objDbCommand.AddInParameter("BranchName", objCOA.BranchName);
            objDbCommand.AddInParameter("BankAccountNo", objCOA.BankAccountNo);
            objDbCommand.AddInParameter("SupplierName", objCOA.SupplierName);
            objDbCommand.AddInParameter("SupplierCode", objCOA.SupplierCode);
            objDbCommand.AddInParameter("SupplierCompany", objCOA.SupplierCompany);
            objDbCommand.AddInParameter("Phone", objCOA.Phone);
            objDbCommand.AddInParameter("ClientName", objCOA.ClientName);
            objDbCommand.AddInParameter("ClientCode", objCOA.ClientCode);
            objDbCommand.AddInParameter("ClientCompany", objCOA.ClientCompany);
            objDbCommand.AddInParameter("CPhone", objCOA.CPhone);
            objDbCommand.AddInParameter("AccountCode", objCOA.AccountCode);
            objDbCommand.AddInParameter("Description", objCOA.Description);
            objDbCommand.AddInParameter("UpdatedBy", objCOA.UpdatedBy);
            objDbCommand.AddInParameter("UpdatedDate", objCOA.UpdatedDate);
            objDbCommand.AddInParameter("Debit", objCOA.Debit);
            objDbCommand.AddInParameter("Credit", objCOA.Credit);
            objDbCommand.AddInParameter("IsActive", objCOA.IsActive);
            objDbCommand.AddInParameter("IsTransactional", objCOA.IsTransactional);
            objDbCommand.AddInParameter("IsBankAccount", objCOA.IsBankAccount);
            objDbCommand.AddInParameter("OpeningBalanceDate", objCOA.OpeningBalanceDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspUpdateCOAInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteCOA(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("COAId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteCOA]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Deleted Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public List<COACategory> GetCategoryNameGroupWise( string groupId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COACategory> objCOACategoryList = new List<COACategory>();
            COACategory objCOACategory;
            try
            {
                objDbCommand.AddInParameter("GroupId", groupId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetCategoryNameGroupWise", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOACategory = new COACategory();
                        objCOACategory.Id = Convert.ToInt16(objDbDataReader["Id"]);
                        objCOACategory.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupId"]);
                        //objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupName"]);

                        objCOACategoryList.Add(objCOACategory);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOACategoryList;
        }

        public List<COA> GetSubCodeCategoryNameGroupWise(string groupId,string categoryName)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objCOAList = new List<COA>();
            COA objCOA;
            try
            {
                objDbCommand.AddInParameter("GroupId", groupId);
                objDbCommand.AddInParameter("COACategoryId", categoryName);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetSubCodeCategoryNameGroupWise", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();
                        objCOA.AccountName = Convert.ToString(objDbDataReader["AccountName"]);
                        objCOA.COAId = Convert.ToInt16(objDbDataReader["COAId"]);
                        //objCOA.BalanceSheetId = Convert.ToInt32(objDbDataReader["BalanceSheetId"]);
                        //objCOA.BalanceSheetName = objDbDataReader["BalanceSheetName"].ToString();
                        //objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupName"]);

                        objCOAList.Add(objCOA);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOAList;
        }

        public List<COA> GetBalanceSheetNameByGroupAndCategoryId(string groupId, string categoryName)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> objCOAList = new List<COA>();
            COA objCOA;
            try
            {
                objDbCommand.AddInParameter("GroupId", groupId);
                objDbCommand.AddInParameter("COACategoryId", categoryName);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].FT_GetBalanceSheetInfoByGroupAndCategoryId", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOA = new COA();

                        objCOA.BalanceSheetId = Convert.ToInt32(objDbDataReader["BalanceSheetId"]);
                        objCOA.BalanceSheetName = objDbDataReader["BalanceSheetName"].ToString();


                        objCOAList.Add(objCOA);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOAList;
        }




        public bool GetAccountNameCheck(string AccountName)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountName", AccountName);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[COA_AccountNameCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetAccountCodeCheck(string AccountCode)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountCode", AccountCode);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[COA_AccountCodeCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

    }
}