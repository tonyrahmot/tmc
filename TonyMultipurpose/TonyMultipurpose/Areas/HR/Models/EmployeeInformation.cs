﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class EmployeeInformation : CommonModel
    {
        public int EmpId { get; set; }

        public string EmpCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string EmployeeName { get; set; }

        public int DesigId { get; set; }

        public int FunctionalId { get; set; }

        public int DeptId { get; set; }

        public decimal? CurrentSalary { get; set; }

        public string JoiningDate { get; set; }

        public string DOB { get; set; }

        public string Religion { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string NID { get; set; }

        public string EmailId { get; set; }

        public string MobileNo { get; set; }

        public string PresentAddress { get; set; }

        public string PermanentAddress { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public string ImagePath { get; set; }

        public string OldImagePath { get; set; }

    }
}