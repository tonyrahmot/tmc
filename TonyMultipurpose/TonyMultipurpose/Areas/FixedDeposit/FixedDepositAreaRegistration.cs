﻿using System.Web.Mvc;

namespace TonyMultipurpose.Areas.FixedDeposit
{
    public class FixedDepositAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FixedDeposit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FixedDeposit_default",
                "FixedDeposit/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}