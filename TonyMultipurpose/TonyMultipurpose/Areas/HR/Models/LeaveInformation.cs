﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class LeaveInformation:CommonModel
    {
        [Key]
        public int LeaveId { get; set; }
        public int LTypeId { get; set; }
        public string LeaveTypeName { get; set; }
        public string EmpId { get; set; }
        public string EmpCode { get; set; }
        public string EmployeeName { get; set; }
        public int LeaveDays { get; set; }
        public string Description { get; set; }
        public DateTime FromDate { get; set; }
        public string FromDateShow { get; set; }
        public DateTime ToDate { get; set; }
        public string ToDateShow { get; set; }
        public string Holiday { get; set; }
    }
}