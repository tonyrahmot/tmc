﻿
$(document).ready(function () {
    //clicking the parent checkbox should check or uncheck all child checkboxes
    $(".parent").click(function () {

        $(this).closest('fieldset').find('.child').prop('checked', this.checked);
    }
    );
    //clicking the last unchecked or checked checkbox should check or uncheck the parent checkbox
    $('.child').click(
        function () {
            if (this.checked == true) {
                var flag = true;
                $(this).closest('fieldset').find('.child').one(
                    function () {
                        if (this.CheckUncheckChildren == true)
                            flag = false;
                    }
                );
                $(this).closest('fieldset').find('.parent').prop('checked', flag);
            }
        }
    );
});

    $(document).ready(function () {


        $('#BranchId').on('change', function () {

            var branchId = $('#BranchId').val();
            $.ajax({
                url: window.applicationBaseUrl + "UserMenuMapping/GetUserByBranchId",//'@Url.Action("GetUserByBranchId", "UserMenuMapping")',
                type: "POST",
                data: { branchId: branchId },
                dataType: "json",
                traditional: true,
                success: function (d) {
                    $("#UserDetailId").html(""); // clear before appending new list
                    $("#UserDetailId").append(
                            $('<option></option>').val(0).html("--Select User Name--"));
                    $.each(d, function (i, u) {
                        $("#UserDetailId").append(
                            $('<option></option>').val(u.UserDetailId).html(u.Username));
                    });
                }
            });
        });
        ////

        $('#UserDetailId').on('change', function () {

            var userDetailId = $('#UserDetailId').val();
            $.ajax({
                url: window.applicationBaseUrl + "UserMenuMapping/GetRoleByUserId",//'@Url.Action("GetRoleByUserId", "UserMenuMapping")',
                type: "POST",
                data: { userDetailId: userDetailId },
                dataType: "json",
                traditional: true,
                success: function (d) {
                    $("#RoleId").html(""); // clear before appending new list
                    $.each(d, function (i, u) {
                        $("#RoleId").append(
                            $('<option></option>').val(u.RoleId).html(u.RoleName));
                    });

                    $('#RoleId').change();
                }
            });
        });

        $('#AssignUserMenu').on('click', function () {
            var prices = [];
            var roleId = $('#RoleId').val();
            var userId = $('#UserDetailId').val();
            $('input:checked').each(function () {

                prices.push($(this).attr("value"));
            });
            $.ajax({
                url: window.applicationBaseUrl + "UserMenuMapping/Save",//'@Url.Action("Save", "UserMenuMapping")',
                type: "POST",
                data: { menuId: prices, roleId: roleId, userId: userId },
                dataType: "json",
                traditional: true,
                success: function (d) {
                    if (d.status == true) {
                        alert("Assign Successfully");
                        location.reload();
                    } else {
                        alert('Error. Please try again.');
                    }
                }
            });
        });
    });

    