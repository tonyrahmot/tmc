﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Accounts.BLL
{
    public class COACategoryBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;



        private void BuildModelForCOACategory(DbDataReader objDataReader, COACategory objCOACategory)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objCOACategory.Id = Convert.ToInt32(objDataReader["Id"]);
                        }
                        break;
                    case "CategoryName":
                        if (!Convert.IsDBNull(objDataReader["CategoryName"]))
                        {
                            objCOACategory.CategoryName = objDataReader["CategoryName"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<COACategory> GetCOACategoryList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COACategory> objCOACategoryList = new List<COACategory>();
            COACategory objCOACategory;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetCOACategoryList", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOACategory = new COACategory();
                        objCOACategory.Id = Convert.ToInt16(objDbDataReader["Id"]);
                        objCOACategory.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupId"]);
                        objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupName"]);
                        
                        objCOACategoryList.Add(objCOACategory);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objCOACategoryList;
        }

        public string SaveCOACategoryInfo(COACategory objCOACategory)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("CategoryName", objCOACategory.CategoryName);
            objDbCommand.AddInParameter("GroupId", objCOACategory.GroupId);
            //objDbCommand.AddInParameter("CreatedBy", objCOACategory.CreatedBy);
            //objDbCommand.AddInParameter("CreatedDate", objCOACategory.CreatedDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCOACategoryInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        public COACategory GetCOACategoryInfo(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            COACategory objCOACategory = new COACategory();

            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCOACategoryInfoById]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCOACategory = new COACategory();
                        //this.BuildModelForCOACategory(objDbDataReader, objCOACategory);
                        objCOACategory.Id = Convert.ToInt16(objDbDataReader["Id"]);
                        objCOACategory.CategoryName = Convert.ToString(objDbDataReader["CategoryName"]);
                        objCOACategory.GroupId = Convert.ToString(objDbDataReader["GroupId"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCOACategory;
        }

        public string UpdateCOACategoryInfo(COACategory objCOACategory)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", objCOACategory.Id);
            objDbCommand.AddInParameter("CategoryName", objCOACategory.CategoryName);
            objDbCommand.AddInParameter("GroupId", objCOACategory.GroupId);
            //objDbCommand.AddInParameter("UpdatedDate", objCOACategory.UpdatedDate);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspUpdateCOACategoryInfo]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Updated Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Update Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteCOACategory(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("Id", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspDeleteCOACategory]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Deleted Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public bool GetCategoryNameCheck(string CategoryName)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("CategoryName", CategoryName);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[COACategory_CategoryNameCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
    }
}