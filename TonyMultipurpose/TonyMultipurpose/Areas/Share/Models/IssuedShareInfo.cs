using System;

namespace TonyMultipurpose.Areas.Share.Models
{
    public class IssuedShareInfo
    {
        public int Id { get; set; }
        public int? AuthorizedShareInfoId { get; set; }
        public string EntryType { get; set; }
        public decimal? IssuedQty { get; set; }
        public decimal? PricePerShare { get; set; }
        public decimal? IssuedAmount { get; set; }
        public decimal? SoldQty { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? IssuedDate { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsRejected { get; set; }
        public string RejectReason { get; set; }
        public string Particular { get; set; }
        public short? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public short? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}