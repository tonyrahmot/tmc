﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.CSS.BLL;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.CSS.Controllers
{
    [AuthenticationFilter]
    public class CSSController : Controller
    {
        // GET: CSS/CSS

        CSSBLL objCssBll = new CSSBLL();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult AutoCompleteAccountNumber(string term)
        {
            var result = objCssBll.GetAllAccountNumberForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objCssBll.GetAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllAccountInfoByAccountNo(string AccountNumber)
        {
            var data = objCssBll.GetAllAccountInfoByAccountNo(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(List<CSSModel> cssModel)
        {
            bool status = false;
            if (ModelState.IsValid)
            {               
                objCssBll.SaveCSSTransaction(cssModel);
                status = true;
            }            
            return new JsonResult { Data = new { status = status } };
        }
    }
}