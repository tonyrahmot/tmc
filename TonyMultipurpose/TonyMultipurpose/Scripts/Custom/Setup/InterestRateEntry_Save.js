﻿$('.datepicker').datepicker({
    dateFormat: "dd/M/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+0"
});

//Load Account Type Dropdown
$("#AccountTitle").change(function () {
    var AccountTitle = $("#AccountTitle").val();
    $("#AccountType").empty();
    var json = {
        AccountTitle: AccountTitle
    };
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Setup/InterestRateEntry/GetAccontTypeByAccountTitle",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(json),
        success: function (data) {
            $("#AccountType").append('<option value="0">--Select Type--</option>');
            $.each(data, function (key, value) {
                $("#AccountType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
            });
        }
    });
});

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode == 13) {
        return false;
    }
    status = "";
    return true;
}