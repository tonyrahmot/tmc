﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    public class Training : CommonModel
    {
        [Key]
        public short TrainingId { get; set; }

        [Required(ErrorMessage ="Employee name is required")]
        [Display(Name ="Employee Name")]
        public short EmployeeId { get; set; }

       // public IEnumerable<Employee> EmployeeInfo { get; set; }
        public string EmployeeName { get; set; }

        [Required]
        [Display(Name ="Organization Name")]
        [StringLength(150,ErrorMessage ="Organization name must be geter then 3 and less then 150",MinimumLength =3)]
        [DataType(DataType.Text)]
        public string OrganizationName { get; set; }

        [Required]
        [Display(Name = "Training Title")]
        [StringLength(150, ErrorMessage = "Training Title  must be geter then 3 and less then 150", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string TrainingTitle { get; set; }


        [Required]
        [Display(Name = "Training Duration")]
        [StringLength(150, ErrorMessage = "Training Duration must be geter then 3 and less then 150", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string TrainingDuration { get; set; }

        [Required]
        [Display(Name = "Training Year")]              
        public string TrainingYear { get; set; }
        
        public string TrainingYearShow { get; set; }

    }
}