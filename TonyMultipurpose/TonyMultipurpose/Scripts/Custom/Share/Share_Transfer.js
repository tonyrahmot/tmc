﻿$(document).ready(function () {
    shareTransferCustomerAutoComplete("FromCustDisplayId", "T", "ToCustomerId", "FromCustomerId");
    shareTransferCustomerAutoComplete("ToCustDisplayId", "R", "FromCustomerId", "ToCustomerId");

    
    $('#shareTransfer').on('submit', function (e) {
        
        if (checkFormIsValid() === false) {
            e.preventDefault();
        }
    });
});



function shareTransferCustomerAutoComplete(controlId, customerType, excludeDataSource, mainControlId) {
//    var excludeCustId = $("#" + excludeDataSource).val();
    $("#" + controlId).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + 'Share/ShareTransfer/AutoCompleteCustomerId',
                type: "GET",
                dataType: "json",
                data: { term: request.term, cType: customerType, excludeCustId: $("#" + excludeDataSource).val() },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        select: function (event, ui) {
            var customerId = ui.item.value;
            $(this).val(customerId);
            $("#" + mainControlId).val(customerId);
            loadShareTransferCustomer(customerId, customerType, $("#" + excludeDataSource).val()); //excludeCustId
            return false;
        }
    });
}

function loadShareTransferCustomer(customerId, customerType, excludeCustId) {
    console.log(excludeCustId);
    var customerUrl = window.applicationBaseUrl + 'Share/ShareTransfer/GetCustomerInfo/' + customerId + '/?cType=' + customerType + '&&excludeCustId=' + excludeCustId;
    if (customerType === 'T') {
        $("#fromCustomerInfo").load(customerUrl);
    } else if (customerType === 'R') {
        $("#toCustomerInfo").load(customerUrl);
    }
}

function checkFormIsValid() {
    if ($("#FromCustomerId").val() === $("#ToCustomerId").val()) {
        toastr.error("Transfer cann't be occured between same person");
        return false;
    }
    return true;
}