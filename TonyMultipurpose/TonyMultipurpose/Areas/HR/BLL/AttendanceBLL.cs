﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.BLL
{
    public class AttendanceBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public List<AttendanceModel> GetActiveEployeeInformation()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            AttendanceModel objAttendanceModel;
            List<AttendanceModel> objAttendanceModelList = new List<AttendanceModel>();
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetActiveEployeeInformation]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAttendanceModel = new AttendanceModel();
                        objAttendanceModel.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objAttendanceModel.Designation = objDbDataReader["Designation"].ToString();
                        objAttendanceModel.EmpId = Convert.ToInt32(objDbDataReader["EmpId"].ToString());
                        objAttendanceModelList.Add(objAttendanceModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAttendanceModelList;
        }
        public List<AttendanceModel> GetAllAttendenceInfoByAttendanceDate(string AttendanceDate)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            AttendanceModel objAttendanceModel;
            List<AttendanceModel> objAttendanceModelList = new List<AttendanceModel>();
            objDbCommand.AddInParameter("AttendanceDate", AttendanceDate);
            try
            {

                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_GetAllAttendenceInfoByAttendanceDate]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAttendanceModel = new AttendanceModel();
                        objAttendanceModel.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objAttendanceModel.Designation = objDbDataReader["Designation"].ToString();
                        objAttendanceModel.EmpId = Convert.ToInt32(objDbDataReader["EmpId"].ToString());
                        objAttendanceModel.InTime = objDbDataReader["InTime"].ToString();
                        objAttendanceModel.OutTime = objDbDataReader["OutTime"].ToString();
                        objAttendanceModel.Comments = objDbDataReader["Comments"].ToString();
                        objAttendanceModel.AttendanceStatus = objDbDataReader["AttendanceStatus"].ToString();
                        objAttendanceModelList.Add(objAttendanceModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAttendanceModelList;
        }
        public bool ExistDateAndWiseDeleteFromAttendence(DateTime attendanceDate)
        {
            int noOfAffacted = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            bool IsExist = false;

            objDbCommand.AddInParameter("attendanceDate", attendanceDate);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_ExistDateAndWiseDeleteFromAttendence]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                     IsExist = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return IsExist;
        }
        public bool ExistDateAndWiseAttendence(int empId, DateTime attendanceDate)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            bool IsExist = false;

            objDbCommand.AddInParameter("empId", empId);
            objDbCommand.AddInParameter("attendanceDate", attendanceDate);
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[hr_ExistDateAndWiseAttendence]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    IsExist = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return IsExist;
        }
        public CommonResult SaveEmployeeAttendenceInformation(List<AttendanceModel> objAttendanceModel)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            
            try
            {

                foreach (var item in objAttendanceModel)
                {
                    objDataAccess = DataAccess.NewDataAccess();
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("AttendanceStatus", item.AttendanceStatus);
                    objDbCommand.AddInParameter("AttendanceDate", item.AttendanceDate);
                    objDbCommand.AddInParameter("EmpId", item.EmpId);
                    objDbCommand.AddInParameter("InTime", item.InTime);
                    objDbCommand.AddInParameter("OutTime", item.OutTime);
                    objDbCommand.AddInParameter("Comments", item.Comments);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[hr_SaveEmployeeAttendenceInformation]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //objDataAccess.Dispose(objDbCommand);
                        oCommonResult.Status = true;
                        oCommonResult.Message = "Successful";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        objDataAccess.Dispose(objDbCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return oCommonResult;
        }
    }
}