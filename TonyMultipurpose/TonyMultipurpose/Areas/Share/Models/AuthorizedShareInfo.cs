﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Share.Models
{
    public class AuthorizedShareInfo
    {
        public long Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string EntryType { get; set; }
        public decimal? AuthorizedAmount { get; set; }
        public decimal? NoOfShare { get; set; }
        public decimal? PricePerShare { get; set; }
        public string Particular { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsRejected { get; set; }
        public string RejectReason { get; set; }
        public short? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public short? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string TransactionId { get; set; }
        public int CompanyId { get; set; }
        public int BranchId { get; set; }
    }
}