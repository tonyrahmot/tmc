﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace TonyMultipurpose.Areas.TermLoan.BLL
{
    public class CommonGetway
    {
        string Connection = WebConfigurationManager.ConnectionStrings["TonyMultipurposeDB"].ConnectionString.ToString();
        public SqlConnection objConnection { get; set; }
        public SqlCommand objCommand { get; set; }
        public SqlDataReader objDataReader { get; set; }
        public SqlDataAdapter objDataAdapter { get; set; }

        public DataTable dt { get; set; }
        public CommonGetway()
        {
            objConnection = new SqlConnection(Connection);
        }
    }
}