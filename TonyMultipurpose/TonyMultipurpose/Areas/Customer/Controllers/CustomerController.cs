﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Customer.BLL;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Customer.Controllers
{
    [AuthenticationFilter]
    public class CustomerController : Controller
    {
        private CustomerBLL objCustomerBll = new CustomerBLL();
        // GET: Customer/Customer
        #region Customer Table All Data Info
        public ActionResult Index(string search = null)
        {
            CustomerModel objCustomerModel = new CustomerModel();
            objCustomerModel.IsApproved = true;
            objCustomerModel.IsDeleted = false;
            List<CustomerModel> customerInfo = objCustomerBll.GetAllCustomerListInfo(objCustomerModel, search);
            return View(customerInfo);
        }
        #endregion Customer Table All Data Info

        #region Insert Into Data Customer Table
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CustomerModel objCustomerModel)
        {
            var path = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                // CustomerModel objCustomerModel = new CustomerModel();
                if (objCustomerModel.CustomerId == "< New Entry >")
                {
                    objCustomerModel.CustomerId = objCustomerBll.GenerateCustomerId();
                }
                objCustomerModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objCustomerModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);

                if (objCustomerModel.CustomerImage == "Cersi")
                {
                    var rootPath = "~\\Uploads\\Customer\\";
                    string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                    if (!Directory.Exists(rootFolderPath))
                    {
                        Directory.CreateDirectory(rootFolderPath);

                    }
                    path = Path.Combine(rootFolderPath, string.Concat(objCustomerModel.CustomerId, ".jpg"));
                    string filesToDelete = objCustomerModel.CustomerId + ".jpg";
                    string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                    foreach (string file in fileList)
                    {
                        System.IO.File.Delete(file);
                    }
                    objCustomerModel.CustomerImage = path;
                }
                var c = objCustomerBll.SaveCustomer(objCustomerModel);
                if (c == "Saved")
                {
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status, Id = objCustomerModel.CustomerId, image = objCustomerModel.CustomerImage } };
        }

        [HttpPost]
        public JsonResult SaveImageFile(CustomerModel objCustomerModel)
        {
            bool status = false;
            try
            {
                objCustomerModel.Imagefile.SaveAs(objCustomerModel.CustomerImage);
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
            }

            var obj = new { status = status, image = objCustomerModel.CustomerImage };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion Insert Into Data Customer Table

        #region Update Data Customer Table
        [HttpGet]
        public ActionResult Edit(string id)
        {
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = true;
            customer.IsDeleted = false;
            customer = objCustomerBll.GetAllCustomerListInfo(customer).FirstOrDefault(a => a.CustomerId == id);
            var customerBankDetails = objCustomerBll.GetCustomerWiseBankInfo(id);
            if (customerBankDetails != null)
            {
                foreach (var customerBank in customerBankDetails)
                {
                    customer.CustomerBankDetails.Add(customerBank);
                }
            }
            if (customer != null)
            {
                return View(customer);
            }
            else
            {
                return View("Create");
            }
        }

        [HttpPost]
        public ActionResult Edit(CustomerModel objCustomerModel)
        {
            var path = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                objCustomerModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objCustomerModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objCustomerModel.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objCustomerModel.IsApproved = true;

                if (objCustomerModel.OldImage == "arya" || objCustomerModel.OldImage == "Cersi")
                {
                    if (objCustomerModel.CustomerImage == "Cersi")
                    {
                        var rootPath = "~\\Uploads\\Customer\\";
                        string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                        if (!Directory.Exists(rootFolderPath))
                        {
                            Directory.CreateDirectory(rootFolderPath);
                        }
                        path = Path.Combine(rootFolderPath, string.Concat(objCustomerModel.CustomerId, ".jpg"));
                        string filesToDelete = objCustomerModel.CustomerId + ".jpg";
                        string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                        foreach (string file in fileList)
                        {
                            System.IO.File.Delete(file);
                        }
                        objCustomerModel.CustomerImage = path;
                    }
                }
                else
                {
                    if (objCustomerModel.CustomerImage == "Cersi")
                    {
                        string customerImageName = string.Empty;
                        string currentFolderPath = string.Empty;
                        if (!string.IsNullOrEmpty(objCustomerModel.OldImage))
                        {
                            currentFolderPath = System.IO.Path.GetDirectoryName(objCustomerModel.OldImage.ToString());
                        }
                        if (!string.IsNullOrEmpty(currentFolderPath))
                        {
                            //var rootPath = "~\\Uploads\\Customer\\";
                            if (Directory.Exists(currentFolderPath))
                            {
                                string[] imageFiles = (Directory.GetFiles(currentFolderPath, "*.JPG"));
                                List<string> imgFiles = imageFiles.Select(imagePath => Path.GetFileNameWithoutExtension(imagePath)).ToList();
                                int imgCount = imgFiles.Count(i => i.StartsWith(objCustomerModel.CustomerId));
                                if (imgCount > 0)
                                {
                                    customerImageName = string.Concat(objCustomerModel.CustomerId, "V",
                                        (imgCount + 1).ToString().PadLeft(2, '0'), ".jpg");
                                    path = Path.Combine(currentFolderPath, customerImageName);
                                }
                            }
                            else
                            {
                                var rootPath = "~\\Uploads\\Customer\\";
                                string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                                if (!Directory.Exists(rootFolderPath))
                                {
                                    Directory.CreateDirectory(rootFolderPath);
                                }
                                path = Path.Combine(rootFolderPath, string.Concat(objCustomerModel.CustomerId, ".jpg"));
                                string filesToDelete = objCustomerModel.CustomerId + ".jpg";
                                string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                                foreach (string file in fileList)
                                {
                                    System.IO.File.Delete(file);
                                }
                            }
                            objCustomerModel.CustomerImage = path;
                        }
                    }
                }
                var c = objCustomerBll.UpdateCustomer(objCustomerModel);
                if (c == "Saved")
                {
                    status = true;
                }
                else
                {
                    status = false;
                }
                
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = objCustomerModel.CustomerId, image = objCustomerModel.CustomerImage } };
        }

        [HttpPost]
        public ActionResult SaveUpdate(CustomerModel objCustomerModel)
        {
            var path = string.Empty;
            bool status = false;
            if (ModelState.IsValid)
            {
                objCustomerModel.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                objCustomerModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objCustomerModel.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objCustomerModel.IsApproved = false;

                if (objCustomerModel.OldImage == "arya" || objCustomerModel.OldImage == null)
                {
                    if (objCustomerModel.CustomerImage == "Cersi")
                    {
                        var rootPath = "~\\Uploads\\Customer\\";
                        string rootFolderPath = Server.MapPath(string.Concat(rootPath, DateTime.Now.Year.ToString(), "\\", DateTime.Now.ToString("MMM"), "\\"));
                        if (!Directory.Exists(rootFolderPath))
                        {
                            Directory.CreateDirectory(rootFolderPath);
                        }
                        path = Path.Combine(rootFolderPath, string.Concat(objCustomerModel.CustomerId, ".jpg"));
                        string filesToDelete = objCustomerModel.CustomerId + ".jpg";
                        string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                        foreach (string file in fileList)
                        {
                            System.IO.File.Delete(file);
                        }
                        objCustomerModel.CustomerImage = path;
                    }
                }
                else
                {
                    if (objCustomerModel.CustomerImage == "Cersi")
                    {
                        string customerImageName = string.Empty;
                        string currentFolderPath = string.Empty;
                        if (!string.IsNullOrEmpty(objCustomerModel.OldImage))
                        {
                            currentFolderPath = System.IO.Path.GetDirectoryName(objCustomerModel.OldImage.ToString());
                        }
                        if (!string.IsNullOrEmpty(currentFolderPath))
                        {
                            string[] imageFiles = (Directory.GetFiles(currentFolderPath, "*.JPG"));
                            List<string> imgFiles = imageFiles.Select(imagePath => Path.GetFileNameWithoutExtension(imagePath)).ToList();
                            int imgCount = imgFiles.Count(i => i.StartsWith(objCustomerModel.CustomerId));
                            if (imgCount > 0)
                            {
                                customerImageName = string.Concat(objCustomerModel.CustomerId, "V",
                                    (imgCount + 1).ToString().PadLeft(2, '0'), ".jpg");
                                path = Path.Combine(currentFolderPath, customerImageName);
                            }
                            objCustomerModel.CustomerImage = path;
                        }
                    }
                }
                var c = objCustomerBll.UpdateCustomer(objCustomerModel);
                status = true;
            }
            else
            {
                status = false;
            }
            return new JsonResult { Data = new { status = status, Id = objCustomerModel.CustomerId, image = objCustomerModel.CustomerImage } };
        }
        #endregion Update Data Customer Table

        #region Delete Data Customer Table
        //public ActionResult Delete(int id)
        //{
        //    objCustomerBll.DeleteCustomer(id);
        //    return RedirectToAction("Index");
        //}
        #endregion Delete Data Customer Table

        #region Auto Load All CustomerId From Customer Table 
        [HttpGet]
        public JsonResult AutoCompleteCustomerId(string term)
        {
            //objCustomerBll = new CustomerBLL();
            var result = objCustomerBll.GetAllCustomerIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion Auto Load All CustomerId From Customer Table 

        #region Customer Detail Info
        public ActionResult CustomerDetail(string id)
        {
            CustomerModel customer = new CustomerModel();
            customer.IsApproved = true;
            customer.IsDeleted = false;
            customer = objCustomerBll.GetAllCustomerListInfo(customer).Where(a => a.CustomerId == id).FirstOrDefault();
            var customerBankDetails = objCustomerBll.GetCustomerWiseBankInfo(id);
            if (customerBankDetails != null)
            {
                foreach (var customerBank in customerBankDetails)
                {
                    customer.CustomerBankDetails.Add(customerBank);
                }
            }
            return View(customer);
        }
        #endregion Customer Detail Info
        public ActionResult MenuEdit()
        {
            return View();
        }
        #region print Customer Detail 
        public ActionResult CreatePDF(string id, string name)
        {
            var root = Server.MapPath("~/PDF/");
            var pdfname = String.Format("{0}.pdf", name);
            //return new Rotativa.MVC.ActionAsPdf("PrintCustomerDetail", new { id = id })
            //{
            //    FileName = pdfname
            //};
            CustomerModel customer = new CustomerModel();
            customer.CustomerId = id;
            customer.IsApproved = true;
            customer.IsDeleted = false;
            customer.dtCommon = objCustomerBll.GetAllCustomerInfoForFDF(customer);
            return new Rotativa.MVC.ViewAsPdf("PrintCustomerDetail", customer);
        }
        public ActionResult PrintCustomerDetail(string id)
        {
            CustomerModel customer = new CustomerModel();
            customer.CustomerId = id;
            customer.IsApproved = true;
            customer.IsDeleted = false;
            customer.dtCommon = objCustomerBll.GetAllCustomerInfoForFDF(customer);
            return View(customer);
        }
        #endregion print Customer Detail 

        #region Duplicate NID Check For Customer
        public JsonResult GetNIDCheck(string NID)
        {
            bool exsits = objCustomerBll.GetNIDCheck(NID);
            if (exsits) { return Json(false, JsonRequestBehavior.AllowGet); }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion Duplicate NID Check For Customer
    }
}