﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Models
{
    //--ataur
    public class RoleInfo:CommonModel
    {
        [Key]
        public int RoleId { get; set; }

        [Display(Name = "Role Name")]
        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Role Name Is Required !")]
        [StringLength(100, ErrorMessage = "Must be between 2 and 100 characters long !", MinimumLength = 2)]
        [RegularExpression("([a-zA-Z  ]+)", ErrorMessage = "Enter Valid Role Name !")]
        public string RoleName { get; set; }

        public IEnumerable<Company> Companies { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        
    }
}