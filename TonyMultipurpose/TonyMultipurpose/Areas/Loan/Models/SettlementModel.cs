﻿using System;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class SettlementModel:CommonModel
    {
        public long SettlementId { get; set; }
        public string LoanNo { get; set; }
        public string LoanId { get; set; }
        public short SettlementTypeId { get; set; }
        public string SettlementTypeName { get; set; }
        public string SettlementDate { get; set; }
        public string SettlementDateShow { get; set; }
        public string Reason { get; set; }
        public bool? IsRejected { get; set; }
        public short BranchId { get; set; }
        public short CompnayId { get; set; }
        public string CreatedName { get; set; }
        public decimal CurrentInterest { get; set; }
        public decimal CurrentInstallmentAmount { get; set; }
        public decimal UpdatedInterest { get; set; }
        public decimal UpdatedInstallmentAmount { get; set; }
        public decimal BeginningBalance { get; set; }
        public decimal EndingBalance { get; set; }
        public decimal Principle { get; set; }
        public decimal Interest { get; set; }
        public int InstallmentNo { get; set; }

    }
}