﻿using System.Web.Mvc;

namespace TonyMultipurpose.Areas.DoubleBenefit
{
    public class DoubleBenefitAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DoubleBenefit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DoubleBenefit_default",
                "DoubleBenefit/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}