﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.BLL
{
    public class ReportsBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public DataTable CustomerEntryReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CustomerId", ReportObject.CustomerId);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptCustomerEntryReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public List<AccountTypeSetup> GetAccontTypeByAccountTitle()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AccountTypeSetup> objAccountTypeSetupList = new List<AccountTypeSetup>();
            AccountTypeSetup objAccountTypeSetup = new AccountTypeSetup();
            try
            {
                objDbCommand.AddInParameter("AccountTitle", "Deposit");
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountypeByTitle]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objAccountTypeSetup = new AccountTypeSetup();
                        if (objDbDataReader["AccountSetupId"] != null)
                        {
                            objAccountTypeSetup.AccountSetupId = Convert.ToInt16(objDbDataReader["AccountSetupId"]);
                            objAccountTypeSetup.AccountTypeName = Convert.ToString(objDbDataReader["AccountTypeName"]);
                            objAccountTypeSetupList.Add(objAccountTypeSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountTypeSetupList;
        }

        public DataTable AccountNumberWiseReport()
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptAccountNumberWiseReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public List<string> GetAllCustomerIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objCustomerList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("CustomerId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCustomerIdForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["CustomerId"] != null)
                        {
                            objCustomerList.Add(objDbDataReader["CustomerId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCustomerList;
        }

        public List<string> GetAllAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountNoForMainReportsViewer]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountNumberList;
        }

        public List<string> AutoCompleteAccountCode(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccounCodeList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccounCode", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountCodeForMainReportsViewer]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountCode"] != null)
                        {
                            objAccounCodeList.Add(objDbDataReader["AccountCode"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccounCodeList;
        }


        public List<string> GetAllLoanIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objLoanIdList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("LoanId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAllLoanIdForAutocomplete]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanNo"] != null)
                        {
                            objLoanIdList.Add(Convert.ToString(objDbDataReader["LoanNo"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objLoanIdList;
        }
        public DataTable GetTestReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].uspGetTestReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public DataTable GetDepositStatementReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("AccountTypeId", ReportObject.AccountType);
                if (!string.IsNullOrEmpty(ReportObject.BranchId))
                {
                    objDbCommand.AddInParameter("BranchId", ReportObject.BranchId);
                }
               
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptGetDepositStatement", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetCustomerwiseTransactionReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CustomerId", ReportObject.CustomerId);
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                objDbCommand.AddInParameter("AccountTypeId", ReportObject.AccountType);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].crGetCustomerWiseTransactionReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetDateWiseAllTransactionReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptDateWiseAllTransaction", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public DataTable GetDateWiseWithdrawReportReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptDateWiseWithdraw", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetDateWiseDepositReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptDateWiseDeposit", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetIrregularInstallmentReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CustomerId", ReportObject.CustomerId);
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                objDbCommand.AddInParameter("AccountTypeId", ReportObject.AccountType);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptIrregularInstallment", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }


        public DataTable GetNextMonthMaturedAccountReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CustomerId", ReportObject.CustomerId);
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                objDbCommand.AddInParameter("AccountTypeId", ReportObject.AccountType);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptNextMonthMaturedAccount", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }


        public DataTable GetAccountOpeningReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptAccountOpeningReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public List<Reports> GetAccountNoByCustomerId(string customerId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Reports> list = new List<Reports>();
            Reports objReports = new Reports();
            try
            {
                objDbCommand.AddInParameter("CustomerId", customerId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountNoByCustomerIdForReport]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objReports = new Reports();
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objReports.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                            list.Add(objReports);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return list;
        }

        public DataTable GetAccountClosingReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("AccountNumber", ReportObject.AccountNumber);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptAccountClosingReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public DataTable GetReverseEntryReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("AccountSetupId", ReportObject.AccountType);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptReverseEntryReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetLoanWiseInstallmentReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptLoanWiseInstallmentReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetDateWisePaidInstallmentReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);
                if (!string.IsNullOrEmpty(ReportObject.FromDate))
                    objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                if (!string.IsNullOrEmpty(ReportObject.ToDate))
                    objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("BranchId", ReportObject.BranchId);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptDateWisePaidInstallment", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetDateWiseUnPaidInstallmentReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);
                if (!string.IsNullOrEmpty(ReportObject.FromDate))
                    objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                if (!string.IsNullOrEmpty(ReportObject.ToDate))
                    objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("BranchId", ReportObject.BranchId);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptDateWiseUnPaidInstallment", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public DataTable LoanAccountWiseInstallmentReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                //objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                //objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptLoanIdWiseInstallment", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        #region Continuous Loan Related Report Area

        public DataTable GetConLoanTransactionLoanIdWiseReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptConLoanTransactionLoanIdWiseReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetAccountPaymentVoucherReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptAccountPaymentVoucherReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetAccountsRecievableReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("VoucherNo", ReportObject.VoucherNo);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptAccountRecievableVoucherReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetContraEntryReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanId);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptAccountPaymentVoucherReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetJournalEntryReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("VoucherNo", ReportObject.VoucherNo);

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptJournalEntryReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetExpenseStatementReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptGetExpenseStatement", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }

        public DataTable GetIncomeStatementReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                //objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                //objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                //dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptGetIncomeStatement", CommandType.StoredProcedure);

                objDbCommand.AddInParameter("Date", ReportObject.FromDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptGetIncomeStatement", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }

        public DataTable GetPeriodicLedgerReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("AccountCode", ReportObject.AccountCode);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].FinancialTransaction_rptPeriodicDateWiseDetailsLedeger", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetPeriodicTrialBalanceReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].FinancialTransaction_rptPeriodicDateWiseTrialBalance", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }
        public DataTable GetPeriodicDateWiseBalanceSheetReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("CreatedBy", Convert.ToInt16(SessionUtility.TMSessionContainer.UserID));

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[FinancialTransaction_rptPeriodicDateWiseBalanceSheet]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        public DataTable GetProfitAndLostStatementReport()
        {

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptGetProfitAndLostStatement", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }

        #endregion


        #region HR Related Report

        public DataTable GetEmployeeAttendanceReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[HR_rptGetEmployeeAttendanceReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }


        public DataTable GetSalaryProcessReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[HR_rptGetSalaryProcessReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }


        public DataTable GetMonthlyPayslipReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[HR_rptGetMonthlyPayslipReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }

        #endregion
        #region Loan Application Report
        public DataTable GetLoanApplicationReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("LoanId", ReportObject.LoanApplication);
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptLoanApplicationReport", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

            return dt;
        }

        private void AssignColumnValue(DataRow initializerDataRow, DataRow assignedDataRow, string columnName, dynamic typeName)
        {
            if (!Convert.IsDBNull(initializerDataRow[columnName]))
            {
                assignedDataRow[columnName] = Convert.ChangeType(initializerDataRow[columnName], Type.GetType(typeName));
            }
        }

        public DataTable GetLoanApprovalReport(string serverPath)
        {
            #region Datatable Assignment

            DataTable dnt = new DataTable();
            dnt.Columns.Add("LoanId", typeof(string));
            dnt.Columns.Add("LoanNo", typeof(string));
            dnt.Columns.Add("AccountSetupId", typeof(int));
            dnt.Columns.Add("CustomerId", typeof(string));
            dnt.Columns.Add("AccountNumber", typeof(string));
            dnt.Columns.Add("LoanDate", typeof(DateTime));
            dnt.Columns.Add("LoanExpiryDate", typeof(DateTime));
            dnt.Columns.Add("LoanAmount", typeof(decimal));
            dnt.Columns.Add("InterestRate", typeof(decimal));
            dnt.Columns.Add("InterestRateToDisplay", typeof(decimal));
            dnt.Columns.Add("InterestAmount", typeof(decimal));
            dnt.Columns.Add("TotalAmountWithInterest", typeof(decimal));
            dnt.Columns.Add("InstallmentAmount", typeof(decimal));
            dnt.Columns.Add("InstallmentTypeId", typeof(byte));
            dnt.Columns.Add("TotalInterest", typeof(decimal));
            dnt.Columns.Add("NumberOfInstallment", typeof(byte));
            dnt.Columns.Add("PurposeOfLoan", typeof(string));
            dnt.Columns.Add("ReferenceName", typeof(string));
            dnt.Columns.Add("CustomerId1", typeof(string));
            dnt.Columns.Add("CustomerName", typeof(string));
            dnt.Columns.Add("CustomerImage", typeof(string));
            dnt.Columns.Add("FatherName", typeof(string));
            dnt.Columns.Add("MotherName", typeof(string));
            dnt.Columns.Add("SpouseName", typeof(string));
            dnt.Columns.Add("CustomerEntryDate", typeof(DateTime));
            dnt.Columns.Add("DateOfBirth", typeof(DateTime));
            dnt.Columns.Add("OccupationAndPosition", typeof(string));
            dnt.Columns.Add("Position", typeof(string));
            dnt.Columns.Add("PassportExpireDate", typeof(string));


            dnt.Columns.Add("PresentAddress", typeof(string));
            dnt.Columns.Add("PermanentAddress", typeof(string));
            dnt.Columns.Add("OfficeAddress", typeof(string));
            dnt.Columns.Add("TradeLicAuthority", typeof(string));
            dnt.Columns.Add("BranchName", typeof(string));
            dnt.Columns.Add("BranchCode", typeof(string));
            dnt.Columns.Add("InstallmentTypeName", typeof(string));
            dnt.Columns.Add("Username", typeof(string));
            dnt.Columns.Add("FirstNomineeName", typeof(string));
            dnt.Columns.Add("SecnondNomineeName", typeof(string));
            dnt.Columns.Add("FirstGuarantorContactNo", typeof(string));
            dnt.Columns.Add("SecnondGuarantorContactNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("SecondChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoAccountName", typeof(string));
            dnt.Columns.Add("FirstChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("SecondChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoAccountNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("SecondChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoChequeNo", typeof(string));
            dnt.Columns.Add("FirstChequeInfoBankName", typeof(string));
            dnt.Columns.Add("SecondChequeInfoBankName", typeof(string));
            dnt.Columns.Add("ThirdChequeInfoBankName", typeof(string));
            dnt.Columns.Add("FirstFunctionalPosition", typeof(string));
            dnt.Columns.Add("SecondFunctionalPosition", typeof(string));
            dnt.Columns.Add("ThirdFunctionalPosition", typeof(string));
            dnt.Columns.Add("ForthFunctionalPosition", typeof(string));


            dnt.Columns.Add("FirstSigDesignation", typeof(string));
            dnt.Columns.Add("SecondSigDesignation", typeof(string));
            dnt.Columns.Add("ThirdSigDesignation", typeof(string));
            dnt.Columns.Add("ForthSigDesignation", typeof(string));
            dnt.Columns.Add("FirstEmployeeName", typeof(string));
            dnt.Columns.Add("SecondEmployeeName", typeof(string));
            dnt.Columns.Add("ThirdEmployeeName", typeof(string));
            dnt.Columns.Add("ForthEmployeeName", typeof(string));

            dnt.Columns.Add("Gurantor1Image", typeof(byte[]));
            dnt.Columns.Add("Gurantor2Image", typeof(byte[]));
            dnt.Columns.Add("CustomerDisplayImage", typeof(byte[]));

            #endregion



            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DataTable dt;
            //List<Account> accountViewModels = new List<Account>();
            //Account objAccountsViewModel = new Account();
            try
            {
                objDbCommand.AddInParameter("LoanNo", ReportObject.LoanNo);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].loan_rptGetLoanApprovalReport", CommandType.StoredProcedure);
                dt.Columns.Add("Gurantor1Image", typeof (byte[]));
                dt.Columns.Add("Gurantor2Image", typeof (byte[]));
                dt.Columns.Add("CustomerDisplayImage", typeof(byte[]));
                foreach (DataRow c in dt.Rows)
                {                  
                    DataRow dr = dnt.NewRow();

                    AssignColumnValue(c, dr, "LoanId", "System.String");
                    AssignColumnValue(c, dr, "LoanNo", "System.String");
                    AssignColumnValue(c, dr, "AccountSetupId", "System.Int32");
                    AssignColumnValue(c, dr, "CustomerId", "System.String");
                    AssignColumnValue(c, dr, "AccountNumber", "System.String");
                    AssignColumnValue(c, dr, "LoanDate", "System.DateTime");
                    AssignColumnValue(c, dr, "LoanExpiryDate", "System.DateTime");
                    AssignColumnValue(c, dr, "LoanAmount", "System.Decimal");
                    AssignColumnValue(c, dr, "InterestRate", "System.Decimal");
                    AssignColumnValue(c, dr, "InterestRateToDisplay", "System.Decimal");
                    AssignColumnValue(c, dr, "InterestAmount", "System.Decimal");
                    AssignColumnValue(c, dr, "TotalAmountWithInterest", "System.Decimal");
                    AssignColumnValue(c, dr, "InstallmentAmount", "System.Decimal");
                    AssignColumnValue(c, dr, "InstallmentTypeId", "System.Byte");
                    AssignColumnValue(c, dr, "TotalInterest", "System.Decimal");
                    AssignColumnValue(c, dr, "NumberOfInstallment", "System.Byte");
                    AssignColumnValue(c, dr, "PurposeOfLoan", "System.String");
                    AssignColumnValue(c, dr, "ReferenceName", "System.String");
                    AssignColumnValue(c, dr, "CustomerId1", "System.String");
                    AssignColumnValue(c, dr, "CustomerName", "System.String");
                    AssignColumnValue(c, dr, "CustomerImage", "System.String");
                    AssignColumnValue(c, dr, "FatherName", "System.String");
                    AssignColumnValue(c, dr, "MotherName", "System.String");
                    AssignColumnValue(c, dr, "SpouseName", "System.String");
                    AssignColumnValue(c, dr, "CustomerEntryDate", "System.DateTime");
                    AssignColumnValue(c, dr, "DateOfBirth", "System.DateTime");
                    AssignColumnValue(c, dr, "OccupationAndPosition", "System.String");
                    AssignColumnValue(c, dr, "Position", "System.String");
                    AssignColumnValue(c, dr, "PassportExpireDate", "System.String");
                    AssignColumnValue(c, dr, "PresentAddress", "System.String");
                    AssignColumnValue(c, dr, "PermanentAddress", "System.String");
                    AssignColumnValue(c, dr, "OfficeAddress", "System.String");
                    AssignColumnValue(c, dr, "TradeLicAuthority", "System.String");
                    AssignColumnValue(c, dr, "BranchName", "System.String");
                    AssignColumnValue(c, dr, "BranchCode", "System.String");
                    AssignColumnValue(c, dr, "InstallmentTypeName", "System.String");
                    AssignColumnValue(c, dr, "Username", "System.String");
                    AssignColumnValue(c, dr, "FirstNomineeName", "System.String");
                    AssignColumnValue(c, dr, "SecnondNomineeName", "System.String");
                    AssignColumnValue(c, dr, "FirstGuarantorContactNo", "System.String");
                    AssignColumnValue(c, dr, "SecnondGuarantorContactNo", "System.String");
                    AssignColumnValue(c, dr, "FirstChequeInfoAccountName", "System.String");
                    AssignColumnValue(c, dr, "SecondChequeInfoAccountName", "System.String");
                    AssignColumnValue(c, dr, "ThirdChequeInfoAccountName", "System.String");
                    AssignColumnValue(c, dr, "FirstChequeInfoAccountNo", "System.String");
                    AssignColumnValue(c, dr, "SecondChequeInfoAccountNo", "System.String");
                    AssignColumnValue(c, dr, "ThirdChequeInfoAccountNo", "System.String");
                    AssignColumnValue(c, dr, "FirstChequeInfoChequeNo", "System.String");
                    AssignColumnValue(c, dr, "SecondChequeInfoChequeNo", "System.String");
                    AssignColumnValue(c, dr, "ThirdChequeInfoChequeNo", "System.String");
                    AssignColumnValue(c, dr, "FirstChequeInfoBankName", "System.String");
                    AssignColumnValue(c, dr, "SecondChequeInfoBankName", "System.String");
                    AssignColumnValue(c, dr, "ThirdChequeInfoBankName", "System.String");
                    AssignColumnValue(c, dr, "FirstFunctionalPosition", "System.String");
                    AssignColumnValue(c, dr, "SecondFunctionalPosition", "System.String");
                    AssignColumnValue(c, dr, "ThirdFunctionalPosition", "System.String");
                    AssignColumnValue(c, dr, "ForthFunctionalPosition", "System.String");
                    AssignColumnValue(c, dr, "FirstSigDesignation", "System.String");
                    AssignColumnValue(c, dr, "SecondSigDesignation", "System.String");
                    AssignColumnValue(c, dr, "ThirdSigDesignation", "System.String");
                    AssignColumnValue(c, dr, "ForthSigDesignation", "System.String");
                    AssignColumnValue(c, dr, "FirstEmployeeName", "System.String");
                    AssignColumnValue(c, dr, "SecondEmployeeName", "System.String");
                    AssignColumnValue(c, dr, "ThirdEmployeeName", "System.String");
                    AssignColumnValue(c, dr, "ForthEmployeeName", "System.String");

                    const int buffersize = 1024*512; // 1024 * 256;
                    var gurantor1ImagePath = serverPath + "Uploads/Guarantor/NotFound.png";
                    var gurantor2ImagePath = serverPath + "Uploads/Guarantor/NotFound.png";
                    var customerImagePath = serverPath + "Uploads/Customer/NotFound.png";
                    if (!string.IsNullOrEmpty(Convert.ToString(c["FirstGuarantorImage"])))
                    {
                       var imagePath = Convert.ToString(c["FirstGuarantorImage"]);
                        imagePath = imagePath.Substring(imagePath.IndexOf("Uploads", StringComparison.Ordinal));
                        imagePath = serverPath + imagePath;
                        if (File.Exists(imagePath))
                        {
                            gurantor1ImagePath = imagePath;
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(c["SecondGuarantorImage"])))
                    {
                        var imagePath = Convert.ToString(c["SecondGuarantorImage"]);
                        imagePath = imagePath.Substring(imagePath.IndexOf("Uploads", StringComparison.Ordinal));

                        imagePath = serverPath + imagePath;
                        if (File.Exists(imagePath))
                        {
                            gurantor2ImagePath = imagePath;
                        }
                        
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(c["CustomerImage"])))
                    {
                        var imagePath = Convert.ToString(c["CustomerImage"]);
                        imagePath = imagePath.Substring(imagePath.IndexOf("Uploads", StringComparison.Ordinal));

                        imagePath = serverPath + imagePath;
                        if (File.Exists(imagePath))
                        {
                            customerImagePath = imagePath;
                        }

                    }


                    var gurantor1Image = new byte[buffersize];
                    var streamProfile1Image = new FileStream(gurantor1ImagePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    streamProfile1Image.Read(gurantor1Image, 0, buffersize);
                    dr["Gurantor1Image"] = gurantor1Image;


                    var gurantor2Image = new byte[buffersize];
                    var streamProfile2Image = new FileStream(gurantor2ImagePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    streamProfile2Image.Read(gurantor2Image, 0, buffersize);
                    dr["Gurantor2Image"] = gurantor2Image;

                    var customerImage = new byte[buffersize];
                    var streamCustomerImage = new FileStream(customerImagePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    streamCustomerImage.Read(customerImage, 0, buffersize);
                    dr["CustomerDisplayImage"] = customerImage;
                    dnt.Rows.Add(dr);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dnt;
        }
        #endregion

        #region DetailList Report
        public DataTable GetDetailListReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                if (ReportObject.ToDate != "")
                {
                    objDbCommand.AddInParameter("ToDate", ReportObject.FromDate);
                }
                else
                {
                    objDbCommand.AddInParameter("ToDate", DateTime.Now);
                }
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));

                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[All_rptGetDetailListReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        #endregion DetailList Report

        #region ShareHoldingPosition Report
        public DataTable GetShareHoldingPosition()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                objDbCommand.AddInParameter("CustomerId", ReportObject.CustomerId);
                objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                objDbCommand.AddInParameter("ToDate", ReportObject.ToDate);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[rptCustomerShareHoldingPosition]", CommandType.StoredProcedure);
              //  dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[All_rptGetDetailListReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        #endregion ShareHoldingPosition Report 
        #region UserListReport Report
        public DataTable GetUserListReport()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            var dt = new DataTable();
            try
            {
                //if (ReportObject.FromDate != "")
                //{
                //    objDbCommand.AddInParameter("FromDate", ReportObject.FromDate);
                //}
                //else
                //{
                //    objDbCommand.AddInParameter("FromDate", DateTime.Now);
                //}
                //objDbCommand.AddInParameter("SearchString", searchQuery);
                objDbCommand.AddInParameter("SearchString", ReportObject.BranchId);
                objDbCommand.AddInParameter("CompanyId", Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId));
                objDbCommand.AddInParameter("BranchId", Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId));
                dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].rptSystemUserList", CommandType.StoredProcedure);
               // dt = objDataAccess.ExecuteTable(objDbCommand, "[dbo].[All_rptGetDetailListReport]", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return dt;
        }
        #endregion UserListReport Report
    }
}