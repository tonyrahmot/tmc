﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    [AuthenticationFilter]
    public class BranchController : Controller
    {
        BranchBLL objBranchBLL = new BranchBLL();
        // GET: Setup/Branch
        public ActionResult Index()
        {
            List<Branch> objBranchList = new List<Branch>();
            objBranchList = objBranchBLL.GetAllBranchInfo();
            return View(objBranchList);
        }
        [HttpGet]
        public ActionResult Save()
        {
            CompanyBLL objCompanyBll = new CompanyBLL();
            var model = new Branch
            {
                CompanyInfo = objCompanyBll.GetAllCompanyInfo()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(Branch objBranchModel)
        {

            bool status = false;
            if (ModelState.IsValid)
            {
                objBranchModel.IsDeleted = false;
                objBranchModel.CreatedBy = 1;
                objBranchModel.CreatedDate = DateTime.Now;
                objBranchBLL.SaveBranchInfo(objBranchModel);
                status = true;
            }
            else
            {
                return View(objBranchModel);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "Branch");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var Branch = objBranchBLL.GetBranchInfo(id);
            return View(Branch);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Branch objBranch)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                objBranch.UpdatedBy = 1;
                objBranch.UpdatedDate = DateTime.Now;
                objBranchBLL.UpdateBranchInfo(objBranch);
                status = true;
            }
            else
            {
                return View(objBranch);
            }
            if (status == true)
            {
                return RedirectToAction("Index", "Branch");
            }
            else
            {
                return new JsonResult { Data = new { status = status } };
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var Branch = objBranchBLL.GetBranchInfo(id);
            return View(Branch);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteBranch(int id)
        {
            objBranchBLL.DeleteBranch(id);
            return RedirectToAction("Index", "Branch");
        }
    }
}