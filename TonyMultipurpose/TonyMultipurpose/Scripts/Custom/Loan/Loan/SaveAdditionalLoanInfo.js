(function () {
    $('#deleteAdditionalLoanInfo').hide();
    $('#updateAdditionalLoanInfo').hide();

    var emiTotal;
    var z;
    var oAdditionalTable;
    ShowData();

    $('#saveAdditionalLoanInfo').click(function () {
        var isValid = checkAdditionalLoanDetailsValidation();
        if (isValid) {
            var data = {
                LoanId: $('#LoanId').val(),
                InstituteName: $('#AdditionalInstituteName').val(),
                LoanAmount: $('#AdditionalLoanAmount').val(),
                ExistingEmi: $('#AdditionalExistingEmi').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/SaveAdditionalLoanInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        oAdditionalTable.ajax.reload();
                        clearAdditionalFields();
                    }
                },
                error: function () {
                }
            });
        }
    });

    $('#updateAdditionalLoanInfo').click(function () {
        var isValid = checkAdditionalLoanDetailsValidation();
        if (isValid) {
            var data = {
                AdditionalLoanDetailId: $('#hdfAdditionalLoanDetail').val(),
                LoanId: $('#LoanId').val(),
                InstituteName: $('#AdditionalInstituteName').val(),
                LoanAmount: $('#AdditionalLoanAmount').val(),
                ExistingEmi: $('#AdditionalExistingEmi').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/UpdateAdditionalLoanInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        oAdditionalTable.ajax.reload();
                        clearAdditionalFields();
                    }
                },
                error: function () {
                }
            });
        }
    });

    $('#deleteAdditionalLoanInfo').click(function () {
        if (confirm("Are you sure?")) {
            var data = {
                LoanId: $('#LoanId').val(),
                AdditionalLoanDetailId: $('#hdfAdditionalLoanDetail').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/DeleteAdditionalLoanInfo",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        oAdditionalTable.ajax.reload();
                        clearAdditionalFields();
                    }
                },
                error: function () {
                }
            });
        }
        return false;
    });

    $('#clearAdditionalLoanInfo').on('click', function () {
        clearAdditionalFields();
    });



    //functions
    function ShowData() {
        oAdditionalTable = $('#AdditionalTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetAdditionalLoanInfoById",
                "type": "get",
                "data": {
                    "LoanId": $('#LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "InstituteName" },
                { "data": "LoanAmount" },
                { "data": "ExistingEmi" }
            ],
            searching: false,
            paging: false,
            "bInfo": false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                emiTotal = api
                    .column(3, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(3).footer()).html(
                     emiTotal
                );
                z = emiTotal;
            }
        });

        $('#AdditionalTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oAdditionalTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindAdditionalLoanData(oAdditionalTable.row(this).data());
        });
    };

    function bindAdditionalLoanData(data) {
        $('#hdfAdditionalLoanDetail').val(data.AdditionalLoanDetailId);
        $('#AdditionalInstituteName').val(data.InstituteName);
        $('#AdditionalLoanAmount').val(data.LoanAmount);
        $('#AdditionalExistingEmi').val(data.ExistingEmi);
        $('#deleteAdditionalLoanInfo').show();
        $('#updateAdditionalLoanInfo').show();
        $('#saveAdditionalLoanInfo').hide();
    }

    function checkAdditionalLoanDetailsValidation() {
        var isValid = true;
        if ($('#AdditionalInstituteName').val().trim() === '') {
            isValid = false;
            $('#AdditionalInstituteName').prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AdditionalInstituteName').prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AdditionalLoanAmount').val().trim() === '') {
            isValid = false;
            $('#AdditionalLoanAmount').prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AdditionalLoanAmount').prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }
    function clearAdditionalFields() {
        $('#AdditionalInstituteName').val('');
        $('#AdditionalLoanAmount').val('');
        $('#AdditionalExistingEmi').val('');
        $('#deleteAdditionalLoanInfo').hide();
        $('#updateAdditionalLoanInfo').hide();
        $('#saveAdditionalLoanInfo').show();
    }

})();