﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace TonyMultipurpose.Models
{
    public class CommonResult
    {
        public string Id { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public string CustomErrorMesg { get; set; }
        public bool ErrorStatus { get; set; }
    }
}