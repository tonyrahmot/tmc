﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TonyMultipurpose.Areas.Share.ViewModels
{
    public class SharePurchaseViewModel
    {
        public string TransactionId { get; set; }
        [Display(Name = "Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy")]
        public DateTime IssuedDate { get; set; }
        [Display(Name = "Customer")]
        [Required]
        public string CustomerId { get; set; }
       
        [Display(Name = "Price/Share")]
        public decimal? PricePerShare { get; set; }
        [Display(Name = "Buy Share Quantity")]
        [Required]
        [Range(1, Int32.MaxValue)]
        public int Amount { get; set; }
        [Display(Name = "Voucher No")]
        [Required]
        public string VoucherNo { get; set; }
        [Display(Name = "Particular")]
        public string Particular { get; set; }
      
        public int? BranchId { get; set; }
        public int? CompanyId { get; set; }
        public short CreatedBy { get; set; }       
       
        public string EntryType { get; set; }  
     
        public bool IsChequePayment { get; set; }
        [Display(Name = "Cheque No")]
        public string ChequeNo { get; set; }
        [Display(Name = "Cheque Issue Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy")]
        public string ChequeIssueDate { get; set; }
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }
        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }
        [Display(Name = "Bank Account No")]
        public string BankAccountNo { get; set; }      
        public int COAId { get; set; }
    }
}