﻿using System;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.Models
{
    
    public class ContinuousLoan:CommonModel
    {
        public long Id { get; set; }

        public string TransactionId { get; set; }

        public DateTime? TransactionDate { get; set; }

        public string LoanId { get; set; }

        public string CustomerId { get; set; }

        public string TransactionType { get; set; }

        public string Particular { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public decimal? Outstanding { get; set; }

        public decimal? AvailableBalance { get; set; }
        public decimal WithdrawAmount { get; set; }
        public decimal DepositAmount { get; set; }
        public bool? IsChequeEntry { get; set; }

        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public string ChequeNo { get; set; }

        public int COAId { get; set; }

        public string VoucherNo { get; set; }

        public decimal? CustomerLoanLimit { get; set; }
    }
}