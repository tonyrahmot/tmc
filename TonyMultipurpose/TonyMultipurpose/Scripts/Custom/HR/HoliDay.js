﻿
(function () {

    $('#submit1').hide();
    holidayTable();




    $('#submit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                Holiday: $('#Holiday').val(),
                HolidayType: $('#HolidayType').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveHoliDay",//'@Url.Action("SaveHoliDay", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#hdfId').val();
                        $('#HolidayType').val('');
                        $('#Holiday').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#submit1').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = {
                HolidayId: $('#hdfId').val(),
                Holiday: $('#Holiday').val(),
                HolidayType: $('#HolidayType').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateHolidayInfo",//'@Url.Action("UpdateHolidayInfo", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#HolidayType').val('');
                        $('#Holiday').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    //document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });



    var oTable;

    function holidayTable() {
        oTable = $('#HolidayTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveHoliDay",//'@Url.Action("SaveHoliDay", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "HolidayId" },
                {
                    "data": "Holiday",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "HolidayType" }
            ],
            "order": [[0, "desc"]],
        });
        $('#HolidayTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        $('#hdfId').val(data.HolidayId);
        var date = new Date(data.Holiday);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var holiday = date.getDate() + "/" + mon + "/" + date.getFullYear();

        $('#Holiday').val(holiday);
        $('#HolidayType').val(data.HolidayType);
    }

    $('#clear').click(function () {
        window.location.reload();
    });

    function CheckValidation() {
        var isValid = true;
        if ($('#HolidayType').val().trim() === '') {
            isValid = false;
            $('#HolidayType').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#HolidayType').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#Holiday').val().trim() === '') {
            isValid = false;
            $('#Holiday').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#Holiday').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }



    function FormateDate(jsonDate) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#Holiday').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    var months = [
           'Jan', 'Feb', 'Mar', 'Apr', 'May',
           'Jun', 'Jul', 'Aug', 'Sep',
           'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }
})();
