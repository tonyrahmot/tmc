﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class AttendanceModel
    {
        public int EmpId { get; set; }

        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public DateTime AttendanceDate { get; set; }

        public string InTime { get; set; }

        public string OutTime { get; set; }

        public string AttendanceStatus { get; set; }
        public string AttStatus { get; set; }
        public string Comments { get; set; }
    }
}