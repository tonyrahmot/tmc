﻿using System;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.BLL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Controllers
{
    [AuthenticationFilter]
    //--ataur
    public class AssignUserRoleController : Controller
    {
        // GET: Setup/AssignUserRole
        public ActionResult Index(string search = null)
        {
            RoleManagementBLL objRoleManagementBll=new RoleManagementBLL();
            var roleInfo = objRoleManagementBll.GetUserRoleMappingInfo(search);

            return View(roleInfo);
        }


        //Load Username,RoleName,BranchName for save
        [HttpGet]
        public ActionResult GetAllDropdownInfo()
        {
            UserDetailBLL objUserDetailBll = new UserDetailBLL();
            RoleBLL objRoleBll = new RoleBLL();
            BranchBLL objBranchBll = new BranchBLL();
            var model = new AssignUserRole
            {
                userDetail = objUserDetailBll.GetAllUserInfo(),
                roleInfo = objRoleBll.GetAllRoleInfo(),
                branchInfo = objBranchBll.GetAllBranchInfo()
            };
            return View("UserPartial", model);
        }

        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(AssignUserRole objAssignUserRole)
        {
            if (ModelState.IsValid)
            {
                RoleManagementBLL objRoleManagementBll = new RoleManagementBLL();
                objRoleManagementBll.AssignUserRoleInfo(objAssignUserRole);
            }
            return RedirectToAction("Index", "AssignUserRole");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            RoleManagementBLL objRoleManagementBll=new RoleManagementBLL();
            UserDetailBLL objUserDetailBll = new UserDetailBLL();
            RoleBLL objRoleBll = new RoleBLL();
            BranchBLL objBranchBll = new BranchBLL();
            var model = new AssignUserRole
            {
                userDetail = objUserDetailBll.GetAllUserInfo(),
                roleInfo = objRoleBll.GetAllRoleInfo(),
                branchInfo = objBranchBll.GetAllBranchInfo()
            };
            var assignRoleInfo = objRoleManagementBll.GetAssignRoleInfoById(id);
            Session["UrmId"] = assignRoleInfo.UrmId;
            ViewBag.UserDetailId=assignRoleInfo.UserDetailId;
            ViewBag.RoleId = assignRoleInfo.RoleId;
            ViewBag.BranchId = assignRoleInfo.BranchId;
            ViewBag.IsActive = assignRoleInfo.IsActive;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AssignUserRole objAssignUserRole)
        {
            objAssignUserRole.UrmId = Convert.ToInt32(Session["UrmId"]);
            RoleManagementBLL objRoleManagementBll = new RoleManagementBLL();
            if (ModelState.IsValid)
            {
                objRoleManagementBll.UpdateAssignUserRole(objAssignUserRole);
                Session.Remove("UrmId");
            }
            return RedirectToAction("Index", "AssignUserRole");
        }
    }
}