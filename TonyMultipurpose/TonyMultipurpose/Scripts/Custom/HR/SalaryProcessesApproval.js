﻿$(document).ready(function () {

    // 1st replace first column header text with checkbox
    checkBoxHeader();

    //3rd click event for checkbox of each row
    $("input[name='ids']").click(function () {
        var totalRows = $("#checkableGrid td :checkbox").length;
        var checked = $("#checkableGrid td :checkbox:checked").length;

        if (checked == totalRows) {
            $("#checkableGrid").find("input:checkbox").each(function () {
                this.checked = true;
            });
        }
        else {
            $("#cbSelectAll").removeAttr("checked");
        }
    });

    $("#search").keyup(function () {
        filter = new RegExp($(this).val(), 'i');
        $("#checkableGrid tbody tr").filter(function () {
            $(this).each(function () {
                found = false;
                $(this).children().each(function () {
                    content = $(this).html();
                    if (content.match(filter)) {
                        found = true;
                    }
                });
                if (!found) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            });
        });
    });


});
function checkBoxHeader() {
    $("#checkableGrid th").each(function () {
        if ($.trim($(this).text().toString().toLowerCase()) === "{checkall}") {
            $(this).text('');
            $("<input/>", { type: "checkbox", id: "cbSelectAll", value: "" }).appendTo($(this));
            $(this).append("");
        }
    });

    $("#cbSelectAll").on("click", function () {
        var ischecked = this.checked;
        $('#checkableGrid').find("input:checkbox").each(function () {
            this.checked = ischecked;
        });
    });
}



//for date picker
function FormateDate(jsonDate) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

$('.datepicker').datepicker({
    dateFormat: "dd/M/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+0"
});
$('#ProcessDate').datepicker({
    dateFormat: 'dd/M/yy',
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+60", type: Text
}).click(function () { $(this).focus(); });

