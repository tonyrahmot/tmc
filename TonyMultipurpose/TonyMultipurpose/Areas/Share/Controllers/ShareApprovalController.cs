﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Areas.Share.BLL;
using TonyMultipurpose.Areas.Share.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.Controllers
{
    public class ShareApprovalController : Controller
    {
        AuthorizedShareInfo _objSaModel;
        ShareApprovalBLL _objShareApprovalBll;
        // GET: Share/ShareEntry
        public ActionResult Index()
        {
            _objSaModel = new AuthorizedShareInfo();
            _objShareApprovalBll = new ShareApprovalBLL();
            //_objSAModel.IsApproved = false;
            _objSaModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            List<AuthorizedShareInfo> unapproveShareList = _objShareApprovalBll.GetUnApprovedShareList(_objSaModel);
            return View(unapproveShareList);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            _objShareApprovalBll = new ShareApprovalBLL();
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                _objShareApprovalBll.ApproveShareEntry(ids);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<AuthorizedShareInfo> objAuthorizedShareInfos = new List<AuthorizedShareInfo>();
            var ids = form.GetValues("Id");
            var irs = form.GetValues("RejectReason");

            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    _objSaModel = new AuthorizedShareInfo();
                    if (irs != null) _objSaModel.TransactionId = irs[i];
                    string[] trans = ids[i].Split('+');                    
                    _objSaModel.Id = Convert.ToInt64(trans[0]);
                    _objSaModel.EntryType = trans[1];
                    _objSaModel.RejectReason = form.GetValues(Convert.ToString(_objSaModel.TransactionId))[0];
                    objAuthorizedShareInfos.Add(_objSaModel);
                }
                _objShareApprovalBll = new ShareApprovalBLL();
                _objShareApprovalBll.RejectShare(objAuthorizedShareInfos);
            }
            return RedirectToAction("Index");
        }
    }
}