﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Share.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Share.BLL
{
    public class ShareApprovalBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;
        CommonResult _oCommonResult;
        private DbDataReader _objDbDataReader;
        //Retrive all upapprove share transaction
        public List<AuthorizedShareInfo> GetUnApprovedShareList(AuthorizedShareInfo _objShareInfoModel)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<AuthorizedShareInfo> _objUpApproveShareList = new List<AuthorizedShareInfo>();
            AuthorizedShareInfo _objUpApproveShare;
            try
            {
                //_objDbCommand.AddInParameter("IsApproved", _objShareInfoModel.IsApproved);
                _objDbCommand.AddInParameter("CompanyId", _objShareInfoModel.CompanyId);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[Share_GetUnApprovedShareInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        _objUpApproveShare = new AuthorizedShareInfo();
                        _objUpApproveShare.Id = Convert.ToInt32(objDbDataReader["Id"]);
                        _objUpApproveShare.TransactionId = Convert.ToString(objDbDataReader["TransactionId"]);
                        _objUpApproveShare.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"]);
                        _objUpApproveShare.EntryType = Convert.ToString(objDbDataReader["EntryType"]);
                        _objUpApproveShare.Particular = Convert.ToString(objDbDataReader["Particular"]);
                        _objUpApproveShare.AuthorizedAmount = Convert.ToInt32(objDbDataReader["Amount"]);
                        _objUpApproveShare.NoOfShare = Convert.ToInt32(objDbDataReader["Qty"]);
                        _objUpApproveShare.PricePerShare = Convert.ToInt32(objDbDataReader["PricePerShare"]);
                        _objUpApproveShareList.Add(_objUpApproveShare);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }
            return _objUpApproveShareList;
        }


        //Approve share transaction
        public string ApproveShareEntry(string[] ids)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                foreach (var id in ids)
                {
                    string[] indVal = id.Split('+');
                    _objDbCommand = null;
                    _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    _objDbCommand.AddInParameter("Id", indVal[0].ToString().Trim());
                    _objDbCommand.AddInParameter("IsApproved", true);
                    _objDbCommand.AddInParameter("EntryType", indVal[1].ToString().Trim());
                    noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_TransactionApprove]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        _objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        _objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return "Saved";
        }
        //Reject
        public string RejectShare(List<AuthorizedShareInfo> _objAuthorizedShareInfos)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in _objAuthorizedShareInfos)
            {
                _objDbCommand = null;
                _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                _objDbCommand.AddInParameter("Id", item.Id);
                _objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                _objDbCommand.AddInParameter("EntryType", item.EntryType);
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[Share_TransactionReject]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

    }
}