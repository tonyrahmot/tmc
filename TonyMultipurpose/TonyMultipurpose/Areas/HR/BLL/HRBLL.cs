﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Areas.HR.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.BLL
{
    public class HRBLL
    {
        private IDataAccess _objDataAccess;
        private DbCommand _objDbCommand;
        private DataTable _objDataTable;
        #region All Data TransferRecord Table
        public List<TransferRecordModel> GetAllTransferRecordInfo(TransferRecordModel objTransferRecord)
        {
            if (objTransferRecord == null) throw new ArgumentNullException(nameof(objTransferRecord));
            //DataTable objDataTable = new DataTable();
            _objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<TransferRecordModel> objTransferRecordList = new List<TransferRecordModel>();

            try
            {
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllTransferRecordInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objTransferRecord = new TransferRecordModel();
                        objTransferRecord.EmpId = Convert.ToString(dr["EmpCode"]);
                        objTransferRecord.Id = Convert.ToInt32(dr["Id"]);
                        objTransferRecord.IsStatus = Convert.ToBoolean(dr["Status"]);
                        objTransferRecord.Reason = Convert.ToString(dr["Reason"]);
                        objTransferRecord.ToBranch = dr["ToBranch"].ToString();
                        objTransferRecord.BranchCode = Convert.ToInt16(dr["BranchCode"]);
                        objTransferRecord.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objTransferRecord.FromBranch = Convert.ToString(dr["FromBranch"]);
                        objTransferRecord.TransferDate = Convert.ToString(dr["TransferDate"]);
                        objTransferRecordList.Add(objTransferRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objTransferRecordList;
        }
        #endregion All Data TransferRecord Table

        #region Insert Data Into TransferRecord Table
        public CommonResult SaveTranferRecordInfo(TransferRecordModel objTransferRecord)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpId", objTransferRecord.EmpId);
            _objDbCommand.AddInParameter("BranchCode", objTransferRecord.BranchCode);
            _objDbCommand.AddInParameter("IsStatus", objTransferRecord.IsStatus);
            _objDbCommand.AddInParameter("Reason", objTransferRecord.Reason);
            _objDbCommand.AddInParameter("CreatedBy", objTransferRecord.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objTransferRecord.CreatedDate);
            _objDbCommand.AddInParameter("TransferDate", objTransferRecord.TransferDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveTranferRecordInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Data Into TransferRecord Table

        #region Update Data TransferRecord Table
        public CommonResult UpdateTranferRecordInfo(TransferRecordModel objTransferRecord)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("Id", objTransferRecord.Id);
            _objDbCommand.AddInParameter("EmpId", objTransferRecord.EmpId);
            _objDbCommand.AddInParameter("BranchCode", objTransferRecord.BranchCode);
            _objDbCommand.AddInParameter("IsStatus", objTransferRecord.IsStatus);
            _objDbCommand.AddInParameter("Reason", objTransferRecord.Reason);
            _objDbCommand.AddInParameter("UpdatedBy", objTransferRecord.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objTransferRecord.UpdatedDate);
            _objDbCommand.AddInParameter("TransferDate", objTransferRecord.TransferDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateTranferRecordInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data TransferRecord Table

        #region   EmployeeInformation Table By EmpCode
        public EmployeeInformation GetEmployeeInfoByEmpCode(string empCode)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            EmployeeInformation objEmployeeInformation = new EmployeeInformation();
            List<EmployeeInformation> objEmployeeInformationList = new List<EmployeeInformation>();
            try
            {
                _objDbCommand.AddInParameter("EmpCode", empCode);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[hr_GetEmployeeInfoByEmpCode]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objEmployeeInformation = new EmployeeInformation();

                        objEmployeeInformation.EmployeeName = objDbDataReader["EmployeeName"].ToString();
                        objEmployeeInformation.MobileNo = objDbDataReader["MobileNo"].ToString();
                        objEmployeeInformation.NID = objDbDataReader["NID"].ToString();
                        objEmployeeInformation.FirstName = objDbDataReader["BranchName"].ToString();
                        objEmployeeInformation.LastName = objDbDataReader["Designation"].ToString();
                        objEmployeeInformation.EmailId = objDbDataReader["Department"].ToString();
                        objEmployeeInformation.CreatedName = objDbDataReader["FunctionalId"].ToString();
                        objEmployeeInformation.CurrentSalary = Convert.ToDecimal(objDbDataReader["CurrentSalary"].ToString());
                        var imagePath = objDbDataReader["ImagePath"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objEmployeeInformation.ImagePath = imagePath;
                        }
                        else
                        {
                            objEmployeeInformation.ImagePath = "\\Not Found\\";
                        }
                        objEmployeeInformationList.Add(objEmployeeInformation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objEmployeeInformation;
        }
        #endregion EmployeeInformation Table By EmpCode

        #region Approve Data TransferRecord
        public string TransferRecordApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_TransferRecordApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";

        }
        #endregion Approve Data TransferRecord

        #region All Data EmpBenefit Table
        public List<BenefitModel> GetAllEmpBenefitInfo(BenefitModel objEmpBenefit)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<BenefitModel> objEmpBenefitList = new List<BenefitModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllEmpBenefitInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objEmpBenefit = new BenefitModel();
                        objEmpBenefit.EmpId = Convert.ToString(dr["EmpCode"]);
                        objEmpBenefit.BenfitId = Convert.ToInt32(dr["BenfitId"]);
                        objEmpBenefit.BenefitAmount = Convert.ToDecimal(dr["BenefitAmount"]);
                        objEmpBenefit.BenefitType = Convert.ToString(dr["BenefitType"]);
                        objEmpBenefit.BenefitTypeName = Convert.ToString(dr["BenefitTypeName"]);
                        objEmpBenefit.BenefitDate = Convert.ToString(dr["BenefitDate"]);
                        objEmpBenefit.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objEmpBenefit.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        objEmpBenefit.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        objEmpBenefit.Description = Convert.ToString(dr["Description"]);
                        objEmpBenefit.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        objEmpBenefit.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        objEmpBenefitList.Add(objEmpBenefit);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objEmpBenefitList;
        }
        #endregion All Data EmpBenefit Table

        #region Insert Data Into EmpBenefit Table
        public CommonResult SaveEmpBenefitInfo(BenefitModel objEmpBenefit)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpId", objEmpBenefit.EmpId);
            _objDbCommand.AddInParameter("BenefitType", objEmpBenefit.BenefitType);
            _objDbCommand.AddInParameter("BenefitDate", objEmpBenefit.BenefitDate);
            _objDbCommand.AddInParameter("BenefitAmount", objEmpBenefit.BenefitAmount);
            _objDbCommand.AddInParameter("CreatedBy", objEmpBenefit.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objEmpBenefit.CreatedDate);
            _objDbCommand.AddInParameter("Description", objEmpBenefit.Description);
            _objDbCommand.AddInParameter("IsApproved", objEmpBenefit.IsApproved);
            _objDbCommand.AddInParameter("CompanyId", objEmpBenefit.CompanyId);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveEmpBenefitInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Data Into EmpBenefit Table

        #region Update Data EmpBenefit Table
        public CommonResult UpdateEmpBenefitInfo(BenefitModel objempBenefit)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            //objDbCommand.AddInParameter("BenfitId", _objempBenefit.BenfitId);
            _objDbCommand.AddInParameter("EmpId", objempBenefit.EmpId);
            _objDbCommand.AddInParameter("BenefitType", objempBenefit.BenefitType);
            _objDbCommand.AddInParameter("Description", objempBenefit.Description);
            _objDbCommand.AddInParameter("BenefitAmount", objempBenefit.BenefitAmount);
            _objDbCommand.AddInParameter("UpdatedBy", objempBenefit.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objempBenefit.UpdatedDate);
            _objDbCommand.AddInParameter("BenefitDate", objempBenefit.BenefitDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmpBenefitInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data EmpBenefit Table

        #region Approve Data EmpBenefit Table
        public string EmpBenefitApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpBenefitApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data EmpBenefit Table

        #region All Data EmpPromotion Table
        public List<PromotionModel> GetAllEmpPromotionInfo(PromotionModel objEmpPromotion)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<PromotionModel> objEmpPromotionList = new List<PromotionModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllEmpPromotionInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objEmpPromotion = new PromotionModel();
                        objEmpPromotion.EmpCode = Convert.ToString(dr["EmpCode"]);
                        objEmpPromotion.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objEmpPromotion.DesigId = Convert.ToInt32(dr["DesigId"]);
                        objEmpPromotion.FunctionalId = Convert.ToInt32(dr["FunctionalId"]);
                        objEmpPromotion.DesignationName = Convert.ToString(dr["DesignationName"]);
                        objEmpPromotion.FunctionalDesignation = Convert.ToString(dr["FunctionalDesignation"]);
                        objEmpPromotion.EffectiveDate = Convert.ToString(dr["EffectiveDate"]);
                        objEmpPromotion.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        objEmpPromotion.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        objEmpPromotion.Description = Convert.ToString(dr["Description"]);
                        objEmpPromotion.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        objEmpPromotion.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        objEmpPromotionList.Add(objEmpPromotion);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objEmpPromotionList;
        }
        #endregion All Data EmpPromotion Table

        #region Insert Into Data EmpPromotion Table
        public CommonResult SaveEmpPromotionInfo(PromotionModel _objEmpPromotion)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpCode", _objEmpPromotion.EmpCode);
            _objDbCommand.AddInParameter("DesigId", _objEmpPromotion.DesigId);
            _objDbCommand.AddInParameter("FunctionalId", _objEmpPromotion.FunctionalId);
            _objDbCommand.AddInParameter("EffectiveDate", _objEmpPromotion.EffectiveDate);
            _objDbCommand.AddInParameter("CreatedBy", _objEmpPromotion.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", _objEmpPromotion.CreatedDate);
            _objDbCommand.AddInParameter("Description", _objEmpPromotion.Description);
            _objDbCommand.AddInParameter("IsApproved", _objEmpPromotion.IsApproved);
            _objDbCommand.AddInParameter("CompanyId", _objEmpPromotion.CompanyId);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveEmpPromotionInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Into Data EmpPromotion Table

        #region Update Data EmpPromotion Table
        public CommonResult UpdateEmpPromotionInfo(PromotionModel objempPromotion)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", objempPromotion.EmpCode);
            _objDbCommand.AddInParameter("DesigId", objempPromotion.DesigId);
             _objDbCommand.AddInParameter("FunctionalId", objempPromotion.FunctionalId);
            _objDbCommand.AddInParameter("Description", objempPromotion.Description);
            _objDbCommand.AddInParameter("EffectiveDate", objempPromotion.EffectiveDate);
            _objDbCommand.AddInParameter("UpdatedBy", objempPromotion.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objempPromotion.UpdatedDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmpPromotionInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data EmpPromotion Table

        #region Approve Data EmpPromotion Table
        public string EmpPromotionApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpPromotionApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data EmpPromotion Table

        #region All Data Holiday Table
        public List<HolidayModel> GetAllEmpHolidayInfo(HolidayModel objHoliday)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<HolidayModel> objHolidayList = new List<HolidayModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllHolidayInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objHoliday = new HolidayModel();
                        objHoliday.HolidayId = Convert.ToInt32(dr["HolidayId"]);
                        objHoliday.HolidayType = Convert.ToString(dr["HolidayType"]);
                        objHoliday.Holiday = Convert.ToString(dr["Holiday"]);
                        objHoliday.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        objHoliday.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        objHoliday.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        objHoliday.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        objHolidayList.Add(objHoliday);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objHolidayList;
        }
        #endregion All Data Holiday Table

        #region Insert Into Data Holiday Table
        public CommonResult SaveHolidayInfo(HolidayModel objHoliday)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("HolidayType", objHoliday.HolidayType);
            _objDbCommand.AddInParameter("Holiday", objHoliday.Holiday);
            _objDbCommand.AddInParameter("CreatedBy", objHoliday.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objHoliday.CreatedDate);
            _objDbCommand.AddInParameter("IsApproved", objHoliday.IsApproved);
            _objDbCommand.AddInParameter("CompanyId", objHoliday.CompanyId);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveHolidayInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Into Data Holiday Table

        #region Update Data Holiday Table
        public CommonResult UpdateHolidayInfo(HolidayModel objHoliday)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("HolidayId", objHoliday.HolidayId);
            _objDbCommand.AddInParameter("HolidayType", objHoliday.HolidayType);
            _objDbCommand.AddInParameter("Holiday", objHoliday.Holiday);
            _objDbCommand.AddInParameter("UpdatedBy", objHoliday.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objHoliday.UpdatedDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateHolidayInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data Holiday Table

        #region Approve Data Holiday Table
        public string HolidayApproval(int holidayId)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("HolidayId", holidayId);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_HolidayApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data Holiday Table

        #region All Data MobileBillInformation Table
        public List<MobileBillModel> GetAllEmpMobileBillInfo(MobileBillModel objEmpMobileBill)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<MobileBillModel> objEmpMobileBillList = new List<MobileBillModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllEmpMobileBillInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objEmpMobileBill = new MobileBillModel();
                        objEmpMobileBill.MobileId = Convert.ToInt32(dr["MobileId"]);
                        objEmpMobileBill.MobileBill = Convert.ToDecimal(dr["MobileBill"]);
                        objEmpMobileBill.BillDate = Convert.ToString(dr["BillDate"]);
                        objEmpMobileBill.EmpCode = Convert.ToString(dr["EmpCode"]);
                        objEmpMobileBill.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objEmpMobileBill.Description = Convert.ToString(dr["Description"]);
                        objEmpMobileBill.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        objEmpMobileBill.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        objEmpMobileBill.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        objEmpMobileBill.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        objEmpMobileBillList.Add(objEmpMobileBill);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objEmpMobileBillList;
        }
        #endregion All Data MobileBillInformation Table

        #region Insert Into Data MobileBillInformation Table
        public CommonResult SaveEmpMobileBillInfo(MobileBillModel objEmpMobileBill)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpCode", objEmpMobileBill.EmpCode);
            _objDbCommand.AddInParameter("MobileBill", objEmpMobileBill.MobileBill);
            _objDbCommand.AddInParameter("BillDate", objEmpMobileBill.BillDate);
            _objDbCommand.AddInParameter("Description", objEmpMobileBill.Description);
            _objDbCommand.AddInParameter("CreatedBy", objEmpMobileBill.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objEmpMobileBill.CreatedDate);
            _objDbCommand.AddInParameter("IsApproved", objEmpMobileBill.IsApproved);
            _objDbCommand.AddInParameter("CompanyId", objEmpMobileBill.CompanyId);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveEmpMobileBillInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Into Data MobileBillInformation Table

        #region Update Data MobileBillInformation Table
        public CommonResult UpdateEmpMobileBillInfo(MobileBillModel objEmpMobileBill)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            int noOfAffacted = 0;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", objEmpMobileBill.EmpCode);
            _objDbCommand.AddInParameter("MobileBill", objEmpMobileBill.MobileBill);
            _objDbCommand.AddInParameter("BillDate", objEmpMobileBill.BillDate);
            _objDbCommand.AddInParameter("Description", objEmpMobileBill.Description);
            _objDbCommand.AddInParameter("UpdatedBy", objEmpMobileBill.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objEmpMobileBill.UpdatedDate);
            try
            {
                noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmpMobileBillInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data MobileBillInformation Table

        #region Approve Data MobileBillInformation Table
        public string EmpMobileBillApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpMobileBillApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data MobileBillInformation Table

        #region All Data AdvanceInformation Table
        public List<AdvanceAdjustmentModel> GetAllEmpAdvanceAdjustmentInfo(AdvanceAdjustmentModel _objEmpAdvanceAdjustment)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<AdvanceAdjustmentModel> _objEmpAdvanceAdjustmentList = new List<AdvanceAdjustmentModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllEmpAdvanceAdjustmentInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        _objEmpAdvanceAdjustment = new AdvanceAdjustmentModel();
                        _objEmpAdvanceAdjustment.AdvanceAmount = Convert.ToDecimal(dr["AdvanceAmount"]);
                        _objEmpAdvanceAdjustment.AdvanceDate = Convert.ToString(dr["AdvanceDate"]);
                        _objEmpAdvanceAdjustment.EffectiveDate = Convert.ToString(dr["EffectiveDate"]);
                        _objEmpAdvanceAdjustment.InstallmentNo = Convert.ToInt32(dr["InstallmentNo"]);
                        _objEmpAdvanceAdjustment.EmpCode = Convert.ToString(dr["EmpCode"]);
                        _objEmpAdvanceAdjustment.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        _objEmpAdvanceAdjustment.Description = Convert.ToString(dr["Description"]);
                        _objEmpAdvanceAdjustment.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        _objEmpAdvanceAdjustment.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        _objEmpAdvanceAdjustment.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        _objEmpAdvanceAdjustment.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        _objEmpAdvanceAdjustmentList.Add(_objEmpAdvanceAdjustment);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return _objEmpAdvanceAdjustmentList;
        }
        #endregion All Data AdvanceInformation Table

        #region Insert Into Data AdvanceInformation Table
        public CommonResult SaveAdvanceAdjustmentInfo(AdvanceAdjustmentModel objEmpAdvanceAdjustment)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpCode", objEmpAdvanceAdjustment.EmpCode);
            _objDbCommand.AddInParameter("AdvanceAmount", objEmpAdvanceAdjustment.AdvanceAmount);
            _objDbCommand.AddInParameter("AdvanceDate", objEmpAdvanceAdjustment.AdvanceDate);
            _objDbCommand.AddInParameter("EffectiveDate", objEmpAdvanceAdjustment.EffectiveDate);
            _objDbCommand.AddInParameter("Description", objEmpAdvanceAdjustment.Description);
            _objDbCommand.AddInParameter("InstallmentNo", objEmpAdvanceAdjustment.InstallmentNo);
            _objDbCommand.AddInParameter("CreatedBy", objEmpAdvanceAdjustment.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objEmpAdvanceAdjustment.CreatedDate);
            _objDbCommand.AddInParameter("IsApproved", objEmpAdvanceAdjustment.IsApproved);
            _objDbCommand.AddInParameter("CompanyId", objEmpAdvanceAdjustment.CompanyId);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveAdvanceAdjustmentInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Into Data AdvanceInformation Table

        #region Update Data AdvanceInformation Table
        public CommonResult UpdateEmpAdvanceAdjustmentInfo(AdvanceAdjustmentModel objEmpAdvanceAdjustment)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", objEmpAdvanceAdjustment.EmpCode);
            _objDbCommand.AddInParameter("InstallmentNo", objEmpAdvanceAdjustment.InstallmentNo);
            _objDbCommand.AddInParameter("AdvanceDate", objEmpAdvanceAdjustment.AdvanceDate);
            _objDbCommand.AddInParameter("EffectiveDate", objEmpAdvanceAdjustment.EffectiveDate);
            _objDbCommand.AddInParameter("AdvanceAmount", objEmpAdvanceAdjustment.AdvanceAmount);
            _objDbCommand.AddInParameter("Description", objEmpAdvanceAdjustment.Description);
            _objDbCommand.AddInParameter("UpdatedBy", objEmpAdvanceAdjustment.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objEmpAdvanceAdjustment.UpdatedDate);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmpAdvanceAdjustmentInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data AdvanceInformation Table

        #region Approve Data AdvanceInformation Table
        public string EmpAdvanceAdjustmentApproval(string empCode)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);
            

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpAdvanceAdjustmentApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data AdvanceInformation Table

        #region All Data ArrierInformation Table
        public List<ArrierModel> GetAllEmpArrierBillInfo(ArrierModel objEmpArrierBill)
        {
            if (objEmpArrierBill == null) throw new ArgumentNullException(nameof(objEmpArrierBill));
            _objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<ArrierModel> objEmpArrierBillList = new List<ArrierModel>();

            try
            {
                _objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllEmpArrierBillInfo]", CommandType.StoredProcedure);

                if (_objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in _objDataTable.Rows)
                    {
                        objEmpArrierBill = new ArrierModel();
                        objEmpArrierBill.ArrierId = Convert.ToInt32(dr["ArrierId"]);
                        objEmpArrierBill.ArrierAmount = Convert.ToDecimal(dr["ArrierAmount"]);
                        objEmpArrierBill.ArrierDate = Convert.ToString(dr["ArrierDate"]);
                        objEmpArrierBill.EmpCode = Convert.ToString(dr["EmpCode"]);
                        objEmpArrierBill.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objEmpArrierBill.Description = Convert.ToString(dr["Description"]);
                        objEmpArrierBill.CreatedBy = Convert.ToByte(dr["CreatedBy"]);
                        objEmpArrierBill.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                        objEmpArrierBill.CompanyId = Convert.ToByte(dr["CompanyId"]);
                        objEmpArrierBill.IsApproved = Convert.ToBoolean(dr["IsApproved"]);
                        objEmpArrierBill.IsRejected = Convert.ToBoolean(dr["IsRejected"]);
                        objEmpArrierBillList.Add(objEmpArrierBill);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objEmpArrierBillList;
        }
        #endregion All Data ArrierInformation Table

        #region Insert Into Data ArrierInformation Table
        public CommonResult SaveEmpArrierBillInfo(ArrierModel objEmpArrierBill)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("EmpCode", objEmpArrierBill.EmpCode);
            _objDbCommand.AddInParameter("ArrierAmount", objEmpArrierBill.ArrierAmount);
            _objDbCommand.AddInParameter("ArrierDate", objEmpArrierBill.ArrierDate);
            _objDbCommand.AddInParameter("Description", objEmpArrierBill.Description);
            _objDbCommand.AddInParameter("CreatedBy", objEmpArrierBill.CreatedBy);
            _objDbCommand.AddInParameter("CreatedDate", objEmpArrierBill.CreatedDate);
            _objDbCommand.AddInParameter("IsApproved", objEmpArrierBill.IsApproved);
            _objDbCommand.AddInParameter("IsRejected", objEmpArrierBill.IsRejected);
            _objDbCommand.AddInParameter("CompanyId", objEmpArrierBill.CompanyId);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveEmpArrierBillInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Into Data ArrierInformation Table

        #region Update Data ArrierInformation Table
        public CommonResult UpdateEmpArrierBillInfo(ArrierModel objEmpArrierBill)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", objEmpArrierBill.EmpCode);
            _objDbCommand.AddInParameter("ArrierAmount", objEmpArrierBill.ArrierAmount);
            _objDbCommand.AddInParameter("ArrierDate", objEmpArrierBill.ArrierDate);
            _objDbCommand.AddInParameter("Description", objEmpArrierBill.Description);
            _objDbCommand.AddInParameter("UpdatedBy", objEmpArrierBill.UpdatedBy);
            _objDbCommand.AddInParameter("UpdatedDate", objEmpArrierBill.UpdatedDate);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmpArrierBillInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data ArrierInformation Table

        #region Approve Data ArrierInformation Table
        public string EmpArrierBillApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpArrierBillApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data ArrierInformation Table

        #region All Data Department Table
        public List<Department> GetAllDepartmentInfo(Department objDepartment)
        {
            if (objDepartment == null) throw new ArgumentNullException(nameof(objDepartment));
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<Department> objDepartmentList = new List<Department>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllDepartmentInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objDepartment = new Department();
                        objDepartment.DeptId = Convert.ToInt32(dr["DeptId"]);
                        objDepartment.DepartmentName = Convert.ToString(dr["Department"]);
                        objDepartment.DeptCode = Convert.ToString(dr["DeptCode"]);
                        objDepartmentList.Add(objDepartment);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objDepartmentList;
        }
        #endregion All Data Department Table

        #region Insert Data Into Department Table
        public CommonResult SaveDepartmentInfo(Department objDepartment)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("DepartmentName", objDepartment.DepartmentName);
            _objDbCommand.AddInParameter("DeptCode", objDepartment.DeptCode);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveDepartmentInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Data Into Department Table

        #region Update Data Department Table
        public CommonResult UpdateDepartmentInfo(Department objDepartment)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("DepartmentName", objDepartment.DepartmentName);
            _objDbCommand.AddInParameter("DeptCode", objDepartment.DeptCode);
            _objDbCommand.AddInParameter("DeptId", objDepartment.DeptId);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateDepartmentInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data Department Table

        #region All Data Designation Table
        public List<Designation> GetAllDesignationInfo(Designation objDesignation)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<Designation> objDesignationList = new List<Designation>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllDesignationInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objDesignation = new Designation();
                        objDesignation.DesigId = Convert.ToInt32(dr["DesigId"]);
                        objDesignation.DesignationName = Convert.ToString(dr["Designation"]);
                        objDesignation.DesignationCode = Convert.ToString(dr["DesignationCode"]);
                        objDesignation.FunctionalDesignation = Convert.ToString(dr["FuntionalDesignation"]);
                        objDesignationList.Add(objDesignation);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objDesignationList;
        }
        #endregion All Data Designation Table

        #region Insert Data Into Designation Table
        public CommonResult SaveDesignationInfo(Designation objDesignation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("DesignationName", objDesignation.DesignationName);
            _objDbCommand.AddInParameter("DesignationCode", objDesignation.DesignationCode);
            _objDbCommand.AddInParameter("FunctionalDesignation", objDesignation.FunctionalDesignation);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_SaveDesignationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Insert Data Into Designation Table

        #region Update Data Designation Table
        public CommonResult UpdateDesignationInfo(Designation objDesignation)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("DesignationName", objDesignation.DesignationName);
            _objDbCommand.AddInParameter("DesignationCode", objDesignation.DesignationCode);
            _objDbCommand.AddInParameter("DesigId", objDesignation.DesigId);
            _objDbCommand.AddInParameter("FunctionalDesignation", objDesignation.FunctionalDesignation);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateDesignationInfo]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data Designation Table

        #region Duplicate Designation Code
        public bool GetDesignationCodeCheck(string designationCode)
        {
            bool status = false;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                _objDbCommand.AddInParameter("DesignationCode", designationCode);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[HR_GetDesignationCodeCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }
            return status;
        }
        #endregion Duplicate Designation Code

        #region Duplicate Designation Name
        public bool GetDesignationNameCheck(string designationName)
        {
            bool status = false;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                _objDbCommand.AddInParameter("DesignationName", designationName);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[HR_GetDesignationNameCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }
            return status;
        }
        #endregion Duplicate Designation Name

        #region Duplicate Department Name
        public bool GetDepartmentNameCheck(string departmentName)
        {
            bool status = false;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                _objDbCommand.AddInParameter("DepartmentName", departmentName);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[HR_GetDepartmentNameCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }
            return status;
        }
        #endregion Duplicate Department Name

        #region Duplicate Department Code
        public bool GetDepartmentCodeCheck(string deptCode)
        {
            bool status = false;
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                _objDbCommand.AddInParameter("deptCode", deptCode);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[HR_GetDepartmentCodeCheck]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }
            return status;
        }
        #endregion Duplicate Department Code


        #region Salary Process
        public CommonResult SaveSalaryProcesses(SalaryProcessesViewModel salaryProcesses)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Process Failed.";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("SalaryMonth", salaryProcesses.ProcessDate);
          
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[SararyPrecessess]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Process Sucessfull.";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        public List<SalaryProcessesViewModel> GetAllUnApprovedSalaryProcessInfo(SalaryProcessesViewModel salaryProcesses)
        {
            if (salaryProcesses == null) throw new ArgumentNullException(nameof(salaryProcesses));

            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<SalaryProcessesViewModel> salaryProcessesList = new List<SalaryProcessesViewModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetAllUnApprovedSalaryProcessInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        salaryProcesses = new SalaryProcessesViewModel();
                        salaryProcesses.SalaryId = Convert.ToInt32(dr["SalaryId"]);
                        salaryProcesses.EmpId = Convert.ToInt32(dr["EmpId"]);
                        salaryProcesses.EmployeeName = dr["EmployeeName"].ToString();
                        salaryProcesses.DlBookNumber = dr["DlBookNumber"].ToString();
                        salaryProcesses.SalaryMonth = dr["SalaryMonth"].ToString();
                        salaryProcesses.Gross = Convert.ToDecimal(dr["Gross"]);
                        salaryProcesses.TA = Convert.ToDecimal(dr["TA"]);
                        salaryProcesses.DA = Convert.ToDecimal(dr["DA"]);
                        salaryProcesses.HRA = Convert.ToDecimal(dr["HRA"]);
                        salaryProcesses.TeleBillAndConveyance = Convert.ToDecimal(dr["TeleBillAndConveyance"]);
                        salaryProcesses.TotalSalary = Convert.ToDecimal(dr["TotalSalary"]);
                        salaryProcesses.BasicSalary = Convert.ToDecimal(dr["BasicSalary"]);
                        salaryProcesses.PF = Convert.ToDecimal(dr["PF"]);
                        salaryProcesses.EFund = Convert.ToDecimal(dr["EFund"]);
                        salaryProcesses.TotalDeduction = Convert.ToDecimal(dr["TotalDeduction"]);
                        salaryProcesses.NetSalary = Convert.ToDecimal(dr["NetSalary"]);
                        salaryProcessesList.Add(salaryProcesses);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return salaryProcessesList;
        }
        public List<SalaryProcessesViewModel> GetProcessDateWiseSalaryProcessInfo(string processDate)
        {

            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<SalaryProcessesViewModel> salaryProcessesList = new List<SalaryProcessesViewModel>();
            try
            {
                _objDbCommand.AddInParameter("processDate", processDate);
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetProcessDateWiseSalaryProcessInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        var salaryProcesses = new SalaryProcessesViewModel();
                        salaryProcesses.SalaryId = Convert.ToInt32(dr["SalaryId"]);
                        salaryProcesses.EmpId = Convert.ToInt32(dr["EmpId"]);
                        salaryProcesses.EmployeeName = dr["EmployeeName"].ToString();
                        salaryProcesses.DlBookNumber = dr["DlBookNumber"].ToString();
                        salaryProcesses.SalaryMonth = dr["SalaryMonth"].ToString();
                        salaryProcesses.Gross = Convert.ToDecimal(dr["Gross"]);
                        salaryProcesses.TA = Convert.ToDecimal(dr["TA"]);
                        salaryProcesses.DA = Convert.ToDecimal(dr["DA"]);
                        salaryProcesses.HRA = Convert.ToDecimal(dr["HRA"]);
                        salaryProcesses.TeleBillAndConveyance = Convert.ToDecimal(dr["TeleBillAndConveyance"]);
                        salaryProcesses.TotalSalary = Convert.ToDecimal(dr["TotalSalary"]);
                        salaryProcesses.BasicSalary = Convert.ToDecimal(dr["BasicSalary"]);
                        salaryProcesses.PF = Convert.ToDecimal(dr["PF"]);
                        salaryProcesses.EFund = Convert.ToDecimal(dr["EFund"]);
                        salaryProcesses.TotalDeduction = Convert.ToDecimal(dr["TotalDeduction"]);
                        salaryProcesses.NetSalary = Convert.ToDecimal(dr["NetSalary"]);
                        salaryProcessesList.Add(salaryProcesses);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return salaryProcessesList;
        }


        public string SalaryProcessesApproval(string[] ch)
        {

            _objDataAccess = DataAccess.NewDataAccess();

            try
            {
                foreach (var salaryId in ch)
                {
                    _objDbCommand = null;
                    _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    _objDbCommand.AddInParameter("SalaryId", salaryId);
                    _objDbCommand.AddInParameter("IsApproved", true);
                    var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[hr_SalaryProcessesApproval]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        _objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        _objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return "Saved";
        }

        #endregion

        #region Insert Data EmployeeSalaryInformation Table
        public string SaveEmployeeSalaryInformation(int salary, int basicsalary, int houserent, int medical,string empCode,string efficientdate,int currentSalary,int grosssalary)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            _objDbCommand.AddInParameter("salary", salary);
            _objDbCommand.AddInParameter("basicsalary", basicsalary);
            _objDbCommand.AddInParameter("houserent", houserent);
            _objDbCommand.AddInParameter("medical", medical);
            //objDbCommand.AddInParameter("others", others);
            _objDbCommand.AddInParameter("empCode", empCode);
            _objDbCommand.AddInParameter("efficientdate", efficientdate);
            _objDbCommand.AddInParameter("currentSalary", currentSalary); 
            _objDbCommand.AddInParameter("grosssalary", grosssalary);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[hr_SaveEmployeeSalaryInformation]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return "Saved";
        }
        #endregion Insert Data EmployeeSalaryInformation

        #region All Data EmployeeSalaryInformation Table Used EmpCode


        public SalaryModel GetEmployeeeSalaryInfoByEmpCode(string empCode)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            SalaryModel objSalaryModel = new SalaryModel();
            List<SalaryModel> objSalaryList = new List<SalaryModel>();
            try
            {
                _objDbCommand.AddInParameter("EmpCode", empCode);
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[hr_GetEmployeeeSalaryInfoByEmpCode]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objSalaryModel = new SalaryModel();
                        objSalaryModel.BasicSalary = Convert.ToDecimal(objDbDataReader["BasicSalary"].ToString());
                        objSalaryModel.HouseRent = Convert.ToDecimal(objDbDataReader["HouseRent"].ToString());
                        objSalaryModel.Medical = Convert.ToDecimal(objDbDataReader["Medical"].ToString());
                        //_objSalaryModel.Others = Convert.ToDecimal(objDbDataReader["Others"].ToString());

                        objSalaryList.Add(objSalaryModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objSalaryModel;
        }
        #endregion All Data EmployeeSalaryInformation Table Used EmpCode

        #region All Data EmployeeSalaryInformation Table

        public List<SalaryModel> GetEmployeeSalaryInfo(SalaryModel objSalary)
        {
            DataTable objDataTable = new DataTable();
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            List<SalaryModel> objSalaryList = new List<SalaryModel>();

            try
            {
                objDataTable = _objDataAccess.ExecuteTable(_objDbCommand, "[dbo].[HR_GetEmployeeSalaryInfo]", CommandType.StoredProcedure);

                if (objDataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in objDataTable.Rows)
                    {
                        objSalary = new SalaryModel();
                        objSalary.EmpCode = Convert.ToString(dr["EmpCode"]);
                        objSalary.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                        objSalary.EfficientDate = Convert.ToString(dr["EfficientDate"]);
                        objSalary.CurrentSalary = Convert.ToDecimal(dr["CurrentSalary"]);
                        objSalary.UpdatedSalary = Convert.ToDecimal(dr["UpdatedSalary"]);
                        objSalaryList.Add(objSalary);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objSalaryList;
        }
        public SalaryModel GetEmployeeeSalaryInfos(string empCode)
        {
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            SalaryModel objSalaryModel = new SalaryModel();
            List<SalaryModel> objSalaryList = new List<SalaryModel>();
            try
            {
                objDbDataReader = _objDataAccess.ExecuteReader(_objDbCommand, "[dbo].[hr_GetEmployeeeSalaryInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objSalaryModel = new SalaryModel();


                        objSalaryModel.BasicSalary = Convert.ToDecimal(objDbDataReader["BasicSalary"].ToString());
                        objSalaryModel.HouseRent = Convert.ToDecimal(objDbDataReader["HouseRent"].ToString());
                        objSalaryModel.Medical = Convert.ToDecimal(objDbDataReader["Medical"].ToString());
                        objSalaryModel.Others = Convert.ToDecimal(objDbDataReader["Others"].ToString());

                        objSalaryList.Add(objSalaryModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                _objDataAccess.Dispose(_objDbCommand);
            }

            return objSalaryModel;
        }
        #endregion All Data EmployeeSalaryInformation Table

        #region Update Data EmployeeSalaryInformation Table
        public CommonResult UpdateEmployeeSalaryInformation(SalaryModel objSalaryModel)
        {
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";
            _objDataAccess = DataAccess.NewDataAccess();
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", objSalaryModel.EmpCode);
            _objDbCommand.AddInParameter("BasicSalary", objSalaryModel.BasicSalary);
            _objDbCommand.AddInParameter("UpdatedSalary", objSalaryModel.UpdatedSalary);
            _objDbCommand.AddInParameter("EfficientDate", objSalaryModel.EfficientDate);
            _objDbCommand.AddInParameter("HouseRent", objSalaryModel.HouseRent);
            _objDbCommand.AddInParameter("Medical", objSalaryModel.Medical);
            _objDbCommand.AddInParameter("Others", objSalaryModel.Others);
            try
            {
                var noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].[HR_UpdateEmployeeSalaryInformation]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    _objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Successful";
                }
                else
                {
                    _objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                _objDataAccess.Dispose(_objDbCommand);
            }
            return oCommonResult;
        }
        #endregion Update Data EmployeeSalaryInformation Table

        #region Approve Data EmployeeSalaryInformation Table
        public string EmpSalaryApproval(string empCode)
        {

            _objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;
            _objDbCommand = null;
            _objDbCommand = _objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            _objDbCommand.AddInParameter("EmpCode", empCode);

            noOfAffacted = _objDataAccess.ExecuteNonQuery(_objDbCommand, "[dbo].HR_EmpSalaryApproval", CommandType.StoredProcedure);
            if (noOfAffacted > 0)
            {
                _objDbCommand.Transaction.Commit();
            }
            else
            {
                _objDbCommand.Transaction.Rollback();
                return "failed";
            }

            return "Saved";
        }
        #endregion Approve Data EmployeeSalaryInformation Table

    }
}