﻿using System.Web.Mvc;

namespace TonyMultipurpose.Areas.TermLoan
{
    public class TermLoanAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TermLoan";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "TermLoan_default",
                "TermLoan/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}