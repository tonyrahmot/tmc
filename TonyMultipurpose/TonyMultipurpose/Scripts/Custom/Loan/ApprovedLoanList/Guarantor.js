
$(function () {

    var oGuarantorTable;
    loadGuarantorTable();
    function loadGuarantorTable() {
        oGuarantorTable = $('#GuarantorTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + 'Loan/ApprovedLoanList/GetAllGurantorInfo', 
                "type": "get",
                "data": {
                    "LoanId": $('#Guarantor_LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "GuarantorName" },
                { "data": "NID" },
                { "data": "FatherName" },
                { "data": "MotherName" },
                { "data": "Occupation" },
                { "data": "Designation" },
                { "data": "InstituteName" },
                { "data": "InstituteAddress" },
                { "data": "CurrentAddress" },
                { "data": "PermanentAddress" },
                { "data": "ContactNo" },
            ],
            "order": [[0, "desc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#GuarantorTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oGuarantorTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataGuarantor(oGuarantorTable.row(this).data());
        });
    }
    function bindDataGuarantor(data) {
        $('#hdfGuarantorId').val(data.GuarantorId);
        $('#img-upload').attr('src', data.ImagePath);
        document.getElementById('Guarantor_GuarantorName').innerHTML = data.GuarantorName;
        document.getElementById('Guarantor_NID').innerHTML = data.NID;
        document.getElementById('Guarantor_FatherName').innerHTML = data.FatherName;
        document.getElementById('Guarantor_MotherName').innerHTML = data.MotherName;
        document.getElementById('Guarantor_Occupation').innerHTML = data.Occupation;
        document.getElementById('Guarantor_Designation').innerHTML = data.Designation;
        document.getElementById('Guarantor_InstituteName').innerHTML = data.InstituteName;
        document.getElementById('Guarantor_InstituteAddress').innerHTML = data.InstituteAddress;
        document.getElementById('Guarantor_CurrentAddress').innerHTML = data.CurrentAddress;
        document.getElementById('Guarantor_PermanentAddress').innerHTML = data.PermanentAddress;
        document.getElementById('Guarantor_ContactNo').innerHTML = data.ContactNo;
        document.getElementById('Guarantor_GuarantorRoll').innerHTML = data.GuarantorRoll;
        document.getElementById('Guarantor_SpouseName').innerHTML = data.SpouseName;
        document.getElementById('Guarantor_RelationWithApplicant').innerHTML = data.RelationWithApplicant;
        document.getElementById('Guarantor_DateofBirth').innerHTML = data.DateofBirth;
        document.getElementById('Guarantor_MonthlyIncome').innerHTML = data.MonthlyIncome;
        document.getElementById('Guarantor_BusinessAddress').innerHTML = data.BusinessAddress;
        document.getElementById('Guarantor_LoanStatusWithTmc').innerHTML = data.LoanStatusWithTmc;
        document.getElementById('GResidentialCondition').innerHTML = data.ResidentialConditionType;


        $('#saveGuarantor').hide();
        $('#UpdateGuarantor').show();
        $('#deleteGuarantor').show();
    }
    // caler function
    $('#Guarantor_Clear').click(function () {
        guarantorClearAll();
    });
    function guarantorClearAll() {
        $('#hdfGuarantorId').val('');
        $('#img-upload').attr('src', '');
        document.getElementById('Guarantor_GuarantorName').innerHTML = '';
        document.getElementById('Guarantor_NID').innerHTML = '';
        document.getElementById('Guarantor_FatherName').innerHTML = '';
        document.getElementById('Guarantor_MotherName').innerHTML = '';
        document.getElementById('Guarantor_Occupation').innerHTML = '';
        document.getElementById('Guarantor_Designation').innerHTML = '';
        document.getElementById('Guarantor_InstituteName').innerHTML = '';
        document.getElementById('Guarantor_InstituteAddress').innerHTML = '';
        document.getElementById('Guarantor_CurrentAddress').innerHTML = '';
        document.getElementById('Guarantor_PermanentAddress').innerHTML = '';
        document.getElementById('Guarantor_ContactNo').innerHTML = '';
        document.getElementById('Guarantor_GuarantorRoll').innerHTML = '';
        document.getElementById('Guarantor_SpouseName').innerHTML = '';
        document.getElementById('Guarantor_RelationWithApplicant').innerHTML = '';
        document.getElementById('Guarantor_DateofBirth').innerHTML = '';
        document.getElementById('Guarantor_MonthlyIncome').innerHTML = '';
        document.getElementById('Guarantor_BusinessAddress').innerHTML = '';
        document.getElementById('Guarantor_LoanStatusWithTmc').innerHTML = '';
        document.getElementById('GResidentialCondition').innerHTML = '';

    }
});

