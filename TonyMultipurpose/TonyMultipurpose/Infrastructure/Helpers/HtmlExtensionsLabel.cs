﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsLabel
    {
        public static MvcHtmlString BootstrapLabelFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            var cssClass = "control-label";

            rvd.Add("class", cssClass);
            return htmlHelper.LabelFor(expression, rvd);
        }
        public static MvcHtmlString BootstrapLabelFor<TModel, TValue>(
          this HtmlHelper<TModel> htmlHelper,
          Expression<Func<TModel, TValue>> expression, string className,
          object htmlAttributes = null
          )
        {
            // Creates the route value dictionary
            var rvd = new RouteValueDictionary(
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            var cssClass = "control-label";
            if (!string.IsNullOrEmpty(className))
            {
                cssClass += " " + className;
            }

            rvd.Add("class", cssClass);
            return htmlHelper.LabelFor(expression, rvd);
        }

    }
}