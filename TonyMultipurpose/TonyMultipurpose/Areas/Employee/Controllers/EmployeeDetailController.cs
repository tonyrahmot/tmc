﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Employee.BLL;
using TonyMultipurpose.Areas.Employee.Models;

namespace TonyMultipurpose.Areas.Employee.Controllers
{
    // created by shovon
    public class EmployeeDetailController : Controller
    {
        //GET: Employee/EmployeeDetail
        public ActionResult Index()
        {
            EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();
            List<EmployeeDetail> employeeList = objEmployeeDetailBLL.GetAllEmployeeList();
            return View(employeeList);
        }
        [HttpGet]
        public ActionResult Save()
        {
            DepartmentBLL objDepartmentBLL = new DepartmentBLL();
            DesignationBLL objDesignationBll = new DesignationBLL();
            var model = new EmployeeDetail
            {
                department = objDepartmentBLL.GetAllDepartment(),
                designation = objDesignationBll.GetAllDesignation()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Save(EmployeeDetail objEmployeeInfo)
        {
            objEmployeeInfo.DesignationId = Convert.ToByte(Request.Form["DesignationId"]);
            objEmployeeInfo.DepartmentId = Convert.ToByte(Request.Form["DepartmentId"]);

            if (ModelState.IsValid)
            {
                EmployeeDetailBLL objEmployeeBLL = new EmployeeDetailBLL();

                objEmployeeBLL.SaveEmployeeInfo(objEmployeeInfo);

            }

            return RedirectToAction("Save", "EmployeeDetail");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            EmployeeDetailBLL objEmployeeBLL = new EmployeeDetailBLL();
            DepartmentBLL objDepartmentBLL = new DepartmentBLL();
            DesignationBLL objDesignationBll = new DesignationBLL();
            ViewBag.department = objDepartmentBLL.GetAllDepartment();
            ViewBag.designation = objDesignationBll.GetAllDesignation();
            var employee = objEmployeeBLL.GetAllEmployeeList().Where(a => a.EmployeeId == id).FirstOrDefault();
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Edit(EmployeeDetail objEmployeeInfo)
        {

            if (ModelState.IsValid)
            {
                EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();

                objEmployeeDetailBLL.UpdateEmployeeDetailInfo(objEmployeeInfo);

            }
            return RedirectToAction("Index", "EmployeeDetail");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EmployeeDetailBLL objEmployeeBLL = new EmployeeDetailBLL();
            DepartmentBLL objDepartmentBLL = new DepartmentBLL();
            DesignationBLL objDesignationBll = new DesignationBLL();
            ViewBag.department = objDepartmentBLL.GetAllDepartment();
            ViewBag.designation = objDesignationBll.GetAllDesignation();
            var employee = objEmployeeBLL.GetAllEmployeeList().Where(a => a.EmployeeId == id).FirstOrDefault();
            return View(employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken()]
        [ActionName("Delete")]
        public ActionResult DeleteEmployeeDetailInfo(int id)
        {

            if (ModelState.IsValid)
            {
                EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();
                objEmployeeDetailBLL.DeleteEmployeeDetailInfo(id);
            }
            return RedirectToAction("Index", "EmployeeDetail");
        }
        [HttpGet]
        public ActionResult EmployeeAllInformation()
        {
            EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
            List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
            ViewBag.employeeName = EmployeeInfo;
            return View();
        }


        public JsonResult EmployeeInformationById(int employeeId)
        {
            try
            {
                EmployeeDetailBLL objEmployeeDetailBLL = new EmployeeDetailBLL();
                if (employeeId > 0)
                {                   
                    List<EmployeeDetail> EmployeeAllInformation = objEmployeeDetailBLL.GetEmployeeAllInformation(employeeId);
                    return Json(EmployeeAllInformation, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    EducationalQualificationBLL objEducationalQualificationBLL = new EducationalQualificationBLL();
                    List<EmployeeDetail> EmployeeInfo = objEducationalQualificationBLL.GetEmployeeDetailName();
                    return Json(EmployeeInfo, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
            }

            return Json(JsonRequestBehavior.AllowGet);

        }
    }
}