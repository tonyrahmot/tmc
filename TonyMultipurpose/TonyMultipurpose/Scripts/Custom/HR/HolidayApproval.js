﻿(function () {

    $('#submit1').hide();
    holidayTable();

    var oTable;

    function holidayTable() {
        oTable = $('#HolidayTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/HolidayUnApprovaList",//'@Url.Action("HolidayUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "HolidayId" },
                {
                    "data": "Holiday",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }

                },
                { "data": "HolidayType" },
                {
                    "data": "HolidayId",

                    "render": function (data) {
                        return '<a class="btn btn-primary" href="'+window.applicationBaseUrl+'HR/HR/HolidayInfoApproval?id=' + data + '">Approve</a>';
                    }
                }
            ],
            "order": [[0, "desc"]],
        });

    }



    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();