﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.CSS.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.CSS.BLL
{
    public class CSSBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        /// <summary>
        /// get all account number for autocomplete
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public List<string> GetAllAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountNoForAutocomplete]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountNumberList;
        }

        /// <summary>
        /// get all css info for account number
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public CSSModel GetAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            CSSModel objCssModel = new CSSModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCssAccountInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        //objCssModel=new CSSModel();
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objCssModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objCssModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objCssModel.FatherName = objDbDataReader["FatherName"].ToString();
                            objCssModel.MotherName = objDbDataReader["MotherName"].ToString();
                            objCssModel.Mobile = objDbDataReader["Mobile"].ToString();
                            objCssModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                            objCssModel.Duration = Convert.ToInt32(objDbDataReader["Duration"].ToString());
                            objCssModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                            objCssModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                            objCssModel.InstallmentNumber =
                                Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                            objCssModel.InstallmentMonth =
                                Convert.ToInt32(objDbDataReader["InstallmentMonth"].ToString());
                            objCssModel.InstallmentYear = Convert.ToInt32(objDbDataReader["InstallmentYear"].ToString());
                            objCssModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                            objCssModel.Duration = Convert.ToInt32(objDbDataReader["Duration"].ToString());
                            objCssModel.CustomerImage = objDbDataReader["CustomerImage"].ToString();
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objCssModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objCssModel.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCssModel;
        }
        public List<CSSModel> GetAllAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CSSModel> objCSSModelList = new List<CSSModel>();
            CSSModel objCssModel = new CSSModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCssAllAccountInfo]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCssModel=new CSSModel();
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objCssModel.TransactionDate =
                               Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                            objCssModel.InstallmentNumber =
                                Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                            objCssModel.InstallmentMonth =
                                Convert.ToInt32(objDbDataReader["InstallmentMonth"].ToString());
                            objCssModel.InstallmentYear = Convert.ToInt32(objDbDataReader["InstallmentYear"].ToString());
                            objCssModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                            objCSSModelList.Add(objCssModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCSSModelList;
        }

        /// <summary>
        /// Save CSS Installment Deposit
        /// </summary>
        /// <param name="cssModel"></param>
        public string SaveCSSTransaction(List<CSSModel> cssModel)
        {
            int noOfAffacted = 0;
            CSSModel cssModels = new CSSModel();
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            try
            {
                objDataAccess = DataAccess.NewDataAccess();
                foreach (CSSModel cssModelitem in cssModel)
                {
                    cssModels.TransactionId = GenerateTransactionId(cssModelitem);
                    if (cssModels.TransactionId != null)
                    {
                        cssModels.TransactionType = "Deposit";
                        cssModels.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                        cssModels.BranchId = Convert.ToInt16(SessionUtility.TMSessionContainer.BranchId);
                        cssModels.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                        cssModels.CreatedDate = DateTime.Now;
                        cssModels.IsActive = true;
                        cssModels.IsApproved = false;
                        //cssModels.COAId = cssModel.coa;

                        objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
                        objDbCommand.AddInParameter("AccountNumber", cssModelitem.AccountNumber);
                        objDbCommand.AddInParameter("TransactionId", cssModels.TransactionId);
                        objDbCommand.AddInParameter("TransactionDate", cssModelitem.TransactionDate);
                        objDbCommand.AddInParameter("TransactionType", cssModels.TransactionType);
                        objDbCommand.AddInParameter("CustomerName", cssModelitem.CustomerName);
                        objDbCommand.AddInParameter("CustomerId", cssModelitem.CustomerId);
                        objDbCommand.AddInParameter("InterestRate", cssModelitem.InterestRate);
                        objDbCommand.AddInParameter("Duration", cssModelitem.Duration);
                        objDbCommand.AddInParameter("MatureDate", cssModelitem.MatureDate);
                        objDbCommand.AddInParameter("MatureAmount", cssModelitem.MatureAmount);
                        objDbCommand.AddInParameter("Fine", cssModelitem.Fine);
                        objDbCommand.AddInParameter("AccruedBalance", cssModelitem.AccruedBalance);
                        objDbCommand.AddInParameter("InstallmentMonth", cssModelitem.InstallmentMonth);
                        objDbCommand.AddInParameter("InstallmentYear", cssModelitem.InstallmentYear);
                        objDbCommand.AddInParameter("InstallmentNumber", cssModelitem.InstallmentNumber);
                        objDbCommand.AddInParameter("Amount", cssModelitem.Amount);
                        objDbCommand.AddInParameter("COAId", cssModelitem.COAId);
                        objDbCommand.AddInParameter("BranchId", cssModels.BranchId);
                        objDbCommand.AddInParameter("CompanyId", cssModels.CompanyId);
                        objDbCommand.AddInParameter("CreatedBy", cssModels.CreatedBy);
                        objDbCommand.AddInParameter("CreatedDate", cssModels.CreatedDate);
                        objDbCommand.AddInParameter("IsApproved", cssModels.IsApproved);
                        objDbCommand.AddInParameter("IsActive", cssModels.IsActive);

                        noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCSSTransaction]",
                            CommandType.StoredProcedure);
                        if (noOfAffacted > 0)
                        {
                            objDbCommand.Transaction.Commit();
                        }
                        else
                        {
                            objDbCommand.Transaction.Rollback();
                        }
                    }
                }
                return "saved";
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }


        }

        /// <summary>
        /// save mature encashment information
        /// </summary>
        /// <param name="cssModel"></param>
        /// <returns></returns>
        public void SaveMatureEncashmentInfo(CSSModel cssModel)
        {
            int noOfAffacted = 0;
            cssModel.TransactionId = GenerateTransactionId(cssModel);

            if (cssModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", cssModel.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", cssModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", cssModel.CustomerId);
                objDbCommand.AddInParameter("InterestRate", cssModel.InterestRate);
                objDbCommand.AddInParameter("Duration", cssModel.Duration);
                objDbCommand.AddInParameter("MatureDate", cssModel.MatureDate);
                objDbCommand.AddInParameter("MatureAmount", cssModel.MatureAmount);
                objDbCommand.AddInParameter("COAId", cssModel.COAId);

                objDbCommand.AddInParameter("transactionId", cssModel.TransactionId);
                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }

        /// <summary>
        /// save pre mature encashment information
        /// </summary>
        /// <param name="cssModel"></param>
        /// <returns></returns>
        public void SavePreMatureEncashmentInfo(CSSModel cssModel)
        {
            int noOfAffacted = 0;
            cssModel.TransactionId = GenerateTransactionId(cssModel);

            if (cssModel.TransactionId != null)
            {
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("AccountNumber", cssModel.AccountNumber);
                objDbCommand.AddInParameter("CustomerName", cssModel.CustomerName);
                objDbCommand.AddInParameter("CustomerId", cssModel.CustomerId);
                objDbCommand.AddInParameter("InterestRate", cssModel.InterestRate);
                objDbCommand.AddInParameter("PreMatureAmount", cssModel.PreMatureAmount);
                objDbCommand.AddInParameter("Balance", cssModel.Balance);
                objDbCommand.AddInParameter("CompanyAmount", cssModel.CompanyAmount);
                objDbCommand.AddInParameter("COAId", cssModel.COAId);

                objDbCommand.AddInParameter("transactionId", cssModel.TransactionId);

                objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
                objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);
                objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);


                try
                {
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveCssPreMatureEncashmentInfo]",
                        CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                        //return "saved";
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        //return "failed";
                    }
                }
                catch (Exception ex)
                {
                    objDbCommand.Transaction.Rollback();
                    throw new Exception("Database Error Occured", ex);
                }

                finally
                {
                    objDataAccess.Dispose(objDbCommand);
                }
            }

        }

        //Generate Transaction Id
        private string GenerateTransactionId(CSSModel cssModel)
        {
            var transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            if (cssModel.AccountNumber != null)
            {
                objDbDataReader = null;
                objDataAccess = DataAccess.NewDataAccess();
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
                int Id = 0;

                string year = Convert.ToString(DateTime.Now.Year);
                string days = Convert.ToString(DateTime.Now.DayOfYear);
                //var prefix = string.Concat(year + days);
                string prefix = year + days;

                objDbCommand.AddInParameter("FieldName", "TransactionId");
                objDbCommand.AddInParameter("TableName", "Css");
                objDbCommand.AddInParameter("Prefix", prefix);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                    CommandType.StoredProcedure);
                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        transactionId = prefix;
                        Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        transactionId += Convert.ToString(Id).PadLeft(4, '0');
                    }
                }
                objDbDataReader.Close();
            }
            return transactionId;
        }

        /// <summary>
        /// get pre mature encashment info
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public CSSModel GetPreMatureAccountInfoByAccountNo(string accountNumber)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            CSSModel objCssModel = new CSSModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", accountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetPreMatureCssAccountInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        //objCssModel=new CSSModel();
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objCssModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objCssModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objCssModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                            objCssModel.InstallmentNumber = Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                            objCssModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                            objCssModel.AccruedBalance = Convert.ToDecimal(objDbDataReader["AccruedBalance"].ToString());


                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objCssModel.CustomerImage = imagePath;
                            }
                            else
                            {
                                objCssModel.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objCssModel;
        }

        public List<CSSModel> GetCSSTransactions(CSSModel oCSSModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<CSSModel> cssTransactions = new List<CSSModel>();
            CSSModel objCSSModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("IsApproved", oCSSModel.IsApproved);
                //objDbCommand.AddInParameter("IsActive", oCSSModel.IsActive);
                objDbCommand.AddInParameter("CompanyId", oCSSModel.CompanyId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCSSTransactions]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objCSSModel = new CSSModel();
                        this.BuildModelForCSS(objDbDataReader, objCSSModel);
                        cssTransactions.Add(objCSSModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return cssTransactions;
        }

        public string ApproveCSSTransactions(string[] ids)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            try
            {
                foreach (var id in ids)
                {
                    objDbCommand = null;
                    objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                    objDbCommand.AddInParameter("Id", id);
                    objDbCommand.AddInParameter("IsApproved", true);
                    noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApproveCSSTransaction]", CommandType.StoredProcedure);
                    if (noOfAffacted > 0)
                    {
                        objDbCommand.Transaction.Commit();
                    }
                    else
                    {
                        objDbCommand.Transaction.Rollback();
                        return "failed";
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return "Saved";
        }
        public string RejectCSSTransaction(List<CSSModel> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspRejectCSSTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        private void BuildModelForCSS(DbDataReader objDbDataReader, CSSModel objCSSModel)
        {
            DataTable objDataTable = objDbDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDbDataReader["TransactionId"]))
                        {
                            objCSSModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDbDataReader["TransactionDate"]))
                        {
                            objCSSModel.TransactionDate = Convert.ToDateTime(objDbDataReader["TransactionDate"].ToString());
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDbDataReader["TransactionType"]))
                        {
                            objCSSModel.TransactionType = objDbDataReader["TransactionType"].ToString();
                        }
                        break;
                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDbDataReader["AccountNumber"]))
                        {
                            objCSSModel.AccountNumber = objDbDataReader["AccountNumber"].ToString();
                        }
                        break;
                    
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDbDataReader["CustomerId"]))
                        {
                            objCSSModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDbDataReader["CustomerName"]))
                        {
                            objCSSModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDbDataReader["InterestRate"]))
                        {
                            objCSSModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "Mobile":
                        if (!Convert.IsDBNull(objDbDataReader["Mobile"]))
                        {
                            objCSSModel.Mobile = objDbDataReader["Mobile"].ToString();
                        }
                        break;
                    case "MatureAmount":
                        if (!Convert.IsDBNull(objDbDataReader["MatureAmount"]))
                        {
                            objCSSModel.MatureAmount = Convert.ToDecimal(objDbDataReader["MatureAmount"].ToString());
                        }
                        break;
                    case "MatureDate":
                        if (!Convert.IsDBNull(objDbDataReader["MatureDate"]))
                        {
                            objCSSModel.MatureDate = Convert.ToDateTime(objDbDataReader["MatureDate"].ToString());
                        }
                        break;
                    case "InstallmentNumber":
                        if (!Convert.IsDBNull(objDbDataReader["InstallmentNumber"]))
                        {
                            objCSSModel.InstallmentNumber = Convert.ToInt32(objDbDataReader["InstallmentNumber"].ToString());
                        }
                        break;
                    case "InstallmentAmount":
                        if (!Convert.IsDBNull(objDbDataReader["InstallmentAmount"]))
                        {
                            objCSSModel.InstallmentAmount = Convert.ToDecimal(objDbDataReader["InstallmentAmount"].ToString());
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDbDataReader["Amount"]))
                        {
                            objCSSModel.Amount = Convert.ToDecimal(objDbDataReader["Amount"].ToString());
                        }
                        break;
                    case "Fine":
                        if (!Convert.IsDBNull(objDbDataReader["Fine"]))
                        {
                            objCSSModel.Fine = Convert.ToDecimal(objDbDataReader["Fine"].ToString());
                        }
                        break;
                    case "Username":
                        if (!Convert.IsDBNull(objDbDataReader["Username"]))
                        {
                            objCSSModel.Username = objDbDataReader["Username"].ToString();
                        }
                        break;
                    case "InstallmentMontWithYear":
                        if (!Convert.IsDBNull(objDbDataReader["InstallmentMontWithYear"]))
                        {
                            objCSSModel.Particular = objDbDataReader["InstallmentMontWithYear"].ToString();
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}