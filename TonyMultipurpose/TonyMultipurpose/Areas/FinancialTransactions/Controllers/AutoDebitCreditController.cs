﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    public class AutoDebitCreditController : Controller
    {
        // GET: FinancialTransactions/AutoDebitCredit
        AutoDebitCreditModel _objAutoDebitCreditModel;
        AutoDebitCreditBLL _objAutoDebitCreditBll;
        CommonResult _oCommonResult;
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Save()
        {
            _objAutoDebitCreditModel = new AutoDebitCreditModel();
            _objAutoDebitCreditBll = new AutoDebitCreditBLL();
            var autodebitCredit = _objAutoDebitCreditBll.GetAllAutoDebitCreditInfo(_objAutoDebitCreditModel);
            return Json(new { data = autodebitCredit }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetActiveAccountNumbers(string term, string type)
        {
            _objAutoDebitCreditBll = new AutoDebitCreditBLL();
            var result = _objAutoDebitCreditBll.GetActiveAccountNumbers(term,type);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccountTypeByNumber(string AccountNumber)
        {
            _objAutoDebitCreditBll = new AutoDebitCreditBLL();
            var data = _objAutoDebitCreditBll.GetAccountTypeByNumber(AccountNumber);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(AutoDebitCreditModel objAutoDebitCredit)
        {
            _oCommonResult = new CommonResult();
            _objAutoDebitCreditBll = new AutoDebitCreditBLL();
            if (ModelState.IsValid)
            {
                objAutoDebitCredit.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
                objAutoDebitCredit.CreatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objAutoDebitCredit.CreatedDate = DateTime.Now;
                objAutoDebitCredit.IsDeleted = false;
                objAutoDebitCredit.IsActive = true;
                _oCommonResult = _objAutoDebitCreditBll.SaveAutoDebitCreditInfo(objAutoDebitCredit);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }

        [HttpPost]
        public ActionResult Update(AutoDebitCreditModel objAutoDebitCredit)
        {
            _oCommonResult = new CommonResult();
            _objAutoDebitCreditBll = new AutoDebitCreditBLL();
            if (ModelState.IsValid)
            {
                objAutoDebitCredit.UpdatedBy = Convert.ToInt16(SessionUtility.TMSessionContainer.UserID);
                objAutoDebitCredit.UpdatedDate = DateTime.Now;
                _oCommonResult = _objAutoDebitCreditBll.UpdateAutoDebitCreditInfo(objAutoDebitCredit);
            }
            return new JsonResult { Data = new { oCommonResult = _oCommonResult } };
        }
    }
}