﻿(function () {

    $('#submit1').hide();
    advanceAdjustmentTable();
    var oTable;

    function advanceAdjustmentTable() {
        oTable = $('#AdvanceAdjustmentTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/SaveAdvanceAdjustmentInfo",//'@Url.Action("SaveAdvanceAdjustmentInfo", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                 { "data": "EmployeeName" },
                {
                    "data": "AdvanceDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "AdvanceAmount" },
                { "data": "Description" },
                {
                    "data": "EffectiveDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                { "data": "InstallmentNo" }
            ],
            "order": [[0, "desc"]]
        });
        $('#AdvanceAdjustmentTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#submit1').show();
        $('#submit').hide();
        document.getElementById("EmpId").readOnly = true;
        $('#EmpId').val(data.EmpCode);
        var date = new Date(data.EffectiveDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var effectiveDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#EffectiveDate').val(effectiveDate);
        date = new Date(data.AdvanceDate);
        month = date.getMonth() + 1;
        mon = monthNumToName(month);
        var advanceDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#AdvanceDate').val(advanceDate);
        $('#Description').val(data.Description);
        $('#AdvanceAmount').val(data.AdvanceAmount);
        $('#InstallmentNo').val(data.InstallmentNo);
        $("#EmpId").change();
    }

    $('#clear').click(function () {
        window.location.reload();
    });


    $('#submit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                EffectiveDate: $('#EffectiveDate').val(),
                AdvanceDate: $('#AdvanceDate').val(),
                Description: $('#Description').val(),
                InstallmentNo: $('#InstallmentNo').val(),
                AdvanceAmount: $('#AdvanceAmount').val(),
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/SaveAdvanceAdjustmentInfo",//'@Url.Action("SaveAdvanceAdjustmentInfo", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#EffectiveDate').val('');
                        $('#AdvanceDate').val('');
                        $('#Description').val('');
                        $('#InstallmentNo').val('');
                        $('#AdvanceAmount').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#submit1').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = {
                EmpCode: $('#EmpId').val(),
                EffectiveDate: $('#EffectiveDate').val(),
                AdvanceDate: $('#AdvanceDate').val(),
                Description: $('#Description').val(),
                InstallmentNo: $('#InstallmentNo').val(),
                AdvanceAmount: $('#AdvanceAmount').val()
            };
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/UpdateEmpAdvanceAdjustmentInfo",//'@Url.Action("UpdateEmpAdvanceAdjustmentInfo", "HR", new { Area = "HR" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {

                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').show();
                        $('#submit1').hide();
                        document.getElementById('showMessage').style.color = "green";
                        $('#EmpId').val('');
                        $('#EffectiveDate').val('');
                        $('#AdvanceDate').val('');
                        $('#Description').val('');
                        $('#InstallmentNo').val('');
                        $('#AdvanceAmount').val('');
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    //document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    //$('#hdfLoanId').val('');
                }
            });

        }
    });

    $("#EmpId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpId").change(function () {
        $('#EmployeeName').empty();
        var empId = $("#EmpId").val();
        var json = { EmpId: empId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('EmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('Designation').innerHTML = data.LastName;
                document.getElementById('Center').innerHTML = data.FirstName;
                document.getElementById('NID').innerHTML = data.NID;
            }
        });
    });

    function checkValidation() {
        var isValid = true;
        if ($('#EmpId').val().trim() === '') {
            isValid = false;
            $('#EmpId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AdvanceDate').val().trim() === '') {
            isValid = false;
            $('#AdvanceDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AdvanceDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#EffectiveDate').val().trim() === '') {
            isValid = false;
            $('#EffectiveDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EffectiveDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AdvanceAmount').val().trim() === '') {
            isValid = false;
            $('#AdvanceAmount').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AdvanceAmount').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#InstallmentNo').val().trim() === '') {
            isValid = false;
            $('#InstallmentNo').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#InstallmentNo').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });

    $('#EffectiveDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    $('#AdvanceDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();