﻿namespace TonyMultipurpose.Areas.Share.ViewModels
{
    public class ShareHoldingViewModel
    {
        public decimal IssuedQty { get; set; }
        public decimal SoldQty { get; set; }
        public decimal PricePerShare { get; set; }
        public decimal Balance { get; set; }

        public decimal IssuedAmount { get; set; }
        public decimal PaidUpAmount { get; set; }
    }
}