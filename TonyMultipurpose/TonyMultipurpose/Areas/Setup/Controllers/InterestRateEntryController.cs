﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    //--ataur
    [AuthenticationFilter]
    public class InterestRateEntryController : Controller
    {
        // GET: Setup/InterestRateEntry
        public ActionResult Index()
        {
            InterestRateEntryBLL objInterestRateEntryBll = new InterestRateEntryBLL();
            List<InterestRateEntry> objInterestRateEntries = objInterestRateEntryBll.GetInterestRateEntryInfo();
            return View(objInterestRateEntries);
        }


        public JsonResult GetAccontTypeByAccountTitle( string AccountTitle)
        {
            InterestRateEntryBLL objInterestRateEntryBll = new InterestRateEntryBLL();
            //AccountBLL objAccountBll = new AccountBLL();
            var data = objInterestRateEntryBll.GetAccontTypeByAccountTitle(AccountTitle);
            return Json(data, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public ActionResult Save()
        {
            //InterestRateEntry objInterestRateEntry = new InterestRateEntry();
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(InterestRateEntry objInterestRateEntry)
        {
            InterestRateEntryBLL objInterestRateEntryBll = new InterestRateEntryBLL();
            objInterestRateEntryBll.AddInterestEntry(objInterestRateEntry);
            return RedirectToAction("Index", "InterestRateEntry");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            InterestRateEntryBLL objInterestRateEntryBll = new InterestRateEntryBLL();
            var interestRateInfo = objInterestRateEntryBll.GetInterestRateEntryInfoById(id);
            Session["AccountSetupId"]=interestRateInfo.AccountSetupId;
            return View(interestRateInfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InterestRateEntry objInterestRateEntry)
        {
            objInterestRateEntry.AccountSetupId = Convert.ToInt32(Session["AccountSetupId"]);
            InterestRateEntryBLL objInterestRateEntryBll = new InterestRateEntryBLL();
            objInterestRateEntryBll.UpdateInterestRateEntryInfo(objInterestRateEntry);
            Session.Remove("AccountSetupId");
            return RedirectToAction("Index", "InterestRateEntry");
        }
    }
}