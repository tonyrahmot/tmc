﻿using System.Web.Mvc;

namespace TonyMultipurpose.Areas.MonthlyBenefit
{
    public class MonthlyBenefitAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MonthlyBenefit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MonthlyBenefit_default",
                "MonthlyBenefit/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}