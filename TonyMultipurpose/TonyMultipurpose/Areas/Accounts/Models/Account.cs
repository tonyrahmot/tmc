﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.Models
{
    public class Account : CommonModel
    {
        public long Id { get; set; }
        [Display(Name = "Customer Id")]
        [Required(ErrorMessage = "Customer Id is required")]
        public string CustomerId { get; set; }
        [Display(Name = "A/C No")]
        public string AccountNumber { get; set; }
        [Display(Name = "Account Tilte")]
        public string AccountTitle { get; set; }
        [Display(Name = "Account Type")]
        [Required(ErrorMessage = "Account Type is required")]
        public int AccountType { get; set; }
        public string SpecialInstructions { get; set; }



        [Display(Name = "COA Code")]
        public string COAcode { get; set; }

        public int COAId { get; set; }

        [Display(Name = "COA Code")]
        public string AccountName { get; set; } //chart of account name
        public string AccountCode { get; set; } //chart of account code


        
        public decimal? InterestRate { get; set; }
        public decimal? DepositAmount { get; set; }
        public string Duration { get; set; }
        public DateTime? MatureDate { get; set; }
        public decimal? MatureAmount { get; set; }
        public decimal? MonthlyBenefit { get; set; }
        public string DurationYear { get; set; }

        public string DurationMonth { get; set; }

        public string InterestAmount { get; set; }








        public short? BranchId { get; set; }
        public DateTime OpeningDate { get; set; }
        public virtual ICollection<Nominee> Nominees { get; set; }
        //public CustomerModel customer { get; set; }
        public Account()
        {
            Nominees = new List<Nominee>();
            //customer = new CustomerModel();
        }

        public DataTable dtCommon { get; set; }
        public string CustomerImage { get; set; }
        public string FirstNomineeImage { get; set; }
        public string SecondNomineeImage { get; set; }
     
    }
}