﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.HR.BLL;
using TonyMultipurpose.Areas.HR.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.HR.Controllers
{
    public class LeaveInformationController : Controller
    {
        LeaveInformationBLL objLeaveInformationBLL = new LeaveInformationBLL();
        CommonResult objCommonResult = new CommonResult();
        // GET: HR/LeveManagement
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LeaveEntry()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveLeaveEntry(LeaveInformation objLeaveInformation)
        {
            objCommonResult = new CommonResult();
            objLeaveInformationBLL = new LeaveInformationBLL();
            objCommonResult = objLeaveInformationBLL.SaveLeaveEntry(objLeaveInformation);
            return new JsonResult { Data = new { objCommonResult.Status,objCommonResult.Message } };
        }
        public ActionResult GetLeaveType()
        {
             objLeaveInformationBLL = new LeaveInformationBLL();
            var LeaveType = objLeaveInformationBLL.GetLeaveType();
            return Json(LeaveType,JsonRequestBehavior.AllowGet);
        }
        public JsonResult AutoCompleteEmployeeCode(string term)
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var result = objLeaveInformationBLL.GetEmployeeCodeForAutoComplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetEmployeeNameByEmpId(string EmpId)
        {
            objLeaveInformationBLL = new LeaveInformationBLL();
            var result = objLeaveInformationBLL.GetEmployeeNameByEmpId(EmpId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}