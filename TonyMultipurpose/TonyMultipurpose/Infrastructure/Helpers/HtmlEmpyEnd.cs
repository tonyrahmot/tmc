﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlEmpyEnd : IDisposable
    {
        private readonly ViewContext _viewContext;
        public HtmlEmpyEnd(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }
        public void Dispose()
        {
            _viewContext.Writer.Write("");
        }
    }
}