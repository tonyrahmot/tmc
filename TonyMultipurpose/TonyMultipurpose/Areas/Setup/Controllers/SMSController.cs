﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Customer.Models;
using TonyMultipurpose.Areas.Setup.BLL;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    public class SMSController : Controller
    {
        // GET: Setup/SMS
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CustomersmsemailList()
        {
            CustomerModel objCustomerList = new CustomerModel();
            SMSBLL _objSMSBLL = new SMSBLL();
            var sms = _objSMSBLL.GetAllCustomerInfoSMSEmail(objCustomerList);
            return Json(new { data = sms }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendCustomeremail()
        {
            return View();
        }
    }
}