﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using TonyMultipurpose.Areas.Employee.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Employee.BLL
{
    public class ExperienceBLL
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        private void BuildModelForExperience(DbDataReader objDataReader, Experience objExperienceInfo)
        {

            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "ExperienceId":
                        if (!Convert.IsDBNull(objDataReader["ExperienceId"]))
                        {
                            objExperienceInfo.ExperienceId = Convert.ToInt16(objDataReader["ExperienceId"]);
                        }
                        break;
                    case "OrganizationName":
                        if (!Convert.IsDBNull(objDataReader["OrganizationName"]))
                        {
                            objExperienceInfo.OrganizationName = objDataReader["OrganizationName"].ToString();
                        }
                        break;
                    case "Position":
                        if (!Convert.IsDBNull(objDataReader["Position"]))
                        {
                            objExperienceInfo.Position = objDataReader["Position"].ToString();
                        }
                        break;
                    case "JoiningDate":
                        if (!Convert.IsDBNull(objDataReader["JoiningDate"]))
                        {
                            objExperienceInfo.JoiningDate = Convert.ToDateTime(objDataReader["JoiningDate"].ToString());
                        }
                        break;
                    case "EmployeeId":
                        if (!Convert.IsDBNull(objDataReader["EmployeeId"]))
                        {
                            objExperienceInfo.EmployeeId = Convert.ToInt16(objDataReader["EmployeeId"]);
                        }
                        break;
                    case "EmployeeName":
                        if (!Convert.IsDBNull(objDataReader["EmployeeName"]))
                        {
                            objExperienceInfo.EmployeeName = objDataReader["EmployeeName"].ToString();
                        }
                        break;
                    case "EndingDate":
                        if (!Convert.IsDBNull(objDataReader["EndingDate"]))
                        {
                            objExperienceInfo.EndingDate = Convert.ToDateTime(objDataReader["EndingDate"].ToString());
                        }
                        break;
                    case "Responsibility":
                        if (!Convert.IsDBNull(objDataReader["Responsibility"]))
                        {
                            objExperienceInfo.Responsibility =
                                objDataReader["Responsibility"].ToString();
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objExperienceInfo.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "UserStatus":
                        if (!Convert.IsDBNull(objDataReader["UserStatus"]))
                        {
                            objExperienceInfo.UserStatus = objDataReader["UserStatus"].ToString();
                        }
                        break;
                    case "CreatedBy":
                        if (!Convert.IsDBNull(objDataReader["CreatedBy"]))
                        {
                            objExperienceInfo.CreatedBy = Convert.ToInt16(objDataReader["CreatedBy"]);
                        }
                        break;
                    case "CreatedDate":
                        if (!Convert.IsDBNull(objDataReader["CreatedDate"]))
                        {
                            objExperienceInfo.CreatedDate = Convert.ToDateTime(objDataReader["CreatedDate"].ToString());
                        }
                        break;
                    case "UpdatedBy":
                        if (!Convert.IsDBNull(objDataReader["UpdatedBy"]))
                        {
                            objExperienceInfo.UpdatedBy = Convert.ToInt16(objDataReader["UpdatedBy"].ToString());
                        }
                        break;
                    case "UpdatedDate":
                        if (!Convert.IsDBNull(objDataReader["UpdatedDate"]))
                        {
                            objExperienceInfo.UpdatedDate = Convert.ToDateTime(objDataReader["UpdatedDate"].ToString());
                        }
                        break;
                    case "SortedBy":
                        if (!Convert.IsDBNull(objDataReader["SortedBy"]))
                        {
                            objExperienceInfo.SortedBy = Convert.ToByte(objDataReader["SortedBy"].ToString());
                        }
                        break;
                    case "Remarks":
                        if (!Convert.IsDBNull(objDataReader["Remarks"]))
                        {
                            objExperienceInfo.Remarks = objDataReader["Remarks"].ToString();
                        }
                        break;

                    default:
                        break;
                }
            }
        }
        public List<Experience> GetAllExperienceInfo(int employeeId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Experience> objExperienceInfoList = new List<Experience>();
            Experience objExperienceInfo;

            try
            {
                objDbCommand.AddInParameter("EmployeeId", employeeId);
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetExperienceInfoList",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objExperienceInfo = new Experience();
                        this.BuildModelForExperience(objDbDataReader, objExperienceInfo);
                        objExperienceInfoList.Add(objExperienceInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objExperienceInfoList;
        }

        public List<Experience> GetAllExperienceInfo()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<Experience> objExperienceInfoList = new List<Experience>();
            Experience objExperienceInfo;

            try
            {
              //  objDbCommand.AddInParameter("EmployeeId", employeeId);
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].uspGetAllExperienceInfoList",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objExperienceInfo = new Experience();
                        this.BuildModelForExperience(objDbDataReader, objExperienceInfo);
                        objExperienceInfoList.Add(objExperienceInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objExperienceInfoList;
        }
        public string SaveExperienceInfo(Experience objExperienceInfo)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("EmployeeId", objExperienceInfo.EmployeeId);
            objDbCommand.AddInParameter("OrganizationName", objExperienceInfo.OrganizationName);
            objDbCommand.AddInParameter("Position", objExperienceInfo.Position);
            objDbCommand.AddInParameter("JoiningDate", objExperienceInfo.JoiningDate);
            objDbCommand.AddInParameter("EndingDate", objExperienceInfo.EndingDate);
            objDbCommand.AddInParameter("Responsibility", objExperienceInfo.Responsibility);

            //objDbCommand.AddInParameter("CreatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspCreateExperience", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string UpdateExperienceInfo(Experience objExperienceInfo)
        {

            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("ExperienceId", objExperienceInfo.ExperienceId);
            objDbCommand.AddInParameter("OrganizationName", objExperienceInfo.OrganizationName);
            objDbCommand.AddInParameter("Position", objExperienceInfo.Position);
            objDbCommand.AddInParameter("JoiningDate", objExperienceInfo.JoiningDate);
            objDbCommand.AddInParameter("EndingDate", objExperienceInfo.EndingDate);
            objDbCommand.AddInParameter("Responsibility", objExperienceInfo.Responsibility);
            //objDbCommand.AddInParameter("IsActive", objExperienceInfo.IsActive);
            //objDbCommand.AddInParameter("UpdatedBy", SessionUtility.STSessionContainer.UserID);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspUpdateExperience", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Save Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Save Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }

        public string DeleteExperienceInfo(int id)
        {
            int noRowCount = 0;

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("ExperienceId", id);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].uspDeleteExperience", CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Delete Successfully";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "Delete Failed";
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
        }
    }
}