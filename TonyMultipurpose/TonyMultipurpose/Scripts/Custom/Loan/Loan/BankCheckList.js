$('#UpdateBankCheckList').hide();
$('#deleteBankCheckList').hide();
// Load Function function
var oBankCheckListTable;
BankCheckListTable();
function BankCheckListTable() {
    oBankCheckListTable = $('#BankCheckListTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllBankCheckListInfo",
            "type": "get",
            "data": {
                "LoanId": $('#BankCheckList_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "BankName" },
            { "data": "AccountNumber" },
            { "data": "ChequeId" },
            { "data": "Description" },
            {
                "data": "Path",
                "render": function (data, type, row) {
                    return '<img src="' + data + '" width="40px" height="40px"/>';
                }
            }
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false
    });
    $('#BankCheckListTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            oBankCheckListTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        bindDataBankCheckList(oBankCheckListTable.row(this).data());
    });
}
function bindDataBankCheckList(data) {
    $('#hdfBankCheckListId').val(data.BankCheckListId);
    $('#LoanId').val(data.LoanId);
    $('#BankId').val(data.BankId);
    $('#BankCheckList_AccountNumber').val(data.AccountNumber);
    $('#ChequeId').val(data.ChequeId);
    $('#Description').val(data.Description);
    $('#imageDisplay').attr('src', data.Path);
    $('#hiddenChequeImagePath').val(data.Path),
    $('#saveBankCheckList').hide();
    $('#UpdateBankCheckList').show();
    $('#deleteBankCheckList').show();
}
//function BankCheckListClearAll() {
//    $('#BankCheckList_TypeId').val('');
//    $('#BankCheckList_Amount').val('');
//    $('#saveBankCheckList').show();
//    $('#UpdateBankCheckList').hide();
//    $('#deleteBankCheckList').hide();
//}
//// caler function
//$('#BankCheckList_Clear').click(function () {
//    BankCheckListClearAll();
//});
//// data load function
function Binddata() {
    var data = {
        BankCheckListId: $('#hdfBankCheckListId').val(),
        LoanId: $('#BankCheckList_LoanId').val(),
        BankId: $('#BankId option:selected').val(),
        ChequeId: $('#ChequeId').val(),
        Description: $('#Description').val(),
        AccountNumber: $('#BankCheckList_AccountNumber').val(),
        Path: 'arya',
    }
    return data;
}
//income type load function
BankName();
function BankName() {
    $("#BankId").empty();
    $.ajax({
        type: "GET",
        url: window.applicationBaseUrl + "Loan/Loan/GetBankName",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#BankId").append('<option value="0">--Select Bank Name--</option>');
            $.each(data, function (key, value) {
                $("#BankId").append('<option value=' + value.BankId + '>' + value.BankName + '</option>');
            });
        }
    });
}
$("#Path").change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imageDisplay').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});
// Save function
$('#saveBankCheckList').click(function () {
    var isAllDataValid = true;
    var newImage = 'arya';
    var data = {
        LoanId: $('#BankCheckList_LoanId').val(),
        BankId: $('#BankId option:selected').val(),
        ChequeId: $('#ChequeId').val(),
        Description: $('#Description').val(),
        AccountNumber: $('#BankCheckList_AccountNumber').val(),
        Path: $('#hiddenChequeImagePath').val(),
    }
    if (isAllDataValid) {
        var file = document.getElementById('Path').files[0];

        //var data = Binddata();
        if (file != null) {
            data.Path = 'Cersi';
            NewImage = 'Cersi';
        }
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/BankCheckListSave",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status === true) {
                    alert('successfully saved');
                    if (NewImage !== 'arya') {
                        var fd = new FormData();
                        fd.append("Imagefile", document.getElementById('Path').files[0]);
                        fd.append("ChequeId", d.Id);
                        fd.append("Path", d.image);
                        $('#hiddenChequeImagePath').val(d.image);
                        $.ajax({
                            url: window.applicationBaseUrl + "Loan/Loan/SaveImageFile",
                            type: "POST",
                            data: fd,
                            dataType: "JSON",
                            contentType: false,
                            processData: false,
                            success: function (obj) {
                                if (obj.status == true) {
                                    //alert('successfully saved');
                                    $('#hiddenChequeImagePath').val(obj.Path);
                                    oBankCheckListTable.ajax.reload();
                                    ClearAllData();
                                } else {
                                    alert("failed");
                                }
                            },
                            error: function () {
                                alert('Error. Please try again.');

                            }
                        });
                    }
                } else {
                    alert('Failed');
                }
                //$('#submit').val('Save');

            },
            error: function () {
                alert('Error. Please try again.');
                //$('#submit').val('Save');

            }
        });
    }
});

function ClearAllData() {
    $('#BankId').val('');
    $('#ChequeId').val('');
    $('#Description').val('');
    $('#BankCheckList_AccountNumber').val('');
    $('#Path').val('');
    $('#imageDisplay').attr('src', '');

    $('#UpdateBankCheckList').hide();
    $('#deleteBankCheckList').hide();
}
$('#BankCheckList_Clear').click(function () {
    ClearAllData();
    $('#UpdateBankCheckList').hide();
    $('#deleteBankCheckList').hide();
    $('#saveBankCheckList').show();
});
$('#UpdateBankCheckList').click(function () {
    var isAllDataValid = true;
    var newImage = 'arya';
    var data = {
        LoanId: $('#BankCheckList_LoanId').val(),
        BankId: $('#BankId option:selected').val(),
        ChequeId: $('#ChequeId').val(),
        Description: $('#Description').val(),
        AccountNumber: $('#BankCheckList_AccountNumber').val(),
        Path: $('#hiddenChequeImagePath').val(),
        BankCheckListId: $('#hdfBankCheckListId').val(),
    }
    if (isAllDataValid) {
        var file = document.getElementById('Path').files[0];

        //var data = Binddata();
        if (file != null) {
            data.Path = 'Cersi';
            NewImage = 'Cersi';
        }
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateBankCheckList",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status === true) {
                    alert('successfully Update');
                    if (NewImage !== 'arya') {
                        var fd = new FormData();
                        fd.append("Imagefile", document.getElementById('Path').files[0]);
                        fd.append("ChequeId", d.Id);
                        fd.append("Path", d.image);
                        $('#hiddenChequeImagePath').val(d.image);
                        $.ajax({
                            url: window.applicationBaseUrl + "Loan/Loan/SaveImageFile",
                            type: "POST",
                            data: fd,
                            dataType: "JSON",
                            contentType: false,
                            processData: false,
                            success: function (obj) {
                                if (obj.status == true) {
                                    $('#hiddenChequeImagePath').val(obj.Path);
                                    oBankCheckListTable.ajax.reload();
                                    ClearAllData();
                                    $('#saveBankCheckList').show();
                                    $('#UpdateBankCheckList').hide();
                                    $('#deleteBankCheckList').hide();
                                } else {
                                    alert("Image Update failed");
                                }
                            },
                            error: function () {
                                alert('Error. Please try again.');

                            }
                        });
                    }
                } else {
                    alert('Failed');
                }
                //$('#submit').val('Save');

            },
            error: function () {
                alert('Error. Please try again.');
                //$('#submit').val('Save');

            }
        });
    }
});
$('#deleteBankCheckList').click(function () {
    if (confirm("Are you sure?")) {
        var newImage = 'Cersi';
        var data = {
            ChequeId: $('#ChequeId').val(),
            Path: newImage,
            BankCheckListId: $('#hdfBankCheckListId').val(),
        }
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/DeleteBankCheckList",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    oBankCheckListTable.ajax.reload();
                    ClearAllData();
                    $('#saveBankCheckList').show();
                    $('#UpdateBankCheckList').hide();
                    $('#deleteBankCheckList').hide();
                }
            },
            error: function () {

            }
        });
    }
    return false;
});