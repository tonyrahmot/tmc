﻿using System;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TonyMultipurpose.Areas.Accounts.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.Controllers
{
    public class TermsAndConditionsController : Controller
    {
        private TermsAndConditionsBLL oTermsAndConditionsBLL;
        [HttpGet]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(TermsAndConditions oTermsAndConditions)
        {
            oTermsAndConditionsBLL = new TermsAndConditionsBLL();
            CommonResult oCommonResult = new CommonResult();
            oCommonResult = oTermsAndConditionsBLL.SaveTermsAndCondition(oTermsAndConditions);
            return new JsonResult { Data = new { oCommonResult} };
        }

        public JsonResult GetAccontTypeByAccountTitle()
        {
            AccountBLL objAccountBll = new AccountBLL();
            var data = objAccountBll.GetAccontTypeByAccountTitle();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTermsAndCondition(int accountTypeId)
        {
            oTermsAndConditionsBLL = new TermsAndConditionsBLL();
            var data = oTermsAndConditionsBLL.GetTermsAndCondition(accountTypeId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}