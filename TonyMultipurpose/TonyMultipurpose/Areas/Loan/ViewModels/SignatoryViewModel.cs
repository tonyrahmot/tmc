﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class SignatoryViewModel : CommonModel
    {
        public int Id { get; set; }
        public string LoanId { get; set; }

        [Display(Name = "Name of Officer")]
        public string EmpCode { get; set; }

        [Display(Name = "Functional Position")]
        public string FunctionalPosition { get; set; }

        [Display(Name = "Designation")]
        public string DesignationName { get; set; }
        public IEnumerable<SelectListItem> EmployeeNameSelectListItems { get; set; }

    }
}