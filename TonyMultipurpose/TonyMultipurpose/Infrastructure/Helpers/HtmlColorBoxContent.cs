﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlColorBoxContent : IDisposable
    {
        private readonly ViewContext _viewContext;

        public HtmlColorBoxContent(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public void Dispose()
        {
            _viewContext.Writer.Write(
                "More info <i class=\"fa fa-arrow-circle-right\"></i>" +
                "</a>" +
                "</div></div>"
            );
        }
    }
}