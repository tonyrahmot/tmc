﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class LoanBasicViewModel
    {
        [Display(Name = "Proposal No")]
        public string LoanId { get; set; }

        [Display(Name = "")]
        public string LoanNo { get; set; }

        [Display(Name = "Loan Type")]
        public byte LoanTypeId { get; set; }

        [Display(Name = "Referencee Name")]
        public string ReferenceName { get; set; }

        [Display(Name = "Date")]
        public string LoanDate { get; set; }

        [Display(Name = "Customer ID")]
        public string CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Loan Request Amount")]
        public decimal? LoanAmount { get; set; }

        [Display(Name = "Interest Rate")]
        public decimal? InterestRate { get; set; }

        [Display(Name = " Installment Type")]
        public byte? InstallmentTypeId { get; set; }

        [Display(Name = "Installment No")]
        public byte? NumberOfInstallment { get; set; }

        [Display(Name = "Extra Deposit Tk")]
        public decimal? ExtraFee { get; set; }

        [Display(Name = "Purpose")]
        public string PurposeOfLoan { get; set; }

        [Display(Name = "Purpose Cost Tk")]
        public decimal? TotalAssetCost { get; set; }

        [Display(Name = "Supplier")]
        public string DistributorName { get; set; }

        [Display(Name = "Residential Condition")]
        public byte? ResidentialConditionId { get; set; }
        //-------------------------------------------------------------------

        public string LoanExpiryDate { get; set; }

        public decimal? InterestAmount { get; set; }
        public decimal? TotalAmountWithInterest { get; set; }
        public decimal? InstallmentAmount { get; set; }



        public string TotalPurchaseAmount { get; set; }




        public string Message { get; set; }
        public string Remarks { get; set; }
        public string RejectReason { get; set; }
        public bool IsEntry { get; set; }
        public bool IsChecked { get; set; }
        public bool IsApproved { get; set; }

        public bool IsCleared { get; set; }
        public bool IsSubmittedToCheck { get; set; }
        public bool IsRejected { get; set; }
        public bool IsClosed { get; set; }
        public Int16 CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public Int16? UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public Int16 BranchId { get; set; }
        public Int16 CompanyId { get; set; }


        //public string InstallmentTypeName { get; set; }
        //public string LoanTypeName { get; set; }
        public string UserName { get; set; }
        public int? COAId { get; set; }
        public string AccountCode { get; set; }
        public bool IsDepositsFromSaving { get; set; }

        /****************************/

        public decimal? TotalInterest { get; set; }
        public decimal? BeginningBalance { get; set; }
        public decimal? EndingBalance { get; set; }
        public decimal? Principle { get; set; }
        public decimal? Interest { get; set; }
        public decimal? DBRatio { get; set; }
        public decimal? NetIncome { get; set; }
        public decimal? ExistingEmi { get; set; }

        /****************************/


        //public string ResidentialConditionType { get; set; }

        public int AccountSetupId { get; set; }
        public string AccountTypeName { get; set; }

        /****************************************************/

        public decimal? WeeklyInstallmentAmount { get; set; }
        public decimal? MonthlyInstallmentAmount { get; set; }
        public byte? WeeklyInstallmentNumber { get; set; }
        public byte? MonthlyInstallmentNumber { get; set; }
        public decimal? WeeklyDbr { get; set; }
        public decimal? MonthlyDbr { get; set; }

        public decimal? Equity { get; set; }


        public IEnumerable<SelectListItem> LoanTypeSelectListItems { get; set; }
        public IEnumerable<SelectListItem> InstallmentTypeSelectListItems { get; set; }
        public IEnumerable<SelectListItem> AccountNumberSelectListItems { get; set; }
        public IEnumerable<SelectListItem> ResidentialConditionSelectListItems { get; set; }
    }
}