﻿
$(document).ready(function () {

    $('#TransactionDate').datepicker({
        prevText: '<i class="fa fa-arrow-left"></i>',
        nextText: '<i class="fa fa-arrow-right"></i>',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/M/yy'
    });

    $(".ReverseButton").click(function () {
        if (confirm('Are you sure you want to reverse this thing into the database?')) {
            var transactionId = $(this).prop('id');
            var data = {
                transactionIdToReverse: transactionId
            }
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/ReverseEntryFT/ReverseEntryFinancialTransaction", //'@Url.Action("ReverseEntryFinancialTransaction", "ReverseEntryFT")',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        alert('Successfully done.');
                    } else {
                        alert('Failed');
                    }
                    window.location.reload();
                },
                error: function () {
                    alert('Error. Please try again.');
                    //$('#submit').val('Save');
                }
            });
        } else {
            alert("Not Done");
        }
    });

    $("#search").keyup(function () {
        filter = new RegExp($(this).val(), 'i');
        $("#checkableGrid tbody tr").filter(function () {
            $(this).each(function () {
                found = false;
                $(this).children().each(function () {
                    content = $(this).html();
                    if (content.match(filter)) {
                        found = true;
                    }
                });
                if (!found) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });
    });
});
