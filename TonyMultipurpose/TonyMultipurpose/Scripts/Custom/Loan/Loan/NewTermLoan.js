$(function () {
    $("#tabs").tabs();
});
(function () {
    $('#submitForCheck').attr('disabled', 'disabled');
    var oTable;
    $(document).ready(function () {
        loadTable();
    });
    function loadTable() {
        oTable = $('#LoanTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/Save",
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "LoanId" },
                { "data": "AccountTypeName" },
                { "data": "LoanDate" },
                //{ "data": "LoanExpiryDate" },
                { "data": "CustomerName" },
                { "data": "AccountNumber" },
                { "data": "LoanAmount" },
                { "data": "InterestRate" },
                { "data": "InstallmentAmount" },
                { "data": "InstallmentTypeName" },
                { "data": "UserName" },
                {
                    "data": "LoanId",
                    "width": "50px",
                    "render": function (data) {
                        var url = window.applicationBaseUrl + "Loan/Loan/PrintLoanApplication?loanId=" + data;
                        return '<a class="btn btn-info" target="_blank" href="' + url +'">Print</a>';
                    }
                }
            ],
            "order": [[0, "desc"]],

        });
        $('#LoanTable tbody').on('click', 'tr', function () {
            $('#submitForCheck').removeAttr('disabled');
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());

        });
    }



    function bindDataToControls(data) {
        $('#submit').val('Update');
        $('#CustomerId').val(data.CustomerId);
        $("#CustomerId").change();
        $(document).ajaxComplete(function () {
            $('#AccountNumber').val(data.AccountNumber);
        });
        document.getElementById("LoanType").disabled = true;
        $('#LoanType').val(data.AccountSetupId);
        $('#RefId').val(data.LoanId);
        $('#hdfLoanId').val(data.LoanId);
        $('#RefName').val(data.ReferenceName);
        $('#LoanDate').val(data.LoanDate);
        $('#LoanAmount').val(data.LoanAmount);
        $('#InterestRate').val(data.InterestRate);
        $('#InstallmentTypeId').val(data.InstallmentTypeId);
        $('#NumberOfInstallment').val(data.NumberOfInstallment);
        $('#ExtraFee').val(data.ExtraFee);
        $('#PurposeOfLoan').val(data.PurposeOfLoan);
        $('#TotalPurchaseAmount').val(data.TotalPurchaseAmount);
        $('#TotalAssetCost').val(data.TotalAssetCost);
        $('#DistributorName').val(data.DistributorName);
        $('#LoanExpiryDate').val(data.LoanExpiryDate);
        $('#LoanExpiryDate').val(data.LoanExpiryDate);
        //$('#COA_accountName').val(data.COAId);
        $('#ResidentialCondition').val(data.ResidentialConditionId);
        if (data.IsDepositsFromSaving == true) {
            $("#IsDepositsFromSaving").attr('checked', true);
        }
        else {
            $("#IsDepositsFromSaving").attr('checked', false);
        }
        $("#LoanAmount").keyup();
        //document.getElementById('labelweeklyLoanAmount').innerHTML = data.LoanAmount;
        //document.getElementById('labelweeklyInterestRate').innerHTML = data.InterestRate;
        //document.getElementById('labelweeklyNoInstallment').innerHTML = data.NumberOfInstallment;
        //document.getElementById('labelweeklyInstallmentAmount').innerHTML = data.InstallmentAmount;
        //document.getElementById('labelweeklyInstallmentType').innerHTML = data.InstallmentTypeName;

        //$('#submit').val('Update');

    }

    $("#print").click(function () {

    });



    // load Loan Type
    $(function () {
        $("#LoanType").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetLoanTypeByAccountTitle",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#LoanType").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#LoanType").append('<option value=' + value.AccountSetupId + '>' + value.AccountTypeName + '</option>');
                });
            }
        });
    });

    $(function () {
        $("#ResidentialCondition").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetResidentialConditionInfo",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#ResidentialCondition").append('<option value="0">--Select Type--</option>');
                $.each(data, function (key, value) {
                    $("#ResidentialCondition").append('<option value=' + value.ResidentialConditionId + '>' + value.ResidentialConditionType + '</option>');
                });
            }
        });
    });

    // Get loan id
    $("#LoanType").change(function () {
        var loanType = $("#LoanType").val();
        var json = {
            loanType: loanType
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetLoanId",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#RefId').val(data.loanId);
                loadReferrencdData();
                $('#InterestRate').val(data.interestInfo.InterestRate);
            }
        });
    });

    function loadReferrencdData() {
        var loanType = $("#LoanType").val();
        var json = {
            loanType: loanType
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetRefName",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#RefName').val(data);
            }
        });
    }

    // Load installment type
    $(function () {
        $("#InstallmentTypeId").empty();
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetInstallmentType",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#InstallmentTypeId").append('<option value="0">--Select installment Type--</option>');
                $.each(data, function (key, value) {
                    $("#InstallmentTypeId").append('<option value=' + value.InstallmentTypeId + '>' + value.InstallmentTypeName + '</option>');
                });
            }
        });
    });

    // AutoComplete Customer Id
    $("#CustomerId").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/AutoCompleteCustomerId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });
    // Get customer Information
    $("#CustomerId").change(function () {
        $('#AccountNumber').empty();
        var customerId = $("#CustomerId").val();
        var json = { customerId: customerId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetCustomerInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#CustomerInfo').show();
                $('#customerImage').attr('src', data.CustomerImage);

                $('#CustomerName').val(data.CustomerName);
                document.getElementById('LabelCustomerName').innerHTML = data.CustomerName;
                document.getElementById('FatherName').innerHTML = data.FatherName;
                document.getElementById('LabelMotherName').innerHTML = data.MotherName;
                document.getElementById('LabelSpouseName').innerHTML =data.SpouseName;
                document.getElementById('LabelDateOfBirth').innerHTML = FormateDate(data.DateOfBirth);
                document.getElementById('LabelNationalId').innerHTML = data.NationalId;
                document.getElementById('LabelOccupationAndPosition').innerHTML = data.OccupationAndPosition;
                document.getElementById('LabelPresentAddress').innerHTML = data.PresentAddress;
                document.getElementById('LabelPermanentAddress').innerHTML = data.PermanentAddress;
                document.getElementById('LabelPosition').innerHTML = data.Position;
                document.getElementById('LabelProfession').innerHTML = data.CustomerIncomeSource;
                GetAccountNumber(customerId);
            }
        });
    });

    function GetAccountNumber(customerId) {
        $("#AccountNumber").empty();
        var json = { customerId: customerId };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/Loan/GetSavingsAccounNumber",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $("#AccountNumber").append('<option value="0">--Select Account Number--</option>');
                $.each(data, function (key, value) {
                    $("#AccountNumber").append('<option value=' + value.AccountNumber + '>' + value.AccountName + '</option>');
                });
            }
        });
    }

    $('#submit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var data = Bindata();

            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/Save",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    $('#hdfLoanId').val('');
                    if (d.oCommonResult.Status == true) {
                        oTable.ajax.reload();
                        $('#submit').val('Save');
                        document.getElementById('showMessage').style.color = "green";
                        $('#hdfLoanId').val(data.LoanId);
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanId').val('');
                }
            });

        }
    });

    $('#submitForCheck').click(function () {
        if (true) {
            var loanId = $('#hdfLoanId').val();
            var data = { loanId: loanId }

            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/SubmitForCheck",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    $('#hdfLoanId').val('');
                    if (d.oCommonResult.Status == true) {
                        window.location.reload();
                        //document.getElementById('showMessage').style.color = "green";
                    }
                    // document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;

                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                    $('#hdfLoanId').val('');
                }

            });

        }
    });

    $('#clear').click(function () {
        window.location.reload();
    });
    function Bindata() {
        var loanAmount = parseInt($('#LoanAmount').val());
        var interestRate = parseFloat($('#InterestRate').val()) / 100;
        var numberOfInstallment = parseInt($('#NumberOfInstallment').val());

        if ($('#InstallmentTypeId').val() == 2) {
            var interestAmount = Math.round((loanAmount * interestRate) / 12);
            var r = parseFloat($('#InterestRate').val()) / 1200;
            var a = parseFloat(loanAmount * r);
            var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
            var c = 1 - b;
            var installmentAmount = Math.round(a / c);
            var principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
            var totalInterest = Math.round(principleWithInterest - loanAmount);

        }
        if ($('#InstallmentTypeId').val() == 1) {
            var interestAmount = parseFloat((loanAmount * interestRate) / 52);
            var r = parseFloat($('#InterestRate').val()) / 5200;
            var a = parseFloat(loanAmount * r);
            var b = parseFloat(Math.pow(1 + r, -numberOfInstallment));
            var c = 1 - b;
            var installmentAmount = Math.round(a / c);
            var principleWithInterest = parseFloat(installmentAmount * numberOfInstallment);
            var totalInterest = Math.round(principleWithInterest - loanAmount);
        }

        var IsDeposits;
        if ($('#IsDepositsFromSaving').prop("checked") === true) {
            IsDeposits = 1;
        }
        else {
            IsDeposits = 0;
        }
        var data = {
            IsDepositsFromSaving: IsDeposits,
            //COAId: $('#COA_accountName option:selected').val(),
            AccountSetupId: $('#LoanType option:selected').val(),
            LoanId: $('#RefId').val(),
            ReferenceName: $('#RefName').val(),
            CustomerId: $('#CustomerId').val(),
            AccountNumber: $('#AccountNumber option:selected').val(),
            LoanDate: $('#LoanDate').val(),
            LoanAmount: $('#LoanAmount').val(),
            InterestRate: $('#InterestRate').val(),
            InstallmentTypeId: $('#InstallmentTypeId option:selected').val(),
            NumberOfInstallment: $('#NumberOfInstallment').val(),
            ExtraFee: $('#ExtraFee').val(),
            PurposeOfLoan: $('#PurposeOfLoan').val(),
            TotalPurchaseAmount: $('#TotalPurchaseAmount').val(),
            TotalAssetCost: $('#TotalAssetCost').val(),
            DistributorName: $('#DistributorName').val(),
            LoanExpiryDate: $('#LoanExpiryDate').val(),
            InterestAmount: interestAmount,
            InstallmentAmount: installmentAmount,
            TotalInterest: totalInterest,
            ResidentialConditionId: $('#ResidentialCondition option:selected').val()
        }

        return data;

    }
    function CheckValidation() {
        var isValid = true;
        if ($('#LoanType').val().trim() === '0') {
            isValid = false;
            $('#LoanType').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#LoanType').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#RefId').val().trim() === '') {
            isValid = false;
            $('#RefId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#RefId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#CustomerId').val().trim() === '') {
            isValid = false;
            $('#CustomerId').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#CustomerId').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#AccountNumber').val() === '0') {
            isValid = false;
            $('#AccountNumber').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#AccountNumber').parent().prev().find('span').css('visibility', 'hidden');
        }
        //if ($('#COA_accountName').val().trim() === '0') {
        //    isValid = false;
        //    $('#COA_accountName').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#COA_accountName').parent().prev().find('span').css('visibility', 'hidden');
        //}

        //if ($('#InstallmentTypeId').val() === '0') {
        //    isValid = false;
        //    $('#InstallmentTypeId').parent().prev().find('span').css('visibility', 'visible');
        //}
        //else {
        //    $('#InstallmentTypeId').parent().prev().find('span').css('visibility', 'hidden');
        //}
        return isValid;
    }

    //json to date converter
    function FormateDate(jsonDate) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
        ];
        var date = new Date(parseInt(jsonDate.substr(6)));
        var dd = date.getDate();
        var mm = monthNames[date.getMonth()];
        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var formmatedDate = dd + '/' + mm + '/' + yyyy;
        return formmatedDate;
    }

    //date picker
    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#LoanDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });
    $('#LoanExpiryDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });


    $('#gaurantorInfoTab').on('click', function() {
        $('#gaurantorInfo').load(window.applicationBaseUrl + "Loan/Loan/Guarantor?loanId=" + $('#hdfLoanId').val());
    });


    $('#commentsAndSignatoryInfoTab').on('click', function() {
        $('#CommentsAndSignatory').load(window.applicationBaseUrl + "Loan/Loan/CommentsAndSignatory?loanId=" + $('#hdfLoanId').val());
    });



    $('#financialInfoTab').on('click', function () {
        $('#Financial').load(window.applicationBaseUrl + "Loan/Loan/FinancialJustification?loanId=" + $('#hdfLoanId').val());
        });

    $('#customersChequeInfoTab').on('click', function () {
        $('#CustomersCheque').load(window.applicationBaseUrl + "Loan/Loan/CustomersChequeInfo?loanId=" + $('#hdfLoanId').val());
    });

    $("#LoanAmount").keyup(function () {
        var amount = parseFloat($('#LoanAmount').val().replace(/,/g, ''));
        var user = $.trim(amount);
        if (user.length >= 1) {
            $.post(window.applicationBaseUrl + "Transaction/Transaction/GetConvertNumberToWord", { number: amount },
                        function (data) {
                            var money = data+'Tk Only';
                            $('#Inwords').val(money);

                        });
        } else {

        }
    });

})();

function IsInteger(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else if (charCode == 13 || charCode == 46) {
        return false;
    }
    status = "";
    return true;
}

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode == 13) {
        return false;
    }
    status = "";
    return true;
}