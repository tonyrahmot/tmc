﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.Areas.Setup.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Controllers
{
    [AuthenticationFilter]
    public class HomeController : Controller
    {
        NoticeInfoBLL objNoticeBll;
        public ActionResult Index()
        {
            objNoticeBll = new NoticeInfoBLL();
            //            var notices = objNoticeBll.GetAllNoticeInfo();
            var notices = objNoticeBll.GetAllNoticeInfo();
            if (SessionUtility.TMSessionContainer.UserRole != 2)
            {
                notices = notices.Where(n => n.RoleId == 0 || n.RoleId == SessionUtility.TMSessionContainer.UserRole)
                    .ToList();
            }
            return View(notices);
        }
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}