﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Areas.Transaction.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Transaction.BLL
{
    public class TransactionBLL
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        private void BuildModelForTransaction(DbDataReader objDataReader, TransactionModel objTransactionModel)
        {
            DataTable objDataTable = objDataReader.GetSchemaTable();
            foreach (DataRow dr in objDataTable.Rows)
            {
                String column = dr.ItemArray[0].ToString();
                switch (column)
                {
                    case "TransactionId":
                        if (!Convert.IsDBNull(objDataReader["TransactionId"]))
                        {
                            objTransactionModel.TransactionId = objDataReader["TransactionId"].ToString();
                        }
                        break;

                    case "Id":
                        if (!Convert.IsDBNull(objDataReader["Id"]))
                        {
                            objTransactionModel.Id = Convert.ToInt64(objDataReader["Id"].ToString());
                        }
                        break;

                    case "TransactionDate":
                        if (!Convert.IsDBNull(objDataReader["TransactionDate"]))
                        {
                            objTransactionModel.TransactionDate = Convert.ToDateTime(objDataReader["TransactionDate"].ToString());
                        }
                        break;

                    case "Username":
                        if (!Convert.IsDBNull(objDataReader["Username"]))
                        {
                            objTransactionModel.Username = objDataReader["Username"].ToString();
                        }
                        break;

                    case "AccountNumber":
                        if (!Convert.IsDBNull(objDataReader["AccountNumber"]))
                        {
                            objTransactionModel.AccountNumber = objDataReader["AccountNumber"].ToString();
                        }
                        break;
                    case "TransactionType":
                        if (!Convert.IsDBNull(objDataReader["TransactionType"]))
                        {
                            objTransactionModel.TransactionType = objDataReader["TransactionType"].ToString();
                        }
                        break;
                    case "CustomerId":
                        if (!Convert.IsDBNull(objDataReader["CustomerId"]))
                        {
                            objTransactionModel.CustomerId = objDataReader["CustomerId"].ToString();
                        }
                        break;
                    case "CustomerName":
                        if (!Convert.IsDBNull(objDataReader["CustomerName"]))
                        {
                            objTransactionModel.CustomerName = objDataReader["CustomerName"].ToString();
                        }
                        break;
                    case "FatherName":
                        if (!Convert.IsDBNull(objDataReader["FatherName"]))
                        {
                            objTransactionModel.FatherName = objDataReader["FatherName"].ToString();
                        }
                        break;
                    case "BranchId":
                        if (!Convert.IsDBNull(objDataReader["BranchId"]))
                        {
                            objTransactionModel.BranchId = Convert.ToInt16(objDataReader["BranchId"].ToString());
                        }
                        break;
                    case "BranchName":
                        if (!Convert.IsDBNull(objDataReader["BranchName"]))
                        {
                            objTransactionModel.BranchName = objDataReader["BranchName"].ToString();
                        }
                        break;
                    case "CompanyId":
                        if (!Convert.IsDBNull(objDataReader["CompanyId"]))
                        {
                            objTransactionModel.CompanyId = Convert.ToInt16(objDataReader["CompanyId"].ToString());
                        }
                        break;
                    case "CompanyName":
                        if (!Convert.IsDBNull(objDataReader["CompanyName"]))
                        {
                            objTransactionModel.CompanyName = objDataReader["CompanyName"].ToString();
                        }
                        break;
                    case "AccountSetupId":
                        if (!Convert.IsDBNull(objDataReader["AccountSetupId"]))
                        {
                            objTransactionModel.AccountSetupId = Convert.ToInt32(objDataReader["AccountSetupId"].ToString());
                        }
                        break;
                    case "AccountTypeName":
                        if (!Convert.IsDBNull(objDataReader["AccountTypeName"]))
                        {
                            objTransactionModel.AccountTypeName = objDataReader["AccountTypeName"].ToString();
                        }
                        break;
                    case "Debit":
                        if (!Convert.IsDBNull(objDataReader["Debit"]))
                        {
                            objTransactionModel.Debit = Convert.ToDecimal(objDataReader["Debit"].ToString());
                        }
                        break;
                    case "Credit":
                        if (!Convert.IsDBNull(objDataReader["Credit"]))
                        {
                            objTransactionModel.Credit = Convert.ToDecimal(objDataReader["Credit"].ToString());
                        }
                        break;
                    case "InterestRate":
                        if (!Convert.IsDBNull(objDataReader["InterestRate"]))
                        {
                            objTransactionModel.InterestRate = Convert.ToDecimal(objDataReader["InterestRate"].ToString());
                        }
                        break;
                    case "AccruedBalance":
                        if (!Convert.IsDBNull(objDataReader["AccruedBalance"]))
                        {
                            objTransactionModel.AccruedBalance = Convert.ToDecimal(objDataReader["AccruedBalance"].ToString());
                        }
                        break;
                    case "Balance":
                        if (!Convert.IsDBNull(objDataReader["Balance"]))
                        {
                            objTransactionModel.Balance = Convert.ToDecimal(objDataReader["Balance"].ToString());
                        }
                        break;
                    case "Amount":
                        if (!Convert.IsDBNull(objDataReader["Amount"]))
                        {
                            objTransactionModel.Amount = Convert.ToDecimal(objDataReader["Amount"].ToString());
                        }
                        break;
                    case "Particular":
                        if (!Convert.IsDBNull(objDataReader["Particular"]))
                        {
                            objTransactionModel.Particular = objDataReader["Particular"].ToString();
                        }
                        break;
                    case "IsApproved":
                        if (!Convert.IsDBNull(objDataReader["IsApproved"]))
                        {
                            objTransactionModel.IsApproved = Convert.ToBoolean(objDataReader["IsApproved"].ToString());
                        }
                        break;
                    case "IsActive":
                        if (!Convert.IsDBNull(objDataReader["IsActive"]))
                        {
                            objTransactionModel.IsActive = Convert.ToBoolean(objDataReader["IsActive"].ToString());
                        }
                        break;
                    case "VoucherNo":
                        if (!Convert.IsDBNull(objDataReader["VoucherNo"]))
                        {
                            objTransactionModel.VoucherNo = Convert.ToString(objDataReader["VoucherNo"].ToString());
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        public List<TransactionModel> GetCustomerInfoByAccountNumber(string id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<TransactionModel> objTransactionModelList = new List<TransactionModel>();
            TransactionModel objTransactionModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("AccountNumber", id);
                // objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetCustomerInfoByAccountNumber]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTransactionModel = new TransactionModel();
                        //this.BuildModelForTransaction(objDbDataReader, objTransactionModel);
                        objTransactionModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objTransactionModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objTransactionModel.AccountSetupId = Convert.ToInt32(objDbDataReader["AccountSetupId"].ToString());
                        objTransactionModel.AccountTypeName = objDbDataReader["AccountTypeName"].ToString();
                        objTransactionModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                        objTransactionModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objTransactionModel.OpeningDate = Convert.ToDateTime(objDbDataReader["OpeningDate"].ToString());
                        objTransactionModel.BranchName = objDbDataReader["BranchName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objTransactionModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objTransactionModel.CustomerImage = "\\Not Found\\";
                        }

                        objTransactionModelList.Add(objTransactionModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objTransactionModelList;
        }

        public List<TransactionModel> GetCustomerInfoByAccountNumberForWithdraw(string id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<TransactionModel> objTransactionModelList = new List<TransactionModel>();
            TransactionModel objTransactionModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                objDbCommand.AddInParameter("AccountNumber", id);
                // objDbCommand.AddInParameter("IsDeleted", Customer.IsDeleted);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Transaction_CustomerInfoForWithdraw]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTransactionModel = new TransactionModel();
                        //this.BuildModelForTransaction(objDbDataReader, objTransactionModel);
                        objTransactionModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objTransactionModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objTransactionModel.AccountSetupId = Convert.ToInt32(objDbDataReader["AccountSetupId"].ToString());
                        objTransactionModel.AccountTypeName = objDbDataReader["AccountTypeName"].ToString();
                        objTransactionModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                        objTransactionModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objTransactionModel.OpeningDate = Convert.ToDateTime(objDbDataReader["OpeningDate"].ToString());
                        objTransactionModel.BranchName = objDbDataReader["BranchName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objTransactionModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objTransactionModel.CustomerImage = "\\Not Found\\";
                        }

                        objTransactionModelList.Add(objTransactionModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objTransactionModelList;
        }

        public TransactionModel GetSingleCustomerInfoByAccountNumberForWithdraw(string id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            var objTransactionModel = new TransactionModel();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Transaction_CustomerInfoForWithdraw]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTransactionModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objTransactionModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objTransactionModel.FatherName = objDbDataReader["FatherName"].ToString();
                        objTransactionModel.AccountSetupId = Convert.ToInt32(objDbDataReader["AccountSetupId"].ToString());
                        objTransactionModel.AccountTypeName = objDbDataReader["AccountTypeName"].ToString();
                        objTransactionModel.Balance = Convert.ToDecimal(objDbDataReader["Balance"].ToString());
                        objTransactionModel.InterestRate = Convert.ToDecimal(objDbDataReader["InterestRate"].ToString());
                        objTransactionModel.OpeningDate = Convert.ToDateTime(objDbDataReader["OpeningDate"].ToString());
                        objTransactionModel.BranchName = objDbDataReader["BranchName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads", StringComparison.Ordinal));
                            imagePath = "/" + imagePath;
                            objTransactionModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objTransactionModel.CustomerImage = "";
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                objDbDataReader?.Close();
                objDataAccess.Dispose(objDbCommand);
            }
            return objTransactionModel;
        }

        public string SaveTransaction(TransactionModel objTransactionModel)
        {
            int noOfAffacted = 0;
            string transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            //var prefix = string.Concat(year + days);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "Savings");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountNumber", objTransactionModel.AccountNumber);
            objDbCommand.AddInParameter("TransactionType", objTransactionModel.TransactionType);
            objDbCommand.AddInParameter("AccountTypeId", objTransactionModel.AccountSetupId);
            objDbCommand.AddInParameter("TransactionId", transactionId);
            objDbCommand.AddInParameter("InterestRate", objTransactionModel.InterestRate);
            objDbCommand.AddInParameter("Amount", objTransactionModel.Amount);
            objDbCommand.AddInParameter("TransactionDate", objTransactionModel.TransactionDate);
            objDbCommand.AddInParameter("Balance", objTransactionModel.Balance);
            objDbCommand.AddInParameter("IsApproved", objTransactionModel.IsApproved);
            objDbCommand.AddInParameter("IsActive", objTransactionModel.IsActive);
            objDbCommand.AddInParameter("BranchId", objTransactionModel.BranchId);
            objDbCommand.AddInParameter("CompanyId", objTransactionModel.CompanyId);
            objDbCommand.AddInParameter("CreatedBy", objTransactionModel.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objTransactionModel.CreatedDate);
            /**************************************************************************************/
            objDbCommand.AddInParameter("IsCashEntry", objTransactionModel.IsCashEntry);
            objDbCommand.AddInParameter("BankName", objTransactionModel.BankName);
            objDbCommand.AddInParameter("BankAccountNo", objTransactionModel.BankAccountNo);
            objDbCommand.AddInParameter("ChequeNo", objTransactionModel.ChequeNo);
            objDbCommand.AddInParameter("COAId", objTransactionModel.COAId);
            objDbCommand.AddInParameter("Particular", objTransactionModel.Particular);
            objDbCommand.AddInParameter("VoucherNo", objTransactionModel.VoucherNo);
            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspCreateTransaction]",
                    CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }


            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        /// <summary>
        /// saving closing account
        /// </summary>
        /// <param name="transactionModel"></param>
        /// <returns></returns>
        public string SaveSavingsAccountClosing(TransactionModel objTransactionModel)
        {
            int noOfAffacted = 0;
            string transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            //var prefix = string.Concat(year + days);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "Savings");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("AccountNumber", objTransactionModel.AccountNumber);
            objDbCommand.AddInParameter("AccountTypeId", objTransactionModel.AccountSetupId);
            objDbCommand.AddInParameter("TransactionId", transactionId);
            objDbCommand.AddInParameter("CustomerId", objTransactionModel.CustomerId);
            objDbCommand.AddInParameter("CustomerName", objTransactionModel.CustomerName);
            objDbCommand.AddInParameter("InterestRate", objTransactionModel.InterestRate);
            objDbCommand.AddInParameter("Amount", objTransactionModel.Amount);
            objDbCommand.AddInParameter("TransactionDate", objTransactionModel.TransactionDate);
            //objDbCommand.AddInParameter("OpeningDate", objTransactionModel.OpeningDate);
            objDbCommand.AddInParameter("IsApproved", objTransactionModel.IsApproved);
            objDbCommand.AddInParameter("IsActive", objTransactionModel.IsActive);
            objDbCommand.AddInParameter("BranchId", objTransactionModel.BranchId);
            objDbCommand.AddInParameter("CompanyId", objTransactionModel.CompanyId);
            objDbCommand.AddInParameter("CreatedBy", objTransactionModel.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objTransactionModel.CreatedDate);
            objDbCommand.AddInParameter("COAId", objTransactionModel.COAId);

            try
            {
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSavingsAccountClosing]", CommandType.StoredProcedure);

                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                    return "Saved";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }


            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }

        }

        //--Savings Transaction approval Start 

        //get IsApproved=false savings tarns. list
        public List<TransactionModel> GetSavingsTransactions(TransactionModel transactionModel)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<TransactionModel> listOfSavingsTransaction = new List<TransactionModel>();
            TransactionModel objTransactionModel;
            try
            {
                //objDbCommand.AddInParameter("CreatedBy", SessionUtility.TBSessionContainer.UserID);
                //objDbCommand.AddInParameter("IsApproved", Customer.IsApproved);
                objDbCommand.AddInParameter("TransactionType", transactionModel.TransactionType);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetSavingsTransactionsList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTransactionModel = new TransactionModel();
                        this.BuildModelForTransaction(objDbDataReader, objTransactionModel);

                        listOfSavingsTransaction.Add(objTransactionModel);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfSavingsTransaction;
        }
        public string ApproveSavingsTransaction(string[] ch)
        {

            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", id);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspApproveSavingsTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";

        }
        public string RejectSavingsTransaction(List<TransactionModel> transactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var item in transactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", item.TransactionId);
                objDbCommand.AddInParameter("RejectReason", item.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspRejectSavingsTransaction]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }
        public TransactionModel GetSavingsTransactionsById(int id)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            List<TransactionModel> listOfTransaction = new List<TransactionModel>();

            TransactionModel objTransactionModel = new TransactionModel();

            try
            {
                objDbCommand.AddInParameter("Id", id);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetSavingsTransactionsById]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objTransactionModel = new TransactionModel();
                        this.BuildModelForTransaction(objDbDataReader, objTransactionModel);
                        listOfTransaction.Add(objTransactionModel);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objTransactionModel;
        }
        public string GetConvertNumberToWord(long number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + GetConvertNumberToWord(Math.Abs(number));

            string words = "";
            if ((number / 1000000000) > 0)
            {
                words += GetConvertNumberToWord(number / 1000000000) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 10000000) > 0)
            {
                words += GetConvertNumberToWord(number / 10000000) + " Crore ";
                number %= 10000000;
            }

            if ((number / 1000000) > 0)
            {
                words += GetConvertNumberToWord(number / 1000000) + " Million ";
                number %= 1000000;
            }


            if ((number / 100000) > 0)
            {
                words += GetConvertNumberToWord(number / 100000) + " Lakh ";
                number %= 100000;
            }


            if ((number / 1000) > 0)
            {
                words += GetConvertNumberToWord(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += GetConvertNumberToWord(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
        //--Savings Transaction approval End 
        /// <summary>
        /// get all savings account number
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public List<string> GetAllAccountNumberForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objAccountNumberList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("AccountNumber", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetAccountNoForSB]",
                    CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["AccountNumber"] != null)
                        {
                            objAccountNumberList.Add(objDbDataReader["AccountNumber"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objAccountNumberList;
        }

        public List<COA> GetTransactionalCoaData()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<COA> listOfCoaType = new List<COA>();
            COA objChartOfAccount;

            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTransactionalCoaData]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objChartOfAccount = new COA();
                        if (objDbDataReader["COAId"] != null)
                        {
                            objChartOfAccount.COAId = Convert.ToInt32(objDbDataReader["COAId"].ToString());
                            objChartOfAccount.AccountCode = objDbDataReader["AccountCode"].ToString();

                            listOfCoaType.Add(objChartOfAccount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return listOfCoaType;
        }

        public bool GetAccountnumberLoanTable(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetAccountnumberLoanTable]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetUnApprovedTransactionByAcNo(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Transaction_GetUnApprovedTransactionByAcNo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }

        public bool GetCloseAccountTransaction(string AccountNumber)
        {
            bool status = false;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;

            try
            {
                objDbCommand.AddInParameter("AccountNumber", AccountNumber);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[Transaction_GetCloseAccountTransaction]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return status;
        }
    }
}