﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.Loan.Models
{
    public class LoanCustomerChequeInfo
    {
        [Key]
        public int LoanCustomerChequeInfoId { get; set; }
        public string LoanId { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
    }
}