﻿

$(document).ready(function () {

    $('#btn_submit').show();
    $('#updateBalanceSheet').hide();
    $('#BalanceSheet_Clear').hide();

    //Load Category on group change
    $("#GroupId").change(function () {
        $('#CategoryName').empty();
        var groupId = $("#GroupId").val();

        $.post(window.applicationBaseUrl + "Accounts/COA/GetCategoryNameGroupWise", { groupId: groupId },//'@Url.Action("GetCategoryNameGroupWise", "COA", new {Area = "Accounts" })', { groupId: groupId },
                        function (data) {
                            $("#CategoryName").append('<option value="0">--Select Category Name--</option>');
                            $.each(data, function (key, value) {
                                $("#CategoryName").append('<option value=' + value.Id + '>' + value.CategoryName + '</option>');
                            });
                        });
    });


    //check validation
    $('#btn_submit').click(function () {
        var categoryName = $("#CategoryName option:selected").val();
        var groupId = $("#GroupId").val();
        var balanceSheetName = $("#BalanceSheetName").val();
        var status = $("#showErrorMessage");
        var allData = true;
        if (groupId == '') {
            allData = false;
            status.html("Please Select Group!");
            return allData;
        }
        else if (categoryName == 0) {
            allData = false;
            status.html("Please Select Category!");
            return allData;
        }
        else if (balanceSheetName == '') {
            allData = false;
            status.html("Please Type Name!");
            return allData;
        }

    });




    $('#BalanceSheet_Clear').click(function () {
        ClearBalanceSheetInfo();
        $('#updateBalanceSheet').hide();
        $('#btn_submit').show();
    });

    function ClearBalanceSheetInfo() {
        $('#hdfBalanceSheetConfigId').val('');
        $('#GroupId').val('');
        $('#CategoryName').val('');
        $('#BalanceSheetName').val('');

        $('#updateBalanceSheet').hide();
        $('#BalanceSheet_Clear').show();
        $('#btn_submit').show();
    }



    var oBalanceSheetDataTable;
    LoadBalanceSheetDataTable();
    function LoadBalanceSheetDataTable() {
        oBalanceSheetDataTable = $('#BalanceSheetDataTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/GetBalanceSheetInfo",//'@Url.Action("GetBalanceSheetInfo", "FinancialTransaction", new { Area = "FinancialTransactions" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "GroupName" },
                { "data": "CategoryName" },
                { "data": "BalanceSheetName" }

            ],
            "order": [[0, "desc"]],
            searching: false,
            paging: false,
            "bInfo": false
        });
        $('#BalanceSheetDataTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oBalanceSheetDataTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindBalanceSheetData(oBalanceSheetDataTable.row(this).data());
        });
    }


    function bindBalanceSheetData(data) {
        $('#hdfBalanceSheetConfigId').val(data.BalanceSheetId);
        $('#GroupId').val(data.GroupId);
        $("#GroupId").change();
        $(document).ajaxComplete(function () {
            $('#CategoryName').val(data.COACategoryId);
        });
        $('#BalanceSheetName').val(data.BalanceSheetName);
        $('#updateBalanceSheet').show();
        $('#BalanceSheet_Clear').show();
        $('#btn_submit').hide();

    }



    $('#updateBalanceSheet').click(function () {

        var isValid = CheckValidationForUpdate();
        if (isValid) {
            var data = BindData();
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/FinancialTransaction/UpdateBalanceSheet",//'@Url.Action("UpdateBalanceSheet", "FinancialTransaction", new { Area = "FinancialTransactions" })',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        oBalanceSheetDataTable.ajax.reload();
                        document.getElementById('showErrorMessage').style.color = "green";
                        ClearBalanceSheetInfo();
                    }
                    document.getElementById('showErrorMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showErrorMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });




    function CheckValidationForUpdate() {
        var isValid = true;
        var status = $("#showErrorMessage");

        if ($('#hdfBalanceSheetConfigId').val().trim() === '') {
            isValid = false;
        }
        else {
            $('#hdfBalanceSheetConfigId').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($("#GroupId").val().trim() === '0') {
            isValid = false;
            status.html("Please Select Group!");
        }
        else {
            status.html("");
        }

        if ($("#CategoryName").val().trim() === '0') {
            isValid = false;
            status.html("Please Select Category!");
        }
        else {
            status.html("");
        }
        if ($("#BalanceSheetName").val().trim() === '') {
            isValid = false;
            status.html("Please Select Category!");
        }
        else {
            status.html("");
        }

        return isValid;

    }

    function BindData() {

        var data = {
            BalanceSheetId: $('#hdfBalanceSheetConfigId').val(),
            CategoryName: $("#CategoryName option:selected").val(),
            GroupId: $('#GroupId').val(),
            BalanceSheetName: $('#BalanceSheetName').val()
        }
        return data;
    }



});

