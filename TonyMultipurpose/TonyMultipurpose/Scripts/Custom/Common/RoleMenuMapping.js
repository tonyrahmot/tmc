﻿
$(document).ready(function () {
    //clicking the parent checkbox should check or uncheck all child checkboxes
    $(".parent").click(function () {

        $(this).closest('fieldset').find('.child').prop('checked', this.checked);
    }
    );
    //clicking the last unchecked or checked checkbox should check or uncheck the parent checkbox
    $('.child').click(
        function () {
            if (this.checked == true) {
                var flag = true;
                $(this).closest('fieldset').find('.child').one(
                    function () {
                        if (this.CheckUncheckChildren == true)
                            flag = false;
                    }
                );
                $(this).closest('fieldset').find('.parent').prop('checked', flag);
            }
        }
    );
});
$(document).ready(function () {
    $('#GetTotal').on('click', function () {
        var prices = [];
        var roleId = $('#RoleId').val();
        $('input:checked').each(function () {
            prices.push($(this).attr("value"));
        });
        $.ajax({
            url: window.applicationBaseUrl + "RoleMenuMapping/Save",//'@Url.Action("Save", "RoleMenuMapping")',
            type: "POST",
            data: { menuId: prices, roleId: roleId },
            dataType: "json",
            traditional: true,
            success: function (d) {
                if (d.status == true) {
                    alert("Assign Successfully");

                } else {
                    alert('Error. Please try again.');
                }
            }
        });
    });
});

