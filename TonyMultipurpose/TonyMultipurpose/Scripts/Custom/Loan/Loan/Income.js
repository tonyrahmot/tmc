$('#UpdateMonthlyIncome').hide();
$('#deleteMonthlyIncome').hide();
var incomeTotal;
var x;
// Load Function function
var oMonthlyIncomeTable;
MonthlyIncomeTable();
function MonthlyIncomeTable() {
    oMonthlyIncomeTable = $('#MonthlyIncomeTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyIncomeInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyIncome_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "IncomeType" },
            { "data": "Amount" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            incomeTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                 incomeTotal

            );
            x = incomeTotal;
        }
    });
    $('#MonthlyIncomeTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            oMonthlyIncomeTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        bindDataGuarantor(oMonthlyIncomeTable.row(this).data());
    });
}

function bindDataGuarantor(data) {
    $('#hdfMonthlyIncomeId').val(data.MonthlyIncomeId);
    $('#MonthlyIncome_TypeId').val(data.IncomeTypeId);
    $('#MonthlyIncome_Amount').val(data.Amount);
    $('#MonthlyIncome_LoanId').val(data.LoanId);
    $('#saveMonthlyIncome').hide();
    $('#UpdateMonthlyIncome').show();
    $('#deleteMonthlyIncome').show();
}
function MonthlyIncomeClearAll() {
    $('#MonthlyIncome_TypeId').val('');
    $('#MonthlyIncome_Amount').val('');
    $('#saveMonthlyIncome').show();
    $('#UpdateMonthlyIncome').hide();
    $('#deleteMonthlyIncome').hide();
}
// caler function
$('#MonthlyIncome_Clear').click(function () {
    MonthlyIncomeClearAll();
});
// data load function
function Binddata() {
    var LoanId = $('#MonthlyIncome_LoanId').val();
    var IncomeTypeId = $('#MonthlyIncome_TypeId ').val();
    var Amount = $('#MonthlyIncome_Amount').val();
    var MonthlyIncomeId = $('#hdfMonthlyIncomeId').val();
    var data = {
        LoanId: $('#MonthlyIncome_LoanId').val(),
        IncomeTypeId: $('#MonthlyIncome_TypeId option:selected').val(),
        Amount: $('#MonthlyIncome_Amount').val(),
        MonthlyIncomeId: $('#hdfMonthlyIncomeId').val()
    }
    return data;
}
// income type load function
$(function () {
    $("#MonthlyIncome_TypeId").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Loan/Loan/GetIncomeType",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#MonthlyIncome_TypeId").append('<option value="0">--Select Income Type--</option>');
            $.each(data, function (key, value) {
                $("#MonthlyIncome_TypeId").append('<option value=' + value.IncomeTypeId + '>' + value.IncomeType + '</option>');
            });
        }
    });
});
// Save function
$('#saveMonthlyIncome').click(function () {
    var isAllDataValid = true;
    if (isAllDataValid) {
        var data = Binddata();
        alert(data.IncomeTypeId);
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/SaveIncome",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.oCommonResult.Status == true) {
                    oMonthlyIncomeTable.ajax.reload();
                    $('#saveMonthlyIncome').show();
                    $('#UpdateMonthlyIncome').hide();
                    $('#deleteMonthlyIncome').hide();
                    document.getElementById('MonthlyIncome_showMessage').style.color = "green";
                }
                document.getElementById('MonthlyIncome_showMessage').innerHTML = d.oCommonResult.Message;
            },
            error: function () {
                document.getElementById('MonthlyIncome_showMessage').innerHTML = 'Error. Please try again.';
            }
        });
    }
});
$('#UpdateMonthlyIncome').click(function () {

    var isAllDataValid = true;

    if (isAllDataValid) {

        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateMonthlyIncome",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    oMonthlyIncomeTable.ajax.reload();
                    MonthlyIncomeClearAll();
                    oMonthlyIncomeTable.ajax.reload();
                    $('#saveMonthlyIncome').show();
                    $('#UpdateMonthlyIncome').hide();
                    $('#deleteMonthlyIncome').hide();
                }
            },
            error: function () {

            }
        });
    }
});
$('#deleteMonthlyIncome').click(function () {
    if (confirm("Are you sure?")) {
        var data = Binddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/DeleteMonthlyIncome",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    oMonthlyIncomeTable.ajax.reload();
                    MonthlyIncomeClearAll();
                    $('#saveMonthlyIncome').show();
                    $('#UpdateMonthlyIncome').hide();
                    $('#deleteMonthlyIncome').hide();
                }
            },
            error: function () {

            }
        });
    }
    return false;
});