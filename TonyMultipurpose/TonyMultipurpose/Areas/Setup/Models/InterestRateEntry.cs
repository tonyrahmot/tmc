﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Setup.Models
{
    //--ataur
    public class InterestRateEntry:CommonModel
    {
        [Key]
        public int Id { get; set; }


        public int? AccountSetupId { get; set; }
        public int? AccountType { get; set; }
        public List<AccountTypeSetup> AccountTypeSetups { get; set; }

        [Required]
        [Display(Name = "Account Title")]
        public string AccountTitleId { get; set; }
        [Display(Name = "Account Type Name")]
      
        [Required]
        public string AccountTypeName { get; set; }


        [Display(Name = "Interest Rate")]
        public decimal InterestRate { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime? Date { get; set; }

        public string ShowDate { get; set; }


        [Display(Name = "Duration")]
        public string Duration { get; set; }


      
    }
}