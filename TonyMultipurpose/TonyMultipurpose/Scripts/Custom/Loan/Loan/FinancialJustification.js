document.getElementById('showMessage').innerHTML = '';
var emiTotal;
var oAdditionalTable;
(function () {

    //  var emiTotal;
    var z;
    //   var oAdditionalTable;
    ShowData();
    //functions
    function ShowData() {
        oAdditionalTable = $('#AdditionalTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "Loan/Loan/GetAdditionalLoanInfoById",
                "type": "get",
                "data": {
                    "LoanId": $('#LoanId').val()
                },
                "datatype": "json"
            },
            "columns": [
                {
                    "data": "AdditionalLoanDetailId",
                    "width": "25px",
                    "className": "dt-center",
                    "render": function (data) {
                        return '<input class="form-control" id="AdditionalLoanDetailId" name="AdditionalLoanDetailId" type="text" readonly="readonly"  value="' + data + '">';
                    }
                },
                {
                    "data": "InstituteName",
                    "width": "130px",
                    "render": function (data) {
                        return '<input class="form-control" id="InstituteName" name="InstituteName" type="text"  value="' + data + '">';
                    }
                },
                {
                    "data": "LoanAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanAmount" name="LoanAmount" type="text"  value="' + data + '">';
                    }
                },
                {
                    "data": "LoanType",
                    "render": function (data) {
                        return '<input class="form-control" id="LoanType" name="LoanType" type="text"  value="' + data + '">';
                    }
                },
                {
                    "data": "Installment",
                    "render": function (data) {
                        return '<input class="form-control" id="Installment" name="Installment" type="text"  value="' + data + '">';
                    }
                },

                {
                    "data": "AccountNo",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountNo" name="AccountNo" type="text"  value="' + data + '">';
                    }
                },
                {
                    "data": "AccountName",
                    "render": function (data) {
                        return '<input class="form-control" id="AccountName" name="AccountName" type="text"  value="' + data + '">';
                    }
                },
                {
                    "data": "StartDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        bindDatePickerforstdate();
                        return '<input class="form-control" id="StartDate" name="StartDate" type="text"  value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "ExpireDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                        bindDatePickerforexdate();
                        return '<input class="form-control" id="ExpireDate" name="ExpireDate" type="text"  value = "' + datadate + '"  >';
                    }
                },
                {
                    "data": "PresentOutAmount",
                    "render": function (data) {
                        return '<input class="form-control" id="PresentOutAmount" name="PresentOutAmount" type="text"  value="' + data + '">';
                    }
                }
            ],
            ordering: false,
            searching: false,
            paging: false,
            "bInfo": false,
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                emiTotal = api
                    .column(4, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);



                var PresentOut = api
                    .column(9, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                    emiTotal
                );

                $(api.column(9).footer()).html(
                    PresentOut
                );


                //z = emiTotal;
            }
        });
    };






    function bindDatePickerforstdate() {
        $("#AdditionalTable tbody tr").each(function (item) {
            $(this).find("#StartDate").datepicker({
                dateFormat: 'dd/M/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+60", type: Text
            }).click(function () { $(this).focus(); });
        });
    }

    function bindDatePickerforexdate() {
        $("#AdditionalTable tbody tr").each(function (item) {
            $(this).find("#ExpireDate").datepicker({
                dateFormat: 'dd/M/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+60", type: Text
            }).click(function () { $(this).focus(); });
        });
    }

    Finance();

})();

/**********************************************************************/

function Finance() {
    var data = {
        LoanId: $('#MonthlyIncome_LoanId').val()
    }
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/Finance",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (obj) {

            $('#hdfLoanId').val(obj.info.LoanId);
            $('#hdfAccounts').val(obj.info.AccountSetupId);
            $('#hdfCustomerId').val(obj.info.CustomerId);
            var netEmi = emiTotal;

            var netIncome = obj.netIncome.TotalIncome !== null ? obj.netIncome.TotalIncome : 0;
            var netExpense = obj.netExpense.TotalExpense !== null ? obj.netExpense.TotalExpense : 0;

            var surplusIncome = parseFloat(netIncome - netExpense);
            document.getElementById('labelSurplusIncome').innerHTML = surplusIncome;

            if (obj.info.AccountSetupId === 1) {
                if (obj.info.InstallmentTypeId == 1) {
                    //var parcent = obj.info.InterestRate+'%';
                    document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelweeklyNoInstallment').innerHTML = obj.info.NumberOfInstallment;
                    document.getElementById('labelweeklyInstallmentAmount').innerHTML = obj.info.InstallmentAmount;
                    document.getElementById('labelweeklyInstallmentType').innerHTML = obj.info.InstallmentTypeName;
                    var db = parseFloat((obj.info.InstallmentAmount + netEmi) / surplusIncome);
                    var dbR = Math.round(db * 100);
                    document.getElementById('labelweeklyDbr').innerHTML = Math.round(dbR) + '%';

                    var eq = parseFloat((obj.info.TotalAssetCost - obj.info.LoanAmount) / obj.info.TotalAssetCost);
                    var eqR = parseFloat(eq * 100).toFixed(2);
                    document.getElementById('labelweeklyEquity').innerHTML = Math.round(eqR) + '%';

                    /***********************************************************************************************************/

                    document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelmonthlyNoInstallment').innerHTML = 12;
                    document.getElementById('labelmonthlyInstallmentType').innerHTML = 'Monthly';

                    var r = parseFloat(obj.info.InterestRate) / 1200;
                    var a = parseFloat(obj.info.LoanAmount * r);
                    var b = parseFloat(Math.pow(1 + r, -12));
                    var c = 1 - b;
                    var installmentAmount = Math.round(a / c);
                    document.getElementById('labelmonthlyInstallmentAmount').innerHTML = installmentAmount.toFixed(2);

                    var mdb = parseFloat((installmentAmount + netEmi) / surplusIncome);
                    var mdbR = Math.round(mdb * 100);
                    document.getElementById('labelmonthlyDbr').innerHTML = mdbR + '%';
                    document.getElementById('labelmonthlyEquity').innerHTML = Math.round(eqR) + '%';

                }

                if (obj.info.InstallmentTypeId == 2) {
                    document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelmonthlyNoInstallment').innerHTML = obj.info.NumberOfInstallment;
                    document.getElementById('labelmonthlyInstallmentAmount').innerHTML = obj.info.InstallmentAmount;
                    document.getElementById('labelmonthlyInstallmentType').innerHTML = obj.info.InstallmentTypeName;
                    var db = parseFloat((obj.info.InstallmentAmount + netEmi) / surplusIncome);
                    var dbR = Math.round(db * 100);
                    document.getElementById('labelmonthlyDbr').innerHTML = dbR + '%';

                    var eq = parseFloat((obj.info.TotalAssetCost - obj.info.LoanAmount) / obj.info.TotalAssetCost);
                    var eqR = parseFloat(eq * 100).toFixed(2);
                    document.getElementById('labelmonthlyEquity').innerHTML = Math.round(eqR) + '%';

                    /***********************************************************************************************************/
                    document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                    document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                    document.getElementById('labelweeklyInstallmentType').innerHTML = 'Weekly';
                    document.getElementById('labelweeklyNoInstallment').innerHTML = 46;

                    var r = parseFloat(obj.info.InterestRate) / 5200;
                    var a = parseFloat(obj.info.LoanAmount * r);
                    var b = parseFloat(Math.pow(1 + r, -46));
                    var c = 1 - b;
                    var installmentAmount = Math.round(a / c);
                    document.getElementById('labelweeklyInstallmentAmount').innerHTML = installmentAmount.toFixed(2);

                    var wdb = parseFloat((installmentAmount + netEmi) / surplusIncome);
                    //var wdbR = parseFloat(wdb * 100).toFixed(2);
                    var wdbR = Math.round(wdb * 100);
                    document.getElementById('labelweeklyDbr').innerHTML = wdbR + '%';
                    document.getElementById('labelweeklyEquity').innerHTML = Math.round(eqR) + '%';
                }
            } else if (obj.info.AccountSetupId === 3) {
                document.getElementById('labelweeklyLoanAmount').innerHTML = obj.info.LoanAmount;
                document.getElementById('labelweeklyInterestRate').innerHTML = obj.info.InterestRate + '%';
                document.getElementById('labelmonthlyLoanAmount').innerHTML = obj.info.LoanAmount;
                document.getElementById('labelmonthlyInterestRate').innerHTML = obj.info.InterestRate + '%';
            }
        },
        error: function () {
        }
    });
}





$('#addadditonLoan').click(function () {
    var loanId = $('#LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/AddaddintionLoan",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function () {
            //ShowData();
            oAdditionalTable.ajax.reload();
        },
        error: function () {
        }
    });
});

$('#DeductadditonLoan').click(function () {
    var loanId = $('#LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/DeductAdditonLoan",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function () {
            //ShowData();
            oAdditionalTable.ajax.reload();
        },
        error: function () {
        }
    });
});


//$('#UpdateMonthlyExpense').hide();
//$('#deleteMonthlyExpense').hide();

/*****************************************************/


var oBankperformanceTable;
BankperformanceTable();

function BankperformanceTable() {
    oBankperformanceTable = $('#BankperformanceTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllBankperformanceInfo",
            "type": "get",
            "data": {
                "LoanId": $('#Bankperformance_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
        {
            "data": "BankperformanceId",
            //"visible": false,
            "width": "25px",
            "className": "dt-center",
            "render": function (data) {
                return '<input class="form-control" id="BankperformanceId" name="BankperformanceId" type="text" readonly="readonly"  value="' + data + '">';
            }
        },
        {
            "data": "NgoName",
            "render": function (data) {
                return '<input class="form-control" id="NgoName" name="NgoName" type="text"  value="' + data + '">';
            }
        },
            {
                "data": "AccNo",
                "render": function (data) {
                    return '<input class="form-control" id="AccNo" name="AccNo" type="text"   value="' + data + '">';
                }
            },
            {
                "data": "FromDate",
                //"visible": false,
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    bindDatePickers();
                    return '<input class="form-control" id="FromDate" name="FromDate" type="text"   value =" ' + datadate + '"  >';
                }
            },
            {
                "data": "ToDate",
                "render": function (data) {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    var mon = monthNumToName(month);
                    var datadate = date.getDate() + "/" + mon + "/" + date.getFullYear();
                    bindDatePicker();
                    return '<input class="form-control" id="ToDate" name="ToDate" type="text"   value = "' + datadate + '"  >';
                }
            },
            {
                "data": "DebitAmount",
                "render": function (data) {
                    return '<input class="form-control" id="DebitAmount" name="DebitAmount" type="text"   value = "' + data + '"  >';
                }
            },
            {
                "data": "CreditAmount",
                "render": function (data) {
                    return '<input class="form-control" id="CreditAmount" name="CreditAmount" type="text"  value="' + data + '">';
                }
            },
            {
                "data": "Avg",
                "render": function (data) {
                    return '<input class="form-control" id="Avg" name="Avg" type="text" readonly="readonly"  value="' + Math.round(data) + '">';
                }
            }
        ],
        //"order": [[0, "desc"]],
        ordering: false,
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            var drTotal = api
                .column(5, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var crTotal = api
                .column(6, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            var avgTotal = api
                .column(7, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            // Update footer
            $(api.column(5).footer()).html(
                drTotal
            );
            $(api.column(6).footer()).html(
                crTotal
            );
            $(api.column(7).footer()).html(
                Math.round(avgTotal)
            );
        }
    });


    /*******************************************************/


    //var expenseTotal;
    //var y;
    // Load Function function

    //$('#ToDate').datepicker({
    //    dateFormat: 'dd/M/yy',
    //    changeMonth: true,
    //    changeYear: true,
    //    yearRange: "-100:+60", type: Text
    //}).click(function () { $(this).focus(); });

    $('#DateFrom').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });
}

//$('#ToDate').datepicker({
//    dateFormat: 'dd/M/yy',
//    changeMonth: true,
//    changeYear: true,
//    yearRange: "-100:+60", type: Text
//}).click(function () { $(this).focus(); });


function bindDatePicker() {
    $("#BankperformanceTable tbody tr").each(function (item) {
        $(this).find("#ToDate").datepicker({
            dateFormat: 'dd/M/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+60", type: Text
        }).click(function () { $(this).focus(); });
    });
}

function bindDatePickers() {
    $("#BankperformanceTable tbody tr").each(function (item) {
        $(this).find("#FromDate").datepicker({
            dateFormat: 'dd/M/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+60", type: Text
        }).click(function () { $(this).focus(); });
    });
}


// ========== BankperformanceBinddata ========
function BankperformanceBinddata() {

    var data = [];
    $("#BankperformanceTable tbody tr").each(function (item) {
        var bank = {};
        bank["BankperformanceId"] = $(this).find("#BankperformanceId").val();
        bank["NgoName"] = $(this).find("#NgoName").val();
        bank["AccNo"] = $(this).find("#AccNo").val();
        bank["FromDate"] = $(this).find("#FromDate").val();
        bank["ToDate"] = $(this).find("#ToDate").val();
        bank["DebitAmount"] = $(this).find("#DebitAmount").val();
        bank["CreditAmount"] = $(this).find("#CreditAmount").val();
        bank["Avg"] = $(this).find("#Avg").val();
        data.push(bank);
    });
    return data;
}


function DifferenceMonth() {
    var fromDate = $('#Bankperformance_FromDate').val();
    var toDate = $('#Bankperformance_ToDate').val();
    var usrDate = new Date(fromDate);
    var curDate = new Date(toDate);
    var usrYear, usrMonth = usrDate.getMonth() + 1;
    var curYear, curMonth = curDate.getMonth() + 1;
    if ((usrYear = usrDate.getFullYear()) < (curYear = curDate.getFullYear())) {
        curMonth += (curYear - usrYear) * 12;
    }
    var diffMonths = curMonth - usrMonth;
    if (usrDate.getDate() > curDate.getDate()) diffMonths--;

    return diffMonths;
}


function UpdateBankperformance() {

    var isAllDataValid = true;

    if (isAllDataValid) {

        var data = BankperformanceBinddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateBankperformance",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    //BankperformanceClearAll();
                    oBankperformanceTable.ajax.reload();
                    //$('#saveBankperformance').show();
                    //$('#UpdateBankperformance').hide();
                    //$('#deleteBankperformance').hide();

                    Finance();
                }
            },
            error: function () {

            }
        });
    }
}

$('#AddBankperformance').click(function () {
    var loanId = $('#LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/AddbankPerformance",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function () {
            oBankperformanceTable.ajax.reload();
        },
        error: function () {
        }
    });
});

$('#DeductBankperformance').click(function () {
    var loanId = $('#LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/Deductbankperformance",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function () {
            oBankperformanceTable.ajax.reload();
        },
        error: function () {
        }
    });
});


var incomeTotal;
var x;

//====== Load Function function=========
var oMonthlyIncomeTable;
MonthlyIncomeTable();

function MonthlyIncomeTable() {
    oMonthlyIncomeTable = $('#MonthlyIncomeTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyIncomeInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyIncome_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "IncomeType" },
            { "data": "Amount" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            incomeTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                incomeTotal
            );
            x = incomeTotal;
        }
    });
    MonthlyInformatiom();
}

/// ======== Monthly Informtion========
function MonthlyInformatiom() {
    var loanId = $('#MonthlyIncome_LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyIncomeInfo",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            if (data.data[0].Amount === 0) {
                $('#Amount').val('');
            } else {
                $('#Amount').val(data.data[0].Amount);
            }
            if (data.data[0].SpouseAmount === 0) {
                $('#SpouseAmount').val('');
            } else {
                $('#SpouseAmount').val(data.data[0].SpouseAmount);
            }
            if (data.data[0].OtherIncomeAmount === 0) {
                $('#OtherIncomeAmount').val('');
            } else {
                $('#OtherIncomeAmount').val(data.data[0].OtherIncomeAmount);
            }
            if (data.data[0].ForeignRemitAmount === 0) {
                $('#ForeignRemitAmount').val('');
            } else {
                $('#ForeignRemitAmount').val(data.data[0].ForeignRemitAmount);
            }
            $('#IncomeType').val(data.data[0].IncomeType);
            $('#SpouseIncomeType').val(data.data[0].SpouseIncomeType);
            $('#TotalAmount').val(data.data[0].TotalIncome);
        }
    });
}

// ========= data load function ======
function BindMonthlydata() {
    var data = {
        LoanId: $('#MonthlyIncome_LoanId').val(),
        IncomeType: $('#IncomeType option:selected').val(),
        SpouseIncomeType: $('#SpouseIncomeType option:selected').val(),
        Amount: $('#Amount').val(),
        SpouseAmount: $('#SpouseAmount').val(),
        OtherIncomeAmount: $('#OtherIncomeAmount').val(),
        ForeignRemitAmount: $('#ForeignRemitAmount').val()
    }
    return data;

}





function MonthlyUpdatedata() {
    var isAllDataValid = CheckValidation();
    var data = null;
    if (isAllDataValid) {
        var loanIdcheck = $('#MonthlyIncome_LoanId').val();
        if (loanIdcheck === 'NotASavedLoan') {
            document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Please select loanId from basic info';
            return false;
        } else {
            data = BindMonthlydata();
        }
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateMonthlyIncome",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.status == true) {
                    //location.reload();
                    // oMonthlyIncomeTable.ajax.reload();
                    //// MonthlyIncomeClearAll();
                    // $('#saveMonthlyExpense').show();
                    // $('#UpdateMonthlyExpense').hide();
                    // $('#deleteMonthlyExpense').hide();
                    document.getElementById('MonthlyExpense_showMessage').style.color = "green";

                    Finance();
                }
                document.getElementById('MonthlyExpense_showMessage').innerHTML = d.oCommonResult.Message;

                //
            },
            error: function () {
                document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Error. Please try again.';
            },



        });
    }
}

function CheckValidation() {
    //var isValid = true;
    //if ($('#Amount').val().trim()  === '') {
    //    isValid = false;
    //    $('#Amount').parent().prev().find('span').css('visibility', 'visible');
    //}
    //else {
    //    $('#Amount').parent().prev().find('span').css('visibility', 'hidden');
    //}
    //if ($('#SpouseAmount').val().trim()  === '') {
    //    isValid = false;
    //    $('#SpouseAmount').parent().prev().find('span').css('visibility', 'visible');
    //}
    //else {
    //    $('#SpouseAmount').parent().prev().find('span').css('visibility', 'hidden');
    //}
    return true;
}

var months = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May',
    'Jun', 'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec'
];

function monthNumToName(monthnum) {
    return months[monthnum - 1] || '';
}




var expenseTotal;
var y;
var oMonthlyExpenseTable;
MonthlyExpenseTable();

function MonthlyExpenseTable() {
    oMonthlyExpenseTable = $('#MonthlyExpenseTable').DataTable({
        "ajax": {
            "url": window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyExpenseInfo",
            "type": "get",
            "data": {
                "LoanId": $('#MonthlyExpense_LoanId').val()
            },
            "datatype": "json"
        },
        "columns": [
            { "data": "LoanId" },
            { "data": "FamilyExpense" },
            { "data": "ChildrenEducation" },
        ],
        "order": [[0, "desc"]],
        searching: false,
        paging: false,
        "bInfo": false,
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            expenseTotal = api
                .column(2, { page: 'current' })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                expenseTotal
            );
            y = expenseTotal;
        }
    });
    MonthlyExpenseInformatiom();
}

/// ========= MonthlyExpenseInformatiom ============

function MonthlyExpenseInformatiom() {
    var loanId = $('#MonthlyExpense_LoanId').val();
    var data = { loanId: loanId };
    $.ajax({
        url: window.applicationBaseUrl + "Loan/Loan/GetAllMonthlyExpenseInfo",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "JSON",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            if (data.data[0].FamilyExpense === 0) {
                $('#FamilyExpense').val('');
            } else {
                $('#FamilyExpense').val(data.data[0].FamilyExpense);
            }
            if (data.data[0].ChildrenEducation === 0) {
                $('#ChildrenEducation').val('');
            } else {
                $('#ChildrenEducation').val(data.data[0].ChildrenEducation);
            }
            if (data.data[0].ExistingEMI === 0) {
                $('#ExistingEMI').val('');
            } else {
                $('#ExistingEMI').val(data.data[0].ExistingEMI);
            }
            if (data.data[0].OthersExpense === 0) {
                $('#OthersExpense').val('');
            } else {
                $('#OthersExpense').val(data.data[0].OthersExpense);
            }
            $('#TotalExpenseAmount').val(data.data[0].TotalExpense);
        }
    });
}



function MonthlyExpenseClearAll() {
    $('#FamilyExpense').val('');
    $('#ChildrenEducation').val('');
    $('#ExistingEMI').val('');
    $('#OthersExpense').val('');
    $('#saveMonthlyExpense').show();
    $('#UpdateMonthlyExpense').hide();
    $('#deleteMonthlyExpense').hide();
}

// caler function
$('#MonthlyExpense_Clear').click(function () {
    MonthlyExpenseClearAll();
});


///=========== BindDataMonthly Expense =========
function Binddata() {
    var data = {
        LoanId: $('#MonthlyExpense_LoanId').val(),
        FamilyExpense: $('#FamilyExpense').val(),
        ChildrenEducation: $('#ChildrenEducation').val(),
        ExistingEMI: $('#ExistingEMI').val(),
        OthersExpense: $('#OthersExpense').val(),
        //Amount: $('#MonthlyExpense_Amount').val(),
        MonthlyExpenseId: $('#hdfMonthlyExpenseId').val()
    }
    return data;

}

function UpdateAdditionalLoan() {

    var isAllDataValid = true;

    if (isAllDataValid) {

        var data = AdditionalLoanBinddata();
        $.ajax({
            url: window.applicationBaseUrl + "Loan/Loan/UpdateAdditionalLoanInfo",
            type: "POST",
            data: JSON.stringify(data),
            dataType: "JSON",
            contentType: "application/json",
            success: function (d) {
                if (d.oCommonResult.Status == true) {
                    oAdditionalTable.ajax.reload();
                    //$('#saveBankperformance').show();
                    //$('#UpdateBankperformance').hide();
                    //$('#deleteBankperformance').hide();
                    //window.ShowData();
                    Finance();
                }
            },
            error: function () {

            }
        });
    }
}

$('#UpdateMonthlyExpense').click(function () {
    var loanIdcheck = $('#MonthlyExpense_LoanId').val();
    if (loanIdcheck == 'NotASavedLoan') {
        document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Please select loanId from basic info';
    } else {
        MonthlyUpdatedata();
        UpdateBankperformance();
        UpdateAdditionalLoan();
        var isAllDataValid = true;
        var data = null;
        if (isAllDataValid) {
            var loanIdcheck = $('#MonthlyExpense_LoanId').val();
            if (loanIdcheck == 'NotASavedLoan') {
                document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Please select loanId from basic info';
                return false;
            } else {
                data = Binddata();
            }
            $.ajax({
                url: window.applicationBaseUrl + "Loan/Loan/UpdateMonthlyExpense",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status == true) {
                        document.getElementById('MonthlyExpense_showMessage').style.color = "green";
                        NetIncomeCalculation();
                        NetExpenseCalculation();
                        //SurplusCal();
                        oAdditionalTable.ajax.reload();
                        oBankperformanceTable.ajax.reload();
                        Finance();


                    }
                    document.getElementById("MonthlyExpense_showMessage").innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('MonthlyExpense_showMessage').innerHTML = 'Error. Please try again.';
                },


            });
        }
    }

});
function NetIncomeCalculation() {

    var Amount = parseFloat($('#Amount').val() === '' ? 0 : $('#Amount').val());
    var SpouseAmount = parseFloat($('#SpouseAmount').val() === '' ? 0 : $('#SpouseAmount').val());
    var OtherIncomeAmount = parseFloat($('#OtherIncomeAmount').val() === '' ? 0 : $('#OtherIncomeAmount').val());
    var ForeignRemitAmount = parseFloat($('#ForeignRemitAmount').val() === '' ? 0 : $('#ForeignRemitAmount').val());
    var totalAmount = parseFloat(SpouseAmount + Amount + OtherIncomeAmount + ForeignRemitAmount);
    $('#TotalAmount').val(totalAmount);


}

function NetExpenseCalculation() {
    var FamilyExpense = parseFloat($('#FamilyExpense').val() === '' ? 0 : $('#FamilyExpense').val());
    var ChildrenEducation = parseFloat($('#ChildrenEducation').val() === '' ? 0 : $('#ChildrenEducation').val());
    var ExistingEMI = parseFloat($('#ExistingEMI').val() === '' ? 0 : $('#ExistingEMI').val());
    var OthersExpense = parseFloat($('#OthersExpense').val() === '' ? 0 : $('#OthersExpense').val());
    var totalAmount = parseFloat(FamilyExpense + ChildrenEducation + ExistingEMI + OthersExpense);
    $('#TotalExpenseAmount').val(totalAmount);

}
function AdditionalLoanBinddata() {

    var data = [];
    $("#AdditionalTable tbody tr").each(function (item) {
        var additionalloan = {};
        additionalloan["AdditionalLoanDetailId"] = $(this).find("#AdditionalLoanDetailId").val();
        additionalloan["InstituteName"] = $(this).find("#InstituteName").val();
        additionalloan["LoanAmount"] = $(this).find("#LoanAmount").val();
        additionalloan["LoanType"] = $(this).find("#LoanType").val();
        additionalloan["Installment"] = $(this).find("#Installment").val();
        additionalloan["AccountNo"] = $(this).find("#AccountNo").val();
        additionalloan["AccountName"] = $(this).find("#AccountName").val();
        additionalloan["StartDate"] = $(this).find("#StartDate").val();
        additionalloan["ExpireDate"] = $(this).find("#ExpireDate").val();
        additionalloan["PresentOutAmount"] = $(this).find("#PresentOutAmount").val();
        data.push(additionalloan);
    });
    return data;
}