$('#TransactionDate').datepicker({ dateFormat: 'dd/M/yy', type: Text }).click(function () { $(this).focus(); });
$('#loan_Bank').hide();
$('#loan_Account').hide();
$('#loan_Cheque').hide();
var totalPayment = 0;
var interest = 0;


$(function () {
    $("#ChartOfAccount").empty();
    $.ajax({
        type: "POST",
        url: window.applicationBaseUrl + "Transaction/Transaction/GetTransactionalCoaData",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#ChartOfAccount").append('<option value="0">--Select COA Type--</option>');
            $.each(data, function (key, value) {
                $("#ChartOfAccount").append('<option value=' + value.COAId + '>' + value.AccountCode + '</option>');
            });
        }
    });
});


$(document).ready(function () {
    $('#ChartOfAccount').change(function () {
        var coaId = $("#ChartOfAccount").val();
        var json = {
            coaId: coaId
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Transaction/Transaction/GetAccountCodeInfo",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                if (data.IsBankAccount === true) {
                    $('#IsChequeEntry').attr('checked', true);
                    $('#loan_Bank').show();
                    $('#loan_Account').show();
                    $('#loan_Cheque').show();
                    $('#loanBankName').val(data.BankName);
                    $('#loanBankAccount').val(data.BankAccountNo);
                } else {
                    $('#IsChequeEntry').attr('checked', false);
                    $('#loan_Bank').hide();
                    $('#loan_Account').hide();
                    $('#loan_Cheque').hide();
                    $('#loanBankName').val('');
                    $('#loanBankAccount').val('');
                }
            }
        });

    });


    $("#LoanNo").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanInstallment/AutoCompleteTermLoanId",
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    if (data.length == 0) {
                        alert("No Account Available for Transaction");
                        clearTermLoanInfo();
                        $('#submitInstallment').attr('disabled', 'disabled');
                        $('#TotalPayment').attr('disabled', 'disabled');

                    } else {
                        response($.map(data, function (item) {
                            return { label: item, value: item };
                        }));
                    }
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });


    $("#LoanNo").change(function () {
        //clearTermLoanInfo();
        removeErrorMessage(this);
        var LoanNo = $("#LoanNo").val();
        var json = {
            LoanNo: LoanNo
        };
        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "Loan/LoanInstallment/GetTermLoanInfoByLoanId",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (obj) {
                if (obj.info.IsCleared == true) {
                    alert("All Installment Has Been Cleared!!!!");
                    //clearTermLoanInfo();
                    $('#submitPayment').attr('disabled', 'disabled');

                } else {
                    $('#submitPayment').removeAttr('disabled');
                    $('#LabelCustomerName').html(obj.info.CustomerName);
                    $('#customerImage').attr('src', obj.info.CustomerImage);
                    $('#LabelTotalFineInterest').html(obj.info.TotalFineInterest);
                    $('#LabelTotalUnpaidPrinciple').html(obj.info.TotalUnpaidPrinciple);

                    $('#TotalPayment').val(parseFloat(obj.info.TotalUnpaidPrinciple) + parseFloat(obj.info.TotalFineInterest));
                    totalPayment = $('#TotalPayment').val();
                    if (obj.info.BeginningBalance == 0) {
                        $('#LabelBeginningBalance').val(obj.info.PrincipleAmount);
                    } else {
                        $('#LabelBeginningBalance').val(obj.info.EndingBalance);
                    }
                    var interestRate = parseFloat(obj.info.InterestRate);

                    var bBalance = parseFloat($('#LabelBeginningBalance').val());
                    if (obj.info.InstallmentTypeId == 1) {
                        interest = Math.round(parseFloat((bBalance * interestRate) / 5200));
                    }
                    if (obj.info.InstallmentTypeId == 2) {
                        interest = Math.round(parseFloat((bBalance * interestRate) / 1200));
                    }

                    $('#LabelOutstanding').html(parseFloat(bBalance) + parseFloat(interest) + parseFloat(obj.info.TotalFineInterest));
                    $("#TotalPayment").keyup();

                }


            }
        });
    });

    $('#TotalPayment').change(function () {
        if (parseFloat($('#TotalPayment').val()) <= 0) {
            $('#submitPayment').attr('disabled', 'disabled');
            alert('Amount must be greater than zero Tk.');
        }
        else if (totalPayment < parseFloat($('#TotalPayment').val())) {
            $('#submitPayment').attr('disabled', 'disabled');
            alert('Amount can not be greater than  ' + totalPayment);
        }
        else {
            $('#submitPayment').removeAttr('disabled');
        }

    });


    $('#submitPayment').click(function () {
        var isAllValid = checkTermLoanValidation();
        
        if (isAllValid) {
           
            var data = {
                LoanNo: $('#LoanNo').val(),
                TotalAmount: $('#TotalPayment').val(),
                Particular: $('#Particular').val(),
                IsChequeEntry: $("#IsChequeEntry").prop('checked'),
                InstallmentDate: $('#TransactionDate').val(),
                BankName: $('#loanBankName').val(),
                ChequeNo: $('#loanChequeNo').val(),
                BankAccountNo: $('#loanBankAccount').val(),
                COAId: $('#ChartOfAccount option:selected').val(),
                VoucherNo: $('#VoucherNo').val()
            }
            $.ajax({
                url: window.applicationBaseUrl + "Transaction/Transaction/SaveTermLoanDuePayment",
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.status === true) {
                        alert('Successfully done.');

                        //clearTermLoanInfo();
                        //clearFields();
                    }
                    else {
                        alert('Failed');
                    }
                    window.location.reload();
                },
                error: function () {
                    alert('Error. Please try again.');
                }
            });
        } else {
            return false;
        }
    });

    function clearTermLoanInfo() {

        //$("#LoanNo").val('');
        document.getElementById('LabelTotalUnpaidPrinciple').innerHTML = '';
        document.getElementById('LabelTotalFineInterest').innerHTML = '';
        document.getElementById('LabelOutstanding').innerHTML = '';
        document.getElementById('LabelCustomerName').innerHTML = '';
        $("#LabelBeginningBalance").val('');
        $("#LoanNo").val('');
    }


    function checkTermLoanValidation() {
        var isAllValid = true;

        if ($('#LoanNo').val().trim() == '') {
            $('#LoanNo').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#LoanNo').siblings('span.error').css('display', 'none');
        }

        if ($('#Particular').val().trim() == 0) {
            $('#Particular').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#Particular').siblings('span.error').css('display', 'none');
        }
        if ($('#ChartOfAccount').val().trim() == 0) {
            $('#ChartOfAccount').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#ChartOfAccount').siblings('span.error').css('display', 'none');
        }
        if ($('#TotalPayment').val().trim() == '') {
            $('#TotalPayment').siblings('span.error').css('display', 'block');
            isAllValid = false;
        } else {
            $('#TotalPayment').siblings('span.error').css('display', 'none');
        }

        return isAllValid;
    }

});

function IsDouble(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false;
    }
    else if (charCode == 13) {
        return false;
    }
    status = "";
    return true;
}
$("#TotalPayment").keyup(function () {
    removeErrorMessage(this);
    var amount = parseInt($(this).val().replace(/,/g, ''));
    var status = $("#InwordsTermLoanDueAmount");
    var user = $.trim(amount);
    if (user.length >= 1) {
        status.val("Checking....");
        $.post(window.applicationBaseUrl + "Loan/LoanInstallment/GetConvertNumberToWord", { number: amount },
                    function (data) {
                        var money = data + ' Tk Only';
                        status.val(money);
                    });
    } else {
        status.val("Need more characters...");
    }

});

function removeErrorMessage(control) {
    var $this = $(control);
    if ($this.val().trim() === 0) {
        $this.siblings('span.error').css('display', 'block');
    } else {
        $this.siblings('span.error').css('display', 'none');
    }
}
function clearCustomerInfo() {
    $('#CustomerName').text('');
    $('#BranchName').text('');
    $('#AccountTypeName').text('');
    $('#Balance').text('');
}