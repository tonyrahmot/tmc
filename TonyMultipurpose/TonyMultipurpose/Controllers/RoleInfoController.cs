﻿using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Setup.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.BLL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Controllers
{
    [AuthenticationFilter]
    //--ataur

    public class RoleInfoController : Controller
    {
        // GET: Setup/Role
        public ActionResult Index()
        {
            RoleBLL objRoleBll = new RoleBLL();
            List<RoleInfo> objRoleInfo = objRoleBll.GetAllRoleInfo();
            return View(objRoleInfo);
        }

        [HttpGet]
        public ActionResult Save()
        {
            CompanyBLL objCompanyBll = new CompanyBLL();
            var model = new RoleInfo
            {
                Companies = objCompanyBll.GetAllCompanyInfo()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(RoleInfo objRole)
        {
            RoleBLL objRoleBll=new RoleBLL();
            objRoleBll.CreateRoleInfo(objRole);

            return RedirectToAction("Index", "RoleInfo");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            CompanyBLL objCompanyBll = new CompanyBLL();
            RoleBLL objRoleBll=new RoleBLL();
            var roleInfo = objRoleBll.GetRoleInfoById(id);

            ViewBag.CompanyNames = objCompanyBll.GetAllCompanyInfo();

            return View(roleInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoleInfo objRole)
        {
            RoleBLL objRoleBll = new RoleBLL();
            objRoleBll.UpdateRoleInfo(objRole);
            return RedirectToAction("Index", "RoleInfo");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            CompanyBLL objCompanyBll = new CompanyBLL();
            RoleBLL objRoleBll = new RoleBLL();
            var roleInfo = objRoleBll.GetRoleInfoById(id);

            ViewBag.CompanyNames = objCompanyBll.GetAllCompanyInfo();

            return View(roleInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteRoleInfo(int id)
        {
            RoleBLL objRoleBll = new RoleBLL();
            objRoleBll.DeleteRoleInfo(id);
            return RedirectToAction("Index", "RoleInfo");
        }
    }
}