﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TonyMultipurpose.Areas.HR.Models
{
    public class EmployeeExperience
    {
        public long Id { get; set; }

        public string EmpCode { get; set; }

        public string InstituteName { get; set; }

        public string YearOfExperience { get; set; }

        public string Position { get; set; }

        public string From { get; set; }

        public string To { get; set; }
    }
}