﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class InstallmentInformationViewModel : CommonModel
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Loan Id")]
        public string LoanId { get; set; }
        public string LoanNo { get; set; }
        [Display(Name = "Installment Date")]
        [Required]
        public string InstallmentDate { get; set; }
        [Required(ErrorMessage = "Particular is Required")]
        public string Particular { get; set; }
        public decimal? Outstanding { get; set; }
        [Display(Name = "Installment Amount")]
        public decimal? InstallmentAmount { get; set; }
        [Display(Name = "Interest Amount")]
        public decimal? InterestAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? PrincipleAmount { get; set; }
        public short? BranchId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string FatherName { get; set; }
        public string CustomerImage { get; set; }
        public decimal? ExtraFee { get; set; }
        public byte NumberOfInstallment { get; set; }
        public decimal? RemainingOutstanding { get; set; }
        public string Username { get; set; }
        public bool IsRejected { get; set; }
        public string RejectReason { get; set; }
        public bool IsPartial { get; set; }
        public string TransactionId { get; set; }
        public string TransactionDate { get; set; }
        public bool IsChequeEntry { get; set; }
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }
        [Display(Name = "Cheque No")]
        public string ChequeNo { get; set; }
        [Display(Name = "Bank Accoun No")]
        public string BankAccountNo { get; set; }
        public decimal? DueInstallmentAmount { get; set; }
        public bool IsCleared { get; set; }
        public bool IsEntry { get; set; }
        public int InstallmentNo { get; set; }
        public decimal? BeginningBalance { get; set; }
        public decimal? EndingBalance { get; set; }
        public decimal? Principle { get; set; }
        public decimal? Interest { get; set; }
        public decimal? InterestRate { get; set; }

        ///////////////////////////////////////////////

        public int COAId { get; set; }
        public string AccountCode { get; set; }


        //////////////////////////////////////////////////

        public decimal? FineInterest { get; set; }
        public decimal? TotalFineInterest { get; set; }
        public decimal? UnpaidPrinciple { get; set; }
        public decimal? TotalUnpaidPrinciple { get; set; }

        public decimal? ExtraPayment { get; set; }
        public byte? InstallmentTypeId { get; set; }

        public string VoucherNo { get; set; }

        //////////////////////////////////////////////////

    }
}