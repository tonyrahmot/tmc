﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Accounts.Models
{
    public class COA:CommonModel
    {
        [Key]
        public int COAId { get; set; }

        [Required]
        [Display(Name = "Account Code")]
        public string AccountCode { get; set; }

        [Required]
        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Group")]
        public int GroupId { get; set; }
        public IEnumerable<AccountGroupModel> AccountGroups { get; set; }

        public int COACategoryId { get; set; }
        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        public IEnumerable<COACategory> COACategoryInfo { get; set; }

        public bool IsSubCode { get; set; }
        public bool IsTransactional { get; set; }
        public bool IsBankAccount { get; set; }

        public int TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }

        [Display(Name = "Sub Code")]
        public int? SubCodeId { get; set; }

        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }

        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [Display(Name = "Branch Name")]
        public string BranchName { get; set; }

        [Display(Name = "Bank A/C No.")]
        public string BankAccountNo { get; set; }

        [Display(Name = "Supplier Code")]
        public string SupplierCode { get; set; }

        [Display(Name = "Supplier Name")]
        public string SupplierName { get; set; }

        [Display(Name = "Supplier Company")]
        public string SupplierCompany { get; set; }

        [Display(Name = "Client Code")]
        public string ClientCode { get; set; }

        [Display(Name = "Client Name")]
        public string ClientName { get; set; }

        [Display(Name = "Client Company")]
        public string ClientCompany { get; set; }
        public string Phone { get; set; }
        public string GroupName { get; set; }
        public string CPhone { get; set; }

        public DateTime OpeningBalanceDate { get; set; }

        [Display(Name = "Name")]
        public string BalanceSheetName { get; set; }

        public int BalanceSheetId { get; set; }

        public decimal? Balance { get; set; }

        public string BalanceInwords { get; set; }
    }
}