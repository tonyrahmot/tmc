﻿using System;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FixedDeposit.Models
{
    //--ata
    public class FixedDepositModel:CommonModel
    {
        public long Id { get; set; }

        public string TransactionId { get; set; }

        public DateTime TransactionDate { get; set; }

        public string TransactionType { get; set; }

        public string CustomerId { get; set; }
        public string CustomerName { get; set; }

        public string FatherName { get; set; }
        public string Particular { get; set; }

        public string AccountNumber { get; set; }

        public decimal? InterestRate { get; set; }

        public decimal? Capital { get; set; }

        public string DurationofMonth { get; set; }

        public DateTime? MatureDate { get; set; }

        public decimal? MatureAmount { get; set; }
        public decimal? PreMatureAmount { get; set; }

        public decimal? InterestAmount { get; set; }
        public decimal? Amount { get; set; }

        public decimal Debit { get; set; }

        public decimal Credit { get; set; }

        public decimal? Balance { get; set; }

        public decimal? AccruedBalance { get; set; }
        public string CustomerImage { get; set; }
        public short? BranchId { get; set; }
        public string BranchName { get; set; }
        public string CompanyName { get; set; }
        public string Username { get; set; }
        public string RejectReason { get; set; }
        public int COAId { get; set; }
    }
}