﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Areas.Accounts.Models;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Models
{
    public class LookUpModel:CommonModel
    {
        public int Id { get; set; }
        
        public string AccountType { get; set; }
        [Required]
        [Display(Name = "Account Type")]
        public int AccountTypeId { get; set; }
        [Required]
        [Display(Name = "COA Head")]
        public int? COAId { get; set; }
        public string AccountCode { get; set; }

        public IEnumerable<COA> ListOfCoaHeads { get; set; } 

    }
}