﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;

namespace TonyMultipurpose.Models
{
    public class CommonModel
    {
        public bool IsCanceled { get; set; }
        public bool IsApproved { get; set; }
        [Display(Name = "User Status")]
        public bool IsActive { get; set; }
        [Display(Name = "User Status")]
        public string UserStatus { get; set; }
        public short CreatedBy { get; set; }
        public string CreatedName { get; set; }
        public DateTime CreatedDate { get; set; }
        public short? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsRejected { get; set; }
        public string RejectReason { get; set; }
        public bool? IsReversed { get; set; }
        public byte? SortedBy { get; set; }
        public string Remarks { get; set; }
        public short? BranchId { get; set; }
        public short? CompanyId { get; set; }
    }
}