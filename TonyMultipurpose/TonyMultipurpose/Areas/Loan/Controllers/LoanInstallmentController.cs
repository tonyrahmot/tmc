﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.Loan.BLL;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Transaction.BLL;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.Loan.Controllers
{
   [AuthenticationFilter]
    public class LoanInstallmentController : Controller
    {
        TermLoanBLL objTermLoanBLL = new TermLoanBLL();
        TransactionBLL objTransactionBll = new TransactionBLL();
        public JsonResult AutoCompleteTermLoanId(string term)
        {
            var result = objTermLoanBLL.GetAllTermLoanIdForAutocomplete(term);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TermLoanDuePayment()
        {
            return View();
        }

        public JsonResult GetConvertNumberToWord(int number)
        {
            var exsits = objTransactionBll.GetConvertNumberToWord(number);
            return new JsonResult { Data = exsits };
        }

        public JsonResult GetTermLoanInfoByLoanId(string LoanNo)
        {
            var info = objTermLoanBLL.GetTermLoanInfoByLoanId().Where(a => a.LoanNo == LoanNo)
                .OrderByDescending(x => x.Id)
                .Take(1)
                .FirstOrDefault();
            var p = objTermLoanBLL.GetInstallmentNoAndDateById(LoanNo);
            //var p = objTermLoanBLL.GetDueInstallment().Where(a => a.LoanId == LoanId)
            //    .OrderByDescending(x => x.Id)
            //    .Take(1)
            //    .FirstOrDefault();
            return new JsonResult { Data = new { info = info,p = p } };
        }
    }
}