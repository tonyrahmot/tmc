
//json to date converter
function FormateDate(jsonDate) {
    var monthNames = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Novr", "Dec"
    ];
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    var mm = monthNames[date.getMonth()];
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    var formmatedDate = dd + '/' + mm + '/' + yyyy;
    return formmatedDate;
}

//date picker
$('.datepicker').datepicker({
    dateFormat: "dd/M/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-60:+0"
});



    $(document).ready(function () {
        $('.js-example-basic-single').select2();
        $('#UpdateCustomerData').hide();

        GetAddtionalInfoData();

        function GetAddtionalInfoData() {
            var LoanId = $('#LRefId').val();
            var data = { loanId: LoanId };
            $.ajax({
                url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetAddtionalInfoData", 
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (data) {
                    bindAddtionalInfoData(data);

                }
            });
        }

        function bindAddtionalInfoData(data) {

            $('#hdfLoanIdForAddtioanlInfo').val(data.data[0].LoanId);
            $('#hdfCustomerDeviationId').val(data.data[0].Id);
            $('#CustomerDetail').val(data.data[0].CustomerDetail);
            $('#GuarantorsDetail').val(data.data[0].GuarantorsDetail);
            $('#Deviation').val(data.data[0].Deviation);
            $('#ApproverComment').val(data.data[0].ApproverComment);

            $('#submitCustomerData').show();
            $('#CustomerDatar_Clear').show();
        }

        function ClearAddtionalInfoData() {
            //$('#hdfCustomerDeviationId').val('');
            $('#CustomerDetail').val('');
            $('#GuarantorsDetail').val('');
            $('#Deviation').val('');
            $('#ApproverComment').val('');
            $('#submitCustomerData').show();
            $('#UpdateCustomerData').hide();
            $('#deleteCustomerData').hide();
            document.getElementById('showMessage').innerHTML = '';
        }

        $('#CustomerDatar_Clear').click(function () {
            ClearAddtionalInfoData();
            $('#submitCustomerData').show();
            $('#UpdateCustomerData').hide();
            $('#deleteCustomerData').hide();
            $('#CustomerDatar_Clear').show();
        });

        $('#clear').click(function () {
            window.location.reload();
        });

    });

    $(document).ready(function() {
        $('#fileDisplay').hide();
        var oScanDocumentTable;
        ScanDocumentTable();

        function ScanDocumentTable() {
            oScanDocumentTable = $('#ScanDocumentTable').DataTable({
                "ajax": {
                    url: window.applicationBaseUrl + "Loan/LoanApplicationApproval/GetScanDocumnetInfo", 
                    "type": "get",
                    "data": {
                        "LoanId": $('#LRefId').val()
                    },
                    "datatype": "json"
                },
                "columns": [
                    { "data": "LoanId" },
                    { "data": "DocumentName" },
                    //{
                    //    "data": "ImagePath",
                    //    "render": function(data, type, row) {
                    //        return '<img src="' + data + '" width="40px" height="40px"/>';
                    //    }
                    //}
                ],
                "order": [[0, "desc"]],
                searching: false,
                paging: false,
                "bInfo": false
            });
            $('#ScanDocumentTable tbody').on('click', 'tr', function() {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    oScanDocumentTable.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
                bindDataForScanDocumentTable(oScanDocumentTable.row(this).data());
            });
        }

        function bindDataForScanDocumentTable(data) {
            $('#hiddenScanDocumnetId').val(data.Id);
            $('#LoanId').val(data.LoanId);
            document.getElementById('DocumentName').innerHTML = data.DocumentName;
            //$('#imageDisplay').attr('src', data.ImagePath);
            $('#imageDisplay').attr('src', '');
            $('#fileDisplay').attr('src', '');
            if (data.ImagePath.indexOf('.jpg') !== -1 || data.ImagePath.indexOf('.jpeg') !== -1 || data.ImagePath.indexOf('.png') !== -1 || data.ImagePath.indexOf('.gif') !== -1) {
                $('#fileDisplay').hide();
                $('#imageDisplay').show();
                $('#imageDisplay').attr('src', data.ImagePath);
            } else {
                $('#imageDisplay').hide();
                $('#fileDisplay').show();
                $('#fileDisplay').attr('src', '../../..' + data.ImagePath);
            }
            $('#hiddenScanDocumentImagePath').val(data.OldImagePath);
        }

        //scan document save area

        $("#fileUpload").change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imageDisplay').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });

        function ClearAllData() {
            $('#DocumentName').html('');
            $('#LoanId').val('');
            $('#fileUpload').val('');
            $('#imageDisplay').attr('src', '');
            $('#fileDisplay').attr('src', '');
            $('#fileDisplay').hide();
        }

        $('#ScanDocument_Clear').click(function() {
            ClearAllData();
        });



    });

    $(document).ready(function() {

        $('#updateSignatories').hide();
        $('#deleteSignatories').hide();

        function ClearSignatoryData() {
            $('#FunctionalPosition').val('');
            $('#EmpCode').val('0');
            document.getElementById('EmpCode').innerHTML = '';
            document.getElementById('DesignationName').innerHTML = '';
            document.getElementById('FunctionalPosition').innerHTML = '';
            $('#saveSignatories').show();
            $('#updateSignatories').hide();
            $('#deleteSignatories').hide();
        }
        $('#Signatories_Clear').click(function () {
            ClearSignatoryData();
            $('#updateSignatories').hide();
            //$('#deleteSignatories').hide();
            $('#saveSignatories').show();
        });

        var oSignatoryDataTable;
        LoadSignatoryDataTable();
        function LoadSignatoryDataTable() {
            oSignatoryDataTable = $('#SignatoryDataTable').DataTable({
                "ajax": {
                    url: window.applicationBaseUrl + "Loan/ApprovedLoanList/GetSignatoryDataForDataTable",
                    "type": "get",
                    "data": {
                        "LoanId": $('#LRefId').val()
                    },
                    "datatype": "json"
                },
                "columns": [
                    { "data": "LoanId" },
                    { "data": "FunctionalPosition" },
                    { "data": "EmployeeName" },
                    { "data": "DesignationName" }


                ],
                "order": [[0, "desc"]],
                searching: false,
                paging: false,
                "bInfo": false
            });
            $('#SignatoryDataTable tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    oSignatoryDataTable.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
                bindSignatoryData(oSignatoryDataTable.row(this).data());
            });
        }

        function bindSignatoryData(data) {

            $('#hdfSignatoryId').val(data.Id);
            $('#hdfLoanIdForSignatory').val(data.LoanId);
            //$('#EmpCode').val(data.EmployeeName);
            //$('#FunctionalPosition').val(data.FunctionalPosition);
            document.getElementById('EmpCode').innerHTML = data.EmployeeName;
            document.getElementById('FunctionalPosition').innerHTML = data.FunctionalPosition;
            document.getElementById('DesignationName').innerHTML = data.DesignationName;
            $('#Signatories_Clear').show();
        }

    });
