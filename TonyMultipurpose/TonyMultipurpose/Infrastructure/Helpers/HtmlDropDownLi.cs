﻿using System;
using System.Web.Mvc;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public class HtmlDropDownLi : IDisposable
    {
        private readonly ViewContext _viewContext;

        public HtmlDropDownLi(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public void Dispose()
        {
            _viewContext.Writer.Write(
                "</ul>" +
                "</li> "
            );
        }
    }
}