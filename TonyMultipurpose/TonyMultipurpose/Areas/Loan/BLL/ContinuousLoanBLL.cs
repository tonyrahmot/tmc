﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using TonyMultipurpose.Areas.Loan.Models;
using TonyMultipurpose.Areas.Loan.ViewModels;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Loan.BLL
{
    public class ContinuousLoanBLL 
    {
        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;
        public List<string> GetContinuousLoanId(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objContinuousLoanIdList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("ContinuousLoanId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetContinuousLoanIdForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanId"] != null)
                        {
                            objContinuousLoanIdList.Add(objDbDataReader["LoanId"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objContinuousLoanIdList;
        }

        public ContinuousLoanViewModel GetContinuousLoanInfo(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            ContinuousLoanViewModel objContinuousLoan = new ContinuousLoanViewModel();
            try
            {

                objDbCommand.AddInParameter("LoanId", loanId);

                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetContinuousLoanInfo]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objContinuousLoan = new ContinuousLoanViewModel();

                        objContinuousLoan.LoanId = objDbDataReader["LoanId"].ToString();
                        objContinuousLoan.AvailableBalance = Convert.ToDecimal(objDbDataReader["AvailableBalance"].ToString());
                        objContinuousLoan.Outstanding = Convert.ToDecimal(objDbDataReader["Outstanding"].ToString());
                        objContinuousLoan.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objContinuousLoan.CustomerName = objDbDataReader["CustomerName"].ToString();
                        objContinuousLoan.FatherName = objDbDataReader["FatherName"].ToString();
                        objContinuousLoan.Mobile = objDbDataReader["Mobile"].ToString();
                        objContinuousLoan.CustomerLoanLimit = Convert.ToDecimal(objDbDataReader["CustomerLoanLimit"].ToString());
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objContinuousLoan.CustomerImage = imagePath;
                        }
                        else
                        {
                            objContinuousLoan.CustomerImage = "";
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objContinuousLoan;
        }

        public CommonResult SaveContinuousLoanWithdraw(ContinuousLoan continuousLoan)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            string transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            //var prefix = string.Concat(year + days);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "ContinuousLoanTransaction");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("TransactionId", transactionId);
            objDbCommand.AddInParameter("LoanId", continuousLoan.LoanId);
            objDbCommand.AddInParameter("TransactionDate", continuousLoan.TransactionDate);
            objDbCommand.AddInParameter("Particular", continuousLoan.Particular);
            objDbCommand.AddInParameter("IsChequeEntry", continuousLoan.IsChequeEntry);
            objDbCommand.AddInParameter("BankName", continuousLoan.BankName);
            objDbCommand.AddInParameter("BankAccountNo", continuousLoan.BankAccountNo);
            objDbCommand.AddInParameter("ChequeNo", continuousLoan.ChequeNo);
            objDbCommand.AddInParameter("CustomerId", continuousLoan.CustomerId);
            objDbCommand.AddInParameter("WithdrawAmount", continuousLoan.WithdrawAmount);
            objDbCommand.AddInParameter("AvailableBalance", continuousLoan.AvailableBalance);
            objDbCommand.AddInParameter("Outstanding", continuousLoan.Outstanding);
            objDbCommand.AddInParameter("COAId", continuousLoan.COAId);
            objDbCommand.AddInParameter("VoucherNo", continuousLoan.VoucherNo);

            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_SaveContinuousLoanWithdraw]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Save Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }

        public CommonResult SaveContinuousLoanDeposit(ContinuousLoan continuousLoan)
        {
            int noRowCount = 0;
            var oCommonResult = new CommonResult();
            oCommonResult.Status = false;
            oCommonResult.Message = "Failed";

            string transactionId = string.Empty;
            DbDataReader objDbDataReader = null;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            int Id = 0;

            string year = Convert.ToString(DateTime.Now.Year);
            string days = Convert.ToString(DateTime.Now.DayOfYear);
            //var prefix = string.Concat(year + days);
            string prefix = year + days;

            objDbCommand.AddInParameter("FieldName", "TransactionId");
            objDbCommand.AddInParameter("TableName", "ContinuousLoanTransaction");
            objDbCommand.AddInParameter("Prefix", prefix);
            objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetMaxIdV2]",
                CommandType.StoredProcedure);
            if (objDbDataReader.HasRows)
            {
                while (objDbDataReader.Read())
                {
                    transactionId = prefix;
                    Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                    transactionId += Convert.ToString(Id).PadLeft(4, '0');
                }
            }
            objDbDataReader.Close();

            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = null;
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

            objDbCommand.AddInParameter("TransactionId", transactionId);
            objDbCommand.AddInParameter("LoanId", continuousLoan.LoanId);
            objDbCommand.AddInParameter("TransactionDate", continuousLoan.TransactionDate);
            objDbCommand.AddInParameter("Particular", continuousLoan.Particular);
            objDbCommand.AddInParameter("IsChequeEntry", continuousLoan.IsChequeEntry);
            objDbCommand.AddInParameter("BankName", continuousLoan.BankName);
            objDbCommand.AddInParameter("BankAccountNo", continuousLoan.BankAccountNo);
            objDbCommand.AddInParameter("ChequeNo", continuousLoan.ChequeNo);
            objDbCommand.AddInParameter("CustomerId", continuousLoan.CustomerId);
            objDbCommand.AddInParameter("DepositAmount", continuousLoan.DepositAmount);
            objDbCommand.AddInParameter("AvailableBalance", continuousLoan.AvailableBalance);
            objDbCommand.AddInParameter("Outstanding", continuousLoan.Outstanding);
            objDbCommand.AddInParameter("COAId", continuousLoan.COAId);
            objDbCommand.AddInParameter("VoucherNo", continuousLoan.VoucherNo);

            objDbCommand.AddInParameter("CreatedBy", SessionUtility.TMSessionContainer.UserID);
            objDbCommand.AddInParameter("BranchId", SessionUtility.TMSessionContainer.BranchId);
            objDbCommand.AddInParameter("CompanyId", SessionUtility.TMSessionContainer.CompanyId);

            try
            {
                noRowCount = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_SaveContinuousLoanDeposit]",
                    CommandType.StoredProcedure);

                if (noRowCount > 0)
                {
                    objDbCommand.Transaction.Commit();
                    oCommonResult.Status = true;
                    oCommonResult.Message = "Save Successful";
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }
            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return oCommonResult;
        }



        #region Continuous Loan Transactions Approval
        public List<ContinuousLoanViewModel> GetUnApprovedContinuousLoanTransactionList()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<ContinuousLoanViewModel> listOfConLOanTransactions = new List<ContinuousLoanViewModel>();
            ContinuousLoanViewModel objContinuousLoanModel;
            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetUnApprovedContinuousLoanTransactionList]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objContinuousLoanModel = new ContinuousLoanViewModel();
                        objContinuousLoanModel.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objContinuousLoanModel.LoanId = objDbDataReader["LoanId"].ToString();
                        objContinuousLoanModel.TransactionDateShow = String.Format("{0:dd-MMM-yyyy}", objDbDataReader["TransactionDateShow"]);
                        objContinuousLoanModel.Debit = Convert.ToDecimal(objDbDataReader["Debit"].ToString());
                        objContinuousLoanModel.Credit = Convert.ToDecimal(objDbDataReader["Credit"].ToString());
                        objContinuousLoanModel.Outstanding = Convert.ToDecimal(objDbDataReader["Outstanding"].ToString());
                        objContinuousLoanModel.AvailableBalance = Convert.ToDecimal(objDbDataReader["AvailableBalance"].ToString());
                        objContinuousLoanModel.IsChequeEntry = Convert.ToBoolean(objDbDataReader["IsChequeEntry"].ToString());
                        objContinuousLoanModel.VoucherNo = objDbDataReader["VoucherNo"].ToString();
                        objContinuousLoanModel.BankName = objDbDataReader["BankName"].ToString();
                        objContinuousLoanModel.BankAccountNo = objDbDataReader["BankAccountNo"].ToString();
                        objContinuousLoanModel.ChequeNo = objDbDataReader["ChequeNo"].ToString();

                        objContinuousLoanModel.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objContinuousLoanModel.CustomerName = objDbDataReader["CustomerName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        objContinuousLoanModel.CreatedName = objDbDataReader["CreatedName"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objContinuousLoanModel.CustomerImage = imagePath;
                        }
                        else
                        {
                            objContinuousLoanModel.CustomerImage = "\\Not Found\\";
                        }
                        listOfConLOanTransactions.Add(objContinuousLoanModel);

                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return listOfConLOanTransactions;
        }
        public string ApproveContinuousLoanTransactions(string[] ch)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var id in ch)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", id);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_ApproveContinuousLoanTransactions]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public string RejectContinuousLoanTransactions(List<ContinuousLoanViewModel> continuousLoanTransactions)
        {
            objDataAccess = DataAccess.NewDataAccess();
            int noOfAffacted = 0;

            foreach (var value in continuousLoanTransactions)
            {
                objDbCommand = null;
                objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);

                objDbCommand.AddInParameter("TransactionId", value.TransactionId);
                objDbCommand.AddInParameter("RejectReason", value.RejectReason);
                noOfAffacted = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[loan_RejectContinuousLoanTransactions]", CommandType.StoredProcedure);
                if (noOfAffacted > 0)
                {
                    objDbCommand.Transaction.Commit();
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    return "failed";
                }
            }
            return "Saved";
        }

        public ContinuousLoanViewModel GetContinuousLoanTransactionDetail(string transactionId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<ContinuousLoanViewModel> objContinuousLoanInfoList = new List<ContinuousLoanViewModel>();
            ContinuousLoanViewModel objContinuousLoan = new ContinuousLoanViewModel();
            try
            {

                objDbCommand.AddInParameter("TransactionId", transactionId);

                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetContinuousLoanTransactionDetail]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        objContinuousLoan = new ContinuousLoanViewModel();

                        objContinuousLoan = new ContinuousLoanViewModel();
                        objContinuousLoan.TransactionId = objDbDataReader["TransactionId"].ToString();
                        objContinuousLoan.LoanId = objDbDataReader["LoanId"].ToString();
                        objContinuousLoan.TransactionDateShow = String.Format("{0:dd-MMM-yyyy}", objDbDataReader["TransactionDateShow"]);
                        objContinuousLoan.Debit = Convert.ToDecimal(objDbDataReader["Debit"].ToString());
                        objContinuousLoan.Credit = Convert.ToDecimal(objDbDataReader["Credit"].ToString());
                        objContinuousLoan.Outstanding = Convert.ToDecimal(objDbDataReader["Outstanding"].ToString());
                        objContinuousLoan.AvailableBalance = Convert.ToDecimal(objDbDataReader["AvailableBalance"].ToString());
                        objContinuousLoan.IsChequeEntry = Convert.ToBoolean(objDbDataReader["IsChequeEntry"].ToString());
                        objContinuousLoan.ChequeEntryStatus = objDbDataReader["ChequeEntryStatus"].ToString();
                        objContinuousLoan.BankName = objDbDataReader["BankName"].ToString();
                        objContinuousLoan.BankAccountNo = objDbDataReader["BankAccountNo"].ToString();
                        objContinuousLoan.ChequeNo = objDbDataReader["ChequeNo"].ToString();

                        objContinuousLoan.CustomerId = objDbDataReader["CustomerId"].ToString();
                        objContinuousLoan.CustomerName = objDbDataReader["CustomerName"].ToString();
                        var imagePath = objDbDataReader["CustomerImage"].ToString();
                        objContinuousLoan.CreatedName = objDbDataReader["CreatedName"].ToString();
                        if (imagePath.Contains("Uploads"))
                        {
                            imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                            imagePath = "/" + imagePath;
                            objContinuousLoan.CustomerImage = imagePath;
                        }
                        else
                        {
                            objContinuousLoan.CustomerImage = "\\Not Found\\";
                        }

                        objContinuousLoanInfoList.Add(objContinuousLoan);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return objContinuousLoan;
        }
        #endregion


    }
}