﻿using System.ComponentModel.DataAnnotations;

namespace TonyMultipurpose.Areas.Loan.ViewModels
{
    public class AdditionalLoanDetailViewModel
    {
        [Display(Name = "Id")]
        public long AdditionalLoanDetailId { get; set; }

        [Display(Name = "")]
        public string LoanId { get; set; }

        [Display(Name = "Institute Name")]
        public string InstituteName { get; set; }

        [Display(Name = "Loan Amount")]
        public decimal? LoanAmount { get; set; }

        [Display(Name = "Loan Type")]
        public string LoanType { get; set; }

        [Display(Name = "Installment Amount")]
        public decimal? Installment { get; set; }

        [Display(Name = "Account No")]
        public string AccountNo { get; set; }

        [Display(Name = "Account Name")]
        public string AccountName { get; set; }

        [Display(Name = "Start Date")]
        public string StartDate { get; set; }

        [Display(Name = "Expire Date")]
        public string ExpireDate { get; set; }

        [Display(Name = "Present Outs Tk.")]
        public decimal? PresentOutAmount { get; set; }

        [Display(Name = "")]
        public short? CreatedBy { get; set; }

    }
}