﻿using System.Web.Mvc;
using TonyMultipurpose.Enums;

namespace TonyMultipurpose.Infrastructure.Helpers
{
    public static class HtmlExtensionsButton
    {
        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, HtmlButtonOpearation.NotSpecified,
              null, null, null, false, false, HtmlButtonTypes.Submit, null,
              htmlAttributes);
        }
        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          HtmlButtonOpearation buttonOpearation,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, buttonOpearation,
              null, null, null, false, false, HtmlButtonTypes.Submit, null,
              htmlAttributes);
        }

        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          string cssClass,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, HtmlButtonOpearation.NotSpecified,
             cssClass, null, null, false, false, HtmlButtonTypes.Submit, null,
             htmlAttributes);
        }

        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          HtmlButtonOpearation buttonOpearation,
          string cssClass,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, buttonOpearation,
             cssClass, null, null, false, false, HtmlButtonTypes.Submit, null,
             htmlAttributes);
        }

        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          string cssClass,
          string saAction,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, HtmlButtonOpearation.NotSpecified,
             cssClass, null, null, false, false, HtmlButtonTypes.Submit, saAction,
             htmlAttributes);
        }

        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          HtmlButtonOpearation buttonOpearation,
          string cssClass,
          string saAction,
          object htmlAttributes = null)
        {
            return BootstrapButton(htmlHelper, innerHtml, buttonOpearation,
             cssClass, null, null, false, false, HtmlButtonTypes.Submit, saAction,
             htmlAttributes);
        }

        public static MvcHtmlString BootstrapButton(
          this HtmlHelper htmlHelper,
          string innerHtml,
          HtmlButtonOpearation buttonOpearation,
          string cssClass,
          string name,
          string title,
          bool isFormNoValidate,
          bool isAutoFocus,
          HtmlButtonTypes buttonType,
          string saAction,
          object htmlAttributes = null)
        {
            TagBuilder tb = new TagBuilder("button");

            if (!string.IsNullOrWhiteSpace(cssClass))
            {
                if (!cssClass.Contains("btn-"))
                {
                    cssClass = "btn-primary " + cssClass;
                }
            }
            else
            {
                cssClass = "btn-primary";
            }

            tb.AddCssClass(cssClass);

            tb.AddCssClass("btn");

            if (!string.IsNullOrWhiteSpace(saAction))
            {
                tb.MergeAttribute("data-sa-action", saAction);
            }

            if (!string.IsNullOrWhiteSpace(title))
            {
                tb.MergeAttribute("title", title);
            }
            if (isFormNoValidate)
            {
                tb.MergeAttribute("formnovalidate", "formnovalidate");
            }
            if (isAutoFocus)
            {
                tb.MergeAttribute("autofocus", "autofocus");
            }

            tb.MergeAttributes(
              HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            HtmlExtensionsCommon.AddName(tb, name, "");

            switch (buttonType)
            {
                case HtmlButtonTypes.Submit:
                    tb.MergeAttribute("type", "submit");
                    break;
                case HtmlButtonTypes.Button:
                    tb.MergeAttribute("type", "button");
                    break;
                case HtmlButtonTypes.Reset:
                    tb.MergeAttribute("type", "reset");
                    break;
            }

            switch (buttonOpearation)
            {
                case HtmlButtonOpearation.Save:
                    innerHtml = "<i class=\"ace-icon fa fa-check\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.ViewDetails:
                    innerHtml = "<i class=\"ace-icon fa fa-fa-search-plus\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.Reset:
                    innerHtml = "<i class=\"ace-icon fa fa-refresh\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.DeleteInfo:
                    innerHtml = "<i class=\"ace-icon fa fa-trash\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.Login:
                    innerHtml = "<i class=\"ace-icon fa fa-key\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.Cancel:
                    innerHtml = "<i class=\"ace-icon fa fa-times\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.Block:
                    innerHtml = "<i class=\"ace-icon fa fa-ban\"></i> " + innerHtml;
                    break;
                case HtmlButtonOpearation.Search:
                    innerHtml = "<i class=\"ace-icon fa fa-search\"></i> " + innerHtml;
                    break;
            }

            tb.InnerHtml = innerHtml;


            return MvcHtmlString.Create(tb.ToString());
        }
    }
}