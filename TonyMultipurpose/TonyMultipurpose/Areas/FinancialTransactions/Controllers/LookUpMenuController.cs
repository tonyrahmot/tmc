﻿using System.Collections.Generic;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.AuthData;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    [AuthenticationFilter]
    public class LookUpMenuController : Controller
    {
        // GET: FinancialTransactions/LookUpMenu
        LookUpBLL objLookUpBll = new LookUpBLL();
        public ActionResult Index()
        {
            List<LookUpModel> listOfLookUp=new List<LookUpModel>();
            listOfLookUp = objLookUpBll.GetAllLookUpData();
            return View(listOfLookUp);
        }

        [HttpGet]
        public ActionResult Save()
        {
            ViewBag.AccountTypeList = objLookUpBll.GetAllAccountTypeLists();
            var model = new LookUpModel
            {
                ListOfCoaHeads = objLookUpBll.GetChartOfAccountHead()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(LookUpModel lookUpModel)
        {
            objLookUpBll.SaveLookUpData(lookUpModel);
           return RedirectToAction("Index", "LookUpMenu");
        }

    }
}