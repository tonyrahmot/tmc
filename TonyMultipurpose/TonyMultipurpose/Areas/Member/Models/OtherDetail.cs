﻿using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Member.Models
{
    //ataur
    public class OtherDetail:CommonModel
    {
        [Key]
        public int OtherDetailId { get; set; }

        public int MemberId { get; set; }

        public string DLBookNumber { get; set; }

        public string LoanIdNumber { get; set; }

        public string OthersBankInfo { get; set; }

        public string HouseInfrastructure { get; set; }

        public string LandProperty { get; set; }

        public string CattleInfo { get; set; }

        public string IncomeInformation { get; set; }

        public string IncomeSource { get; set; }
    }
}