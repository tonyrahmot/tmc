﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    [AuthenticationFilter]
    public class ChequeClearenceController : Controller
    {
        // GET: FinancialTransactions/ChequeClearence
        public ActionResult ChequeClearence()
        {
            FinancialTransactionsModel objFinancialTransactionsModel = new FinancialTransactionsModel();
            ChequeClearenceBLL objChequeClearenceBLL = new ChequeClearenceBLL();
            List<FinancialTransactionsModel> ChequeClearenceList = objChequeClearenceBLL.GetChequeClearenceDetails();
            return View(ChequeClearenceList);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Clearence")]
        public ActionResult Clearence(FormCollection form)
        {
            ChequeClearenceBLL objChequeClearenceBLL = new ChequeClearenceBLL();
            var ids = form.GetValues("Id");
            if(ids != null)
            {
                objChequeClearenceBLL.SavingsChequeClearence(ids);
            }
            return RedirectToAction("ChequeClearence");
        }


        [HttpGet]
        public ActionResult Detail(int Id)
        {
            ChequeClearenceBLL objChequeClearenceBLL = new ChequeClearenceBLL();
            if (Id == null) return null;
            var ChequeClearenceDetails = objChequeClearenceBLL.GetChequeClearenceDetailsById(Id);
            return View(ChequeClearenceDetails);
        }
    }
}