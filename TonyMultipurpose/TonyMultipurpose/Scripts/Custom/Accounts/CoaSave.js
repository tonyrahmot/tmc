﻿$(function () {
    $("#tabs").tabs();
});

$(document).ready(function () {
           //$("#CategoryName").change(function () {
           //    var name = $("#CategoryName").val();
           //    var status = $("#showErrorMessage");
           //    var user = $.trim(name);
           //    if (user.length >= 0) {
           //        status.html("Checking....");
           //        $.post('@Url.Action("GetCategoryNameCheck", "COACategory", new {Area = "Accounts" })', { CategoryName: name },
           //                    function (data) {
           //                        if (data === true) {
           //                            status.html("<font color=green>'<b>" + name + "</b>' is not exist !</font>");
           //                            $('#btn_submit').attr('disabled', false);
           //                        } else {
           //                            status.html("<font color=red>'<b>" + name + "</b>' is exist !</font>");
           //                            $('#btn_submit').attr('disabled', true);
           //                        }
           //                    });
           //    } else {
           //        status.html("");
           //    }
           //});

               $('.datepicker').datepicker({
                   dateFormat: "dd/M/yy",
                   changeMonth: true,
                   changeYear: true,
                   yearRange: "-60:+0"
               });
    $('#OpeningBalanceDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    $('#btn_submit').click(function () {
        var categoryName = $("#CategoryName option:selected").val();
        var groupId = $("#GroupId").val();
        var accountName = $("#AccountName").val();
        var accountCode = $("#AccountCode").val();
        var balanceSheetId = $("#BalanceSheetId option:selected").val();
        var status = $("#showErrorMessage");
        var allData = true;
        if (groupId == '') {
            allData = false;
            status.html("Please Select Group Name");
            return allData;
        }
        else if (categoryName == 0) {
            allData = false;
            status.html("Please Select Chart of Accounts Category Name");
            return allData;
        }
        else if (accountCode == '') {
            allData = false;
            status.html("Please Type Account Code");
            return allData;
        }
        else if (accountName == '') {
            allData = false;
            status.html("Please Type Account Name");
            return allData;
        }
            //else if (balanceSheetId == 0) {
            //    allData = false;
            //    status.html("Please Select Balance Sheet");
            //    return allData;
            //}
                
        else if (document.getElementById("IsSubCode").checked) {
            var SubCodeId = $('#SubCodeId option:selected').val();
            if (SubCodeId == 0)
            {
                allData = false;
                status.html("Please Select SubCode");
                return allData;
            }
            else
            {
                            
                var num = accountCode;
                var regex = /^[0-9]+$/;
                if (num.match(regex)) {
                    allData = true;
                    status.html("");
                    return allData;
                }
                else {
                    allData = false;
                    status.html("Account Code will be only integer numbers");
                    return allData;
                }
            }
        }
        else
        {
            var num = accountCode;
            var regex = /^[0-9]+$/;
            if (num.match(regex)) {
                status.html("");
                return allData;
            }
            else
            {
                allData = false;
                status.html("Account Code will be only integer numbers");
                return allData;
            }
        }
    });



    $('#IsSubCode').change(function () {
        if (document.getElementById("IsSubCode").checked) {
            $('#dvsubcode').show();
        }
        else {
            $('#dvsubcode').hide();
        }
    });

    $("#GroupId").change(function () {
        $('#CategoryName').empty();
        var groupId = $("#GroupId").val();

        $.post(window.applicationBaseUrl + "Accounts/COA/GetCategoryNameGroupWise",{ groupId: groupId },
                        function (data) {
                            $("#CategoryName").append('<option value="0">--Select Category Name--</option>');
                            $.each(data, function (key, value) {
                                $("#CategoryName").append('<option value=' + value.Id + '>' + value.CategoryName + '</option>');
                            });
                        });
    });


    $('#CategoryName').change(function () {
        $('#SubCodeId').empty();
        var groupId = $("#GroupId").val();
        var categoryName = $("#CategoryName option:selected").val();
        $.post(window.applicationBaseUrl + "Accounts/COA/GetSubCodeCategoryNameGroupWise", { groupId: groupId, categoryName: categoryName },
                        function (data) {
                            $("#SubCodeId").append('<option value="0">--Select SubCode--</option>');
                                   
                            $.each(data, function (key, value) {
                                $("#SubCodeId").append('<option value=' + value.COAId + '>' + value.AccountName + '</option>');
                                      
                            });
                        });
    });

    $('#CategoryName').change(function () {
        $('#BalanceSheetId').empty();
        var groupId = $("#GroupId").val();
        var categoryName = $("#CategoryName option:selected").val();
        $.post(window.applicationBaseUrl + "Accounts/COA/GetBalanceSheetNameByGroupAndCategoryId",{ groupId: groupId, categoryName: categoryName },
                        function (data) {
                                 
                            $("#BalanceSheetId").append('<option value="0">--Select Balance Sheet--</option>');
                            $.each(data, function (key, value) {
                                $("#BalanceSheetId").append('<option value=' + value.BalanceSheetId + '>' + value.BalanceSheetName + '</option>');

                            });
                        });
    });


    $("#AccountName").keyup(function () {
        var accountName = $("#AccountName").val();
        var status = $("#showErrorMessage");
        var user = $.trim(accountName);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "Accounts/COA/GetAccountNameCheck", { accountName: accountName },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + accountName + "</b>' is not exist !</font>");
                                $('#btn_submit').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + accountName + "</b>' is exist !</font>");
                                $('#btn_submit').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }

    });

    $("#AccountCode").keyup(function () {
        var accountCode = $("#AccountCode").val();
        var status = $("#showErrorMessage");
        var user = $.trim(accountCode);
        if (user.length >= 0) {
            status.html("Checking....");
            $.post(window.applicationBaseUrl + "Accounts/COA/GetAccountCodeCheck", { accountCode: accountCode },
                        function (data) {
                            if (data === true) {
                                status.html("<font color=green>'<b>" + accountCode + "</b>' is not exist !</font>");
                                $('#btn_submit').attr('disabled', false);
                            } else {
                                status.html("<font color=red>'<b>" + accountCode + "</b>' is exist !</font>");
                                $('#btn_submit').attr('disabled', true);
                            }
                        });
        } else {
            status.html("");
        }

    });

    $("#AccountCode").keypress(function () {
        var evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        else if (charCode == 13 || charCode == 46) {
            return false;
        }
        status = "";
        return true;
    });

    $("#Credit").keypress(function () {
        var evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        else if (charCode == 13 || charCode == 46) {
            return false;
        }
        status = "";
        return true;
    });

    $("#Debit").keypress(function () {
        var evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        else if (charCode == 13 || charCode == 46) {
            return false;
        }
        status = "";
        return true;
    });

    //$('#AccountName').bind("cut paste", function (e) {
    //    e.preventDefault();
    //});

    $('#AccountName').attr('autocomplete', 'off');

    $('#AccountCode').attr('autocomplete', 'off');

    //$('#AccountCode').bind("cut paste", function (e) {
    //    e.preventDefault();
    //});

});