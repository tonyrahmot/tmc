﻿namespace TonyMultipurpose.Areas.HR.ViewModels
{
    public class BenefitViewModel
    {
        public string BenefitTypeName { get; set; }
        public string EmployeeName { get; set; }
    }
}