﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using TonyMultipurpose.Areas.TermLoan.Models;
using TonyMultipurpose.DAL;

namespace TonyMultipurpose.Areas.TermLoan.BLL
{
    public class TermLoanBLL : CommonGetway
    {

        private IDataAccess objDataAccess;
        private DbCommand objDbCommand;

        public int DeleteHasTempRow(string data)
        {
            objCommand = new SqlCommand("TestspDeleteHasTempRow", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.Add(new SqlParameter("data", data));
            objConnection.Open();
            int RowAffacted = objCommand.ExecuteNonQuery();
            if (RowAffacted > 0)
            {
                string a = "Success";
            }
            else
            {
                string b = "Faield";
            }
            objConnection.Close();
            return RowAffacted;
        }
        public string ExistingCheckUniqueNumber(string existingCheck)
        {
            string lastUniqueNumber = string.Empty;
            objCommand = new SqlCommand("TestspGetExistUniqNumber", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.Add(new SqlParameter("ExistingCheck", existingCheck));
            objConnection.Open();
            objDataReader = objCommand.ExecuteReader();
            if (objDataReader.HasRows)
            {
                while (objDataReader.Read())
                {
                    lastUniqueNumber = objDataReader["LastUniqueNumber"].ToString();
                }
            }
            else
            {
                string b = "Faield";
            }
            objConnection.Close();
            return lastUniqueNumber;
        }
        public int UniqueNumberSave(string UserUniqId, string UserTempId)
        {
            objCommand = new SqlCommand("TestspUniqueNumberSave", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.Add(new SqlParameter("UserUniqId", UserUniqId));
            objCommand.Parameters.Add(new SqlParameter("UserTempId", UserTempId));
            objConnection.Open();
            int RowAffacted = objCommand.ExecuteNonQuery();
            if (RowAffacted > 0)
            {
                string a = "Success";
            }
            else
            {
                string b = "Faield";
            }
            objConnection.Close();
            return RowAffacted;
        }
        string Connection = WebConfigurationManager.ConnectionStrings["TonyMultipurposeDB"].ConnectionString.ToString();
        public bool SaveGuarontorInfo(Guarantor objGuarantor)
        {
            bool isSucessful = false;
            objConnection = new SqlConnection(Connection);
            objCommand = new SqlCommand("uspSaveGuarantorInfo", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.AddWithValue("CustomerId", objGuarantor.CustomerId);
            objCommand.Parameters.AddWithValue("Name", objGuarantor.Name);
            objCommand.Parameters.AddWithValue("NID", objGuarantor.NID);
            objCommand.Parameters.AddWithValue("PhoneNo", objGuarantor.PhoneNo);
            objConnection.Open();

            int rowAffected = objCommand.ExecuteNonQuery();
            if (rowAffected > 0)
            {
                isSucessful = true;
            }

            objConnection.Close();
            return isSucessful;
        }
        public List<string> GetAutoCompleteCustomerIdForGuarantor(string term)
        {
            objConnection = new SqlConnection(Connection);
            objCommand = new SqlCommand("uspGetCustomerIdForGuarantor", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.AddWithValue("CustomerId", term);
            objConnection.Open();
            SqlDataReader objDataReader = objCommand.ExecuteReader();
            List<string> list = new List<string>();

            if (objDataReader.HasRows)
            {
                while (objDataReader.Read())
                {
                    list.Add(objDataReader["CustomerId"].ToString());
                }
            }
            objConnection.Close();
            return list;
        }
        public List<Guarantor> GetGuarantorInfoByCustomerId(string customerId)
        {
            objConnection = new SqlConnection(Connection);
            objCommand = new SqlCommand("uspGetGuarantorInfoByCustomerId", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.AddWithValue("CustomerId", customerId);
            objConnection.Open();
            SqlDataReader objDataReader = objCommand.ExecuteReader();
            List<Guarantor> guarantorList = new List<Guarantor>();
            Guarantor objGuarantor;
            if (objDataReader.HasRows)
            {
                while (objDataReader.Read())
                {
                    objGuarantor = new Guarantor();
                    objGuarantor.GuarantorId = Convert.ToInt16(objDataReader["GuarantorId"].ToString());
                    objGuarantor.Name = objDataReader["Name"].ToString();
                    objGuarantor.NID = objDataReader["NID"].ToString();
                    objGuarantor.PhoneNo = objDataReader["PhoneNo"].ToString();

                    guarantorList.Add(objGuarantor);
                }
            }
            objConnection.Close();
            return guarantorList;
        }
        public Guarantor GetGuarantorInfoForUpdate(int? GuarantorId)
        {
            objConnection = new SqlConnection(Connection);
            objCommand = new SqlCommand("uspGetGuarantorInfoForUpdate", objConnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.Parameters.AddWithValue("GuarantorId", GuarantorId);
            objConnection.Open();
            SqlDataReader objDataReader = objCommand.ExecuteReader();
            List<Guarantor> guarantorList = new List<Guarantor>();
            Guarantor objGuarantor=new Guarantor(); ;
            if (objDataReader.HasRows)
            {
                while (objDataReader.Read())
                {
                    objGuarantor = new Guarantor();
                    objGuarantor.Name = objDataReader["Name"].ToString();
                    objGuarantor.NID = objDataReader["NID"].ToString();
                    objGuarantor.PhoneNo = objDataReader["PhoneNo"].ToString();
                    guarantorList.Add(objGuarantor);
                }
            }
            objConnection.Close();
            return objGuarantor;
        }

        /// <summary>
        /// Install Information start
        /// </summary>
        /// <returns></returns>
        public List<InstallmentInformation> GetInstallmentInformation()
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<InstallmentInformation> infoList = new List<InstallmentInformation>();
            InstallmentInformation objI;

            try
            {
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "uspGetTermLoanInstallmentInformation", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {

                    while (objDbDataReader.Read())
                    {
                        objI = new InstallmentInformation();
                        objI.Id = Convert.ToInt32(objDbDataReader["Id"].ToString());
                        objI.LoanId = objDbDataReader["LoanId"].ToString();
                        objI.InstallmentDate = objDbDataReader["InstallmentDate"].ToString();
                        objI.EntryDate = objDbDataReader["EntryDate"].ToString();
                        objI.Particular = objDbDataReader["Particular"].ToString();
                        objI.Outstanding = objDbDataReader["Outstanding"].ToString();
                        objI.InstallmentAmount = Convert.ToDecimal(objDbDataReader["InstallmentAmount"].ToString());
                        objI.DepositedAmount = Convert.ToDecimal(objDbDataReader["DepositedAmount"].ToString());
                        objI.InterestAmount = Convert.ToDecimal(objDbDataReader["InterestAmount"].ToString());
                        objI.PrincipleAmount = Convert.ToDecimal(objDbDataReader["PrincipleAmount"].ToString());

                        infoList.Add(objI);
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }
            return infoList;
        }
        public bool SaveInstallmentInformation(InstallmentInformation objInfo)
        {
            bool isSucessful = false;
            int rowAffected = 0;
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.Serializable);
            objDbCommand.AddInParameter("LoanId", objInfo.LoanId);
            objDbCommand.AddInParameter("EntryDate", objInfo.EntryDate);
            objDbCommand.AddInParameter("InstallmentDate", objInfo.InstallmentDate);
            objDbCommand.AddInParameter("Particular", objInfo.Particular);
            objDbCommand.AddInParameter("InstallmentAmount", objInfo.InstallmentAmount);
            objDbCommand.AddInParameter("DepositedAmount", objInfo.DepositedAmount);
            objDbCommand.AddInParameter("InterestAmount", objInfo.InterestAmount);
            objDbCommand.AddInParameter("PrincipleAmount", objInfo.PrincipleAmount);
            objDbCommand.AddInParameter("Outstanding", objInfo.Outstanding);
            objDbCommand.AddInParameter("CompanyId", objInfo.CompanyId);
            objDbCommand.AddInParameter("BranchId", objInfo.BranchId);
            objDbCommand.AddInParameter("CreatedBy", objInfo.CreatedBy);
            objDbCommand.AddInParameter("CreatedDate", objInfo.CreatedDate);
            try
            {
                 rowAffected = objDataAccess.ExecuteNonQuery(objDbCommand, "[dbo].[uspSaveTermLoanInstallmentInformation]",
                    CommandType.StoredProcedure);
                if (rowAffected > 0)
                {
                    objDbCommand.Transaction.Commit();
                    isSucessful = true;
                }
                else
                {
                    objDbCommand.Transaction.Rollback();
                    isSucessful = false;
                }
            }
            catch (Exception ex)
            {
                objDbCommand.Transaction.Rollback();
                throw new Exception("Database Error Occured", ex);
            }

            finally
            {
                objDataAccess.Dispose(objDbCommand);
            }
            return isSucessful;
        }

        public List<string> GetAllTermLoanIdForAutocomplete(string term)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            List<string> objList = new List<string>();
            try
            {
                objDbCommand.AddInParameter("LoanId", term);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[uspGetTermLoanIdForAutocomplete]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanNo"] != null)
                        {
                            objList.Add(objDbDataReader["LoanNo"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objList;
        }

        public InstallmentInformation GetTermLoanInfoByLoanId(string loanId)
        {
            objDataAccess = DataAccess.NewDataAccess();
            objDbCommand = objDataAccess.GetCommand(true, IsolationLevel.ReadCommitted);
            DbDataReader objDbDataReader = null;
            InstallmentInformation objI = new InstallmentInformation();
            try
            {
                objDbCommand.AddInParameter("LoanId", loanId);
                objDbDataReader = objDataAccess.ExecuteReader(objDbCommand, "[dbo].[loan_GetTermLoanInfoByLoanId]", CommandType.StoredProcedure);

                if (objDbDataReader.HasRows)
                {
                    while (objDbDataReader.Read())
                    {
                        if (objDbDataReader["LoanId"] != null)
                        {
                            objI.CustomerName = objDbDataReader["CustomerName"].ToString();
                            objI.CustomerId = objDbDataReader["CustomerId"].ToString();
                            objI.FatherName = objDbDataReader["FatherName"].ToString();
                            objI.InterestAmount = Convert.ToDecimal(objDbDataReader["InterestAmount"].ToString());
                            objI.PrincipleAmount = Convert.ToDecimal(objDbDataReader["PrincipleAmount"].ToString());
                            objI.ExtraFee = Convert.ToDecimal(objDbDataReader["ExtraFee"].ToString());
                            objI.NumberOfInstallment = Convert.ToByte(objDbDataReader["NumberOfInstallment"].ToString());
                            var imagePath = objDbDataReader["CustomerImage"].ToString();
                            if (imagePath.Contains("Uploads"))
                            {
                                imagePath = imagePath.Substring(imagePath.IndexOf("Uploads"));
                                imagePath = "/" + imagePath;
                                objI.CustomerImage = imagePath;
                            }
                            else
                            {
                                objI.CustomerImage = "\\Not Found\\";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }
            finally
            {
                if (objDbDataReader != null)
                {
                    objDbDataReader.Close();
                }
                objDataAccess.Dispose(objDbCommand);
            }

            return objI;
        }
    }
}