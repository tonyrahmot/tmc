﻿(function () {
    $('#btnEdit').hide();
    var oTable;
    $(document).ready(function () {
        autoDebitCreditTable();
    });

    function autoDebitCreditTable() {
        oTable = $('#AutoDebitCreditTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/Save",//'@Url.Action("Save", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
                "type": "get",
                "datatype": "json"
            },
            "lengthMenu": [[50, 25, 10, -1], [50, 25, 10, "All"]],
            "columns": [
                { "data": "Id" },
                { "data": "AccountFrom" },
                { "data": "AccountTo" },
                { "data": "Amount" },
                { "data": "IsAuto" },
                { "data": "UserName" }
            ],
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }
            ],
            "order": [[0, "desc"]]
        });
        $('#AutoDebitCreditTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#hdfId').val(data.Id);
        $('#AccountFrom').val(data.AccountFrom);
        $('#AccountTo').val(data.AccountTo);
        $('#Amount').val(data.Amount);
        if (data.IsAuto === true) {
            $("#IsAuto").prop('checked', true);
        } else {
            $("#IsAuto").prop('checked', false);
        }
        $("#AccountFrom").change();
        $("#AccountTo").change();
        $('#btnSave').hide();
        $('#btnEdit').show();
    }

    $('#btnSave').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = bindata();
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/Save",//'@Url.Action("Save", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#btnEdit').click(function () {
        var isValid = checkValidation();
        if (isValid) {
            var data = bindata();
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/Update",//'@Url.Action("Update", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
                type: "POST",
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                success: function (d) {
                    if (d.oCommonResult.Status === true) {
                        oTable.ajax.reload();
                        $('#btnSave').show();
                        $('#btnEdit').hide();
                        document.getElementById('showMessage').style.color = "green";
                    }
                    document.getElementById('showMessage').innerHTML = d.oCommonResult.Message;
                },
                error: function () {
                    document.getElementById('showMessage').innerHTML = 'Error. Please try again.';
                }
            });

        }
    });

    $('#btnClear').click(function () {
        window.location.reload();
    });

    function bindata() {
        var isAuto;
        if ($('#IsAuto').prop("checked") === true) {
            isAuto = 1;
        } else {
            isAuto = 0;
        }
        var data = {
            Id: $('#hdfId').val(),
            AccountFrom: $('#AccountFrom').val(),
            AccountTo: $('#AccountTo').val(),
            Amount: $('#Amount').val(),
            IsAuto: isAuto
        }

        return data;
    }

    function checkValidation() {
        var isValid = true;
        if ($('#AccountFrom').val().trim() === '') {
            isValid = false;
            $('#AccountFrom').parent().prev().find('span').css('visibility', 'visible');
        } else {
            $('#AccountFrom').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#AccountTo').val().trim() === '') {
            isValid = false;
            $('#AccountTo').parent().prev().find('span').css('visibility', 'visible');
        } else {
            $('#AccountTo').parent().prev().find('span').css('visibility', 'hidden');
        }

        if ($('#Amount').val().trim() === '') {
            isValid = false;
            $('#Amount').parent().prev().find('span').css('visibility', 'visible');
        } else {
            $('#Amount').parent().prev().find('span').css('visibility', 'hidden');
        }

        return isValid;
    }

    $("#AccountFrom").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/GetActiveAccountNumbers",//'@Url.Action("GetActiveAccountNumbers", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
                type: "GET",
                dataType: "json",
                data: { term: request.term, type: "2,11" },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });
    $("#AccountTo").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/GetActiveAccountNumbers",//'@Url.Action("GetActiveAccountNumbers", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
                type: "GET",
                dataType: "json",
                data: { term: request.term, type: "10" },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#AccountFrom").change(function () {
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/GetAccountTypeByNumber",//'@Url.Action("GetAccountTypeByNumber", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
            contentType: "application/json; charset=utf-8",
            data: { AccountNumber: $(this).val() },
            success: function (data) {
                $('#AccountFromType').val(data);
            }
        });
    });

    $("#AccountTo").change(function () {
        $.ajax({
            type: "GET",
            url: window.applicationBaseUrl + "FinancialTransactions/AutoDebitCredit/GetAccountTypeByNumber",//'@Url.Action("GetAccountTypeByNumber", "AutoDebitCredit", new {Area = "FinancialTransactions"})',
            contentType: "application/json; charset=utf-8",
            data: { AccountNumber: $(this).val() },
            success: function (data) {
                $('#AccountToType').val(data);
            }
        });
    });

})();
