﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TonyMultipurpose.Areas.FinancialTransactions.BLL;
using TonyMultipurpose.Areas.FinancialTransactions.Models;
using TonyMultipurpose.AuthData;
using TonyMultipurpose.DAL;
using TonyMultipurpose.Models;
using TonyMultipurpose.ReportsViewer;

namespace TonyMultipurpose.Areas.FinancialTransactions.Controllers
{
    [AuthenticationFilter]
    public class FTApprovalController : Controller
    {
        FinancialTransactionsModel _objFTModel;
        FinancialTransactionsBLL _objFinancialTransationsBLL;
        // GET: FinancialTransactions/FTApproval
        public ActionResult Index()
        {
            _objFTModel = new FinancialTransactionsModel();
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            _objFTModel.IsApproved = false;
            _objFTModel.CompanyId = Convert.ToInt16(SessionUtility.TMSessionContainer.CompanyId);
            List<FinancialTransactionsModel> FinancialTransactionsList = _objFinancialTransationsBLL.GetUnApprovedFinancialTransactionsList(_objFTModel);
            return View(FinancialTransactionsList);
        }
        // GET: FinancialTransactions/ApproveTransaction

        public ActionResult ApproveTransaction()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ApprovedTransactionList()
        {
            _objFTModel = new FinancialTransactionsModel();
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            _objFTModel.IsApproved = true;
            var tra =
                _objFinancialTransationsBLL.GetUnApprovedFinancialTransactionsList(_objFTModel).Where(a => a.IsApproved == true);
            return Json(new { data = tra }, JsonRequestBehavior.AllowGet);
        }
        public void PaymentVoucherReport(string VoucherNo, string VoucherType)
        {
            if (VoucherType == "Journal")
            {
                ReportObject.ReportName = "JournalEntryReport";
                ReportObject.VoucherNo = VoucherNo;
                Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
            }
            else if (VoucherType == "Voucher Receipt")
            {
                ReportObject.ReportName = "AccountsRecievableVoucherReport";
                ReportObject.VoucherNo = VoucherNo;
                Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
            }
            else if (VoucherType == "Voucher Payment")
            {
                ReportObject.ReportName = "AccountPaymentVoucherReport";
                ReportObject.LoanId = VoucherNo;
                Response.Redirect("~/ReportsViewer/ReportViewer.aspx");
            }

        }

       

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Approve")]
        public ActionResult Approve(FormCollection form)
        {
            _objFinancialTransationsBLL = new FinancialTransactionsBLL();
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                _objFinancialTransationsBLL.ApproveFinancialTransactions(ids);
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Reject")]
        public ActionResult Reject(FormCollection form)
        {
            List<FinancialTransactionsModel> _objFinancialTransactions = new List<FinancialTransactionsModel>();
            var ids = form.GetValues("Id");
            if (ids != null)
            {
                var totalItem = ids.Length;
                for (int i = 0; i < totalItem; i++)
                {
                    _objFTModel = new FinancialTransactionsModel();
                    _objFTModel.TransactionId = ids[i];
                    _objFTModel.RejectReason = form.GetValues(_objFTModel.TransactionId)[0];
                    _objFinancialTransactions.Add(_objFTModel);
                }
                _objFinancialTransationsBLL = new FinancialTransactionsBLL();
                _objFinancialTransationsBLL.RejectFinancialTransactions(_objFinancialTransactions);
            }
            return RedirectToAction("Index");
        }
    }
}