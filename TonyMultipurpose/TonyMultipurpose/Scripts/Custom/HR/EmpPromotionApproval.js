﻿(function () {

    empPromotionTable();
    var oTable;

    function empPromotionTable() {
        oTable = $('#EmpPromotionTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/EmpPromotionUnApprovaList",// '@Url.Action("EmpPromotionUnApprovaList", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmpCode" },
                { "data": "EmployeeName" },
                { "data": "DesignationName" },
                { "data": "Description" },
                {
                    "data": "EffectiveDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                },
                {
                    "data": "EmpCode",
                    "render": function (data) {
                        return '<a class="btn btn-primary" href="' + window.applicationBaseUrl + 'HR/HR/PromotionApproval?id=' + data + '">Approve</a>';
                    }
                }
            ],
            "order": [[0, "desc"]]
        });
    }

    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May',
        'Jun', 'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

})();