﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TonyMultipurpose.Models;

namespace TonyMultipurpose.Areas.Employee.Models
{
    public class Experience : CommonModel
    {

        [Key]
        public short ExperienceId { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Organization Name is Required")]
        [Display(Name = "Organization Name")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "Customer Name cannot be longer than 150 characters.")]
        public string OrganizationName { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Position is Required")]
        [Display(Name = "Position")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "Position cannot be longer than 150 characters.")]
        public string Position { get; set; }


        [Required(ErrorMessage = "Joining Date is Required")]
        [Display(Name = "Joining Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yy}",ApplyFormatInEditMode = true)]
        public DateTime JoiningDate { get; set; }

        [Required(ErrorMessage = "Employee name is required")]
        [Display(Name = "Employee Name")]
        public short EmployeeId { get; set; }

        public IEnumerable<EmployeeDetail> EmployeeInfo { get; set; }
        public string EmployeeName { get; set; }


        [Required(ErrorMessage = "Ending Date is Required")]
        [Display(Name = "Ending Date")]
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MMM/yy}", ApplyFormatInEditMode = true)]
        public DateTime EndingDate { get; set; }

        [Display(Name = "Responsibility")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Responsibility is Required")]
        [StringLength(500, MinimumLength = 2, ErrorMessage = "Responsibility cannot be longer than 500 characters.")]
        public string Responsibility { get; set; }

        public Experience experience { get; set; }
    }
}