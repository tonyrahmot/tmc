﻿$(document).ready(function () {
    $('#btnEdit').hide();
    //$('#btnDelete').hide();
    $('#empAllowanceInfo').hide();


    $('.datepicker').datepicker({
        dateFormat: "dd/M/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "-60:+0"
    });
    $('#EfficientDate').datepicker({
        dateFormat: 'dd/M/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+60", type: Text
    }).click(function () { $(this).focus(); });

    $("#EmpCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.applicationBaseUrl + "HR/HR/AutoCompleteEmployeeCode",//'@Url.Action("AutoCompleteEmployeeCode", "HR", new { Area = "HR" })',
                type: "GET",
                dataType: "json",
                data: { term: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }));
                }
            });
        },
        messages: {
            noResults: "",
            results: ""
        }
    });

    $("#EmpCode").change(function () {
        $('#EmployeeName').empty();
        var EmpId = $("#EmpCode").val();
        var json = { EmpId: EmpId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeInfoByEmpCode",//'@Url.Action("GetEmployeeInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                document.getElementById('labelEmployeeName').innerHTML = data.EmployeeName;
                document.getElementById('labelEmployeeDesignation').innerHTML = data.LastName;
                document.getElementById('labelEmployeeDepartment').innerHTML = data.EmailId;
                document.getElementById('labelCurrentSalary').innerHTML = data.CurrentSalary;
                $('#img-upload').attr('src', data.ImagePath);
            }
        });
    });

    $('#UpdatedSalary').change(function () {

        var isValid = CheckValidation();
        if (isValid) {
            var salary = parseInt($('#UpdatedSalary').val());
            //var basicSalary = parseInt(salary * .6);
            var da = parseInt(salary * .05);
            var hra = parseInt(salary * .2);
            var convence = parseInt(salary * .1);
            var grosssalary = parseInt(salary + da + hra + convence);
            //var houserent = parseInt((salary - basicSalary) * .8);
            //var medical = parseInt(salary - (basicSalary + houserent));
            //var medicalGive = 0;
            //var Others = 0;
            //if (medical > 3000) {
            //    medicalGive = parseInt(3000);
            //    Others = parseInt(medical - medicalGive);
            //}
            //else {
            //    medicalGive = medical;
            //}
            var empCode = $('#EmpCode').val();
            var date = $('#EfficientDate').val();
            var CSalary = $('#labelCurrentSalary').text();
            var json = { salary: salary, da: da, hra: hra, convence: convence, empCode: empCode, date: date, CSalary: CSalary, grosssalary: grosssalary };

            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "HR/HR/SaveEmployeeSalaryInformation",//'@Url.Action("SaveEmployeeSalaryInformation", "HR", new { Area = "HR" })',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                success: function () {
                    AllDataEmployeeeSalary();
                }
            });
        }

    });

    function AllDataEmployeeeSalary() {
        var EmpId = $("#EmpCode").val();
        var json = { EmpId: EmpId };

        $.ajax({
            type: "POST",
            url: window.applicationBaseUrl + "HR/HR/GetEmployeeeSalaryInfoByEmpCode",//'@Url.Action("GetEmployeeeSalaryInfoByEmpCode", "HR", new { Area = "HR" })',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(json),
            success: function (data) {
                $('#empAllowanceInfo').show();
                document.getElementById('labelBasic').innerHTML = data.BasicSalary;
                document.getElementById('labelHouserent').innerHTML = data.HouseRent;
                document.getElementById('labelMedical').innerHTML = data.Medical;
                //document.getElementById('labelOther').innerHTML = data.Others;
            }
        });
    }

    var oTable;
    EmployeeSalaryTable();
    function EmployeeSalaryTable() {
        oTable = $('#salaryInfoTable').DataTable({
            "ajax": {
                "url": window.applicationBaseUrl + "HR/HR/EmployeeSalaryInfo",//'@Url.Action("EmployeeSalaryInfo", "HR", new {Area = "HR" })',
                "type": "get",
                "datatype": "json"
            },
            "columns": [
                { "data": "EmployeeName" },
                { "data": "CurrentSalary" },
                { "data": "UpdatedSalary" },
                {
                    "data": "EfficientDate",
                    "render": function (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        var mon = monthNumToName(month);
                        return date.getDate() + "/" + mon + "/" + date.getFullYear();
                    }
                }
            ],
            "order": [[0, "desc"]],
        });
        $('#salaryInfoTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                oTable.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            bindDataToControls(oTable.row(this).data());
        });
    }

    function bindDataToControls(data) {
        $('#btnEdit').show();
        $('#btnSave').hide();
        document.getElementById("EmpCode").readOnly = true;
        $('#EmpCode').val(data.EmpCode);
        $('#UpdatedSalary').val(data.UpdatedSalary);
        var date = new Date(data.EfficientDate);
        var month = date.getMonth() + 1;
        var mon = monthNumToName(month);
        var efficientDate = date.getDate() + "/" + mon + "/" + date.getFullYear();
        $('#EfficientDate').val(efficientDate);
        $("#EmpCode").change();
        AllDataEmployeeeSalary();
    }

    $('#btnclear').click(function () {
        window.location.reload();
    });

    var months = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May',
            'Jun', 'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }

    function CheckValidation() {
        var isValid = true;
        if ($('#EmpCode').val().trim() === '') {
            isValid = false;
            $('#EmpCode').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EmpCode').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#EfficientDate').val().trim() === '') {
            isValid = false;
            $('#EfficientDate').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#EfficientDate').parent().prev().find('span').css('visibility', 'hidden');
        }
        if ($('#UpdatedSalary').val().trim() === '') {
            isValid = false;
            $('#UpdatedSalary').parent().prev().find('span').css('visibility', 'visible');
        }
        else {
            $('#UpdatedSalary').parent().prev().find('span').css('visibility', 'hidden');
        }
        return isValid;
    }

    $('#btnSave').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            $('#EmpCode').val('');
            $('#EfficientDate').val('');
            $('#UpdatedSalary').val('');
            $('#empAllowanceInfo').hide();
            oTable.ajax.reload();
        }
    });

    $('#btnEdit').click(function () {
        var isValid = CheckValidation();
        if (isValid) {
            var json = {
                EmpCode: $('#EmpCode').val(),
                EfficientDate: $('#EfficientDate').val(),
                UpdatedSalary: $('#UpdatedSalary').val(),
                BasicSalary: $('#labelBasic').text(),
                HouseRent: $('#labelHouserent').text(),
                Medical: $('#labelMedical').text(),
                //Others: $('#labelOther').text(),
            };

            $.ajax({
                type: "POST",
                url: window.applicationBaseUrl + "HR/HR/UpdateEmployeeSalaryInformation",//'@Url.Action("UpdateEmployeeSalaryInformation", "HR", new { Area = "HR" })',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(json),
                success: function (data) {
                    $('#EmpCode').val('');
                    $('#EfficientDate').val('');
                    $('#UpdatedSalary').val('');
                    $('#empAllowanceInfo').hide();
                    oTable.ajax.reload();
                }
            });
        }
    });

    $('#UpdatedSalary').keypress(function () {
        var evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        else if (charCode == 13 || charCode == 46) {
            return false;
        }
        status = "";
        return true;
    });

});